package com.kindiedays.common.pushnotifications;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.ui.MainActivity;
import com.starcut.starflight.StarFlightBroadcastReceiver;

import java.util.List;
import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by pleonard on 09/09/2015.
 */
public abstract class BroadcastReceiver extends StarFlightBroadcastReceiver {
    protected Class clazz;
    private static final String CONVERSATION_ACTIVITY = "ConversationActivity";
    private static final String TAG = "BroadcastReceiver";
    private static final String ERROR = "error";
    private static final String NEW_MESSAGE = "CONVERSATION_MESSAGE_ADDED";
    private static final String NEW_EVENT = "EVENT_MESSAGE_ADDED";

    @Override
    public void onReceive(Context context, String subject, String text, String url, String badgeNumber) {
        //Ignore notification if user said so
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean usePush = sharedPref.getBoolean(MainActivity.NOTIFICATIONS, true);
        if (!usePush) {
            Log.i(getClass().getName(), "Ignored push message by user preference:" + text);
            return;
        }
        Intent intent = new Intent(context, clazz);
        intent.putExtra(MainActivity.OPEN_FROM_NOTIFICATION, true);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Random random = new Random();
        int uniqueIntegerNumber = random.nextInt(9999 - 1000) + 1000;

        if (isAppIsInBackground(context)) {
            Notification notification;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Notification.Builder mBuilder;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    mBuilder = new Notification.Builder(context, KindieDaysApp.getApp().getChanelId());
                } else {
                    mBuilder = new Notification.Builder(context);
                }
                mBuilder.setLargeIcon(((BitmapDrawable) ContextCompat.getDrawable(context, R.mipmap.ic_launcher)).getBitmap())
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(KindieDaysApp.getApp().getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle().bigText(text))
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setLights(Color.BLUE, 3000, 3000)
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setContentText(text)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent);
                notification = mBuilder.build();
            } else {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setLargeIcon(((BitmapDrawable) ContextCompat.getDrawable(context, R.mipmap.ic_launcher)).getBitmap())
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(KindieDaysApp.getApp().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setLights(Color.BLUE, 3000, 3000)
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setContentText(text)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent);
                notification = mBuilder.build();
            }
            notificationManager.notify(uniqueIntegerNumber, notification);
        } else if (!isAppIsInBackground(context) && getCurrentActivityName(context).equals(CONVERSATION_ACTIVITY)) {
            Log.d(TAG, "Message arrived");
        } else {
            Notification notification;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Notification.Builder mBuilder;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    mBuilder = new Notification.Builder(context, KindieDaysApp.getApp().getChanelId());
                } else {
                    mBuilder = new Notification.Builder(context);
                }
                mBuilder.setLargeIcon(((BitmapDrawable) ContextCompat.getDrawable(context, R.mipmap.ic_launcher)).getBitmap())
                        .setLargeIcon(((BitmapDrawable) ContextCompat.getDrawable(context, R.mipmap.ic_launcher)).getBitmap())
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(KindieDaysApp.getApp().getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle().bigText(text))
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setContentText(text)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent);
                notification = mBuilder.build();
            } else {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setLargeIcon(((BitmapDrawable) ContextCompat.getDrawable(context, R.mipmap.ic_launcher)).getBitmap())
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(KindieDaysApp.getApp().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setContentText(text)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent);
                notification = mBuilder.build();
            }
            notificationManager.notify(uniqueIntegerNumber, notification);
        }

        int counter;
        try {
            counter = Integer.parseInt(badgeNumber);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("iconCounter", counter);
            editor.apply();
        } catch (NumberFormatException e) {
            counter = 0;

        }

        if (counter != 0) {
            Log.d("App icon counter: ", String.valueOf(badgeNumber));
            ShortcutBadger.applyCount(context, counter);
        }

        if (subject.equals(NEW_MESSAGE)) {
            Intent refreshView = new Intent("message_notification");
            LocalBroadcastManager.getInstance(context).sendBroadcast(refreshView);
        } else if (subject.equals(NEW_EVENT)) {
            Intent refreshView = new Intent("event_notification");
            LocalBroadcastManager.getInstance(context).sendBroadcast(refreshView);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    public static boolean doesNotificationChannelExist(final Context context, String channelId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel notificationChannel = notificationManager.getNotificationChannel(channelId);
        return notificationChannel != null;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    public static void createNotificationChannel(final Context context, String channelId) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            String name = context.getString(R.string.channel_name);
            NotificationChannel channel = new NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(context.getString(R.string.channel_description));
            channel.enableLights(true);
            channel.setLightColor(Color.GREEN);
            channel.enableVibration(true);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private String getCurrentActivityName(Context context) {
        PackageManager pm = context.getPackageManager();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        ActivityInfo app;
        try {
            app = pm.getActivityInfo(componentInfo, 0);
            return app.name.substring(app.name.lastIndexOf(".") + 1).trim();
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "getCurrentActivityName: ", e);
            return ERROR;
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}
