package com.kindiedays.common.model;


import android.graphics.Rect;

import org.parceler.Parcel;

@Parcel
public class BlurredRect {
    private int left;
    private int top;
    private int right;
    private int bottom;

    public BlurredRect() {
        //default
    }

    public BlurredRect(int left, int top, int right, int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public BlurredRect(Rect rect) {
        this.left = rect.left;
        this.top = rect.top;
        this.right = rect.right;
        this.bottom = rect.bottom;
    }

    public Rect getRect() {
        return new Rect(left, top, right, bottom);
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }
}
