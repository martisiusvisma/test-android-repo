package com.kindiedays.common.model;


import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class BlurredImageWrapper {
    private String url;
    private int width;
    private int height;
    private List<BlurredRect> tags;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<BlurredRect> getTags() {
        return tags;
    }

    public void setTags(List<BlurredRect> tags) {
        this.tags = tags;
    }

    public void addTag(BlurredRect tag) {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        tags.add(tag);
    }
}
