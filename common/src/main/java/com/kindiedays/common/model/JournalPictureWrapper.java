package com.kindiedays.common.model;


import android.support.annotation.NonNull;

import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.pojo.JournalPostPicture;

import org.joda.time.DateTime;

import java.util.List;

public class JournalPictureWrapper {
    private JournalPost journalPost;

    public JournalPictureWrapper() {
        //default
    }

    public JournalPictureWrapper(@NonNull JournalPost journalPost) {
        this.journalPost = journalPost;
    }

    public void setJournalPost(JournalPost journalPost) {
        this.journalPost = journalPost;
    }

    public long getId() {
        return journalPost.getId();
    }

    public String getText() {
        return journalPost.getText();
    }

    public DateTime getTime() {
        return journalPost.getTime();
    }

    public List<JournalPostPicture> getPictures() {
        return journalPost.getJournalPostPictures();
    }

    public int getNbPictures() {
        return journalPost.getJournalPostPictures().isEmpty()
                ? 0
                : journalPost.getJournalPostPictures().get(0).getNbPictures();
    }
}
