package com.kindiedays.common.ui.dailyreport;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.MedRes;
import com.kindiedays.common.utils.PictureUtils;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DRMedicationActivity extends AppCompatActivity {
    private static final String MED = "MED";
    private TextView noteLbl;
    private TextView timeLbl;
    private TextView medicineLbl;
    private TextView dosageLbl;
    private TextView medicineLblTitle;
    private TextView dosageLblTitle;
    Intent intent;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_medication);

        intent = this.getIntent();
        bundle = intent.getExtras();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextViewCustom title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.daily_medication);

        ImageView sectionIcon = (ImageView) findViewById(R.id.rightIcon);
        sectionIcon.setVisibility(View.VISIBLE);
        sectionIcon.setImageDrawable(PictureUtils.svgToBitmapDrawable(getResources(), R.raw.dr_medication_bg, 80));

        timeLbl = (TextView) findViewById(R.id.timeLbl);
        noteLbl = (TextView) findViewById(R.id.noteContentLbl);
        medicineLblTitle = (TextView) findViewById(R.id.medicineTitle);
        medicineLbl = (TextView) findViewById(R.id.medicineContent);
        dosageLblTitle = (TextView) findViewById(R.id.dosageTitle);
        dosageLbl = (TextView) findViewById(R.id.dosageContent);
        setContentView();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            overridePendingTransition(R.anim.from_left_to_right, R.anim.exit_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setContentView(){
        MedRes data = (MedRes) bundle.getSerializable(MED);
        if (data != null) {
            timeLbl.setText(data.getTime().toString("HH:mm"));
            medicineLblTitle.setText(R.string.Medicine);
            medicineLbl.setText(data.getDrugName());
            dosageLblTitle.setText(R.string.Dosage);
            dosageLbl.setText(data.getDosage());

            if (!"null".equals(data.getNotes())) {
                noteLbl.setText(data.getNotes());
            } else {
                noteLbl.setVisibility(View.GONE);
            }
        }

    }

}
