package com.kindiedays.common.ui.picturegallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.common.pojo.GalleryPicture;

/**
 * Created by pleonard on 16/09/2015.
 */
public class GalleryPictureFragment extends AbstractPictureFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        caption.setText(((GalleryPicture) picture).getCaption());
        return v;
    }
}