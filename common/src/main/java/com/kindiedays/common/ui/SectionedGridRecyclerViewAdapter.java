package com.kindiedays.common.ui;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Gabriele Mariotti (gabri.mariotti@gmail.com)
 */
@SuppressWarnings({"squid:S1068", "squid:S1226"})
public class SectionedGridRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private static final int SECTION_TYPE = 0;
    private static final int HEADER_TYPE = 100;
    private static final int FOOTER_TYPE = 101;

    private boolean mValid = true;
    private int mSectionResourceId;
    private int[] mTextResourceId;
    private LayoutInflater mLayoutInflater;
    private RecyclerView.Adapter mBaseAdapter;
    private SparseArray<Section> mSections = new SparseArray<>();
    private RecyclerView mRecyclerView;
    private View header;
    private View footer;

    public SectionedGridRecyclerViewAdapter(Context context, int sectionResourceId
            , int[] textResourceId, RecyclerView recyclerView, RecyclerView.Adapter baseAdapter) {

        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSectionResourceId = sectionResourceId;
        mTextResourceId = textResourceId;
        mBaseAdapter = baseAdapter;
        mContext = context;
        mRecyclerView = recyclerView;



        mBaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                mValid = mBaseAdapter.getItemCount()>0;
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount()>0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount()>0;
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount()>0;
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        });

        final GridLayoutManager layoutManager = (GridLayoutManager)(mRecyclerView.getLayoutManager());
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0 && header != null){
                    return layoutManager.getSpanCount();
                }
                else if(footer != null && position == getItemCount() - 1){
                    return layoutManager.getSpanCount();
                }
                else{
                    if(header != null){
                        position--;
                    }
                    return (isSectionHeaderPosition(position))? layoutManager.getSpanCount() : 1 ;
                }

            }
        });
    }

    public View getHeader() {
        return header;
    }

    public void setHeader(View header) {
        this.header = header;
    }

    public View getFooter() {
        return footer;
    }

    public void setFooter(View footer) {
        this.footer = footer;
    }


    public static class SectionViewHolder extends RecyclerView.ViewHolder {

        private List<TextView> textViews = new ArrayList<>();

        public SectionViewHolder(View view,int[] mTextResourceid) {
            super(view);
            for(int i = 0; i < mTextResourceid.length; i++){
                textViews.add(i,(TextView) view.findViewById(mTextResourceid[i]));
            }
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{

        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder{

        public FooterViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int typeView) {
        if (typeView == SECTION_TYPE) {
            final View view = LayoutInflater.from(mContext).inflate(mSectionResourceId, parent, false);
            return new SectionViewHolder(view, mTextResourceId);
        }
        else if(typeView == HEADER_TYPE){
            return new HeaderViewHolder(header);
        }
        else if(typeView == FOOTER_TYPE){
            return new FooterViewHolder(footer);
        }
        else{
            return mBaseAdapter.onCreateViewHolder(parent, typeView -1);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder sectionViewHolder, int position) {
        if(header != null && position == 0){
            //Bind header
        }
        else if(footer != null && position == getItemCount() - 1){
            //Bind footer
        }
        else{
            if(header != null){
                position--;
            }
            if (isSectionHeaderPosition(position)) {
                for(int i = 0 ; i < ((SectionViewHolder)sectionViewHolder).textViews.size() ; i++){
                    ((SectionViewHolder)sectionViewHolder).textViews.get(i).setText(mSections.get(position).text[i]);
                    sectionViewHolder.itemView.setTag(mSections.get(position).tag);
                }
            }else{
                mBaseAdapter.onBindViewHolder(sectionViewHolder,sectionedPositionToPosition(position));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(header != null && position == 0){
            return HEADER_TYPE;
        }
        else if(footer != null && position == getItemCount() - 1){
            return FOOTER_TYPE;
        }
        else{
            if(header != null){
                position--;
            }
            return isSectionHeaderPosition(position)
                    ? SECTION_TYPE
                    : mBaseAdapter.getItemViewType(sectionedPositionToPosition(position)) +1 ;
        }
    }


    public static class Section {
        int firstPosition;
        int sectionedPosition;
        CharSequence[] text;
        Object tag;

        public Section(int firstPosition, CharSequence[] text) {
            this.firstPosition = firstPosition;
            this.text = text;
        }

        public Section(int firstPosition, CharSequence[] text, Object tag) {
            this.firstPosition = firstPosition;
            this.text = text;
            this.tag = tag;
        }

        public CharSequence[] getText() {
            return text;
        }
    }


    @SuppressWarnings("squid:S3358")
    public void setSections(Section[] sections) {
        mSections.clear();

        Arrays.sort(sections, (o, o1) -> (o.firstPosition == o1.firstPosition)
                ? 0
                : ((o.firstPosition < o1.firstPosition) ? -1 : 1));

        int offset = 0; // offset positions for the headers we're adding
        for (Section section : sections) {
            section.sectionedPosition = section.firstPosition + offset;
            mSections.append(section.sectionedPosition, section);
            ++offset;
        }

        notifyDataSetChanged();
    }

    public int positionToSectionedPosition(int position) {
        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).firstPosition > position) {
                break;
            }
            ++offset;
        }
        return position + offset;
    }

    public int sectionedPositionToPosition(int sectionedPosition) {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return RecyclerView.NO_POSITION;
        }

        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).sectionedPosition > sectionedPosition) {
                break;
            }
            --offset;
        }
        return sectionedPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return mSections.get(position) != null;
    }


    @Override
    public long getItemId(int position) {
        if(position == 0 && header != null){
            return header.getId();
        }
        else if(position == this.getItemCount() - 1 && footer != null){
            return footer.getId();
        }
        else{
            if(header != null){
                position--;
            }
            return isSectionHeaderPosition(position)
                    ? Integer.MAX_VALUE - mSections.indexOfKey(position)
                    : mBaseAdapter.getItemId(sectionedPositionToPosition(position));
        }
    }

    @Override
    public int getItemCount() {
        int count = (mValid ? mBaseAdapter.getItemCount() + mSections.size() : 0);
        if(header != null){
            count++;
        }
        if(footer != null){
            count++;
        }
        return count;
    }

}