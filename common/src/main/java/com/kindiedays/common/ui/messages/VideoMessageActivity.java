package com.kindiedays.common.ui.messages;


import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kindiedays.common.R;
import com.kindiedays.common.network.conversations.GetMediaFile;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.ui.KindieDaysActivity;

import org.parceler.Parcels;

public class VideoMessageActivity extends KindieDaysActivity implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener {
    private static final String EXTRA_ATTACHMENT = "extra_attach";
    private static final String EXTRA_POSITION = "extra_pos";
    private static final String EXTRA_VIDEO_STATE = "extra_state";
    private static final int VIDEO_PAUSED = 1;
    private static final int VIDEO_PLAYING = 0;

    private VideoView videoView;
    private MediaController mediaController;
    private ProgressBar pbLoader;
    private RelativeLayout rlContent;

    private Attachment attachment;
    private int savedPosition;
    private int videoState;

    private android.content.BroadcastReceiver brDownloadComplete = new android.content.BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                showVideoDownloaded();
            }
        }
    };

    public static void start(final Context context, Attachment attachment) {
        Intent intent = new Intent(context, VideoMessageActivity.class);
        intent.putExtra(EXTRA_ATTACHMENT, Parcels.wrap(attachment));
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_message);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        savedPosition = 0;
        if (savedInstanceState != null) {
            savedPosition = savedInstanceState.getInt(EXTRA_POSITION);
            videoState = savedInstanceState.getInt(EXTRA_VIDEO_STATE);
        }

        attachment = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_ATTACHMENT));
        videoView = (VideoView) findViewById(R.id.vv_content);
        pbLoader = (ProgressBar) findViewById(R.id.pb_loader);
        rlContent = (RelativeLayout) findViewById(R.id.rl_content);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setVideoURI(Uri.parse(attachment.getUri()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(brDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    protected void onPause() {
        super.onPause();
        savedPosition = videoView.getCurrentPosition();
        videoView.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(brDownloadComplete);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int videoStateView = videoView.isPlaying() ? VIDEO_PLAYING : VIDEO_PAUSED;
        outState.putInt(EXTRA_POSITION, videoView.getCurrentPosition());
        outState.putInt(EXTRA_VIDEO_STATE, videoStateView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_message_photo_gallery, menu);
        return true;
    }

    @SuppressWarnings("squid:S3516")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        } else if (i == R.id.action_save) {
            DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            GetMediaFile.downloadFile(this, downloadManager, attachment.getFilename(), attachment.getUri());
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (!mediaController.isShowing()) {
            mediaController.show();
            mp.seekTo(0);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        pbLoader.setVisibility(View.GONE);
        playVideoFromPosition(savedPosition);
    }

    private void playVideoFromPosition(int position) {
        videoView.seekTo(position);
        if (videoState != VIDEO_PAUSED) {
            videoView.start();
        }
    }

    @SuppressWarnings("squid:S3398")
    private void showVideoDownloaded() {
        TSnackbar snackbar = TSnackbar.make(rlContent, getString(R.string.video_downloaded),
                TSnackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
