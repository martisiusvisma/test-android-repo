package com.kindiedays.common.ui.picturegallery;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.R;
import com.kindiedays.common.pojo.AbstractPicture;
import com.kindiedays.common.utils.PictureUtils;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

/**
 * Created by pleonard on 04/12/2015.
 */
public class AbstractPictureFragment extends Fragment {
    protected ImageViewTouch imageView;
    protected TextView caption;
    protected ProgressBar progressBar;
    protected AbstractPicture picture;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_picture, null);
        caption = (TextView) v.findViewById(R.id.caption);
        imageView = (ImageViewTouch) v.findViewById(R.id.image);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        Point screenDimensions = PictureUtils.getScreenDimensions(getActivity());
        if (picture == null) {
            return v;
        }
        Glide.with(getActivity()).load(picture.getUrl() + "&preferredWidth=" + screenDimensions.x + "&preferredHeight=" + screenDimensions.y).into(new GlideDrawableImageViewTarget(imageView) {
            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                super.onResourceReady(drawable, anim);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                progressBar.setVisibility(View.GONE);
            }
        });
        return v;
    }

    public void setPicture(AbstractPicture picture){
        this.picture = picture;
    }

}
