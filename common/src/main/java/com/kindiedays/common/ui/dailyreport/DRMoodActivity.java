package com.kindiedays.common.ui.dailyreport;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.daily.ResourceFaceTab;
import com.kindiedays.common.pojo.MoodRes;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.common.utils.constants.DailyReportConstants;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DRMoodActivity extends AppCompatActivity {
    private static final String MOOD = "MOOD";
    private TextView noteLbl;
    private TextView timeLbl;
    FragmentTabHost moodTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_mood);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextViewCustom title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.daily_mood);

        ImageView sectionIcon = (ImageView) findViewById(R.id.rightIcon);
        sectionIcon.setVisibility(View.VISIBLE);
        sectionIcon.setImageDrawable(PictureUtils.svgToBitmapDrawable(getResources(), R.raw.dr_mood_happy_bg, 80));

        timeLbl = (TextView) findViewById(R.id.timeLbl);
        noteLbl = (TextView) findViewById(R.id.noteContentLbl);
        moodTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        moodTabHost.setup(this, this.getSupportFragmentManager(), R.id.realtabcontent);
        TabHost.TabSpec ts = moodTabHost.newTabSpec("smile").setIndicator(createTabViewDrawable(R.raw.dr_mood_happy_circle, R.raw.dr_mood_happy_circle_tp));
        Bundle bundle = new Bundle();
        bundle.putString(DailyReportConstants.TYPE, DailyReportConstants.SMILING);
        moodTabHost.addTab(ts, FaceFragment.class, bundle);

        TabHost.TabSpec ts2 = moodTabHost.newTabSpec("serious").setIndicator(createTabViewDrawable(R.raw.dr_mood_ok_circle, R.raw.dr_mood_ok_circle_tp));
        Bundle bundle2 = new Bundle();
        bundle2.putString(DailyReportConstants.TYPE, DailyReportConstants.SERIOUS);
        moodTabHost.addTab(ts2, FaceFragment.class, bundle2);

        TabHost.TabSpec ts3 = moodTabHost.newTabSpec("sad").setIndicator(createTabViewDrawable(R.raw.dr_mood_sad_circle, R.raw.dr_mood_sad_circle_tp));
        Bundle bundle3 = new Bundle();
        bundle3.putString(DailyReportConstants.TYPE, DailyReportConstants.SAD);
        moodTabHost.addTab(ts3, FaceFragment.class, bundle3);

        setContentView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            overridePendingTransition(R.anim.from_left_to_right, R.anim.exit_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class FaceFragment extends Fragment {
    }

    private ResourceFaceTab createTabViewDrawable(int pressed, int normal) {
        BitmapDrawable pressedBmp = PictureUtils.svgToBitmapDrawable(getResources(), pressed, 80);
        BitmapDrawable normalBmp = PictureUtils.svgToBitmapDrawable(getResources(), normal, 80);

        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_selected}, pressedBmp);
        res.addState(new int[]{}, normalBmp);
        ResourceFaceTab indicator = new ResourceFaceTab(this);
        indicator.setBackgroundDrawable(res);
        return indicator;
    }

    private void setContentView() {
        Bundle bundle = getIntent().getExtras();
        MoodRes data = (MoodRes) bundle.getSerializable(MOOD);
        if (data != null) {
            if (DailyReportConstants.SMILING.equals(data.getTemper())) {
                moodTabHost.setCurrentTab(0);
            } else if (DailyReportConstants.SERIOUS.equals(data.getTemper())) {
                moodTabHost.setCurrentTab(1);
            } else {
                moodTabHost.setCurrentTab(2);
            }
            for (int i = 0; i < 3; i++) {
                moodTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(false);
            }
            timeLbl.setText(data.getTime().toString("HH:mm"));
            noteLbl.setText(data.getNotes());

        }
    }

}
