package com.kindiedays.common.ui.dailyreport;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.daily.ResourceFaceTab;
import com.kindiedays.common.network.menus.GetDRDrink;
import com.kindiedays.common.network.menus.GetMenus;
import com.kindiedays.common.pojo.DrinkRes;
import com.kindiedays.common.pojo.Meal;
import com.kindiedays.common.pojo.MealRes;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.common.utils.constants.DailyReportConstants;

import org.joda.time.LocalDate;

import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DRMealActivity extends AppCompatActivity {
    private static final String MEAL = "MEAL";
    private TextView plateLbl;
    private TextView drinkLbl;

    private FragmentTabHost plateTabHost;
    private FragmentTabHost drinkTabHost;

    private static final String TYPE = "Type";

    private static final String PLATE_EMPTY_TAG = "Smile";
    private static final String PLATE_HALF_TAG = "Serious";
    private static final String PLATE_FULL_TAG = "Sad";

    private static final String DRINK_EMPTY_TAG = PLATE_EMPTY_TAG;
    private static final String DRINK_HALF_TAG = PLATE_HALF_TAG;
    private static final String DRINK_FULL_TAG = PLATE_FULL_TAG;

    private Integer mealId = -1;
    private Integer drinkId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_meal);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        TextViewCustom title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.daily_meal);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        ImageView sectionIcon = (ImageView) findViewById(R.id.rightIcon);
        sectionIcon.setVisibility(View.VISIBLE);
        sectionIcon.setImageDrawable(PictureUtils.svgToBitmapDrawable(getResources(), R.raw.dr_meals_bg, 80));

        drinkTabHost = (FragmentTabHost) findViewById(R.id.drinktabhost);
        drinkTabHost.setup(this, this.getSupportFragmentManager(), R.id.realdrinktabcontent);

        plateTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        plateTabHost.setup(this, this.getSupportFragmentManager(), R.id.realplatetabcontent);

        // PLATE TAB LAYOUT

        TabHost.TabSpec ts = plateTabHost.newTabSpec("plate1").setIndicator(createTabViewDrawable(R.raw.dr_meals_plate_empty_circle, R.raw.dr_meals_plate_empty_circle_tp));
        Bundle bundle = new Bundle();
        bundle.putString(TYPE, PLATE_EMPTY_TAG);
        plateTabHost.addTab(ts, FaceFragment.class, bundle);

        TabHost.TabSpec ts2 = plateTabHost.newTabSpec("plate2").setIndicator(createTabViewDrawable(R.raw.dr_meals_plate_half_circle, R.raw.dr_meals_plate_half_circle_tp));
        Bundle bundle2 = new Bundle();
        bundle2.putString(TYPE, PLATE_HALF_TAG);
        plateTabHost.addTab(ts2, FaceFragment.class, bundle2);

        TabHost.TabSpec ts3 = plateTabHost.newTabSpec("plate3").setIndicator(createTabViewDrawable(R.raw.dr_meals_plate_full_circle, R.raw.dr_meals_plate_full_circle_tp));
        Bundle bundle3 = new Bundle();
        bundle3.putString(TYPE, PLATE_FULL_TAG);
        plateTabHost.addTab(ts3, FaceFragment.class, bundle3);

        // DRINK TAB LAYOUT
        TabHost.TabSpec ts4 = drinkTabHost.newTabSpec("drink1").setIndicator(createTabViewDrawable(R.raw.dr_meals_glass_empty_circle, R.raw.dr_meals_glass_empty_circle_tp));
        Bundle bundle4 = new Bundle();
        bundle.putString(TYPE, DRINK_EMPTY_TAG);
        drinkTabHost.addTab(ts4, FaceFragment.class, bundle4);

        TabHost.TabSpec ts5 = drinkTabHost.newTabSpec("drink2").setIndicator(createTabViewDrawable(R.raw.dr_meals_glass_half_circle, R.raw.dr_meals_glass_half_circle_tp));
        Bundle bundle5 = new Bundle();
        bundle.putString(TYPE, DRINK_HALF_TAG);
        drinkTabHost.addTab(ts5, FaceFragment.class, bundle5);

        TabHost.TabSpec ts6 = drinkTabHost.newTabSpec("drink3").setIndicator(createTabViewDrawable(R.raw.dr_meals_glass_full_circle, R.raw.dr_meals_glass_full_circle_tp));
        Bundle bundle6 = new Bundle();
        bundle.putString(TYPE, DRINK_FULL_TAG);
        drinkTabHost.addTab(ts6, FaceFragment.class, bundle6);

        setContentView();
    }

    private ResourceFaceTab createTabViewDrawable(int pressed, int normal) {
        BitmapDrawable pressedBmp = PictureUtils.svgToBitmapDrawable(getResources(), pressed, 80);
        BitmapDrawable normalBmp = PictureUtils.svgToBitmapDrawable(getResources(), normal, 80);

        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_selected}, pressedBmp);
        res.addState(new int[]{}, normalBmp);
        ResourceFaceTab indicator = new ResourceFaceTab(this);
        indicator.setBackgroundDrawable(res);
        return indicator;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            overridePendingTransition(R.anim.from_left_to_right, R.anim.exit_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("squid:S1185")
    public static class FaceFragment extends Fragment {
        @Override
        public void onStart() {
            super.onStart();
            // When you tap a face
        }
    }

    @SuppressWarnings("squid:S106")
    private void setContentView() {
        Bundle bundle = getIntent().getExtras();
        MealRes data = (MealRes) bundle.getSerializable(MEAL);
        if (data != null) {
            TextView timeLbl = (TextView) findViewById(R.id.timeLbl);
            TextView noteLbl = (TextView) findViewById(R.id.noteContentLbl);
            plateLbl = (TextView) findViewById(R.id.whatDidIEat);
            drinkLbl = (TextView) findViewById(R.id.whatDidIDrink);

            mealId = data.getMealIndex();
            drinkId = data.getDrink();

            setFoodDrink();
            noteLbl.setText(data.getNotes());
            timeLbl.setText(data.getCreated().toString("HH:mm"));

            System.out.println("Drink quantity: " + data.getDrinkQuantity());
            if ("1.0".equals(data.getDrinkQuantity())) {
                drinkTabHost.setCurrentTab(2);
                drinkTabHost.setVisibility(View.VISIBLE);
            } else if ("0.5".equals(data.getDrinkQuantity())) {
                drinkTabHost.setCurrentTab(1);
                drinkTabHost.setVisibility(View.VISIBLE);
            } else if ("0.0".equals(data.getDrinkQuantity())) {
                drinkTabHost.setCurrentTab(0);
                drinkTabHost.setVisibility(View.VISIBLE);
            } else {
                drinkTabHost.setVisibility(View.GONE);
            }

            if (DailyReportConstants.NOT_ATE.equals(data.getReaction())) {
                plateTabHost.setCurrentTab(0);
                plateTabHost.setVisibility(View.VISIBLE);
            } else if (DailyReportConstants.ATE_SOME.equals(data.getReaction())) {
                plateTabHost.setCurrentTab(1);
                plateTabHost.setVisibility(View.VISIBLE);
            } else if (DailyReportConstants.ATE_WELL.equals(data.getReaction())) {
                plateTabHost.setCurrentTab(2);
                plateTabHost.setVisibility(View.VISIBLE);
            } else {
                plateTabHost.setVisibility(View.GONE);
            }

            for (int i = 0; i < 3; i++) {
                plateTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(false);
                drinkTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(false);
            }
        }
    }

    private void setFoodDrink() {
        if (mealId != null){
            GetMealsAsyncTask mealsList = new GetMealsAsyncTask(plateLbl, mealId);
            mealsList.execute();
        }
        if (drinkId != null){
            GetDrinksAsyncTask drinksList = new GetDrinksAsyncTask(drinkLbl, drinkId);
            drinksList.execute();
        }
    }

    public static void setFood(TextView textView, Integer mealId) {
        if (mealId != null) {
            GetMealsAsyncTask mealsList = new GetMealsAsyncTask(textView, mealId);
            mealsList.execute();
        }
    }

    public static void setDrink(TextView textView, Integer drinkId) {
        if (drinkId != null) {
            GetDrinksAsyncTask drinksList = new GetDrinksAsyncTask(textView, drinkId);
            drinksList.execute();
        }
    }

    private static class GetMealsAsyncTask extends NetworkAsyncTask<List<Menu>> {
        private int mealId;
        private TextView plateLbl;

        GetMealsAsyncTask (TextView textView, int mealId){
            this.mealId = mealId;
            this.plateLbl = textView;
        }

        @Override
        protected List<Menu> doNetworkTask() throws Exception {
            try {
                return GetMenus.getMenus();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.menu_list).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Menu> menus) {
            super.onPostExecute(menus);
            for (int i = 0; i < menus.size(); i++) {
                LocalDate mealDate = menus.get(i).getDate();
                LocalDate today = new LocalDate();
                if (mealDate.compareTo(today) == 0) {

                    List<Meal> menuOfTheDay = menus.get(i).getMeals();
                    for (Meal item : menuOfTheDay) {
                        if (item.getIndex().equals(mealId) && mealId != -1) {
                            plateLbl.setText(item.getText());
                        }
                    }

                }
            }
        }
    }


    private static class GetDrinksAsyncTask extends NetworkAsyncTask<List<DrinkRes>> {
        private int drinkId;
        private TextView drinkLbl;

        GetDrinksAsyncTask (TextView textView, int drinkId){
            this.drinkId = drinkId;
            this.drinkLbl = textView;
        }

        @Override
        protected void onPostExecute(List<DrinkRes> drinks) {
            super.onPostExecute(drinks);

            for (DrinkRes item : drinks) {
                if (item.getId().equals(drinkId) && drinkId != -1) {
                    drinkLbl.setText(item.getName());
                }
            }

        }

        @Override
        protected List<DrinkRes> doNetworkTask() throws Exception {
            return GetDRDrink.getItems();
        }
    }

}
