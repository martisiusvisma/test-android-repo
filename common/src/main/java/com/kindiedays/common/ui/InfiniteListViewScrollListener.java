package com.kindiedays.common.ui;

import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

/**
 * Created by pleonard on 13/07/2015.
 */
public class InfiniteListViewScrollListener implements AbsListView.OnScrollListener {

    public interface EndListReachedListener{
        public void topReached();
        public void bottomReached();
    }

    private int oldTop;
    private int oldFirstVisibleItem;
    private int scrollingDirection;

    private EndListReachedListener listener;

    private static final int SCROLL_UP = 1;
    private static final int SCROLL_DOWN = 2;
    private boolean bottomReached;

    public InfiniteListViewScrollListener(EndListReachedListener listener){
        this.listener = listener;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        //ignore
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        onDetectedListScroll(view, firstVisibleItem);
        if(scrollingDirection == SCROLL_UP && firstVisibleItem == 0){
            listener.topReached();
        }

        if(scrollingDirection == SCROLL_DOWN && view.getLastVisiblePosition() == view.getAdapter().getCount()  - 1 ){
            listener.bottomReached();
        } else if (view.getLastVisiblePosition() > 0
                && (view.getLastVisiblePosition() == view.getAdapter().getCount()  - 1)
                && (scrollingDirection == 0 || scrollingDirection == SCROLL_DOWN)
                && !bottomReached
                && view.getAdapter().getCount() % 15 == 0) {
            listener.bottomReached();
            bottomReached = true;
        }
    }

    private void onDetectedListScroll(AbsListView absListView, int firstVisibleItem) {
        View view = absListView.getChildAt(0);
        int top = (view == null) ? 0 : view.getTop();

        if (firstVisibleItem == oldFirstVisibleItem) {
            if (top > oldTop) {
                scrollingDirection = SCROLL_UP;
                bottomReached = false;
            } else if (top < oldTop) {
                scrollingDirection = SCROLL_DOWN;
                bottomReached = false;
            }
        } else {
            if (firstVisibleItem < oldFirstVisibleItem) {
                scrollingDirection = SCROLL_UP;
                bottomReached = false;
            } else {
                scrollingDirection = SCROLL_DOWN;
                bottomReached = false;
            }
        }

        oldTop = top;
        oldFirstVisibleItem = firstVisibleItem;
    }
}
