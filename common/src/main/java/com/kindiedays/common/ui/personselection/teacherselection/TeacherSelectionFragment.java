package com.kindiedays.common.ui.personselection.teacherselection;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.ui.ChildTouchListener;
import com.kindiedays.common.ui.personselection.PeopleSelectionFragment;
import com.kindiedays.common.ui.personselection.PersonButtonAdapter;
import com.kindiedays.common.ui.personselection.PersonSelectionActivity;

import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by pleonard on 13/11/2015.
 */
public class TeacherSelectionFragment extends PeopleSelectionFragment {

    PersonSelectionActivity activity;

    @Override
    protected PersonButtonAdapter initPeopleAdapter() {
        return new TeacherButtonAdapter(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (PersonSelectionActivity) getActivity();
        View v = super.onCreateView(inflater, container, savedInstanceState);

        recyclerView.setAdapter(personAdapter);
        recyclerView.addOnItemTouchListener(new ChildTouchListener(this));
        new GetConversationPartiesAsyncTask(activity.getSelectedTeacherIds()).execute();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        ConversationPartyBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ConversationPartyBusiness.getInstance().deleteObserver(this);
    }

    private class GetConversationPartiesAsyncTask extends ConversationPartyBusiness.LoadConversationParties {

        public GetConversationPartiesAsyncTask(List<Long> selected) {
            super(selected);
        }

        @Override
        protected void onPreExecute() {

        }

        protected List<ConversationParty> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {

                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<ConversationParty> conversationParties) {
            ((TeacherButtonAdapter) personAdapter).setParties(ConversationPartyBusiness.getInstance().getConversationPartyPerType().get(ConversationParty.TEACHER_TYPE));
        }
    }
}
