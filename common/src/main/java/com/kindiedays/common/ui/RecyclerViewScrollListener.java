package com.kindiedays.common.ui;

import android.support.v7.widget.RecyclerView;

/**
 * Created by pleonard on 10/11/2015.
 */
public class RecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private int currentState;

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        currentState = newState;
    }

    public int getCurrentState() {
        return currentState;
    }
}
