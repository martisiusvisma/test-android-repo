package com.kindiedays.common.ui.personselection;

import android.os.Bundle;
import android.view.MenuItem;

import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.KindieDaysActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 13/11/2015.
 */
public abstract class PersonSelectionActivity extends KindieDaysActivity {

    public static final String SELECTED_TEACHER_IDS = "selectedTeacherIds";
    protected List<Long> selectedTeachers = new ArrayList<>();

    public abstract void onPersonClick(Person person);

    public abstract void onPersonLongClick(Person person);

    protected abstract void setResult();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//Getting rid of drop shadow on android 5

        long[] teacherIds = getIntent().getLongArrayExtra(SELECTED_TEACHER_IDS);

        if(teacherIds != null){
            for(long id : teacherIds){
                selectedTeachers.add(id);
            }
        }
    }

    public List<Long> getSelectedTeacherIds() {
        return selectedTeachers;
    }

    public void setSelectedTeacherIds(List<Long> selectedTeachers) {
        this.selectedTeachers = selectedTeachers;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        if(item.getItemId() == R.id.action_ok){
            setResult();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
