package com.kindiedays.common.ui.messages;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.utils.exceptions.CastListenerException;

public class AttachmentDialog extends DialogFragment {
    private static final String TAG = AttachmentDialog.class.getSimpleName();

    private OnButtonClickListener listener;

    public static AttachmentDialog newInstance() {
        return new AttachmentDialog();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnButtonClickListener) {
            listener = (OnButtonClickListener) context;
        } else {
            throw new CastListenerException(context);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);
        return inflater.inflate(R.layout.dialog_choose_attachment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout llAddMedia = (LinearLayout) view.findViewById(R.id.ll_add_media);
        LinearLayout llAddDocument = (LinearLayout) view.findViewById(R.id.ll_add_document);
        TextView tvCancel = (TextView) view.findViewById(R.id.tv_cancel);

        llAddMedia.setOnClickListener(v -> {
            dismiss();
            listener.addPhotos();
        });
        llAddDocument.setOnClickListener(v -> {
            dismiss();
            listener.addDocuments();
        });
        tvCancel.setOnClickListener(v -> {
            dismiss();
            listener.onDismiss();
        });
    }

    public void show(@NonNull FragmentManager manager) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, TAG);
            ft.commitAllowingStateLoss();
            manager.executePendingTransactions();
        } catch (IllegalStateException e) {
            Log.e(TAG, "show: ignored", e);
        }
    }

    public interface OnButtonClickListener {
        void addPhotos();

        void addDocuments();

        void onDismiss();
    }
}
