package com.kindiedays.common.ui.album;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by pleonard on 29/09/2015.
 */
public class SquareThumbnailItemView extends ImageView {


    public SquareThumbnailItemView(Context context) {
        super(context);
    }

    public SquareThumbnailItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareThumbnailItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
    {
        final int width = getDefaultSize(getSuggestedMinimumWidth(),widthMeasureSpec);
        setMeasuredDimension(width, width);
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh)
    {
        super.onSizeChanged(w, w, oldw, oldh);
    }
}
