package com.kindiedays.common.ui.journal;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.kindiedays.common.R;
import com.kindiedays.common.business.AppMenuBusiness;
import com.kindiedays.common.business.JournalBusiness;
import com.kindiedays.common.model.BlurredImageWrapper;
import com.kindiedays.common.model.JournalPictureWrapper;
import com.kindiedays.common.pojo.AppMenuState;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.JournalGalleryPicture;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.ui.picturegallery.MessagePhotoGalleryActivity;
import com.kindiedays.common.ui.picturegallery.PictureGalleryActivity;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by pleonard on 25/11/2015.
 */
public class JournalPostDisplayer implements JournalPostPictureAdapter.OnPictureClickListener {
    private static final String TAG = JournalPostDisplayer.class.getSimpleName();
    private List<Object> journalItems = new ArrayList<>();
    private TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPosts;
    private RecyclerView journalPostList;
    private JournalPostPictureAdapter journalPostPictureAdapter;
    private ProgressBar progress;
    private JournalPostSource journalPostSource;
    private Child child;
    private List<Place> places;
    private static final int VISIBLE_THRESHOLD = 50;
    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private boolean isClickOnPhotoEnabled = true;
    private boolean wasAddedIgnoredItems;


    private RecyclerView.OnScrollListener onRecyclerViewScroll = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            GridLayoutManager glm = (GridLayoutManager) recyclerView.getLayoutManager();

            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = glm.getItemCount();
            int firstVisibleItem = glm.findFirstVisibleItemPosition();

            if (loading && (totalItemCount > previousTotal || wasAddedIgnoredItems)) {
                loading = false;
                previousTotal = totalItemCount;
                wasAddedIgnoredItems = false;
            }

            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
                // End has been reached
                Log.d(TAG, "!onScrolled: load more");
                journalPostSource.requestMoreJournalPosts();

                loading = true;
            }
        }
    };

    public void nullPreviousTotal(){
        previousTotal = 0;
    }

    public interface JournalPostSource {
        void requestMoreJournalPosts();
        Context getContext();
    }

    public JournalPostDisplayer() {
        places = new ArrayList<>();
    }

    public SortedMap<LocalDate, TreeMap<LocalTime, Object>> getJournalPosts() {
        return journalPosts;
    }

    @SuppressWarnings("squid:S1319")
    public void setJournalPosts(TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPosts) {
        this.journalPosts = journalPosts;
        displayJournalPosts();
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
        providePlaces();
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public void init(View view, int journalPostListId, int journalProgressId, JournalPostSource journalPostSource) {
        AppMenuState appMenuState = AppMenuBusiness.getInstance().getMenuStates();
        if (appMenuState != null) {
            isClickOnPhotoEnabled = appMenuState.isCameraEnabled();
        }
        this.journalPostSource = journalPostSource;
        Context context = journalPostSource.getContext();

        journalPostList = (RecyclerView) view.findViewById(journalPostListId);
        progress = (ProgressBar) view.findViewById(journalProgressId);
        journalPostList.setHasFixedSize(true);
        journalPostList.setLayoutManager(new GridLayoutManager(context, 1));
        journalPostList.addOnScrollListener(onRecyclerViewScroll);
        journalPostPictureAdapter = new JournalPostPictureAdapter(context, child, this);
        journalPostList.setAdapter(journalPostPictureAdapter);
    }

    private void onDisabledPhotoClicked(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setNeutralButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
        builder.setTitle(R.string.Locked);
        builder.setMessage(R.string.locked_edu_message);
        Dialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onGalleryPictureClicked(JournalGalleryPicture picture, BlurredImageWrapper wrapper) {
        if (isClickOnPhotoEnabled) {
            Attachment attachment = new Attachment("-1", picture.getHash(), picture.getCaption());
            attachment.setUri(picture.getUrl());
            attachment.setImage(wrapper);
            MessagePhotoGalleryActivity.start(journalPostSource.getContext(), attachment); //context
        } else {
            onDisabledPhotoClicked(journalPostSource.getContext());
        }
    }

    @Override
    public void onJournalPostPictureClicked(int picturePosition, long postId) {
        if (isClickOnPhotoEnabled) {
            Context context = journalPostSource.getContext();
            Intent intent = new Intent(context, PictureGalleryActivity.class);
            intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, picturePosition);
            intent.putExtra(PictureGalleryActivity.JOURNAL_PICTURE_MODE, true);
            intent.putExtra(PictureGalleryActivity.JOURNAL_POST_ID, postId);
            context.startActivity(intent);
        } else {
            onDisabledPhotoClicked(journalPostSource.getContext());
        }
    }



    public void notifyDataSetChanged() {
        if (journalPostPictureAdapter != null) {
            journalPostPictureAdapter.notifyDataSetChanged();
        }
    }

    private void displayJournalPosts() {
        wasAddedIgnoredItems = true;
        if (child != null) {
            journalPostPictureAdapter.setChild(child);
            if (journalPosts != null && journalPostList != null) {
                journalItems.clear();
                for (Map.Entry<LocalDate, TreeMap<LocalTime, Object>> dayPosts : journalPosts.descendingMap().entrySet()) {
                    journalItems.add(dayPosts.getKey());
                    Menu menu = null;
                    for (Map.Entry<LocalTime, Object> posts : dayPosts.getValue().descendingMap().entrySet()) {
                        if (posts.getValue() instanceof JournalPost) {
                            JournalPost journalPost = (JournalPost) posts.getValue();
                            if (journalPost.getJournalPostPictures() != null) {
                                journalItems.add(journalPost);
                                journalItems.add(new JournalPictureWrapper(journalPost));
                            }

                        } else if (posts.getValue() instanceof Menu) {
                            menu = (Menu) posts.getValue();

                        } else {
                            journalItems.add(posts.getValue());
                        }
                    }
                   /* if (menu != null) {
                        journalItems.add(menu);
                    }*/
                }

                journalPostPictureAdapter.setListData(journalItems);
                GridLayoutManager glm = (GridLayoutManager) journalPostList.getLayoutManager();

                if ((journalItems.isEmpty() || glm.findLastVisibleItemPosition() == journalItems.size() - 1) && !JournalBusiness.getInstance().isAllEntriesLoaded()) {
                    //Request more items
                    if (journalPostSource != null) {
                        journalPostSource.requestMoreJournalPosts();
                    }
                }
                journalPostList.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                journalPostPictureAdapter.notifyDataSetChanged();
            }
        }
    }

    private void providePlaces() {
        if (child != null) {
            journalPostPictureAdapter.setPlaces(this.places);
        }
    }
}
