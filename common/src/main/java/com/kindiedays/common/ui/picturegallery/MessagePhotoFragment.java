package com.kindiedays.common.ui.picturegallery;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.R;
import com.kindiedays.common.model.BlurredImageWrapper;
import com.kindiedays.common.pojo.Attachment;

import org.parceler.Parcels;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class MessagePhotoFragment extends Fragment {
    private static final String EXTRA_ATTACHMENT = "extra_attachment";
    private static final String TAG = MessagePhotoFragment.class.getSimpleName();

    private ImageViewTouch ivTouch;
    private ProgressBar progressBar;

    public static MessagePhotoFragment newInstance(Attachment attachment) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_ATTACHMENT, Parcels.wrap(attachment));
        MessagePhotoFragment fragment = new MessagePhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_message_photo, container, false);
        ivTouch = (ImageViewTouch) rootView.findViewById(R.id.iv_photo);
        progressBar = (ProgressBar) rootView.findViewById(R.id.pb_loader);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Attachment attachment = Parcels.unwrap(getArguments().getParcelable(EXTRA_ATTACHMENT));
        BlurredImageWrapper wrapper = attachment.getImage();
        if (wrapper != null) {
            setGlideBitmap(wrapper.getUrl());
        } else {
            setGlideDrawable(attachment.getUri());
        }
    }

    private void setGlideBitmap(String url) {
        Glide.with(getActivity())
                .load(url)
                .into(new GlideDrawableImageViewTarget(ivTouch) {
            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                super.onResourceReady(drawable, anim);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, "onLoadFailed: ", e);
            }
        });
    }

    private void setGlideDrawable(String url) {
        Glide.with(getActivity())
                .load(url)
                .into(new GlideDrawableImageViewTarget(ivTouch) {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                super.onResourceReady(resource, animation);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
