package com.kindiedays.common.ui.messages;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.business.ConversationBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.conversations.GetMediaFile;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Message;
import com.kindiedays.common.pushnotifications.BroadcastReceiver;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.ui.picturegallery.MessagePhotoGalleryActivity;
import com.kindiedays.common.utils.FileUtils;
import com.kindiedays.common.utils.FormatUtils;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

import static com.kindiedays.common.ui.messages.MessageAdapter.TYPE_VIDEO;
import static com.kindiedays.common.utils.constants.CommonConstants.EXTRA_GALLERY;
import static com.kindiedays.common.utils.constants.CommonConstants.MAX_UPLOAD_SIZE_KB;
import static com.kindiedays.common.utils.constants.CommonConstants.MB_SIZE;
import static com.kindiedays.common.utils.constants.CommonConstants.SAMSUNG_ACTION_GET_CONTENT;
import static com.kindiedays.common.utils.constants.CommonConstants.SAMSUNG_CONTENT_TYPE;
import static com.kindiedays.common.utils.constants.CommonConstants.SAMSUNG_MODEL;
import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 20/07/2015.
 */
public abstract class ConversationActivity extends KindieDaysActivity
        implements AttachmentDialog.OnButtonClickListener, MessageAdapter.OnMediaClickListener {
    private static final String TAG = ConversationActivity.class.getSimpleName();
    private static final int PICK_IMAGE_MULTIPLE = 1;
    private static final int PICK_PDF_SINGLE = 2;
    private static final String PDF_MIME_TYPE = "application/pdf";
    private static final String OUTGOING_MESSAGE = "MessageOutgoing.wav";
    private static final int REQUEST_FROM_DIALOG = 10;
    private static final int REQUEST_FROM_MEDIA_FILE = 11;

    protected ListView messageList;
    protected Button sendNewMessage;
    protected EditText newMessage;
    protected Conversation conversation;
    protected ProgressBar progressBar;
    protected MessageAdapter messageAdapter;
    protected ImageView ivAddAttachment;
    private LinearLayout llBottomBar;
    private RelativeLayout rlContent;

    MediaPlayer player;
    private Attachment attachment;
    Map<String, AssetFileDescriptor> sounds = new HashMap<>();

    public static final String CONVERSATION_ID = "conversationId";

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshMessages();
            startSound(getApplicationContext(), "MessageIncoming.wav");
        }
    };

    private android.content.BroadcastReceiver brDownloadComplete = new android.content.BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                openDownloadedAttachment(context, downloadId);
            }
        }
    };

    View.OnClickListener onSendNewMessageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SendNewMessageWithFiles task = new SendNewMessageWithFiles(newMessage.getText().toString(),
                    Collections.emptyList(), conversation.getId(), false);
            getPendingTasks().add(task);
            task.execute();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        messageList = (ListView) findViewById(R.id.messageList);
        messageList.setOnItemClickListener((parent, view, position, id) -> hideKeyboard());
        sendNewMessage = (Button) findViewById(R.id.sendMessage);
        sendNewMessage.setEnabled(false);
        ivAddAttachment = (ImageView) findViewById(R.id.iv_add_attachment);
        llBottomBar = (LinearLayout) findViewById(R.id.bottom_write_bar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        sendNewMessage.setOnClickListener(onSendNewMessageClickListener);
        rlContent = (RelativeLayout) findViewById(R.id.rl_content);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(""); //Clear the title before the other conversation party is retrieved

        newMessage = (EditText) findViewById(R.id.newMessage);
        newMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeSendButtonState();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                changeSendButtonState();
            }

            @Override
            public void afterTextChanged(Editable s) {
                changeSendButtonState();
            }
        });
    }

    private void changeSendButtonState() {
        if (newMessage.getText().length() == 0) {
            sendNewMessage.setTextColor(ContextCompat.getColor(ConversationActivity.this, R.color.grey));
            sendNewMessage.setEnabled(false);
        } else {
            sendNewMessage.setTextColor(ContextCompat.getColor(ConversationActivity.this, R.color.kindieblue));
            sendNewMessage.setEnabled(true);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(newMessage.getWindowToken(), 0);
    }

    private void refreshMessages() {
        LoadMessagesAsyncTask loadMessagesAsyncTask = new LoadMessagesAsyncTask(getIntent().getLongExtra(CONVERSATION_ID, 0));
        getPendingTasks().add(loadMessagesAsyncTask);
        loadMessagesAsyncTask.execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
        messageList.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(mMessageReceiver, new IntentFilter("message_notification"));
        registerReceiver(brDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        refreshMessages();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            llBottomBar.setVisibility(View.VISIBLE);
            newMessage.requestFocus();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }

        for (AssetFileDescriptor sound : sounds.values()) {
            try {
                sound.close();
            } catch (IOException e) {
                Log.d(getClass().getSimpleName(), "Failed to close sound file descriptor", e);
            }
        }

        sounds.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.unregisterReceiver(mMessageReceiver);
        unregisterReceiver(brDownloadComplete);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        }

        return false;
    }

    @Override
    public void addPhotos() {
        Intent intent = new Intent(ConversationActivity.this, CustomPhotoGalleryActivity.class);
        startActivityForResult(intent, PICK_IMAGE_MULTIPLE);
    }

    @Override
    public void addDocuments() {
        Intent intent;
        if (Build.MANUFACTURER.equalsIgnoreCase(SAMSUNG_MODEL)) {
            intent = new Intent(SAMSUNG_ACTION_GET_CONTENT);
            intent.putExtra(SAMSUNG_CONTENT_TYPE, PDF_MIME_TYPE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType(PDF_MIME_TYPE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        }
        startActivityForResult(intent, PICK_PDF_SINGLE);
    }

    @Override
    public void onDismiss() {
        llBottomBar.setVisibility(View.VISIBLE);
        newMessage.requestFocus();
    }

    public void openAttachmentDialog(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_FROM_DIALOG);
        } else {
            showAttachmentDialog();
        }
    }

    private void showAttachmentDialog() {
        llBottomBar.setVisibility(View.INVISIBLE);
        AttachmentDialog.newInstance().show(getSupportFragmentManager());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_MULTIPLE) {
                List<File> attachments = new ArrayList<>();
                String extraGallery = data.getStringExtra(EXTRA_GALLERY);

                if (extraGallery != null && !extraGallery.isEmpty()) {
                    String[] imagesPath = extraGallery.split("\\|");
                    for (String path : imagesPath) {
                        attachments.add(new File(path));
                    }

                    SendNewMessageWithFiles task = new SendNewMessageWithFiles(newMessage.getText().toString(),
                            attachments, conversation.getId(), false);
                    getPendingTasks().add(task);
                    task.execute();
                }
            } else if (requestCode == PICK_PDF_SINGLE) {
                File file;
                if (FileUtils.isGogleDriveDocument(data.getData())) {
                    DownloadPdfAsyncTask task = new DownloadPdfAsyncTask();
                    getPendingTasks().add(task);
                    task.execute(data);
                } else {
                    String path = FileUtils.getFilePath(ConversationActivity.this, data.getData());
                    file = new File(path);
                    if (isValidSize(file)) {
                        SendNewMessageWithFiles task = new SendNewMessageWithFiles(newMessage.getText().toString(),
                                file, conversation.getId(), false);
                        getPendingTasks().add(task);
                        task.execute();
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == REQUEST_FROM_MEDIA_FILE) {
                downloadMediaFile(attachment);
            } else if (requestCode == REQUEST_FROM_DIALOG) {
                showAttachmentDialog();
            }
        }
    }

    @Override
    public void onMediaClick(Attachment attachment) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            this.attachment = attachment;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_FROM_MEDIA_FILE);
        } else {
            downloadMediaFile(attachment);
        }
    }

    private void downloadMediaFile(Attachment attachment) {
        if (FormatUtils.getFormatType(attachment.getFormat()) == TYPE_VIDEO) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                ExoPlayerActivity.start(this, attachment);
            } else {
                VideoMessageActivity.start(this, attachment);
            }
        } else {
            showSnackbar(R.string.unsupported_format);
            DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            GetMediaFile.downloadFile(this, downloadManager, attachment.getFilename(), attachment.getUri());
        }
    }

    @Override
    public void onPhotoClick(Attachment attachment) {
        if (attachment == null) {
            Toast.makeText(this, "Invalid attachment", Toast.LENGTH_SHORT).show();
            return;
        }
        MessagePhotoGalleryActivity.start(this, attachment);
    }

    @SuppressWarnings("squid:S3398")
    private void openDownloadedAttachment(final Context context, final long downloadedId) {
        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadedId);
        Cursor cursor = downloadManager.query(query);
        if (cursor.moveToFirst()) {
            int downloadStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            String downloadLocalUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
            String downloadMimeType = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
            if ((downloadStatus == DownloadManager.STATUS_SUCCESSFUL) && downloadLocalUri != null) {
                openDownloadedAttachment(context, Uri.parse(downloadLocalUri), downloadMimeType);
            }
        }
        cursor.close();
    }

    protected abstract void openDownloadedAttachment(final Context context, Uri attachmentUri,
                                                     final String attachmentMimeType);


    private class SendNewMessageWithFiles extends ConversationBusiness.SendNewMessageWithMedia {
        private ProgressDialog progressDialog;

        SendNewMessageWithFiles(String message, List<File> attachments, long conversationId, boolean isDelete) {
            super(message, attachments, conversationId, isDelete);
            startSound(getApplicationContext(), OUTGOING_MESSAGE);
        }

        SendNewMessageWithFiles(String message, File attachment, long conversationId, boolean isDelete) {
            this(message, Collections.singletonList(attachment), conversationId, isDelete);
        }

        @Override
        protected void onPreExecute() {
            if (attachments.isEmpty()) {
                return;
            }

            initProgressDialog();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int currentProgress = values[0];
            progressDialog.setMessage(getString(R.string.dialog_progress_file_uploading, currentProgress, attachments.size()));
            progressDialog.setProgress(currentProgress);
        }

        @Override
        protected void onPostExecute(Message message) {
            if (!checkNetworkException(message, exception)) {
                super.onPostExecute(message);
                conversation.getMessages().add(message);
                messageAdapter.setMessages(conversation.getMessages());
                messageList.clearFocus();
                messageList.post(() -> {
                    messageList.requestFocusFromTouch();
                    messageList.setSelection(messageAdapter.getCount() - 1);
                    newMessage.requestFocus();
                });
                newMessage.setText(STRING_EMPTY);
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            getPendingTasks().remove(this);
        }

        private void initProgressDialog() {
            progressDialog = new ProgressDialog(ConversationActivity.this);
            progressDialog.setTitle(R.string.Upload_In_Progress);
            progressDialog.setMax(attachments.size());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(1);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(getString(R.string.dialog_progress_file_uploading, 1, attachments.size()));
            progressDialog.show();
        }
    }

    private void showSnackbar(@StringRes int stringRes) {
        TSnackbar snackbar = TSnackbar.make(rlContent, getString(stringRes),
                TSnackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private boolean isValidSize(File file) {
        if (file.length() / MB_SIZE > MAX_UPLOAD_SIZE_KB) {
            showSnackbar(R.string.gallery_file_too_large);
            return false;
        }
        return true;
    }

    protected abstract void setPageTitle(Set<ConversationParty> parties);


    private class DownloadPdfAsyncTask extends AsyncTask<Intent, Void, File> {

        @Override
        protected File doInBackground(Intent... params) {
            return FileUtils.getFileFromGoogleDrive(ConversationActivity.this, params[0]);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (file != null && isValidSize(file)) {
                SendNewMessageWithFiles task = new SendNewMessageWithFiles(newMessage.getText().toString(),
                        file, conversation.getId(), true);
                getPendingTasks().add(task);
                task.execute();
            }
        }
    }


    private class LoadMessagesAsyncTask extends ConversationBusiness.LoadConversationTask {

        LoadMessagesAsyncTask(long conversationId) {
            super(conversationId);
        }

        @Override
        protected Conversation doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.Conversations).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(Conversation conversation) {
            if (!checkNetworkException(conversation, exception)) {
                super.onPostExecute(conversation);
                setPageTitle(conversation.getParties());
                messageAdapter.setContent(conversation.getMessages(), conversation.getParties());
                messageList.clearFocus();
                messageList.post(() -> {
                    messageList.requestFocusFromTouch();
                    messageList.setSelection(messageAdapter.getCount() - 1);
                    newMessage.requestFocus();
                });
                ConversationActivity.this.conversation = conversation;
                progressBar.setVisibility(View.GONE);
                messageList.setVisibility(View.VISIBLE);
            } else {
                finish();
            }
            getPendingTasks().remove(this);
        }
    }

    @Override
    protected boolean checkNetworkException(Object value, KindieDaysNetworkException exception) {
        boolean exceptionDetected = (exception != null || value == null);
        if (exceptionDetected) {
            if (exception != null) {
                if (exception.isLocalizedMessage()) {
                    if (exception.getPayload() != null) {
                        Toast.makeText(getApplicationContext(), exception.getMessagePayload(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                        stopPendingTasks();
                        Intent i = new Intent(this, ((KindieDaysApp) getApplication()).getLoginActivityClass());
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), "3 Internal error", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "4 Internal error", Toast.LENGTH_SHORT).show();
            }
        }
        return exceptionDetected;
    }

    private void startSound(Context context, String filename) {

        try {
            AssetFileDescriptor sound = sounds.get(filename);

            if (sound == null) {
                sound = context.getAssets().openFd("sounds/" + filename);
                sounds.put(filename, sound);
            }

            if (player == null) {
                player = new MediaPlayer();
                player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            }

            player.reset();
            player.setDataSource(sound.getFileDescriptor(), sound.getStartOffset(), sound.getLength());
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.e(TAG, "startSound: ", e);
        }
    }
}
