package com.kindiedays.common.ui.album;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.kindiedays.common.R;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.common.ui.InfiniteListViewScrollListener;
import com.kindiedays.common.ui.MainActivityFragment;

import java.util.List;

/**
 * Created by pleonard on 16/09/2015.
 */
public abstract class AlbumFragment extends MainActivityFragment {

    protected GridView grid;
    protected ThumbnailAdapter thumbnailAdapter;
    protected TextViewCustom title;
    protected ProgressBar progressBar;
    protected boolean isUpdating;
    protected List<GalleryPicture> galleryPictureList;

    protected AdapterView.OnItemClickListener onPictureClickListener;
    protected AdapterView.OnItemLongClickListener onPictureLongClickListener;

    InfiniteListViewScrollListener.EndListReachedListener endGridReachedListener = new InfiniteListViewScrollListener.EndListReachedListener() {
        @Override
        public void topReached() {
            //Do nothing
        }

        @Override
        public void bottomReached() {
            bottomViewReached();
        }
    };

    private void bottomViewReached() {

        if (thumbnailAdapter.isReadyToLoadMore()
                && !isUpdating && PicturesBusiness.getInstance().getCurrentPictureList() != null
                && PicturesBusiness.getInstance().getCurrentPictureList().getLinkNext() != null) {
            //Load more pictures
            isUpdating = true;
            GetMorePicturesAsyncTask task = new GetMorePicturesAsyncTask(PicturesBusiness.getInstance().getCurrentPictureList().getLinkNext());
            pendingTasks.add(task);
            task.execute();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_album, null);
        progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
        grid = (GridView) convertView.findViewById(R.id.photoGrid);
        thumbnailAdapter = new ThumbnailAdapter(getActivity());
        grid.setAdapter(thumbnailAdapter);
        grid.setOnItemClickListener(onPictureClickListener);
        grid.setOnItemLongClickListener(onPictureLongClickListener);
        grid.setOnScrollListener(new InfiniteListViewScrollListener(endGridReachedListener));

        if (galleryPictureList != null) {
            thumbnailAdapter.setPictures(galleryPictureList);
            progressBar.setVisibility(View.GONE);
            if (galleryPictureList != null && galleryPictureList.size() == 15) {
                GetMorePicturesAsyncTask task = new GetMorePicturesAsyncTask(PicturesBusiness.getInstance().getCurrentPictureList().getLinkNext());
                pendingTasks.add(task);
                task.execute();
            }
        }
        return convertView;
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.Album));
    }

    @Override
    public abstract void refreshData();

    protected void morePicturesLoaded(PictureList pictureList) {
        //The picture list in the adapter is the same than the one updated in the business class -> we only need to notify that the dataset has changed
        thumbnailAdapter.notifyDataSetChanged();
    }

    class GetMorePicturesAsyncTask extends PicturesBusiness.GetMorePicturesAsyncTask {

        public GetMorePicturesAsyncTask(String url) {
            super(url);
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(PictureList pictureList) {
            isUpdating = false;
            if (!checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                morePicturesLoaded(pictureList);
            }
            pendingTasks.remove(this);

        }
    }

}
