package com.kindiedays.common.ui;

/**
 * @author Eugeniy Shein on 12/12/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 12/12/2017
 */
public interface Refreshable {
    void  refreshData();
}
