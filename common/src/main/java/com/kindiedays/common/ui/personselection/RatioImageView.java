package com.kindiedays.common.ui.personselection;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.utils.PictureUtils;

import java.io.InputStream;

/**
 * Created by pleonard on 15/04/2015.
 */
@SuppressWarnings({"squid:S1068", "squid:S1450"})
public class RatioImageView extends ImageView {

    private int logicalBitmapHeight; //Original height of picture
    private int logicalBitmapWidth; //Original width of picture

    private float originalDisplayedHeightRatio;
    private float originalDisplayedWidthRatio;

    private float intrasicDisplayedHeightRatio;
    private float intrasicDisplayedWidthRatio;

    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != 0 && h != 0) {
            initRatios();
        }
    }

    private void initRatios() {
        Drawable drawable = this.getDrawable();

        //source height and width of the bitmap
        int intrinsicHeight = drawable.getIntrinsicHeight();
        int intrinsicWidth = drawable.getIntrinsicWidth();

        //height and width of the visible (scaled) image
        int scaledHeight = this.getHeight();
        int scaledWidth = this.getWidth();

        //Find the ratio of the original image to the scaled image
        //Should normally be equal unless a disproportionate scaling
        //(e.g. fitXY) is used.

        originalDisplayedHeightRatio = (float) logicalBitmapHeight / (float) scaledHeight;
        originalDisplayedWidthRatio = (float) logicalBitmapWidth / (float) scaledWidth;

        intrasicDisplayedHeightRatio = (float) intrinsicHeight / (float) scaledHeight;
        intrasicDisplayedWidthRatio = (float) intrinsicWidth / (float) scaledWidth;
    }

    public void setImageBitmap(final Activity activity, final Uri uri, int originalBitmapWidth, int originalBitmapHeight, final GlideDrawableImageViewTarget callback) throws Exception {
        if (originalBitmapWidth != 0 && originalBitmapHeight != 0) {
            //Picture comes from the server
            this.logicalBitmapWidth = originalBitmapWidth;
            this.logicalBitmapHeight = originalBitmapHeight;
        } else {
            //Local picture
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            if (!uri.toString().startsWith("http")) {
                //Content uri, the file is available locally
                ParcelFileDescriptor fd = activity.getContentResolver().openFileDescriptor(uri, "r"); // u is your Uri
                BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(), null, options);
                InputStream is = null;
                int exifOrientation;
                try {
                    is = activity.getContentResolver().openInputStream(uri);
                    exifOrientation = PictureUtils.getExifRotation(is);
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
                if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90 || exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    this.logicalBitmapWidth = options.outHeight;
                    this.logicalBitmapHeight = options.outWidth;
                } else {
                    this.logicalBitmapWidth = options.outWidth;
                    this.logicalBitmapHeight = options.outHeight;
                }
            } else {
                //Online picture, should be normalized already
                this.logicalBitmapWidth = options.outWidth;
                this.logicalBitmapHeight = options.outHeight;
            }
        }

        Glide.with(getContext()).load(uri).into(new GlideDrawableImageViewTarget(this) {
            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                super.onResourceReady(drawable, anim);
                initRatios();
                callback.onResourceReady(drawable, anim);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                callback.onLoadFailed(e, errorDrawable);
            }
        });

    }

    public int getOriginalXPosition(int drawnXPosition) {
        return (int) (drawnXPosition * originalDisplayedWidthRatio);
    }

    public int getOriginalYPosition(int drawnYPosition) {
        return (int) (drawnYPosition * originalDisplayedHeightRatio);
    }

}
