package com.kindiedays.common.ui.settings;

import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.session.DeleteSession;
import com.kindiedays.common.network.session.PatchSession;
import com.kindiedays.common.pojo.Kindergarten;
import com.kindiedays.common.ui.MainActivity;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.utils.LocaleHelper;

import java.net.UnknownHostException;

/**
 * Created by pleonard on 16/09/2015.
 */
public abstract class SettingsFragment extends MainActivityFragment {
    private static final String TAG = SettingsFragment.class.getSimpleName();

    protected ImageView ivKindergartenPicture;
    protected TextView tvName;
    protected TextView tvAddress;
    protected TextView tvPhone;
    protected TextView tvManagerEmail;
    protected TextView tvVersionNumber;
    protected Spinner spLanguages;
    protected Switch swNotifications;
    protected Button btnLogout;
    protected ProgressBar logoutProgress;
    protected TextViewCustom title;

    private CompoundButton.OnCheckedChangeListener onNotificationChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(KindieDaysApp.SETTINGS, getActivity().MODE_PRIVATE);
            boolean isCurrentlyEnabled = sharedPreferences.getBoolean(MainActivity.NOTIFICATIONS, true);
            Log.d("myLogs", "isCurrentlyEnabled = " + isCurrentlyEnabled);
            if (isCurrentlyEnabled != isChecked) {
                if (isChecked) {
                    //enable push notifications
                    KindieDaysApp.getApp().subscribePushNotifications(getActivity());
                } else {
                    //disable push notifications
                    KindieDaysApp.getApp().unsubscribePushNotifications(getActivity());
                }
            }
        }
    };

    public View onCreateView(View v) {
        initSpinner();

        initNotifications();

        // Executed just from carer app, will patch the kindergarden as null
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PatchSessionTask task = new PatchSessionTask();
                task.execute();
                pendingTasks.add(task);
            }

            class PatchSessionTask extends NetworkAsyncTask<Boolean> {

                @Override
                protected void onPostExecute(Boolean success) {
                    if (success != null && success) {
                        LogoutAsyncTask task = new LogoutAsyncTask();
                        task.execute();
                    } else {
                        if (exception != null && exception.isLocalizedMessage()) {
                            Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "5 Internal application error", Toast.LENGTH_LONG).show();
                        }
                    }
                    pendingTasks.remove(this);
                }

                @Override
                protected Boolean doNetworkTask() throws Exception {
                    PatchSession.patchKindergarden();
                    return true;
                }
            }
        });

        try {
            PackageManager pm = getActivity().getPackageManager();
            PackageInfo pi;
            pi = pm.getPackageInfo(getActivity().getPackageName(), 0);
            tvVersionNumber.setText("v.".concat(pi.versionName));
        } catch (Exception e) {
            Log.e(TAG, "onCreateView: ", e);
        }

        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.Settings));
    }

    private void initSpinner() {
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_text, getActivity().getResources().getStringArray(R.array.languages));
        spLanguages.setAdapter(languageAdapter);
        String currentLanguageCode = LocaleHelper.getPersistedData(KindieDaysApp.getApp(), "");
        String[] languageCodes = getResources().getStringArray(R.array.languageCodes);
        for (int i = 0; i < languageCodes.length; i++) {
            if (languageCodes[i].equals(currentLanguageCode)) {
                spLanguages.setSelection(i);
                break;
            }
        }

        spLanguages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String languageCode = getActivity().getResources().getStringArray(R.array.languageCodes)[position];
                if (!LocaleHelper.getLanguage(KindieDaysApp.getApp()).equals(languageCode) && !(TextUtils.isEmpty(languageCode) && LocaleHelper.getPersistedData(KindieDaysApp.getApp(), null) == null)) {
                    switchToLanguage(languageCode);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //do nothing
            }
        });
    }

    private void initNotifications() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(KindieDaysApp.SETTINGS, getActivity().MODE_PRIVATE);
        boolean notificationsEnabled = sharedPreferences.getBoolean(MainActivity.NOTIFICATIONS, true);
        swNotifications.setChecked(notificationsEnabled);
        swNotifications.setOnCheckedChangeListener(onNotificationChecked);
    }


    public void switchToLanguage(String language) {
        //Switch UI language
        LocaleHelper.setLocale(KindieDaysApp.getApp(), language);
    }

    protected void updateKindergartenInformation(Kindergarten kindergarten) {
        Glide.with(getActivity()).load(kindergarten.getProfilePictureUrl()).into(ivKindergartenPicture);
        tvName.setText(kindergarten.getName());
        tvAddress.setText(kindergarten.getAddress());
        tvPhone.setText(kindergarten.getTelephoneNumber());
        tvManagerEmail.setText(kindergarten.getManagerEmail());

    }

    private class LogoutAsyncTask extends NetworkAsyncTask<Void> {

        @Override
        protected void onPostExecute(Void o) {
            if (getActivity() != null && exception == null) {
                clearCurrentUser();
                restartApp(null);
            } else if (getActivity() != null && exception != null) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
            pendingTasks.remove(this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            logoutProgress.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.GONE);
        }

        protected Void doNetworkTask() throws Exception {
            try {
                KindieDaysApp.getApp().unsubscribePushNotifications(getActivity());
                DeleteSession.logout();
                return null;
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.OperationFailed);
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }
    }

    protected abstract void clearCurrentUser();

    protected abstract void restartApp(@Nullable String key);

}
