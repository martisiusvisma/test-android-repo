package com.kindiedays.common.ui.personselection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.utils.GlideUtils;

/**
 * Created by pleonard on 20/07/2015.
 */
public abstract class PersonSelectionButton extends LinearLayout {

    protected Person person;

    protected ImageView ivPersonPicture;
    protected TextView tvName;
    protected TextView tvPicturesNotAllowed;
    protected ImageView ivSelectionMark;


    public PersonSelectionButton(Context context) {
        super(context);
        initViews();
    }

    public PersonSelectionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public PersonSelectionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.button_person_selection, this);
        ivPersonPicture = (ImageView) v.findViewById(R.id.kidPicture);
        tvName = (TextView) v.findViewById(R.id.kidName);
        tvPicturesNotAllowed = (TextView) v.findViewById(R.id.captionPicturesNotAllowed);
        ivSelectionMark = (ImageView) v.findViewById(R.id.selectionmarker);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
        refreshPerson();
        tvName.setText(person.getListName());
        Glide.with(getContext()).load(person.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(ivPersonPicture);
    }

    protected abstract void refreshPerson();

}
