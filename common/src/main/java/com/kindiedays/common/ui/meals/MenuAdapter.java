package com.kindiedays.common.ui.meals;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Menu;

import java.util.List;
import java.util.Locale;

/**
 * Created by pleonard on 11/06/2015.
 */
public class MenuAdapter extends BaseAdapter {

    private List<Menu> menus;
    private Activity activity;

    public MenuAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setMeals(List<Menu> menus) {
        this.menus = menus;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (menus == null) {
            return 0;
        }
        return menus.size();
    }

    @Override
    public Menu getItem(int position) {
        return menus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_meal, null);
            holder = new ViewHolder();
            holder.tvDate = (TextView) convertView.findViewById(R.id.dateMeal);
            holder.tvMeal = (TextView) convertView.findViewById(R.id.meal);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Menu menu = getItem(position);
        String date;
        if ("fi".equals(Locale.getDefault().getLanguage())) {
            date = activity.getResources().getStringArray(R.array.days)[menu.getDate().getDayOfWeek() - 1] + menu.getDate().toString(" dd/MM");
        } else {
            date = menu.getDate().toString("EEEEE dd/MM");
        }
        StringBuilder sbuilder = new StringBuilder(date);
        sbuilder.setCharAt(0, Character.toUpperCase(date.charAt(0)));
        holder.tvDate.setText(date);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < menu.getMeals().size(); i++) {
            sb.append(menu.getMeals().get(i).getText());
            if (i < menu.getMeals().size() - 1) {
                sb.append("\n");
            }
        }
        holder.tvMeal.setText(sb.toString());
        return convertView;
    }

    static class ViewHolder {
        TextView tvDate;
        TextView tvMeal;
    }
}
