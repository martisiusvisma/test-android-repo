package com.kindiedays.common.ui.messages;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.R;
import com.kindiedays.common.utils.GlideUtils;

import java.util.List;

public class ChildrenAvatarsAdapter extends RecyclerView.Adapter<ChildrenAvatarsAdapter.ImagesViewHolder> {
    private List<String> photoUrls;

    public ChildrenAvatarsAdapter(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    @Override
    public ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversation_children,
                parent, false);
        return new ImagesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImagesViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext())
                .load(photoUrls.get(position))
                .bitmapTransform(GlideUtils.roundTransformation)
                .into(holder.ivPhoto);
    }

    @Override
    public int getItemCount() {
        return photoUrls.size();
    }


    class ImagesViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPhoto;

        ImagesViewHolder(View itemView) {
            super(itemView);
            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_photo);
        }
    }
}
