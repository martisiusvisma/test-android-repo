package com.kindiedays.common.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;

import java.util.HashSet;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

/**
 * Created by pleonard on 09/06/2015.
 */
public abstract class MainActivityFragment extends Fragment implements Refreshable {

    protected Set<AsyncTask> pendingTasks = new HashSet<>();

    protected void stopPendingTasks() {
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        pendingTasks.clear();
    }

    protected void updateData() {
        if (Webservice.isAuthorizationTokenInitialized()) {
            refreshData();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateData();
    }

    @Override
    public void onDetach() {
        stopPendingTasks();
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_actionbar);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    /**
     *
     * @param value
     * @param exception
     * @return true if an exception has been raised, false otherwise
     */
    @SuppressWarnings("deprecation")
    protected boolean checkNetworkException(Object value, KindieDaysNetworkException exception) {
        boolean exceptionDetected = (exception != null && value == null);
        if (exceptionDetected && getActivity() != null) {
            if (exception.isLocalizedMessage()) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    stopPendingTasks();
                    Intent i = new Intent(getActivity(), ((KindieDaysApp) getActivity().getApplication()).getLoginActivityClass());
                    startActivity(i);
                } else {
                    Toast.makeText(getActivity(), "2 Internal error", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return exceptionDetected;
    }

    public abstract void refreshData();

}
