package com.kindiedays.common.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.business.AppMenuBusiness;
import com.kindiedays.common.business.DailyReportBusiness;
import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.AppMenuState;
import com.kindiedays.common.pojo.DRSettings;
import com.kindiedays.common.pojo.Kindergarten;
import com.kindiedays.common.utils.AppStartCheck;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 27/05/2015.
 */
public abstract class MainActivity extends KindieDaysActivity implements
        PersonSelectionDialogFragment.PersonSelectionDialogListener {
    public static final String TAG = MainActivity.class.getName();
    public static final String NOTIFICATIONS = "notifications";

    public enum MenuItemsStates {
        MESSAGES_STATE(true), EVENTS_STATE(true), MEALS_STATE(true), CALENDAR_STATE(true),
        DAILY_REPORT_STATE(true),
        CAMERA_STATE(true), JOURNAL_STATE(true);

        boolean state;

        MenuItemsStates(boolean state) {
            this.state = state;
        }

        public void setState(boolean state) {
            this.state = state;
        }

        public boolean getState() {
            return state;
        }
    }

    protected Set<AsyncTask> pendingTasks = new HashSet<>();

    protected DrawerLayout mDrawerLayout;
    protected ListView menu;
    protected ListView bottomMenu;
    protected List<String> fragmentTags;

    private static final int REQUEST_LOGIN = 1;

    boolean justCreated = true;

    private int itemsTopMenus;

    public static final String OPEN_FROM_NOTIFICATION = "fromNotification";

    protected RelativeLayout navigationMenu;

    protected ActionBarDrawerToggle mDrawerToggle;

    protected int currentFragment;

    private int numberOfAppMenuTasks = 2;

    protected View.OnClickListener onDisabledItemClickListener = view -> {
        lockedMenuItemDialogShow(this);
    };

    public static void lockedMenuItemDialogShow(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setNeutralButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
        builder.setTitle(R.string.Locked);
        builder.setMessage(R.string.locked_edu_message);
        Dialog dialog = builder.create();
        dialog.show();
    }

    protected AdapterView.OnItemClickListener onBottomMenuClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!((MenuAdapter) parent.getAdapter()).isMenuItemEnabled(position)) {
                onDisabledItemClickListener.onClick(view);
            } else {
                int pageIndex = itemsTopMenus + position;
                openPage(pageIndex, true);
                mDrawerLayout.closeDrawer(navigationMenu);
            }
        }
    };

    protected AdapterView.OnItemClickListener onMenuClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!((MenuAdapter) parent.getAdapter()).isMenuItemEnabled(position)) {
                onDisabledItemClickListener.onClick(view);
            } else {
                openPage(position, true);
                if (position < itemsTopMenus) {
                    //Bottom elements are not loaded into fragments so current fragment is unchanged
                    currentFragment = position;
                }
                mDrawerLayout.closeDrawer(navigationMenu);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        justCreated = true;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);//Getting rid of drop shadow on android 5

        switch (AppStartCheck.checkAppStart(this, KindieDaysApp.getApp().getCurrentSharedPreferences(this))) {
            case NORMAL:
                Log.d(MainActivity.class.getSimpleName(), "NORMAL");
                break;
            case FIRST_TIME_VERSION:
                Log.d(MainActivity.class.getSimpleName(), "FIRST_TIME_VERSION");
                break;
            case FIRST_TIME:
                Log.d(MainActivity.class.getSimpleName(), "FIRST_TIME");
                break;
            default:
                break;
        }
    }

    @Override
    protected void stopPendingTasks() {
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        pendingTasks.clear();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Webservice.isAuthorizationTokenInitialized()) {
            refreshData();
        }

        if (justCreated) {
            justCreated = false;
            openPage(currentFragment, true);
        }
    }

    @Override
    public void onStop() {
        stopPendingTasks();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectCurrentPositionInMenu();
        if (!Webservice.isAuthorizationTokenInitialized()) {
            onNotAuthorized();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN) {
            if (resultCode != RESULT_OK) {
                finish();
            } else {
                KindieDaysApp.getApp().subscribePushNotifications(this);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @SuppressWarnings("deprecation")
    protected void onCreateInitMenu() {
        itemsTopMenus = initMenu();
        initBottomMenu();

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                com.kindiedays.common.R.string.drawer_open,  /* "open drawer" description */
                com.kindiedays.common.R.string.drawer_close  /* "close drawer" description */
        ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                hideKeyboard(true, MainActivity.this);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setOnTouchListener((v, event) -> false);
    }

    @Override
    public void onNoOneSelected() {
        finish();
    }

    @Override
    public void onNotAuthorized() {
        Intent intent = new Intent(this, ((KindieDaysApp) getApplication()).getLoginActivityClass());
        startActivityForResult(intent, REQUEST_LOGIN);
    }

    protected abstract int initMenu();

    protected abstract int initBottomMenu();

    protected abstract void openPage(int position, boolean addToBackStack);

    public void setJustCreated(boolean justCreated) {
        this.justCreated = justCreated;
    }

    private void selectCurrentPositionInMenu() {
        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        if (tag != null) {
            Integer position = fragmentTags.indexOf(tag);
            menu.setItemChecked(position, true);
            bottomMenu.clearChoices();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
            selectCurrentPositionInMenu();
        } else {
            finish();
        }
    }

    public static void hideKeyboard(boolean val, Activity activity) {
        View view;
        view = activity.getWindow().getCurrentFocus();
        if (val) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null && view != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public abstract void refreshData();

    public abstract void refreshMenuStates();

    public abstract @StringRes int getLockedMenuMessageRes();

    private void setMenuStates(AppMenuState appMenuState) {
        MenuItemsStates.MESSAGES_STATE.setState(appMenuState.isMessagesEnabled());
        MenuItemsStates.EVENTS_STATE.setState(appMenuState.isEventsEnabled());
        MenuItemsStates.MEALS_STATE.setState(appMenuState.isMealsEnabled());
        MenuItemsStates.CALENDAR_STATE.setState(appMenuState.isCalendarEnabled());
        MenuItemsStates.CAMERA_STATE.setState(appMenuState.isCameraEnabled());
        MenuItemsStates.JOURNAL_STATE.setState(appMenuState.isJournalEnabled());
        //MenuItemsStates.DAILY_REPORT_STATE.setState(appMenuState.);
        Log.d(this.getClass().getName(), "calendar state = " + appMenuState.isCalendarEnabled());
    }

    public class KindergartenTask extends KindergartenBusiness.LoadKindergartenDataTask {

        @Override
        protected void onPostExecute(List<Kindergarten> kindergartens) {
            super.onPostExecute(kindergartens);
            if (!checkNetworkException(kindergartens, exception) && kindergartens != null) {
                Log.d(this.getClass().getName(), "create AppMenu Task");
                long kindergartenId = kindergartens.get(0).getId();
                AppMenuTask appMenuTask = new AppMenuTask(kindergartenId);
                pendingTasks.add(appMenuTask);
                appMenuTask.execute();
                DRSettingsTask drSettingsTask = new DRSettingsTask(kindergartenId);
                pendingTasks.add(drSettingsTask);
                drSettingsTask.execute();
            }
            pendingTasks.remove(this);
        }
    }

    private class DRSettingsTask extends DailyReportBusiness.LoadDRSettingsTask {
        public DRSettingsTask(Long kindergartenId) {
            super(kindergartenId);
        }

        @Override
        protected DRSettings doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.Settings).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(DRSettings settings) {
            super.onPostExecute(settings);
            if (!checkNetworkException(settings, exception)) {
                MenuItemsStates.DAILY_REPORT_STATE.setState(settings.isEnabled());
            }
            if (--numberOfAppMenuTasks == 0) {
                refreshMenuStates();
            }
            pendingTasks.remove(this);
        }
    }

    private class AppMenuTask extends AppMenuBusiness.LoadAppMenuStates {
        public AppMenuTask(Long kindergartenId) {
            super(kindergartenId);
        }

        @Override
        protected AppMenuState doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(AppMenuState appMenuState) {
            super.onPostExecute(appMenuState);
            if (!checkNetworkException(appMenuState, exception)) {
                Log.d(this.getClass().getName(), "appMenu onPostExecute");
                setMenuStates(appMenuState);
            }
            if (--numberOfAppMenuTasks == 0) {
                refreshMenuStates();
            }
            pendingTasks.remove(this);
        }
    }
}
