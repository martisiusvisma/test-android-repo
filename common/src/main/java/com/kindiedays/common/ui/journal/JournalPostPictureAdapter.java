package com.kindiedays.common.ui.journal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.model.BlurredImageWrapper;
import com.kindiedays.common.model.JournalPictureWrapper;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.JournalGalleryPicture;
import com.kindiedays.common.pojo.JournalPlace;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.pojo.JournalPostPicture;
import com.kindiedays.common.pojo.JournalSignin;
import com.kindiedays.common.pojo.Meal;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.pojo.Nap;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.ui.CalendarTitleFormatter;
import com.kindiedays.common.ui.album.SquareThumbnailItemView;
import com.kindiedays.common.ui.picturegallery.MessagePhotoGalleryActivity;
import com.kindiedays.common.ui.picturegallery.PictureGalleryActivity;
import com.kindiedays.common.utils.PlacesUtils;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 08/07/2015.
 */
public class JournalPostPictureAdapter extends RecyclerView.Adapter {
    private List<Object> listData = new ArrayList<>();
    private List<Place> places;
    private Context context;
    protected Child child;

    private static final int TYPE_DAY = 1;
    private static final int TYPE_GALLERY_PICTURE = 2;
    private static final int TYPE_EVENT = 3;
    private static final int TYPE_NAP = 4;
    private static final int TYPE_SIGNIN = 5;
    private static final int TYPE_JOURNAL_POST = 6;
    private static final int TYPE_PICTURE_SINGLE = 7;
    private static final int TYPE_PICTURE_TWICE = 8;
    private static final int TYPE_PICTURE_SMALL_FACEBOOK = 9;
    private static final int TYPE_PICTURE_FOURTH = 11;
    private static final int TYPE_PICTURE_LARGE_FACEBOOK_COLUMNS = 10;
    private static final int TYPE_JOURNAL_PLACE = 12;
    private static final int TYPE_MENU = 13;
    private static final String TIME_PATTERN = "HH:mm";
    private static final int PICTURE_CAP = 5;
    private static final int TYPE_INVALID = 0;

    private String blurringChild;
    private OnPictureClickListener listener;

    public interface OnPictureClickListener {
        void onGalleryPictureClicked(JournalGalleryPicture picture, BlurredImageWrapper wrapper);
        void onJournalPostPictureClicked(int picturePosition, long postId);
    }

    public JournalPostPictureAdapter(Context context, Child child, OnPictureClickListener listener) {
        this.context = context;

        this.child = child;
        this.listener = listener;
        blurringChild = "com.kindiedays.teacherapp.TeacherApp".equals(context.getApplicationInfo().className)
                ? "&child=" + child.getId()
                : STRING_EMPTY;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public void setListData(List<Object> data) {
        listData = new ArrayList<>();
        Object previousObject = new Object();
        for (Object o: data) {
            if (!(data instanceof Menu)) {
               // listData.add(o);
                if (previousObject instanceof LocalDate && !(o instanceof LocalDate )) {
                    Log.e("previousObject", previousObject.toString());
                    listData.add(previousObject);
                    listData.add(o);
                } else if(!(o instanceof LocalDate )) {
                    listData.add(o);
                }
                previousObject = o;

            }

        }
      //  this.listData = data;
        this.notifyDataSetChanged();
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        View view;
        switch (viewType) {
            case TYPE_DAY:
                view = LayoutInflater.from(context).inflate(R.layout.item_journal_day, parent, false);
                holder = new DayViewHolder(view);
                break;
            case TYPE_PICTURE_SINGLE:
                view = LayoutInflater.from(context).inflate(R.layout.item_single_thumbnail, parent, false);
                holder = new SinglePictureViewHolder(view);
                break;
            case TYPE_PICTURE_LARGE_FACEBOOK_COLUMNS:
                view = LayoutInflater.from(context).inflate(R.layout.item_large_facebook_view, parent, false);
                holder = new FacebookViewHolder(view);
                break;
            case TYPE_PICTURE_SMALL_FACEBOOK:
                view = LayoutInflater.from(context).inflate(R.layout.item_small_facebook_view, parent, false);
                holder = new SmallFacebookViewHolder(view);
                break;
            case TYPE_PICTURE_TWICE:
                view = LayoutInflater.from(context).inflate(R.layout.item_twice_journal_pictures, parent, false);
                holder = new TwicePictureViewHolder(view);
                break;
            case TYPE_PICTURE_FOURTH:
                view = LayoutInflater.from(context).inflate(R.layout.item_fourth_journal_picture, parent, false);
                holder = new FourthPictureViewHolder(view);
                break;
            case TYPE_JOURNAL_POST:
                view = LayoutInflater.from(context).inflate(R.layout.item_journal_journal_post, parent, false);
                holder = new JournalPostHolder(view);
                break;
            case TYPE_GALLERY_PICTURE:
                view = LayoutInflater.from(context).inflate(R.layout.item_gallery_picture, parent, false);
                holder = new GalleryPictureHolder(view);
                break;
            case TYPE_MENU:
                view = LayoutInflater.from(context).inflate(R.layout.item_gallery_menu, parent, false);
                holder = new MenuHolder(view);
                break;
            default:
                view = LayoutInflater.from(context).inflate(R.layout.item_journal_event, parent, false);
                holder = new TextEventHolder(view);

        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        Object data = listData.get(position);
        if (data instanceof JournalPictureWrapper) {
            switch (((JournalPictureWrapper) data).getNbPictures()) {
                case 0:
                    return TYPE_INVALID;
                case 1:
                    return TYPE_PICTURE_SINGLE;
                case 2:
                    return TYPE_PICTURE_TWICE;
                case 4:
                    return TYPE_PICTURE_FOURTH;
                case 3:
                    return TYPE_PICTURE_SMALL_FACEBOOK;
                default:
                    return TYPE_PICTURE_LARGE_FACEBOOK_COLUMNS;

            }
        }
        if (data instanceof Event) {
            return TYPE_EVENT;
        }
        if (data instanceof JournalPost) {
            return TYPE_JOURNAL_POST;
        }
        if (data instanceof Nap) {
            return TYPE_NAP;
        }
        if (data instanceof JournalSignin) {
            return TYPE_SIGNIN;
        }
        if (data instanceof LocalDate) {
            return TYPE_DAY;
        }
        if (data instanceof JournalPlace) {
            return TYPE_JOURNAL_PLACE;
        }
        if (data instanceof JournalGalleryPicture) {
            return TYPE_GALLERY_PICTURE;
        }
        if (data instanceof Menu) {
            return TYPE_MENU;
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_PICTURE_LARGE_FACEBOOK_COLUMNS:
                onBindViewHolder((FacebookViewHolder) holder,
                        (JournalPictureWrapper) listData.get(position));
                break;
            case TYPE_PICTURE_SINGLE:
                onBindViewHolder((SinglePictureViewHolder) holder,
                        (JournalPictureWrapper) listData.get(position));
                break;
            case TYPE_PICTURE_TWICE:
                onBindViewHolder((TwicePictureViewHolder) holder,
                        (JournalPictureWrapper) listData.get(position));
                break;
            case TYPE_PICTURE_FOURTH:
                onBindViewHolder((FourthPictureViewHolder) holder,
                        (JournalPictureWrapper) listData.get(position));
                break;
            case TYPE_PICTURE_SMALL_FACEBOOK:
                onBindViewHolder((SmallFacebookViewHolder) holder,
                        (JournalPictureWrapper) listData.get(position));
                break;
            case TYPE_EVENT:
                onBindViewHolder((TextEventHolder) holder, (Event) listData.get(position));
                break;
            case TYPE_DAY:
                onBindViewHolder((DayViewHolder) holder, (LocalDate) listData.get(position));
                break;
            case TYPE_JOURNAL_POST:
                onBindViewHolder((JournalPostHolder) holder, (JournalPost) listData.get(position));
                break;
            case TYPE_JOURNAL_PLACE:
                onBindViewHolder((TextEventHolder) holder, (JournalPlace) listData.get(position));
                break;
            case TYPE_NAP:
                onBindViewHolder((TextEventHolder) holder, (Nap) listData.get(position));
                break;
            case TYPE_SIGNIN:
                onBindViewHolder((TextEventHolder) holder, (JournalSignin) listData.get(position));
                break;
            case TYPE_GALLERY_PICTURE:
                Log.d("myLogs", "on bind picture");
                onBindViewHolder((GalleryPictureHolder) holder, (JournalGalleryPicture) listData.get(position));
                break;
            case TYPE_MENU:
                onBindViewHolder((MenuHolder) holder, (Menu) listData.get(position));
                break;
            default:
                break;

        }
    }

    private void onBindViewHolder(final SinglePictureViewHolder holder,
                                  JournalPictureWrapper journalPictureWrapper) {
        holder.bind(journalPictureWrapper);
    }

    private void onBindViewHolder(final TwicePictureViewHolder holder,
                                  JournalPictureWrapper pictureWrapper) {
        holder.bind(pictureWrapper);
    }

    private void onBindViewHolder(final SmallFacebookViewHolder holder,
                                  JournalPictureWrapper pictureWrapper) {
        holder.bind(pictureWrapper);
    }

    private void onBindViewHolder(final FourthPictureViewHolder holder,
                                  JournalPictureWrapper pictureWrapper) {
        holder.bind(pictureWrapper);
    }

    private void onBindViewHolder(final FacebookViewHolder holder,
                                  JournalPictureWrapper pictureWrapper) {
        holder.bind(pictureWrapper);
    }

    private void onBindViewHolder(final TextEventHolder holder, Event event) {
        holder.ivEventicon.setImageDrawable(context.getResources().getDrawable(R.drawable.events));
        holder.tvTitle.setText(event.getName());
        holder.tvTime.setText(event.getStart().toString(TIME_PATTERN));
        holder.tvData.setText(event.getStart().toString(TIME_PATTERN) + " " +
                event.getEnd().toString(TIME_PATTERN) + " ");
        holder.tvData.setVisibility(View.VISIBLE);
    }

    private void onBindViewHolder(final JournalPostHolder holder, JournalPost journalPost) {
        holder.tvTitle.setText(journalPost.getText());
        holder.tvTime.setText(journalPost.getTime().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toLocalTime().toString(TIME_PATTERN));
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void onBindViewHolder(TextEventHolder holder, Nap nap) {
        holder.tvTitle.setText(context.getString(R.string.Nap));
        holder.tvTime.setText(nap.getStart().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toLocalTime().toString(TIME_PATTERN));
        holder.ivEventicon.setImageDrawable(context.getResources().getDrawable(R.drawable.nap_journal));
        Minutes minutes;
        if (nap.getEnd() != null) {
            minutes = Minutes.minutesBetween(nap.getStart(), nap.getEnd());
        } else {
            LocalTime now = LocalDateTime.now().toLocalTime();
            minutes = Minutes.minutesBetween(nap.getStart().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toLocalTime(), now);
        }
        holder.tvData.setVisibility(View.VISIBLE);
        holder.tvData.setText(context.getString(R.string.XX_minutes, minutes.getMinutes()));
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void onBindViewHolder(final TextEventHolder holder, JournalSignin signin) {
        holder.tvData.setVisibility(View.GONE);
        if (signin.getEventType() == JournalSignin.SIGNIN) {
            holder.tvTime.setText(signin.getDate().toString(TIME_PATTERN));
            holder.tvTitle.setText(context.getString(R.string.HasArrived, child.getNickname()));
            holder.ivEventicon.setImageDrawable(context.getResources().getDrawable(R.drawable.signin_journal));

        } else if (signin.getEventType() == JournalSignin.SIGNOUT) {
            holder.tvTime.setText(signin.getDate().toString(TIME_PATTERN));
            holder.tvTitle.setText(context.getString(R.string.HasBeenPickedUp, child.getNickname()));
            holder.ivEventicon.setImageDrawable(context.getResources().getDrawable(R.drawable.signout_journal));

        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void onBindViewHolder(final TextEventHolder holder, JournalPlace journalPlace) {
        holder.tvData.setVisibility(View.GONE);
        holder.tvTime.setText(journalPlace.getLocalTime().toString(TIME_PATTERN));
        holder.ivEventicon.setImageResource(R.drawable.place);
        Place place = PlacesUtils.findPlaceById(journalPlace.getId(), places);
        String actionText = journalPlace.getAction() == JournalPlace.Action.LEAVE
                ? context.getString(R.string.leave_from_location, child.getNickname(), place.getName())
                : context.getString(R.string.entered_in_location, child.getNickname(), place.getName());
        holder.tvTitle.setText(actionText);
    }

    private void onBindViewHolder(final DayViewHolder holder, LocalDate localDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(localDate.toDate());
        holder.tvDay.setText(CalendarTitleFormatter.getDayOfWeekString(KindieDaysApp.getApp(), calendar)
                .concat(localDate.toString(" dd/MM/yyyy")));
    }

    private void onBindViewHolder(final GalleryPictureHolder holder, JournalGalleryPicture picture) {
        holder.bind(picture);
    }

    private void onBindViewHolder(MenuHolder holder, Menu menu) {
        ArrayList<Map<String, String>> meals = new ArrayList<>();
        for (Meal meal : menu.getMeals()) {
            meals.add(Collections.singletonMap("Name", meal.getText()));
        }
        holder.list.setAdapter(new SimpleAdapter(context, meals, android.R.layout.simple_list_item_1, new String[]{"Name"}, new int[]{android.R.id.text1}));
    }

    @Override
    public int getItemCount() {
        if (listData == null) {
            return 0;
        }
        return listData.size();
    }

    @SuppressWarnings("squid:S3398")
    private void setProgressGlidePicture(ImageView imageView, String url, ProgressBar progress) {
        Glide.with(imageView.getContext()).load(url).into(new GlideDrawableImageViewTarget(imageView) {
            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                super.onResourceReady(drawable, anim);
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                progress.setVisibility(View.GONE);
                Log.e(JournalPostPictureAdapter.class.getName(), e.getMessage(), e);
            }
        });
    }

    private void callGallery(int pos, long id) {
        listener.onJournalPostPictureClicked(pos, id);
    }

    private static class DayViewHolder extends RecyclerView.ViewHolder {
        TextView tvDay;

        DayViewHolder(View view) {
            super(view);
            tvDay = (TextView) view.findViewById(R.id.day);
        }
    }


    private static class JournalPostHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvTime;

        JournalPostHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.title);
            tvTime = (TextView) view.findViewById(R.id.time);
        }
    }


    private static class TextEventHolder extends RecyclerView.ViewHolder {
        ImageView ivEventicon;
        TextView tvTitle;
        TextView tvData;
        TextView tvTime;

        TextEventHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.title);
            tvData = (TextView) view.findViewById(R.id.data);
            ivEventicon = (ImageView) view.findViewById(R.id.event_icon);
            tvTime = (TextView) view.findViewById(R.id.time);
        }
    }


    private class GalleryPictureHolder extends RecyclerView.ViewHolder {
        TextView tvCaption;
        TextView tvTime;
        ProgressBar pbLoader;
        ImageView ivPicture;

        private JournalGalleryPicture picture;
        private BlurredImageWrapper wrapper;

        GalleryPictureHolder(View itemView) {
            super(itemView);
            tvCaption = (TextView) itemView.findViewById(R.id.tv_caption);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            pbLoader = (ProgressBar) itemView.findViewById(R.id.pb_loader);
            ivPicture = (ImageView) itemView.findViewById(R.id.iv_picture);
            ivPicture.setOnClickListener(v -> listener.onGalleryPictureClicked(picture, wrapper));
        }

        private void bind(JournalGalleryPicture picture) {
            this.picture = picture;
            tvTime.setText(picture.getCreated().toLocalTime().toString(TIME_PATTERN));
            String caption = picture.getCaption();
            tvCaption.setVisibility(TextUtils.isEmpty(caption) ? View.GONE : View.VISIBLE);
            tvCaption.setText(caption);
            wrapper = new BlurredImageWrapper();
            picture.setUrl(picture.getUrl() + blurringChild);
            wrapper.setUrl(picture.getUrl());
            Glide.with(itemView.getContext()).load(picture.getUrl()).into(new GlideDrawableImageViewTarget(ivPicture) {
                @Override
                public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                    super.onResourceReady(drawable, anim);
                    pbLoader.setVisibility(View.GONE);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    pbLoader.setVisibility(View.GONE);
                    Log.e(JournalPostPictureAdapter.class.getName(), e.getMessage(), e);
                }
            });
        }
    }

    private class MenuHolder extends RecyclerView.ViewHolder {
        private ListView list;

        public MenuHolder(View itemView) {
            super(itemView);
            list = (ListView) itemView.findViewById(R.id.menuList);
        }
    }


    private class SinglePictureViewHolder extends RecyclerView.ViewHolder {
        private SquareThumbnailItemView thumbnail;
        private ProgressBar progress;

        private JournalPictureWrapper wrapper;

        SinglePictureViewHolder(View view) {
            super(view);
            thumbnail = (SquareThumbnailItemView) view.findViewById(R.id.thumbnail);
            progress = (ProgressBar) view.findViewById(R.id.progressBar);

            thumbnail.setOnClickListener(v -> callGallery(0, wrapper.getId()));
        }

        void bind(JournalPictureWrapper wrapper) {
            this.wrapper = wrapper;
            String url = wrapper.getPictures().get(0).getUrl();
            setProgressGlidePicture(thumbnail, url, progress);
        }
    }


    private class TwicePictureViewHolder extends RecyclerView.ViewHolder {
        SquareThumbnailItemView ivMain;
        SquareThumbnailItemView ivSecondary;
        ProgressBar pb1;
        ProgressBar pb2;

        JournalPictureWrapper wrapper;

        TwicePictureViewHolder(View view) {
            super(view);
            ivMain = (SquareThumbnailItemView) view.findViewById(R.id.iv_main_1);
            ivSecondary = (SquareThumbnailItemView) view.findViewById(R.id.iv_main_2);
            pb1 = (ProgressBar) view.findViewById(R.id.pb_1);
            pb2 = (ProgressBar) view.findViewById(R.id.pb_2);

            ivMain.setOnClickListener(v -> callGallery(0, wrapper.getId()));
            ivSecondary.setOnClickListener(v -> callGallery(1, wrapper.getId()));
        }

        void bind(JournalPictureWrapper wrapper) {
            this.wrapper = wrapper;
            List<JournalPostPicture> list = wrapper.getPictures();
            setProgressGlidePicture(ivMain, list.get(0).getUrl(), pb1);
            setProgressGlidePicture(ivSecondary, list.get(1).getUrl(), pb2);
        }
    }


    private class FacebookViewHolder extends RecyclerView.ViewHolder {
        SquareThumbnailItemView ivMain1;
        SquareThumbnailItemView ivMain2;
        ImageView ivSecondary1;
        ImageView ivSecondary2;
        ImageView ivSecondary3;
        TextView tvPicsCounter;
        RelativeLayout rlContaner;

        ProgressBar pb1;
        ProgressBar pb2;
        ProgressBar pb3;
        ProgressBar pb4;
        ProgressBar pb5;

        private JournalPictureWrapper wrapper;

        FacebookViewHolder(View itemView) {
            super(itemView);
            ivMain1 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_main_1);
            ivMain2 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_main_2);
            ivSecondary1 = (ImageView) itemView.findViewById(R.id.iv_secondary_1);
            ivSecondary2 = (ImageView) itemView.findViewById(R.id.iv_secondary_2);
            ivSecondary3 = (ImageView) itemView.findViewById(R.id.iv_secondary_3);
            tvPicsCounter = (TextView) itemView.findViewById(R.id.tv_pics_counter);
            rlContaner = (RelativeLayout) itemView.findViewById(R.id.rl_container);

            pb1 = (ProgressBar) itemView.findViewById(R.id.pb_1);
            pb2 = (ProgressBar) itemView.findViewById(R.id.pb_2);
            pb3 = (ProgressBar) itemView.findViewById(R.id.pb_3);
            pb4 = (ProgressBar) itemView.findViewById(R.id.pb_4);
            pb5 = (ProgressBar) itemView.findViewById(R.id.pb_5);

            ivMain1.setOnClickListener(v -> callGallery(0, wrapper.getId()));
            ivMain2.setOnClickListener(v -> callGallery(1, wrapper.getId()));
            ivSecondary1.setOnClickListener(v -> callGallery(2, wrapper.getId()));
            ivSecondary2.setOnClickListener(v -> callGallery(3, wrapper.getId()));
            rlContaner.setOnClickListener(v -> callGallery(4, wrapper.getId()));
        }

        void bind(JournalPictureWrapper pictureWrapper) {
            this.wrapper = pictureWrapper;
            List<JournalPostPicture> list = pictureWrapper.getPictures();
            int size = list.size();

            setProgressGlidePicture(ivMain1, list.get(0).getUrl(), pb1);
            setProgressGlidePicture(ivMain2, list.get(1).getUrl(), pb2);
            setProgressGlidePicture(ivSecondary1, list.get(2).getUrl(), pb3);
            setProgressGlidePicture(ivSecondary2, list.get(3).getUrl(), pb4);

            Glide.with(context).load(list.get(4).getUrl()).into(new GlideDrawableImageViewTarget(ivSecondary3) {
                @Override
                public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                    super.onResourceReady(drawable, anim);
                    pb5.setVisibility(View.GONE);
                    if (size > PICTURE_CAP) {
                        tvPicsCounter.setVisibility(View.VISIBLE);
                        String text = "+" + (size - PICTURE_CAP);
                        tvPicsCounter.setText(text);
                    }
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    pb5.setVisibility(View.GONE);
                    Log.e(JournalPostPictureAdapter.class.getName(), e.getMessage(), e);
                }
            });
        }
    }


    private class FourthPictureViewHolder extends RecyclerView.ViewHolder {
        SquareThumbnailItemView ivMain1;
        SquareThumbnailItemView ivMain2;
        SquareThumbnailItemView ivSecondary1;
        SquareThumbnailItemView ivSecondary2;

        ProgressBar pb1;
        ProgressBar pb2;
        ProgressBar pb3;
        ProgressBar pb4;

        private JournalPictureWrapper wrapper;

        FourthPictureViewHolder(View itemView) {
            super(itemView);
            ivMain1 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_main_1);
            ivMain2 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_main_2);
            ivSecondary1 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_secondary_1);
            ivSecondary2 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_secondary_2);

            pb1 = (ProgressBar) itemView.findViewById(R.id.pb_1);
            pb2 = (ProgressBar) itemView.findViewById(R.id.pb_2);
            pb3 = (ProgressBar) itemView.findViewById(R.id.pb_3);
            pb4 = (ProgressBar) itemView.findViewById(R.id.pb_4);

            ivMain1.setOnClickListener(v -> callGallery(0, wrapper.getId()));
            ivMain2.setOnClickListener(v -> callGallery(1, wrapper.getId()));
            ivSecondary1.setOnClickListener(v -> callGallery(2, wrapper.getId()));
            ivSecondary2.setOnClickListener(v -> callGallery(3, wrapper.getId()));
        }

        void bind(JournalPictureWrapper wrapper) {
            this.wrapper = wrapper;
            List<JournalPostPicture> list = wrapper.getPictures();
            setProgressGlidePicture(ivMain1, list.get(0).getUrl(), pb1);
            setProgressGlidePicture(ivMain2, list.get(1).getUrl(), pb2);
            setProgressGlidePicture(ivSecondary1, list.get(2).getUrl(), pb3);
            setProgressGlidePicture(ivSecondary2, list.get(3).getUrl(), pb4);
        }
    }


    private class SmallFacebookViewHolder extends RecyclerView.ViewHolder {
        SquareThumbnailItemView ivMain;
        SquareThumbnailItemView ivSecondary1;
        SquareThumbnailItemView ivSecondary2;

        ProgressBar pb1;
        ProgressBar pb2;
        ProgressBar pb3;

        JournalPictureWrapper wrapper;

        SmallFacebookViewHolder(View itemView) {
            super(itemView);
            ivMain = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_main_1);
            ivSecondary1 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_secondary_1);
            ivSecondary2 = (SquareThumbnailItemView) itemView.findViewById(R.id.iv_secondary_2);
            pb1 = (ProgressBar) itemView.findViewById(R.id.pb_1);
            pb2 = (ProgressBar) itemView.findViewById(R.id.pb_2);
            pb3 = (ProgressBar) itemView.findViewById(R.id.pb_3);

            ivMain.setOnClickListener(v -> callGallery(0, wrapper.getId()));
            ivSecondary1.setOnClickListener(v -> callGallery(1, wrapper.getId()));
            ivSecondary2.setOnClickListener(v -> callGallery(2, wrapper.getId()));
        }

        void bind(JournalPictureWrapper wrapper) {
            this.wrapper = wrapper;
            List<JournalPostPicture> list = wrapper.getPictures();
            setProgressGlidePicture(ivMain, list.get(0).getUrl(), pb1);
            setProgressGlidePicture(ivSecondary1, list.get(1).getUrl(), pb2);
            setProgressGlidePicture(ivSecondary2, list.get(2).getUrl(), pb3);
        }
    }
}

