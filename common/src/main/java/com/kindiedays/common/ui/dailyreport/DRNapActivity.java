package com.kindiedays.common.ui.dailyreport;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.NapRes;
import com.kindiedays.common.utils.PictureUtils;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DRNapActivity extends AppCompatActivity {
    public static final String NAP = "NAP";
    private TextView noteLbl;
    private TextView timeLbl;
    private TextView startLbl;
    private TextView endLbl;
    private TextView startLblTitle;
    private TextView endLblTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_nap);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextViewCustom title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.daily_nap);

        ImageView sectionIcon = (ImageView) findViewById(R.id.rightIcon);
        sectionIcon.setVisibility(View.VISIBLE);
        sectionIcon.setImageDrawable(PictureUtils.svgToBitmapDrawable(getResources(), R.raw.dr_sleep_bg, 80));

        timeLbl = (TextView) findViewById(R.id.timeLbl);
        noteLbl = (TextView) findViewById(R.id.noteContentLbl);
        startLblTitle = (TextView) findViewById(R.id.startTitle);
        startLbl = (TextView) findViewById(R.id.startContent);
        endLblTitle = (TextView) findViewById(R.id.endTitle);
        endLbl = (TextView) findViewById(R.id.endContent);

        setContentView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            overridePendingTransition(R.anim.from_left_to_right, R.anim.exit_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("squid:S1192")
    private void setContentView(){
        Bundle bundle = getIntent().getExtras();
        NapRes data = (NapRes) bundle.getSerializable(NAP);
        if (data != null) {
            timeLbl.setText(data.getStart().toString("HH:mm")); // ask jonne getDate with full format
            noteLbl.setText(data.getNotes());
            startLblTitle.setText(R.string.Start);
            startLbl.setText(data.getStart().toString("HH:mm"));
            endLblTitle.setText(R.string.End);
            endLbl.setText(data.getEnd().toString("HH:mm"));
        }
    }

}
