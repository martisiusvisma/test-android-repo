package com.kindiedays.common.ui.messages;


import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.kindiedays.common.R;
import com.kindiedays.common.network.conversations.GetMediaFile;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.ui.KindieDaysActivity;

import org.parceler.Parcels;

public class ExoPlayerActivity extends KindieDaysActivity implements ExoPlayer.EventListener {
    private static final String EXTRA_ATTACHMENT = "extra_attach";
    private static final String EXTRA_POSITION = "extra_pos";

    private SimpleExoPlayerView videoView;
    private SimpleExoPlayer player;
    private RelativeLayout rlContent;
    private ProgressBar progressBar;
    private Attachment attachment;

    private long savedPosition;

    private android.content.BroadcastReceiver brDownloadComplete = new android.content.BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                showVideoDownloaded();
            }
        }

        private void showVideoDownloaded() {
            TSnackbar snackbar = TSnackbar.make(rlContent, getString(R.string.video_downloaded),
                    TSnackbar.LENGTH_SHORT);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    };

    public static void start(final Context context, Attachment attachment) {
        Intent intent = new Intent(context, ExoPlayerActivity.class);
        intent.putExtra(EXTRA_ATTACHMENT, Parcels.wrap(attachment));
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        savedPosition = 0;
        if (savedInstanceState != null) {
            savedPosition = savedInstanceState.getLong(EXTRA_POSITION);
        }

        videoView = (SimpleExoPlayerView) findViewById(R.id.exo_view);
        rlContent = (RelativeLayout) findViewById(R.id.rl_content);
        progressBar = (ProgressBar) findViewById(R.id.pb_loader);
        attachment = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_ATTACHMENT));
        initPlayer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_message_photo_gallery, menu);
        return true;
    }

    @SuppressWarnings("squid:S3516")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        } else if (i == R.id.action_save) {
            DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            GetMediaFile.downloadFile(this, downloadManager, attachment.getFilename(), attachment.getUri());
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(EXTRA_POSITION, player.getCurrentPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedPosition = savedInstanceState.getLong(EXTRA_POSITION);
        player.seekTo(savedPosition);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(brDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    protected void onPause() {
        super.onPause();
        savedPosition = player.getCurrentPosition();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(brDownloadComplete);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    private void initPlayer() {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        videoView.setPlayer(player);
        player.addListener(this);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getPackageName()), bandwidthMeter);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource(Uri.parse(attachment.getUri()),
                dataSourceFactory, extractorsFactory, null, null);
        player.setPlayWhenReady(true);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.seekTo(savedPosition);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
        //no implementation
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        //no implementation
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        //no implementation
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
            player.seekTo(0);
            player.setPlayWhenReady(false);
        } else if (playbackState == ExoPlayer.STATE_READY) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.e("ExoPlayerActivity", "onPlayerError: " + error );
        Toast.makeText(this, error.getCause().getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositionDiscontinuity() {
        //no implementation
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        //no implementation
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }
}
