package com.kindiedays.common.ui.personselection;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.utils.GlideUtils;

import java.util.List;

/**
 * Created by pleonard on 16/06/2015.
 */
@SuppressWarnings("squid:S1226")
public class PersonAdapter<T extends Person> extends BaseAdapter {

    private List<T> persons;
    private Activity activity;

    public PersonAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setPersons(List<T> persons) {
        this.persons = persons;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (persons == null) {
            return 0;
        }
        return persons.size();
    }

    @Override
    public Person getItem(int position) {
        if (persons != null && persons.size() >= 1) {
            return persons.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return persons.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_person, parent, false);
            holder = new ViewHolder();
            holder.ivPersonPicture = (ImageView) convertView.findViewById(R.id.personThumbnail);
            holder.tvPersonName = (TextView) convertView.findViewById(R.id.personName);
            holder.tvUndreadMessages = (TextView) convertView.findViewById(R.id.tv_unread_messages);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Person person = getItem(position);
        if (person != null) {
            holder.tvPersonName.setText(person.getListName());
            int count;
            if (person instanceof Teacher && ((count = ((Teacher) person).getUnreadMessages()) > 0)) {
                holder.tvUndreadMessages.setText(String.valueOf(count));
                holder.tvUndreadMessages.setVisibility(View.VISIBLE);
            } else {
                holder.tvUndreadMessages.setVisibility(View.GONE);
            }
            Glide.with(activity).load(person.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(holder.ivPersonPicture);
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView ivPersonPicture;
        TextView tvPersonName;
        TextView tvUndreadMessages;
    }
}
