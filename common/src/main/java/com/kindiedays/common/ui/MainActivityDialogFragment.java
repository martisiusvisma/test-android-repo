package com.kindiedays.common.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;

import java.util.HashSet;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

/**
 * Created by pleonard on 09/06/2015.
 */
public abstract class MainActivityDialogFragment extends DialogFragment {
    public static final String TAG = MainActivity.class.getName();
    protected Set<AsyncTask> pendingTasks = new HashSet<>();
    protected ScrollView scrollingView;

    protected void stopPendingTasks() {
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        pendingTasks.clear();
    }

    @Override
    public void onDetach() {
        stopPendingTasks();
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        if (Webservice.isAuthorizationTokenInitialized()) {
            refreshData();
        }
    }

    @SuppressWarnings("deprecation")
    protected boolean checkNetworkException(Object value, KindieDaysNetworkException exception) {
        boolean exceptionDetected = (exception != null && value == null);
        if (exceptionDetected) {
            if (exception.isLocalizedMessage()) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    stopPendingTasks();
                    Intent i = new Intent(getActivity(), ((KindieDaysApp) getActivity().getApplication()).getLoginActivityClass());
                    startActivity(i);
                } else {
                    Log.e(this.getClass().getSimpleName(), "Internal error");
                }
            }
            Log.d(TAG, "An exception has been raised");
        }
        return exceptionDetected;
    }

    public abstract void refreshData();

}
