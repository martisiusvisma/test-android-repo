package com.kindiedays.common.ui.personselection.teacherselection;

import android.content.Context;
import android.view.ViewGroup;

import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.personselection.PersonButtonAdapter;
import com.kindiedays.common.ui.personselection.PersonSelectionButton;

import java.util.List;

/**
 * Created by pleonard on 13/11/2015.
 */
public class TeacherButtonAdapter extends PersonButtonAdapter {

    public TeacherButtonAdapter(Context context) {
        super(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PersonSelectionButton button = new TeacherSelectionButton(mContext);
        return new ItemViewHolder(button);
    }

    public void setParties(List<? extends Person> parties){
        this.persons = parties;
        notifyDataSetChanged();
    }
}
