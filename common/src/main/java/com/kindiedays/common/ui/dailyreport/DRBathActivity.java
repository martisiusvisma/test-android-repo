package com.kindiedays.common.ui.dailyreport;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.daily.ResourceFaceTab;
import com.kindiedays.common.pojo.BathRes;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.common.utils.constants.DailyReportConstants;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DRBathActivity extends AppCompatActivity {
    public static final String BATH = "BATH";
    private FragmentTabHost outcomeTabHost;

    private static final String TYPE = "Type";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_bath);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);


        TextViewCustom title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.daily_bath);

        ImageView sectionIcon = (ImageView) findViewById(R.id.rightIcon);
        sectionIcon.setVisibility(View.VISIBLE);
        sectionIcon.setImageDrawable(PictureUtils.svgToBitmapDrawable(getResources(), R.raw.dr_wc_potty_bg, 80));

        outcomeTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        outcomeTabHost.setup(this, this.getSupportFragmentManager(), R.id.realplatetabcontent);

        // OUTCOME TAB LAYOUT
        TabHost.TabSpec ts = outcomeTabHost.newTabSpec("item1").setIndicator(createTabViewDrawable(R.raw.dr_wc_diaper_circle, R.raw.dr_wc_diaper_circle_tp));
        Bundle bundle = new Bundle();
        bundle.putString(TYPE, DailyReportConstants.DIAPER_TAG);
        outcomeTabHost.addTab(ts, OutComeFragment.class, bundle);

        TabHost.TabSpec ts2 = outcomeTabHost.newTabSpec("item2").setIndicator(createTabViewDrawable(R.raw.dr_wc_potty_circle, R.raw.dr_wc_potty_circle_tp));
        Bundle bundle2 = new Bundle();
        bundle2.putString(TYPE, DailyReportConstants.POTTY_TAG);
        outcomeTabHost.addTab(ts2, OutComeFragment.class, bundle2);

        TabHost.TabSpec ts3 = outcomeTabHost.newTabSpec("item3").setIndicator(createTabViewDrawable(R.raw.dr_wc_toilet_circle, R.raw.dr_wc_toilet_circle_tp));
        Bundle bundle3 = new Bundle();
        bundle3.putString(TYPE, DailyReportConstants.TOILET_TAG);
        outcomeTabHost.addTab(ts3, OutComeFragment.class, bundle3);

        setContentView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            overridePendingTransition(R.anim.from_left_to_right, R.anim.exit_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class OutComeFragment extends Fragment {

    }

    private ResourceFaceTab createTabViewDrawable(int pressed, int normal) {
        BitmapDrawable pressedBmp = PictureUtils.svgToBitmapDrawable(getResources(), pressed, 80);
        BitmapDrawable normalBmp = PictureUtils.svgToBitmapDrawable(getResources(), normal, 80);

        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_selected}, pressedBmp);
        res.addState(new int[]{}, normalBmp);
        ResourceFaceTab indicator = new ResourceFaceTab(this);
        indicator.setBackgroundDrawable(res);
        return indicator;
    }

    private void setContentView() {
        Bundle bundle = getIntent().getExtras();
        BathRes data = (BathRes) bundle.getSerializable(BATH);
        if (data != null) {
            TextView timeLbl = (TextView) findViewById(R.id.timeLbl);
            TextView noteLbl = (TextView) findViewById(R.id.noteContentLbl);
            TextView typeLbl = (TextView) findViewById(R.id.typeLbl);
            TextView constitutionLbl = (TextView) findViewById(R.id.constitutionLbl);

            noteLbl.setText(data.getNotes());
            timeLbl.setText(data.getTime().toString("HH:mm"));

            typeLbl.setText(data.getTypeDescription());

            handleConstitution(data, constitutionLbl);
            handleEquipment(data);

            for (int i = 0; i < 3; i++) {
                outcomeTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(false);
            }

        }
    }

    private void handleConstitution(BathRes data, TextView constitutionLbl) {
        if (data.getConstitution().length() != 0) {
            switch (data.getConstitution()) {
                case DailyReportConstants.LOOSE_TAG:
                    constitutionLbl.setText(getResources().getText(R.string.Loose));
                    break;
                case DailyReportConstants.NORMAL_TAG:
                    constitutionLbl.setText(getResources().getText(R.string.Normal));
                    break;
                case DailyReportConstants.SOLID_TAG:
                    constitutionLbl.setText(getResources().getText(R.string.Solid));
                    break;
                default:
                    break;
            }
        } else {
            constitutionLbl.setVisibility(View.GONE);
        }
    }

    private void handleEquipment(BathRes data) {
        for (int i = 0; i < 3; i++) {
            outcomeTabHost.getTabWidget().getChildTabViewAt(i).setVisibility(View.GONE);
        }

        if (data.getEquipment().get(0).equals(DailyReportConstants.DIAPER_TAG)) {
            outcomeTabHost.setCurrentTab(0);
            outcomeTabHost.getTabWidget().getChildTabViewAt(0).setVisibility(View.VISIBLE);
        } else if (data.getEquipment().get(0).equals(DailyReportConstants.POTTY_TAG)) {
            outcomeTabHost.setCurrentTab(1);
            outcomeTabHost.getTabWidget().getChildTabViewAt(1).setVisibility(View.VISIBLE);
        } else {
            outcomeTabHost.setCurrentTab(2);
            outcomeTabHost.getTabWidget().getChildTabViewAt(2).setVisibility(View.VISIBLE);
        }
    }
}
