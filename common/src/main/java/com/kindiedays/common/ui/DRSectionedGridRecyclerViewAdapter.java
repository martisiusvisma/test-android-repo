package com.kindiedays.common.ui;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Gabriele Mariotti (gabri.mariotti@gmail.com)
 */
@SuppressWarnings({"squid:S1068", "squid:S3358", "squid:S1226"})
public class DRSectionedGridRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private static final int SECTION_TYPE = 0;
    private static final int HEADER_TYPE = 100;
    private static final int FOOTER_TYPE = 101;

    private boolean mValid = true;
    private int mSectionResourceId;
    private int[] mTextResourceId;
    private int[] mSelectionResourceId;
    private int[] mSelectionTextResourceId;
    private RecyclerView.Adapter mBaseAdapter;
    private OnSectionViewClickListener mOnSectionViewClickListener;
    private SparseArray<Section> mSections = new SparseArray<>();
    private View header;
    private View footer;

    public interface OnSectionViewClickListener {
        void onSelectionViewClick(View selectionView);
        void onTextViewClick(View textView);
    }

    public DRSectionedGridRecyclerViewAdapter(Context context, int sectionResourceId,
                                              int[] textResourceId,
                                              int[] selectionResourceId,
                                              RecyclerView recyclerView,
                                              RecyclerView.Adapter baseAdapter,
                                              OnSectionViewClickListener onSectionViewClickListener) {

        mSectionResourceId = sectionResourceId;
        mTextResourceId = textResourceId;
        mSelectionResourceId = selectionResourceId;
        mBaseAdapter = baseAdapter;
        mContext = context;
        mOnSectionViewClickListener = onSectionViewClickListener;

        mBaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        });

        final GridLayoutManager layoutManager = (GridLayoutManager) (recyclerView.getLayoutManager());
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 && header != null) {
                    return layoutManager.getSpanCount();
                } else if (footer != null && position == getItemCount() - 1) {
                    return layoutManager.getSpanCount();
                } else {
                    if (header != null) {
                        position--;
                    }
                    return (isSectionHeaderPosition(position)) ? layoutManager.getSpanCount() : 1;
                }
            }
        });
    }

    public View getHeader() {
        return header;
    }

    public void setHeader(View header) {
        this.header = header;
    }

    public View getFooter() {
        return footer;
    }

    public void setFooter(View footer) {
        this.footer = footer;
    }

    public int[] getSelectionView() {
        return mSelectionTextResourceId;
    }

    public void setSelectionView(int[] mSelectionTextResourceId) {
        this.mSelectionTextResourceId = mSelectionTextResourceId;
        notifyItemChanged(0, mSelectionTextResourceId);
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {

        private List<TextView> textViews = new ArrayList<>();
        private List<Button> selectionViews = new ArrayList<>();

        public SectionViewHolder(View view, int[] mTextResourceId, int[] mSelectionResourceId,
                                 OnSectionViewClickListener onSectionViewClickListener) {
            super(view);
            for (int i = 0; i < mTextResourceId.length; i++) {
                TextView textView = (TextView) view.findViewById(mTextResourceId[i]);
                textView.setOnClickListener(onSectionViewClickListener::onTextViewClick);
                textViews.add(i, textView);
            }
            for (int i = 0; i < mSelectionResourceId.length; i++) {
                View selectionView = view.findViewById(mSelectionResourceId[i]);
                selectionView.setOnClickListener(onSectionViewClickListener::onSelectionViewClick);
                selectionViews.add(i, (Button) selectionView);
            }
        }
    }


    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View view) {
            super(view);
        }
    }


    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int typeView) {
        switch (typeView) {
            case HEADER_TYPE:
                return new HeaderViewHolder(header);
            case SECTION_TYPE: {
                final View view = LayoutInflater.from(mContext).inflate(mSectionResourceId, parent, false);
                return new SectionViewHolder(view, mTextResourceId, mSelectionResourceId, mOnSectionViewClickListener);
            }
            case FOOTER_TYPE:
                return new FooterViewHolder(footer);
            default:
                return mBaseAdapter.onCreateViewHolder(parent, typeView - 1);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty())
            super.onBindViewHolder(holder, position, payloads);
        else if (position == 0) {
            int[] textResources = (int[]) payloads.get(0);
            List<Button> selectionViews = ((SectionViewHolder) holder).selectionViews;
            Log.d(DRSectionedGridRecyclerViewAdapter.class.getName(), "on bind position == 0");
            for (int i = 0; i < selectionViews.size(); i++) {
                selectionViews.get(i).setText(textResources[i]);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder sectionViewHolder, int position) {
        if (header != null && position == 0) {
            //Bind header
        } else if (footer != null && position == getItemCount() - 1) {
            //Bind footer
        } else {
            if (header != null) {
                position--;
            }
            if (isSectionHeaderPosition(position)) {
                List<Button> selectionViews = ((SectionViewHolder) sectionViewHolder).selectionViews;
                for (int i = 0; i < selectionViews.size(); i++) {
                    Button selectionButton = selectionViews.get(i);
                    if (position == 0) {
                        selectionButton.setText(mContext.getString(mSelectionTextResourceId[i]));
                        selectionButton.setVisibility(View.VISIBLE);
                    } else
                        selectionButton.setVisibility(View.GONE);
                }
                List<TextView> sectionTextViews = ((SectionViewHolder) sectionViewHolder).textViews;
                for (int i = 0; i < sectionTextViews.size(); i++) {
                    sectionTextViews.get(i).setText(mSections.get(position).text[i]);
                    sectionTextViews.get(i).setTag(mSections.get(position).tag);
                }
            } else {
                mBaseAdapter.onBindViewHolder(sectionViewHolder, sectionedPositionToPosition(position));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (header != null && position == 0) {
            return HEADER_TYPE;
        } else if (footer != null && position == getItemCount() - 1) {
            return FOOTER_TYPE;
        } else {
            if (header != null) {
                position--;
            }
            return isSectionHeaderPosition(position)
                    ? SECTION_TYPE
                    : mBaseAdapter.getItemViewType(sectionedPositionToPosition(position)) + 1;
        }
    }


    public static class Section {
        int firstPosition;
        int sectionedPosition;
        CharSequence[] text;
        Object tag;

        public Section(int firstPosition, CharSequence[] text) {
            this.firstPosition = firstPosition;
            this.text = text;
        }

        public Section(int firstPosition, CharSequence[] text, Object tag) {
            this.firstPosition = firstPosition;
            this.text = text;
            this.tag = tag;
        }

        public CharSequence[] getText() {
            return text;
        }
    }


    public void setSections(List<Section> sections) {
        mSections.clear();

        Arrays.sort(sections.toArray(new Section[0]), (o, o1) -> (o.firstPosition == o1.firstPosition)
                ? 0
                : ((o.firstPosition < o1.firstPosition) ? -1 : 1));

        int offset = 0; // offset positions for the headers we're adding
        for (Section section : sections) {
            section.sectionedPosition = section.firstPosition + offset;
            mSections.append(section.sectionedPosition, section);
            ++offset;
        }

        notifyDataSetChanged();
    }

    public int positionToSectionedPosition(int position) {
        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).firstPosition > position) {
                break;
            }
            ++offset;
        }
        return position + offset;
    }

    public int sectionedPositionToPosition(int sectionedPosition) {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return RecyclerView.NO_POSITION;
        }

        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).sectionedPosition > sectionedPosition) {
                break;
            }
            --offset;
        }
        return sectionedPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return mSections.get(position) != null;
    }


    @Override
    public long getItemId(int position) {
        if (position == 0 && header != null) {
            return header.getId();
        } else if (position == this.getItemCount() - 1 && footer != null) {
            return footer.getId();
        } else {
            if (header != null) {
                position--;
            }
            return isSectionHeaderPosition(position)
                    ? Integer.MAX_VALUE - mSections.indexOfKey(position)
                    : mBaseAdapter.getItemId(sectionedPositionToPosition(position));
        }
    }

    @Override
    public int getItemCount() {
        int count = (mValid ? mBaseAdapter.getItemCount() + mSections.size() : 0);
        if (header != null) {
            count++;
        }
        if (footer != null) {
            count++;
        }
        return count;
    }

}