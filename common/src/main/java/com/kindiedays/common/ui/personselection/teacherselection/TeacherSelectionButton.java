package com.kindiedays.common.ui.personselection.teacherselection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.kindiedays.common.R;
import com.kindiedays.common.ui.personselection.PersonSelectionButton;

/**
 * Created by pleonard on 13/11/2015.
 */
public class TeacherSelectionButton extends PersonSelectionButton {


    public TeacherSelectionButton(Context context) {
        super(context);
    }

    public TeacherSelectionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TeacherSelectionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void refreshPerson() {
        if(person.isSelected()){
            ivPersonPicture.setColorFilter(getContext().getResources().getColor(R.color.selectedfilter));
            ivPersonPicture.setAlpha(0xFF);
            ivSelectionMark.setVisibility(View.VISIBLE);
        }
        else {
            ivPersonPicture.clearColorFilter();
            ivSelectionMark.setVisibility(View.GONE);
        }
    }
}
