package com.kindiedays.common.ui.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.business.SessionBusiness;

import org.json.JSONArray;

import cz.msebera.android.httpclient.HttpStatus;

import static com.kindiedays.common.utils.NetworkUtils.isOnline;

/**
 * A login screen that offers login via email/password.
 */
@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class LoginActivity extends Activity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private static final String DEVELOPER_MODE = "developerMode";
    private static final String DEVELOPER_LOGIN = "developer";
    private static final String DEVELOPER_PSWRD = "qwerty";

    // UI references.
    protected EditText mEmailView;
    protected EditText mPasswordView;
    public ImageView bgImage;
    private View mProgressView;
    private View mLoginFormView;
    private Switch swEnvironment;

    private boolean isDestroyed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isDestroyed = false;
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        bgImage = (ImageView) findViewById(R.id.bgImage);

        swEnvironment = (Switch) findViewById(R.id.sw_environment);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(view -> attemptLogin());
        if (getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE).getBoolean(DEVELOPER_MODE, false)) {
            swEnvironment.setVisibility(View.VISIBLE);
        }
        mLoginFormView = findViewById(R.id.email_login_form);
        mProgressView = findViewById(R.id.login_progress);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
        if (mAuthTask != null) {
            mAuthTask.cancel(true);
        }
    }

    protected boolean isProd() {
        return (swEnvironment.getVisibility() == View.VISIBLE) && (!swEnvironment.isChecked()) ? false : true;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        if (email.equals(DEVELOPER_LOGIN) && password.equals(DEVELOPER_PSWRD)) {
            SharedPreferences.Editor editor = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE).edit();
            editor.putBoolean(DEVELOPER_MODE, true);
            editor.commit();
            swEnvironment.setVisibility(View.VISIBLE);
            mEmailView.getText().clear();
            mPasswordView.getText().clear();
            return;
        }

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.Error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.Error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isUsernameValid(email)) {
            mEmailView.setError(getString(R.string.Error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mAuthTask = new UserLoginTask(email, password, swEnvironment.isChecked());
            mAuthTask.execute((Void) null);
        }
    }

    protected boolean isEmailValid(String email) {

        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    protected boolean isUsernameValid(String email) {
        return email.length() > 0;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    @SuppressWarnings("squid:S2176")
    public class UserLoginTask extends SessionBusiness.UserLoginTask {

        public UserLoginTask(String email, String password, boolean environment) {
            super(email, password, environment);
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(final String authenticationToken) {
            Log.d(TAG, "onPostExecute: ");
            mAuthTask = null;
            showProgress(false);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (isDestroyed()) {
                    return;
                }
            } else {
                if (isDestroyed) {
                    return;
                }
            }

            if (authenticationToken != null) {
                KindieDaysApp.getApp().setStaging(!swEnvironment.isChecked());
                KindieDaysApp.getApp().initStarflightClient();
                setResult(RESULT_OK);
                finish();
            } else if (exception != null) {
                Log.d(TAG, "onPostExecute: exception != null ");
                if (exception.isLocalizedMessage()) {
                    Log.d(TAG, "onPostExecute: exception.isLocalizedMessage() ");

                    Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    if (exception.getStatusCode() == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                        Log.d(TAG, "onPostExecute: HttpStatus.SC_UNPROCESSABLE_ENTITY ");

                        try {
                            JSONArray errors = exception.getPayload();
                            for (int i = 0; i < errors.length(); i++) {
                                if ("email".equals(errors.getJSONObject(i).getString("property"))) {
                                    mEmailView.setError(errors.getJSONObject(i).getString("text"));
                                }
                                if ("password".equals(errors.getJSONObject(i).getString("property"))) {
                                    mPasswordView.setError(errors.getJSONObject(i).getString("text"));
                                }
                            }
                        } catch (Exception e) {
                            Log.e(LoginActivity.class.getName(), "Unable to parse exception payload", e);
                        }

                    }
                } else {
                    //Internal application error
                    Toast.makeText(LoginActivity.this, "Internal application error", Toast.LENGTH_LONG).show();
                }
            } else {
                if (isOnline(LoginActivity.this)) {
                    DialogFragment newFragment = new WrongLoginDialog();
                    newFragment.show(getFragmentManager(), WrongLoginDialog.TAG);
                }

            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}