package com.kindiedays.common.ui.personselection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.ChildTouchListener;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by pleonard on 13/11/2015.
 */
public abstract class PeopleSelectionFragment extends Fragment implements Observer, ChildTouchListener.OnChildButtonTouchListener {

    protected RecyclerView recyclerView;
    protected PersonButtonAdapter personAdapter;

    @Override
    public void onChildButtonTapped(View childView){
        if (childView != null && childView.getTag() instanceof Person) {
            Person person = (Person) childView.getTag();
            ((PersonSelectionActivity) getActivity()).onPersonClick(person);
        }
    }

    @Override
    public void onChildButtonLongPress(View childView){
        if(childView!= null && childView.getTag() instanceof Person){
            Person person = (Person) childView.getTag();
            ((PersonSelectionActivity) getActivity()).onPersonLongClick(person);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_recyclerview, null);
        recyclerView = (RecyclerView) v.findViewById(R.id.childrenList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        personAdapter = initPeopleAdapter();
        return v;
    }

    protected abstract PersonButtonAdapter initPeopleAdapter();

    @Override
    public void update(Observable observable, Object data) {
        personAdapter.notifyDataSetChanged();
    }
}
