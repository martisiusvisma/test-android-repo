package com.kindiedays.common.ui.picturegallery;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.kindiedays.common.business.JournalBusiness;

/**
 * Created by pleonard on 15/07/2015.
 */
public class JournalPictureGalleryAdapter extends AbstractPictureGalleryAdapter {

    public JournalPictureGalleryAdapter(FragmentActivity activity, long journalPostId) {
        super(activity);
        pictures = JournalBusiness.getInstance().getJournalPostPictures(journalPostId);
    }

    @Override
    public Fragment getItem(int position) {
        JournalPostPictureFragment pictureFragment = new JournalPostPictureFragment();
        pictureFragment.setPicture(pictures.get(position));
        return pictureFragment;
    }
}
