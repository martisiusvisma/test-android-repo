package com.kindiedays.common.ui.picturegallery;


import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kindiedays.common.pojo.Attachment;

import java.util.List;

public class MessagePhotoGalleryAdapter extends FragmentStatePagerAdapter {
    protected List<Attachment> attachments;

    public MessagePhotoGalleryAdapter(FragmentActivity activity, List<Attachment> attachments) {
        super(activity.getSupportFragmentManager());
        this.attachments = attachments;
    }

    @Override
    public MessagePhotoFragment getItem(int position) {
        return MessagePhotoFragment.newInstance(attachments.get(position));
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return attachments != null ? attachments.size() : 0;
    }
}
