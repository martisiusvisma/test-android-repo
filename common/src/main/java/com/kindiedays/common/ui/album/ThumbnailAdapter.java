package com.kindiedays.common.ui.album;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.kindiedays.common.R;
import com.kindiedays.common.pojo.GalleryPicture;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 14/07/2015.
 */
public class ThumbnailAdapter extends BaseAdapter{
    public final static int PICTURES_NUMBER = 9;

    private List<GalleryPicture> pictures;
    private Activity activity;
    private String blurringChild;
    private List<String> selectedPicturesHashes = new ArrayList<>();
    private Set<String> picturesUrlSet = new HashSet<>();

    public ThumbnailAdapter(Activity activity){
        this.activity = activity;
    }

    public ThumbnailAdapter(Activity activity, String blurringChild) {
        this(activity);
        this.blurringChild = blurringChild;
    }

    public List<GalleryPicture> getPictures(){
        return pictures;
    }

    public void setPictures(List<GalleryPicture> pictures){
        this.pictures = pictures;
        this.notifyDataSetChanged();
    }

    public boolean isReadyToLoadMore(){
        Log.e("1", " " + pictures.size() + "==" + picturesUrlSet.size());
        return pictures != null && (pictures.size() >= picturesUrlSet.size());
    }

    @Override
    public int getCount() {
        if(pictures == null) {
            return 0;
        }
        return pictures.size();
    }

    @Override
    public GalleryPicture getItem(int position) {
        return pictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        }
        else {
            holder = new ViewHolder();
            convertView = activity.getLayoutInflater().inflate(R.layout.item_thumbnail, null);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            holder.imageView = (SquareThumbnailItemView) convertView.findViewById(R.id.thumbnail);
            convertView.setTag(holder);
        }

        GalleryPicture picture = getItem(position);
        String imageParam = blurringChild != null ? blurringChild : STRING_EMPTY;
        String url = picture.getUrl() + "&preferredWidth=300&preferredHeight=300" + imageParam;
        //String url = picture.getUrl() + "&" + imageParam;
        Glide.with(activity).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                picturesUrlSet.add(s);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                picturesUrlSet.add(s);
                return false;
            }
        }).into(new GlideDrawableImageViewTarget(holder.imageView) {
            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                super.onResourceReady(drawable, anim);
                picturesUrlSet.add(url);
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                picturesUrlSet.add(url);
                Log.d(String.valueOf(ThumbnailAdapter.this.getClass()), e.getMessage());
                holder.progressBar.setVisibility(View.GONE);
            }
        });

        if(selectedPicturesHashes.contains(picture.getHash())){
            holder.imageView.setColorFilter(activity.getResources().getColor(R.color.selectedfilter));
        }
        else{
            holder.imageView.clearColorFilter();
        }
        return convertView;
    }

    public List<String> getSelectedPicturesHashes() {
        return selectedPicturesHashes;
    }

    public void setSelectedPicturesHashes(ArrayList<String> selectedPicturesHashes) {
        this.selectedPicturesHashes = selectedPicturesHashes;
    }

    static class ViewHolder{
        ProgressBar progressBar;
        SquareThumbnailItemView imageView;
    }
}
