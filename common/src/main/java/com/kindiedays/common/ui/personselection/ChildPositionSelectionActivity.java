package com.kindiedays.common.ui.personselection;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.R;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.utils.BlurUtils;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by pleonard on 30/09/2015.
 */
public abstract class ChildPositionSelectionActivity extends KindieDaysActivity {

    protected SurfaceView mSurfaceView;
    protected RatioImageView mPictureView;
    protected Bitmap srcBitmap;

    protected int originalWidth;
    protected int originalHeight;
    protected Rect rectangleBox = null;
    protected View.OnTouchListener surfaceTouchListener;
    protected final AtomicReference<Point> rectBoxFirstCorner = new AtomicReference<>();

    protected final Paint rectStrokePaint = new Paint();
    protected final Paint rectFillPaint = new Paint();
    protected String picturePath;

    public static final String PATH_OF_PICTURE = "pathOfPicture";
    public static final String X = "x";
    public static final String Y = "y";
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String IMAGE_WIDTH = "image_width";
    public static final String IMAGE_HEIGHT = "image_height";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_child_position);
        originalHeight = getIntent().getIntExtra(HEIGHT, 0);
        originalWidth = getIntent().getIntExtra(WIDTH, 0);

        mPictureView = (RatioImageView) findViewById((R.id.pictureView));
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);

        mSurfaceView.setOnTouchListener(surfaceTouchListener);
        mSurfaceView.setZOrderOnTop(true);    // necessary
        SurfaceHolder sfhTrackHolder = mSurfaceView.getHolder();
        sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);

        rectStrokePaint.setColor(getResources().getColor(R.color.kindieblue));
        rectStrokePaint.setStrokeWidth(5);
        rectStrokePaint.setStyle(Paint.Style.STROKE);

        rectFillPaint.setColor(getResources().getColor(R.color.half_transparent_kindieblue));
        rectFillPaint.setStyle(Paint.Style.FILL);
        picturePath = getIntent().getStringExtra(PATH_OF_PICTURE);

        try {
            mPictureView.setImageBitmap(this, Uri.parse(picturePath), originalWidth, originalHeight, new GlideDrawableImageViewTarget(mPictureView) {
                @Override
                public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                    super.onResourceReady(drawable, anim);
                    mSurfaceView.setVisibility(View.VISIBLE);
                    mPictureView.post(() -> srcBitmap = BlurUtils.loadBitmapFromView(mPictureView));
                }
            });
        }
        catch(Exception e){
            Log.e(ChildPositionSelectionActivity.class.getName(), e.getMessage(), e);
            finish();
        }
    }
}
