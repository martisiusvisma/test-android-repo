package com.kindiedays.common.ui.messages;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Message;
import com.kindiedays.common.utils.FormatUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pleonard on 20/07/2015.
 */
@SuppressWarnings("squid:S1226")
public abstract class MessageAdapter extends BaseAdapter {
    private static final int TYPE_MESSAGE = 0;
    public static final int TYPE_PHOTO = 1;
    public static final int TYPE_OTHER_MEDIA = 2;
    public static final int TYPE_VIDEO = 3;
    private static final int TYPES_COUNT = 4;

    private List<Object> items;
    private List<Message> messages;
    private Map<Integer, ConversationParty> conversationParties;
    private OnMediaClickListener listener;
    private DisplayMetrics metrics;

    public MessageAdapter(Activity activity, OnMediaClickListener listener) {
        this.listener = listener;
        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @Override
    public int getCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object data = items.get(position);
        if (data instanceof Message) {
            return TYPE_MESSAGE;
        } else if (data instanceof Attachment) {
            Attachment attachment = (Attachment) data;
            return FormatUtils.getFormatType(attachment.getFormat());
        }

        return TYPE_OTHER_MEDIA;
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return TYPES_COUNT;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        switch (type) {
        case TYPE_MESSAGE:
            return onBindViewHolder(convertView, (Message) items.get(position), parent);
        case TYPE_PHOTO:
            return onBindPhotoViewHolder(convertView, (Attachment) items.get(position), parent);
        case TYPE_VIDEO:
            return onBindVideoHolder(convertView, (Attachment) items.get(position), parent);
        default:
            return onBindMediaViewHolder(convertView, (Attachment) items.get(position), parent);
        }
    }

    protected abstract boolean isConversationPartyCurrentUser(ConversationParty party);

    public void setContent(List<Message> messages, Set<ConversationParty> conversationParties) {
        parseMessages(messages);
        Map<Integer, ConversationParty> conversationPartyMap = new HashMap<>();
        for (ConversationParty conversationParty : conversationParties) {
            conversationPartyMap.put(conversationParty.getIndex(), conversationParty);
        }
        this.conversationParties = conversationPartyMap;
        this.notifyDataSetChanged();
    }

    public void setMessages(List<Message> messages) {
        parseMessages(messages);
        this.notifyDataSetChanged();
    }

    private void parseMessages(List<Message> messages) {
        this.items = new ArrayList<>();
        this.messages = messages;
        for (Message message : this.messages) {
            List<Attachment> attachments = message.getAttachmentList();
            String contentMsg = KindieDaysApp.getApp().getString(R.string.Content);
            if (attachments != null && !attachments.isEmpty()
            && contentMsg.equals(message.getText())) {
                message.setText("");
            }
            this.items.add(message);
            for (Attachment attachment : attachments) {
                attachment.setAuthorIndex(message.getAuthorIndex());
                attachment.setMessageIndex(message.getIndex());
                this.items.add(attachment);
            }
        }
    }

    private View onBindViewHolder(View convertView, Message message, ViewGroup parent) {
        MessageViewHolder holder;
        if (convertView != null) {
            holder = (MessageViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, null);
            holder = new MessageViewHolder(convertView);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.message);
            holder.tvDate = (TextView) convertView.findViewById(R.id.dateTime);
            convertView.setTag(holder);
        }

        holder.bind(message);
        return convertView;
    }

    private View onBindVideoHolder(View convertView, Attachment attachment, ViewGroup parent) {
        VideoViewHolder holder;
        if (convertView != null) {
            holder = (VideoViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_video, null);
            holder = new VideoViewHolder(convertView);
            holder.ivPhoto = (ImageView) convertView.findViewById(R.id.iv_photo);
            holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_play_icon);
            convertView.setTag(holder);
        }

        holder.bind(attachment);
        return convertView;
    }

    private View onBindPhotoViewHolder(View convertView, Attachment attachment, ViewGroup parent) {
        PhotoViewHolder holder;
        if (convertView != null) {
            holder = (PhotoViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_photo, null);
            holder = new PhotoViewHolder(convertView);
            holder.ivPhoto = (ImageView) convertView.findViewById(R.id.iv_photo);
            convertView.setTag(holder);
        }

        holder.bind(attachment);
        return convertView;
    }

    private View onBindMediaViewHolder(View convertView, Attachment attachment, ViewGroup parent) {
        MediaViewHolder holder;
        if (convertView != null) {
            holder = (MediaViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_media, parent, false);
            holder = new MediaViewHolder(convertView);
            holder.tvText = (TextView) convertView.findViewById(R.id.tv_media_link);
            holder.tvFormat = (TextView) convertView.findViewById(R.id.tv_format);
            holder.llContent = (LinearLayout) convertView.findViewById(R.id.ll_content);
            convertView.setTag(holder);
        }

        holder.bind(attachment);
        return convertView;
    }


    private class MessageViewHolder implements View.OnLongClickListener {
        View itemView;
        TextView tvMessage;
        TextView tvDate;

        Message message;

        MessageViewHolder(View itemView) {
            this.itemView = itemView;
            this.itemView.setOnLongClickListener(this);
        }

        private void bind(Message message) {
            this.message = message;
            ConversationParty conversationParty = conversationParties.get(message.getAuthorIndex());
            String text = message.getText();
            tvMessage.setText(text);
            tvMessage.setVisibility(TextUtils.isEmpty(text) ? View.GONE : View.VISIBLE);
            tvDate.setText(getPrettyFormattedDate(message.getCreated()));//.toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toString("dd/MM/yyyy HH:mm"));
            if (isConversationPartyCurrentUser(conversationParty)) {
                tvMessage.setBackgroundResource(R.drawable.speech_bubble_green);
                tvMessage.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));
                ((LinearLayout.LayoutParams) tvMessage.getLayoutParams()).gravity = Gravity.RIGHT;
            } else {
                tvMessage.setBackgroundResource(R.drawable.speech_bubble_orange);
                tvMessage.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.black));
                ((LinearLayout.LayoutParams) tvMessage.getLayoutParams()).gravity = Gravity.LEFT;
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (listener != null) {
                listener.onTextLongClick(message);
                return true;
            }
            return false;
        }
    }

    private String getPrettyFormattedDate(DateTime massageDate) {
        long dateInMillis = massageDate.toDateTime(DateTimeZone.UTC).toDate().getTime();
        DateTimeFormatter builder = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
        return builder.print(dateInMillis);
    }


    private class PhotoViewHolder implements View.OnClickListener {
        View itemView;
        ImageView ivPhoto;

        private Attachment attachment;

        PhotoViewHolder(View itemView) {
            this.itemView = itemView;
            this.itemView.setOnClickListener(this);
        }

        private void bind(Attachment attachment) {
            this.attachment = attachment;
            ConversationParty conversationParty = conversationParties.get(attachment.getAuthorIndex());
            ((LinearLayout.LayoutParams) ivPhoto.getLayoutParams()).gravity = isConversationPartyCurrentUser(conversationParty)
                    ? Gravity.RIGHT
                    : Gravity.LEFT;
            ivPhoto.getLayoutParams().width = getNewDimens(metrics.widthPixels);
            Glide.with(itemView.getContext()).load(attachment.getUri()).into(ivPhoto);
        }

        private int getNewDimens(int pixels) {
            return (pixels / 3) * 2;
        }

        //DON'T DELETE!!
        @SuppressWarnings("squid:UnusedPrivateMethod")
        private Message findMessageContainer(Attachment attachment) {
            int messageCount = messages.size();
            for (int i = messageCount - 1; i >= 0; i--) {
                Message message = messages.get(i);
                if (attachment.getAuthorIndex() == message.getIndex()) {
                    return message;
                }
            }

            return null;
        }

        @Override
        public void onClick(View v) {
            //findMessageContainer(attachment);  DON'T DELETE
            if (listener != null) {
                listener.onPhotoClick(attachment);
            }
        }
    }


    private class VideoViewHolder implements View.OnClickListener {
        View itemView;
        ImageView ivPhoto;
        ImageView ivIcon;

        private Attachment attachment;

        VideoViewHolder(View itemView) {
            this.itemView = itemView;
            this.itemView.setOnClickListener(this);
        }

        private void bind(Attachment attachment) {
            this.attachment = attachment;
            ConversationParty conversationParty = conversationParties.get(attachment.getAuthorIndex());
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivPhoto.getLayoutParams();
            int verb = isConversationPartyCurrentUser(conversationParty)
                    ? RelativeLayout.ALIGN_PARENT_RIGHT
                    : RelativeLayout.ALIGN_PARENT_LEFT;
            layoutParams.addRule(verb);
            ivPhoto.setLayoutParams(layoutParams);
            setVideoBitmap(ivPhoto, ivIcon, attachment.getUri());
        }

        @SuppressLint("StaticFieldLeak")
        private void setVideoBitmap(final ImageView iv, final ImageView ivIcon, final String path) {
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    iv.setImageResource(R.drawable.gallery_placeholder);
                }

                @Override
                protected Bitmap doInBackground(Void... params) {
                    Bitmap bitmap = null;
                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    try {
                        retriever.setDataSource(path, new HashMap<>());
                        bitmap = retriever.getFrameAtTime();
                        retriever.release();
                    } catch (Exception e) {
                        Log.e("MessageAdapter", "setVideoBitmap", e);
                    }
                    return bitmap;
                }

                @Override
                protected void onPostExecute(Bitmap result) {
                    if (result != null) {
                        iv.setImageBitmap(result);
                        ivIcon.setVisibility(View.VISIBLE);
                    }
                }
            }.execute();
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onMediaClick(attachment);
            }
        }
    }


    private class MediaViewHolder implements View.OnClickListener {
        View itemView;
        TextView tvText;
        TextView tvFormat;
        LinearLayout llContent;

        private Attachment attachment;

        MediaViewHolder(View itemView) {
            this.itemView = itemView;
            this.itemView.setOnClickListener(this);
        }

        private void bind(Attachment attachment) {
            this.attachment = attachment;
            ConversationParty conversationParty = conversationParties.get(attachment.getAuthorIndex());
            tvText.setText(attachment.getFilename());
            tvFormat.setText(attachment.getFormat());
            if (isConversationPartyCurrentUser(conversationParty)) {
                llContent.setBackgroundResource(R.drawable.speech_bubble_green);
                tvText.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.white));
                ((LinearLayout.LayoutParams) llContent.getLayoutParams()).gravity = Gravity.RIGHT;
            } else {
                llContent.setBackgroundResource(R.drawable.speech_bubble_orange);
                tvText.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.black));
                ((LinearLayout.LayoutParams) llContent.getLayoutParams()).gravity = Gravity.LEFT;
            }
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onMediaClick(attachment);
            }
        }
    }

    public interface OnMediaClickListener {
        void onMediaClick(Attachment attachment);
        void onTextLongClick(Message message);
        void onPhotoClick(Attachment attachment);
    }
}
