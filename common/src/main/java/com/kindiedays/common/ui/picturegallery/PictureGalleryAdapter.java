package com.kindiedays.common.ui.picturegallery;

import android.support.v4.app.FragmentActivity;

import com.kindiedays.common.business.PicturesBusiness;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 15/07/2015.
 */
public class PictureGalleryAdapter extends AbstractPictureGalleryAdapter {
    protected String blurringChild;

    public PictureGalleryAdapter(FragmentActivity activity) {
        super(activity);
    }

    public PictureGalleryAdapter(FragmentActivity activity, String blurringChild) {
        this(activity);
        this.blurringChild = blurringChild;
    }

    @Override
    public void notifyDataSetChanged() {
        pictures = PicturesBusiness.getInstance().getCurrentPictureList().getPictures();
        super.notifyDataSetChanged();
    }

    @Override
    public GalleryPictureFragment getItem(int position) {
        GalleryPictureFragment pictureFragment = new GalleryPictureFragment();
        String imageParams = blurringChild != null ? blurringChild : STRING_EMPTY;
        String url = pictures.get(position).getUrl() + imageParams;
        pictures.get(position).setUrl(url);
        pictureFragment.setPicture(pictures.get(position));
        return pictureFragment;
    }
}
