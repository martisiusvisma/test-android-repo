package com.kindiedays.common.ui;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.pojo.Place;

/**
 * Created by pleonard on 30/10/2015.
 */

@SuppressWarnings("squid:S1151")
public class ChildTouchListener extends RecyclerView.SimpleOnItemTouchListener {

    private static final long LONG_PRESS_DURATION = 250;
    private static final long SHORT_TAP_DURATION = 25;
    private static final float SCROLL_THRESHOLD = 5;

    private OnChildButtonTouchListener gestureListener;

    Handler handler = new Handler();
    EventRunnable run = new EventRunnable();

    public ChildTouchListener(OnChildButtonTouchListener gestureListener) {
        this.gestureListener = gestureListener;
    }

    public interface OnChildButtonTouchListener {
        void onChildButtonTapped(View childButton);

        void onChildButtonLongPress(View childButton);
    }


    class EventRunnable implements Runnable {
        private View childButton;

        public void setChildButton(View childButton) {
            this.childButton = childButton;
        }

        @Override
        public void run() {
            // Your code to run on long click
            gestureListener.onChildButtonLongPress(childButton);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View v = getButtonView(view, e);
        switch (e.getAction()) {
        case MotionEvent.ACTION_DOWN:
            run.setChildButton(getButtonView(view, e));
            handler.postDelayed(run, LONG_PRESS_DURATION/* OR the amount of time you want */);
            if (v != null) {
                v.setPressed(true);
            }
            break;

        case MotionEvent.ACTION_MOVE:
            if (e.getHistorySize() > 0 && (Math.abs(e.getHistoricalX(0) - e.getX()) > SCROLL_THRESHOLD || Math.abs(e.getHistoricalY(0) - e.getY()) > SCROLL_THRESHOLD)) {
                handler.removeCallbacks(run);
                if (v != null) {
                    v.setPressed(false);
                }
            }
            break;

        case MotionEvent.ACTION_SCROLL:
            handler.removeCallbacks(run);
            if (v != null) {
                v.setPressed(false);
            }
            break;

        case MotionEvent.ACTION_UP:
            handler.removeCallbacks(run);
            v = getButtonView(view, e);
            if (v != null) {
                Object tag = v.getTag();
                if (v.isPressed() &&
                        e.getEventTime() - e.getDownTime() < LONG_PRESS_DURATION &&
                        e.getEventTime() - e.getDownTime() > SHORT_TAP_DURATION &&
                        (tag instanceof Group || tag instanceof Place || tag instanceof Person)) {
                    gestureListener.onChildButtonTapped(v);
                }
                v.setPressed(false);
            }
            break;

        default:
            break;
        }
        return false;
    }

    private View getButtonView(RecyclerView view, MotionEvent e) {
        return view.findChildViewUnder(e.getX(), e.getY());
    }

}
