package com.kindiedays.common.ui.picturegallery;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.kindiedays.common.R;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.common.ui.KindieDaysActivity;

import java.util.List;

/**
 * Created by pleonard on 15/07/2015.
 */
public class PictureGalleryActivity extends KindieDaysActivity {

    protected ViewPager viewPager;
    protected AbstractPictureGalleryAdapter pictureGalleryAdapter;
    public static final String JOURNAL_PICTURE_MODE = "journalPictureMode";
    public static final String JOURNAL_POST_ID = "journalPostId";
    private boolean initCompleted = false;
    protected boolean journalMode;
    protected String blurringChild;
    protected List<GalleryPicture> pictures;

    public static final String PICTURE_POSITION = "picturePosition";
    public static final String GALLERY_PICTURE_ID = "gallery_pic_id";
    public static final String PICTURE_PARAMS = "extra_picture_params";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initCompleted = false;
        setContentView(R.layout.activity_picture_gallery);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.pictureViewPager);

        PictureList pictureList = PicturesBusiness.getInstance().getCurrentPictureList();
        if (pictureList == null) {
            relaunchActivity();
            return;
        }
        pictures = pictureList.getPictures();
        if (pictures == null || pictures.isEmpty()) {
            relaunchActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        journalMode = getIntent().getBooleanExtra(JOURNAL_PICTURE_MODE, false);
        if (initCompleted) {
            int selectedItem = viewPager.getCurrentItem();
            pictureGalleryAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(selectedItem);
        } else {
            blurringChild = getIntent().getStringExtra(PICTURE_PARAMS);
            initAdapter();
            long picId = getIntent().getLongExtra(GALLERY_PICTURE_ID, 0L);
            int picturePosition = getIntent().getIntExtra(PICTURE_POSITION, 0);
            picturePosition = picId != 0L ? findPicturePositionById(picId) : picturePosition;
            viewPager.setAdapter(pictureGalleryAdapter);
            viewPager.setCurrentItem(picturePosition);
            initCompleted = true;
        }
    }

    private int findPicturePositionById(long picId) {
        List<GalleryPicture> pictures = PicturesBusiness.getInstance().getCurrentPictureList().getPictures();
        for (int i = 0; i < pictures.size(); i++) {
            GalleryPicture galleryPicture = pictures.get(i);
            if (galleryPicture.getChildTags().get(0).getPictureId() == picId) {
                return i;
            }
        }
        return 0;
    }

    protected void initAdapter() {
        if (journalMode) {
            pictureGalleryAdapter = new JournalPictureGalleryAdapter(this, getIntent().getLongExtra(JOURNAL_POST_ID, 0));
        } else {
            pictureGalleryAdapter = new PictureGalleryAdapter(this, blurringChild);
            pictureGalleryAdapter.setPictures(pictures);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }

}
