package com.kindiedays.common.ui.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.kindiedays.common.R;

/**
 * @author Eugeniy Shein on 06/12/2017.
 * e.shein@andersenlab.com
 * Last edit by Eugeniy Shein on 06/12/2017
 */
public class WrongLoginDialog extends DialogFragment {
    public static final String TAG = WrongLoginDialog.class.getSimpleName();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_login, null);
        view.findViewById(R.id.ok_button).setOnClickListener(v -> WrongLoginDialog.this.dismiss());
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
