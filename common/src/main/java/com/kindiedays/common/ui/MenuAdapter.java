package com.kindiedays.common.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.components.CheckedTextViewCustom;
import com.kindiedays.common.utils.PictureUtils;


/**
 * Created by pleonard on 23/07/2015.
 */
public class MenuAdapter extends ArrayAdapter<Integer> {

    private Integer[] iconArray;
    private Integer[] iconSelectedArray;
    private Integer[] counterArray;
    private Boolean[] enabled;
    private static final Integer disabledMenuIconId = R.raw.iconmonstr_lock;

    public MenuAdapter(Context context, int resource, Integer[] labelIds, Integer[] iconsIds, Integer[] selectedIconsIds, Integer[] counterIds, Boolean[] enabled) {
        super(context, resource, labelIds);
        this.iconArray = iconsIds;
        this.counterArray = counterIds;
        this.iconSelectedArray = selectedIconsIds;
        this.enabled = enabled;
    }

    public void setEnabled(boolean enabled, int position) {
        if(this.enabled[position] != enabled){
            //Only do something if new value != old value
            this.enabled[position] = enabled;
            notifyDataSetChanged();
        }
    }

    public void setEnabled(Boolean[] enabled) {
        this.enabled = enabled;
        notifyDataSetChanged();
    }

    public boolean isMenuItemEnabled(int position){
        return enabled[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View main;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null) {
            main = inflater.inflate(R.layout.item_menu, null);
        } else {
            main = convertView;
        }

        CheckedTextViewCustom title = (CheckedTextViewCustom) main.findViewById(R.id.textitem);
        title.setText(parent.getContext().getString(getItem(position)));

        int unreadNotification = counterArray[position];
        TextView counter = (TextView) main.findViewById(R.id.counteritem);

        Drawable imageDrawable;
        if(enabled[position]){
            if (unreadNotification != 0){
                counter.setVisibility(View.VISIBLE);
                counter.setText(String.valueOf(unreadNotification));
            } else {
                counter.setVisibility(View.GONE);
            }
            imageDrawable = PictureUtils.createStateDrawableFromSvg(getContext().getResources(), iconSelectedArray[position], iconArray[position]);
        }
        else{
            counter.setVisibility(View.GONE);
            imageDrawable = PictureUtils.createStateDrawableFromSvg(getContext().getResources(), disabledMenuIconId, disabledMenuIconId);
        }
        int pixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getContext().getResources().getDisplayMetrics());
        imageDrawable.setBounds(0,0, pixels, pixels);
        title.setCompoundDrawables(imageDrawable, null,null, null);

        title.setEnabled(enabled[position]);
        return main;
    }
}
