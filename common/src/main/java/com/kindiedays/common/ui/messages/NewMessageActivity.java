package com.kindiedays.common.ui.messages;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kindiedays.common.R;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.kindiedays.common.utils.constants.CommonConstants.EXTRA_GALLERY;
import static com.kindiedays.common.utils.constants.CommonConstants.MAX_UPLOAD_SIZE_KB;
import static com.kindiedays.common.utils.constants.CommonConstants.MB_SIZE;
import static com.kindiedays.common.utils.constants.CommonConstants.PDF_MIME_TYPE;
import static com.kindiedays.common.utils.constants.CommonConstants.PICK_IMAGE_MULTIPLE;
import static com.kindiedays.common.utils.constants.CommonConstants.PICK_PDF_SINGLE;
import static com.kindiedays.common.utils.constants.CommonConstants.SAMSUNG_ACTION_GET_CONTENT;
import static com.kindiedays.common.utils.constants.CommonConstants.SAMSUNG_CONTENT_TYPE;
import static com.kindiedays.common.utils.constants.CommonConstants.SAMSUNG_MODEL;

/**
 * Created by pleonard on 14/01/2016.
 */
public abstract class NewMessageActivity extends KindieDaysActivity implements AttachmentDialog.OnButtonClickListener {

    public static final String NEW_CONVERSATIONS_CREATED = "NEW_CONVERSATIONS_CREATED";

    protected static final int RECIPIENT_SELECTION_TAG = 1;
    private static final int REQUEST_FROM_DIALOG = 10;

    private MenuItem btnSendMessage;
    private EditText titleEditText;
    private EditText messageEditText;
    private ImageView ivAddAttach;
    private LinearLayout llContent;
    protected TextView recipientsTextView;
    protected long[] selectedTeachersId;

    protected View.OnClickListener onSelectPeopleClickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        Button choosePeopleButton = (Button) findViewById(R.id.peopleSelectionButton);
        titleEditText = (EditText) findViewById(R.id.titleEdit);
        messageEditText = (EditText) findViewById(R.id.bodyEdit);
        recipientsTextView = (TextView) findViewById(R.id.recipientsTextView);
        ivAddAttach = (ImageView) findViewById(R.id.iv_add_attachment);
        llContent = (LinearLayout) findViewById(R.id.ll_content);
        ivAddAttach.setEnabled(false);
        choosePeopleButton.setOnClickListener(onSelectPeopleClickListener);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        CustomTextWatcher textWatcher = new CustomTextWatcher();
        titleEditText.addTextChangedListener(textWatcher);
        choosePeopleButton.performClick();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_message, menu);
        btnSendMessage = menu.findItem(R.id.action_sendNewMessage);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        if (item.getItemId() == R.id.action_sendNewMessage) {
            if (isFieldsFilled()) {
                btnSendMessage.setEnabled(false);
                sendNewMessage(titleEditText.getText().toString(), messageEditText.getText().toString(),
                        Collections.emptyList(), false);
            } else {
                showSnackbar(R.string.new_conversation_requirement);
            }
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    protected abstract void sendNewMessage(String subject, String message, List<File> attachments, boolean isDelete);

    @Override
    public void addPhotos() {
        Intent intent = new Intent(NewMessageActivity.this, CustomPhotoGalleryActivity.class);
        startActivityForResult(intent, PICK_IMAGE_MULTIPLE);
    }

    @Override
    public void addDocuments() {
        Intent intent;
        if (Build.MANUFACTURER.equalsIgnoreCase(SAMSUNG_MODEL)) {
            intent = new Intent(SAMSUNG_ACTION_GET_CONTENT);
            intent.putExtra(SAMSUNG_CONTENT_TYPE, PDF_MIME_TYPE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType(PDF_MIME_TYPE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        }
        startActivityForResult(intent, PICK_PDF_SINGLE);
    }

    @Override
    public void onDismiss() {
        //do nothing
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_MULTIPLE) {
                String extraGallery = data.getStringExtra(EXTRA_GALLERY);

                if (extraGallery != null && !extraGallery.isEmpty()) {
                    List<File> attachments = new ArrayList<>();
                    String[] imagesPath = extraGallery.split("\\|");
                    for (String path : imagesPath) {
                        attachments.add(new File(path));
                    }
                    sendNewMessage(titleEditText.getText().toString(), messageEditText.getText().toString(),
                            attachments, false);
                }
            } else if (requestCode == PICK_PDF_SINGLE) {
                File file;
                if (FileUtils.isGogleDriveDocument(data.getData())) {
                    DownloadPdfAsyncTask task = new DownloadPdfAsyncTask();
                    getPendingTasks().add(task);
                    task.execute(data);
                } else {
                    String path = FileUtils.getFilePath(NewMessageActivity.this, data.getData());
                    file = new File(path);
                    if (isValidSize(file)) {
                        sendNewMessage(titleEditText.getText().toString(), messageEditText.getText().toString(),
                                Collections.singletonList(file), false);
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && requestCode == REQUEST_FROM_DIALOG) {
            AttachmentDialog.newInstance().show(getSupportFragmentManager());
        }
    }

    public void openAttachmentDialog(View view) {
        if (isFieldsFilled()) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_FROM_DIALOG);
            } else {
                AttachmentDialog.newInstance().show(getSupportFragmentManager());
            }
        } else {
            showSnackbar(R.string.new_conversation_requirement);
        }
    }

    private void showSnackbar(@StringRes int stringRes) {
        TSnackbar snackbar = TSnackbar.make(llContent, getString(stringRes),
                TSnackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    protected boolean isFieldsFilled() {
        if (!TextUtils.isEmpty(titleEditText.getText()) && !TextUtils.isEmpty(recipientsTextView.getText())) {
            ivAddAttach.setImageResource(R.drawable.plus_active);
            ivAddAttach.setEnabled(true);
            return true;
        } else {
            ivAddAttach.setImageResource(R.drawable.plus_inactive);
            ivAddAttach.setEnabled(true);
            return false;
        }
    }

    private boolean isValidSize(File file) {
        if (file.length() / MB_SIZE > MAX_UPLOAD_SIZE_KB) {
            showSnackbar(R.string.gallery_file_too_large);
            return false;
        }
        return true;
    }

    private class CustomTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //non needed
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //non needed
        }

        @Override
        public void afterTextChanged(Editable s) {
            isFieldsFilled();
        }
    }


    private class DownloadPdfAsyncTask extends AsyncTask<Intent, Void, File> {

        @Override
        protected File doInBackground(Intent... params) {
            return FileUtils.getFileFromGoogleDrive(NewMessageActivity.this, params[0]);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (file != null && isValidSize(file)) {
                sendNewMessage(titleEditText.getText().toString(), messageEditText.getText().toString(),
                        Collections.singletonList(file), true);
            }
        }
    }
}
