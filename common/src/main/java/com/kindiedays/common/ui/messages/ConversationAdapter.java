package com.kindiedays.common.ui.messages;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationList;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.utils.GlideUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_COMMA;

/**
 * Created by pleonard on 29/05/2015.
 */
public abstract class ConversationAdapter extends BaseAdapter {
    private List<Conversation> conversations;
    private List<Child> children = new ArrayList<>(0);
    private Activity activity;

    public ConversationAdapter(Activity activity) {
        this.activity = activity;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(ConversationList conversationList) {
        if (conversationList != null) {
            this.conversations = new ArrayList<>(conversationList.getConversations());
        }
        this.notifyDataSetChanged();
    }

    public void setChildren(List<Child> children) {
        if (children != null) {
            this.children = children;
        }
        this.notifyDataSetChanged();
    }

    public void hideConversation(Conversation conversation) {
        this.conversations.remove(conversation);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (conversations == null) {
            return 0;
        }
        return conversations.size();
    }

    @Override
    public Conversation getItem(int position) {
        return conversations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.item_conversation, parent, false);
            holder = new ViewHolder();
            holder.lastDate = (TextView) convertView.findViewById(R.id.lastMessageDate);
            holder.subject = (TextView) convertView.findViewById(R.id.subject);
            holder.parties = (TextView) convertView.findViewById(R.id.parties);
            holder.partyPicture = (ImageView) convertView.findViewById(R.id.partyPicture);
            holder.rvChildren = (RecyclerView) convertView.findViewById(R.id.rv_children);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.bind(conversations.get(position));
        return convertView;
    }

    protected abstract boolean isConversationPartyCurrentUser(ConversationParty party, boolean isTeacherAsKindergarten);

    protected abstract boolean isTeacherAsKindergarten(Set<ConversationParty> conversationParties);

    private class ViewHolder {
        private TextView subject;
        private TextView lastDate;
        private TextView parties;
        private ImageView partyPicture;
        private RecyclerView rvChildren;

        void bind(Conversation conversation) {
            subject.setText(conversation.getSubject());
            DateTime lastMessageCreated = conversation.getLastMessageCreated();
            lastDate.setText(getPrettyFormattedDate(lastMessageCreated));
            StringBuilder partiesTest = new StringBuilder();

            boolean isTeacherAsKindergarten = isTeacherAsKindergarten(conversation.getParties());

            for (ConversationParty party : conversation.getParties()) {
                if (!isConversationPartyCurrentUser(party, isTeacherAsKindergarten)) {
                    partiesTest.append(party.getName());
                    partiesTest.append(STRING_COMMA);
                    String profileImageUrl = party.getProfilePictureUrl();
                    if (profileImageUrl != null) {
                        Glide.with(activity).load(profileImageUrl).bitmapTransform(GlideUtils.roundTransformation).into(partyPicture);
                    } else {
                        Glide.with(activity).load(R.drawable.peopleplaceholder).into(partyPicture);
                    }

                    rvChildren.setAdapter(new ChildrenAvatarsAdapter(getChildrenAvatarUrls(conversation.getChildren())));
                    rvChildren.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                }
            }

            //Removing trailing ,
            if (partiesTest.length() > 2) {
                partiesTest.delete(partiesTest.length() - 2, partiesTest.length() - 1);
            }

            parties.setText(partiesTest.toString());
            if (conversation.getUnreadMessages() > 0) {
                subject.setTypeface(null, Typeface.BOLD);
                lastDate.setTypeface(null, Typeface.BOLD);
                parties.setTypeface(null, Typeface.BOLD);
            } else {
                subject.setTypeface(null, Typeface.NORMAL);
                lastDate.setTypeface(null, Typeface.NORMAL);
                parties.setTypeface(null, Typeface.NORMAL);
            }
        }

        private List<String> getChildrenAvatarUrls(long[] ids) {
            List<String> urls = new ArrayList<>();
            if (ids == null){
                return urls;
            }
            for (long id : ids) {
                for (Child child : children) {
                    if (id == child.getId()) {
                        urls.add(child.getProfilePictureUrl());
                    }
                }
            }
            return urls;
        }

        private String getPrettyFormattedDate(DateTime massageDate) {
            LocalDate today = new LocalDate();
            LocalDate massageLocalDate = massageDate.toLocalDate();
            long dateInMillis = massageDate.toDateTime(DateTimeZone.UTC).toDate().getTime();

            if (massageLocalDate.equals(today)) {
                DateTimeFormatter builder = DateTimeFormat.forPattern("hh:mm a");
                return builder.print(dateInMillis);
            } else if (today.minusWeeks(1).compareTo(massageLocalDate) < 0) {
                return massageLocalDate.dayOfWeek().getAsShortText();
            } else {
                DateTimeFormatter builder = DateTimeFormat.forPattern("dd/MM/yy");
                return builder.print(dateInMillis);
            }
        }
    }

}
