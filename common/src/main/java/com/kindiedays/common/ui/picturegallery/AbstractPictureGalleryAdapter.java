package com.kindiedays.common.ui.picturegallery;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kindiedays.common.pojo.AbstractPicture;
import com.kindiedays.common.pojo.GalleryPicture;

import java.util.List;

/**
 * Created by pleonard on 04/12/2015.
 */
public abstract class AbstractPictureGalleryAdapter extends FragmentStatePagerAdapter {

    protected List<? extends AbstractPicture> pictures;

    public AbstractPictureGalleryAdapter(FragmentActivity activity) {
        super(activity.getSupportFragmentManager());
    }

    public void setPictures(List<GalleryPicture> pictures) {
        this.pictures = pictures;
        notifyDataSetChanged();
    }

    /*
    Forces the owning view to reload ALL content after tag update, http://solvedstack.com/questions/viewpager-pageradapter-not-updating-the-view
     */
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    @Override
    public int getCount() {
        if (pictures == null) {
            return 0;
        }
        return pictures.size();
    }

}
