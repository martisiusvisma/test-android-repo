package com.kindiedays.common.ui;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.util.Log;

/**
 * Created by anduser on 25.01.18.
 */

public class PersonSelectionDialogFragment extends DialogFragment {

    protected PersonSelectionDialogListener listener;

    public interface PersonSelectionDialogListener {
        void onNoOneSelected();
        void onPersonSelected();
        void onNotAuthorized();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (PersonSelectionDialogListener) context;
        } catch (ClassCastException exc) {
            Log.d(this.getClass().getName(),
                    context.toString() + " must implement " + PersonSelectionDialogListener.class);
        }
    }
}
