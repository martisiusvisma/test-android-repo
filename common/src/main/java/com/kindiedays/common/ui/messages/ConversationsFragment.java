package com.kindiedays.common.ui.messages;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.business.ConversationBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationList;
import com.kindiedays.common.pojo.ConversationPatchResponse;
import com.kindiedays.common.pushnotifications.BroadcastReceiver;
import com.kindiedays.common.ui.InfiniteListViewScrollListener;
import com.kindiedays.common.ui.MainActivityFragment;

import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by pleonard on 27/05/2015.
 */
public abstract class ConversationsFragment extends MainActivityFragment implements Observer {

    private SwipeMenuListView conversationsList;
    private ProgressBar progressBar;
    protected ConversationAdapter conversationAdapter;
    protected AdapterView.OnItemClickListener onConversationClickListener;
    protected TextViewCustom title;
    public static final String CHILD_ID = "childId";
    protected boolean isUpdating;
    private Context mContext;
    private ConversationList conversationList;
    private OnAsynkPostExecuteListener onAsynkPostExecuteListener;

    protected interface OnAsynkPostExecuteListener {
        void onPostExecute();
    }

    public void setOnAsynkPostExecuteListener(OnAsynkPostExecuteListener onAsynkPostExecuteListener) {
        this.onAsynkPostExecuteListener = onAsynkPostExecuteListener;
    }

    InfiniteListViewScrollListener.EndListReachedListener endGridReachedListener = new InfiniteListViewScrollListener.EndListReachedListener() {
        @Override
        public void topReached() {
            //Do nothing
        }

        @Override
        public void bottomReached() {
            if (!isUpdating && ConversationBusiness.getInstance().getConversations() != null && ConversationBusiness.getInstance().getConversations().getLinkNext() != null) {
                GetMoreConversationsTask task = new GetMoreConversationsTask(ConversationBusiness.getInstance().getConversations().getLinkNext());
                pendingTasks.add(task);
                task.execute();
            }
        }
    };

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshMessagesList();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_conversations, null);
        mContext = getContext();
        isUpdating = true;
        conversationsList = (SwipeMenuListView) v.findViewById(R.id.conversationsList);
        conversationsList.setAdapter(conversationAdapter);
        conversationsList.setOnItemClickListener(onConversationClickListener);
        conversationsList.setMenuCreator(this::createSwipeMenu);
        conversationsList.setOnMenuItemClickListener(this::onMenuItemClick);
        conversationsList.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);
        conversationsList.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        conversationsList.setOnScrollListener(new InfiniteListViewScrollListener(endGridReachedListener));
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        setHasOptionsMenu(true);

        if (conversationList != null) {
            conversationAdapter.setConversations(conversationList);
            progressBar.setVisibility(View.GONE);
            conversationsList.setVisibility(View.VISIBLE);
        }
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        ConversationBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver, new IntentFilter("message_notification"));
        title = (TextViewCustom) getActivity().findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Messages));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onStop() {
        super.onStop();
        ConversationBusiness.getInstance().deleteObserver(this);
    }

    @Override
    public void refreshData() {
        refreshMessagesList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_conversations, menu);
    }

    @Override
    public void update(Observable observable, Object data) {
        conversationList = ((ConversationBusiness) observable).getConversations();
        conversationAdapter.setConversations(conversationList);
    }

    @SuppressWarnings("squid:S2176")
    private class LoadConversationsTask extends ConversationBusiness.LoadConversationsTask {

        LoadConversationsTask(long childId) {
            super(childId);
        }

        protected ConversationList doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.pictures));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(ConversationList conversations) {
            if (!checkNetworkException(conversations, exception)) {
                super.onPostExecute(conversations);
                progressBar.setVisibility(View.GONE);
                conversationsList.setVisibility(View.VISIBLE);
                conversationList = conversations;
                conversationAdapter.setConversations(conversationList);
            }
            pendingTasks.remove(this);
            isUpdating = false;
        }
    }


    private class MarkConversationUnreadTask extends ConversationBusiness.MarkConversationUnread {

        MarkConversationUnreadTask(Conversation conversation) {
            super(conversation);
        }

        protected ConversationPatchResponse doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.data).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(ConversationPatchResponse response) {
            if (exception == null) {
                refreshData();
            } else {
                Toast.makeText(getActivity(), TextUtils.isEmpty(exception.getMessage()) ? getString(R.string.OperationFailed) : exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
            pendingTasks.remove(this);
            if (onAsynkPostExecuteListener != null) {
                onAsynkPostExecuteListener.onPostExecute();
            }
        }
    }

    private class HideConversationTask extends ConversationBusiness.DeleteConversation {

        HideConversationTask(long conversationId) {
            super(conversationId);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            } else {
                Toast.makeText(getActivity(), getString(R.string.OperationFailed), Toast.LENGTH_SHORT).show();
                refreshData();
            }
            pendingTasks.remove(this);
            if (onAsynkPostExecuteListener != null) {
                onAsynkPostExecuteListener.onPostExecute();
            }
        }
    }


    @SuppressWarnings("squid:S2176")
    private class GetMoreConversationsTask extends ConversationBusiness.GetMoreConversationsTask {

        GetMoreConversationsTask(String url) {
            super(url);
        }

        @Override
        protected void onPreExecute() {
            isUpdating = true;
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ConversationList conversationList) {
            if (!checkNetworkException(conversationList, exception)) {
                super.onPostExecute(conversationList);
                moreConversationsLoaded(conversationList);
            }
            pendingTasks.remove(this);
            isUpdating = false;
        }
    }

    @SuppressWarnings("squid:S1172")
    protected void moreConversationsLoaded(ConversationList list) {
        conversationAdapter.notifyDataSetChanged();
    }

    private void refreshMessagesList() {
        long childId = -1;
        if (getArguments() != null) {
            childId = getArguments().getLong(CHILD_ID, -1);
        }
        LoadConversationsTask task = new LoadConversationsTask(childId);
        pendingTasks.add(task);
        task.execute();
    }

    private void createSwipeMenu(SwipeMenu menu) {
        SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
        int pixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, getContext().getResources().getDisplayMetrics());
        deleteItem.setWidth(pixels);
        deleteItem.setIcon(android.R.drawable.ic_delete);
        menu.addMenuItem(deleteItem);

        SwipeMenuItem markUnreadItem = new SwipeMenuItem(getActivity());
        markUnreadItem.setWidth(pixels);
        markUnreadItem.setTitle(R.string.MarkUnread);
        markUnreadItem.setTitleSize(18);
        markUnreadItem.setTitleColor(R.color.black);
        markUnreadItem.setBackground(R.color.grey);
        menu.addMenuItem(markUnreadItem);
    }

    private boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
        //calling due to stupidity of sonar
        menu.getViewType();
        Conversation conversation = conversationAdapter.getItem(position);
        if (index == 0) {
            conversationAdapter.hideConversation(conversation);
            HideConversationTask task = new HideConversationTask(conversation.getId());
            task.execute();
            pendingTasks.add(task);
        } else {
            MarkConversationUnreadTask markUnreadTask = new MarkConversationUnreadTask(conversation);
            markUnreadTask.execute();
            pendingTasks.add(markUnreadTask);
        }
        return true;
    }

    public abstract boolean onOptionsItemSelected(MenuItem item);
}