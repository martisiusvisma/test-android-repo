package com.kindiedays.common.ui.meals;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.menus.GetMenus;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.ui.MainActivityFragment;

import org.joda.time.LocalDate;

import java.util.Collections;
import java.util.List;

/**
 * Created by pleonard on 11/06/2015.
 */
public class MealsFragment extends MainActivityFragment {

    private ListView mealList;
    private MenuAdapter mealAdapter;
    private ProgressBar progressBar;
    private TextViewCustom title;

    private List<Menu> meals;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_meals, null);
        mealList = (ListView) v.findViewById(R.id.menuList);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        mealAdapter = new MenuAdapter(getActivity());
        mealList.setAdapter(mealAdapter);
        setHasOptionsMenu(true);

        if (meals != null) {
            mealAdapter.setMeals(meals);
            progressBar.setVisibility(View.GONE);
            mealList.setVisibility(View.VISIBLE);
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.Meals));
    }

    @Override
    public void refreshData() {
        GetMealsAsyncTask task = new GetMealsAsyncTask();
        pendingTasks.add(task);
        task.execute();
    }

    private class GetMealsAsyncTask extends NetworkAsyncTask<List<Menu>> {

        @Override
        protected void onPostExecute(List<Menu> meals) {
            super.onPostExecute(meals);
            if (checkNetworkException(meals, exception)) {
                return;
            }
            Collections.sort(meals, (o1, o2) -> {
                LocalDate today = new LocalDate();
                if (o1.getDate().equals(today)) {
                    return -1;
                } else if (o2.getDate().equals(today)) {
                    return 1;
                } else {
                    return o2.getDate().compareTo(o1.getDate());
                }
            });

            mealAdapter.setMeals(meals);
            MealsFragment.this.meals = meals;
            progressBar.setVisibility(View.GONE);
            mealList.setVisibility(View.VISIBLE);
            pendingTasks.remove(this);
        }

        @Override
        protected List<Menu> doNetworkTask() throws Exception {
            try {
                return GetMenus.getMenus();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }
    }
}
