package com.kindiedays.common.ui.picturegallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.common.R;
import com.kindiedays.common.pojo.JournalPostPicture;

/**
 * Created by pleonard on 04/12/2015.
 */
public class JournalPostPictureFragment extends AbstractPictureFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        caption.setText(((JournalPostPicture) picture).getCaption());
        v.findViewById(R.id.taggedChildren).setVisibility(View.GONE);
        return v;
    }
}
