package com.kindiedays.common.ui;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.utils.LocaleHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;
import static com.kindiedays.common.utils.NetworkUtils.isOnline;

/**
 * Created by pleonard on 30/06/2015.
 */
public class KindieDaysActivity extends AppCompatActivity {

    private static final String TAG = KindieDaysActivity.class.getSimpleName();
    TextViewCustom title;
    private Set<AsyncTask> pendingTasks = new HashSet<>();
    private BroadcastReceiver networkStateReceiver;
    private ProgressDialogFragment mProgressDialog;

    protected Set<AsyncTask> getPendingTasks() {
        return pendingTasks;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLocale();
        Log.d(this.getClass().getName(), "onCreate");
        final ActionBar abar = getSupportActionBar();
        //Sometimes the title doesn't update after changing the language
        try {
            String labelRes = getResources().getString(getPackageManager().getActivityInfo(getComponentName(), 0).labelRes);
            if (!labelRes.isEmpty()) {
                abar.setTitle(labelRes);
            }
        } catch (Exception e) {
            //Shouldn't happen. And if it does it doesn't matter
            Log.e("KindieDaysActivity", "onCreate: ", e);
        }
        abar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.color.kindieblue));
        abar.setElevation(0);
    }

    private void setupLocale() {
        Locale locale = new Locale(LocaleHelper.getLanguage(this));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onResume() {
        super.onResume();
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (isOnline(context)) {
                    closeInternetLostDialog();
                    refreshVisibleFragments();
                } else {
                    showInternetLostDialog();
                }

            }
        };
        registerReceiver(networkStateReceiver, new IntentFilter(CONNECTIVITY_ACTION));
    }

    private void refreshVisibleFragments() {
        List<String> fragmentTags = Arrays.asList("journal", "messages", "album", "events", "meals",
                "attendance", "about", "dailyreport", "settings", "calendar", "settings");
        for (String fragmentTag : fragmentTags) {
            android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
            if (fragment != null && fragment.isVisible() && fragment instanceof Refreshable) {
                Log.d(TAG, "refreshVisibleFragments: refresh fragment " + fragmentTag);
                ((Refreshable) fragment).refreshData();
            }

        }

    }

    protected void showInternetLostDialog() {
        Fragment previousDialog = getFragmentManager().findFragmentByTag(InternetLostDialog.TAG);
        if (previousDialog == null) {
            DialogFragment newFragment = new InternetLostDialog();
            newFragment.show(getFragmentManager(), InternetLostDialog.TAG);
        }

    }

    protected void closeInternetLostDialog() {
        DialogFragment previousDialog = (DialogFragment) getFragmentManager().findFragmentByTag(InternetLostDialog.TAG);
        if (previousDialog != null) {
            previousDialog.dismiss();
        }

    }

    public void relaunchActivity() {
        Intent intent = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkStateReceiver);
    }

    protected void stopPendingTasks() {
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        pendingTasks.clear();
    }

    @Override
    public void onDestroy() {
        stopPendingTasks();
        super.onDestroy();
    }

    @SuppressWarnings("deprecation")
    protected boolean checkNetworkException(Object value, KindieDaysNetworkException exception) {
        boolean exceptionDetected = (exception != null && value == null);
        if (exceptionDetected) {
            if (exception.isLocalizedMessage()) {
                if (exception.getPayload() != null) {
                    Toast.makeText(this, exception.getMessagePayload(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
            } else {
                if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    stopPendingTasks();
                    Intent i = new Intent(this, ((KindieDaysApp) getApplication()).getLoginActivityClass());
                    startActivity(i);
                } else {
                    Log.e(this.getClass().getSimpleName(), "Internal error");
                }
            }
        }
        return exceptionDetected;
    }

    public void showProgress() {
        hideProgress();
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialogFragment();
        }
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentByTag(ProgressDialogFragment.class.getSimpleName());
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            ft.remove(fragment);
        }
        mProgressDialog = new ProgressDialogFragment();
        ft.add(mProgressDialog, ProgressDialogFragment.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismissAllowingStateLoss();
        }
    }
}
