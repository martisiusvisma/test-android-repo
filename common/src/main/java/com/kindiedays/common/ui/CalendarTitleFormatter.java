package com.kindiedays.common.ui;

import android.content.Context;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static java.util.Calendar.FRIDAY;
import static java.util.Calendar.MONDAY;
import static java.util.Calendar.SATURDAY;
import static java.util.Calendar.SUNDAY;
import static java.util.Calendar.THURSDAY;
import static java.util.Calendar.TUESDAY;
import static java.util.Calendar.WEDNESDAY;

/**
 * Created by pleonard on 11/01/2016.
 */
public class CalendarTitleFormatter implements TitleFormatter {

    final DateFormat dateFormat = new SimpleDateFormat(
            "MMMM yyyy", Locale.getDefault()
    );

    @Override
    public CharSequence format(CalendarDay day) {
        if ("fi".equals(Locale.getDefault().getLanguage())) {
            String[] months = KindieDaysApp.getApp().getResources().getStringArray(R.array.months);
            return months[day.getMonth()] + " " + day.getYear();
        } else {
            return dateFormat.format(day.getDate());
        }
    }

    public static String getDayOfWeekString(Context context, Calendar calendar){
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        String dayResult;
        switch (dayOfWeek){
            case MONDAY:
                dayResult = context.getString(R.string.Monday);
                break;
            case TUESDAY:
                dayResult = context.getString(R.string.Tuesday);
                break;
            case WEDNESDAY:
                dayResult = context.getString(R.string.Wednesday);
                break;
            case THURSDAY:
                dayResult = context.getString(R.string.Thursday);
                break;
            case FRIDAY:
                dayResult = context.getString(R.string.Friday);
                break;
            case SATURDAY:
                dayResult = context.getString(R.string.Saturday);
                break;
            case SUNDAY:
                dayResult = context.getString(R.string.Sunday);
                break;
            default:
                dayResult = "";
                break;
        }
        return dayResult;
    }
}
