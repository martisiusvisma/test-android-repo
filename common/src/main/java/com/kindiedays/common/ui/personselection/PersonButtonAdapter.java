package com.kindiedays.common.ui.personselection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.kindiedays.common.pojo.Person;

import java.util.List;

/**
 * Created by pleonard on 13/11/2015.
 */
public abstract class PersonButtonAdapter extends RecyclerView.Adapter<PersonButtonAdapter.ItemViewHolder> {

    protected List<? extends Person> persons;
    protected final Context mContext;

    public PersonButtonAdapter(Context context) {
        this.mContext = context;
    }
    
    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Person person = persons.get(position);
        ((PersonSelectionButton) holder.itemView).setPerson(person);
        holder.itemView.setTag(person);
    }

    @Override
    public int getItemCount() {
        if (persons == null) {
            return 0;
        }
        return persons.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public ItemViewHolder(PersonSelectionButton button) {
            super(button);
        }
    }
}
