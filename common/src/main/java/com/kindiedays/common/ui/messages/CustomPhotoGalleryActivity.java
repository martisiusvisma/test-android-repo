package com.kindiedays.common.ui.messages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kindiedays.common.R;

import java.io.File;
import java.util.ArrayList;

import static com.kindiedays.common.utils.constants.CommonConstants.EXTRA_GALLERY;
import static com.kindiedays.common.utils.constants.CommonConstants.MAX_UPLOAD_SIZE_KB;
import static com.kindiedays.common.utils.constants.CommonConstants.MB_SIZE;

public class CustomPhotoGalleryActivity extends AppCompatActivity {
    private static final String TAG = CustomPhotoGalleryActivity.class.getSimpleName();
    private static final int COLUMN_COUNT = 3;
    public static final int RESOURCES_CAP = 20;
    private static final String DESC = " DESC";
    public static final String HIDE_SPINNER_KEY = "hide_spiner";
    public static final String ONLY_JPEG_IMAGES = "only_jpeg_images";

    private static final String SELECTED_PHOTOS_EXTRA = "SelectedPhotosCustomPhotoGalleryActivity";
    private static final String SELECTED_PHOTOS_NUMBER = "SelectedPhotosNumber";

    private TextView btnSelect;
    private RecyclerView rvImages;
    private TextView tvCount;
    private CoordinatorLayout clContent;
    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            rvImages.setAdapter(position == 0 ? imageAdapter : videoAdapter);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            //do nothing
        }
    };

    private int totalCheckedCount;
    private int totalCheckedFromDevice;

    private RecycleImageAdapter imageAdapter;
    private Cursor imageCursor;
    private String[] arrPathImages;
    private boolean[] thumbnailsSelection;
    private int[] idsImages;
    private int imagesCount;

    private RecycleVideoAdapter videoAdapter;
    private Cursor videoCursor;
    private String[] arrPathVideos;
    private boolean[] videoSelection;
    private int[] idsVideo;
    private int videoCount;
    private ArrayList<CharSequence> selectedPictures;

    public static Intent getInstance(Context context, ArrayList<CharSequence> selectedPicturesFromDevice, int selectedNumber){
        Intent intent = new Intent(context, CustomPhotoGalleryActivity.class);
        intent.putExtra(CustomPhotoGalleryActivity.HIDE_SPINNER_KEY, true);
        intent.putExtra(SELECTED_PHOTOS_NUMBER, selectedNumber);
        intent.putCharSequenceArrayListExtra(SELECTED_PHOTOS_EXTRA, selectedPicturesFromDevice);
        return intent;
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_gallery);
        selectedPictures = getIntent().getCharSequenceArrayListExtra(SELECTED_PHOTOS_EXTRA);
        totalCheckedCount = getIntent().getIntExtra(SELECTED_PHOTOS_NUMBER, 0);
        totalCheckedFromDevice = selectedPictures != null ? selectedPictures.size() : 0;
        rvImages = (RecyclerView) findViewById(R.id.rv_gallery);
        btnSelect = (TextView) findViewById(R.id.btnSelect);
        clContent = (CoordinatorLayout) findViewById(R.id.cl_content);
        tvCount = (TextView) findViewById(R.id.tv_select_counter);
        tvCount.setText(String.valueOf(totalCheckedCount));
        tvCount.setVisibility(View.INVISIBLE);
        btnSelect.setEnabled(false);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.color.kindieblue));
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        boolean isHideSpinner = false;
        boolean onlyJpeg = false;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isHideSpinner = extras.getBoolean(HIDE_SPINNER_KEY, false);
            onlyJpeg = extras.getBoolean(ONLY_JPEG_IMAGES, false);
        }

        final String[] itemTypes = getResources().getStringArray(R.array.gallery_item_types);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.item_spinner_dropdown, itemTypes);
        spinnerAdapter.setDropDownViewResource(R.layout.item_spinner_simple_dropdown);
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(listener);
        if (isHideSpinner) {
            spinner.setVisibility(View.INVISIBLE);
        }

        videoAdapter = new RecycleVideoAdapter();
        imageAdapter = new RecycleImageAdapter();
        rvImages.setLayoutManager(new GridLayoutManager(getApplicationContext(), COLUMN_COUNT));

        initImageCursor(onlyJpeg);
        initVideoCursor();

        btnSelect.setOnClickListener(v -> {
            StringBuilder selectedItems = new StringBuilder();
            selectedItems.append(getCheckedFileNames(thumbnailsSelection, arrPathImages));
            selectedItems.append(getCheckedFileNames(videoSelection, arrPathVideos));
            Intent i = new Intent();
            i.putExtra(EXTRA_GALLERY, selectedItems.toString());
            setResult(Activity.RESULT_OK, i);
            finish();
        });
    }

    private void initImageCursor(boolean onlyJpeg) {
        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
        final String orderBy = android.provider.MediaStore.Images.Media.DATE_TAKEN + DESC;
        final String whereClause = onlyJpeg ? MediaStore.Images.ImageColumns.MIME_TYPE + "=?" : null;
        final String[] whereArgs = onlyJpeg ? new String[] {"image/jpeg"} : null;

        imageCursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, whereClause, whereArgs, orderBy);
        int imageColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media._ID);
        this.imagesCount = imageCursor.getCount();
        this.arrPathImages = new String[this.imagesCount];
        idsImages = new int[imagesCount];
        this.thumbnailsSelection = new boolean[this.imagesCount];
        for (int i = 0; i < this.imagesCount; i++) {
            imageCursor.moveToPosition(i);
            idsImages[i] = imageCursor.getInt(imageColumnIndex);
            int dataColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);
            arrPathImages[i] = imageCursor.getString(dataColumnIndex);
            if (selectedPictures != null && selectedPictures.contains(arrPathImages[i])){
                thumbnailsSelection[i] = true;
                updateFooterView();
            }
        }
    }

    private void initVideoCursor() {
        final String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};
        final String orderBy = android.provider.MediaStore.Video.Media.DATE_TAKEN + DESC;

        videoCursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
        int imageColumnIndex = videoCursor.getColumnIndex(MediaStore.Video.Media._ID);
        this.videoCount = videoCursor.getCount();
        this.arrPathVideos = new String[this.videoCount];
        idsVideo = new int[videoCount];
        this.videoSelection = new boolean[this.videoCount];
        for (int i = 0; i < this.videoCount; i++) {
            videoCursor.moveToPosition(i);
            idsVideo[i] = videoCursor.getInt(imageColumnIndex);
            int dataColumnIndex = videoCursor.getColumnIndex(MediaStore.Video.Media.DATA);
            arrPathVideos[i] = videoCursor.getString(dataColumnIndex);
        }
    }

    private StringBuilder getCheckedFileNames(boolean[] selection, String[] path) {
        final int len = selection.length;
        StringBuilder selectedItems = new StringBuilder();
        for (int i = 0; i < len; i++) {
            if (selection[i]) {
                selectedItems.append(path[i]).append("|");
            }
        }
        return selectedItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (totalCheckedFromDevice > 0){
            setResult(Activity.RESULT_CANCELED);
        } else {
            Intent i = new Intent();
            i.putExtra(EXTRA_GALLERY, "");
            setResult(Activity.RESULT_OK, i);
        }
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageCursor.close();
        videoCursor.close();
    }

    private boolean checkResourcesOnCap() {
        if (totalCheckedCount >= RESOURCES_CAP) {
            showTopSnackBar(R.string.gallery_max_items_cap);
            return false;
        }
        return true;
    }

    @SuppressWarnings("squid:S3398")
    private boolean checkResourcesOnSize(String path) {
        File file = new File(path);
        if (file.length() / MB_SIZE > MAX_UPLOAD_SIZE_KB) {
            showTopSnackBar(R.string.gallery_file_too_large);
            return false;
        }
        return true;
    }

    private void showTopSnackBar(@StringRes int stringRes) {
        TSnackbar snackbar = TSnackbar.make(clContent, stringRes, TSnackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void updateFooterView() {
        btnSelect.setEnabled(totalCheckedFromDevice > 0);
        int color = totalCheckedFromDevice > 0
                ? ContextCompat.getColor(getApplicationContext(), R.color.kindieblue)
                : ContextCompat.getColor(getApplicationContext(), R.color.grey);
        btnSelect.setTextColor(color);
        tvCount.setVisibility(totalCheckedFromDevice > 0 ? View.VISIBLE : View.INVISIBLE);
        tvCount.setText(String.valueOf(totalCheckedCount));
    }

    private class RecycleImageAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_gallery_item,
                    parent, false);
            return new ImageViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ImageViewHolder vh = (ImageViewHolder) holder;
            vh.bind(position);
        }

        @Override
        public int getItemCount() {
            return imagesCount;
        }
    }

    private class RecycleVideoAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_gallery_item,
                    parent, false);
            return new VideoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            VideoViewHolder vh = (VideoViewHolder) holder;
            vh.bind(position);
        }

        @Override
        public int getItemCount() {
            return videoCount;
        }
    }

    private class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgThumb;
        private CheckBox chkImage;
        int id;

        ImageViewHolder(View itemView) {
            super(itemView);
            imgThumb = (ImageView) itemView.findViewById(R.id.imgThumb);
            chkImage = (CheckBox) itemView.findViewById(R.id.chkImage);
        }

        private void bind(int position) {
            chkImage.setId(position);
            imgThumb.setId(position);
            chkImage.setOnClickListener(v -> resolveSelection((CheckBox) v));
            imgThumb.setOnClickListener(v -> resolveSelection(chkImage));

            try {
                setBitmap(imgThumb, idsImages[position]);
            } catch (Exception e) {
                Log.e(TAG, "bind: ", e);
            }

            chkImage.setChecked(thumbnailsSelection[position]);
            id = position;
        }

        private void resolveSelection(CheckBox view) {
            int viewId = view.getId();
            if (thumbnailsSelection[viewId]) {
                view.setChecked(false);
                thumbnailsSelection[viewId] = false;
                totalCheckedFromDevice--;
                totalCheckedCount--;
            } else if (checkResourcesOnCap()) {
                view.setChecked(true);
                thumbnailsSelection[viewId] = true;
                totalCheckedFromDevice++;
                totalCheckedCount++;
            } else {
                view.setChecked(false);
            }
            updateFooterView();
        }

        private void setBitmap(final ImageView iv, final int id) {
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    iv.setImageResource(R.drawable.gallery_placeholder);
                }

                @Override
                protected Bitmap doInBackground(Void... params) {
                    return MediaStore.Images.Thumbnails.getThumbnail(getApplicationContext().getContentResolver(),
                            id, MediaStore.Images.Thumbnails.MINI_KIND, null);
                }

                @Override
                protected void onPostExecute(Bitmap result) {
                    super.onPostExecute(result);
                    iv.setImageBitmap(result);
                }
            }.execute();
        }
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgThumb;
        private CheckBox chkImage;
        int id;

        VideoViewHolder(View itemView) {
            super(itemView);
            imgThumb = (ImageView) itemView.findViewById(R.id.imgThumb);
            chkImage = (CheckBox) itemView.findViewById(R.id.chkImage);
        }

        private void bind(int position) {
            chkImage.setId(position);
            imgThumb.setId(position);
            chkImage.setOnClickListener(v -> resolveSelection((CheckBox) v));
            imgThumb.setOnClickListener(v -> resolveSelection(chkImage));

            try {
                setVideoBitmap(imgThumb, arrPathVideos[position]);
            } catch (Exception e) {
                Log.e(TAG, "bind: ", e);
            }

            chkImage.setChecked(videoSelection[position]);
            id = position;
        }

        private void resolveSelection(CheckBox view) {
            int viewId = view.getId();
            if (videoSelection[viewId]) {
                view.setChecked(false);
                videoSelection[viewId] = false;
                totalCheckedFromDevice--;
                totalCheckedCount--;
            } else if (checkResourcesOnCap() && checkResourcesOnSize(arrPathVideos[viewId])) {
                view.setChecked(true);
                videoSelection[viewId] = true;
                totalCheckedFromDevice++;
                totalCheckedCount++;
            } else {
                view.setChecked(false);
            }

            updateFooterView();
        }

        private void setVideoBitmap(final ImageView iv, final String path) {
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    iv.setImageResource(R.drawable.gallery_placeholder);
                }

                @Override
                protected Bitmap doInBackground(Void... params) {
                    return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
                }

                @Override
                protected void onPostExecute(Bitmap result) {
                    super.onPostExecute(result);
                    iv.setImageBitmap(result);
                }
            }.execute();
        }
    }
}
