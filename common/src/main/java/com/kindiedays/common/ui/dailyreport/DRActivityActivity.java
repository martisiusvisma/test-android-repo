package com.kindiedays.common.ui.dailyreport;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindiedays.common.R;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.ActivityRes;
import com.kindiedays.common.utils.PictureUtils;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DRActivityActivity extends AppCompatActivity {
    public static final String ACTIVITY = "ACTIVITY";

    Intent intent;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_activity);

        intent = this.getIntent();
        bundle = intent.getExtras();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextViewCustom title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.daily_activity);

        ImageView sectionIcon = (ImageView) findViewById(R.id.rightIcon);
        sectionIcon.setVisibility(View.VISIBLE);
        sectionIcon.setImageDrawable(PictureUtils.svgToBitmapDrawable(getResources(), R.raw.dr_activites_circle_bg, 80));

        setContentView();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            overridePendingTransition(R.anim.from_left_to_right, R.anim.exit_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setContentView(){
        TextView noteLbl = (TextView) findViewById(R.id.noteLbl);
        TextView timeLbl = (TextView) findViewById(R.id.timeLbl);
        TextView titleLbl = (TextView) findViewById(R.id.titleLbl);

        ActivityRes data = (ActivityRes) bundle.getSerializable(ACTIVITY);
        if (data != null){
            noteLbl.setText(data.getText());
            timeLbl.setText(data.getModified().toString("HH:mm"));
            titleLbl.setText(data.getCategoryName());
        }
    }

}
