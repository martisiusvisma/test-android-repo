package com.kindiedays.common.ui;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Window;

import com.kindiedays.common.R;

public class ProgressDialogFragment extends DialogFragment {

    private static ProgressDialogFragment instance;

    public static DialogFragment getInstance() {
        if (instance == null) {
            instance = new ProgressDialogFragment();
        }
        return instance;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.progress_dialog_application);
        Drawable drawable = new ColorDrawable(ContextCompat.getColor(getActivity(), android.R.color.transparent));
        dialog.getWindow().setBackgroundDrawable(drawable);
        return dialog;
    }
}
