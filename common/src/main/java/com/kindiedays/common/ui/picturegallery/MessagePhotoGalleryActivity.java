package com.kindiedays.common.ui.picturegallery;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.kindiedays.common.R;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.ui.KindieDaysActivity;

import org.parceler.Parcels;

import java.util.Collections;
import java.util.List;

public class MessagePhotoGalleryActivity extends KindieDaysActivity {
    private static final String EXTRA_ATTACHMENT = "extra_attachment";

    private ViewPager viewPager;
    private List<Attachment> attachments;

    public static void start(final Context context, Attachment attachment) {
        Intent intent = new Intent(context, MessagePhotoGalleryActivity.class);
        intent.putExtra(EXTRA_ATTACHMENT, Parcels.wrap(attachment));
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_gallery);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Attachment attachment = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_ATTACHMENT));
        this.attachments = Collections.singletonList(attachment);
        viewPager = (ViewPager) findViewById(R.id.pictureViewPager);
        viewPager.setAdapter(new MessagePhotoGalleryAdapter(this, attachments));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_message_photo_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        } else if (i == R.id.action_save) {
            Attachment picture = attachments.get(viewPager.getCurrentItem());
            AlertDialog.Builder builder = new AlertDialog.Builder(MessagePhotoGalleryActivity.this);
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> saveImageToExternalStorage(picture));
            builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
            builder.setTitle(R.string.save);
            builder.setMessage(R.string.save_photo);
            builder.setCancelable(false);
            Dialog dialog = builder.create();
            dialog.show();
        }
        return true;
    }

    private void saveImageToExternalStorage(final Attachment attachment) {
        Glide.with(getApplicationContext())
                .load(attachment.getUri())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        MediaStore.Images.Media.insertImage(MessagePhotoGalleryActivity.this.getContentResolver(),
                                resource, attachment.getFilename(), attachment.getFormat());
                    }
                });
    }
}
