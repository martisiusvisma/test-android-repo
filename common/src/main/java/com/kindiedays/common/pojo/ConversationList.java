package com.kindiedays.common.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giuseppefranco on 04/10/16.
 */

public class ConversationList extends PaginatedResource {

    private List<Conversation> conversations = new ArrayList<>();

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }
}
