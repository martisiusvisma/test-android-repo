package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class MealRes implements Serializable {
    private Integer index;
    private Integer child;
    private List<Integer> children;
    private DateTime date;
    private DateTime created;
    private String reaction;
    private String reactionDescription;
    private Integer drink;
    private Integer mealIndex;
    private String drinkQuantity;
    private String drinkUnit;
    private String notes;

    public Integer getIndex() {
        return index;
    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getReactionDescription() {
        return reactionDescription;
    }

    public void setReactionDescription(String reactionDescription) {
        this.reactionDescription = reactionDescription;
    }

    public Integer getDrink() {
        return drink;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setDrink(Integer drink) {
        this.drink = drink;
    }

    public Integer getMealIndex() {
        return mealIndex;
    }

    public void setMealIndex(Integer mealIndex) {
        this.mealIndex = mealIndex;
    }

    public String getDrinkQuantity() {
        return drinkQuantity;
    }

    public void setDrinkQuantity(String drinkQuantity) {
        this.drinkQuantity = drinkQuantity;
    }

    public String getDrinkUnit() {
        return drinkUnit;
    }

    public void setDrinkUnit(String drinkUnit) {
        this.drinkUnit = drinkUnit;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Integer> getChildren() {
        return children;
    }

    public void setChildren(List<Integer> children) {
        this.children = children;
    }
}
