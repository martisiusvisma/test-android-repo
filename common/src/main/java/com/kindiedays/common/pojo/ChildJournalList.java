package com.kindiedays.common.pojo;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 03/11/2015.
 */
public class ChildJournalList extends PaginatedResource {

    private LocalDate start;
    private LocalDate end;

    private List<Event> events = new ArrayList<>();
    private List<Menu> menus = new ArrayList<>();
    private List<Nap> naps = new ArrayList<>();
    private List<Signin> signins = new ArrayList<>();
    private List<JournalPost> journalPosts = new ArrayList<>();
    private List<JournalGalleryPicture> galleryPictures = new ArrayList<>();

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<Nap> getNaps() {
        return naps;
    }

    public void setNaps(List<Nap> naps) {
        this.naps = naps;
    }

    public List<Signin> getSignins() {
        return signins;
    }

    public void setSignins(List<Signin> signins) {
        this.signins = signins;
    }

    public List<JournalPost> getJournalPosts() {
        return journalPosts;
    }

    public void setJournalPosts(List<JournalPost> journalPosts) {
        this.journalPosts = journalPosts;
    }

    public List<JournalGalleryPicture> getGalleryPictures() {
        return galleryPictures;
    }

    public void setGalleryPictures(List<JournalGalleryPicture> galleryPictures) {
        this.galleryPictures = galleryPictures;
    }

    @Override
    public String toString() {
        return "ChildJournalList{" +
                "start=" + start +
                ", end=" + end +
                ", events=" + events.size() +
               // ", menus=" + menus +
               // ", naps=" + naps +
              //  ", signins=" + signins +
                ", journalPosts=" + journalPosts.size() +
              //  ", galleryPictures=" + galleryPictures +
                "} " + super.toString();
    }


}
