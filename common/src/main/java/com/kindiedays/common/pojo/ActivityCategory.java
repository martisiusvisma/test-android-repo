package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 05/08/2016.
 */
public class ActivityCategory extends ServerObject {

    private String name;
    private boolean enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
