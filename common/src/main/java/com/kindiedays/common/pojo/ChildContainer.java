package com.kindiedays.common.pojo;

import android.util.Log;

/**
 * Created by pleonard on 15/05/2015.
 */
public abstract class ChildContainer extends ServerObject {

    private String name;
    private int nameRes;

    public int getNameRes() {
        return nameRes;
    }

    public void setNameRes(int nameRes) {
        this.nameRes = nameRes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
