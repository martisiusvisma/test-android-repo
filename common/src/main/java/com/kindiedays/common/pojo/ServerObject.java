package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 21/04/2015.
 */
public class ServerObject {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
