package com.kindiedays.common.pojo;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 28/05/2015.
 */
public class Conversation extends ServerObject implements Comparable<Conversation> {

    private String subject;
    private Set<ConversationParty> parties;
    private String messagesURI;
    private int lastMessageIndex;
    private DateTime lastMessageCreated;
    private int unreadMessages;
    private String selfURI;
    private List<Message> messages;
    private long[] children;


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<ConversationParty> getParties() {
        return parties;
    }

    public void setParties(Set<ConversationParty> parties) {
        this.parties = parties;
    }

    public String getMessagesURI() {
        return messagesURI;
    }

    public void setMessagesURI(String messagesURI) {
        this.messagesURI = messagesURI;
    }

    public int getLastMessageIndex() {
        return lastMessageIndex;
    }

    public void setLastMessageIndex(int lastMessageIndex) {
        this.lastMessageIndex = lastMessageIndex;
    }

    public DateTime getLastMessageCreated() {
        return lastMessageCreated;
    }

    public void setLastMessageCreated(DateTime lastMessageCreated) {
        this.lastMessageCreated = lastMessageCreated;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public String getSelfURI() {
        return selfURI;
    }

    public void setSelfURI(String selfURI) {
        this.selfURI = selfURI;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public int compareTo(@NonNull Conversation another) {
        return lastMessageCreated.compareTo(another.lastMessageCreated);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Conversation that = (Conversation) o;

        if (getLastMessageIndex() != that.getLastMessageIndex()) {
            return false;
        }
        if (getUnreadMessages() != that.getUnreadMessages()) {
            return false;
        }
        if (getSubject() != null ? !getSubject().equals(that.getSubject()) : that.getSubject() != null) {
            return false;
        }
        if (getParties() != null ? !getParties().equals(that.getParties()) : that.getParties() != null) {
            return false;
        }
        if (getMessagesURI() != null ? !getMessagesURI().equals(that.getMessagesURI()) : that.getMessagesURI() != null) {
            return false;
        }
        if (getLastMessageCreated() != null ? !getLastMessageCreated().equals(that.getLastMessageCreated()) : that.getLastMessageCreated() != null) {
            return false;
        }
        if (getSelfURI() != null ? !getSelfURI().equals(that.getSelfURI()) : that.getSelfURI() != null) {
            return false;
        }
        if (getMessages() != null ? !getMessages().equals(that.getMessages()) : that.getMessages() != null) {
            return false;
        }
        return Arrays.equals(getChildren(), that.getChildren());

    }

    @Override
    public int hashCode() {
        int result = getSubject() != null ? getSubject().hashCode() : 0;
        result = 31 * result + (getParties() != null ? getParties().hashCode() : 0);
        result = 31 * result + (getMessagesURI() != null ? getMessagesURI().hashCode() : 0);
        result = 31 * result + getLastMessageIndex();
        result = 31 * result + (getLastMessageCreated() != null ? getLastMessageCreated().hashCode() : 0);
        result = 31 * result + getUnreadMessages();
        result = 31 * result + (getSelfURI() != null ? getSelfURI().hashCode() : 0);
        result = 31 * result + (getMessages() != null ? getMessages().hashCode() : 0);
        result = 31 * result + Arrays.hashCode(getChildren());
        return result;
    }

    public long[] getChildren() {
        return children;
    }

    public void setChildren(long[] children) {
        this.children = children;
    }
}
