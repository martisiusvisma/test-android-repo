package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 04/12/2015.
 */
public abstract class AbstractPicture {

    private String hash;
    private String format;
    private String url;
    private String caption;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public String toString() {
        return "AbstractPicture{" +
                "hash='" + hash + '\'' +
                ", format='" + format + '\'' +
                ", url='" + url + '\'' +
                ", caption='" + caption + '\'' +
                '}';
    }
}
