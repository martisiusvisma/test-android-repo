package com.kindiedays.common.pojo;

import org.joda.time.LocalTime;

/**
 * Created by pleonard on 10/11/2015.
 */
public class JournalSignin {

    public static final int SIGNIN = 1;
    public static final int SIGNOUT = 2;

    private int eventType;
    private LocalTime date;

    public JournalSignin(int eventType, LocalTime date){
        this.eventType = eventType;
        this.date = date;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public LocalTime getDate() {
        return date;
    }

    public void setDate(LocalTime date) {
        this.date = date;
    }
}
