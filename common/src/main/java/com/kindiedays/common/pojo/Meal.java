package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 11/06/2015.
 */
public class Meal {

    private Integer index;
    private String text;

    public Meal(Integer index, String text) {
        this.index = index;
        this.text = text;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
