package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public  class NapRes implements Serializable {
        private Integer child;
        private List<Integer> children;
        private DateTime date;
        private DateTime start;
        private Boolean local;
        private DateTime end;
        private String notes;

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public Boolean getLocal() {
        return local;
    }

    public void setLocal(Boolean local) {
        this.local = local;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Integer> getChildren() {
        return children;
    }

    public void setChildren(List<Integer> children) {
        this.children = children;
    }
}
