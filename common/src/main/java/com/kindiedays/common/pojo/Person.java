package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 14/09/2015.
 */
public abstract class Person extends ServerObject{
    protected String profilePictureUrl;
    protected String name;

    //From app
    private boolean selected;

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public abstract String getListName();
}
