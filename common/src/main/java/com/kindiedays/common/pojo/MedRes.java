package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public  class MedRes implements Serializable {
        private Integer child;
        private List<Integer> children;
        private DateTime date;
        private DateTime time;
        private Boolean local;
        private String drugName;
        private String dosage;
        private String notes;

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public Boolean getLocal() {
        return local;
    }

    public void setLocal(Boolean local) {
        this.local = local;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Integer> getChildren() {
        return children;
    }

    public void setChildren(List<Integer> children) {
        this.children = children;
    }
}