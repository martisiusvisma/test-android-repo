package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 29/06/2015.
 */
public class Kindergarten extends ServerObject{

    private String name;
    private String teacherEmail;
    private String managerEmail;
    private String timezone;
    private String profilePictureUrl;
    private String address;
    private String telephoneNumber;
    private String defaultAttendance;
    private int presenceLockDays;

    public static final String ABSENT = "ABSENT";
    public static final String PRESENT = "PRESENT";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getDefaultAttendance() {
        return defaultAttendance;
    }

    public void setDefaultAttendance(String defaultAttendance) {
        this.defaultAttendance = defaultAttendance;
    }

    public int getPresenceLockDays() {
        return presenceLockDays;
    }

    public void setPresenceLockDays(int presenceLockDays) {
        this.presenceLockDays = presenceLockDays;
    }
}
