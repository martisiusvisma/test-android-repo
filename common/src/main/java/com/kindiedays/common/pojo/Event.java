package com.kindiedays.common.pojo;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

/**
 * Created by pleonard on 15/04/2015.
 */
public class Event extends ServerObject implements Comparable<Event> {

    private LocalDate date;
    private LocalTime start;
    private LocalTime end;
    private String name;
    private String notes;
    private String location;
    private String state;
    private List<EventInvitation> invitations;
    private String interval;
    private LocalDate endRecurrence;


    public static final String NEVER = "NEVER";
    public static final String DAILY = "DAILY";
    public static final String WEEKLY = "WEEKLY";
    public static final String BIWEEKLY = "BIWEEKLY";
    public static final String MONTHLY = "MONTHLY";

    public static final String OPEN = "OPEN";
    public static final String CLOSED = "CLOSED";
    public static final String CANCELLED = "CANCELLED";


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<EventInvitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<EventInvitation> invitations) {
        this.invitations = invitations;
    }

    @Override
    public int compareTo(Event another) {
        int returnValue = this.date.compareTo(another.date);
        if (returnValue == 0) {
            return this.start.compareTo(another.start);
        } else {
            return returnValue;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Event event = (Event) o;

        if (!getDate().equals(event.getDate())) {
            return false;
        }
        if (!getStart().equals(event.getStart())) {
            return false;
        }
        if (!getEnd().equals(event.getEnd())) {
            return false;
        }
        if (!getName().equals(event.getName())) {
            return false;
        }
        if (!getNotes().equals(event.getNotes())) {
            return false;
        }
        if (!getLocation().equals(event.getLocation())) {
            return false;
        }
        if (!getState().equals(event.getState())) {
            return false;
        }
        if (!getInvitations().equals(event.getInvitations())) {
            return false;
        }
        if (!getInterval().equals(event.getInterval())) {
            return false;
        }
        return getEndRecurrence().equals(event.getEndRecurrence());

    }

    @Override
    public int hashCode() {
        int result = (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getStart() != null ? getStart().hashCode() : 0);
        result = 31 * result + (getEnd() != null ? getEnd().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getNotes() != null ? getNotes().hashCode() : 0);
        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (getInvitations() != null ? getInvitations().hashCode() : 0);
        result = 31 * result + (getInterval() != null ? getInterval().hashCode() : 0);
        result = 31 * result + (getEndRecurrence() != null ? getEndRecurrence().hashCode() : 0);
        return result;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public LocalDate getEndRecurrence() {
        return endRecurrence;
    }

    public void setEndRecurrence(LocalDate endRecurrence) {
        this.endRecurrence = endRecurrence;
    }

}
