package com.kindiedays.common.pojo;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by pleonard on 20/05/2015.
 */
@Parcel
public class ChildTag {

    private long childId;
    private long pictureId;
    private int x;
    private int y;
    private int width;
    private int height;
    private List<Long> carerIds;

    //hmm
    Boolean isPhotoConsent;

    public ChildTag() {
        //default
    }

    public ChildTag(int top, int left, int width, int height, long childId) {
        this.y = top;
        this.x = left;
        this.width = width;
        this.height = height;
        this.childId = childId;
    }

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public long getPictureId() {
        return pictureId;
    }

    public void setPictureId(long pictureId) {
        this.pictureId = pictureId;
    }

    public Boolean getPhotoConsent() {
        return isPhotoConsent;
    }

    public void setPhotoConsent(Boolean photoConsent) {
        isPhotoConsent = photoConsent;
    }

    public List<Long> getCarerIds() {
        return carerIds;
    }

    public void setCarerIds(List<Long> carerIds) {
        this.carerIds = carerIds;
    }
}
