package com.kindiedays.common.pojo.converters;


import android.os.Parcel;

import org.joda.time.DateTime;
import org.parceler.ParcelConverter;

public class JodaDateTimeConverter implements ParcelConverter<DateTime> {
    @Override
    public void toParcel(DateTime input, Parcel parcel) {
        parcel.writeLong(input.getMillis());
    }

    @Override
    public DateTime fromParcel(Parcel parcel) {
        return new DateTime(parcel.readLong());
    }
}
