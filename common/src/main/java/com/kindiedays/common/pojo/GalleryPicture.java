package com.kindiedays.common.pojo;

import java.util.List;

/**
 * Created by pleonard on 15/04/2015.
 */
public class GalleryPicture extends AbstractPicture {


    private String created;
    private int width;
    private int height;
    private String link; //self link
    private List<ChildTag> childTags;


    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public List<ChildTag> getChildTags() {
        return childTags;
    }

    public void setChildTags(List<ChildTag> childTags) {
        this.childTags = childTags;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "GalleryPicture{" +
                "created='" + created + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", link='" + link + '\'' +
                ", childTags=" + childTags +
                "} " + super.toString();
    }
}
