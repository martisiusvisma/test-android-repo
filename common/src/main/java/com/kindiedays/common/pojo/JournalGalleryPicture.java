package com.kindiedays.common.pojo;


import com.kindiedays.common.pojo.converters.JodaDateTimeConverter;

import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

@Parcel
public class JournalGalleryPicture extends AbstractPicture {
    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    private DateTime created;
    private Integer width;
    private Integer height;
    private String link;
    private List<ChildTag> children;

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<ChildTag> getChildren() {
        return children;
    }

    public void setChildren(List<ChildTag> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "JournalGalleryPicture{" +
                "created=" + created +
                ", width=" + width +
                ", height=" + height +
                ", link='" + link + '\'' +
                ", children=" + children +
                "} " + super.toString();
    }
}
