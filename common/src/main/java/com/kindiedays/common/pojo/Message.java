package com.kindiedays.common.pojo;

import com.kindiedays.common.pojo.converters.JodaDateTimeConverter;

import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by pleonard on 21/04/2015.
 */
@Parcel
public class Message {
    private long conversationId;
    private int index;
    private int authorIndex;

    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    private DateTime created;

    private String text;
    private List<Attachment> attachmentList;

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getAuthorIndex() {
        return authorIndex;
    }

    public void setAuthorIndex(int authorIndex) {
        this.authorIndex = authorIndex;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        this.attachmentList = attachmentList;
    }
}
