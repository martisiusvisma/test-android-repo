package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

/**
 * Created by pleonard on 03/11/2015.
 */
public class Nap {

    private long childId;
    private DateTime start;
    private DateTime end;

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Nap{" +
                "childId=" + childId +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
