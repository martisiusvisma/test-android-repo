package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class ActivityRes implements Serializable{
    private Integer child;
    private DateTime date;
    private DateTime created;
    private DateTime modified;
    private Long category;
    private String categoryName;
    private String text;
    private boolean valueUpdated;
    private boolean existsOnServer; //true -> update, false -> post

    public ActivityRes() {
    }

    public ActivityRes(Integer child, DateTime date, String text, Boolean existsOnServer) {
        this.child = child;
        this.date = date;
        this.text = text;
        this.existsOnServer = existsOnServer;
    }

    public ActivityRes(Integer child, DateTime date, DateTime created, Long category, String categoryName, String text) {
        this.child = child;
        this.date = date;
        this.created = created;
        this.category = category;
        this.categoryName = categoryName;
        this.text = text;

    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getModified() {
        return modified;
    }

    public void setModified(DateTime modified) {
        this.modified = modified;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isValueUpdated() {
        return valueUpdated;
    }

    public void setValueUpdated(boolean valueUpdated) {
        this.valueUpdated = valueUpdated;
    }

    public boolean isExistsOnServer(){
        return existsOnServer;
    }

    public void setExistsOnServer(boolean existsOnServer){this.existsOnServer = existsOnServer;}

    @Override
    public String toString() {
        return "ActivityRes{" +
                "child=" + child +
                ", date=" + date +
                ", created=" + created +
                ", modified=" + modified +
                ", category=" + category +
                ", categoryName='" + categoryName + '\'' +
                ", text='" + text + '\'' +
                ", valueUpdated=" + valueUpdated +
                ", existsOnServer=" + existsOnServer +
                '}';
    }
}
