package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by pleonard on 08/06/2015.
 */
public class NoteRes implements Serializable {

    private long child;
    private String text;
    private DateTime date;
    private DateTime created;
    private DateTime modified;

    public long getChild() {
        return child;
    }

    public void setChild(long child) {
        this.child = child;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getModified() {
        return modified;
    }

    public void setModified(DateTime modified) {
        this.modified = modified;
    }
}
