package com.kindiedays.common.pojo;

/**
 * Created by Giuseppe Franco - Starcut on 13/10/16.
 */

public class ConversationPatchResponse {
    private int messagesRead;
    private int lastReadMessageIndex;

    public int getMessagesRead() {
        return messagesRead;
    }

    public void setMessagesRead(int messagesRead) {
        this.messagesRead = messagesRead;
    }

    public int getLastReadMessageIndex() {
        return lastReadMessageIndex;
    }

    public void setLastReadMessageIndex(int lastReadMessageIndex) {
        this.lastReadMessageIndex = lastReadMessageIndex;
    }
}
