package com.kindiedays.common.pojo;

/**
 * Created by anduser on 29.01.18.
 */

public class AppMenuState {

    private long kindergartenId;
    private boolean messagesEnabled;
    private boolean eventsEnabled;
    private boolean mealsEnabled;
    private boolean calendarEnabled;
    private boolean cameraEnabled;
    private boolean journalEnabled;

    public long getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(long kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public boolean isMessagesEnabled() {
        return messagesEnabled;
    }

    public void setMessagesEnabled(boolean messagesEnabled) {
        this.messagesEnabled = messagesEnabled;
    }

    public boolean isEventsEnabled() {
        return eventsEnabled;
    }

    public void setEventsEnabled(boolean eventsEnabled) {
        this.eventsEnabled = eventsEnabled;
    }

    public boolean isMealsEnabled() {
        return mealsEnabled;
    }

    public void setMealsEnabled(boolean mealsEnabled) {
        this.mealsEnabled = mealsEnabled;
    }

    public boolean isCalendarEnabled() {
        return calendarEnabled;
    }

    public void setCalendarEnabled(boolean calendarEnabled) {
        this.calendarEnabled = calendarEnabled;
    }

    public boolean isCameraEnabled() {
        return cameraEnabled;
    }

    public void setCameraEnabled(boolean cameraEnabled) {
        this.cameraEnabled = cameraEnabled;
    }

    public boolean isJournalEnabled() {
        return journalEnabled;
    }

    public void setJournalEnabled(boolean journalEnabled) {
        this.journalEnabled = journalEnabled;
    }
}
