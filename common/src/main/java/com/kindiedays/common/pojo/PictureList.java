package com.kindiedays.common.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 14/07/2015.
 */
public class PictureList extends PaginatedResource {

    private List<GalleryPicture> pictures = new ArrayList<>();

    public List<GalleryPicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<GalleryPicture> pictures) {
        this.pictures = pictures;
    }
}
