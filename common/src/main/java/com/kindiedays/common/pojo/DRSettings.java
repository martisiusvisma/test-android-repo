package com.kindiedays.common.pojo;

import java.util.List;

/**
 * Created by pleonard on 05/08/2016.
 */
public class DRSettings {

    private List<CategoryRes> categoryResList;
    private int kindergartenId;
    private boolean enabled;
    private String selfLink;


    public List<CategoryRes> getCategoryResList() {
        return categoryResList;
    }

    public void setCategoryResList(List<CategoryRes> categoryResList) {
        this.categoryResList = categoryResList;
    }

    public int getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(int kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }
}
