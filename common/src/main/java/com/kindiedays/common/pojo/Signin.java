package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

/**
 * Created by pleonard on 03/11/2015.
 */
public class Signin {

    private long childId;
    private DateTime start;
    private DateTime end;
    private long placeId;
    private SigninType type;

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(long placeId) {
        this.placeId = placeId;
    }

    public SigninType getType() {
        return type;
    }

    public void setType(SigninType type) {
        this.type = type;
    }

    public void setType(String strType) {
        this.type = SigninType.valueOf(strType);
    }

    @Override
    public String toString() {
        return "Signin{" +
                "childId=" + childId +
                ", start=" + start +
                ", end=" + end +
                ", placeId=" + placeId +
                ", type=" + type +
                '}';
    }
}

