package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 10/06/2015.
 */
public class EventInvitation {

    public enum InvitationState {
        INVITED, CONFIRMED, DECLINED
    }


    private long eventId;
    private long childId;
    private InvitationState state;

    public long getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public InvitationState getState() {
        return state;
    }

    public void setState(InvitationState state) {
        this.state = state;
    }
}
