package com.kindiedays.common.pojo;

import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by pleonard on 11/06/2015.
 */
public class Menu {

    private LocalDate date;
    private List<Meal> meals;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "date=" + date +
                ", meals=" + meals +
                '}';
    }
}
