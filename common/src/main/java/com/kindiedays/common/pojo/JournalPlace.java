package com.kindiedays.common.pojo;


import org.joda.time.LocalTime;

public class JournalPlace {
    private long id;
    private String placeName;
    private LocalTime localTime;
    private Action action;

    public JournalPlace() {
        //default
    }

    public JournalPlace(long id, LocalTime localTime, Action action) {
        this.id = id;
        this.localTime = localTime;
        this.action = action;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public enum Action {
        ENTER,
        LEAVE
    }
}
