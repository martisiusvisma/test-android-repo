package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 15/04/2015.
 */
public class Group extends ChildContainer {

    private boolean dailyReportEnabled;

    public boolean isDailyReportEnabled() {
        return dailyReportEnabled;
    }

    public void setDailyReportEnabled(boolean dailyReportEnabled) {
        this.dailyReportEnabled = dailyReportEnabled;
    }
}
