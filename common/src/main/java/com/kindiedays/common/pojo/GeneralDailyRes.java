package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public  class GeneralDailyRes {
    private Integer child;
    private DateTime date;
    private List<Generic> generic;   // to handle all different categories as one

    public List<Generic> getGeneric() {
        return generic;
    }

    public void setGeneric(List<Generic> generic) {
        this.generic = generic;
    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
