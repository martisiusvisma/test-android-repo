package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pleonard on 15/04/2015.
 */
@SuppressWarnings("squid:S1948")
public class Child extends Person implements Comparable<Child>, Serializable {

    //From getChild
    private int groupId;
    private String nickname;

    private long kindergartenId;


    //From getStatus
    private long currentPlaceId;

    private ChildPresence presence;
    private long temporaryGroupId;
    private String temporaryGroupUrl;
    private int unreadMessages;

    private ChildProfile childProfile;
    private List<Long> carerIds;

    private DateTime startedNapping;


    public long getKindergartenId() {
        return kindergartenId;
    }

    public void setKindergartenId(long kindergartenId) {
        this.kindergartenId = kindergartenId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }


    public long getCurrentPlaceId() {
        return currentPlaceId;
    }

    public void setCurrentPlaceId(long currentPlaceId) {
        this.currentPlaceId = currentPlaceId;
    }

    public ChildPresence getPresence() {
        return presence;
    }

    public void setPresence(ChildPresence presence) {
        this.presence = presence;
    }

    public List<Long> getCarerIds() {
        return carerIds;
    }

    public void setCarerIds(List<Long> carerIds) {
        this.carerIds = carerIds;
    }

    @Override
    public int compareTo(Child another) {
        return this.nickname.compareTo(another.nickname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Child child = (Child) o;

        if (getGroupId() != child.getGroupId()) {
            return false;
        }
        if (getKindergartenId() != child.getKindergartenId()) {
            return false;
        }
        if (getCurrentPlaceId() != child.getCurrentPlaceId()) {
            return false;
        }
        if (getTemporaryGroupId() != child.getTemporaryGroupId()) {
            return false;
        }
        if (getUnreadMessages() != child.getUnreadMessages()) {
            return false;
        }
        if (getNickname() != null ? !getNickname().equals(child.getNickname()) : child.getNickname() != null) {
            return false;
        }
        if (getPresence() != null ? !getPresence().equals(child.getPresence()) : child.getPresence() != null) {
            return false;
        }
        if (getTemporaryGroupUrl() != null ? !getTemporaryGroupUrl().equals(child.getTemporaryGroupUrl()) : child.getTemporaryGroupUrl() != null) {
            return false;
        }
        if (getChildProfile() != null ? !getChildProfile().equals(child.getChildProfile()) : child.getChildProfile() != null) {
            return false;
        }
        return getStartedNapping() != null ? getStartedNapping().equals(child.getStartedNapping()) : child.getStartedNapping() == null;

    }

    @Override
    public int hashCode() {
        int result = getGroupId();
        result = 31 * result + (getNickname() != null ? getNickname().hashCode() : 0);
        result = 31 * result + (int) (getKindergartenId() ^ (getKindergartenId() >>> 32));
        result = 31 * result + (int) (getCurrentPlaceId() ^ (getCurrentPlaceId() >>> 32));
        result = 31 * result + (getPresence() != null ? getPresence().hashCode() : 0);
        result = 31 * result + (int) (getTemporaryGroupId() ^ (getTemporaryGroupId() >>> 32));
        result = 31 * result + (getTemporaryGroupUrl() != null ? getTemporaryGroupUrl().hashCode() : 0);
        result = 31 * result + getUnreadMessages();
        result = 31 * result + (getChildProfile() != null ? getChildProfile().hashCode() : 0);
        result = 31 * result + (getStartedNapping() != null ? getStartedNapping().hashCode() : 0);
        return result;
    }

    public long getTemporaryGroupId() {
        return temporaryGroupId;
    }

    public void setTemporaryGroupId(long temporaryGroupId) {
        this.temporaryGroupId = temporaryGroupId;
    }

    public String getTemporaryGroupUrl() {
        return temporaryGroupUrl;
    }

    public void setTemporaryGroupUrl(String temporaryGroupUrl) {
        this.temporaryGroupUrl = temporaryGroupUrl;
    }

    public boolean isNapping() {
        return startedNapping != null;
    }

    public void setStartedNapping(DateTime startedNapping) {
        this.startedNapping = startedNapping;
    }

    public DateTime getStartedNapping() {
        return startedNapping;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public ChildProfile getChildProfile() {
        return childProfile;
    }

    public void setChildProfile(ChildProfile childProfile) {
        this.childProfile = childProfile;
    }

    public String getListName() {
        return nickname != null ? nickname : name;
    }

}
