package com.kindiedays.common.pojo;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public  class DrinkRes {
        private Integer id;
        private Integer kindergarten;
        private String name;
        private Boolean enabled;

    public DrinkRes(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(Integer kindergarten) {
        this.kindergarten = kindergarten;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
