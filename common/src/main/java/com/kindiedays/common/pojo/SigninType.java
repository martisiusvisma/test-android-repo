package com.kindiedays.common.pojo;


public enum  SigninType {
    INITIAL,
    INTERMEDIARY,
    TERMINAL
}
