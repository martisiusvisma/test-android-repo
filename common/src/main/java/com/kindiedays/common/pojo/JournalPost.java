package com.kindiedays.common.pojo;

import android.util.Log;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by pleonard on 10/06/2015.
 */
public class JournalPost extends ServerObject{

    private String text;
    private List<JournalPostPicture> journalPostPictures;
    private DateTime time;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<JournalPostPicture> getJournalPostPictures() {
        return journalPostPictures;
    }

    public void setJournalPostPictures(List<JournalPostPicture> journalPostPictures) {
        this.journalPostPictures = journalPostPictures;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "JournalPost{" +
                "text='" + text + '\'' +
                ", journalPostPictures=" + journalPostPictures +
                ", time=" + time +
                "} " + super.toString();
    }
}
