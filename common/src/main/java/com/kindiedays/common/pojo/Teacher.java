package com.kindiedays.common.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 30/04/2015.
 */
public class Teacher extends Person {

    private Integer unreadMessages;
    private List<Long> groups = new ArrayList<>();
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Teacher){
            return (this.getId() == ((Teacher) o).getId());
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = unreadMessages.hashCode();
        result = 31 * result + groups.hashCode();
        return result;
    }

    public Integer getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(Integer unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public String getListName(){
        return name;
    }

    public List<Long> getGroups() {
        return groups;
    }
}
