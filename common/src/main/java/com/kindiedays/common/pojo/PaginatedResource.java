package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 16/07/2015.
 */
public abstract class PaginatedResource {

    private String linkNext;
    private String linkPrevious;

    public String getLinkNext() {
        return linkNext;
    }

    public void setLinkNext(String linkNext) {
        this.linkNext = linkNext;
    }

    public String getLinkPrevious() {
        return linkPrevious;
    }

    public void setLinkPrevious(String linkPrevious) {
        this.linkPrevious = linkPrevious;
    }


}
