package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 28/01/2016.
 */
public class Session {

    private String name;
    private String email;
    private String type;
    private String kindergartenLink;
    private String selfLink;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKindergartenLink() {
        return kindergartenLink;
    }

    public void setKindergartenLink(String kindergartenLink) {
        this.kindergartenLink = kindergartenLink;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }
}
