package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 20/05/2015.
 */
public class PendingFile {

    private String id;
    private String uploadUrl;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }
}
