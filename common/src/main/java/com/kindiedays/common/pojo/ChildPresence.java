package com.kindiedays.common.pojo;

import org.joda.time.LocalDate;

import java.io.Serializable;

/**
 * Created by pleonard on 15/05/2015.
 */
public class ChildPresence implements Serializable {

    private LocalDate start;
    private LocalDate end;
    private long carerId;
    private long childId;
    private String status;

    public static final String ABSENT_STATUS = "ABSENT";
    public static final String PRESENT_STATUS = "PRESENT";

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public long getCarerId() {
        return carerId;
    }

    public void setCarerId(long carerId) {
        this.carerId = carerId;
    }

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
