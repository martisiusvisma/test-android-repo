package com.kindiedays.common.pojo;

import com.kindiedays.common.model.BlurredImageWrapper;

import org.parceler.Parcel;

@Parcel
public class Attachment {
    private String pendingFileId;
    private String filename;
    private String format;
    private String uri;

    //for view
    private int authorIndex;
    private int messageIndex;
    //for blur
    private BlurredImageWrapper image;

    public Attachment() {
        //default
    }

    public Attachment(String pendingFileId, String filename, String format) {
        this.pendingFileId = pendingFileId;
        this.filename = filename;
        this.format = format;
    }

    public String getPendingFileId() {
        return pendingFileId;
    }

    public void setPendingFileId(String pendingFileId) {
        this.pendingFileId = pendingFileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getAuthorIndex() {
        return authorIndex;
    }

    public void setAuthorIndex(int authorIndex) {
        this.authorIndex = authorIndex;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getMessageIndex() {
        return messageIndex;
    }

    public void setMessageIndex(int messageIndex) {
        this.messageIndex = messageIndex;
    }

    public BlurredImageWrapper getImage() {
        return image;
    }

    public void setImage(BlurredImageWrapper image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "pendingFileId='" + pendingFileId + '\'' +
                ", filename='" + filename + '\'' +
                ", format='" + format + '\'' +
                ", uri='" + uri + '\'' +
                ", authorIndex=" + authorIndex +
                ", messageIndex=" + messageIndex +
                ", image=" + image +
                '}';
    }
}
