package com.kindiedays.common.pojo;

/**
 * Created by pleonard on 28/05/2015.
 */
public class ConversationParty extends Person{

    private String type;
    private long partyId;
    private int index;
    private long conversationId;

    public static final String CARER_TYPE = "CARER";
    public static final String TEACHER_TYPE = "TEACHER";
    public static final String KINDERGARTEN_TYPE = "KINDERGARTEN";

    @Override
    public String getListName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getPartyId() {
        return partyId;
    }

    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

}
