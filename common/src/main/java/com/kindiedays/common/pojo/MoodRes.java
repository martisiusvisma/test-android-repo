package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class MoodRes implements Serializable, Comparable<MoodRes> {

    private Integer id;
    private Integer index;
    private Integer child;
    private List<Integer> children;
    private DateTime date;
    private DateTime time;
    private Boolean local;
    private String temper; // Optional
    private String temperDescription;
    private String notes;   // Optional

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public Boolean getLocal() {
        return local;
    }

    public void setLocal(Boolean locale) {
        this.local = locale;
    }

    public String getTemper() {
        return temper;
    }

    public void setTemper(String temper) {
        this.temper = temper;
    }

    public String getTemperDescription() {
        return temperDescription;
    }

    public void setTemperDescription(String temperDescription) {
        this.temperDescription = temperDescription;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int compareTo(MoodRes o) {
        if (getIndex() == null || o.getIndex() == null) {
            return 0;
        }
        return getIndex().compareTo(o.getIndex());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MoodRes moodRes = (MoodRes) o;

        if (!id.equals(moodRes.id)) {
            return false;
        }
        if (!index.equals(moodRes.index)) {
            return false;
        }
        if (!child.equals(moodRes.child)) {
            return false;
        }
        if (!children.equals(moodRes.children)) {
            return false;
        }
        if (!date.equals(moodRes.date)) {
            return false;
        }
        if (!time.equals(moodRes.time)) {
            return false;
        }
        if (!local.equals(moodRes.local)) {
            return false;
        }
        if (!temper.equals(moodRes.temper)) {
            return false;
        }
        return notes.equals(moodRes.notes);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + index.hashCode();
        result = 31 * result + child.hashCode();
        result = 31 * result + children.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + local.hashCode();
        result = 31 * result + temper.hashCode();
        result = 31 * result + notes.hashCode();
        return result;
    }

    public List<Integer> getChildren() {
        return children;
    }

    public void setChildren(List<Integer> children) {
        this.children = children;
    }
}
