package com.kindiedays.common.pojo;

import java.util.List;

/**
 * Created by pleonard on 27/05/2015.
 */
public class ChildCarer extends ServerObject {

    private long childId;
    private String email;
    private String type;
    private boolean pickupPermission;
    private String name;
    private String occupation;
    private String placeOfWork;
    private Integer unreadMessages;
    private List<Couple> childrenUnreadMessages;
    private Integer unreadEvents;
    private List<Couple> childrenUnreadEvents;
    private String workPhoneNumber;
    private String homePhoneNumber;
    private String mobilePhoneNumber;
    private String nativeLanguage;
    private boolean readonly;
    private boolean defaultConversationParty;
    private boolean contactDetailsPublic;


    public Integer getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(Integer unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public Integer getUnreadEvents() {
        return unreadEvents;
    }

    public void setUnreadEvents(Integer unreadEvents) {
        this.unreadEvents = unreadEvents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isPickupPermission() {
        return pickupPermission;
    }

    public void setPickupPermission(boolean pickupPermission) {
        this.pickupPermission = pickupPermission;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPlaceOfWork() {
        return placeOfWork;
    }

    public void setPlaceOfWork(String placeOfWork) {
        this.placeOfWork = placeOfWork;
    }

    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(String nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public boolean isDefaultConversationParty() {
        return defaultConversationParty;
    }

    public void setDefaultConversationParty(boolean defaultConversationParty) {
        this.defaultConversationParty = defaultConversationParty;
    }

    public boolean isContactDetailsPublic() {
        return contactDetailsPublic;
    }

    public void setContactDetailsPublic(boolean contactDetailsPublic) {
        this.contactDetailsPublic = contactDetailsPublic;
    }

    public List<Couple> getChildrenUnreadMessages() {
        return childrenUnreadMessages;
    }

    public void setChildrenUnreadMessages(List<Couple> childrenUnreadMessages) {
        this.childrenUnreadMessages = childrenUnreadMessages;
    }

    public List<Couple> getChildrenUnreadEvents() {
        return childrenUnreadEvents;
    }

    public void setChildrenUnreadEvents(List<Couple> childrenUnreadEvents) {
        this.childrenUnreadEvents = childrenUnreadEvents;
    }

    public Integer getUnreadMessagesActiveChild(int childId){
        for (int i=0;i<childrenUnreadMessages.size();i++){
            int temp = Integer.parseInt(childrenUnreadMessages.get(i).getChildId());
            if (temp == childId){
                return childrenUnreadMessages.get(i).getValue();
            }
        }
        return 0;
    }

    public Integer getUnreadEventsActiveChild(int childId){
        for (int i=0;i<childrenUnreadEvents.size();i++){
            int temp = Integer.parseInt(childrenUnreadEvents.get(i).getChildId());
            if (temp == childId){
                return childrenUnreadEvents.get(i).getValue();
            }
        }
        return 0;
    }
}
