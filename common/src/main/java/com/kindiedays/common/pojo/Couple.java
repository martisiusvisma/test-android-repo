package com.kindiedays.common.pojo;

/**
 * Created by Giuseppe Franco - Starcut on 14/10/16.
 */

public class Couple {
    private String childId;
    private Integer value;

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
