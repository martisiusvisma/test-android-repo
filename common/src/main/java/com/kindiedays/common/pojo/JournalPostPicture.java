package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

/**
 * Created by pleonard on 08/07/2015.
 */
public class JournalPostPicture extends AbstractPicture {

    private DateTime created;

    private JournalPost journalPost;

    private int nbPictures;

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public JournalPost getJournalPost() {
        return journalPost;
    }

    public void setJournalPost(JournalPost journalPost) {
        this.journalPost = journalPost;
    }

    public int getNbPictures() {
        return nbPictures;
    }

    public void setNbPictures(int nbPictures) {
        this.nbPictures = nbPictures;
    }
}
