package com.kindiedays.common.pojo;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class CategoryRes {
    private Integer id;
    private String name;
    private Integer priority;
    private Boolean enabled;

    public Integer getId() {
        return id;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
