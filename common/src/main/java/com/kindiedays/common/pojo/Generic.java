package com.kindiedays.common.pojo;

import org.joda.time.DateTime;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class Generic {
    private DateTime time;
    private String type;
    private String row1;
    private String row2;

    private ActivityRes activity;
    private  BathRes bath;
    private  MealRes meal;
    private  MedRes med;
    private  MoodRes mood;
    private  NapRes nap;
    private  NoteRes note;

    public Generic(DateTime time, String type, String row1, String row2) {
        super();
        this.time = time;
        this.type = type;
        this.row1 = row1;
        this.row2 = row2;
    }

    public ActivityRes getActivity() {
        return activity;
    }

    public void setActivity(ActivityRes activity) {
        this.activity = activity;
    }

    public BathRes getBath() {
        return bath;
    }

    public void setBath(BathRes bath) {
        this.bath = bath;
    }

    public MealRes getMeal() {
        return meal;
    }

    public void setMeal(MealRes meal) {
        this.meal = meal;
    }

    public MedRes getMed() {
        return med;
    }

    public void setMed(MedRes med) {
        this.med = med;
    }

    public MoodRes getMood() {
        return mood;
    }

    public void setMood(MoodRes mood) {
        this.mood = mood;
    }

    public NapRes getNap() {
        return nap;
    }

    public void setNap(NapRes nap) {
        this.nap = nap;
    }

    public NoteRes getNote() {
        return note;
    }

    public void setNote(NoteRes note) {
        this.note = note;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRow1() {
        return row1;
    }

    public void setRow1(String row1) {
        this.row1 = row1;
    }

    public String getRow2() {
        return row2;
    }

    public void setRow2(String row2) {
        this.row2 = row2;
    }
}
