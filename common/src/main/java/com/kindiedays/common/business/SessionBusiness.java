package com.kindiedays.common.business;

import android.os.AsyncTask;
import android.util.Log;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.session.GetSession;
import com.kindiedays.common.network.session.PatchSession;
import com.kindiedays.common.network.session.PostSession;
import com.kindiedays.common.pojo.Session;

/**
 * Created by pleonard on 25/09/2015.
 */
public class SessionBusiness {

    private SessionBusiness() {
        //no instance
    }

    public static class PatchSessionStarflightUuidTask extends NetworkAsyncTask<Void> {

        protected String uuid;

        public PatchSessionStarflightUuidTask(String uuid) {
            this.uuid = uuid;
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            PatchSession.patchStarflightUuid(uuid);
            return null;
        }
    }


    public static class GetSessionTask extends NetworkAsyncTask<Session> {

        @Override
        protected Session doNetworkTask() throws Exception {
            return GetSession.getSession();
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public static class UserLoginTask extends AsyncTask<Void, Void, String> {

        protected final String mEmail;
        protected final String mPassword;
        protected final boolean mEnvironment;

        protected KindieDaysNetworkException exception;

        public UserLoginTask(String email, String password, boolean environment) {
            mEmail = email;
            mPassword = password;
            mEnvironment = environment;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return PostSession.login(mEmail, mPassword, KindieDaysApp.getApp().getString(R.string.realm), mEnvironment);
            } catch (Exception e2) {
                Log.e(UserLoginTask.class.getName(), "Unexpected exception", e2);
                return null;
            }
        }
    }
}

