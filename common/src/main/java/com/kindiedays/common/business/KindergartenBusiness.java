package com.kindiedays.common.business;

import android.support.annotation.Nullable;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.kindergartens.GetKindergartens;
import com.kindiedays.common.pojo.Kindergarten;

import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by pleonard on 14/08/2015.
 */
public class KindergartenBusiness extends Observable {

    private List<Kindergarten> kindergartens = new ArrayList<>();
    private DateTimeZone timeZone = DateTimeZone.UTC;

    private static KindergartenBusiness instance;

    private KindergartenBusiness() {
    }

    public static KindergartenBusiness getInstance() {
        if (instance == null) {
            instance = new KindergartenBusiness();
        }
        return instance;
    }

    @Nullable
    public Kindergarten getKindergarten() {
        return kindergartens.isEmpty() ? null : kindergartens.get(0);
    }

    public Kindergarten getKindergarten(long id) {
        for (Kindergarten kindergarten : kindergartens) {
            if (id == kindergarten.getId()) {
                return kindergarten;
            }
        }
        return null;
    }

    public DateTimeZone getTimeZone() {
        return timeZone;
    }

    public void loadKindergartens() {
        LoadKindergartenDataTask task = new LoadKindergartenDataTask();
        task.execute();
    }

    public static class LoadKindergartenDataTask extends NetworkAsyncTask<List<Kindergarten>> {

        @Override
        protected void onPostExecute(List<Kindergarten> kindergartens) {
            getInstance().setChanged();
            getInstance().notifyObservers(kindergartens);
        }

        @Override
        protected List<Kindergarten> doNetworkTask() throws Exception {
            List<Kindergarten> kindergartens = GetKindergartens.getKindergartens();
            getInstance().kindergartens = kindergartens;
            DateTimeZone dtz = null;
            if (kindergartens != null) {
                dtz = DateTimeZone.forID(kindergartens.get(0).getTimezone());
            }
            if (dtz == null) {
                dtz = DateTimeZone.UTC;
            }
            getInstance().timeZone = dtz;
            return getInstance().kindergartens;
        }
    }

}
