package com.kindiedays.common.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.appmenu.GetAppMenuState;
import com.kindiedays.common.pojo.AppMenuState;

/**
 * Created by anduser on 29.01.18.
 */

public class AppMenuBusiness {

    private AppMenuState appMenuStates;
    protected static AppMenuBusiness instance;

    private AppMenuBusiness() {}

    public AppMenuState getMenuStates() {
        return appMenuStates;
    }

    public static AppMenuBusiness getInstance() {
        if (instance == null) {
            instance = new AppMenuBusiness();
        }
        return instance;
    }

    public static class LoadAppMenuStates extends NetworkAsyncTask<AppMenuState> {
        private Long kindergartenId;

        public LoadAppMenuStates(Long kindergartenId) {
            this.kindergartenId = kindergartenId;
        }

        @Override
        protected AppMenuState doNetworkTask() throws Exception {
            getInstance().appMenuStates = GetAppMenuState.getAppMenuStates(kindergartenId);
            return getInstance().appMenuStates;
        }
    }
}
