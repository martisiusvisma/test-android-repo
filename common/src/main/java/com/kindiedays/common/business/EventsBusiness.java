package com.kindiedays.common.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.events.GetEvents;
import com.kindiedays.common.network.events.PostEvents;
import com.kindiedays.common.network.events.PutEvents;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

/**
 * Created by pleonard on 10/06/2015.
 */
public class EventsBusiness extends Observable {

    private static EventsBusiness instance;
    private List<Event> events = new ArrayList<>();

    public static EventsBusiness getInstance() {
        if (instance == null) {
            instance = new EventsBusiness();
        }
        return instance;
    }

    public List<Event> getPastEvents() {
        List<Event> pastEvents = new ArrayList<>();
        for (Event event : events) {
            if (isPastEvent(event)) {
                pastEvents.add(event);
            }
        }
        Collections.sort(pastEvents);
        Collections.reverse(pastEvents);
        return pastEvents;
    }

    public boolean isPastEvent(Event event) {
        LocalDateTime now = new LocalDateTime();
        LocalDateTime finalEvent = null;
        if (event.getInterval() == null) {
            finalEvent = new LocalDateTime(event.getDate().getYear(),
                    event.getDate().getMonthOfYear(),
                    event.getDate().getDayOfMonth(),
                    event.getEnd().getHourOfDay(),
                    event.getEnd().getMinuteOfHour());
        } else {
            List<LocalDateTime> allOccurences = getAllOccurences(event);
            if (!allOccurences.isEmpty()) {
                finalEvent = allOccurences.get(allOccurences.size() - 1);
            }
        }
        return finalEvent == null || finalEvent.isBefore(now);
    }

    public LocalDateTime getLastOccurence(Event event) {
        LocalDateTime now = new LocalDateTime();
        List<LocalDateTime> allOccurences = getAllOccurences(event);
        for (int i = 0; i < allOccurences.size(); i++) {
            if (allOccurences.get(i).isBefore(now)) {
                return allOccurences.get(i);
            }
        }
        return getLocalTimeFromEvent(event);
    }

    public LocalDateTime getNextOccurence(Event event) {
        LocalDateTime now = new LocalDateTime();
        List<LocalDateTime> allOccurences = getAllOccurences(event);
        for (int i = 0; i < allOccurences.size(); i++) {
            if (allOccurences.get(i).isAfter(now)) {
                return allOccurences.get(i);
            }
        }
        return getLocalTimeFromEvent(event);
    }


    public List<LocalDateTime> getAllOccurences(Event event) {
        List<LocalDateTime> list = new ArrayList<>();
        LocalDateTime currentOccurence = getLocalTimeFromEvent(event);
        LocalDate endRecurrence = event.getEndRecurrence();
        if (endRecurrence == null) {
            endRecurrence = currentOccurence.toLocalDate().plusDays(1);
        }
        LocalDateTime endOccurenceTime = endRecurrence.toLocalDateTime(event.getStart());
        while (currentOccurence.isBefore(endOccurenceTime)) {
            list.add(currentOccurence);
            //Calculate next occurence
            switch (event.getInterval()) {
                case Event.DAILY:
                    currentOccurence = currentOccurence.plusDays(1);
                    break;
                case Event.WEEKLY:
                    currentOccurence = currentOccurence.plusWeeks(1);
                    break;
                case Event.BIWEEKLY:
                    currentOccurence = currentOccurence.plusWeeks(2);
                    break;
                case Event.MONTHLY:
                    //Probably should be redefined
                    currentOccurence = currentOccurence.plusMonths(1);
                    break;
                default:
                    break;
            }
        }
        return list;
    }

    public List<Event> getComingUpEvents(long childId) {
        List<Event> comingUpEvents = new ArrayList<>();
        for (Event event : events) {
            if (childId != -1) {
                List<EventInvitation> invitation = event.getInvitations();
                for (int i = 0; i < invitation.size(); i++) {
                    if (invitation.get(i).getChildId() == childId && !isPastEvent(event)) {
                        comingUpEvents.add(event);
                    }
                }
            } else if (!isPastEvent(event)) {
                comingUpEvents.add(event);
            }
        }

        Collections.sort(comingUpEvents);
        return comingUpEvents;
    }

    public List<Event> getPastEvents(long childId) {
        List<Event> pastEvents = new ArrayList<>();
        for (Event event : events) {
            fillEvents(event, childId, pastEvents);
        }

        Collections.sort(pastEvents);
        Collections.reverse(pastEvents);
        return pastEvents;
    }

    private void fillEvents(Event event, long childId, List<Event> pastEvents) {
        if (childId != -1) {
            List<EventInvitation> invitations = event.getInvitations();
            for (int i = 0; i < invitations.size(); i++) {
                if (invitations.get(i).getChildId() == childId && isPastEvent(event)) {
                    pastEvents.add(event);
                }
            }
        } else if (isPastEvent(event)) {
            pastEvents.add(event);
        }
    }

    public List<Event> getEvents() {
        return events;
    }

    public Event getEvent(long id) {
        for (Event event : events) {
            if (event.getId() == id) {
                return event;
            }
        }
        return null;
    }

    private LocalDateTime getLocalTimeFromEvent(Event event) {
        return new LocalDateTime(event.getDate().getYear(),
                event.getDate().getMonthOfYear(),
                event.getDate().getDayOfMonth(),
                event.getEnd().getHourOfDay(),
                event.getEnd().getMinuteOfHour());
    }

    public static class GetEventsTask extends NetworkAsyncTask<List<Event>> {
        @Override
        protected void onPostExecute(List<Event> events) {
            super.onPostExecute(events);
            getInstance().setChanged();
            getInstance().notifyObservers();
        }

        @Override
        protected List<Event> doNetworkTask() throws Exception {
            List<Event> events = GetEvents.getEventList();
            getInstance().events = events;
            return events;
        }
    }


    public static class SaveEventTask extends NetworkAsyncTask<Event> {

        Event event;

        public SaveEventTask(Event event) {
            this.event = event;
        }

        @Override
        protected void onPostExecute(Event event) {
            super.onPostExecute(event);
            getInstance().setChanged();
            getInstance().notifyObservers();
        }

        @Override
        protected Event doNetworkTask() throws Exception {
            if (event.getId() == 0) {
                getInstance().events.add(event);
                return PostEvents.postEvent(event);
            } else {
                PutEvents.updateEvent(event);
                return event;
            }

        }
    }

}
