package com.kindiedays.common.business;

import android.util.Log;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.journalposts.GetChildJurnalPosts;
import com.kindiedays.common.pojo.ChildJournalList;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.JournalGalleryPicture;
import com.kindiedays.common.pojo.JournalPlace;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.pojo.JournalPostPicture;
import com.kindiedays.common.pojo.JournalSignin;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.pojo.Nap;
import com.kindiedays.common.pojo.Signin;
import com.kindiedays.common.pojo.SigninType;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by pleonard on 14/09/2015.
 */
public class JournalBusiness {
    private static final String TAG = JournalBusiness.class.getSimpleName();
    protected ChildJournalList currentPictureList;

    private String afterUrl;
    private String previousUrl;
    private boolean allEntriesLoaded;

    public static final int AFTER = 1;
    public static final int BEFORE = 2;
    private static final long SIGN_IN_OFFSET = 2L;

    private TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPostMap = new TreeMap<>();
    private Map<Long, JournalPost> journalPostPerId = new HashMap<>();

    private static JournalBusiness instance;

    public String getAfterUrl() {
        return afterUrl;
    }

    public String getPreviousUrl() {
        return previousUrl;
    }

    public static JournalBusiness getInstance() {
        if (instance == null) {
            instance = new JournalBusiness();
        }
        return instance;
    }

    private TreeMap<LocalTime, Object> getLocalDateMap(LocalDate localDate) {
        if (journalPostMap.get(localDate) == null) {
            journalPostMap.put(localDate, new TreeMap<LocalTime, Object>());
        }
        return journalPostMap.get(localDate);
    }

    public void populateMap(ChildJournalList childJournalList) {
        for (Event event : childJournalList.getEvents()) {
            SortedMap<LocalTime, Object> localDateMap = getLocalDateMap(event.getDate());
            localDateMap.put(event.getStart(), event);
        }
        for (JournalPost journalPost : childJournalList.getJournalPosts()) {
            SortedMap<LocalTime, Object> localDateMap = getLocalDateMap(journalPost.getTime().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toLocalDate());
            localDateMap.put(journalPost.getTime().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toLocalTime(), journalPost);
            journalPostPerId.put(journalPost.getId(), journalPost);
        }
        for (Nap nap : childJournalList.getNaps()) {
            SortedMap<LocalTime, Object> localDateMap = getLocalDateMap(nap.getStart().toLocalDate());
            localDateMap.put(nap.getStart().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toLocalTime(), nap);
        }

        //Temporary map with signins per day
        SortedMap<LocalDate, List<Signin>> signins = new TreeMap<>();
        for (Signin signin : childJournalList.getSignins()) {
            if (signins.get(signin.getStart().toLocalDate()) == null) {
                signins.put(signin.getStart().toLocalDate(), new ArrayList<>());
            }
            if (signin.getEnd() != null && signins.get(signin.getEnd().toLocalDate()) == null) {
                signins.put(signin.getEnd().toLocalDate(), new ArrayList<>());
            }
            signins.get(signin.getStart().toLocalDate()).add(signin);
            if (signin.getEnd() != null && !signins.get(signin.getEnd().toLocalDate()).contains(signin)) {
                signins.get(signin.getEnd().toLocalDate()).add(signin);
            }
        }

        //loop on every day
        DateTimeZone dtz = DateTimeZone.getDefault();
        long timeInstance = DateTime.now().getMillis();
        long offset = dtz.getOffset(timeInstance);

        for (JournalGalleryPicture picture : childJournalList.getGalleryPictures()) {
            LocalDate date = dateTimeToLocalDate(picture.getCreated(), offset);
            SortedMap<LocalTime, Object> localDateMap = getLocalDateMap(date);
            localDateMap.put(dateTimeToLocalTime(picture.getCreated(), offset), picture);
        }

        LocalDate[] keys = signins.keySet().toArray(new LocalDate[signins.size()]);
        for (int j = 0; j < keys.length; j++) {
            List<Signin> daySignins = signins.get(keys[j]);

            int signinSize = daySignins.size();
            for (int i = 0; i < signinSize; i++) {
                Signin signin = daySignins.get(i);
                if (signin.getType() == SigninType.INITIAL) {
                    //offset on 1 millisecond to get rid of replacing singings due the same key
                    putJournalSignin(signin.getStart(), offset - SIGN_IN_OFFSET, JournalSignin.SIGNIN);
                } else if (signin.getType() == SigninType.TERMINAL) {
                    if ((i != 0 && daySignins.get(i - 1).getType() == SigninType.TERMINAL) || i == 0) {
                        putJournalSignin(signin.getStart(), offset - SIGN_IN_OFFSET, JournalSignin.SIGNIN);
                    }
                    putJournalSignin(signin.getEnd(), offset + SIGN_IN_OFFSET, JournalSignin.SIGNOUT);
                }

                putJournalPlace(signin.getStart(), offset, signin.getPlaceId(), JournalPlace.Action.ENTER);
            }
        }
        for (Menu menu : childJournalList.getMenus()) {
            TreeMap<LocalTime, Object> localDateMap = getLocalDateMap(menu.getDate());
            localDateMap.put(new LocalTime(), menu);
        }
    }

    private void putJournalSignin(DateTime time, long offset, int eventType) {
        DateTime zonedTime = time.plus(offset);
        SortedMap<LocalTime, Object> localDateMap = getLocalDateMap(zonedTime.toLocalDate());
        LocalTime localTime = zonedTime.toLocalTime();
        localDateMap.put(localTime, new JournalSignin(eventType, localTime));
    }

    private void putJournalPlace(DateTime time, long offset, long placeId, JournalPlace.Action action) {
        DateTime zonedTime = time.plus(offset);
        SortedMap<LocalTime, Object> localDateMap = getLocalDateMap(zonedTime.toLocalDate());
        LocalTime localTime = zonedTime.toLocalTime();
        localDateMap.put(localTime, new JournalPlace(placeId, localTime, action));
    }

    private static LocalDate dateTimeToLocalDate(DateTime dateTime, long offset) {
        DateTime zonedTime = dateTime.plus(offset);
        return zonedTime.toLocalDate();
    }

    private static LocalTime dateTimeToLocalTime(DateTime dateTime, long offset) {
        DateTime zonedTime = dateTime.plus(offset);
        return zonedTime.toLocalTime();
    }

    @SuppressWarnings("squid:S1319")
    public TreeMap<LocalDate, TreeMap<LocalTime, Object>> getJournalPostMap() {
        return journalPostMap;
    }

    public List<JournalPostPicture> getJournalPostPictures(long journalPostId) {
        return journalPostPerId.get(journalPostId).getJournalPostPictures();
    }

    public int getIndexPicture(JournalPostPicture journalPostPicture) {
        List<JournalPostPicture> pictures = getJournalPostPictures(journalPostPicture.getJournalPost().getId());
        for (int i = 0; i < pictures.size(); i++) {
            if (pictures.get(i).getHash().equals(journalPostPicture.getHash())) {
                return i;
            }
        }
        return 0;
    }

    public boolean isAllEntriesLoaded() {
        return allEntriesLoaded;
    }

    public static class GetChildJournalPostsAsyncTask extends NetworkAsyncTask<ChildJournalList> {

        long childId;

        public GetChildJournalPostsAsyncTask(long childId) {
            this.childId = childId;
        }

        @Override
        protected ChildJournalList doNetworkTask() throws Exception {
            getInstance().allEntriesLoaded = false;
            //ChildJournalList childJournalList = GetChildJournalPosts.getPosts(childId);
            ChildJournalList childJournalListFixed = GetChildJurnalPosts.getPosts(childId);
            Log.d("GetPaginatedResource", "doNetworkTask: childJournalListFixed: " + childJournalListFixed);
            getInstance().journalPostMap.clear();
            getInstance().populateMap(childJournalListFixed);
            getInstance().afterUrl = childJournalListFixed.getLinkNext();
            getInstance().previousUrl = childJournalListFixed.getLinkPrevious();

            return childJournalListFixed;
        }
    }

    public static class GetMoreChildJournalPostsAsyncTask extends NetworkAsyncTask<ChildJournalList> {

        protected int direction;

        public GetMoreChildJournalPostsAsyncTask(int direction) {
            this.direction = direction;
        }

        @Override
        protected ChildJournalList doNetworkTask() throws Exception {
            String link = "";
            if (direction == AFTER) {
                link = getInstance().afterUrl;

            } else if (direction == BEFORE) {
                link = getInstance().previousUrl;

            }
            ChildJournalList childJournalList = null;
            if (link != null) {
                childJournalList = GetChildJurnalPosts.getPosts(link);
                Log.d(TAG, "doNetworkTask: more  " + childJournalList);
            }

            if (childJournalList != null) {
                getInstance().populateMap(childJournalList);
                if (direction == AFTER) {
                    getInstance().afterUrl = childJournalList.getLinkNext();

                } else if (direction == BEFORE) {
                    getInstance().previousUrl = childJournalList.getLinkPrevious();

                }
            } else {
                getInstance().allEntriesLoaded = true;
            }
            return childJournalList;
        }
    }
}
