package com.kindiedays.common.business;

import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.album.GetPictures;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.PictureList;

import java.io.File;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pleonard on 14/07/2015.
 */
public class PicturesBusiness {
    public static final int ALL_GROUPS = -1;

    protected PictureList currentPictureList;
    protected Map<String, GalleryPicture> pictureMap = new HashMap<>();

    private static PicturesBusiness instance;

    public static PicturesBusiness getInstance() {
        if (instance == null) {
            instance = new PicturesBusiness();
        }
        return instance;
    }

    public PictureList getCurrentPictureList() {
        return currentPictureList;
    }

    public Map<String, GalleryPicture> getPictureMap() {
        return pictureMap;
    }

    public void setNewPictures(List<File> files) {
        List<GalleryPicture> pictures = new ArrayList<>();
        pictureMap.clear();
        for (File file : files) {
            GalleryPicture picture = new GalleryPicture();
            picture.setUrl(Uri.fromFile(file).toString());
            picture.setChildTags(new ArrayList<>());
            pictures.add(picture);

        }
        populateMap(pictureMap, pictures);
    }

    private void populateMap(Map<String, GalleryPicture> pictureMap, List<GalleryPicture> pictures) {
        for (GalleryPicture picture : pictures) {
            if (picture.getLink() != null) {
                pictureMap.put(picture.getLink(), picture);
            } else {
                pictureMap.put(picture.getUrl(), picture);
            }

        }
    }

    /**
     * The key in this map should be picture.link if the picture comes from the server, or picture.url if it is local
     *
     * @param key: picture.link if available, picture.url otherwise
     * @return picture or null
     */
    public GalleryPicture getPictureFromKey(String key) {
        return pictureMap.get(key);
    }

    public static class GetMorePicturesAsyncTask extends NetworkAsyncTask<PictureList> {

        protected String url;

        public GetMorePicturesAsyncTask(String url) {
            this.url = url;
        }

        protected PictureList doNetworkTask() throws Exception {
            try {
                PictureList pictureList = GetPictures.getPictures(url);
                getInstance().populateMap(getInstance().pictureMap, pictureList.getPictures());
                return pictureList;
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.pictures));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            getInstance().currentPictureList.setLinkPrevious(url);
            if (pictureList != null) {

                getInstance().currentPictureList.setLinkNext(pictureList.getLinkNext());
                getInstance().currentPictureList.getPictures().addAll(pictureList.getPictures());
            } else if (exception != null) {
                Toast.makeText(KindieDaysApp.getApp(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(KindieDaysApp.getApp(), "1 Internal error", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public static class GetPicturesForChildAsyncTask extends NetworkAsyncTask<PictureList> {

        protected long childId;

        public GetPicturesForChildAsyncTask(long childId) {
            this.childId = childId;
        }

        @Override
        protected PictureList doNetworkTask() throws Exception {
            getInstance().currentPictureList = GetPictures.getPictures(childId);
            PictureList pictureList = getInstance().currentPictureList;
            getInstance().pictureMap.clear();
            getInstance().populateMap(getInstance().pictureMap, pictureList.getPictures());
            return pictureList;
        }
    }


    public static class GetPicturesAsyncTask extends NetworkAsyncTask<PictureList> {

        @Override
        protected PictureList doNetworkTask() throws Exception {
            getInstance().currentPictureList = GetPictures.getPictures();
            getInstance().pictureMap.clear();
            getInstance().populateMap(getInstance().pictureMap, getInstance().currentPictureList.getPictures());
            return getInstance().currentPictureList;
        }
    }


    public static class GetGroupPicturesAsyncTask extends NetworkAsyncTask<PictureList> {
        private long groupId;

        public GetGroupPicturesAsyncTask(long groupId) {
            this.groupId = groupId;
        }

        @Override
        protected PictureList doNetworkTask() throws Exception {
            getInstance().currentPictureList = GetPictures.getGroupPictures(groupId);
            getInstance().pictureMap.clear();
            getInstance().populateMap(getInstance().pictureMap, getInstance().currentPictureList.getPictures());
            return getInstance().currentPictureList;
        }
    }


    public static class GetTeacherPicturesAsyncTask extends NetworkAsyncTask<PictureList> {
        private long teacherId;

        public GetTeacherPicturesAsyncTask(long teacherId) {
            this.teacherId = teacherId;
        }

        @Override
        protected PictureList doNetworkTask() throws Exception {
            getInstance().currentPictureList = GetPictures.getTeacherPictures(teacherId);
            getInstance().pictureMap.clear();
            getInstance().populateMap(getInstance().pictureMap, getInstance().currentPictureList.getPictures());
            return getInstance().currentPictureList;
        }
    }

}
