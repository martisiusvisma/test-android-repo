package com.kindiedays.common.business;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.NetworkProgressAsyncTask;
import com.kindiedays.common.network.conversations.GetConversation;
import com.kindiedays.common.network.conversations.GetConversations;
import com.kindiedays.common.network.conversations.PatchConversation;
import com.kindiedays.common.network.conversations.PostConversations;
import com.kindiedays.common.network.conversations.PostMessageToConversation;
import com.kindiedays.common.network.fileupload.PostPendingFiles;
import com.kindiedays.common.network.fileupload.PutFile;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationList;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.ConversationPatchResponse;
import com.kindiedays.common.pojo.Message;
import com.kindiedays.common.pojo.PendingFile;
import com.kindiedays.common.utils.ArrayUtils;
import com.kindiedays.common.utils.FormatUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by pleonard on 28/05/2015.
 */
public class ConversationBusiness extends Observable {

    private ConversationList conversationList;
    private Map<Long, Set<Long>> selectedCarersPerChild = new HashMap<>();
    private static SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(KindieDaysApp.getApp().getBaseContext());
    protected static ConversationBusiness instance;

    private ConversationBusiness() {}

    public static ConversationBusiness getInstance() {
        if (instance == null) {
            instance = new ConversationBusiness();
        }
        return instance;
    }

    public Map<Long, Set<Long>> getSelectedCarersPerChild() {
        return selectedCarersPerChild;
    }

    public void setSelectedCarersPerChild(Map<Long, Set<Long>> selectedCarersPerChild) {
        this.selectedCarersPerChild = selectedCarersPerChild;
    }

    public ConversationList getConversations() {
        return conversationList;
    }

    public Map<Long, Set<Long>> buildChildrenPerSelectedCarerMap() {
        Map<Long, Set<Long>> childrenPerCarer = new HashMap<>();
        for (Map.Entry<Long, Set<Long>> entry : selectedCarersPerChild.entrySet()) {
            if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                long childId = entry.getKey();
                for (Long carerId : entry.getValue()) {
                    if (childrenPerCarer.get(carerId) == null) {
                        childrenPerCarer.put(carerId, new HashSet<Long>());
                    }
                    childrenPerCarer.get(carerId).add(childId);
                }
            }
        }
        return childrenPerCarer;
    }

    @SuppressWarnings("squid:S2209")
    public static class GetMoreConversationsTask extends NetworkAsyncTask<ConversationList> {

        protected String url;

        public GetMoreConversationsTask(String url) {
            this.url = url;
        }

        @Override
        protected void onPostExecute(ConversationList newConversationList) {
            super.onPostExecute(newConversationList);
            getInstance().conversationList.setLinkNext(newConversationList.getLinkNext());
            getInstance().conversationList.getConversations().addAll(newConversationList.getConversations());
            getInstance().setChanged();
            getInstance().notifyObservers(newConversationList);
        }

        @Override
        protected ConversationList doNetworkTask() throws Exception {
            return GetConversations.getConversations(url);
        }
    }

    @SuppressWarnings("squid:S2696")
    public static class LoadConversationsTask extends NetworkAsyncTask<ConversationList> {

        private long childId;

        public LoadConversationsTask(long childId) {
            this.childId = childId;
        }

        @Override
        protected void onPostExecute(ConversationList conversations) {
            getInstance().setChanged();
//            getInstance().notifyObservers(conversations);
        }

        @Override
        protected ConversationList doNetworkTask() throws Exception {
            if (childId <= 0) {
                getInstance().conversationList = GetConversations.getConversations();
            } else {
                getInstance().conversationList = GetConversations.getConversations(childId);
            }

            return getInstance().conversationList;
        }
    }


    public static class LoadConversationTask extends NetworkAsyncTask<Conversation> {

        long conversationId;

        public LoadConversationTask(long conversationId) {
            this.conversationId = conversationId;
        }

        @Override
        protected Conversation doNetworkTask() throws Exception {
            Conversation conversation = GetConversation.getConversation(conversationId);
            if (conversation.getUnreadMessages() > 0) {
                ConversationPatchResponse response = PatchConversation.markConversationAsRead(conversation, conversation.getLastMessageIndex());
                int appCounter = sharedPref.getInt("iconCounter", 0);
                int respMessages = response == null ? 0 : response.getMessagesRead();
                if (appCounter != 0 &&  appCounter >= respMessages) {
                    ShortcutBadger.applyCount(KindieDaysApp.getApp().getBaseContext(), appCounter - respMessages);
                }
            }
            return conversation;
        }
    }


    public static class CreateConversationsTask extends NetworkProgressAsyncTask<Set<Conversation>, Integer> {
        private Set<ConversationParty> conversationParties;
        private long[] childrenIds;
        private String subject;
        private String message;
        private boolean createAsKindergarten;
        protected List<File> attachments;
        private boolean isDelete;

        public CreateConversationsTask(String subject, String message, Set<ConversationParty> parties,
                                       boolean createAsKindergarten, long[] childrenIds, List<File> attachments,
                                       boolean isDelete) {
            this.conversationParties = parties;
            this.subject = subject;
            this.message = message;
            this.createAsKindergarten = createAsKindergarten;
            this.childrenIds = childrenIds;
            this.attachments = attachments;
            this.isDelete = isDelete;
        }

        @Override
        protected Set<Conversation> doNetworkTask() throws Exception {
            Set<Conversation> conversations = new HashSet<>();
            List<Attachment> preparedFiles = new ArrayList<>(attachments.size());

            for (int i = 0; i < attachments.size(); i++) {
                preparedFiles.add(prepareFile(attachments.get(i)));
                publishProgress(i + 1);
            }

            for (ConversationParty conversationParty : conversationParties) {
                Conversation conversation = PostConversations.startNewConversation(subject, message, conversationParty,
                        createAsKindergarten, childrenIds, preparedFiles);
                conversations.add(conversation);
            }
            return conversations;
        }

        @Override
        protected void onPostExecute(Set<Conversation> conversations) {
            super.onPostExecute(conversations);
            if (isDelete) {
                isDelete = attachments.get(0).delete();
            }
        }
    }


    public static class CreateTeacherConversationsTask extends NetworkProgressAsyncTask<Set<Conversation>, Integer> {
        private Map<Long, Set<Long>> childrenPerCarer;
        private String subject;
        private String message;
        private boolean createAsKindergarten;
        private long[] teacherIds;
        protected List<File> attachments;
        private boolean isDelete;

        public CreateTeacherConversationsTask(String subject, String message, Map<Long, Set<Long>> childrenPerCarer,
                                              boolean createAsKindergarten, long[] teacherIds, List<File> attachments,
                                              boolean isDelete) {
            this.childrenPerCarer = childrenPerCarer;
            this.subject = subject;
            this.message = message;
            this.createAsKindergarten = createAsKindergarten;
            this.teacherIds = teacherIds;
            this.attachments = attachments;
            this.isDelete = isDelete;
        }

        @Override
        protected Set<Conversation> doNetworkTask() throws Exception {
            List<Long> allChildrenIds = new ArrayList<>();
            Set<Conversation> conversations = new HashSet<>();
            List<Attachment> preparedFiles = new ArrayList<>(attachments.size());

            for (int i = 0; i < attachments.size(); i++) {
                preparedFiles.add(prepareFile(attachments.get(i)));
                publishProgress(i + 1);
            }

            if (childrenPerCarer != null && childrenPerCarer.size() > 0) {
                for (Map.Entry<Long, Set<Long>> entry : childrenPerCarer.entrySet()) {
                    long carerId = entry.getKey();
                    long[] childIdsOfCarer = ArrayUtils.longCollectionToArray(entry.getValue());
                    for (Long childId : entry.getValue()) {
                        allChildrenIds.add(childId);
                    }

                    //Conversation parties might not be initialized at this point in the business class, so let's build a new object
                    ConversationParty conversationParty = new ConversationParty();
                    conversationParty.setPartyId(carerId);
                    conversationParty.setType(ConversationParty.CARER_TYPE);
                    Conversation conversation = PostConversations.startNewConversation(subject, message, conversationParty,
                            createAsKindergarten, childIdsOfCarer, preparedFiles);
                    conversations.add(conversation);
                }
            }
            for (Long teacherId : teacherIds) {
                ConversationParty teacher = ConversationPartyBusiness.getInstance().getConversationParty(ConversationParty.TEACHER_TYPE, teacherId);
                Conversation conversation = PostConversations.startNewConversation(subject, message, teacher,
                        createAsKindergarten, ArrayUtils.longCollectionToArray(allChildrenIds), preparedFiles);
                conversations.add(conversation);
            }

            return conversations;
        }

        @Override
        protected void onPostExecute(Set<Conversation> conversations) {
            super.onPostExecute(conversations);
            if (isDelete) {
                isDelete = attachments.get(0).delete();
            }
        }
    }


    public static class SendNewMessageWithMedia extends NetworkProgressAsyncTask<Message, Integer> {
        private String message;
        protected List<File> attachments;
        private long conversationId;
        private boolean isDelete;

        public SendNewMessageWithMedia(String message, List<File> attachments, long conversationId, boolean isDelete) {
            this.message = message;
            this.attachments = attachments;
            this.conversationId = conversationId;
            this.isDelete = isDelete;
        }

        @Override
        protected Message doNetworkTask() throws Exception {
            try {
                List<Attachment> preparedFiles = new ArrayList<>(attachments.size());

                for (int i = 0; i < attachments.size(); i++) {
                    preparedFiles.add(prepareFile(attachments.get(i)));
                    publishProgress(i + 1);
                }
                return PostMessageToConversation.postMessageWithAttach(this.message,
                        conversationId, preparedFiles);
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(Message message) {
            super.onPostExecute(message);
            if (isDelete) {
                isDelete = attachments.get(0).delete();
            }
        }
    }

    public static Attachment prepareFile(File file) throws CommonKindieException {

        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(byteArray);
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PendingFile pendingFile = PostPendingFiles.post();
        PutFile.uploadFile(file, pendingFile.getUploadUrl());
        return new Attachment(pendingFile.getId(), file.getName(),
                FormatUtils.getServerFormat(FormatUtils.getFormat(file.getAbsolutePath())));
    }

    public static class DeleteConversation extends NetworkAsyncTask<Void> {

        private long conversationId;

        public DeleteConversation(long conversationId) {
            this.conversationId = conversationId;
        }

        protected Void doNetworkTask() throws Exception {
            PatchConversation.hideConversation(conversationId, true);
            return null;
        }
    }


    public static class MarkConversationUnread extends NetworkAsyncTask<ConversationPatchResponse> {

        private Conversation conversation;

        public MarkConversationUnread(Conversation conversation) {
            this.conversation = conversation;
        }

        protected ConversationPatchResponse doNetworkTask() throws Exception {
            conversation = GetConversation.getConversation(conversation.getId());
            int index = -1;
            if (conversation.getMessages().size() > 1) {
                index = conversation.getMessages().get(conversation.getMessages().size() - 2).getIndex();
            }
            return PatchConversation.markConversationAsRead(conversation, index);
        }

    }

}
