package com.kindiedays.common.business;

import android.support.annotation.Nullable;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.groups.GetGroups;
import com.kindiedays.common.pojo.Group;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by pleonard on 11/08/2016.
 */
public class GroupBusiness extends ContainerBusiness {

    protected static volatile GroupBusiness instance;

    protected List<Group> realGroups = new CopyOnWriteArrayList<>();
    protected List<Group> allGroups = new CopyOnWriteArrayList<>();

    protected Group allGroup;

    public static final long ALL_GROUP_ID = -1;
    public static final long NO_GROUP_ID = 0;

    public static GroupBusiness getInstance() {
        GroupBusiness localInstance = instance;
        if (localInstance == null) {
            synchronized (GroupBusiness.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = new GroupBusiness();
                    localInstance.allGroup = new Group();
                    localInstance.allGroup.setId(ALL_GROUP_ID);
                    localInstance.allGroup.setName(KindieDaysApp.getApp().getString(R.string.All));
                    localInstance.initAllGroups();
                    instance = localInstance;
                }
            }
        }
        return localInstance;
    }

    @Nullable
    public Group getGroup(long groupId) {
        for (Group group : allGroups) {
            if (group.getId() == groupId) {
                return group;
            }
        }
        return null;
    }

    @Override
    public List getContainers(boolean includeVirtualContainers) {
        return Collections.emptyList();
    }

    public void initAllGroups() {
        allGroups.add(allGroup);
        if (realGroups != null) {
            allGroups.addAll(realGroups);
        }
    }

    public static class LoadGroupsTask extends NetworkAsyncTask<List<Group>> {

        @Override
        protected void onPostExecute(List<Group> groups) {
            getInstance().setChanged();
            getInstance().notifyObservers(groups);
        }

        @Override
        protected List<Group> doNetworkTask() throws Exception {
            getInstance().realGroups = GetGroups.getGroups();
            getInstance().initAllGroups();
            return getInstance().realGroups;
        }
    }
}
