package com.kindiedays.common.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.teachers.GetTeacher;
import com.kindiedays.common.network.teachers.GetTeachers;
import com.kindiedays.common.pojo.Teacher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pleonard on 30/04/2015.
 */
public class TeacherBusiness extends java.util.Observable {

    private List<Teacher> teachers;
    private Teacher currentTeacher;
    private Map<Long, Teacher> teachersById;

    private static TeacherBusiness instance;

    protected TeacherBusiness() {}

    public static TeacherBusiness getInstance() {
        if (instance == null) {
            instance = new TeacherBusiness();
        }
        return instance;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public Teacher getTeacherForId(long id) {
        return teachersById.get(id);
    }

    public Teacher getCurrentTeacher() {
        return currentTeacher;
    }

    public void setCurrentTeacher(Teacher currentTeacher) {
        if (this.currentTeacher != currentTeacher) {
            this.currentTeacher = currentTeacher;
            getInstance().setChanged();
            getInstance().notifyObservers(currentTeacher);
        }
    }

    public void selectTeacher(Teacher teacher, boolean selected) {
        teacher.setSelected(selected);
        getInstance().setChanged();
        getInstance().notifyObservers(teacher);
    }

    public void unselectAllTeachers() {
        for (Teacher teacher : teachers) {
            teacher.setSelected(false);
        }
    }

    public static class LoadTeachersTask extends NetworkAsyncTask<List<Teacher>> {

        @Override
        protected void onPostExecute(List<Teacher> teachers) {
            getInstance().setChanged();
            getInstance().notifyObservers(teachers);
        }

        @Override
        protected List<Teacher> doNetworkTask() throws Exception {
            getInstance().teachers = GetTeachers.getTeachers();
            getInstance().teachersById = new HashMap<>();
            for (Teacher teacher : getInstance().teachers) {
                getInstance().teachersById.put(teacher.getId(), teacher);
            }
            return getInstance().teachers;
        }
    }


    public static class LoadCurrentTeacherDetails extends NetworkAsyncTask<Teacher> {

        private String mLink;

        public LoadCurrentTeacherDetails(String link) {
            this.mLink = link;
        }


        @Override
        protected Teacher doNetworkTask() throws Exception {
            return GetTeacher.getTeacherByLink(mLink);
        }
    }

}
