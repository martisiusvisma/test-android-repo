package com.kindiedays.common.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.daily.GetDRDaily;
import com.kindiedays.common.network.daily.GetDRSetting;
import com.kindiedays.common.pojo.CategoryRes;
import com.kindiedays.common.pojo.DRSettings;
import com.kindiedays.common.pojo.GeneralDailyRes;

import org.joda.time.DateTime;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class DailyReportBusiness {

    private DRSettings settings;
    protected static DailyReportBusiness instance;

    protected DailyReportBusiness() {}

    public DRSettings getSettings() {return settings;}

    public List<CategoryRes> getContainers() {
        return settings.getCategoryResList();
    }

    public static DailyReportBusiness getInstance() {
        if (instance == null) {
            instance = new DailyReportBusiness();
        }
        return instance;
    }

    public static class GetDailyForChildAsyncTask extends NetworkAsyncTask<GeneralDailyRes> {

        private Long childId;
        private DateTime date;

        public GetDailyForChildAsyncTask(Long childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected GeneralDailyRes doNetworkTask() throws Exception {
            return GetDRDaily.getItem(childId, date);
        }
    }


    public static class LoadDRSettingsTask extends NetworkAsyncTask<DRSettings> {
        private Long kindergartenId;


        public LoadDRSettingsTask(Long kindergardenId) {
            this.kindergartenId = kindergardenId;
        }

        @Override
        protected DRSettings doNetworkTask() throws Exception {
            DRSettings settings = GetDRSetting.getSettings(kindergartenId);
            Collections.sort(settings.getCategoryResList(), new DateComparator());
            getInstance().settings = settings;
            return getInstance().settings;
        }

        public class DateComparator implements Comparator<CategoryRes> {
            @Override
            public int compare(CategoryRes lhs, CategoryRes rhs) {
                String distance = String.valueOf(lhs.getPriority());
                String distance1 = String.valueOf(rhs.getPriority());
                if (distance.compareTo(distance1) < 0) {
                    return -1;
                } else if (distance.compareTo(distance1) > 0) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

    }

}
