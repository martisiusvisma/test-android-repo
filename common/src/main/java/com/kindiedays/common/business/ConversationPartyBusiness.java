package com.kindiedays.common.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.conversations.GetConversationParties;
import com.kindiedays.common.pojo.ConversationParty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pleonard on 25/11/2015.
 */
public class ConversationPartyBusiness extends java.util.Observable {

    private Map<String, List<ConversationParty>> conversationPartyPerType;
    private Map<String, Map<Long, ConversationParty>> conversationPartyPerId;
    private List<ConversationParty> conversationParties;

    private static ConversationPartyBusiness instance;

    private ConversationPartyBusiness() {}

    public static ConversationPartyBusiness getInstance() {
        if (instance == null) {
            instance = new ConversationPartyBusiness();
        }
        return instance;
    }


    public ConversationParty getConversationParty(String type, long id) {
        if (conversationPartyPerId == null || conversationPartyPerId.get(type) == null) {
            return null;
        }
        return conversationPartyPerId.get(type).get(id);
    }

    public void select(ConversationParty conversationParty, boolean selected) {
        conversationParty.setSelected(selected);
        setChanged();
        notifyObservers();
    }

    public void selectAll(boolean select) {
        if (conversationParties != null) {
            for (ConversationParty conversationParty : conversationParties) {
                conversationParty.setSelected(select);
            }
        }
        setChanged();
        notifyObservers();
    }

    public void unselectAll() {
        if (conversationParties != null) {
            for (ConversationParty conversationParty : conversationParties) {
                conversationParty.setSelected(false);
            }
        }
    }

    public Map<String, List<ConversationParty>> getConversationPartyPerType() {
        return conversationPartyPerType;
    }

    public List<ConversationParty> getConversationParties(String type) {
        return conversationPartyPerType.get(type);
    }

    public List<ConversationParty> getConversationParties() {
        return conversationParties;
    }

    public void setConversationParties(List<ConversationParty> conversationParties) {
        this.conversationParties = conversationParties;
    }

    public static class LoadConversationParties extends NetworkAsyncTask<List<ConversationParty>> {

        List<Long> selected;

        public LoadConversationParties(List<Long> selected) {
            this.selected = selected;
        }

        @Override
        protected List<ConversationParty> doNetworkTask() throws Exception {
            List<ConversationParty> parties = GetConversationParties.getConversationParties();
            Map<String, List<ConversationParty>> conversationPartyPerType = new HashMap<>();
            Map<String, Map<Long, ConversationParty>> conversationPartyPerId = new HashMap<>();
            for (ConversationParty conversationParty : parties) {
                if (selected.contains(conversationParty.getPartyId())) {
                    conversationParty.setSelected(true);
                }
                if (conversationPartyPerType.get(conversationParty.getType()) == null) {
                    conversationPartyPerType.put(conversationParty.getType(), new ArrayList<>());
                    conversationPartyPerId.put(conversationParty.getType(), new HashMap<>());
                }
                conversationPartyPerType.get(conversationParty.getType()).add(conversationParty);
                conversationPartyPerId.get(conversationParty.getType()).put(conversationParty.getPartyId(), conversationParty);
            }

            getInstance().conversationParties = parties;
            getInstance().conversationPartyPerType = conversationPartyPerType;
            getInstance().conversationPartyPerId = conversationPartyPerId;
            return parties;
        }
    }
}
