package com.kindiedays.common.business;

import com.kindiedays.common.R;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.places.GetPlaces;
import com.kindiedays.common.network.places.PostPlaces;
import com.kindiedays.common.pojo.Place;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pleonard on 30/04/2015.
 */
public class PlaceBusiness extends ContainerBusiness {

    protected List<Place> realPlaces;
    protected List<Place> allPlaces;

    public static final long ABSENT_PLACE_ID = -1;
    public static final long NO_PLACE_ID = 0;

    private static Place absentPlace;

    protected static PlaceBusiness instance;

    protected PlaceBusiness() {}

    protected static void initInstance() {
        absentPlace = new Place();
        absentPlace.setType("NORMAL");
        absentPlace.setAddress(null);
        absentPlace.setId(ABSENT_PLACE_ID);
        absentPlace.setName(String.valueOf(R.string.Absent));
        instance.initAllPlaces();
    }

    public static PlaceBusiness getInstance() {
        if (instance == null) {
            instance = new PlaceBusiness();
            initInstance();
        }
        return instance;
    }

    public Place getPlace(long placeId) {
        for (Place place : allPlaces) {
            if (place.getId() == placeId) {
                return place;
            }
        }
        return null;
    }

    public Place getMainPlace() {
        for (Place place : realPlaces) {
            if (place.isMainBuilding()) {
                return place;
            }
        }
        return null;
    }

    public void setAbsencePlaceName(String name){
        absentPlace.setName(name);
    }

    private void initAllPlaces() {
        allPlaces = new ArrayList<>();
        if (realPlaces != null) {
            allPlaces.addAll(realPlaces);
        }
        allPlaces.add(absentPlace);
    }


    public List<Place> getContainers(boolean includeVirtualPlaces) {
        if (includeVirtualPlaces) {
            return allPlaces;
        } else {
            return realPlaces;
        }
    }

    public static class CreateTemporaryPlaceAsyncTask extends NetworkAsyncTask<Place> {

        private String address;
        private String name;

        public CreateTemporaryPlaceAsyncTask(String name, String address) {
            this.address = address;
            this.name = name;
        }

        @Override
        protected void onPostExecute(Place place) {
            super.onPostExecute(place);
            getInstance().setChanged();
            getInstance().notifyObservers(getInstance().realPlaces);
        }

        @Override
        protected Place doNetworkTask() throws Exception {
            Place place = PostPlaces.createTemporaryPlace(name, address);
            getInstance().realPlaces.add(place);
            getInstance().initAllPlaces();
            return place;
        }
    }


    public static class LoadPlacesTask extends NetworkAsyncTask<List<Place>> {

        @Override
        protected void onPostExecute(List<Place> places) {
            getInstance().setChanged();
            getInstance().notifyObservers(places);
        }

        @Override
        protected List<Place> doNetworkTask() throws Exception {
            getInstance().realPlaces = GetPlaces.getPlaces();
            getInstance().initAllPlaces();
            return getInstance().realPlaces;

        }
    }
}
