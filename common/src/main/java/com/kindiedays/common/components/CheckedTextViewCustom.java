package com.kindiedays.common.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.util.AttributeSet;

import com.kindiedays.common.R;

/**
 * Created by Giuseppe Franco - Starcut on 04/03/16.
 */
public class CheckedTextViewCustom extends AppCompatCheckedTextView {
    public CheckedTextViewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CheckedTextViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CheckedTextViewCustom(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs!=null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontCustom);
            String font = a.getString(R.styleable.FontCustom_fontName);
            if (font!=null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/"+font);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}
