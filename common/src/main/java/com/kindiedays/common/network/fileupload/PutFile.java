package com.kindiedays.common.network.fileupload;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Http;
import com.kindiedays.common.network.KindieDaysNetworkException;

import java.io.File;
import java.net.URI;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.FileEntity;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by pleonard on 20/05/2015.
 */
public class PutFile {
    private static final String TAG = PutFile.class.getSimpleName();
    private static final Http.HttpClientHolder httpClientHolder = Http.buildHttpClient(30 * 1000, true);

    private PutFile() {
        //no instance
    }

    public static void uploadFile(File file, String url) throws CommonKindieException {
        uploadFileWithType(file, url, ContentType.APPLICATION_OCTET_STREAM);
    }

    public static void uploadFileWithType(File file, String url, ContentType contentType) throws CommonKindieException {
        try {
            HttpPut request = new HttpPut();
            FileEntity fe = new FileEntity(file, contentType);
            request.setEntity(fe);
            request.setURI(new URI(url));
            HttpResponse response = httpClientHolder.get().execute(request);

            Log.d(TAG, "url " + url);
            Log.d(TAG, "Response code: " + response.getStatusLine().getStatusCode());
            Log.d(TAG, "Response code: " +  EntityUtils.toString(response.getEntity(), "UTF-8"));
            EntityUtils.consumeQuietly(response.getEntity());
            if (response.getStatusLine().getStatusCode() > 299) {
                throw new KindieDaysNetworkException(response.getStatusLine().getStatusCode(), "unable to save picture, status code", true);
            }
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }
}
