package com.kindiedays.common.network.events;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Event;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class PatchEvent extends Webservice<Event> {

    public static void markEventAsRead(Event event, int childId) throws CommonKindieException {
        try {
            PatchEvent instance = new PatchEvent();
            JSONObject json = new JSONObject();
            json.put("read", true);
            instance.patchObjects("events/" + event.getId() + "/invitations/" + childId, json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Event parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
