package com.kindiedays.common.network;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;
import com.kindiedays.common.network.utils.LanguageUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HeaderElementIterator;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NoHttpResponseException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpRequestBase;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.socket.LayeredConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLContexts;
import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.TrustSelfSignedStrategy;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicHeaderElementIterator;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpCoreContext;

/**
 * Created by pleonard on 25/05/2015.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public class Http {
    private static final String TAG = "common.network.Http";

    private Http() {
        //no instance
    }

    @SuppressWarnings("deprecation")
    public static class HttpClientHolder {
        private int reservations = 0;
        private boolean closed = false;
        private final CloseableHttpClient httpClient;

        public HttpClientHolder(CloseableHttpClient httpClient) {
            this.httpClient = httpClient;
        }

        public synchronized HttpClient get() {
            if (closed) {
                throw new IllegalStateException("The http client has been closed");
            }

            return httpClient;
        }

        public synchronized void reserve() {
            if (closed) {
                throw new IllegalStateException("The http client has been closed");
            }

            reservations++;
        }

        public synchronized void release() {
            reservations--;

            if (reservations == 0) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    Log.w(Http.class.getName(), "failed to close http client", e);
                }
            }

            closed = true;
        }
    }


    public static HttpClientHolder buildHttpClient(int timeoutMilliseconds, boolean keepAlive) {
        return buildHttpClient(timeoutMilliseconds, keepAlive, newSslSocketFactory());
    }

    private static SSLConnectionSocketFactory newSslSocketFactory() {
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs)
            InputStream in = KindieDaysApp.getApp().getResources().openRawResource(R.raw.kindiedaysstaging);
            try {
                // Initialize the keystore with the provided trusted certificates
                // Also provide the password of the keystore
                trusted.load(in, "123456".toCharArray());
            } finally {
                in.close();
            }

            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(trusted, new TrustSelfSignedStrategy()).useTLS().build();
            // Hostname verification from certificate
            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
            return new SSLConnectionSocketFactory(sslContext, SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    public static void addCommonHeaders(HttpRequestBase request, String authorizationToken) {
        String version = "";
        KindieDaysApp app = KindieDaysApp.getApp();
        try {
            PackageManager pm = app.getPackageManager();
            PackageInfo pi;
            // Version
            pi = pm.getPackageInfo(app.getPackageName(), 0);
            version = (pi.versionName);
        } catch (Exception e) {
            Log.e(TAG, "addCommonHeaders: ", e);
        }
        request.addHeader("User-Agent", app.getString(R.string.app_name) + "/" + version + " Android/" + Build.VERSION.RELEASE + " " + Build.MODEL);
        request.addHeader("Accept-Language", LanguageUtils.getLanguageCode(app));
        if (authorizationToken != null) {
            request.addHeader("Authorization", authorizationToken);
        }
    }

    @SuppressWarnings("squid:S1172")
    public static HttpClientHolder buildHttpClient(int timeoutMilliseconds, boolean keepAlive, LayeredConnectionSocketFactory sslSocketFactory) {
        RequestConfig.Builder configBuilder = RequestConfig.custom()
                .setConnectTimeout(timeoutMilliseconds)
                .setConnectionRequestTimeout(timeoutMilliseconds)
                .setSocketTimeout(timeoutMilliseconds);

        HttpClientBuilder builder = HttpClientBuilder.create()
                .setRetryHandler(new RetryHandler(3))
                .setServiceUnavailableRetryStrategy(new ServiceUnavailableRetryStrategy(3))
                .setDefaultRequestConfig(configBuilder.build());

        if (keepAlive) {
            builder.setKeepAliveStrategy(new KeepAliveStrategy());
        }

        return new HttpClientHolder(builder.build());
    }

    private static class RetryHandler implements HttpRequestRetryHandler {
        private static final int BACKOFF_DELAY = 2000;

        private static final Set<Class<? extends IOException>> exceptionWhitelist = new HashSet<>();

        private static final Set<Class<? extends IOException>> exceptionBlacklist = new HashSet<>();

        static {
            exceptionWhitelist.add(NoHttpResponseException.class);
            exceptionWhitelist.add(UnknownHostException.class);
            exceptionWhitelist.add(SocketException.class);

            exceptionBlacklist.add(InterruptedIOException.class);
            exceptionBlacklist.add(SSLHandshakeException.class);
            exceptionBlacklist.add(SSLPeerUnverifiedException.class);
        }

        private final int maxRetries;
        private int retryCount;

        public RetryHandler(int maxRetries) {
            this.maxRetries = maxRetries;
        }

        @Override
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            final boolean retry;

            if (executionCount > maxRetries) {
                // Do not retry if over max retry count
                retry = false;
            } else if (exceptionBlacklist.contains(exception.getClass())) {
                // immediately cancel retry if the error is blacklisted
                retry = false;
            } else if (exceptionWhitelist.contains(exception.getClass())) {
                // immediately retry if error is whitelisted
                retry = true;
            } else {
                Boolean requestSent = (Boolean) context.getAttribute(HttpCoreContext.HTTP_REQ_SENT);
                retry = (requestSent == null || !requestSent);
            }

            if (retry) {
                int sleepTime = BACKOFF_DELAY + BACKOFF_DELAY * retryCount * retryCount;
                Log.w(String.format(Http.class.getName(), "request failed - %1$s: %2$s, attempt: %3$d - will retry in %4$d seconds", exception.getClass().getCanonicalName(), exception.getMessage(), executionCount, (long) (sleepTime / 1000.0)), exception);
                try {
                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                    Log.w(Http.class.getName(), "Thread not feeling sleepy", e);
                }
                retryCount = executionCount;
            } else {
                Log.w(Http.class.getName(), "request failed after " + executionCount + " attempts", exception);
            }

            return retry;
        }
    }


    @SuppressWarnings("squid:S2176")
    private static class ServiceUnavailableRetryStrategy implements cz.msebera.android.httpclient.client.ServiceUnavailableRetryStrategy {
        private static final long BACKOFF_DELAY = 2000;

        private final int maxRetries;
        private int retryCount;

        public ServiceUnavailableRetryStrategy(int maxRetries) {
            this.maxRetries = maxRetries;
        }

        @Override
        public boolean retryRequest(HttpResponse response, int executionCount, HttpContext context) {
            if (executionCount > maxRetries) {
                return false;
            }

            int statusCode = response.getStatusLine().getStatusCode();

            // retry if the status code is in the 5xx range
            if (statusCode / 100 == 5) {
                HttpRequest request = (HttpRequest) context.getAttribute(HttpCoreContext.HTTP_REQUEST);
                String requestUri = (request == null ? "unknown" : request.getRequestLine().getUri());
                Log.w(Http.class.getName(), String.format("request to %1$s failed (statusCode: %2$d, attempt: %3$d), will retry in %4$d seconds", requestUri, statusCode, executionCount, (long) (getRetryInterval() / 1000.0)));

                retryCount = executionCount;
                return true;
            }

            return false;
        }

        @Override
        public long getRetryInterval() {
            return BACKOFF_DELAY + BACKOFF_DELAY * retryCount * retryCount;
        }
    }


    private static class KeepAliveStrategy implements ConnectionKeepAliveStrategy {
        @Override
        public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
            // Honor 'keep-alive' header
            HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
            while (it.hasNext()) {
                HeaderElement he = it.nextElement();
                String param = he.getName();
                String value = he.getValue();
                if (value != null && "timeout".equalsIgnoreCase(param))
                {
                    try
                    {
                        return Long.parseLong(value) * 1000;
                    } catch (NumberFormatException ignore) {
                        throw new NumberFormatException("Error parsing keepAliveDuration");
                    }
                }
            }

            // otherwise keep alive for 30 seconds
            return 60 * 1000L;
        }
    }

}
