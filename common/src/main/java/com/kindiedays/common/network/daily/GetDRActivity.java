package com.kindiedays.common.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.ActivityRes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class GetDRActivity extends Webservice<ActivityRes> {

    DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
    DateTimeFormatter dtfFull = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public static ActivityRes getItems(Long child, DateTime date, Integer category) throws CommonKindieException {
        GetDRActivity getCategory = new GetDRActivity();
        return getCategory.getObjects("daily-report/activities/" + child + "/" + date.toString("yyyy-MM-dd") + "/" + category);
    }

    @Override
    public ActivityRes parseJsonFromServer(JSONObject json) throws JSONException {
        return new ActivityRes(json.getInt("child"),
                dtf.parseDateTime(json.getString("date")),
                dtfFull.parseDateTime(json.getString("created")), json.getLong("category"),
                json.getString("categoryName"),
                json.getString("text"));
    }

}
