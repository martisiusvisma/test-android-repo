package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("meals")
    @Expose
    private List<Meal> meals = new ArrayList<Meal>();

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "date='" + date + '\'' +
                ", meals=" + meals +
                '}';
    }
}