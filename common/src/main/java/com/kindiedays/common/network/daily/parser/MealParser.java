package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.MealRes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class MealParser {
    private MealParser() {
        //no instance
    }

    public static MealRes parserMealItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        MealRes mealItem = new MealRes();
        mealItem.setChild(json.getInt("child"));
        mealItem.setDate(dataFormatter.parseDateTime(json.getString("date")));
        mealItem.setCreated(DateTime.parse(json.getString("created")));
        mealItem.setReaction(json.optString("reaction"));
        mealItem.setReactionDescription(json.optString("reactionDescription"));
        if (json.has("drink")){
            mealItem.setDrink(json.optInt("drink"));
        }
        if (json.has("mealIndex")){
            mealItem.setMealIndex(json.optInt("mealIndex"));
        }
        mealItem.setDrinkQuantity(json.optString("drinkQuantity"));
        mealItem.setDrinkUnit(json.optString("drinkUnit"));
        mealItem.setNotes(json.optString("notes"));
        return mealItem;
    }
}