package com.kindiedays.common.network.conversations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Conversation;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 28/05/2015.
 */
public class GetConversation extends Webservice<Conversation> {

    public static Conversation getConversation(long id) throws CommonKindieException{
        GetConversation getConversation = new GetConversation();
        return getConversation.getObjects("conversations/v2/" + id);
    }

    @Override
    public Conversation parseJsonFromServer(JSONObject json) throws JSONException {
        return ConversationParser.parseConversation(json);
    }
}
