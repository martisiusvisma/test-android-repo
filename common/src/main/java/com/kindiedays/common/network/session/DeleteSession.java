package com.kindiedays.common.network.session;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 15/05/2015.
 */
public class DeleteSession extends Webservice<Void> {


    public static void logout() throws CommonKindieException {
        DeleteSession deleteSession = new DeleteSession();
        deleteSession.deleteObjects("sessions/current", authEndpoint);
        clearSessionSettings();
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
