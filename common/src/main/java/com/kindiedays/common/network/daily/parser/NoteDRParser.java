package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.NoteRes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class NoteDRParser {
    private NoteDRParser() {
        //no instance
    }

    public static NoteRes parseNote(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        NoteRes note = new NoteRes();
        note.setText(json.getString("text"));
        note.setChild(json.getLong("child"));
        note.setDate(dataFormatter.parseDateTime(json.getString("date")));
        note.setModified(DateTime.parse(json.getString("modified")));
        return note;
    }
}