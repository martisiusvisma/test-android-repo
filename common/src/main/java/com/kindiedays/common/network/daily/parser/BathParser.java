package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.BathRes;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 21/07/2015.
 */
public class BathParser {

    private BathParser() {
        //no instance
    }

    public static BathRes parserBathItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mm:ss");

        BathRes bathItem = new BathRes();
        bathItem.setChild(json.getInt("child"));
        bathItem.setDate(dataFormatter.parseDateTime(json.getString("date")));
        bathItem.setTime(timeFormatter.parseDateTime(json.getString("time")));
        bathItem.setLocal(false);
        JSONArray jsonArray = json.getJSONArray("equipment");
        List<String> temp = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            temp.add(jsonArray.get(i).toString());
        }

        bathItem.setEquipment(temp);
        if (json.has("type")) {
            bathItem.setType(json.getString("type"));
            bathItem.setTypeDescription(json.optString("typeDescription"));
        }
        bathItem.setConstitution(json.optString("constitution"));
        bathItem.setNotes(json.optString("notes"));

        return bathItem;
    }
}