package com.kindiedays.common.network.journalposts.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meal {

    @SerializedName("index")
    @Expose
    private int index;
    @SerializedName("text")
    @Expose
    private String text;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "index=" + index +
                ", text='" + text + '\'' +
                '}';
    }
}