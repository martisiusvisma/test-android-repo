package com.kindiedays.common.network.conversations;

import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Message;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 20/07/2015.
 */
public class ConversationParser {
    private static final String MESSAGES_FIELD = "messages";

    private ConversationParser() {
        throw new IllegalStateException("Utility class");
    }

    public static Conversation parseConversation(JSONObject json) throws JSONException {
        Conversation conversation = new Conversation();
        conversation.setId(json.getInt("id"));
        conversation.setSubject(json.getString("subject"));
        if (json.has("lastMessageCreated")) {
            conversation.setLastMessageCreated(DateTime.parse(json.getString("lastMessageCreated")));
        } else {
            conversation.setLastMessageCreated(DateTime.now());
        }

        conversation.setLastMessageIndex(json.getInt("lastMessageIndex"));
        conversation.setUnreadMessages(json.getInt("unreadMessages"));

        JSONArray jsonParties = json.getJSONArray("parties");
        Set<ConversationParty> parties = new HashSet<>();
        for (int i = 0; i < jsonParties.length(); i++) {
            JSONObject jsonParty = jsonParties.getJSONObject(i);
            ConversationParty party = parseConversationParty(jsonParty);
            party.setConversationId(conversation.getId());
            parties.add(party);
        }
        conversation.setParties(parties);

        if (json.has(MESSAGES_FIELD)) {
            List<Message> messages = new ArrayList<>();
            JSONArray jsonMessages = json.getJSONArray(MESSAGES_FIELD);
            for (int i = 0; i < jsonMessages.length(); i++) {
                JSONObject jsonMessage = (JSONObject) jsonMessages.get(i);
                messages.add(parseMessage(jsonMessage));
            }
            conversation.setMessages(messages);
        }

        if (json.has("links")) {
            JSONObject links = json.getJSONObject("links");
            conversation.setMessagesURI(links.getString(MESSAGES_FIELD));
            conversation.setSelfURI(links.getString("self"));
        }

        if (json.has("children")) {
            JSONArray children = json.getJSONArray("children");
            long[] childrenIds = new long[children.length()];
            for (int i = 0; i < children.length(); i++) {
                childrenIds[i] = children.getLong(i);
            }
            conversation.setChildren(childrenIds);
        }

        return conversation;
    }

    public static ConversationParty parseConversationParty(JSONObject jsonParty) throws JSONException {
        ConversationParty party = new ConversationParty();
        party.setIndex(jsonParty.optInt("index"));
        party.setPartyId(jsonParty.getInt("partyId"));
        party.setType(jsonParty.getString("type"));
        party.setName(jsonParty.getString("name"));
        JSONArray jsonThumbnails = jsonParty.getJSONObject("profilePicture").getJSONArray("thumbnails");
        for (int j = jsonThumbnails.length() - 1; j >= 0; j--) {
            JSONObject thumbnail = jsonThumbnails.getJSONObject(j);
            if (thumbnail.getLong("width") == 256) {
                party.setProfilePictureUrl(thumbnail.getString("url"));
                break;
            }
        }
        return party;
    }

    public static List<Attachment> parseAttachment(JSONArray jsonAttachments) throws JSONException {
        if (jsonAttachments == null) {
            return Collections.emptyList();
        }

        List<Attachment> list = new ArrayList<>(jsonAttachments.length());
        for (int i = 0; i < jsonAttachments.length(); i++) {
            JSONObject jsonAttachment = jsonAttachments.getJSONObject(i);
            Attachment attachment = new Attachment();
            attachment.setFilename(jsonAttachment.getString("filename"));
            attachment.setPendingFileId(jsonAttachment.optString("filePendingId"));
            attachment.setFormat(jsonAttachment.getString("format"));
            attachment.setUri(jsonAttachment.getString("url"));
            list.add(attachment);
        }

        return list;
    }

    public static Message parseMessage(JSONObject jsonMessage) throws JSONException {
        Message message = new Message();
        message.setConversationId(jsonMessage.getInt("conversation"));
        message.setIndex(jsonMessage.getInt("index"));
        message.setAuthorIndex(jsonMessage.getInt("authorIndex"));
        message.setCreated(DateTime.parse(jsonMessage.getString("created")));
        message.setText(jsonMessage.getString("text"));
        message.setAttachmentList(parseAttachment(jsonMessage.getJSONArray("attachment")));
        return message;
    }
}
