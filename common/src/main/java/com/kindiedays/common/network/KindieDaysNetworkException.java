package com.kindiedays.common.network;

import android.util.Log;

import org.json.JSONArray;

import java.io.IOException;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by pleonard on 20/04/2015.
 */
@SuppressWarnings("squid:S1165")
public class KindieDaysNetworkException extends IOException {
    private static final String TAG = "NetworkException";

    private int statusCode;
    private boolean localizedMessage;
    private JSONArray payload;

    public KindieDaysNetworkException(int statusCode, String errorMessage, boolean localizedMessage) {
        super(errorMessage);
        this.setStatusCode(statusCode);
        this.setLocalizedMessage(localizedMessage);
    }

    public KindieDaysNetworkException(int statusCode, String errorMessage, boolean localizedMessage, JSONArray payload) {
        super(errorMessage);
        this.setStatusCode(statusCode);
        this.setLocalizedMessage(localizedMessage);
        this.payload = payload;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isLocalizedMessage() {
        return localizedMessage;
    }

    public void setLocalizedMessage(boolean localizedMessage) {
        this.localizedMessage = localizedMessage;
    }

    public String getMessagePayload(){
        StringBuilder message = new StringBuilder();
        if(payload != null){
            for(int i = 0 ; i < payload.length() ; i++){
                try{
                    message.append(payload.getJSONObject(i).getString("text"));
                    if(i < payload.length() - 1){
                        message.append("\n");
                    }
                }
                catch(Exception e){
                    Log.e(TAG, "getMessagePayload: ", e);
                }
            }
        }
        return message.toString();
    }

    public JSONArray getPayload() {
        return payload;
    }

}
