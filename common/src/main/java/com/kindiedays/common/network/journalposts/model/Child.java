package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Child {

    @SerializedName("picture")
    @Expose
    private int picture;
    @SerializedName("child")
    @Expose
    private int child;
    @SerializedName("carersIds")
    @Expose
    private List<Integer> carersIds = new ArrayList<Integer>();
    @SerializedName("x")
    @Expose
    private int x;
    @SerializedName("y")
    @Expose
    private int y;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public List<Integer> getCarersIds() {
        return carersIds;
    }

    public void setCarersIds(List<Integer> carersIds) {
        this.carersIds = carersIds;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Child{" +
                "picture=" + picture +
                ", child=" + child +
                ", carersIds=" + carersIds +
                ", x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}