package com.kindiedays.common.network.conversations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 20/07/2015.
 */
public class PostMessageToConversation extends Webservice<Message> {

    public static Message postMessageWithAttach(String text, long conversationId,
                                                List<Attachment> attachments) throws CommonKindieException {
        try {
            PostMessageToConversation instance = new PostMessageToConversation();
            JSONObject json = new JSONObject();
            json.put("text", text != null ? text : STRING_EMPTY);
            JSONArray jsonAttachments = new JSONArray();
            for (Attachment attachment : attachments) {
                JSONObject jsonFile = new JSONObject();
                jsonFile.put("pendingFileId", attachment.getPendingFileId());
                jsonFile.put("filename", attachment.getFilename());
                jsonFile.put("format", attachment.getFormat());
                jsonAttachments.put(jsonFile);
            }
            json.put("attachment", jsonAttachments);
            return instance.postObjects("conversations/v2/" + conversationId + "/messages", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Message parseJsonFromServer(JSONObject json) throws JSONException {
        return ConversationParser.parseMessage(json);
    }
}
