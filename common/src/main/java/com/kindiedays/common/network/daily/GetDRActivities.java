package com.kindiedays.common.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.ActivityParser;
import com.kindiedays.common.pojo.ActivityRes;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class GetDRActivities extends Webservice<List<ActivityRes>> {

    public static List<ActivityRes> getItems(int child, DateTime date) throws CommonKindieException {
        GetDRActivities items = new GetDRActivities();
        return items.getObjects("daily-report/activities/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public List<ActivityRes> parseJsonFromServer(JSONObject json) throws JSONException {

        List<ActivityRes> activityRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportActivities");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            ActivityRes activity = ActivityParser.parserActivityItem(object);
            activity.setExistsOnServer(true);
            activityRes.add(activity);
        }
        return activityRes;
    }

}
