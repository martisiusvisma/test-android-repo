package com.kindiedays.common.network.places;


import com.kindiedays.common.pojo.Place;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class PlaceParser {

    private PlaceParser() {
        //no instance
    }

    public static Place parserPlace(JSONObject json) throws JSONException {
        Place place = new Place();
        place.setAddress(json.getString("address"));
        place.setName(json.getString("name"));
        place.setId(json.getInt("id"));
        place.setType(json.getString("type"));
        place.setValidityDate(json.optString("date"));
        return place;
    }
}
