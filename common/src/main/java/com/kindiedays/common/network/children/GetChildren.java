package com.kindiedays.common.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 29/04/2015.
 */
public class GetChildren extends Webservice<List<Child>> {

    public static List<Child> getChildren() throws CommonKindieException {
        GetChildren getChildren = new GetChildren();
        return getChildren.getObjects("children");
    }

    @Override
    public List<Child> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Child> children = new ArrayList<>();
        JSONArray array = json.getJSONArray("children");
        for (int i = 0; i < array.length(); i++) {
            JSONObject jsonKid = array.getJSONObject(i);
            Child child = ChildJsonParser.parseChild(jsonKid);
            children.add(child);
        }
        return children;
    }

}
