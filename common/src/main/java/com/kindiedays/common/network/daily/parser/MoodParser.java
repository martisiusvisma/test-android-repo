package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.MoodRes;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class MoodParser {
    private MoodParser() {
        //no instance
    }

    public static MoodRes parserMoodItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mm:ss");
        MoodRes moodItem = new MoodRes();
        moodItem.setChild(json.getInt("child"));
        moodItem.setIndex(json.getInt("index"));
        moodItem.setDate(dataFormatter.parseDateTime(json.getString("date")));
        moodItem.setTime(timeFormatter.parseDateTime(json.getString("time")));
        moodItem.setLocal(false);
        moodItem.setTemper(json.getString("temper"));
        moodItem.setTemperDescription(json.optString("temperDescription"));
        if (!json.isNull("notes")) {
            moodItem.setNotes(json.optString("notes"));
        }
        return moodItem;
    }
}