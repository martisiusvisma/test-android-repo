package com.kindiedays.common.network.places;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Place;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 09/07/2015.
 */
public class PostPlaces extends Webservice<Place> {

    public static Place createTemporaryPlace(String name, String address) throws CommonKindieException {
        try {
            JSONObject json = new JSONObject();
            json.put("name", name);
            json.put("address", address);
            json.put("type", "NORMAL");
            PostPlaces instance = new PostPlaces();
            return instance.postObjects("places", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Place parseJsonFromServer(JSONObject json) throws JSONException {
        return PlaceParser.parserPlace(json);
    }
}
