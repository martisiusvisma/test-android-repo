package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.ActivityCategory;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class ActivityCategoryDRParser {

    private ActivityCategoryDRParser() {
        //no instance
    }

    public static ActivityCategory parseCategory(JSONObject json) throws JSONException {
        ActivityCategory activityCategory = new ActivityCategory();
        activityCategory.setId(json.getInt("id"));
        activityCategory.setName(json.getString("name"));
        activityCategory.setEnabled(json.getBoolean("enabled"));
        return activityCategory;
    }
}
