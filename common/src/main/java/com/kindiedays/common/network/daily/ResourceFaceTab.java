package com.kindiedays.common.network.daily;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kindiedays.common.R;
import com.kindiedays.common.svg.SVGImageView;
import com.kindiedays.common.utils.PictureUtils;

/**
 * Created by Giuseppe Franco - Starcut on 01/04/16.
 */
public class ResourceFaceTab extends LinearLayout {

    private SVGImageView ivResourceImage;

    public ResourceFaceTab(Context context) {
        super(context);
        initViews();
    }

    public ResourceFaceTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public ResourceFaceTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_face, this);
        ivResourceImage = (SVGImageView) v.findViewById(R.id.current);
    }

    public void setResourceImage(Integer resource, Integer selectedResource){
        Drawable drawable = PictureUtils.createStateDrawableFromSvg(getResources(), resource, selectedResource);
        ivResourceImage.setBackgroundDrawable(drawable);
        ivResourceImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }
}
