package com.kindiedays.common.network.album;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.GetPaginatedResource;
import com.kindiedays.common.pojo.PictureList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 14/07/2015.
 */
public class GetPictures extends GetPaginatedResource<PictureList> {

    public static PictureList getPictures() throws CommonKindieException {
        GetPictures instance = new GetPictures();
        return instance.getObjects("pictures?max=15");
    }

    public static PictureList getPictures(String url) throws CommonKindieException {
        GetPictures instance = new GetPictures();
        return instance.getObjectsFromUrl(url);
    }

    public static PictureList getPictures(long id) throws CommonKindieException {
        GetPictures instance = new GetPictures();
        return instance.getObjects("children/" + id + "/pictures");
    }

    public static PictureList getGroupPictures(long groupId) throws CommonKindieException {
        GetPictures instance = new GetPictures();
        return instance.getObjects("pictures/group/" + groupId);
    }

    public static PictureList getTeacherPictures(long teacherId) throws CommonKindieException {
        GetPictures instance = new GetPictures();
        return instance.getObjects("pictures/teacher/" + teacherId);
    }

    @Override
    public PictureList parseJsonFromServer(JSONObject json) throws JSONException {
        PictureList pictureList = new PictureList();
        JSONArray jsonPictures = json.getJSONArray("pictures");
        for (int i = 0; i < jsonPictures.length(); i++) {
            JSONObject jsonPicture = jsonPictures.getJSONObject(i);
            pictureList.getPictures().add(PictureParser.parsePicture(jsonPicture));
        }
        return pictureList;
    }
}
