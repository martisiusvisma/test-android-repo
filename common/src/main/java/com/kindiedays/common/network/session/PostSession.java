package com.kindiedays.common.network.session;

import android.content.Context;
import android.content.SharedPreferences;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.HttpResponse;

/**
 * Created by pleonard on 20/04/2015.
 */
public class PostSession extends Webservice<String> {


    public static String login(String email, String password, String realm, boolean environment) throws CommonKindieException {

        try {
            SharedPreferences.Editor edit = KindieDaysApp.getApp().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit();
            edit.putBoolean(ENVIRONMENT, environment);
            edit.commit();
            initEndPointsIfNeeded();
            PostSession postSession = new PostSession();
            JSONObject json = new JSONObject();
            json.put("email", email);
            json.put("password", password);
            json.put("realm", realm);
            return postSession.postObjects("sessions", json, authEndpoint);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    protected void responseUnauthorized(HttpResponse response) throws KindieDaysNetworkException {
        clearSessionSettings();
        connectionError(response, true, true);
    }

    @Override
    public String parseJsonFromServer(JSONObject json) throws JSONException {
        authorizationToken = json.getString("token");
        //Save token for future use
        SharedPreferences.Editor edit = KindieDaysApp.getApp().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit();
        edit.putString(AUTHORIZATION_TOKEN, authorizationToken);
        edit.commit();
        return authorizationToken;
    }

}
