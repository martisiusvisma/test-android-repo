package com.kindiedays.common.network.album;

import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.JournalGalleryPicture;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 20/07/2015.
 */
public class PictureParser {
    private static final String WIDTH_FIELD = "width";
    private static final String HEIGHT_FIELD = "height";

    private PictureParser() {
        //no instance
    }

    public static GalleryPicture parsePicture(JSONObject json) throws JSONException {
        GalleryPicture picture = new GalleryPicture();
        picture.setHash(json.getString("hash"));
        picture.setUrl(json.optString("url"));
        picture.setHeight(json.optInt(HEIGHT_FIELD));
        picture.setWidth(json.optInt(WIDTH_FIELD));
        picture.setCaption(json.optString("caption"));
        picture.setFormat(json.getString("format"));
        picture.setLink(json.getJSONObject("links").getString("self"));
        JSONArray jsonChildren = json.getJSONArray("children");
        List<ChildTag> photoTags = parseChildTag(jsonChildren);
        picture.setChildTags(photoTags);
        return picture;
    }

    public static JournalGalleryPicture parseJournalGalleryPicture(JSONObject json) throws JSONException {
        JournalGalleryPicture picture = new JournalGalleryPicture();
        picture.setHash(json.getString("hash"));
        picture.setUrl(json.optString("url"));
        picture.setHeight(json.optInt(HEIGHT_FIELD));
        picture.setWidth(json.optInt(WIDTH_FIELD));
        picture.setCaption(json.optString("caption"));
        picture.setFormat(json.getString("format"));
        picture.setLink(json.getJSONObject("links").getString("self"));
        picture.setCreated(DateTime.parse(json.getString("created")));
        JSONArray jsonChildren = json.getJSONArray("children");
        List<ChildTag> children = parseChildTag(jsonChildren);
        picture.setChildren(children);
        return picture;
    }

    private static List<ChildTag> parseChildTag(JSONArray jsonTags) throws JSONException {
        List<ChildTag> photoTags = new ArrayList<>();
        for (int j = 0; j < jsonTags.length(); j++) {
            JSONObject jsonChild = jsonTags.getJSONObject(j);
            ChildTag photoTag = new ChildTag();
            photoTag.setHeight(jsonChild.optInt(HEIGHT_FIELD));
            photoTag.setX(jsonChild.optInt("x"));
            photoTag.setY(jsonChild.optInt("y"));
            photoTag.setWidth(jsonChild.optInt(WIDTH_FIELD));
            photoTag.setChildId(jsonChild.getLong("child"));
            photoTag.setPictureId(jsonChild.getLong("picture"));

            JSONArray jsonCarers = jsonChild.getJSONArray("carersIds");
            List<Long> carersIds = new ArrayList<>(jsonCarers.length());
            for (int i = 0; i < jsonCarers.length(); i++) {
                carersIds.add(jsonCarers.getLong(i));
            }

            photoTag.setCarerIds(carersIds);
            photoTags.add(photoTag);
        }

        return photoTags;
    }
}
