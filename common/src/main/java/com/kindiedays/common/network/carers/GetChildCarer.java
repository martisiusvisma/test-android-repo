package com.kindiedays.common.network.carers;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.ChildCarer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 27/05/2015.
 */
public class GetChildCarer extends Webservice<List<ChildCarer>> {

    public static List<ChildCarer> getCarers(long childId) throws CommonKindieException{
        GetChildCarer getChildCarer = new GetChildCarer();
        return getChildCarer.getObjects("children/" + childId + "/carers");
    }

    @Override
    public List<ChildCarer> parseJsonFromServer(JSONObject json) throws JSONException {
        List<ChildCarer> carers = new ArrayList<>();
        JSONArray array = json.getJSONArray("childCarers");
        for(int i = 0 ; i < array.length() ; i++){
            JSONObject jsonCarer = (JSONObject) array.get(i);
            carers.add(CarerParser.parseCarer(jsonCarer));
        }
        return carers;
    }
}
