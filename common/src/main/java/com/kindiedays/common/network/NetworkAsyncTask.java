package com.kindiedays.common.network;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by pleonard on 15/05/2015.
 */
public abstract class NetworkAsyncTask<T> extends AsyncTask<Void, Void, T> {

    protected KindieDaysNetworkException exception;

    protected abstract T doNetworkTask() throws Exception;

    @Override
    protected T doInBackground(Void... params) {
        try{
            return doNetworkTask();
        }
        catch (KindieDaysNetworkException e){
            exception = e;
            return null;
        }
        catch(Exception e1){
            Throwable cause = e1.getCause();
            if (cause instanceof KindieDaysNetworkException){
                exception = (KindieDaysNetworkException) cause;
            }
            Log.e(this.getClass().getName(), "Unexpected exception: " + e1);
            return null;
        }
    }
}
