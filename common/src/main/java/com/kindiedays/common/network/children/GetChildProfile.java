package com.kindiedays.common.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.ChildProfile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 15/05/2015.
 */
public class GetChildProfile extends Webservice<ChildProfile>{

    public static ChildProfile getChild(long id) throws CommonKindieException {
        GetChildProfile gcp = new GetChildProfile();
        return gcp.getObjects("children/" + id + "/profile");
    }

    @Override
    public ChildProfile parseJsonFromServer(JSONObject json) throws JSONException {
        return ChildJsonParser.parseProfile(json);
    }
}
