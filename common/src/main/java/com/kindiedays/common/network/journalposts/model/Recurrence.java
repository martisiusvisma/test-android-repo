package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Recurrence {

    @SerializedName("event")
    @Expose
    private int event;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("interval")
    @Expose
    private String interval;

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    @Override
    public String toString() {
        return "Recurrence{" +
                "event=" + event +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", interval='" + interval + '\'' +
                '}';
    }
}
