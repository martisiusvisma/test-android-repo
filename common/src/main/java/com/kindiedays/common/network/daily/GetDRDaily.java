package com.kindiedays.common.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.GeneralParser;
import com.kindiedays.common.pojo.GeneralDailyRes;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRDaily extends Webservice<GeneralDailyRes> {

    public static GeneralDailyRes getItem(Long child, DateTime date) throws CommonKindieException{
        GetDRDaily item = new GetDRDaily();
        Log.d("myLogs", "GeneralDailyRes + " + item.getObjects("daily-report?childId=" + child + "&date=" + date.toString("yyyy-MM-dd")));
        return item.getObjects("daily-report/full?childId=" + child + "&date=" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public GeneralDailyRes parseJsonFromServer(JSONObject json) throws JSONException {
        return GeneralParser.parserItem(json);
    }

}
