package com.kindiedays.common.network.conversations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.utils.ArrayUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 28/05/2015.
 */
public class PostConversations extends Webservice<Conversation> {

    public static Conversation startNewConversation(String subject, String message, ConversationParty party,
                                                    boolean createAsKindergarten, long[] taggedChildren,
                                                    List<Attachment> attachments) throws CommonKindieException{
        try {
            JSONObject object = new JSONObject();
            object.put("subject", subject);
            object.put("message", message != null ? message : STRING_EMPTY);
            object.put("createAsKindergarten", createAsKindergarten);

            JSONArray partyArray = new JSONArray();
            JSONObject jsonParty = new JSONObject();
            jsonParty.put("partyId", party.getPartyId());
            jsonParty.put("type", party.getType());
            partyArray.put(jsonParty);
            object.put("parties", partyArray);
            object.put("children", ArrayUtils.longArrayToJson(taggedChildren));

            JSONArray jsonAttachments = new JSONArray();
            for (Attachment attachment : attachments) {
                JSONObject jsonFile = new JSONObject();
                jsonFile.put("pendingFileId", attachment.getPendingFileId());
                jsonFile.put("filename", attachment.getFilename());
                jsonFile.put("format", attachment.getFormat());
                jsonAttachments.put(jsonFile);
            }
            object.put("attachment", jsonAttachments);

            PostConversations postConversations = new PostConversations();
            return postConversations.postObjects("conversations/v2", object);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Conversation parseJsonFromServer(JSONObject json) throws JSONException {
        return ConversationParser.parseConversation(json);
    }
}
