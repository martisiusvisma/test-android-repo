package com.kindiedays.common.network.journalposts;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.JournalPost;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 10/06/2015.
 */
public class GetChildrenJournalPosts extends Webservice<List<JournalPost>> {

    public static List<JournalPost> getPosts(long childId) throws CommonKindieException {
        GetChildrenJournalPosts getChildrenJournalPosts = new GetChildrenJournalPosts();
        return getChildrenJournalPosts.getObjects("children/" + childId + "/journal-posts");
    }

    @Override
    public List<JournalPost> parseJsonFromServer(JSONObject json) throws JSONException {
        JSONArray array = json.getJSONArray("journalPosts");
        List<JournalPost> journalPosts = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            JournalPost journalPost = JournalPostJsonParser.parseJson(array.getJSONObject(i));
            journalPosts.add(journalPost);
        }
        return journalPosts;
    }
}
