package com.kindiedays.common.network.journalposts;

import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.pojo.JournalPostPicture;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/07/2015.
 */
public class JournalPostJsonParser {

    private JournalPostJsonParser() {
        //no instance
    }

    public static JournalPost parseJson(JSONObject json) throws JSONException {
        JournalPost journalPost = new JournalPost();
        journalPost.setId(json.getLong("id"));
        journalPost.setText(json.getString("text"));
        JSONArray picturesJSONArray = json.getJSONArray("pictures");
        journalPost.setTime(DateTime.parse(json.getString("created"), ISODateTimeFormat.dateTime()));
        List<JournalPostPicture> pictures = new ArrayList<>();
        for (int i = 0; i < picturesJSONArray.length(); i++) {
            JSONObject jsonPicture = picturesJSONArray.getJSONObject(i);
            JournalPostPicture journalPostPicture = new JournalPostPicture();
            journalPostPicture.setHash(jsonPicture.getString("hash"));
            journalPostPicture.setFormat(jsonPicture.getString("format"));
            journalPostPicture.setUrl(jsonPicture.getString("url"));
            journalPostPicture.setCreated(DateTime.parse(jsonPicture.getString("created")));
            journalPostPicture.setCaption(jsonPicture.optString("caption", ""));
            journalPostPicture.setNbPictures(picturesJSONArray.length());
            pictures.add(journalPostPicture);
        }
        journalPost.setJournalPostPictures(pictures);
        return journalPost;
    }
}
