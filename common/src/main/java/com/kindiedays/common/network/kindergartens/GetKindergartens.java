package com.kindiedays.common.network.kindergartens;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Kindergarten;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 29/06/2015.
 */
public class GetKindergartens extends Webservice<List<Kindergarten>> {


    public static List<Kindergarten> getKindergartens() throws CommonKindieException {
        GetKindergartens getKindergartens = new GetKindergartens();
        return getKindergartens.getObjects("kindergartens");
    }

    @Override
    public List<Kindergarten> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Kindergarten> kindergartens = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("kindergartens");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            kindergartens.add(KindergartenParser.parseKindergarten(object));
        }
        return kindergartens;
    }
}
