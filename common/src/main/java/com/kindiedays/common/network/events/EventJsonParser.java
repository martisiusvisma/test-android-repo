package com.kindiedays.common.network.events;

import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 10/07/2015.
 */
public class EventJsonParser {
    private EventJsonParser() {
        //no instance
    }

    public static Event parseEvent(JSONObject jsonEvent) throws JSONException {
        Event event = new Event();
        event.setId(jsonEvent.getInt("id"));
        event.setDate(LocalDate.parse(jsonEvent.getString("date")));
        event.setStart(LocalTime.parse(jsonEvent.getString("start")));
        event.setEnd(LocalTime.parse(jsonEvent.getString("end")));
        event.setName(jsonEvent.getString("name"));
        event.setNotes(jsonEvent.getString("notes"));
        event.setLocation(jsonEvent.getString("location"));
        event.setState(jsonEvent.getString("state"));
        if (jsonEvent.has("invitations")) {
            JSONArray jsonInvitations = jsonEvent.getJSONArray("invitations");
            List<EventInvitation> invitations = new ArrayList<>();
            for (int j = 0; j < jsonInvitations.length(); j++) {
                EventInvitation invitation = new EventInvitation();
                JSONObject jsonInvitation = jsonInvitations.getJSONObject(j);
                invitation.setChildId(jsonInvitation.getInt("child"));
                invitation.setEventId(jsonInvitation.getInt("event"));
                invitation.setState(EventInvitation.InvitationState.valueOf(jsonInvitation.getString("state")));
                invitations.add(invitation);
            }
            event.setInvitations(invitations);
        }
        JSONObject jsonRecurrence = jsonEvent.optJSONObject("recurrence");
        if (jsonRecurrence != null) {
            event.setInterval(jsonRecurrence.getString("interval"));
            event.setEndRecurrence(LocalDate.parse(jsonRecurrence.getString("end")));
        }
        return event;
    }
}