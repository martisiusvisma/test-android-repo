package com.kindiedays.common.network.kindergartens;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Kindergarten;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 29/06/2015.
 */
public class GetKindergarten extends Webservice<Kindergarten> {

    public static Kindergarten getKindergarten(int id) throws CommonKindieException {
        GetKindergarten getKindergarten = new GetKindergarten();
        return getKindergarten.getObjects("kindergartens/" + id);
    }

    @Override
    public Kindergarten parseJsonFromServer(JSONObject json) throws JSONException {
        return KindergartenParser.parseKindergarten(json);
    }
}
