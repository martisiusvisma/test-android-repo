package com.kindiedays.common.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.CategoryRes;
import com.kindiedays.common.pojo.DRSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRSetting extends Webservice<DRSettings> {

    public static DRSettings getSettings(long id) throws CommonKindieException {
        GetDRSetting getCategories = new GetDRSetting();
        return getCategories.getObjects("kindergartens/" + id + "/daily-report-settings");
    }

    @Override
    public DRSettings parseJsonFromServer(JSONObject json) throws JSONException {
        DRSettings settings = new DRSettings();
        List<CategoryRes> categories = new ArrayList<>();
        // Check category permission for activities
        if (Boolean.valueOf(json.optString("activities"))) {
            CategoryRes activities = new CategoryRes();
            activities.setName("activities");
            activities.setPriority(1);
            categories.add(activities);
        }
        // Check category permission for bathroom
        if (Boolean.valueOf(json.optString("bathroom"))) {
            CategoryRes bathroom = new CategoryRes();
            bathroom.setName("bathroom");
            bathroom.setPriority(3);
            categories.add(bathroom);
        }
        // Check category permission for meals
        if (Boolean.valueOf(json.optString("meals"))) {
            CategoryRes meals = new CategoryRes();
            meals.setName("meals");
            meals.setPriority(2);
            categories.add(meals);
        }
        // Check category permission for medication
        if (Boolean.valueOf(json.optString("medication"))) {
            CategoryRes medication = new CategoryRes();
            medication.setName("medication");
            medication.setPriority(6);
            categories.add(medication);
        }
        // Check category permission for mood
        if (Boolean.valueOf(json.optString("mood"))) {
            CategoryRes mood = new CategoryRes();
            mood.setName("mood");
            mood.setPriority(4);
            categories.add(mood);
        }
        // Check category permission for naps
        if (Boolean.valueOf(json.optString("naps"))) {
            CategoryRes setting = new CategoryRes();
            setting.setName("naps");
            setting.setPriority(5);
            categories.add(setting);
        }
        // Check category permission for notes
        if (Boolean.valueOf(json.optString("notes"))) {
            CategoryRes naps = new CategoryRes();
            naps.setName("notes");
            naps.setPriority(7);
            categories.add(naps);
        }
        if (Boolean.valueOf(json.optString("enabled"))) {
            settings.setEnabled(true);
        }
        settings.setKindergartenId(json.optInt("kindergarten", 0));
        settings.setCategoryResList(categories);
        return settings;
    }

}
