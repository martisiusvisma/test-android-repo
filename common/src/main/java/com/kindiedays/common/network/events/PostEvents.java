package com.kindiedays.common.network.events;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 10/06/2015.
 */
public class PostEvents extends Webservice<Event> {
    private static final String DATE_PATTERN = "YYYY-MM-dd";

    public static Event postEvent(Event event) throws CommonKindieException {
        try {
            PostEvents postEvents = new PostEvents();
            JSONObject json = new JSONObject();
            json.put("date", event.getDate().toString(DATE_PATTERN));
            json.put("start", event.getStart().toString("HH:mm:ss"));
            json.put("end", event.getEnd().toString("HH:mm:ss"));
            json.put("name", event.getName());
            json.put("notes", event.getNotes());
            json.put("location", event.getLocation());
            json.put("state", event.getState());
            JSONArray jsonInvitations = new JSONArray();
            for (EventInvitation invitation : event.getInvitations()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("child", invitation.getChildId());
                jsonObject.put("state", invitation.getState().toString());
                jsonInvitations.put(jsonObject);
            }
            json.put("invitations", jsonInvitations);
            if (event.getInterval() != null && !event.getInterval().equals(Event.NEVER)) {
                JSONObject jsonRecurrence = new JSONObject();
                jsonRecurrence.put("start", event.getDate().toString(DATE_PATTERN));
                if (event.getEndRecurrence() != null) {
                    jsonRecurrence.put("end", event.getEndRecurrence().toString(DATE_PATTERN));
                } else {
                    jsonRecurrence.put("end", JSONObject.NULL);
                }
                jsonRecurrence.put("interval", event.getInterval());
                json.put("recurrence", jsonRecurrence);
            } else {
                json.put("recurrence", JSONObject.NULL);
            }
            return postEvents.postObjects("events", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Event parseJsonFromServer(JSONObject json) throws JSONException {
        return EventJsonParser.parseEvent(json);
    }


}
