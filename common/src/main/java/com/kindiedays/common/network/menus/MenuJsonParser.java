package com.kindiedays.common.network.menus;

import com.kindiedays.common.pojo.Meal;
import com.kindiedays.common.pojo.Menu;

import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by pleonard on 10/07/2015.
 */
public class MenuJsonParser {
    private MenuJsonParser() {
        //no instance
    }
    public static Menu parseMenu(JSONObject jsonMenu) throws JSONException {
        Menu menu = new Menu();
        menu.setDate(LocalDate.parse(jsonMenu.getString("date")));
        JSONArray jsonMeals = jsonMenu.getJSONArray("meals");
        List<Meal> meals = new ArrayList<>();
        for (int j = 0; j < jsonMeals.length(); j++) {
            Meal meal = new Meal(jsonMeals.getJSONObject(j).getInt("index"), jsonMeals.getJSONObject(j).getString("text"));
            meals.add(meal);
        }
        menu.setMeals(meals);
        return menu;
    }
}