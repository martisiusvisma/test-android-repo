package com.kindiedays.common.network.events;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 10/06/2015.
 */
public class GetEvents extends Webservice<List<Event>> {

    public static List<Event> getEventList() throws CommonKindieException{
        GetEvents getEvents = new GetEvents();
        return getEvents.getObjects("events");
    }


    @Override
    public List<Event> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Event> events = new ArrayList<>();
        JSONArray jsonEvents = json.getJSONArray("events");
        for(int i = 0 ; i < jsonEvents.length() ; i++){
            JSONObject jsonEvent = jsonEvents.getJSONObject(i);
            if (!EventJsonParser.parseEvent(jsonEvent).getState().equals(Event.CANCELLED)){
                events.add(EventJsonParser.parseEvent(jsonEvent));
            }

        }
        return events;
    }
}