package com.kindiedays.common.network.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of <a href="http://tools.ietf.org/html/draft-nottingham-http-link-header-06">Link Headers v6</a>
 *
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class Link {
    protected String title;
    protected String relationship;
    protected String href;
    protected String type;
    protected Map<String, List<String>> extensions = new HashMap<>();

    public Link() {
    }

    public Link(String title, String relationship, String href, String type, Map<String, List<String>> extensions) {
        this.relationship = relationship;
        this.href = href;
        this.type = type;
        this.title = title;
        if (extensions != null) {
            this.extensions = extensions;
        }
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, List<String>> getExtensions() {
        return extensions;
    }


    public String toString() {
        StringBuilder buf = new StringBuilder("<");
        buf.append(href).append(">");
        if (relationship != null) {
            buf.append("; rel=\"").append(relationship).append("\"");
        }
        if (type != null) {
            buf.append("; type=\"").append(type).append("\"");
        }
        if (title != null) {
            buf.append("; title=\"").append(title).append("\"");
        }
        for (String key : getExtensions().keySet()) {
            List<String> values = getExtensions().get(key);
            for (String val : values) {
                buf.append("; ").append(key).append("=\"").append(val).append("\"");
            }
        }
        return buf.toString();
    }

}