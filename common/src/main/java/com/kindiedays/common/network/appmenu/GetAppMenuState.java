package com.kindiedays.common.network.appmenu;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.AppMenuState;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anduser on 29.01.18.
 */

public class GetAppMenuState extends Webservice<AppMenuState> {

    public static AppMenuState getAppMenuStates(long id) throws CommonKindieException {
        GetAppMenuState getStates = new GetAppMenuState();
        return getStates.getObjects("kindergartens/" + id + "/app-menu");
    }

    @Override
    public AppMenuState parseJsonFromServer(JSONObject json) throws JSONException {
        AppMenuState appMenuState = new AppMenuState();
        appMenuState.setKindergartenId(json.optInt("kindergarten", 0));
        appMenuState.setMessagesEnabled(json.optBoolean("messagesEnabled", true));
        appMenuState.setEventsEnabled(json.optBoolean("eventsEnabled", true));
        appMenuState.setMealsEnabled(json.optBoolean("mealsEnabled", true));
        appMenuState.setCalendarEnabled(json.optBoolean("calendarEnabled", true));
        appMenuState.setCameraEnabled(json.optBoolean("cameraEnabled", true));
        appMenuState.setJournalEnabled(json.optBoolean("journalEnabled", true));
        return appMenuState;
    }
}
