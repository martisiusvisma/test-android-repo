package com.kindiedays.common.network.session;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 24/09/2015.
 */
public class PatchSession extends Webservice<Void> {

    public static void patchKindergarden() throws CommonKindieException {
        try {
            PatchSession patchSession = new PatchSession();
            JSONObject json = new JSONObject();
            json.put("kindergarten", JSONObject.NULL);
            Log.d(PatchSession.class.getSimpleName(), "patchKindergarden: " + json);
            patchSession.patchObjects("sessions/current", json, authEndpoint);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    public static Void patchStarflightUuid(String uuid) throws CommonKindieException {
        try {
            PatchSession instance = new PatchSession();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("starFlightUuid", uuid);
            return instance.patchObjects("sessions/current", jsonObject, authEndpoint);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
