package com.kindiedays.common.network;


import android.os.AsyncTask;
import android.util.Log;

public abstract class NetworkProgressAsyncTask<T, S> extends AsyncTask<Void, S, T> {
    protected KindieDaysNetworkException exception;

    protected abstract T doNetworkTask() throws Exception;

    @Override
    protected T doInBackground(Void... params) {
        try {
            return doNetworkTask();
        } catch (KindieDaysNetworkException e) {
            exception = e;
            return null;
        } catch (Exception e1) {
            Log.e(this.getClass().getName(), "Unexpected exception: ", e1);
            return null;
        }
    }
}
