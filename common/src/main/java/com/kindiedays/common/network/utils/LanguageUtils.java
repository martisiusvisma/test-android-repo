package com.kindiedays.common.network.utils;

import com.kindiedays.common.KindieDaysApp;

public class LanguageUtils {

    private static final String NORWEGIAN_CODE = "no";
    private static final String NORWEGIAN_CODE_NN = "nn";

    public static String getLanguageCode(KindieDaysApp app) {
        String code = app.getResources().getConfiguration().locale.getLanguage();
        if (code.equals(NORWEGIAN_CODE)) {
            return NORWEGIAN_CODE_NN;
        } else {
            return code;
        }
    }
}
