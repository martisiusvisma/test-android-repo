package com.kindiedays.common.network.conversations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.GetPaginatedResource;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by pleonard on 28/05/2015.
 */
public class GetConversations extends GetPaginatedResource<ConversationList> {

    public static ConversationList getConversations() throws CommonKindieException {
        GetConversations getConversations = new GetConversations();
        return getConversations.getObjects("conversations/v2");
    }

    public static ConversationList getConversations(String url) throws CommonKindieException {
        GetConversations getConversations = new GetConversations();
        return getConversations.getObjectsFromUrl(url);
    }

    public static ConversationList getConversations(long childId) throws CommonKindieException {
        GetConversations getConversations = new GetConversations();
        return getConversations.getObjects("children/" + childId + "/conversations");
    }

    @Override
    public ConversationList parseJsonFromServer(JSONObject json) throws JSONException {
        ConversationList conversations = new ConversationList();
        JSONArray jsonConversations = json.getJSONArray("conversations");

        List<Conversation> temp = new ArrayList<>();
        for (int i = 0; i < jsonConversations.length(); i++) {
            JSONObject jsonConversation = jsonConversations.getJSONObject(i);
            temp.add(ConversationParser.parseConversation(jsonConversation));
        }
        Collections.sort(temp);
        Collections.reverse(temp);
        conversations.setConversations(temp);
        return conversations;
    }
}
