package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.MedRes;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class MedParser {
    private MedParser() {
        //no instance
    }

    public static MedRes parserMedItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mm:ss");
        MedRes medItem = new MedRes();
        medItem.setChild(json.getInt("child"));
        medItem.setDate(dataFormatter.parseDateTime(json.getString("date")));
        medItem.setTime(timeFormatter.parseDateTime(json.getString("time")));
        medItem.setLocal(false);
        medItem.setDrugName(json.getString("drugName"));
        medItem.setDosage(json.getString("dosage"));
        medItem.setNotes(json.optString("notes"));
        return medItem;
    }
}