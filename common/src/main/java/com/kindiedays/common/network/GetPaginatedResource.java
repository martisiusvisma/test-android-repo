package com.kindiedays.common.network;

import android.util.Log;

import com.kindiedays.common.network.utils.Link;
import com.kindiedays.common.network.utils.LinkHeader;
import com.kindiedays.common.network.utils.LinkHeaderParser;
import com.kindiedays.common.pojo.PaginatedResource;

import java.io.IOException;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;

import static com.kindiedays.common.ui.album.ThumbnailAdapter.PICTURES_NUMBER;

/**
 * Created by pleonard on 16/07/2015.
 */
@SuppressWarnings("deprecation")
public abstract class GetPaginatedResource<T extends PaginatedResource> extends Webservice<T> {
    private static final String TAG = GetPaginatedResource.class.getSimpleName();
    @Override
    protected T responseOk(HttpResponse response) throws IOException {
        Log.d(TAG, "responseOk: " + response);
        T paginatedResource = super.responseOk(response);
        if(paginatedResource != null) {
            LinkHeaderParser parser = new LinkHeaderParser();
            Header[] headers = response.getHeaders("Link");
            Log.d(TAG, "responseOk: " + Arrays.toString(headers));
            for (int i = 0; i < headers.length; i++) {
                resolvePaginatedResource(paginatedResource, parser, headers, i);
            }
        }
        return paginatedResource;
    }

    private void resolvePaginatedResource(T paginatedResource, LinkHeaderParser parser, Header[] headers, int i) {
        LinkHeader header = parser.parse(headers[i].getValue());
        Log.d(TAG, "resolvePaginatedResource: " + header.getLinks().size());
        for (int j = 0; j < header.getLinks().size(); j++) {

            Link link = header.getLinks().get(j);
            String nextLink;
            if (link.getHref().contains("pictures")){
                nextLink = link.getHref().substring(0, link.getHref().lastIndexOf("=") + 1) + PICTURES_NUMBER;
            } else {
                nextLink = link.getHref();
            }

            if ("next".equals(link.getRelationship())) {
                Log.d(TAG, "resolvePaginatedResource: next " + nextLink);
                paginatedResource.setLinkNext(nextLink);
            }
            if ("previous".equals(link.getRelationship())) {
                Log.d(TAG, "resolvePaginatedResource:  prev " + nextLink);
                paginatedResource.setLinkPrevious(nextLink);
            }

            if ("pack_next".equals(link.getRelationship())) {
                Log.d(TAG, "resolvePaginatedResource: pack_next " + nextLink);
                paginatedResource.setLinkPrevious(nextLink);
            }
        }
    }
}
