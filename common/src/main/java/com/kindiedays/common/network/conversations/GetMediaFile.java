package com.kindiedays.common.network.conversations;


import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import com.kindiedays.common.R;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.utils.FormatUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class GetMediaFile extends Webservice<Long> {
    public static Long downloadFile(final Context context, DownloadManager downloadManager, String filename, String url) {
        Uri downloadUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri)
                .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(context.getString(R.string.downloading_progress, filename))
                .setDescription(context.getString(R.string.downloading_description))
                .setVisibleInDownloadsUi(true)
                .setMimeType(FormatUtils.getMimeType(filename))
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
        return downloadManager.enqueue(request);
    }

    @Override
    public Long parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
