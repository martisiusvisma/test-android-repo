package com.kindiedays.common.network;

import com.facebook.stetho.inspector.network.NetworkEventReporter;

import java.io.IOException;

import javax.annotation.Nullable;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpRequestBase;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.util.EntityUtils;

import static cz.msebera.android.httpclient.Consts.UTF_8;

public class InspectorResponse implements NetworkEventReporter.InspectorResponse {

    private final HttpRequestBase request;
    private final HttpResponse response;
    private final String requestId;

    public InspectorResponse(HttpRequestBase request, HttpResponse response, String requestId) {
        this.request = request;
        this.response = response;
        this.requestId = requestId;
    }

    @Override
    public String url() {
        return request.getURI().toString();
    }

    @Override
    public boolean connectionReused() {
        return false;
    }

    @Override
    public int connectionId() {
        // TODO: 08.10.2018 change requestId to connectionId
        return requestId == null ? 0 : requestId.hashCode();
    }

    @Override
    public boolean fromDiskCache() {
        return false;
    }

    @Override
    public String requestId() {
        return requestId;
    }

    @Override
    public int statusCode() {
        return response.getStatusLine().getStatusCode();
    }

    @Override
    public String reasonPhrase() {
        try {
            String string = EntityUtils.toString(response.getEntity(), UTF_8);
            response.setEntity(new StringEntity(string, UTF_8));
            return string;
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public int headerCount() {
        return response.getAllHeaders().length;
    }

    @Override
    public String headerName(int index) {
        return response.getAllHeaders()[index].getName();
    }

    @Override
    public String headerValue(int index) {
        return response.getAllHeaders()[index].getValue();
    }

    @Nullable
    @Override
    public String firstHeaderValue(String name) {
        return response.getFirstHeader(name).getValue();
    }
}
