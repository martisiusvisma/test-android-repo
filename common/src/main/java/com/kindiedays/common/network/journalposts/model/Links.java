package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Eugeniy Shein on 29/11/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 29/11/2017
 */
public class Links {
    @SerializedName("self")
    @Expose
    private String self;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    @Override
    public String toString() {
        return "Links{" +
                "self='" + self + '\'' +
                '}';
    }
}
