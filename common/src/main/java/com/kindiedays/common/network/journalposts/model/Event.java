package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Eugeniy Shein on 29/11/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 29/11/2017
 */
public class Event {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("kindergarten")
    @Expose
    private int kindergarten;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("recurrence")
    @Expose
    private Recurrence recurrence;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKindergarten() {
        return kindergarten;
    }

    public void setKindergarten(int kindergarten) {
        this.kindergarten = kindergarten;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Recurrence getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", kindergarten=" + kindergarten +
                ", date='" + date + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", name='" + name + '\'' +
                ", notes='" + notes + '\'' +
                ", location='" + location + '\'' +
                ", state='" + state + '\'' +
                ", recurrence=" + recurrence +
                '}';
    }
}
