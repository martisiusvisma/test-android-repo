package com.kindiedays.common.network.children;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildPresence;
import com.kindiedays.common.pojo.ChildProfile;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 09/07/2015.
 */
public class ChildJsonParser {

    private ChildJsonParser() {
        //no instance
    }

    public static ChildPresence parsePresence(JSONObject json) throws JSONException {
        ChildPresence presence = new ChildPresence();
        presence.setCarerId(json.optLong("carer", 0));
        presence.setStart(LocalDate.parse(json.getString("start")));
        presence.setEnd(LocalDate.parse(json.getString("end")));
        presence.setChildId(json.getLong("child"));
        presence.setStatus(json.getString("status"));
        return presence;
    }

    public static Child parseChild(JSONObject jsonKid) throws JSONException {
        Child child = new Child();
        child.setId(jsonKid.getInt("id"));
        child.setGroupId(jsonKid.getInt("group"));
        child.setName(jsonKid.getString("name"));
        child.setNickname(jsonKid.optString("nickname", null));
        child.setKindergartenId(jsonKid.getInt("kindergarten"));

        JSONObject jsonPicture = jsonKid.getJSONObject("profilePicture");
        JSONArray jsonThumbnails = jsonPicture.getJSONArray("thumbnails");
        for (int j = jsonThumbnails.length() - 1; j >= 0; j--) {
            JSONObject thumbnail = jsonThumbnails.getJSONObject(j);
            if (thumbnail.getLong("width") == 256) {
                child.setProfilePictureUrl(thumbnail.getString("url"));
                break;
            }
        }
        return child;
    }

    public static ChildProfile parseProfile(JSONObject json) throws JSONException {
        ChildProfile profile = new ChildProfile();
        if (json.has("dropoffTime")) {
            profile.setDropoffTime(LocalTime.parse(json.getString("dropoffTime")));
        }
        if (json.has("pickupTime")) {
            profile.setPickupTime(LocalTime.parse(json.getString("pickupTime")));
        }
        profile.setPhotoConsent(json.getBoolean("photoConsent"));
        if (json.has("dateOfBirth")) {
            profile.setDateOfBirth(LocalDate.parse(json.getString("dateOfBirth")));
        }
        if (json.has("socialSecurityNumber")) {
            profile.setSocialSecurityNumber(json.getString("socialSecurityNumber"));
        }
        if (json.has("streetAddress")) {
            profile.setStreetAddress(json.optString("streetAddress", null));
        }
        if (json.has("zipCode")) {
            profile.setZipCode(json.optString("zipCode", null));
        }
        if (json.has("city")) {
            profile.setCity(json.optString("city", null));
        }
        if (json.has("nativeLanguage")) {
            profile.setNativeLanguage(json.optString("nativeLanguage", null));
        }
        if (json.has("allergies")) {
            profile.setAllergies(json.optString("allergies", null));
        }
        if (json.has("diet")) {
            profile.setDiet(json.optString("diet", null));
        }
        if (json.has("child")) {
            profile.setChildId(json.getLong("child"));
        }








        return profile;
    }
}
