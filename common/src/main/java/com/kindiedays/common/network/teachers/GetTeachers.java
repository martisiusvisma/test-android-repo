package com.kindiedays.common.network.teachers;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Teacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 30/04/2015.
 */
public class GetTeachers extends Webservice<List<Teacher>> {

    public static List<Teacher> getTeachers() throws CommonKindieException {
        GetTeachers getTeachers = new GetTeachers();
        return getTeachers.getObjects("teachers");
    }

    @Override
    public List<Teacher> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Teacher> teachers = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("teachers");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            teachers.add(ParseTeacher.parse(object));
        }
        return teachers;
    }
}
