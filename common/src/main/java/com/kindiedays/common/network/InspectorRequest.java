package com.kindiedays.common.network;

import com.facebook.stetho.inspector.network.NetworkEventReporter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Nullable;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.client.methods.HttpEntityEnclosingRequestBase;
import cz.msebera.android.httpclient.client.methods.HttpRequestBase;
import cz.msebera.android.httpclient.entity.StringEntity;

import static cz.msebera.android.httpclient.Consts.UTF_8;

public class InspectorRequest implements NetworkEventReporter.InspectorRequest {

    private final HttpRequestBase request;
    private final String requestId;

    public InspectorRequest(HttpRequestBase request, String requestId) {
        this.request = request;
        this.requestId = requestId;
    }

    @Nullable
    @Override
    public Integer friendlyNameExtra() {
        return null;
    }

    @Override
    public String url() {
        return request.getURI().toString();
    }

    @Override
    public String method() {
        return request.getMethod();
    }

    @Nullable
    @Override
    public byte[] body() throws IOException {
        if (!(request instanceof HttpEntityEnclosingRequestBase)) {
            return null;
        }
        HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase = (HttpEntityEnclosingRequestBase) request;
        HttpEntity entity = httpEntityEnclosingRequestBase.getEntity();
        InputStream content = entity.getContent();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];
        while ((nRead = content.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        byte[] bytes = buffer.toByteArray();

        httpEntityEnclosingRequestBase.setEntity(new StringEntity(new String(bytes, UTF_8), UTF_8));

        return bytes;
    }

    @Override
    public String id() {
        return requestId;
    }

    @Override
    public String friendlyName() {
        return null;
    }

    @Override
    public int headerCount() {
        return request.getAllHeaders().length;
    }

    @Override
    public String headerName(int index) {
        return request.getAllHeaders()[index].getName();
    }

    @Override
    public String headerValue(int index) {
        return request.getAllHeaders()[index].getValue();
    }

    @Nullable
    @Override
    public String firstHeaderValue(String name) {
        return request.getFirstHeader(name).getValue();
    }
}
