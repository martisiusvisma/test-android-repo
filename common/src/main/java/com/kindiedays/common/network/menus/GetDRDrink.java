package com.kindiedays.common.network.menus;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.DrinkParser;
import com.kindiedays.common.pojo.DrinkRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRDrink extends Webservice<List<DrinkRes>> {

    public static List<DrinkRes> getItems() throws CommonKindieException {
        GetDRDrink items = new GetDRDrink();
        return items.getObjects("daily-report/drinks");
    }

    @Override
    public List<DrinkRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<DrinkRes> drinkRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportDrinks");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            drinkRes.add(DrinkParser.parserDrinkItem(object));
        }
        return drinkRes;
    }

}
