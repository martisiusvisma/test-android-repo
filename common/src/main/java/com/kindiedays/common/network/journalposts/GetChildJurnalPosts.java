package com.kindiedays.common.network.journalposts;

import android.util.Log;

import com.google.gson.Gson;
import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.GetPaginatedResource;
import com.kindiedays.common.network.album.PictureParser;
import com.kindiedays.common.network.events.EventJsonParser;
import com.kindiedays.common.network.journalposts.model.ChildJurnalFixed;
import com.kindiedays.common.network.menus.MenuJsonParser;
import com.kindiedays.common.pojo.AbstractPicture;
import com.kindiedays.common.pojo.ChildJournalList;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.JournalGalleryPicture;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.pojo.Nap;
import com.kindiedays.common.pojo.Signin;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Eugeniy Shein on 29/11/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 29/11/2017
 */
public class GetChildJurnalPosts extends GetPaginatedResource<ChildJournalList> {
    private static final String TAG = GetChildJurnalPosts.class.getSimpleName();
    private static final String START_FIELD = "start";

    public static ChildJournalList getPosts(long childId) throws CommonKindieException {
        GetChildJurnalPosts getChildrenJournalPosts = new GetChildJurnalPosts();
        return getChildrenJournalPosts.getObjects("children/" + childId + "/journal"/* + "/pack"*/);// pack loads all data, it works very slowly
    }

    public static ChildJournalList getPosts(String url) throws CommonKindieException {
        GetChildJurnalPosts instance = new GetChildJurnalPosts();
        return instance.getObjectsFromUrl(url);
    }

    @Override
    public ChildJournalList parseJsonFromServer(JSONObject json) throws JSONException {
        ChildJurnalFixed childJurnalFixed = new Gson().fromJson(json.toString(), ChildJurnalFixed.class);
        Log.d(TAG, "parseJsonFromServer: " + json);
        if (!json.has(START_FIELD)) {
            return null;
        }
        List<String> postPictureHashes = new ArrayList<>();
        ChildJournalList childJournalList = new ChildJournalList();
        childJournalList.setStart(LocalDate.parse(json.getString(START_FIELD)));
        childJournalList.setEnd(LocalDate.parse(json.getString("end")));
        JSONArray eventArray = json.getJSONArray("events");
        for (int i = 0; i < eventArray.length(); i++) {
            Event event = EventJsonParser.parseEvent(eventArray.getJSONObject(i));
            childJournalList.getEvents().add(event);
        }
        if (json.has("menus")) {
            JSONArray menuArray = json.getJSONArray("menus");
            for (int i = 0; i < menuArray.length(); i++) {
                Menu menu = MenuJsonParser.parseMenu(menuArray.getJSONObject(i));
                childJournalList.getMenus().add(menu);
            }
        }
        if (json.has("naps")) {
            JSONArray napArray = json.getJSONArray("naps");
            for (int i = 0; i < napArray.length(); i++) {
                Nap nap = parseNap(napArray.getJSONObject(i));
                childJournalList.getNaps().add(nap);
            }
        }
        if (json.has("signins")) {
            JSONArray signinArray = json.getJSONArray("signins");
            for (int i = 0; i < signinArray.length(); i++) {
                Signin signin = parseSignin(signinArray.getJSONObject(i));
                childJournalList.getSignins().add(signin);
            }
        }
        if (json.has("journalPosts")) {
            JSONArray journalPostArray = json.getJSONArray("journalPosts");
            for (int i = 0; i < journalPostArray.length(); i++) {
                JournalPost journalPost = JournalPostJsonParser.parseJson(journalPostArray.getJSONObject(i));
                childJournalList.getJournalPosts().add(journalPost);
                for (AbstractPicture abstractPicture : journalPost.getJournalPostPictures()){
                    postPictureHashes.add(abstractPicture.getHash());
                }
            }
        }
        if (json.has("galleryPictures")) {
            JSONArray galleryPictureArray = json.getJSONArray("galleryPictures");
            for (int i = 0; i < galleryPictureArray.length(); i++) {
                JournalGalleryPicture journalGalleryPicture = PictureParser
                        .parseJournalGalleryPicture(galleryPictureArray.getJSONObject(i));
                if (!postPictureHashes.contains(journalGalleryPicture.getHash())){
                    childJournalList.getGalleryPictures().add(journalGalleryPicture);
                }

            }
        }

        return childJournalList;
    }

    public Signin parseSignin(JSONObject json) throws JSONException {
        Signin signin = new Signin();
        signin.setChildId(json.getLong("child"));
        signin.setStart(DateTime.parse(json.getString(START_FIELD)));
        if (json.has("end") && !json.isNull("end")) {
            signin.setEnd(DateTime.parse(json.getString("end")));
        }
        signin.setPlaceId(json.getLong("place"));
        signin.setType(json.getString("type"));
        return signin;
    }

    public Nap parseNap(JSONObject json) throws JSONException {
        Nap nap = new Nap();
        nap.setChildId(json.getLong("child"));
        nap.setStart(DateTime.parse(json.getString(START_FIELD)));
        if (json.has("end") && !json.isNull("end")) {
            nap.setEnd(DateTime.parse(json.getString("end")));
        }
        return nap;
    }

    public static List<String> splitString(String text, int sliceSize) {
        ArrayList<String> textList = new ArrayList<String>();
        String aux;
        int left = -1, right = 0;
        int charsLeft = text.length();
        while (charsLeft != 0) {
            left = right;
            if (charsLeft >= sliceSize) {
                right += sliceSize;
                charsLeft -= sliceSize;
            }
            else {
                right = text.length();
                aux = text.substring(left, right);
                charsLeft = 0;
            }
            aux = text.substring(left, right);
            textList.add(aux);
        }
        return textList;
    }

}
