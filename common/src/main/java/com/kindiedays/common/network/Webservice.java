package com.kindiedays.common.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.stetho.inspector.network.NetworkEventReporter;
import com.facebook.stetho.inspector.network.NetworkEventReporterImpl;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpStatus;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpDelete;
import cz.msebera.android.httpclient.client.methods.HttpEntityEnclosingRequestBase;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPatch;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.client.methods.HttpRequestBase;
import cz.msebera.android.httpclient.client.utils.URLEncodedUtils;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;
import cz.msebera.android.httpclient.util.TextUtils;

import static cz.msebera.android.httpclient.Consts.UTF_8;

/**
 * Created by pleonard on 20/04/2015.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public abstract class Webservice<T> {
    /*protected static final String STAGING_API_ENDPOINT = "https://app-staging.kindiedays.com/api/v1";
    protected static final String STAGING_AUTH_ENDPOINT = "https://app-staging.kindiedays.com/auth/v1";
    protected static final String STAGING_CHANGE_PASS_ENDPOINT = "https://app-staging.kindiedays.com";
    protected static final String PROD_API_ENDPOINT = "https://app-prod.kindiedays.com/api/v1";
    protected static final String PROD_AUTH_ENDPOINT = "https://app-prod.kindiedays.com/auth/v1";
    protected static final String PROD_CHANGE_PASS_ENDPOINT = "https://app-prod.kindiedays.com";*/

    protected static final String STAGING_API_ENDPOINT = "http://192.168.12.227:8090/api/v1";
    protected static final String STAGING_AUTH_ENDPOINT = "http://192.168.12.227:8090/auth/v1";
    protected static final String STAGING_CHANGE_PASS_ENDPOINT = "http://192.12.227.109:8090/";
    protected static final String PROD_API_ENDPOINT = "http://192.168.12.227:8090/api/v1";
    protected static final String PROD_AUTH_ENDPOINT = "http://192.168.12.227:8090/auth/v1";
    protected static final String PROD_CHANGE_PASS_ENDPOINT = "http://192.168.12.227:8090/";

    protected static final String PREFERENCES_FILE = "preferences";
    protected static final String AUTHORIZATION_TOKEN = "authorizationToken";
    protected static final String ENVIRONMENT = "environmnent";

    private static final int MAX_LOG_MESSAGE_LENGTH = 4000;
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON_CHARSET = "application/json;charset=utf-8";

    private static final Http.HttpClientHolder httpClientHolder = Http.buildHttpClient(60 * 1000, true);
    protected static String authorizationToken;
    protected static String apiEndpoint;
    protected static String authEndpoint;
    protected static String changePasswordEndpoint;

    private final NetworkEventReporter mEventReporter = NetworkEventReporterImpl.get();

    private static void initAuthorizationTokenIfNeeded() {
        if (authorizationToken == null) {
            SharedPreferences preferences = KindieDaysApp.getApp().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
            authorizationToken = preferences.getString(AUTHORIZATION_TOKEN, null);
        }
    }

    protected static void initEndPointsIfNeeded() {
        if (apiEndpoint == null || authEndpoint == null) {
            SharedPreferences preferences = KindieDaysApp.getApp().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
            if (!preferences.getBoolean(ENVIRONMENT, false)) {
                apiEndpoint = STAGING_API_ENDPOINT;
                authEndpoint = STAGING_AUTH_ENDPOINT;
                changePasswordEndpoint = STAGING_CHANGE_PASS_ENDPOINT;
            } else {
                apiEndpoint = PROD_API_ENDPOINT;
                authEndpoint = PROD_AUTH_ENDPOINT;
                changePasswordEndpoint = PROD_CHANGE_PASS_ENDPOINT;
            }
        }
    }

    protected static void clearSessionSettings() {
        authorizationToken = null;
        apiEndpoint = null;
        authEndpoint = null;
        KindieDaysApp.getApp().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit().clear().commit();
    }

    public static boolean isAuthorizationTokenInitialized() {
        initAuthorizationTokenIfNeeded();
        return authorizationToken != null;
    }


    public T postObjects(String resource, JSONObject json) throws CommonKindieException {

        initEndPointsIfNeeded();
        return postObjects(resource, json, apiEndpoint);
    }

    public T postObjects(String resource, JSONObject postJson, String endpoint) throws CommonKindieException {
        try {
            String url = endpoint + "/" + resource;
            HttpEntityEnclosingRequestBase postRequest = new HttpPost();
            if (postJson != null) {
                HttpEntity entity = new StringEntity(postJson.toString(), UTF_8);
                postRequest.setEntity(entity);
                postRequest.setHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET);
            }
            initAuthorizationTokenIfNeeded();
            Http.addCommonHeaders(postRequest, authorizationToken);
            postRequest.setURI(new URI(url));
            return executeRequest(postRequest);
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }

    public T putObjects(String resource, JSONObject json) throws CommonKindieException {
        initEndPointsIfNeeded();
        return putObjects(resource, json, apiEndpoint);
    }

    public T putObjects(String resource, JSONObject putJson, String endpoint) throws CommonKindieException {
        try {
            String url = endpoint + "/" + resource;
            HttpEntityEnclosingRequestBase putRequest = new HttpPut();
            if (putJson != null) {
                HttpEntity entity = new StringEntity(putJson.toString(), UTF_8);
                putRequest.setEntity(entity);
                putRequest.setHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET);
            }
            initAuthorizationTokenIfNeeded();
            Http.addCommonHeaders(putRequest, authorizationToken);
            putRequest.setURI(new URI(url));
            return executeRequest(putRequest);
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }

    public T patchObjects(String resource, JSONObject json) throws CommonKindieException {
        initEndPointsIfNeeded();
        return patchObjects(resource, json, apiEndpoint);
    }

    public T patchObjects(JSONObject json, String url) throws CommonKindieException {
        try {
            HttpEntityEnclosingRequestBase request = new HttpPatch();
            HttpEntity entity = new StringEntity(json.toString(), UTF_8);
            request.setEntity(entity);
            initAuthorizationTokenIfNeeded();
            Http.addCommonHeaders(request, authorizationToken);
            request.setHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET);
            request.setURI(new URI(url));
            return executeRequest(request);
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }


    public T patchObjects(String resource, JSONObject json, String endpoint) throws CommonKindieException {
        String url = endpoint + "/" + resource;
        return patchObjects(json, url);
    }

    public T deleteObjectsAtUrl(String url) throws CommonKindieException {
        try {
            HttpRequestBase request = new HttpDelete();
            initAuthorizationTokenIfNeeded();
            Http.addCommonHeaders(request, authorizationToken);
            request.setURI(new URI(url));
            return executeRequest(request);
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }

    public T deleteObjects(String resource) throws CommonKindieException {
        initEndPointsIfNeeded();
        return deleteObjects(resource, apiEndpoint);
    }

    public T deleteObjects(String resource, String endpoint) throws CommonKindieException {
        try {
            String url = endpoint + "/" + resource;
            HttpRequestBase request = new HttpDelete();
            initAuthorizationTokenIfNeeded();
            Http.addCommonHeaders(request, authorizationToken);
            request.setURI(new URI(url));
            return executeRequest(request);
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }

    public T getObjects(String resource) throws CommonKindieException {
        initEndPointsIfNeeded();
        return getObjects(resource, apiEndpoint);
    }

    public T getObjects(String resource, String endPoint, String... params) throws CommonKindieException {
        String url = endPoint + "/" + resource;
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        if (params != null && params.length > 0) {
            url += "?";
            for (int i = 0; i < params.length; i = i + 2) {
                NameValuePair nameValuePair = new BasicNameValuePair(params[i], params[i + 1]);
                nameValuePairs.add(nameValuePair);
            }
        }
        url += URLEncodedUtils.format(nameValuePairs, UTF_8);
        return getObjectsFromUrl(url);
    }

    public T getObjectsFromUrl(String url) throws CommonKindieException {
        try {
            HttpRequestBase request = new HttpGet();
            initAuthorizationTokenIfNeeded();
            Http.addCommonHeaders(request, authorizationToken);
            request.setURI(new URI(url));
            return executeRequest(request);
        } catch (Exception e) {
            throw new CommonKindieException(e);
        }
    }

    protected T responseOk(HttpResponse response) throws IOException {
        String responseString = EntityUtils.toString(response.getEntity(), UTF_8);
        if (responseString != null && responseString.length() > 0) {
            try {
                return parseJsonFromServer(new JSONObject(responseString));
            } catch (Exception e) {
                Log.e(Webservice.class.getName(), e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    protected T responseCreated(HttpResponse response) throws IOException {
        return responseOk(response);
    }

    @SuppressWarnings("squid:S1172")
    protected T responseNoContent(HttpResponse response) {
        return null;
    }

    protected void responseBadRequest(HttpResponse response) throws IOException {
        connectionError(response, true, true);
    }

    protected void responseUnauthorized(HttpResponse response) throws IOException {
        clearSessionSettings();
        connectionError(response, true, false);
    }

    protected void responseForbidden(HttpResponse response) throws IOException {
        connectionError(response, true, false);
    }

    protected void responseNotFound(HttpResponse response) throws IOException {
        connectionError(response, true, true);
    }

    protected void responseConflict(HttpResponse response) throws IOException {
        connectionError(response, true, true);
    }

    protected void responseUnsupportedMediaType(HttpResponse response) throws IOException {
        connectionError(response, false, false);
    }

    protected void responseServiceUnavailable(HttpResponse response) throws IOException {
        throw new KindieDaysNetworkException(HttpStatus.SC_SERVICE_UNAVAILABLE, KindieDaysApp.getApp().getString(R.string.Service_Not_Available), true);
    }

    protected void responseUnprocessableEntity(HttpResponse response) throws IOException {
        KindieDaysNetworkException exception;
        try {
            JSONObject json = new JSONObject(EntityUtils.toString(response.getEntity()));
            String error = json.getString("error");
            JSONArray payload = json.getJSONArray("errors");
            exception = new KindieDaysNetworkException(response.getStatusLine().getStatusCode(), error, true, payload);
            Log.e(Webservice.class.getName(), response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase(), exception);
        } catch (JSONException e) {
            Log.e(Webservice.class.getName(), "unable to parse error", e);
            exception = new KindieDaysNetworkException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), false);
            Log.e(Webservice.class.getName(), response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase(), exception);
        }
        throw exception;
    }

    @SuppressWarnings({"squid:MethodCyclomaticComplexity", "squid:S1151"})
    protected T executeRequest(HttpRequestBase request) throws IOException {
        HttpClient client = httpClientHolder.get();

        String requestId = mEventReporter.nextRequestId();
        if (mEventReporter.isEnabled()) {
            mEventReporter.requestWillBeSent(new InspectorRequest(request, requestId));
        }

       /* String requestPayload = getRequestPayload(request);
        String requestHeaders = getHttpMessageHeaders(request);
        Log.d(Webservice.class.getName(), "Request #" + requestId + " : " + request.getMethod() + " " + request.getURI().toString() + " , headers : " + requestHeaders);
        if (!TextUtils.isEmpty(requestPayload)) {
            largeLog(Webservice.class.getName(), "Request #" + requestId + " : " + request.getMethod() + " " + request.getURI().toString() + " , with payload :" + requestPayload);
        }
*/
        HttpResponse response = client.execute(request);
        Log.d(Webservice.class.getName(), "Response #" + requestId + " : " + request.getMethod() + " " + request.getURI().toString() + " , StatusCode : " + response.getStatusLine().getStatusCode());
        String responseHeaders = getHttpMessageHeaders(request);
        Log.d(Webservice.class.getName(), "Response #" + requestId + " : " + request.getMethod() + " " + request.getURI().toString() + " , responseHeaders : " + responseHeaders);
        String responsePayload = getResponsePayload(response);
        String responsePayloadMessage = TextUtils.isEmpty(responsePayload) ? "[empty response]" : responsePayload;
        largeLog(Webservice.class.getName(), "Response #" + requestId + " : " + request.getMethod() + " " + request.getURI().toString() + " , responsePayload : " + responsePayloadMessage);

        if (mEventReporter.isEnabled()) {
            mEventReporter.responseHeadersReceived(new InspectorResponse(request, response, requestId));
        }

        switch (response.getStatusLine().getStatusCode()) {
        case HttpStatus.SC_OK:
            return responseOk(response);
        case HttpStatus.SC_CREATED:
            return responseCreated(response);
        case HttpStatus.SC_NO_CONTENT:
            return responseNoContent(response);
        case HttpStatus.SC_BAD_REQUEST:
            responseBadRequest(response);
            break;
        case HttpStatus.SC_UNAUTHORIZED:
            responseUnauthorized(response);
            break;
        case HttpStatus.SC_FORBIDDEN:
            responseForbidden(response);
            break;
        case HttpStatus.SC_NOT_FOUND:
            responseNotFound(response);
            break;
        case HttpStatus.SC_CONFLICT:
            responseConflict(response);
            break;
        case HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE:
            responseUnsupportedMediaType(response);
            break;
        case HttpStatus.SC_UNPROCESSABLE_ENTITY:
            responseUnprocessableEntity(response);
            break;
        case HttpStatus.SC_SERVICE_UNAVAILABLE:
            responseServiceUnavailable(response);
            break;
        default:
            if (response.getStatusLine().getStatusCode() >= 400 && response.getStatusLine().getStatusCode() <= 499) {
                connectionError(response, false, false);
            } else if (response.getStatusLine().getStatusCode() >= 500 && response.getStatusLine().getStatusCode() <= 599) {
                throw new KindieDaysNetworkException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), false);
            }
            break;
        }
        return null;
    }

    /**
     * Error codes 400-499
     *
     * @param response
     * @param parseError
     * @param localizedError
     * @throws Exception
     */
    protected void connectionError(HttpResponse response, boolean parseError,
                                   boolean localizedError) throws KindieDaysNetworkException {

        String error = response.getStatusLine().getReasonPhrase();
        if (parseError) {
            try {
                JSONObject json = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));
                error = json.getString("error");
            } catch (Exception e) {
                Log.e(Webservice.class.getName(), "unable to parse error", e);
            }
        }
        KindieDaysNetworkException exception = new KindieDaysNetworkException(response.getStatusLine().getStatusCode(), error, localizedError);
        Log.e(Webservice.class.getName(), response.getStatusLine().getReasonPhrase(), exception);
        throw exception;
    }

    public abstract T parseJsonFromServer(JSONObject json) throws JSONException;

    protected String getHttpMessageHeaders(HttpMessage message) {
       return android.text.TextUtils.join(", " , message.getAllHeaders());
    }

    protected String getRequestPayload(HttpRequestBase request) throws IOException {
        if (!(request instanceof HttpEntityEnclosingRequestBase)) {
            return null;
        }
        HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase = (HttpEntityEnclosingRequestBase) request;
        HttpEntity entity = httpEntityEnclosingRequestBase.getEntity();
        InputStream content = entity.getContent();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];
        while ((nRead = content.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        byte[] bytes = buffer.toByteArray();

        String string = new String(bytes, UTF_8);
        httpEntityEnclosingRequestBase.setEntity(new StringEntity(string, UTF_8));

        return string;
    }

    protected String getResponsePayload(HttpResponse response) throws IOException {
        HttpEntity entity = response.getEntity();
        if (entity == null){
            return null;
        }
        String string = EntityUtils.toString(entity, UTF_8);
        response.setEntity(new StringEntity(string, UTF_8));
        return string;
    }

    public static void largeLog(String tag, String content) {
        if (content.length() > MAX_LOG_MESSAGE_LENGTH) {
            Log.d(tag, content.substring(0, MAX_LOG_MESSAGE_LENGTH));
            largeLog(tag, content.substring(MAX_LOG_MESSAGE_LENGTH));
        } else {
            Log.d(tag, content);
        }
    }
}
