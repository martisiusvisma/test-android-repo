package com.kindiedays.common.network.teachers;

import com.kindiedays.common.pojo.Teacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by pleonard on 28/01/2016.
 */
public class ParseTeacher {
    private ParseTeacher() {
        //no instance
    }
    public static Teacher parse(JSONObject json) throws JSONException {
        Teacher teacher = new Teacher();
        teacher.setName(json.getString("name"));
        teacher.setId(json.getInt("id"));
        teacher.setUnreadMessages(json.optInt("unreadMessages"));
        JSONObject jsonPicture = json.getJSONObject("profilePicture");
        JSONArray jsonThumbnails = jsonPicture.getJSONArray("thumbnails");
        for (int j = jsonThumbnails.length() - 1; j >= 0; j--) {
            JSONObject thumbnail = jsonThumbnails.getJSONObject(j);
            if (thumbnail.getLong("width") == 256) {
                teacher.setProfilePictureUrl(thumbnail.getString("url"));
                break;
            }
        }
        if (json.has("groups")) {
            JSONArray groups = json.getJSONArray("groups");
            for (int i = 0; i < groups.length(); i++) {
                teacher.getGroups().add(groups.getLong(i));
            }
        }
        return teacher;
    }
}