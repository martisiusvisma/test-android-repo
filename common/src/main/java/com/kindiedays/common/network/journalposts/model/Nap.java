package com.kindiedays.common.network.journalposts.model;

/**
 * @author Eugeniy Shein on 29/11/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 29/11/2017
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Nap {

    @SerializedName("child")
    @Expose
    private int child;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Nap{" +
                "child=" + child +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
