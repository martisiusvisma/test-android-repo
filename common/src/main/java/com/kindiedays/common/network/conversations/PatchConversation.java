package com.kindiedays.common.network.conversations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationPatchResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 14/08/2015.
 */
public class PatchConversation extends Webservice<ConversationPatchResponse> {
    private static final String CONVERSATION_FIELD = "lastReadMessageIndex";

    public static ConversationPatchResponse markConversationAsRead(Conversation conversation,
                                                                   int lastReadMessageId) throws CommonKindieException {
        try {
            PatchConversation instance = new PatchConversation();
            JSONObject json = new JSONObject();
            json.put(CONVERSATION_FIELD, lastReadMessageId);
            return instance.patchObjects("conversations/v2/" + conversation.getId(), json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    public static void hideConversation(long conversationId, boolean hidden) throws CommonKindieException {
        try {
            PatchConversation instance = new PatchConversation();
            JSONObject json = new JSONObject();
            json.put("hidden", hidden);
            instance.patchObjects("conversations/v2/" + conversationId, json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public ConversationPatchResponse parseJsonFromServer(JSONObject json) throws JSONException {
        ConversationPatchResponse conversationResponse = new ConversationPatchResponse();
        if (!json.isNull("messagesRead")) {
            conversationResponse.setMessagesRead(json.getInt("messagesRead"));
        } else if (!json.isNull(CONVERSATION_FIELD)) {
            conversationResponse.setLastReadMessageIndex(json.getInt(CONVERSATION_FIELD));
        }
        return conversationResponse;
    }
}
