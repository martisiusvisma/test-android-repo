package com.kindiedays.common.network.places;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pleonard on 22/04/2015.
 */
public class GetPlaces extends Webservice<List<Place>> {

    public static List<Place> getPlaces() throws CommonKindieException {
        GetPlaces getPlaces = new GetPlaces();
        return getPlaces.getObjects("places");
    }

    @Override
    public List<Place> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Place> places = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("places");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            places.add(PlaceParser.parserPlace(object));
        }
        return places;
    }
}
