package com.kindiedays.common.network.groups;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Group;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetGroups extends Webservice<List<Group>> {

    public static List<Group> getGroups() throws CommonKindieException {
        GetGroups getGroups = new GetGroups();
        return getGroups.getObjects("groups");
    }

    @Override
    public List<Group> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Group> groups = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("groups");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            Group group = new Group();
            group.setName(object.getString("name"));
            group.setId(object.getInt("id"));


            // this is some functionality, which locks/unlocks Daily Report field in some group of the kindergarden
            // to enable just uncomment
            //group.setDailyReportEnabled(object.optBoolean("dailyReportEnabled", false));
            group.setDailyReportEnabled(true);

            groups.add(group);
        }
        return groups;
    }

}
