package com.kindiedays.common.network.kindergartens;

import com.kindiedays.common.pojo.Kindergarten;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 19/08/2015.
 */
public class KindergartenParser {

    private KindergartenParser() {
        //no instance
    }

    public static Kindergarten parseKindergarten(JSONObject json) throws JSONException {
        Kindergarten kindergarten = new Kindergarten();
        kindergarten.setId(json.getInt("id"));
        kindergarten.setName(json.getString("name"));
        kindergarten.setTeacherEmail(json.getString("teacherEmail"));
        kindergarten.setManagerEmail(json.getString("managerEmail"));
        kindergarten.setTimezone((json.getString("timeZone")));
        kindergarten.setTelephoneNumber(json.getString("telephoneNumber"));
        kindergarten.setAddress(json.getString("address"));
        kindergarten.setDefaultAttendance(json.getString("defaultAttendance"));
        kindergarten.setPresenceLockDays(json.getInt("presenceLockDays"));
        JSONArray jsonThumbnails = json.getJSONObject("profilePicture").getJSONArray("thumbnails");
        for (int j = jsonThumbnails.length() - 1; j >= 0; j--) {
            JSONObject thumbnail = jsonThumbnails.getJSONObject(j);
            if (thumbnail.getLong("width") == 256) {
                kindergarten.setProfilePictureUrl(thumbnail.getString("url"));
                break;
            }
        }
        return kindergarten;
    }
}
