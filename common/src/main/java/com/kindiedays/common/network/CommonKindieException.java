package com.kindiedays.common.network;


public class CommonKindieException extends Exception {
    public CommonKindieException() {
    }

    public CommonKindieException(String message) {
        super(message);
    }

    public CommonKindieException(Throwable cause) {
        super(cause);
    }
}
