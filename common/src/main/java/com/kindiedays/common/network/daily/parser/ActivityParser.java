
package com.kindiedays.common.network.daily.parser;

import android.util.Log;

import com.kindiedays.common.pojo.ActivityRes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class ActivityParser {
    private static final String TAG = ActivityParser.class.getSimpleName();

    private ActivityParser() {
        //no instance
    }

    public static ActivityRes parserActivityItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        Integer child = json.getInt("child");
        DateTime now = dataFormatter.parseDateTime(json.getString("date"));
        DateTime created = DateTime.parse(json.getString("created"));
        DateTime modified = DateTime.parse(json.getString("modified"));
        Long category = json.getLong("category");
        String categoryName = json.getString("categoryName");
        String text = json.getString("text");
        ActivityRes activityItem = new ActivityRes(child, now, created, category, categoryName, text);
        activityItem.setModified(modified);
        Log.d(TAG, "parserActivityItem: child" + " " + now + " " + created + " " + category
                + " " + categoryName + " " + text + " " + modified);
        return activityItem;
    }
}