package com.kindiedays.common.network.carers;

import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.Couple;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by pleonard on 18/12/2015.
 */
public class CarerParser {

    private CarerParser() {
        //no instance
    }

    public static ChildCarer parseCarer(JSONObject jsonCarer) throws JSONException {
        ChildCarer carer = new ChildCarer();
        carer.setId(jsonCarer.getInt("carer"));
        carer.setChildId(jsonCarer.getInt("child"));
        carer.setEmail(jsonCarer.optString("carerEmail"));
        carer.setName(jsonCarer.getString("carerName"));
        carer.setType(jsonCarer.getString("type"));
        carer.setPickupPermission(jsonCarer.getBoolean("pickupPermission"));
        carer.setOccupation(jsonCarer.optString("carerOccupation"));
        carer.setPlaceOfWork(jsonCarer.optString("carerPlaceOfWork"));
        carer.setWorkPhoneNumber(jsonCarer.optString("carerWorkPhoneNumber"));
        carer.setHomePhoneNumber(jsonCarer.optString("carerHomePhoneNumber"));
        carer.setMobilePhoneNumber(jsonCarer.optString("carerMobilePhoneNumber"));
        carer.setNativeLanguage(jsonCarer.optString("carerNativeLanguage"));
        carer.setContactDetailsPublic(jsonCarer.getBoolean("carerContactDetailsPublic"));
        carer.setReadonly(jsonCarer.getBoolean("readOnly"));
        carer.setDefaultConversationParty(jsonCarer.getBoolean("defaultConversationParty"));
        return carer;
    }

    public static ChildCarer parseSingleCarer(JSONObject jsonCarer) throws JSONException {
        ChildCarer carer = new ChildCarer();
        carer.setId(jsonCarer.getInt("id"));
        carer.setEmail(jsonCarer.getString("email"));
        carer.setName(jsonCarer.getString("name"));
        carer.setOccupation(jsonCarer.optString("occupation"));
        carer.setPlaceOfWork(jsonCarer.optString("placeOfWork"));
        carer.setWorkPhoneNumber(jsonCarer.optString("workPhoneNumber"));
        carer.setHomePhoneNumber(jsonCarer.optString("homePhoneNumber"));
        carer.setMobilePhoneNumber(jsonCarer.optString("mobilePhoneNumber"));
        carer.setUnreadEvents(jsonCarer.optInt("unreadEvents"));
        carer.setUnreadMessages(jsonCarer.optInt("unreadMessages"));
        carer.setNativeLanguage(jsonCarer.optString("nativeLanguage"));
        carer.setContactDetailsPublic(jsonCarer.getBoolean("contactDetailsPublic"));

        JSONObject unreadMessagesChildren = jsonCarer.getJSONObject("childrenUnreadMessages");
        List<Couple> unreadMessagesList = new ArrayList<>();
        Iterator<String> keysMessages = unreadMessagesChildren.keys();
        while (keysMessages.hasNext()) {
            String key = keysMessages.next();
            Couple newItem = new Couple();
            newItem.setChildId(key);
            newItem.setValue((Integer) unreadMessagesChildren.get(key));
            unreadMessagesList.add(newItem);
        }
        carer.setChildrenUnreadMessages(unreadMessagesList);

        JSONObject unreadEventsChildren = jsonCarer.getJSONObject("childrenUnreadEvents");
        List<Couple> unreadEventsList = new ArrayList<>();
        Iterator<String> keysEvents = unreadEventsChildren.keys();
        while (keysEvents.hasNext()) {
            String key = keysEvents.next();
            Couple newItem = new Couple();
            newItem.setChildId(key);
            newItem.setValue((Integer) unreadEventsChildren.get(key));
            unreadEventsList.add(newItem);
        }
        carer.setChildrenUnreadEvents(unreadEventsList);

        return carer;
    }
}
