package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.pojo.NapRes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class NapParser {
    private NapParser() {
        //no instance
    }

    public static NapRes parserNapItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        NapRes napItem = new NapRes();
        napItem.setChild(json.getInt("child"));
        napItem.setDate(dataFormatter.parseDateTime(json.getString("date")));
        napItem.setStart(DateTime.parse(json.getString("start")).toDateTime(KindergartenBusiness.getInstance().getTimeZone()));
        napItem.setEnd(DateTime.parse(json.getString("end")).toDateTime(KindergartenBusiness.getInstance().getTimeZone()));
        napItem.setLocal(false);
        napItem.setNotes(json.optString("notes"));
        return napItem;
    }
}