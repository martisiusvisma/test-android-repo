package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Eugeniy Shein on 29/11/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 29/11/2017
 */
public class Signin {
    @SerializedName("child")
    @Expose
    private int child;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("place")
    @Expose
    private int place;
    @SerializedName("type")
    @Expose
    private String type;

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Signin{" +
                "child=" + child +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", place=" + place +
                ", type='" + type + '\'' +
                '}';
    }
}
