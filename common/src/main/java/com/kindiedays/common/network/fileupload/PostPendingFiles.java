package com.kindiedays.common.network.fileupload;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.PendingFile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 20/05/2015.
 */
public class PostPendingFiles extends Webservice<PendingFile> {

    public static PendingFile post() throws CommonKindieException {
        PostPendingFiles postPendingFiles = new PostPendingFiles();
        return postPendingFiles.postObjects("pending-files", null);
    }

    @Override
    public PendingFile parseJsonFromServer(JSONObject json) throws JSONException {
        PendingFile pendingFile = new PendingFile();
        pendingFile.setId(json.getString("id"));
        pendingFile.setUploadUrl(json.getString("uploadUrl"));
        return pendingFile;
    }

}
