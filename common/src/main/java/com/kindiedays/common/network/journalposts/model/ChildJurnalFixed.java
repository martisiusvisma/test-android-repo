package com.kindiedays.common.network.journalposts.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChildJurnalFixed {

    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("events")
    @Expose
    private List<Event> events = new ArrayList<Event>();
    @SerializedName("menus")
    @Expose
    private List<Menu> menus = new ArrayList<Menu>();
    @SerializedName("naps")
    @Expose
    private List<Nap> naps = new ArrayList<Nap>();
    @SerializedName("signins")
    @Expose
    private List<Signin> signins = new ArrayList<Signin>();
    @SerializedName("journalPosts")
    @Expose
    private List<Object> journalPosts = new ArrayList<Object>();
    @SerializedName("galleryPictures")
    @Expose
    private List<GalleryPicture> galleryPictures = new ArrayList<GalleryPicture>();

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<Nap> getNaps() {
        return naps;
    }

    public void setNaps(List<Nap> naps) {
        this.naps = naps;
    }

    public List<Signin> getSignins() {
        return signins;
    }

    public void setSignins(List<Signin> signins) {
        this.signins = signins;
    }

    public List<Object> getJournalPosts() {
        return journalPosts;
    }

    public void setJournalPosts(List<Object> journalPosts) {
        this.journalPosts = journalPosts;
    }

    public List<GalleryPicture> getGalleryPictures() {
        return galleryPictures;
    }

    public void setGalleryPictures(List<GalleryPicture> galleryPictures) {
        this.galleryPictures = galleryPictures;
    }

    @Override
    public String toString() {
        return "ChildJurnalFixed{" +
                "start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", events=" + events +
                ", menus=" + menus +
                ", naps=" + naps +
                ", signins=" + signins +
                ", journalPosts=" + journalPosts +
                ", galleryPictures=" + galleryPictures +
                '}';
    }
}
