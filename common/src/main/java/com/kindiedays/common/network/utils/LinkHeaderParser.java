package com.kindiedays.common.network.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Adapted from https://raw.githubusercontent.com/resteasy/Resteasy/master/jaxrs/resteasy-jaxrs/src/main/java/org/jboss/resteasy/plugins/delegates/LinkHeaderDelegate.java
 *
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class LinkHeaderParser {
    private static final String TITLE = "title";
    private static final String UNABLE_MESSAGE = "Unable to parse Link header.  No end to parameter: ";
    private int curr;
    private String value;
    private LinkHeader header = new LinkHeader();

    public LinkHeader parse(String value) {
        this.value = value;
        curr = 0;
        header = new LinkHeader();
        String href = null;
        Map<String, List<String>> attributes = new HashMap<>();
        while (curr < value.length()) {

            char c = value.charAt(curr);
            if (c == '<') {
                if (href != null) {
                    throw new IllegalArgumentException("Unable to parse Link header. Too many links in declaration: " + value);
                }
                href = parseLink();
            } else if (c == ';' || c == ' ') {
                curr++;
                continue;
            } else if (c == ',') {
                populateLink(href, attributes);
                href = null;
                attributes = new HashMap<>();
                curr++;
            } else {
                parseAttribute(attributes);
            }
        }
        populateLink(href, attributes);
        return header;

    }

    protected void populateLink(String href, Map<String, List<String>> attributes) {
        List<String> rels = attributes.get("rel");
        List<String> revs = attributes.get("rev");
        String title = (attributes.get(TITLE) != null && !attributes.get(TITLE).isEmpty() ? attributes.get(TITLE).get(0) : null);
        if (title != null) {
            attributes.remove(TITLE);
        }
        String type = (attributes.get("type") != null && !attributes.get("type").isEmpty() ? attributes.get("type").get(0) : null);
        if (type != null) {
            attributes.remove("type");
        }

        Set<String> relationships = new HashSet<>();
        if (rels != null) {
            relationships.addAll(rels);
            attributes.remove("rel");
        }
        if (revs != null) {
            relationships.addAll(revs);
            attributes.remove("rev");
        }

        for (String relationship : relationships) {
            StringTokenizer tokenizer = new StringTokenizer(relationship);
            while (tokenizer.hasMoreTokens()) {
                String rel = tokenizer.nextToken();
                Link link = new Link(title, rel, href, type, attributes);
                header.getLinksByRelationship().put(rel, link);
                header.getLinksByTitle().put(title, link);
                header.getLinks().add(link);
            }

        }
    }

    public String parseLink() {
        int end = value.indexOf('>', curr);
        if (end == -1) {
            throw new IllegalArgumentException(UNABLE_MESSAGE + value);
        }
        String href = value.substring(curr + 1, end);
        curr = end + 1;
        return href;
    }

    public void parseAttribute(Map<String, List<String>> attributes) {
        int end = value.indexOf('=', curr);
        if (end == -1 || end + 1 >= value.length()) {
            throw new IllegalArgumentException(UNABLE_MESSAGE + value);
        }
        String name = value.substring(curr, end);
        name = name.trim();
        curr = end + 1;
        String val = null;
        if (curr >= value.length()) {
            val = "";
        } else {

            if (value.charAt(curr) == '"') {
                if (curr + 1 >= value.length()) {
                    throw new IllegalArgumentException(UNABLE_MESSAGE + value);
                }
                curr++;
                end = value.indexOf('"', curr);
                if (end == -1) {
                    throw new IllegalArgumentException(UNABLE_MESSAGE + value);
                }
                val = value.substring(curr, end);
                curr = end + 1;
            } else {
                StringBuilder buf = new StringBuilder();
                while (curr < value.length()) {
                    char c = value.charAt(curr);
                    if (c == ',' || c == ';') {
                        break;
                    }
                    buf.append(value.charAt(curr));
                    curr++;
                }
                val = buf.toString();
            }
        }
        if (attributes.get(name) == null) {
            attributes.put(name, new ArrayList<String>());
        }
        attributes.get(name).add(val);
    }

}