package com.kindiedays.common.network.session;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Session;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 25/01/2016.
 */
public class GetSession extends Webservice<Session> {

    private static final String CARER = "CARER";
    private static final String TEACHER = "TEACHER";

    public static Session getSession() throws CommonKindieException {
        Log.d(GetSession.class.getName(), "Getting session");
        GetSession item = new GetSession();
        initEndPointsIfNeeded();
        return item.getObjects("sessions/current", authEndpoint);
    }

    @Override
    public Session parseJsonFromServer(JSONObject json) throws JSONException {
        Session session = new Session();
        session.setEmail(json.getString("email"));
        session.setName(json.getString("name"));
        session.setType(json.getString("type"));
        JSONObject links = json.getJSONObject("links");
        session.setKindergartenLink(links.getString("kindergarten"));
        if (CARER.equals(session.getType())) {
            session.setSelfLink(links.getString("carer"));
        }
        if (TEACHER.equals(session.getType())) {
            session.setSelfLink(links.getString("teacher"));
        }
        return session;
    }
}
