package com.kindiedays.common.network.pictures;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.album.PictureParser;
import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by pleonard on 20/05/2015.
 */
public class PostPicture extends Webservice<GalleryPicture> {


    public static GalleryPicture post(String id, String caption, List<ChildTag> tags) throws CommonKindieException {
        try {
            PostPicture instance = new PostPicture();
            JSONObject json = new JSONObject();
            json.put("format", "JPEG");
            json.put("pendingFileId", id);
            json.put("caption", caption);
            JSONArray jsonArray = new JSONArray();
            for (ChildTag tag : tags) {
                JSONObject jsonTag = new JSONObject();
                jsonTag.put("child", tag.getChildId());
                if (tag.getWidth() > 0 && tag.getHeight() > 0) {
                    jsonTag.put("x", tag.getX());
                    jsonTag.put("y", tag.getY());
                    jsonTag.put("width", tag.getWidth());
                    jsonTag.put("height", tag.getHeight());
                }
                jsonArray.put(jsonTag);
            }
            json.put("children", jsonArray);
            Log.d("myLogs", "json picture = " + json);
            return instance.postObjects("pictures", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public GalleryPicture parseJsonFromServer(JSONObject json) throws JSONException {
        return PictureParser.parsePicture(json);
    }
}
