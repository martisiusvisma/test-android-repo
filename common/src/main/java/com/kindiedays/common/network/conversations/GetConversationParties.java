package com.kindiedays.common.network.conversations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.ConversationParty;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 25/11/2015.
 */
public class GetConversationParties extends Webservice<List<ConversationParty>> {

    public static List<ConversationParty> getConversationParties() throws CommonKindieException {
        GetConversationParties instance = new GetConversationParties();
        return instance.getObjects("conversation-parties");
    }

    @Override
    public List<ConversationParty> parseJsonFromServer(JSONObject json) throws JSONException {
        List<ConversationParty> parties = new ArrayList<>();
        JSONArray jsonParties = json.getJSONArray("conversationParties");
        for (int i = 0; i < jsonParties.length(); i++) {
            JSONObject jsonParty = jsonParties.getJSONObject(i);
            ConversationParty conversationParty = ConversationParser.parseConversationParty(jsonParty);
            parties.add(conversationParty);
        }
        return parties;
    }
}
