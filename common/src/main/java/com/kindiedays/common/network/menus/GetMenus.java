package com.kindiedays.common.network.menus;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Menu;

import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 11/06/2015.
 */
public class GetMenus extends Webservice<List<Menu>> {
    private static final String MENUS_FIELD = "menus";

    public static List<Menu> getMenus() throws CommonKindieException {
        GetMenus getMeals = new GetMenus();
        return getMeals.getObjects(MENUS_FIELD);
    }

    public static List<Menu> getMenus(LocalDate start, LocalDate end) throws CommonKindieException {
        GetMenus getMeals = new GetMenus();
        return getMeals.getObjects(MENUS_FIELD, apiEndpoint, "start", start.toString(), "end", end.toString());
    }

    @Override
    public List<Menu> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Menu> menus = new ArrayList<>();
        JSONArray array = json.getJSONArray(MENUS_FIELD);
        for (int i = 0; i < array.length(); i++) {
            JSONObject jsonMenu = array.getJSONObject(i);
            menus.add(MenuJsonParser.parseMenu(jsonMenu));
        }
        return menus;
    }
}
