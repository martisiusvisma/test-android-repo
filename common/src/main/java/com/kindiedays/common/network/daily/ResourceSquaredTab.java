package com.kindiedays.common.network.daily;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.common.R;

/**
 * Created by Giuseppe Franco - Starcut on 01/04/16.
 */
public class ResourceSquaredTab extends LinearLayout {

    private TextView tvResourceName;

    public ResourceSquaredTab(Context context) {
        super(context);
        initViews();
    }

    public ResourceSquaredTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public ResourceSquaredTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_resource_squared, this);
        tvResourceName = (TextView) v.findViewById(R.id.current);
    }

    public void setResourceName(String current){
        tvResourceName.setText(current);
    }
}
