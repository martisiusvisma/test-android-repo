package com.kindiedays.common.network.daily.parser;

import android.util.Log;

import com.kindiedays.common.pojo.GeneralDailyRes;
import com.kindiedays.common.pojo.Generic;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 21/07/2015.
 */
public class GeneralParser {
    private static final String MOOD = "MOOD";
    private static final String MED = "MED";
    private static final String NAP = "NAP";
    private static final String MEAL = "MEAL";
    private static final String BATH = "BATH";
    private static final String ACTIVITY = "ACTIVITY";
    private static final String NOTE = "NOTE";

    private GeneralParser() {
        //no instance
    }

    public static GeneralDailyRes parserItem(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter timeFormatterLong = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.000Z");
        GeneralDailyRes dailyRes = new GeneralDailyRes();
        dailyRes.setChild(json.getInt("child"));
        dailyRes.setDate(dataFormatter.parseDateTime(json.getString("date")));
        List<Generic> genericArray = new ArrayList<>();
        JSONArray moodJsonArray = json.getJSONArray("moods");
        for (int i = 0; i < moodJsonArray.length(); i++) {
            JSONObject item = moodJsonArray.getJSONObject(i);
            /* GENERIC MOOD OBJECT */
            String time = item.getString("date").concat("T").concat(item.getString("time")).concat("Z");
            Generic generic = new Generic(DateTime.parse(time), MOOD, item.getString("temper"), item.optString("notes"));
            generic.setMood(MoodParser.parserMoodItem(item));
            genericArray.add(generic);
            /* ------------------- */
        }
        JSONArray medJsonArray = json.getJSONArray("medications");
        for (int i = 0; i < medJsonArray.length(); i++) {
            JSONObject item = medJsonArray.getJSONObject(i);
            /* GENERIC MED OBJECT */
            String time = item.getString("date").concat("T").concat(item.getString("time")).concat("Z");
            Generic generic = new Generic(DateTime.parse(time), MED, item.getString("drugName"), item.getString("dosage"));
            generic.setMed(MedParser.parserMedItem(item));
            genericArray.add(generic);
            /* ------------------- */
        }
        JSONArray napJsonArray = json.getJSONArray("naps");
        for (int i = 0; i < napJsonArray.length(); i++) {
            JSONObject item = napJsonArray.getJSONObject(i);
            /* GENERIC NAP OBJECT */
            Generic generic = new Generic(DateTime.parse(item.getString("start")),
                    NAP, (DateTime.parse(item.getString("start"))).toString("HH:mm") + " - " + (DateTime.parse(item.getString("end"))).toString("HH:mm"), item.optString("notes"));
            generic.setNap(NapParser.parserNapItem(item));
            genericArray.add(generic);
            /* ------------------- */
        }
        JSONArray mealJsonArray = json.getJSONArray("meals");
        for (int i = 0; i < mealJsonArray.length(); i++) {
            JSONObject item = mealJsonArray.getJSONObject(i);
            /* GENERIC MEAL OBJECT */
            Generic generic = new Generic(timeFormatterLong.parseDateTime(item.getString("created")), MEAL, item.optString("reaction"), item.optString("drinkQuantity"));
            generic.setMeal(MealParser.parserMealItem(item));
            genericArray.add(generic);
            /* ------------------- */
        }
        JSONArray bathJsonArray = json.getJSONArray("bathroomLogEntries");
        for (int i = 0; i < bathJsonArray.length(); i++) {
            Log.d(GeneralParser.class.getSimpleName(), "parserItem: " + i);
            JSONObject item = bathJsonArray.getJSONObject(i);
            /* GENERIC BATH OBJECT */
            String time = item.getString("date").concat("T").concat(item.getString("time")).concat("Z");
            Generic generic = new Generic(DateTime.parse(time), BATH, item.getJSONArray("equipment").get(0).toString(), item.optString("type"));
            generic.setBath(BathParser.parserBathItem(item));
            genericArray.add(generic);
            /* ------------------- */
        }
        JSONArray activityJsonArray = json.getJSONArray("activities");
        for (int i = 0; i < activityJsonArray.length(); i++) {
            JSONObject item = activityJsonArray.getJSONObject(i);
            /* GENERIC ACTIVITY OBJECT */
            Generic generic = new Generic(timeFormatterLong.parseDateTime(item.getString("modified")), ACTIVITY, item.getString("categoryName"), item.getString("text"));
            generic.setActivity(ActivityParser.parserActivityItem(item));
            genericArray.add(generic);
            /* ------------------- */
        }
        /* GENERIC NOTE OBJECT */
        if (json.has("note") && !json.isNull("note")) {
            JSONObject noteJsonObj = json.getJSONObject("note");
            Generic generic = new Generic(timeFormatterLong.parseDateTime(noteJsonObj.getString("modified")), NOTE, noteJsonObj.getString("text"), "");
            generic.setNote(NoteDRParser.parseNote(noteJsonObj));
            genericArray.add(generic);
        }
        /* ------------------- */
        dailyRes.setGeneric(genericArray);
        return dailyRes;
    }
}