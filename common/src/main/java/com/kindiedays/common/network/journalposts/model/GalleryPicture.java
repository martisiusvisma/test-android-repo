package com.kindiedays.common.network.journalposts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GalleryPicture {

    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("children")
    @Expose
    private List<Child> children = new ArrayList<Child>();
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("links")
    @Expose
    private Links links;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "GalleryPicture{" +
                "hash='" + hash + '\'' +
                ", format='" + format + '\'' +
                ", created='" + created + '\'' +
                ", url='" + url + '\'' +
                ", children=" + children +
                ", caption='" + caption + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", links=" + links +
                '}';
    }
}
