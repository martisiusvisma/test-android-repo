package com.kindiedays.common.network.teachers;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Teacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Giuseppe Franco - Starcut on 25/05/16.
 */
public class GetTeacher extends Webservice<Teacher> {

    public static Teacher getTeacherByLink(String link) throws CommonKindieException {
        GetTeacher item = new GetTeacher();
        return item.getObjectsFromUrl(link);
    }

    @Override
    public Teacher parseJsonFromServer(JSONObject json) throws JSONException {
        Teacher teacher = new Teacher();
        teacher.setName(json.getString("name"));
        teacher.setId(json.getInt("id"));
        teacher.setUnreadMessages(json.getInt("unreadMessages"));
        JSONObject jsonPicture = json.getJSONObject("profilePicture");
        JSONArray jsonThumbnails = jsonPicture.getJSONArray("thumbnails");
        for (int j = jsonThumbnails.length() - 1; j >= 0; j--) {
            JSONObject thumbnail = jsonThumbnails.getJSONObject(j);
            if (thumbnail.getLong("width") == 256) {
                teacher.setProfilePictureUrl(thumbnail.getString("url"));
                break;
            }
        }
        if (json.has("groups")) {
            JSONArray groups = json.getJSONArray("groups");
            for (int i = 0; i < groups.length(); i++) {
                teacher.getGroups().add(groups.getLong(i));
            }
        }
        return teacher;
    }
}
