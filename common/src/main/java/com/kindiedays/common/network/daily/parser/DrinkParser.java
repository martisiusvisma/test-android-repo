package com.kindiedays.common.network.daily.parser;

import com.kindiedays.common.pojo.DrinkRes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class DrinkParser {
    private DrinkParser() {
        //no instance
    }

    public static DrinkRes parserDrinkItem(JSONObject json) throws JSONException {
        DrinkRes drinkItem = new DrinkRes(json.getInt("id"), json.getString("name"));
        drinkItem.setEnabled(json.getBoolean("enabled"));
        return drinkItem;
    }
}