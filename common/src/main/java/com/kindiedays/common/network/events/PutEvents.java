package com.kindiedays.common.network.events;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 10/06/2015.
 */
public class PutEvents extends Webservice<Void> {
    private static final String DATE_PATTERN = "YYYY-MM-dd";

    public static Void updateEvent(Event event) throws CommonKindieException {
        try {
            PutEvents putEvents = new PutEvents();
            JSONObject jsonEvent = new JSONObject();
            jsonEvent.put("date", event.getDate().toString(DATE_PATTERN));
            jsonEvent.put("start", event.getStart().toString("HH:mm:ss"));
            jsonEvent.put("end", event.getEnd().toString("HH:mm:ss"));
            jsonEvent.put("name", event.getName());
            jsonEvent.put("notes", event.getNotes());
            jsonEvent.put("location", event.getLocation());
            jsonEvent.put("state", event.getState());
            JSONArray jsonInvitations = new JSONArray();
            for (EventInvitation invitation : event.getInvitations()) {
                JSONObject jsonInvitation = new JSONObject();
                jsonInvitation.put("child", invitation.getChildId());
                jsonInvitation.put("state", invitation.getState().toString());
                jsonInvitations.put(jsonInvitation);
            }
            jsonEvent.put("invitations", jsonInvitations);
            if (event.getInterval() != null && !event.getInterval().equals(Event.NEVER)) {
                JSONObject jsonRecurrence = new JSONObject();
                jsonRecurrence.put("start", event.getDate().toString(DATE_PATTERN));
                if (event.getEndRecurrence() != null) {
                    jsonRecurrence.put("end", event.getEndRecurrence().toString(DATE_PATTERN));
                } else {
                    jsonRecurrence.put("end", JSONObject.NULL);
                }
                jsonRecurrence.put("interval", event.getInterval());
                jsonEvent.put("recurrence", jsonRecurrence);
            } else {
                jsonEvent.put("recurrence", JSONObject.NULL);
            }
            return putEvents.putObjects("events/" + event.getId(), jsonEvent);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        Log.d("JSON", json.toString());
        return null;
    }
}
