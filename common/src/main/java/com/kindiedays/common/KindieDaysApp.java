package com.kindiedays.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.kindiedays.common.business.SessionBusiness;
import com.kindiedays.common.pushnotifications.BroadcastReceiver;
import com.kindiedays.common.ui.MainActivity;
import com.starcut.starflight.StarFlightClient;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by pleonard on 20/04/2015.
 */
public abstract class KindieDaysApp extends MultiDexApplication implements StarFlightClient.RegistrationCallback {

    public static final String SETTINGS = "settings";

    protected static KindieDaysApp app;
    protected Class loginActivityClass;
    protected StarFlightClient client;
    private boolean isStaging;

    public static KindieDaysApp getApp() {
        return app;
    }

    @Override
    @SuppressWarnings("squid:S2696")
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        app = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!BroadcastReceiver.doesNotificationChannelExist(this, getChanelId())) {
                BroadcastReceiver.createNotificationChannel(this, getChanelId());
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onStarflightRegistrationSuccess(String uuid) {
        //Patch session with uuid
        SessionBusiness.PatchSessionStarflightUuidTask task = new SessionBusiness.PatchSessionStarflightUuidTask(uuid);
        task.execute();
    }

    @Override
    public void onStarflightRegistrationFailure() {
        Log.e(MainActivity.class.getName(), "Failed to register to Starflight");
    }

    @Override
    public void onStarflightAlreadyRegistered(String uuid) {
        SessionBusiness.PatchSessionStarflightUuidTask task = new SessionBusiness.PatchSessionStarflightUuidTask(uuid);
        task.execute();
    }

    public void subscribePushNotifications(Activity activity) {
        if (client == null) {
            initStarflightClient();
        }
        client.register(activity);
        SharedPreferences.Editor editor = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE).edit();
        editor.putBoolean(MainActivity.NOTIFICATIONS, true).commit();

    }

    public void initStarflightClient() {
        if (isStaging) {
            client = new StarFlightClient(KindieDaysApp.getApp().getString(R.string.starflight_sender_id), KindieDaysApp.getApp().getString(R.string.starflight_staging_app_id), KindieDaysApp.getApp().getString(R.string.starflight_staging_client_secret));
        } else {
            client = new StarFlightClient(KindieDaysApp.getApp().getString(R.string.starflight_sender_id), KindieDaysApp.getApp().getString(R.string.starflight_prod_app_id), KindieDaysApp.getApp().getString(R.string.starflight_prod_client_secret));
        }
        client.setCallback(this);
    }

    public void unsubscribePushNotifications(Activity activity) {
        if (client == null) {
            initStarflightClient();
        }
        client.unregister(activity, null);
        SharedPreferences.Editor editor = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE).edit();
        editor.putBoolean(MainActivity.NOTIFICATIONS, false).commit();

    }

    public void setStaging(boolean isStaging) {
        this.isStaging = isStaging;
    }

    public Class getLoginActivityClass() {
        return loginActivityClass;
    }

    public SharedPreferences getCurrentSharedPreferences(Activity activity) {
        return getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE);
    }

    public abstract String getChanelId();
}
