package com.kindiedays.common.utils.exceptions;


import android.content.Context;

public class CastListenerException extends ClassCastException {
    private static final String MESSAGE = " must implement listener";

    public CastListenerException(String className) {
        super(className + MESSAGE);
    }

    public CastListenerException(Context context) {
        super(context + MESSAGE);
    }
}
