package com.kindiedays.common.utils;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.view.View;

public class BlurUtils {
    private static final float BITMAP_SCALE = 0.3f;
    private static final float BLUR_RADIUS = 25f;
    private static final float BLUR_LIGHT_RADIUS = 12f;

    private BlurUtils() {
        //no instance
    }

    public static Bitmap loadBitmapFromView(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas bitmapHolder = new Canvas(bitmap);
        view.draw(bitmapHolder);
        return bitmap;
    }

    public static Bitmap loadBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas bitmapHolder = new Canvas(bitmap);
        view.draw(bitmapHolder);
        return bitmap;
    }

    public static float getRatio(int pic, int blur) {
        return pic / (float) blur;
    }

    public static int getIncreasedDimen(int defDimen, float ratio, int cap) {
        return getIncreasedDimen(defDimen, ratio, cap, 0);
    }

    public static int getIncreasedDimen(int defDimen, float ratio, int cap, int fault) {
        int result = Math.min(Math.round(defDimen * ratio), cap) + fault;
        return result >= 0 ? result : 0;
    }

    public static Bitmap getBlurredBitmap(final Context context, Bitmap srcBitmap, Rect blurredRect) {
        int width = blurredRect.width();
        int height = blurredRect.height();
        int scaledWidth = Math.round(width * BITMAP_SCALE);
        int scaledHeight = Math.round(height * BITMAP_SCALE);

        //check on valid dimens
        if (width <= 0 || height <= 0) {
            return srcBitmap;
        }

        if (scaledHeight <= 0 || scaledWidth <= 0) {
            return srcBitmap;
        }

        //create erased bitmap, to scale it down to improve blurring degree
        Bitmap outputBitmap = Bitmap.createScaledBitmap(Bitmap.createBitmap(srcBitmap, blurredRect.left,
                blurredRect.top, width, height), scaledWidth, scaledHeight, false);
        //create bitmap for blur
        Bitmap blurredBitmap = Bitmap.createBitmap(outputBitmap);

        //blur
        RenderScript rs = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(rs, outputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, blurredBitmap);
        ScriptIntrinsicBlur scriptBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        scriptBlur.setRadius(BLUR_RADIUS);
        scriptBlur.setInput(tmpIn);
        scriptBlur.forEach(tmpOut);
        tmpOut.copyTo(blurredBitmap);

        //append bitmaps to single one
        Bitmap resultBitmap = Bitmap.createBitmap(srcBitmap.getWidth(), srcBitmap.getHeight(),
                srcBitmap.getConfig());
        Canvas resultCanvas = new Canvas(resultBitmap);
        resultCanvas.drawBitmap(srcBitmap, 0f, 0f, null);
        Bitmap unscaledBitmap = Bitmap.createScaledBitmap(blurredBitmap, width, height, false);
        resultCanvas.drawBitmap(unscaledBitmap, blurredRect.left, blurredRect.top, null);

        rs.destroy();
        return resultBitmap;
    }

    public static int cropDimensions(int value, int cap) {
        return value > cap ? cap : value;
    }

    public static Bitmap getLightBlurredBitmap(final Context context, Bitmap srcBitmap, Rect blurredRect) {
        int width = blurredRect.width();
        int height = blurredRect.height();

        //check on valid dimens
        if (width <= 0 || height <= 0) {
            return srcBitmap;
        }

        Bitmap outputBitmap = Bitmap.createBitmap(srcBitmap, blurredRect.left,
                blurredRect.top, width, height);
        //create bitmap for blur
        Bitmap blurredBitmap = Bitmap.createBitmap(outputBitmap);

        RenderScript rs = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(rs, outputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, blurredBitmap);
        ScriptIntrinsicBlur scriptBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        scriptBlur.setRadius(BLUR_LIGHT_RADIUS);
        scriptBlur.setInput(tmpIn);
        scriptBlur.forEach(tmpOut);
        tmpOut.copyTo(blurredBitmap);

        Bitmap resultBitmap = Bitmap.createBitmap(srcBitmap.getWidth(), srcBitmap.getHeight(),
                srcBitmap.getConfig());
        Canvas resultCanvas = new Canvas(resultBitmap);
        resultCanvas.drawBitmap(srcBitmap, 0f, 0f, null);
        resultCanvas.drawBitmap(blurredBitmap, blurredRect.left, blurredRect.top, null);

        rs.destroy();
        return resultBitmap;
    }
}
