package com.kindiedays.common.utils;


import android.webkit.MimeTypeMap;

import static com.kindiedays.common.ui.messages.MessageAdapter.TYPE_OTHER_MEDIA;
import static com.kindiedays.common.ui.messages.MessageAdapter.TYPE_PHOTO;
import static com.kindiedays.common.ui.messages.MessageAdapter.TYPE_VIDEO;
import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

public class FormatUtils {
    private static final String PNG_FORMAT = "png";
    private static final String JPEG_FORMAT = "jpeg";
    private static final String JPG_FORMAT = "jpg";
    private static final String MP4_FORMAT = "mp4";
    public static final String MKV_FORMAT = "mkv";
    public static final String AVI_FORMAT = "avi";
    public static final String MOV_FORMAT = "mov";

    private FormatUtils() {
        //no instance
    }

    public static int getFormatType(String format) {
        if (format.equalsIgnoreCase(PNG_FORMAT) || format.equalsIgnoreCase(JPEG_FORMAT)
                || format.equalsIgnoreCase(JPG_FORMAT)) {
            return TYPE_PHOTO;
        } else if (format.equalsIgnoreCase(MP4_FORMAT) || format.equalsIgnoreCase(MKV_FORMAT)
                || format.equalsIgnoreCase(AVI_FORMAT) || format.equalsIgnoreCase(MOV_FORMAT)) {
            return TYPE_VIDEO;
        } else {
            return TYPE_OTHER_MEDIA;
        }
    }

    public static String getServerFormat(String format) {
        if (format.equalsIgnoreCase(JPG_FORMAT)) {
            return JPEG_FORMAT;
        } else {
            return format;
        }
    }

    public static String getMimeType(String format) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(format);
        if (extension != null) {
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            return mimeTypeMap.getMimeTypeFromExtension(format);
        }
        return null;
    }

    public static String getFormat(String filename) {
        int i = filename.lastIndexOf('.');
        return i > 0 ? filename.substring(i + 1) : STRING_EMPTY;
    }

    public static String getFileName(String path) {
        int i = path.lastIndexOf('/');
        return i > 0 ? path.substring(i + 1) : STRING_EMPTY;
    }
}
