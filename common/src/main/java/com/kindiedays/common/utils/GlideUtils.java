package com.kindiedays.common.utils;


import com.kindiedays.common.KindieDaysApp;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by pleonard on 11/08/2015.
 */
public class GlideUtils {
    public static final CropCircleTransformation roundTransformation = new CropCircleTransformation(KindieDaysApp.getApp());

    private GlideUtils() {
        //no instance
    }
}
