package com.kindiedays.common.utils.constants;


public class CommonConstants {
    public static final String STRING_EMPTY = "";
    public static final String STRING_COMMA = ", ";
    public static final String STRING_SPACE = " ";
    public static final String STRING_NOT_EMPTY = "string";

    public static final String EXTRA_GALLERY = "gallery_data";

    public static final int PICK_IMAGE_MULTIPLE = 11;
    public static final int PICK_PDF_SINGLE = 12;
    public static final String PDF_MIME_TYPE = "application/pdf";

    public static final int MAX_UPLOAD_SIZE_KB = 100 * 1024;
    public static final int MB_SIZE = 1024;

    public static final String TEACHER_APP_PACKAGE = "com.kindiedays.teacherapp";
    public static final String CARER_APP_PACKAGE = "com.kindiedays.carerapp";

    public static final String SAMSUNG_MODEL = "Samsung";
    public static final String SAMSUNG_ACTION_GET_CONTENT = "com.sec.android.app.myfiles.PICK_DATA";
    public static final String SAMSUNG_CONTENT_TYPE = "CONTENT_TYPE";

    private CommonConstants() {
        //no instance
    }
}
