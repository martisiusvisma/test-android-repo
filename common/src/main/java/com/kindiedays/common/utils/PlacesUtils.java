package com.kindiedays.common.utils;


import com.kindiedays.common.pojo.Place;

import java.util.List;

public class PlacesUtils {
    private PlacesUtils() {
        //no instance
    }

    public static Place findPlaceById(long id, List<Place> places) {
        if (places != null) {
            for (Place place : places) {
                if (place.getId() == id) {
                    return place;
                }
            }
        }

        return new Place("Unknown");
    }
}
