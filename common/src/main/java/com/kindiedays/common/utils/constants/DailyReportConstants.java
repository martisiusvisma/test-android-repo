package com.kindiedays.common.utils.constants;


public class DailyReportConstants {
    public static final String MOOD = "MOOD";
    public static final String NOTE = "NOTE";
    public static final String MED = "MED";
    public static final String MEAL = "MEAL";
    public static final String NAP = "NAP";
    public static final String ACTIVITY = "ACTIVITY";
    public static final String BATH = "BATH";

    public static final String TYPE = "Type";

    public static final String SAD = "SAD";
    public static final String SMILING = "SMILING";
    public static final String SERIOUS = "SERIOUS";

    public static final String ATE_WELL = "ATE_WELL";
    public static final String ATE_SOME = "ATE_SOME";
    public static final String NOT_ATE = "NOT_ATE";

    public static final String DIAPER_TAG = "DIAPER";
    public static final String POTTY_TAG = "POTTY";
    public static final String TOILET_TAG = "TOILET";

    public static final String DRY = "DRY";
    public static final String WET = "WET";
    public static final String BOWEL_MOVEMENT = "BOWEL_MOVEMENT";
    public static final String WET_AND_BOWEL_MOVEMENT = "WET_AND_BOWEL_MOVEMENT";

    public static final String LOOSE_TAG = "LOOSE";
    public static final String NORMAL_TAG = "NORMAL";
    public static final String SOLID_TAG = "SOLID";

    public static final String PLATE_SOME_TAG = "PLATE_SOME";
    public static final String DRINK_SOME_TAG = "DRINK_SOME";

    private DailyReportConstants() {
        //no instance
    }
}
