package com.kindiedays.common.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.kindiedays.common.R;

public class AppUtils {

    public static void copyToClipboard(Context context, String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("url", text);
        clipboard.setPrimaryClip(clip);
        showToast(context, R.string.copied_to_clipboard_str);
    }

    public static void showToast(Context context, int msgId) {
        String msg = context.getString(msgId);
        showToast(context, msg);
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
