package com.kindiedays.common.utils;

import org.json.JSONArray;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by pleonard on 13/01/2016.
 */
public class ArrayUtils {

    private ArrayUtils() {
        //no instance
    }

    public static Integer[] mergeArrays(Integer[] array1, Integer[] array2){
        int aLen = array1.length;
        int bLen = array2.length;
        Integer[] c= new Integer[aLen+bLen];
        System.arraycopy(array1, 0, c, 0, aLen);
        System.arraycopy(array2, 0, c, aLen, bLen);
        return c;
    }

    public static Boolean[] mergeArrays(Boolean[] array1, Boolean[] array2){
        int aLen = array1.length;
        int bLen = array2.length;
        Boolean[] c= new Boolean[aLen+bLen];
        System.arraycopy(array1, 0, c, 0, aLen);
        System.arraycopy(array2, 0, c, aLen, bLen);
        return c;
    }

    public static long[] longCollectionToArray(Collection<Long> values){
        Set<Long> childrenSet = new HashSet<>();
        childrenSet.addAll(values);
        long[]array = new long[childrenSet.size()];
        int i = 0;
        for(Long value : childrenSet){
            array[i] = value;
            i++;
        }
        return array;
    }

    public static JSONArray longArrayToJson(long[] values){
        JSONArray jsonArray = new JSONArray();
        for(int i = 0 ; i < values.length ;i++){
            jsonArray.put(values[i]);
        }
        return jsonArray;
    }
}
