package com.kindiedays.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import com.bumptech.glide.load.resource.bitmap.ImageHeaderParser;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.jpeg.JpegDirectory;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.svg.SVG;
import com.kindiedays.common.svg.SVGParseException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;

/**
 * Created by pleonard on 15/04/2015.
 */
@SuppressWarnings("squid:S1226")
public class PictureUtils {
    private static final String TAG = "CameraExif";

    private PictureUtils() {
        //no instance
    }

    public static Point getScreenDimensions(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    private static Rect translateSelectionCoordinates(int exifOrientation, Rect selection, int width, int height) {
        //Height and width are the dimensions of the picture regardless of the exif orientation, so possibly different from what the user sees
        Rect translatedSelection = null;
        switch (exifOrientation) {
        case ExifInterface.ORIENTATION_ROTATE_90:
            translatedSelection = new Rect(selection.top, height - selection.right, selection.bottom, height - selection.left);
            break;
        case ExifInterface.ORIENTATION_ROTATE_180:
            translatedSelection = new Rect(width - selection.left, height - selection.top, width - selection.right, height - selection.bottom);
            break;
        case ExifInterface.ORIENTATION_ROTATE_270:
            translatedSelection = new Rect(width - selection.bottom, selection.left, width - selection.top, selection.right);
            break;
            case ExifInterface.ORIENTATION_NORMAL:
        default:
            translatedSelection = selection;
            break;
        }
        return translatedSelection;
    }

    @SuppressWarnings({"squid:S1751", "squid:S2093"})
    public static File cropRotateAndCreate(Context context, String picturePath, File outputFile, Rect region) {
        InputStream is = null;
        try {
            int exifOrientation = ExifInterface.ORIENTATION_NORMAL;
            int logicalWidth = 0;
            int logicalHeight = 0;
            is = context.getContentResolver().openInputStream(Uri.parse(picturePath));
            final Metadata metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(is));
            final Collection<ExifIFD0Directory> exifIFD0Directories = metadata.getDirectoriesOfType(ExifIFD0Directory.class);
            if (exifIFD0Directories != null && !exifIFD0Directories.isEmpty()) {
                int count = 1;
                for (ExifIFD0Directory exifIFD0Directory : exifIFD0Directories) {
                    count++;
                    try {
                        exifOrientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
                    }catch (MetadataException e){}
                    break;
                }
            }
            final Collection<JpegDirectory> jpegDirectories = metadata.getDirectoriesOfType(JpegDirectory.class);
            if (jpegDirectories != null && !jpegDirectories.isEmpty()) {
                for (JpegDirectory jpegDirectory : jpegDirectories) {
                    logicalWidth = jpegDirectory.getImageWidth();
                    logicalHeight = jpegDirectory.getImageHeight();
                }
            }

            Rect translatedCoordinates = translateSelectionCoordinates(exifOrientation, region, logicalWidth, logicalHeight);
            is.close();

            is = context.getContentResolver().openInputStream(Uri.parse(picturePath));

            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(is, false);
            Bitmap bitmap = decoder.decodeRegion(translatedCoordinates, null);

            rotateAndCreate(exifOrientation, outputFile, bitmap);
        } catch (Exception e) {
            Log.e(PictureUtils.class.getName(), "Unable to generate thumbnail", e);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (Exception e) {
                Log.e(TAG, "cropRotateAndCreate: ", e);
            }
        }
        return null;
    }

    public static int getExifRotation(InputStream is) {
        int orientation = ExifInterface.ORIENTATION_NORMAL;
        try {
            orientation = new ImageHeaderParser(is).getOrientation();
        } catch (Exception e) {
            Log.d(TAG, "Unable to read exif: " + e);
        }

        return orientation;
    }

    public static int getAngleForExifRotation(int exifRotation) {
        switch (exifRotation) {
        case ExifInterface.ORIENTATION_ROTATE_90:
            return 90;
        case ExifInterface.ORIENTATION_ROTATE_180:
            return 180;
        case ExifInterface.ORIENTATION_ROTATE_270:
            return 270;
        default:
            return 0;
        }
    }

    public static File rotateAndCreate(InputStream exifPictureStream, File outputFile, Bitmap bmp) {
        int exifRotation = getExifRotation(exifPictureStream);
        return rotateAndCreate(exifRotation, outputFile, bmp);
    }

    @SuppressWarnings("squid:S2093")
    public static File rotateAndCreate(int exifRotation, File outputFile, Bitmap bmp) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(outputFile);
            Matrix matrix = new Matrix();

            matrix.setRotate(getAngleForExifRotation(exifRotation));
            Bitmap bmpRotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, false);
            bmpRotated.compress(Bitmap.CompressFormat.JPEG, 40, fos);
            bmp.recycle();
        } catch (Exception e) {
            Log.d(TAG, "Unable to rotate picture: ", e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    Log.e(TAG, "rotateAndCreate: ", e);
                }
            }
            Log.i(TAG, "Finishing saving picture: " + new Date().getTime());

        }
        return outputFile;
    }


    /**
     * @param selectedImage: content uri
     * @return BitmapFactory.options with height, width and MIME type
     */
    public static BitmapFactory.Options readImageDimensions(Uri selectedImage, boolean useExifData) throws IOException {
        InputStream pictureStream = KindieDaysApp.getApp().getContentResolver().openInputStream(selectedImage);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(pictureStream, null, options);
        pictureStream.close();
        int orientation = getOrientation(selectedImage);
        if (useExifData && (orientation == ExifInterface.ORIENTATION_ROTATE_90 || orientation == ExifInterface.ORIENTATION_ROTATE_270)) {
            //switch height and width
            int tmp = options.outWidth;
            options.outWidth = options.outHeight;
            options.outHeight = tmp;
        }
        return options;
    }

    public static int getOrientation(Uri photoUri) {
    /* it's on the external media. */
        Cursor cursor = KindieDaysApp.getApp().getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static Bitmap loadScaledDownBitmap(Uri selectedImage, int targetWidth, int targetHeight, boolean exifRotate) throws IOException {
        BitmapFactory.Options options = readImageDimensions(selectedImage, exifRotate);
        return loadScaledDownBitmap(selectedImage, options.outWidth, options.outHeight, targetWidth, targetHeight, exifRotate);
    }

    public static Bitmap loadScaledDownBitmap(Uri selectedImage, int originalWidth,
                                              int originalHeight, int targetWidth, int targetHeight,
                                              boolean exifRotate) throws IOException {
        InputStream pictureStream = KindieDaysApp.getApp().getContentResolver().openInputStream(selectedImage);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        options.inSampleSize = calculateInSampleSize(originalWidth, originalHeight, targetWidth, targetHeight);
        Bitmap bitmap = BitmapFactory.decodeStream(pictureStream, null, options);
        pictureStream.close();

        if (exifRotate) {
            Matrix matrix = new Matrix();
            int orientation = getOrientation(selectedImage);
            matrix.postRotate(orientation);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
        return bitmap;
    }

    // Returns the degrees in clockwise. Values are 0, 90, 180, or 270.
    @SuppressWarnings("squid:S135")
    public static int getOrientation(byte[] jpeg) {
        if (jpeg == null) {
            return 0;
        }

        int offset = 0;
        int length = 0;

        // ISO/IEC 10918-1:1993(E)
        while (offset + 3 < jpeg.length && (jpeg[offset++] & 0xFF) == 0xFF) {
            int marker = jpeg[offset] & 0xFF;

            // Check if the marker is a padding.
            if (marker == 0xFF) {
                continue;
            }
            offset++;

            // Check if the marker is SOI or TEM.
            if (marker == 0xD8 || marker == 0x01) {
                continue;
            }
            // Check if the marker is EOI or SOS.
            if (marker == 0xD9 || marker == 0xDA) {
                break;
            }

            // Get the length and check if it is reasonable.
            length = pack(jpeg, offset, 2, false);
            if (length < 2 || offset + length > jpeg.length) {
                Log.e(TAG, "Invalid length");
                return 0;
            }

            // Break if the marker is EXIF in APP1.
            if (marker == 0xE1 && length >= 8 &&
                    pack(jpeg, offset + 2, 4, false) == 0x45786966 &&
                    pack(jpeg, offset + 6, 2, false) == 0) {
                offset += 8;
                length -= 8;
                break;
            }

            // Skip other markers.
            offset += length;
            length = 0;
        }

        // JEITA CP-3451 Exif Version 2.2
        if (length > 8) {
            // Identify the byte order.
            int tag = pack(jpeg, offset, 4, false);
            if (tag != 0x49492A00 && tag != 0x4D4D002A) {
                Log.e(TAG, "Invalid byte order");
                return 0;
            }
            boolean littleEndian = (tag == 0x49492A00);

            // Get the offset and check if it is reasonable.
            int count = pack(jpeg, offset + 4, 4, littleEndian) + 2;
            if (count < 10 || count > length) {
                Log.e(TAG, "Invalid offset");
                return 0;
            }
            offset += count;
            length -= count;

            // Get the count and go through all the elements.
            count = pack(jpeg, offset - 2, 2, littleEndian);
            while (count-- > 0 && length >= 12) {
                // Get the tag and check if it is orientation.
                tag = pack(jpeg, offset, 2, littleEndian);
                if (tag == 0x0112) {
                    // We do not really care about type and count, do we?
                    int orientation = pack(jpeg, offset + 8, 2, littleEndian);
                    switch (orientation) {
                    case 1:
                        return 0;
                    case 3:
                        return 180;
                    case 6:
                        return 90;
                    case 8:
                        return 270;
                    default:
                        break;
                    }
                    Log.i(TAG, "Unsupported orientation");
                    return 0;
                }
                offset += 12;
                length -= 12;
            }
        }

        Log.i(TAG, "Orientation not found");
        return 0;
    }

    private static int pack(byte[] bytes, int offset, int length,
                            boolean littleEndian) {
        int step = 1;
        if (littleEndian) {
            offset += length - 1;
            step = -1;
        }

        int value = 0;
        while (length-- > 0) {
            value = (value << 8) | (bytes[offset] & 0xFF);
            offset += step;
        }
        return value;
    }


    public static int calculateInSampleSize(int width, int height, int reqWidth, int reqHeight) {
        // Raw height and width of image
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static Bitmap svgToBitmap(Resources res, int resource, int size) {
        try {
            size = (int) (size * res.getDisplayMetrics().density);
            SVG svg = SVG.getFromResource(res, resource);

            Bitmap bmp;
            bmp = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            svg.renderToCanvas(canvas);


            return bmp;
        } catch (SVGParseException e) {
            Log.e(TAG, "svgToBitmap: ", e);
        }
        return null;
    }


    public static BitmapDrawable svgToBitmapDrawable(Resources res, int resource, int size) {
        try {
            size = (int) (size * res.getDisplayMetrics().density);
            SVG svg = SVG.getFromResource(res, resource);
            Bitmap bmp = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            svg.renderToCanvas(canvas);

            return new BitmapDrawable(res, bmp);
        } catch (SVGParseException e) {
            Log.e(TAG, "svgToBitmapDrawable: ", e);
        }
        return null;
    }

    public static Drawable createStateDrawableFromSvg(Resources resources, int svgPressed, int svgNormal) {
        BitmapDrawable pressedBmp = PictureUtils.svgToBitmapDrawable(resources, svgPressed, 80);
        BitmapDrawable normalBmp = PictureUtils.svgToBitmapDrawable(resources, svgNormal, 80);
        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_checked}, pressedBmp);
        res.addState(new int[]{android.R.attr.state_selected}, pressedBmp);
        res.addState(new int[]{}, normalBmp);
        return res;
    }

    public static Bitmap makeTransparent(Bitmap src, int value) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap transBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(transBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        // config paint
        final Paint paint = new Paint();
        paint.setAlpha(value);
        canvas.drawBitmap(src, 0, 0, paint);
        return transBitmap;
    }
}
