package com.kindiedays.common.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.kindiedays.common.R;

import java.util.Arrays;
import java.util.Locale;

public class LocaleHelper {

    private static final String LANGUAGE_CODE = "languageCode";

    public static String getLanguage(Context context) {
        String systemLanguage = Resources.getSystem().getConfiguration().locale.getLanguage();
        boolean contains = Arrays.asList(context.getResources().getStringArray(R.array.languageCodes)).contains(systemLanguage);
        return getPersistedData(context, contains ? systemLanguage : context.getString(R.string.default_language));
    }

    public static void setLocale(Context context, @Nullable String language) {
        if (TextUtils.isEmpty(language)) {
            removeLanguage(context);
            String systemLanguage = Resources.getSystem().getConfiguration().locale.getLanguage();
            boolean contains = Arrays.asList(context.getResources().getStringArray(R.array.languageCodes)).contains(systemLanguage);
            language = contains ? systemLanguage : context.getString(R.string.default_language);
        } else {
            persist(context, language);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResources(context, language);
        } else {
            updateResourcesLegacy(context, language);
        }

    }

    public static String getPersistedData(Context context, String defaultLanguage) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(LANGUAGE_CODE, defaultLanguage);
    }

    private static void persist(Context context, String language) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LANGUAGE_CODE, language);
        editor.apply();
    }

    private static void removeLanguage(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().remove(LANGUAGE_CODE).apply();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);

        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale);
        }

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }
}