package com.kindiedays.common.utils.comparator;


import com.kindiedays.common.pojo.Generic;

import java.util.Comparator;

public class DateComparator implements Comparator<Generic> {
    @Override
    public int compare(Generic lhs, Generic rhs) {
        String distance = String.valueOf(lhs.getTime());
        String distance1 = String.valueOf(rhs.getTime());
        if (distance.compareTo(distance1) > 0) {
            return -1;
        } else if (distance.compareTo(distance1) < 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
