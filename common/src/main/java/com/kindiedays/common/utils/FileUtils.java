package com.kindiedays.common.utils;


import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

@SuppressWarnings("squid:S1226")
public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();

    private FileUtils() {
        //no instance
    }

    @SuppressLint("NewApi")
    public static String getFilePath(final Context context, Uri uri) {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null) {
                return getFilePathFromCursor(cursor, MediaStore.Images.Media.DATA);
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    @SuppressWarnings("squid:S2093")
    public static File getFileFromGoogleDrive(final Context context, final Intent data) {
        File file = null;
        InputStream inputStream = null;
        FileOutputStream fos = null;
        try {
            inputStream = context.getContentResolver().openInputStream(data.getData());
            if (inputStream == null) {
                return null;
            }

            byte[] buffer = new byte[8128];
            Cursor cursor = context.getContentResolver().query(data.getData(), null, null, null, null);
            String name = "unknown.pdf";
            if (cursor != null) {
                name = getFilePathFromCursor(cursor, "_display_name");
            }

            file = new File(context.getCacheDir() + "/" + name);
            fos = new FileOutputStream(file);

            int symbol;
            while ((symbol = inputStream.read(buffer, 0, buffer.length)) > -1) {
                fos.write(buffer, 0, symbol);
            }
            fos.flush();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "file not found: ", e);
        } catch (IOException e) {
            Log.e(TAG, "onActivityResult: ", e);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }

                if (inputStream != null) {
                    inputStream.close();
                }

            } catch (IOException e) {
                Log.e(TAG, "onActivityResult: ", e);
            }
        }

        return file;
    }

    private static String getFilePathFromCursor(Cursor cursor, String columnName) {
        try {
            int columnIndex = cursor.getColumnIndexOrThrow(columnName);
            if (cursor.moveToFirst()) {
                return cursor.getString(columnIndex);
            }
        } catch (Exception e) {
            Log.e(TAG, "getFilePath: ", e);
        } finally {
            cursor.close();
        }

        return STRING_EMPTY;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGogleDriveDocument(Uri uri) {
        return "com.google.android.apps.docs.storage.legacy".equals(uri.getAuthority());
    }
}
