package com.starcut.starflight;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpStatus;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;


public class StarFlightClient {
    public static final String TEXT_KEY = "text";
    public static final String URL_KEY = "url";
    public static final String SUBJECT_KEY = "subject";
    public static final String BADGE_KEY = "badgeNumber";

    private static final String PUSH_SERVER_URL = "https://starflight.starcloud.us/push";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String LOG_TAG = "StarFlight Push Client";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_LAST_SENT_REG_ID = "last_sent_registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_REGISTERED_TAGS = "registeredTags";
    private static final String PROPERTY_STARFLIGHT_UUID = "starflightUuid";

    private static final String SEPARATOR = ",";
    private static final String TAG = StarFlightClient.class.getSimpleName();

    private final String senderId;
    private final String appId;
    private final String clientSecret;

    private GoogleCloudMessaging gcm;
    private String registrationId;

    private RegistrationCallback callback;

    public StarFlightClient(String senderId, String appId, String clientSecret) {
        this.senderId = senderId;
        this.appId = appId;
        this.clientSecret = clientSecret;
    }

    public RegistrationCallback getCallback() {
        return callback;
    }

    public void setCallback(RegistrationCallback callback) {
        this.callback = callback;
    }


    public interface RegistrationCallback {
        void onStarflightRegistrationSuccess(String uuid);

        void onStarflightRegistrationFailure();

        void onStarflightAlreadyRegistered(String uuid);
    }

    public static String getStarflightUuid(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(StarFlightClient.class.getSimpleName(), Context.MODE_PRIVATE);
        return preferences.getString(PROPERTY_STARFLIGHT_UUID, null);
    }

    public void register(Activity activity) {
        register(activity, null);
    }

    public void register(Activity activity, List<String> tags) {
        if (checkPlayServices(activity)) {
            gcm = GoogleCloudMessaging.getInstance(activity);
            registrationId = getRegistrationId(activity.getApplicationContext());

            if (registrationId.length() == 0) {
                getRegistrationIdInBackground(activity, tags);
            } else {
                sendRegistrationIdIfNeeded(activity, tags);
            }
        }
    }

    public void unregister(Activity activity, List<String> tags) {
        if (checkPlayServices(activity)) {
            gcm = GoogleCloudMessaging.getInstance(activity);
            registrationId = getRegistrationId(activity.getApplicationContext());

            if (registrationId.length() == 0) {
                return; // App not registered
            }

            sendUnregistrationInBackground(activity, tags);

        }
    }

    @SuppressWarnings("unchecked")
    private void sendUnregistrationInBackground(final Context context, List<String> tags) {

        new AsyncTask<List<String>, Void, String>() {

            @Override
            protected String doInBackground(List<String>... params) {
                String msg = "";
                List<String> tags = params[0];
                if (tags != null && !tags.isEmpty()) {
                    // only unregister the specified tags
                    // Unregister tags from server
                    if (sendUnregistrationIdToBackend(tags)) {
                        // If successful, update list of tags
                        removeTagsFromStorage(context, tags);
                    }
                } else {
                    // Unregister completely
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    try {
                        gcm.unregister();
                        sendUnregistrationIdToBackend(null);
                        removeRegistrationFromStorage(context);
                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                        Log.e(TAG, "doInBackground: ", ex);
                        // If there is an error, don't just keep trying to
                        // register.
                        // Require the user to click a button again, or perform
                        // exponential back-off.
                    }
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(LOG_TAG, msg);
            }

        }.execute(tags);
    }

    @SuppressWarnings("unchecked")
    private void sendRegistrationIdIfNeeded(final Context context, List<String> tags) {
        SharedPreferences preferences = getGCMPreferences(context);
        String lastSentId = preferences.getString(PROPERTY_LAST_SENT_REG_ID, "");

        if (!lastSentId.equals(registrationId)) {
            new AsyncTask<List<String>, Void, String>() {
                @Override
                protected String doInBackground(List<String>... params) {
                    List<String> tags = null;
                    if (params.length > 0) {
                        tags = params[0];
                    }

                    String uuid = sendRegistrationIdToBackend(tags);
                    if (uuid != null) {
                        storeSentRegistrationId(context, registrationId, tags);
                    }

                    return uuid;
                }

                @Override
                protected void onPostExecute(String uuid) {
                    if (uuid != null) {
                        SharedPreferences.Editor editor = context.getSharedPreferences(StarFlightClient.class.getSimpleName(), Context.MODE_PRIVATE).edit();
                        editor.putString(PROPERTY_STARFLIGHT_UUID, uuid).commit();
                    }
                    if (callback != null) {
                        if (uuid != null) {
                            callback.onStarflightRegistrationSuccess(uuid);
                        } else {
                            callback.onStarflightRegistrationFailure();
                        }
                    }
                }
            }.execute(tags, null, null);
        } else if (callback != null) {
            callback.onStarflightAlreadyRegistered(getStarflightUuid(context));
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    private static boolean checkPlayServices(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.e(LOG_TAG, "This device is not supported. Finishing activity.");
                activity.finish();
            }

            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String regId = prefs.getString(PROPERTY_REG_ID, "");
        if (regId.length() == 0) {
            Log.i(LOG_TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(LOG_TAG, "App version changed.");
            return "";
        }
        return regId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(StarFlightClient.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    @SuppressWarnings("squid:S00112")
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @SuppressWarnings("unchecked")
    private void getRegistrationIdInBackground(final Context context, List<String> tags) {
        new AsyncTask<List<String>, Void, String>() {
            String uuid = null;

            @Override
            protected String doInBackground(List<String>... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }

                    registrationId = gcm.register(senderId);
                    msg = "Device registered, registration ID=" + registrationId;

                    // You should send the registration ID to your server over
                    // HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your
                    // app.
                    // The request to your server should be authenticated if
                    // your app
                    // is using accounts.
                    List<String> tags = null;
                    if (params.length > 0) {
                        tags = params[0];
                    }
                    uuid = sendRegistrationIdToBackend(tags);
                    if (uuid != null) {
                        storeSentRegistrationId(context, registrationId, tags);
                    }

                    // For this demo: we don't need to send it because the
                    // device
                    // will send upstream messages to a server that echo back
                    // the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, registrationId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.e(TAG, "doInBackground: ", ex);
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(LOG_TAG, msg);
                if (uuid != null) {
                    SharedPreferences.Editor editor = context.getSharedPreferences(StarFlightClient.class.getSimpleName(), Context.MODE_PRIVATE).edit();
                    editor.putString(PROPERTY_STARFLIGHT_UUID, uuid).commit();
                }
                if (callback != null) {
                    if (uuid != null) {
                        callback.onStarflightRegistrationSuccess(uuid);
                    } else {
                        callback.onStarflightRegistrationFailure();
                    }
                }
            }
        }.execute(tags, null, null);
    }

    private boolean sendUnregistrationIdToBackend(List<String> tags) {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(PUSH_SERVER_URL);
        boolean success = false;

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("action", "unregister"));
            nameValuePairs.add(new BasicNameValuePair("appId", appId));
            nameValuePairs.add(new BasicNameValuePair("clientSecret", clientSecret));
            nameValuePairs.add(new BasicNameValuePair("type", "android"));
            nameValuePairs.add(new BasicNameValuePair("token", registrationId));

            if (tags != null && !tags.isEmpty()) {
                nameValuePairs.add(new BasicNameValuePair("tags", join(tags, SEPARATOR)));
            }

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = client.execute(post);
            int code = response.getStatusLine().getStatusCode();

            if (code == HttpStatus.SC_CREATED) {
                success = true;
                Log.i(LOG_TAG, "Registered push client");
            } else if (code == HttpStatus.SC_OK) {
                success = true;
                Log.i(LOG_TAG, "Push client registration already exists");
            } else {
                Log.e(LOG_TAG, "Failed to register push client, http status " + code);
            }

            response.getEntity().consumeContent();
        } catch (Exception e) {
            success = false;
            Log.e(LOG_TAG, "Failed to register push client", e);
        }

        return success;
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use
     * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
     * since the device sends upstream messages to a server that echoes back the
     * message using the 'from' address in the message.
     */
    private String sendRegistrationIdToBackend(List<String> tags) {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(PUSH_SERVER_URL);
        String uuid = null;

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("action", "register"));
            nameValuePairs.add(new BasicNameValuePair("appId", appId));
            nameValuePairs.add(new BasicNameValuePair("clientSecret", clientSecret));
            nameValuePairs.add(new BasicNameValuePair("type", "android"));
            nameValuePairs.add(new BasicNameValuePair("token", registrationId));

            if (tags != null && !tags.isEmpty()) {
                nameValuePairs.add(new BasicNameValuePair("tags", join(tags, SEPARATOR)));
            }

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = client.execute(post);
            int code = response.getStatusLine().getStatusCode();

            if (code == HttpStatus.SC_CREATED) {
                uuid = getUuidFromResponse(EntityUtils.toString(response.getEntity(), "utf-8"));
                Log.i(LOG_TAG, "Registered push client");
            } else if (code == HttpStatus.SC_OK) {
                uuid = getUuidFromResponse(EntityUtils.toString(response.getEntity(), "utf-8"));
                Log.i(LOG_TAG, "Push client registration already exists");
            } else {
                Log.e(LOG_TAG, "Failed to register push client, http status " + code);
            }
            response.getEntity().consumeContent();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to register push client", e);
        }

        return uuid;
    }

    private String getUuidFromResponse(String response) {
        try {
            JSONObject json = new JSONObject(response.replace("for(;;);", ""));
            return json.getString("clientUuid");
        } catch (Exception e) {
            Log.e(TAG, "getUuidFromResponse: ", e);
            return null;
        }
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(LOG_TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private void removeRegistrationFromStorage(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(PROPERTY_REG_ID);
        editor.remove(PROPERTY_REGISTERED_TAGS);
        editor.remove(PROPERTY_APP_VERSION);
        editor.remove(PROPERTY_LAST_SENT_REG_ID);
        editor.commit();
    }

    private void removeTagsFromStorage(Context context, List<String> tags) {
        final SharedPreferences prefs = getGCMPreferences(context);

        if (tags != null && !tags.isEmpty()) {
            List<String> previousTags = Arrays.asList(prefs.getString(PROPERTY_REGISTERED_TAGS, "").split(SEPARATOR));

            for (String tag : tags) {
                previousTags.remove(tag);
            }

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(PROPERTY_REGISTERED_TAGS, join(previousTags, SEPARATOR));
            editor.commit();
        } else {
            //no tags specified, we remove them all
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(PROPERTY_REGISTERED_TAGS);
            editor.commit();
        }
    }

    private void storeSentRegistrationId(Context context, String regId, List<String> tags) {
        final SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_LAST_SENT_REG_ID, regId);
        editor.putString(PROPERTY_REGISTERED_TAGS, join(tags, SEPARATOR));
        editor.commit();
    }

    private static String join(List<String> list, String separator) {
        if (list != null && !list.isEmpty()) {
            StringBuilder joined = new StringBuilder();

            for (int i = 0; i < list.size(); i++) {
                joined.append(list.get(i));

                if (i < list.size() - 1) {
                    joined.append(separator);
                }
            }

            return joined.toString();
        }

        return null;
    }
}
