package com.starcut.starflight;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public abstract class StarFlightBroadcastReceiver extends BroadcastReceiver
{
	@Override
    public void onReceive(Context context, Intent data)
    {
    	Log.i("StarFlight Push Client", "Received broadcast: " + data);
    	
    	Bundle extras = data.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        String messageType = gcm.getMessageType(data);

		if (!extras.isEmpty() && extras.containsKey(StarFlightClient.TEXT_KEY) && GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
			String subject = (extras.containsKey(StarFlightClient.SUBJECT_KEY)) ? extras.getString(StarFlightClient.SUBJECT_KEY) : null;
			String url = (extras.containsKey(StarFlightClient.URL_KEY)) ? extras.getString(StarFlightClient.URL_KEY) : null;
			String badgeNumber = (extras.containsKey(StarFlightClient.BADGE_KEY)) ? extras.getString(StarFlightClient.BADGE_KEY) : null;
			onReceive(context, subject, extras.getString(StarFlightClient.TEXT_KEY), url, badgeNumber);
			Log.d("StarFlight Push Client", extras.toString());
		}
    }
	
	public abstract void onReceive(Context context, String subject, String text, String url, String badgeNumber);
}
