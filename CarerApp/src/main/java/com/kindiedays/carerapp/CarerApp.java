package com.kindiedays.carerapp;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.stetho.Stetho;
import com.kindiedays.carerapp.ui.LoginActivity;
import com.kindiedays.common.KindieDaysApp;

/**
 * Created by pleonard on 14/09/2015.
 */
public class CarerApp extends KindieDaysApp {
    private static final String LOGIN_SETTINGS = "com.kindiedays.carerapp.loginSettings";
    private static final String CHANNEL_ID = BuildConfig.APPLICATION_ID + ".pushnotifications";

    private SharedPreferences sharedPreferences;
    public static CarerApp instance;

    public static CarerApp getApp() {
        return instance;
    }

    @Override
    public String getChanelId() {
        return CHANNEL_ID;
    }

    public CarerApp() {
        super();
        instance = this;
        loginActivityClass = LoginActivity.class;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

    public SharedPreferences getCarerSharedPreferences() {
        if (sharedPreferences == null)
            sharedPreferences = getSharedPreferences(LOGIN_SETTINGS, Context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
