package com.kindiedays.carerapp.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.ui.MainActivity;
import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Kindergarten;

import java.util.List;
import java.util.Observable;

import static com.kindiedays.carerapp.util.constants.FamilyAppConstants.CHANGE_LANGUAGE_TAG;
import static com.kindiedays.common.utils.constants.CommonConstants.STRING_NOT_EMPTY;

/**
 * Created by pleonard on 11/09/2015.
 */
public class SettingsFragment extends com.kindiedays.common.ui.settings.SettingsFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_settings, null);
        ivKindergartenPicture = (ImageView) v.findViewById(R.id.kindergartenPicture);
        tvName = (TextView) v.findViewById(R.id.kindergartenName);
        tvAddress = (TextView) v.findViewById(R.id.kindergartenAddress);
        tvPhone = (TextView) v.findViewById(R.id.kindergartenPhoneNumber);
        tvManagerEmail = (TextView) v.findViewById(R.id.managerEmail);
        tvVersionNumber = (TextView) v.findViewById(R.id.versionNumber);
        spLanguages = (Spinner) v.findViewById(R.id.language);
        logoutProgress = (ProgressBar) v.findViewById(R.id.logoutProgress);
        swNotifications = (Switch) v.findViewById(R.id.notifications);
        btnLogout = (Button) v.findViewById(R.id.logout);
        v = onCreateView(v);
        return v;
    }

    @Override
    public void switchToLanguage(String language){
        super.switchToLanguage(language);
        restartApp(STRING_NOT_EMPTY);
    }

    @Override
    protected void clearCurrentUser() {
        ChildBusiness.getInstance().setCurrentChild(null);
    }

    @Override
    protected void restartApp(String key) {
        Intent refresh = new Intent(getActivity(), MainActivity.class);
        refresh.putExtra(CHANGE_LANGUAGE_TAG, key);
        startActivity(refresh);
        getActivity().finish();
    }

    @Override
    public void refreshData() {
        LoadKindergartenDataAsyncTask task = new LoadKindergartenDataAsyncTask();
        pendingTasks.add(task);
        task.execute();
    }

    private class LoadKindergartenDataAsyncTask extends KindergartenBusiness.LoadKindergartenDataTask {

        @Override
        protected void onPostExecute(List<Kindergarten> kindergartens) {
            if(!checkNetworkException(kindergartens, exception)){
                super.onPostExecute(kindergartens);
                Child child = ChildBusiness.getInstance().getCurrentChild();
                Kindergarten kindergarten = KindergartenBusiness.getInstance().getKindergarten(child.getKindergartenId());
                if (kindergarten != null){
                    updateKindergartenInformation(kindergarten);
                }
            }
            pendingTasks.remove(this);
        }

    }

}
