package com.kindiedays.carerapp.ui.childselection;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.network.session.PatchSession;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.ui.PersonSelectionDialogFragment;
import com.kindiedays.common.ui.personselection.PersonAdapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

/**
 * Created by pleonard on 12/06/2015.
 */
public class ChildSelectionDialogFragment extends PersonSelectionDialogFragment {
    private ListView childList;
    private PersonAdapter<Child> childAdapter;
    private Button okButton;
    private ProgressBar progress;
    private Set<AsyncTask> pendingTasks = new HashSet<>();
    private int selectedPersonPosition = NO_SELECTION;
    private static final int NO_SELECTION = -1;
    private boolean isLoginClicked;

    AdapterView.OnItemClickListener onChildClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectedPersonPosition = position;
            childList.setItemChecked(position, true);
        }
    };

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (selectedPersonPosition != NO_SELECTION && !isLoginClicked) {
                isLoginClicked = true;
                Child selectedChild = (Child) childAdapter.getItem(selectedPersonPosition);
                if (selectedChild != null) {
                    PatchSessionTask task = new PatchSessionTask(selectedChild.getKindergartenId());
                    task.execute();
                }
                if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    for (int i = getActivity().getSupportFragmentManager().getBackStackEntryCount(); i > 1; i--) {
                        getActivity().onBackPressed();
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_pincode, null);
        isLoginClicked = false;
        childList = (ListView) v.findViewById(R.id.personList);
        childAdapter = new PersonAdapter<>(getActivity());
        childList.setAdapter(childAdapter);
        childList.setOnItemClickListener(onChildClickedListener);
        v.findViewById(R.id.pincode).setVisibility(View.GONE);
        okButton = (Button) v.findViewById(R.id.okButton);
        progress = (ProgressBar) v.findViewById(R.id.progressBar);
        getDialog().setCanceledOnTouchOutside(ChildBusiness.getInstance().getCurrentChild() != null);
        getDialog().setTitle(getString(R.string.Child_Selection));
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        okButton.setOnClickListener(onOkClickedListener);
        LoadChildrenListTask task = new LoadChildrenListTask();
        pendingTasks.add(task);
        task.execute();

        return v;
    }

    @Override
    public void onStop() {
        super.onStop();
        stopPendingTasks();
    }

    @SuppressWarnings("deprecation")
    private void checkNetworkException(KindieDaysNetworkException exception) {
        if (exception.isLocalizedMessage()) {
            Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                listener.onNotAuthorized();
            } else {
                Log.e(this.getClass().getSimpleName(), "Internal error");
            }
        }
    }

    protected void stopPendingTasks() {
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        pendingTasks.clear();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (ChildBusiness.getInstance().getCurrentChild() == null) {
            listener.onNoOneSelected();
        }
    }

    private class LoadChildrenListTask extends ChildBusiness.LoadChildrenTask {

        @Override
        protected List<Child> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(List<Child> children) {
            super.onPostExecute(children);
            if (exception != null && (children == null || children.size() == 0)) {
                checkNetworkException(exception);
            } else if (children != null) {
                childAdapter.setPersons(children);
                Child activeChild = ChildBusiness.getInstance().getCurrentChild();
                if (activeChild != null) {
                    childList.setSelection(children.indexOf(ChildBusiness.getInstance().getCurrentChild()));
                }
                progress.setVisibility(View.GONE);
                okButton.setVisibility(View.VISIBLE);
                selectFirstChild();
            }
            pendingTasks.remove(this);
        }

        private void selectFirstChild() {
            childList.setItemChecked(0, true);
            selectedPersonPosition = 0;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
            okButton.setVisibility(View.GONE);
        }

    }

    private class PatchSessionTask extends NetworkAsyncTask<Boolean> {

        private final long kindergartenId;

        public PatchSessionTask(long kindergartenId) {
            this.kindergartenId = kindergartenId;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success != null && success) {
                Child selectedChild = (Child) childAdapter.getItem(selectedPersonPosition);
                LoadChildInfoTask task = new LoadChildInfoTask(selectedChild.getId());
                pendingTasks.add(task);
                task.execute();
            } else {
                if (exception != null && exception.isLocalizedMessage()) {
                    Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Internal application error", Toast.LENGTH_LONG).show();
                }
            }
            pendingTasks.remove(this);
        }

        @Override
        protected Boolean doNetworkTask() throws Exception {
            PatchSession.patch(kindergartenId);
            return true;
        }
    }

    private class LoadChildInfoTask extends ChildBusiness.GetChildInfo {

        LoadChildInfoTask(Long id) {
            super(id);
        }

        @Override
        protected void onPostExecute(Child child) {
            super.onPostExecute(child);
            if (child == null && exception != null) {
                checkNetworkException(exception);
            } else if (child != null) {
                listener.onPersonSelected();
                dismissAllowingStateLoss();
            }
            pendingTasks.remove(this);
        }
    }
}
