package com.kindiedays.carerapp.ui.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.CarerBusiness;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.ui.view.ContainersTabIndicator;
import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.ui.MainActivityFragment;

import java.util.List;

/**
 * Created by pleonard on 11/09/2015.
 */
public class EventsFragment extends MainActivityFragment {
    private static final String COMING_UP_TAG = "tag1";
    private static final String PAST_TAG = "tag2";

    private TabHost tabHost;
    private ListView lvComingUpEvents;
    private ListView lvPastEvents;
    private EventsAdapter pastAdapter;
    private EventsAdapter comingUpAdapter;
    private ProgressBar progressBar;
    private TextViewCustom title;
    private List<Event> comingUpEvents;
    private List<Event> pastEvents;

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //just check references
            boolean isPast = parent.getAdapter().equals(pastAdapter);
            Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
            intent.putExtra(EventDetailsActivity.EVENT_ID, id);
            intent.putExtra(EventDetailsActivity.IS_PAST_EVENT, isPast);

            startActivity(intent);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_events, null);
        tabHost = (TabHost) v.findViewById(R.id.tabHost);
        lvComingUpEvents = (ListView) v.findViewById(R.id.comingup);
        lvPastEvents = (ListView) v.findViewById(R.id.past);
        progressBar = (ProgressBar) v.findViewById(R.id.progress);
        comingUpAdapter = new EventsAdapter(getActivity(), this, false);
        lvComingUpEvents.setAdapter(comingUpAdapter);
        pastAdapter = new EventsAdapter(getActivity(), this, true);
        lvPastEvents.setAdapter(pastAdapter);
        lvPastEvents.setOnItemClickListener(onItemClickListener);
        lvComingUpEvents.setOnItemClickListener(onItemClickListener);

        if (comingUpEvents != null) {
            comingUpAdapter.setEvents(comingUpEvents);
        }

        if (pastEvents != null) {
            pastAdapter.setEvents(pastEvents);
            progressBar.setVisibility(View.GONE);
        }

        setHasOptionsMenu(true);
        initTabs();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Events));
    }

    private void initTabs() {
        tabHost.setup();
        TabHost.TabSpec ts = tabHost.newTabSpec(COMING_UP_TAG);
        ts.setContent(R.id.comingup);
        ts.setIndicator(createTabView(getString(R.string.Coming_up)));
        tabHost.addTab(ts);
        ts = tabHost.newTabSpec(PAST_TAG);
        ts.setContent(R.id.past);
        ts.setIndicator(createTabView(getString(R.string.Past)));
        tabHost.addTab(ts);
    }

    @Override
    public void refreshData() {
        LoadCurrentCarerForChildTask loadCarerTask = new LoadCurrentCarerForChildTask(
                ChildBusiness.getInstance().getCurrentChild().getId());
        loadCarerTask.execute();
        pendingTasks.add(loadCarerTask);
        LoadEvents task = new LoadEvents();
        pendingTasks.add(task);
        task.execute();
    }

    private ContainersTabIndicator createTabView(String name) {
        ContainersTabIndicator indicator = new ContainersTabIndicator(getActivity());
        indicator.setText(name);
        return indicator;
    }

    private class LoadCurrentCarerForChildTask extends CarerBusiness.LoadCurrentCarerForChild {
        LoadCurrentCarerForChildTask(long childId) {
            super(childId);
        }

        @Override
        protected ChildCarer doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(ChildCarer childCarer) {
            if (!checkNetworkException(childCarer, exception)) {
                super.onPostExecute(childCarer);
                pastAdapter.setReadOnly(childCarer.isReadonly());
            }
            pendingTasks.remove(this);
        }
    }


    private class LoadEvents extends EventsBusiness.GetEventsTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            tabHost.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(List<Event> events) {
            if (!checkNetworkException(events, exception)) {
                super.onPostExecute(events);
                long childId = ChildBusiness.getInstance().getCurrentChild().getId();
                comingUpEvents = EventsBusiness.getInstance().getComingUpEvents(childId);
                comingUpAdapter.setEvents(comingUpEvents);
                pastEvents = EventsBusiness.getInstance().getPastEvents(childId);
                pastAdapter.setEvents(pastEvents);
            }
            progressBar.setVisibility(View.GONE);
            tabHost.setVisibility(View.VISIBLE);
            pendingTasks.remove(this);
        }
    }
}
