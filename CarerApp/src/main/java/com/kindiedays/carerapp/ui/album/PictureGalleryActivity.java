package com.kindiedays.carerapp.ui.album;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.kindiedays.carerapp.R;
import com.kindiedays.common.business.JournalBusiness;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.JournalPostPicture;

import java.util.List;

/**
 * Created by pleonard on 15/07/2015.
 */
public class PictureGalleryActivity extends com.kindiedays.common.ui.picturegallery.PictureGalleryActivity {
    private static final String TAG = PictureGalleryActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_WRITE_STORAGE = 0;
    private Dialog dialog;
    private GalleryPicture picture;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_photo_gallery_carer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            if (!journalMode) {
                final List<GalleryPicture> pictures = PicturesBusiness.getInstance().getCurrentPictureList().getPictures();
                picture = pictures.get(viewPager.getCurrentItem());
                int i = item.getItemId();
                if (i == R.id.action_save) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PictureGalleryActivity.this);
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (isPermissionGrunted()) {
                                saveImageToExternalStorage(picture);
                            } else {
                                askForPermission();
                            }
                        }
                    });
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setTitle(R.string.save);
                    builder.setMessage(R.string.save_photo);
                    builder.setCancelable(false);
                    dialog = builder.create();
                    dialog.show();
                }
            } else {
                final List<JournalPostPicture> pictures = JournalBusiness.getInstance().getJournalPostPictures(getIntent().getLongExtra(JOURNAL_POST_ID, 0));
                final JournalPostPicture picture = pictures.get(viewPager.getCurrentItem());
                int i = item.getItemId();
                if (i == R.id.action_save) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PictureGalleryActivity.this);
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            saveJournalImageToExternalStorage(picture);
                        }
                    });
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setTitle(R.string.save);
                    builder.setMessage(R.string.save_photo);
                    builder.setCancelable(false);
                    dialog = builder.create();
                    dialog.show();
                }
            }
        }
        return true;
    }

    private void askForPermission() {
        ActivityCompat.requestPermissions(PictureGalleryActivity.this, new String[]
                {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_WRITE_STORAGE);
    }

    private boolean isPermissionGrunted() {
        return ContextCompat.checkSelfPermission(PictureGalleryActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void saveImageToExternalStorage(final GalleryPicture pic) {
        Glide.with(getApplicationContext())
                .load(pic.getUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        MediaStore.Images.Media.insertImage(PictureGalleryActivity.this.getContentResolver(), resource, pic.getHash(), pic.getCaption());
                    }
                });
    }

    private void saveJournalImageToExternalStorage(final JournalPostPicture pic) {
        Glide.with(getApplicationContext())
                .load(pic.getUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        MediaStore.Images.Media.insertImage(PictureGalleryActivity.this.getContentResolver(), resource, pic.getHash(), pic.getCaption());
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveImageToExternalStorage(picture);
                }
            }
        }
    }

}
