package com.kindiedays.carerapp.ui.messages;


import android.app.ProgressDialog;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.common.business.ConversationBusiness;
import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationParty;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 15/09/2015.
 */
public class NewMessageActivity extends com.kindiedays.common.ui.messages.NewMessageActivity {

    public NewMessageActivity() {
        onSelectPeopleClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewMessageActivity.this, TeacherSelectionActivity.class);
                intent.putExtra(TeacherSelectionActivity.SELECTED_TEACHER_IDS, selectedTeachersId);
                startActivityForResult(intent, RECIPIENT_SELECTION_TAG);
            }
        };
    }

    @Override
    protected void sendNewMessage(String subject, String message, List<File> attachments, boolean isDelete) {
        if (selectedTeachersId == null || selectedTeachersId.length == 0) {
            Toast.makeText(this, R.string.Please_select_receivers, Toast.LENGTH_SHORT).show();
        } else {
            Set<ConversationParty> parties = new HashSet<>();
            if (selectedTeachersId != null) {
                for (long id : selectedTeachersId) {
                    ConversationParty party = new ConversationParty();
                    party.setPartyId(id);
                    party.setType(ConversationParty.TEACHER_TYPE);
                    parties.add(party);
                }
            }

            CreateConversationsAsyncTask task = new CreateConversationsAsyncTask(subject, message,
                    parties, false, new long[]{ChildBusiness.getInstance().getCurrentChild().getId()},
                    attachments, isDelete);
            getPendingTasks().add(task);
            task.execute();
        }
    }

    private class CreateConversationsAsyncTask extends ConversationBusiness.CreateConversationsTask {
        private ProgressDialog progressDialog;

        CreateConversationsAsyncTask(String subject, String message, Set<ConversationParty> parties,
                                     boolean createAsKindergarten, long[] childrenIds, List<File> attachments,
                                     boolean isDelete) {
            super(subject, message, parties, createAsKindergarten, childrenIds, attachments, isDelete);
        }

        @Override
        protected void onPreExecute() {
            if (attachments.isEmpty()) {
                return;
            }

            initProgressDialog();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int currentProgress = values[0];
            progressDialog.setMessage(getString(com.kindiedays.common.R.string.dialog_progress_file_uploading, currentProgress,
                    attachments.size()));
            progressDialog.setProgress(currentProgress);
        }

        @Override
        protected void onPostExecute(Set<Conversation> conversations) {
            if (!checkNetworkException(conversations, exception)) {
                super.onPostExecute(conversations);
                finish();
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            getPendingTasks().remove(this);
        }

        private void initProgressDialog() {
            progressDialog = new ProgressDialog(NewMessageActivity.this);
            progressDialog.setTitle(com.kindiedays.common.R.string.Upload_In_Progress);
            progressDialog.setMax(attachments.size());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(1);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(getString(com.kindiedays.common.R.string.dialog_progress_file_uploading, 1,
                    attachments.size()));
            progressDialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED && requestCode == RECIPIENT_SELECTION_TAG) {
            finish();
        } else if (requestCode == RECIPIENT_SELECTION_TAG && resultCode == RESULT_OK) {
            selectedTeachersId = data.getLongArrayExtra(TeacherSelectionActivity.SELECTED_TEACHER_IDS);
            initRecipients();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initRecipients() {
        StringBuilder sb = new StringBuilder();
        ConversationPartyBusiness conversationPartyBusiness = ConversationPartyBusiness.getInstance();
        if (conversationPartyBusiness.getConversationParties() == null) {
            relaunchActivity();
            return;
        }

        if (selectedTeachersId != null) {
            for (long id : selectedTeachersId) {
                ConversationParty conversationParty = conversationPartyBusiness.getConversationParty(ConversationParty.TEACHER_TYPE, id);
                sb.append(conversationParty.getName()).append("\n");
            }
        }
        recipientsTextView.setText(sb.toString());
        if (!TextUtils.isEmpty(sb)) {
            isFieldsFilled();
        }
    }

}
