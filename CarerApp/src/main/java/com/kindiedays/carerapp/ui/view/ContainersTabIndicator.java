package com.kindiedays.carerapp.ui.view;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.carerapp.R;

public class ContainersTabIndicator extends LinearLayout {
    private TextView tvContainerName;

    public ContainersTabIndicator(Context context) {
        super(context);
        initViews();
    }

    public ContainersTabIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public ContainersTabIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_containers, this);
        tvContainerName = (TextView) v.findViewById(R.id.containerName);
    }

    public void setText(String text){
        tvContainerName.setText(text);
    }
}
