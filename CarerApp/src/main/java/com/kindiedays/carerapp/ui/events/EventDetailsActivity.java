package com.kindiedays.carerapp.ui.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.CarerBusiness;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.network.invitations.PutInvitations;
import com.kindiedays.carerapp.util.EventUtils;
import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.events.PatchEvent;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;
import com.kindiedays.common.ui.KindieDaysActivity;

import org.joda.time.LocalDateTime;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_SPACE;

/**
 * Created by pleonard on 18/09/2015.
 */
public class EventDetailsActivity extends KindieDaysActivity {
    public static final String EVENT_ID = "eventId";
    public static final String IS_PAST_EVENT = "extra_event_status";

    private Button btnJoin;
    private Button btnDecline;
    private TextView tvState;
    private TextView time;
    private TextView eventLocation;
    private TextView eventDescription;
    private TextView eventName;
    private TextView eventDate;
    private Event event;
    private Button btnAddToCalendar;
    private TextViewCustom title;
    private Button btnBack;

    View.OnClickListener onEventButtonClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            EventInvitation.InvitationState state;
            if (v.getId() == R.id.joinButton) {
                state = EventInvitation.InvitationState.CONFIRMED;
            } else {
                state = EventInvitation.InvitationState.DECLINED;
            }
            JoinAsyncTask task = new JoinAsyncTask(state, ChildBusiness.getInstance().getCurrentChild().getId(), event.getId());
            task.execute();
            getPendingTasks().add(task);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        btnBack = (Button) findViewById(R.id.leftIcon);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setBackgroundResource(R.drawable.menu_left);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.Event_Description);

        long eventId = getIntent().getLongExtra(EVENT_ID, 0);
        boolean isPast = getIntent().getBooleanExtra(IS_PAST_EVENT, true);

        event = EventsBusiness.getInstance().getEvent(eventId);
        if (event == null) {
            relaunchActivity();
            return;
        }

        btnJoin = (Button) findViewById(R.id.joinButton);
        btnDecline = (Button) findViewById(com.kindiedays.carerapp.R.id.declineButton);
        eventLocation = (TextView) findViewById(R.id.eventLocation);
        eventDate = (TextView) findViewById(com.kindiedays.carerapp.R.id.eventDate);
        time = (TextView) findViewById(com.kindiedays.carerapp.R.id.time);
        eventDescription = (TextView) findViewById(com.kindiedays.carerapp.R.id.eventDescription);
        eventName = (TextView) findViewById(com.kindiedays.carerapp.R.id.eventName);
        tvState = (TextView) findViewById(com.kindiedays.carerapp.R.id.state);
        btnAddToCalendar = (Button) findViewById(R.id.addToCalendarButton);
        eventLocation.setText(event.getLocation());
        eventDate.setText(event.getDate().toString("dd-MM-yyyy"));
        String fromDate = getString(com.kindiedays.common.R.string.from_event_date);
        String toDate = getString(com.kindiedays.common.R.string.to_event_date);
        StringBuilder dateStr = new StringBuilder()
                .append(fromDate).append(STRING_SPACE)
                .append(event.getStart().toString("HH:mm")).append(STRING_SPACE)
                .append(toDate).append(STRING_SPACE)
                .append(event.getEnd().toString("HH:mm")).append(STRING_SPACE);
        time.setText(dateStr);
        eventDescription.setText(event.getNotes());
        ChildCarer carer = CarerBusiness.getInstance().getCarerForCurrentChild();
        eventName.setText(event.getName());
        EventInvitation selectedChildInvitation = null;
        if (event.getInvitations().size() > 0) {
            //look for an invitation for the selected child
            for (EventInvitation invitation : event.getInvitations()) {
                if (invitation.getChildId() == ChildBusiness.getInstance().getCurrentChild().getId()) {
                    selectedChildInvitation = invitation;
                    break;
                }
            }
        }
        if (selectedChildInvitation != null) {
            String state = getString(EventUtils.getEventStateName(event.getInvitations().get(0).getState()));
            tvState.setText(state.toUpperCase());
            btnAddToCalendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LocalDateTime startTime = event.getDate().toLocalDateTime(event.getStart());
                    LocalDateTime endTime = event.getDate().toLocalDateTime(event.getEnd());
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra("beginTime", startTime.toDate().getTime());
                    intent.putExtra("allDay", false);
                    intent.putExtra("endTime", endTime.toDate().getTime());
                    intent.putExtra("title", event.getName());
                    startActivity(intent);
                }
            });
            if (carer != null && !carer.isReadonly()) {
                btnJoin.setOnClickListener(onEventButtonClick);
                btnDecline.setOnClickListener(onEventButtonClick);
            } else {
                btnJoin.setVisibility(View.GONE);
                btnDecline.setVisibility(View.GONE);
            }
        } else {
            btnJoin.setVisibility(View.GONE);
            btnDecline.setVisibility(View.GONE);
        }

        checkEventsOnPast(isPast);
        MarkEventRead readIt = new MarkEventRead(event, (int) ChildBusiness.getInstance().getCurrentChild().getId());
        readIt.execute();
        getPendingTasks().add(readIt);
    }

    private void checkEventsOnPast(boolean isPast) {
        int visibility = isPast ? View.GONE : View.VISIBLE;
        btnJoin.setVisibility(visibility);
        btnDecline.setVisibility(visibility);
        btnAddToCalendar.setVisibility(visibility
        );
    }

    class JoinAsyncTask extends NetworkAsyncTask<Void> {

        EventInvitation.InvitationState state;
        long childId;
        long eventId;

        public JoinAsyncTask(EventInvitation.InvitationState state, long childId, long eventId) {
            this.state = state;
            this.childId = childId;
            this.eventId = eventId;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (!checkNetworkException(aVoid, exception)) {
                super.onPostExecute(aVoid);
                String stringState = getString(EventUtils.getEventStateName(state));
                tvState.setText(stringState.toUpperCase());
            }
            getPendingTasks().remove(this);
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            PutInvitations.putInvitation(eventId, childId, state);
            return null;
        }
    }

    class MarkEventRead extends NetworkAsyncTask<Void> {

        private Event mEvent;
        private int mChildId;

        public MarkEventRead(Event event, int childId) {
            this.mEvent = event;
            this.mChildId = childId;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (!checkNetworkException(aVoid, exception)) {
                super.onPostExecute(aVoid);
            }
            getPendingTasks().remove(this);
        }

        protected Void doNetworkTask() throws Exception {
            PatchEvent.markEventAsRead(mEvent, mChildId);
            return null;
        }

    }
}
