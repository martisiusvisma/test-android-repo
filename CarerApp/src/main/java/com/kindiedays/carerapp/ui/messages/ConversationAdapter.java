package com.kindiedays.carerapp.ui.messages;

import android.app.Activity;

import com.kindiedays.common.pojo.ConversationParty;

import java.util.Set;

/**
 * Created by pleonard on 01/10/2015.
 */
public class ConversationAdapter extends com.kindiedays.common.ui.messages.ConversationAdapter {

    public ConversationAdapter(Activity activity) {
        super(activity);
    }

    @Override
    protected boolean isConversationPartyCurrentUser(ConversationParty party, boolean isTeacherAsKindergarten) {
        return party.getType().equals(ConversationParty.CARER_TYPE);
    }

    protected boolean isTeacherAsKindergarten(Set<ConversationParty> conversationParties){
        return false;
    }
}
