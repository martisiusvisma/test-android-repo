package com.kindiedays.carerapp.ui.dailyreport;


import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindiedays.carerapp.R;
import com.kindiedays.common.pojo.Generic;
import com.kindiedays.common.svg.SVGImageView;
import com.kindiedays.common.ui.dailyreport.ParallaxRecyclerAdapter;

import java.util.List;

import static com.kindiedays.common.utils.constants.DailyReportConstants.ACTIVITY;
import static com.kindiedays.common.utils.constants.DailyReportConstants.BATH;
import static com.kindiedays.common.utils.constants.DailyReportConstants.DIAPER_TAG;
import static com.kindiedays.common.utils.constants.DailyReportConstants.MEAL;
import static com.kindiedays.common.utils.constants.DailyReportConstants.MED;
import static com.kindiedays.common.utils.constants.DailyReportConstants.MOOD;
import static com.kindiedays.common.utils.constants.DailyReportConstants.NAP;
import static com.kindiedays.common.utils.constants.DailyReportConstants.NOTE;
import static com.kindiedays.common.utils.constants.DailyReportConstants.POTTY_TAG;
import static com.kindiedays.common.utils.constants.DailyReportConstants.SAD;
import static com.kindiedays.common.utils.constants.DailyReportConstants.SERIOUS;
import static com.kindiedays.common.utils.constants.DailyReportConstants.SMILING;
import static com.kindiedays.common.utils.constants.DailyReportConstants.TOILET_TAG;

public class DailyReportRecycleViewAdapter extends ParallaxRecyclerAdapter<Generic> {
    private List<Generic> items;

    public DailyReportRecycleViewAdapter(List<Generic> items) {
        super(items);
        this.items = items;
    }

    @Override
    public void onBindViewHolderImpl(RecyclerView.ViewHolder viewHolder, ParallaxRecyclerAdapter<Generic> adapter, int i) {
        Generic currentItem = adapter.getData().get(i);
        DailyReportViewHolder vh = (DailyReportViewHolder) viewHolder;
        vh.bind(currentItem);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup viewGroup,
                                                          final ParallaxRecyclerAdapter<Generic> adapter,
                                                          int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_recyclerview, viewGroup, false);
        return new DailyReportViewHolder(rootView);
    }

    @Override
    public int getItemCountImpl(ParallaxRecyclerAdapter<Generic> adapter) {
        return items.size();
    }


    private class DailyReportViewHolder extends RecyclerView.ViewHolder {
        private TextView timeTv;
        private SVGImageView logoIv;
        private TextView raw1Tv;
        private TextView raw2Tv;
        private ImageView ivMoreInfo;

        private DailyReportViewHolder(View itemView) {
            super(itemView);
            timeTv = (TextView) itemView.findViewById(R.id.time);
            logoIv = (SVGImageView) itemView.findViewById(R.id.logo);
            raw1Tv = (TextView) itemView.findViewById(R.id.raw1);
            raw2Tv = (TextView) itemView.findViewById(R.id.raw2);
            ivMoreInfo = (ImageView) itemView.findViewById(R.id.iv_more_info);
        }

        private String validateAcceptableLength(final String str) {
            String temp = str;
            if (str.length() > 34) {
                temp = str.substring(0, 24) + "...";
                ivMoreInfo.setImageResource(R.drawable.chevrone_right);
                ivMoreInfo.setVisibility(View.VISIBLE);
            } else {
                ivMoreInfo.setVisibility(View.GONE);
            }
            return temp;
        }

        private void bind(Generic currentItem) {
            timeTv.setText(currentItem.getTime().toString("HH:mm"));
            if (currentItem.getRow1().length() != 0) {
                raw1Tv.setVisibility(View.VISIBLE);
            } else {
                raw1Tv.setVisibility(View.GONE);
            }
            //check it
            if (currentItem.getRow2().length() != 0) {
                raw2Tv.setVisibility(View.VISIBLE);
            } else {
                raw2Tv.setVisibility(View.GONE);
            }

            switch (currentItem.getType()) {
                case MOOD:
                    resolveMood(currentItem);
                    break;
                case NOTE:
                    logoIv.setImageResource(+R.raw.dr_notes_circle);
                    raw1Tv.setText(validateAcceptableLength(currentItem.getRow1()));
                    raw2Tv.setVisibility(View.GONE);
                    break;
                case MED:
                    logoIv.setImageResource(+R.raw.dr_medication_circle);
                    raw1Tv.setText(validateAcceptableLength(currentItem.getRow1()));
                    raw2Tv.setText(validateAcceptableLength(currentItem.getRow2()));
                    break;
                case MEAL:
                    resolveMeal(currentItem);
                    break;
                case NAP:
                    logoIv.setImageResource(+R.raw.dr_sleep_circle);
                    raw1Tv.setText(currentItem.getRow1());
                    raw2Tv.setText(validateAcceptableLength(currentItem.getNap().getNotes()));
                    break;
                case ACTIVITY:
                    logoIv.setImageResource(+R.raw.dr_activites_circle);
                    raw1Tv.setText(currentItem.getRow1());
                    raw2Tv.setText(validateAcceptableLength(currentItem.getRow2()));
                    break;
                case BATH:
                    resolveBath(currentItem);
                    break;
                default:
                    break;
            }
        }

        private void resolveMood(Generic currentItem) {
            switch (currentItem.getRow1()) {
                case SAD:
                    raw1Tv.setText(R.string.Sad);
                    logoIv.setImageResource(+R.raw.dr_mood_sad_circle);
                    break;
                case SMILING:
                    raw1Tv.setText(R.string.Happy);
                    logoIv.setImageResource(+R.raw.dr_mood_happy_circle);
                    break;
                case SERIOUS:
                    raw1Tv.setText(R.string.OK);
                    logoIv.setImageResource(+R.raw.dr_mood_ok_circle);
                    break;
                default:
                    break;
            }

            raw2Tv.setText(validateAcceptableLength(currentItem.getMood().getNotes()));
        }

        private void resolveMeal(Generic currentItem) {
            logoIv.setImageResource(+R.raw.dr_meals_circle);
            raw1Tv.setText(currentItem.getMeal().getReactionDescription());
            if (!TextUtils.isEmpty(currentItem.getMeal().getNotes())) {
                ivMoreInfo.setImageResource(R.drawable.chevrone_right);
                ivMoreInfo.setVisibility(View.VISIBLE);
            } else {
                ivMoreInfo.setVisibility(View.GONE);
            }

            switch (currentItem.getRow2()) {
                case "0.0":
                    raw2Tv.setText(R.string.empty_glass);
                    break;
                case "0.5":
                    raw2Tv.setText(R.string.half_glass);
                    break;
                case "1.0":
                    raw2Tv.setText(R.string.full_glass);
                    break;
                default:
                    //DRMealActivity.setDrink(raw2Tv, currentItem.getMeal().getDrink());
                    break;
//            default:
//                break;
            }

        }

        private void resolveBath(Generic currentItem) {
            switch (currentItem.getRow1()) {
                case DIAPER_TAG:
                    raw1Tv.setText(R.string.Diaper);
                    logoIv.setImageResource(+R.raw.dr_wc_diaper_circle);
                    break;
                case POTTY_TAG:
                    raw1Tv.setText(R.string.Potty);
                    logoIv.setImageResource(+R.raw.dr_wc_potty_circle);
                    break;
                case TOILET_TAG:
                    raw1Tv.setText(R.string.Toilet);
                    logoIv.setImageResource(+R.raw.dr_wc_toilet_circle);
                    break;
                default:
                    break;
            }
            raw2Tv.setText(currentItem.getBath().getTypeDescription());
            if (!TextUtils.isEmpty(currentItem.getBath().getNotes())) {
                ivMoreInfo.setImageResource(R.drawable.chevrone_right);
                ivMoreInfo.setVisibility(View.VISIBLE);
            } else {
                ivMoreInfo.setVisibility(View.GONE);
            }

        }
    }
}