package com.kindiedays.carerapp.ui.attendance;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.business.PresenceBusiness;
import com.kindiedays.carerapp.pojo.Presence;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Kindergarten;
import com.kindiedays.common.ui.CalendarTitleFormatter;
import com.kindiedays.common.ui.MainActivityFragment;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by pleonard on 11/09/2015.
 */
public class AttendanceFragment extends MainActivityFragment {

    private MaterialCalendarView calendarView;
    private Kindergarten kindergarten;
    private List<Presence> presences;
    private Child currentChild;

    OnDateSelectedListener onDateSelectedListener = new OnDateSelectedListener() {

        @Override
        public void onDateSelected(MaterialCalendarView widget, final CalendarDay date, final boolean selected) {
            if (kindergarten == null) {
                Toast.makeText(getActivity(), getString(R.string.not_loaded_properly_message, getString(R.string.kindergarten_list)), Toast.LENGTH_SHORT).show();
                return;
            } else if (date.getDate().getTime() < LocalDate.now().plusDays(kindergarten.getPresenceLockDays()).toDate().getTime()) {
                //Presence locked
                calendarView.setDateSelected(date, !selected);
                Toast.makeText(getActivity(), getString(R.string.Locked), Toast.LENGTH_SHORT).show();
                return;
            }
            showDialog(selected, date, widget);
        }
    };

    OnMonthChangedListener onMonthChangedListener = new OnMonthChangedListener() {

        @Override
        public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
            refreshData();
        }
    };

    @Override
    public void onAttach(Context context) {
        currentChild = ChildBusiness.getInstance().getCurrentChild();
        kindergarten = KindergartenBusiness.getInstance().getKindergarten(currentChild.getKindergartenId());
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_attendance, null);
        calendarView = (MaterialCalendarView) v.findViewById(R.id.calendarView);
        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
        calendarView.setOnMonthChangedListener(onMonthChangedListener);
        calendarView.setOnDateChangedListener(onDateSelectedListener);
        calendarView.setTitleFormatter(new CalendarTitleFormatter());
        Kindergarten kindergarten = KindergartenBusiness.getInstance().getKindergarten();
        calendarView.addDecorator(new LockDecorator(Color.RED, kindergarten != null ? kindergarten.getPresenceLockDays() : 0));
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        TextViewCustom title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.CareTimes));
    }

    @Override
    public void refreshData() {
        Calendar calendar = Calendar.getInstance();
        String root = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-";
        String startDate = root + "01";
        String endDate = root + calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        LoadPresences task = new LoadPresences(startDate, endDate, currentChild.getId());
        task.execute();
        pendingTasks.add(task);
    }

    @SuppressWarnings("squid:S3398")
    private void showDialog(final boolean selected, final CalendarDay date, final MaterialCalendarView widget) {

        String message = buildDialogMessage(selected, date);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.Confirmation))
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                onOkClicked(selected, date);
                            }
                        }
                ).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                widget.setDateSelected(date, !selected);
                dialog.dismiss();
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }

    @SuppressWarnings("squid:S3398")
    private void onOkClicked(boolean selected, CalendarDay date) {
        if (kindergarten == null) {
            Toast.makeText(getActivity(), getString(R.string.not_loaded_properly_message, getString(R.string.kindergarten_list)), Toast.LENGTH_SHORT).show();
            return;
        } else if (presences == null) {
            Toast.makeText(getActivity(), getString(R.string.not_loaded_properly_message, getString(R.string.presence_list)), Toast.LENGTH_SHORT).show();
            return;
        }

        String defaultStatus = kindergarten.getDefaultAttendance();

        if (selected) {
            //Post new presence
            String status = defaultStatus.equals(Presence.ABSENT) ? Presence.PRESENT : Presence.ABSENT;
            Presence presence = new Presence();
            presence.setStatus(status);
            presence.setChildId(currentChild.getId());
            presence.setStart(LocalDate.fromCalendarFields(date.getCalendar()));
            presence.setEnd(LocalDate.fromCalendarFields(date.getCalendar()));
            PostPresence task = new PostPresence(presence);
            task.execute();
            //No need to add to pending tasks as there's no UI update
        } else {
            //Delete presence information
            Set<Presence> presencesToDelete = new HashSet<>();
            for (Presence presence : presences) {
                LocalDate selectedDate = LocalDate.fromCalendarFields(date.getCalendar());
                if (!selectedDate.isBefore(presence.getStart()) && !selectedDate.isAfter(presence.getEnd())) {
                    presencesToDelete.add(presence);
                }
            }
            for (Presence presence : presencesToDelete) {
                presences.remove(presence);
                DeletePresence task = new DeletePresence(presence.getId());
                task.execute();
                //No need to add to pending tasks as there's no UI update
            }
        }
    }

    @SuppressWarnings("squid:S3398")
    private String buildDialogMessage(boolean selected, CalendarDay date) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
        String formattedDate = dateFormat.format(date.getDate().getTime());
        String attendance = selected ? getContext().getString(R.string.Absence)
                : getContext().getString(R.string.Presence);
        return getContext().getString(R.string.Confirm) + " " + attendance + " - "
                + CalendarTitleFormatter.getDayOfWeekString(getContext(), date.getCalendar()) + " " + formattedDate + "?";
    }

    class PostPresence extends PresenceBusiness.PostPresenceAsyncTask {

        public PostPresence(Presence presence) {
            super(presence);
        }

        protected Presence doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.data).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(Presence presence) {
            if (!checkNetworkException(presence, exception)) {
                AttendanceFragment.this.presences.add(presence);
            }
        }
    }


    class DeletePresence extends PresenceBusiness.DeletePresenceAsyncTask {

        public DeletePresence(long presenceId) {
            super(presenceId);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (!checkNetworkException(aVoid, exception)) {
                super.onPostExecute(aVoid);
            }
        }
    }


    class LoadPresences extends PresenceBusiness.GetPresencesAsyncTask {

        public LoadPresences(String startDate, String endDate, long childId) {
            super(startDate, endDate, childId);
        }

        @Override
        protected List<Presence> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(List<Presence> presences) {
            if (kindergarten == null) {
                Toast.makeText(getActivity(), getString(R.string.not_loaded_properly_message, getString(R.string.kindergarten_list)), Toast.LENGTH_SHORT).show();
            } else if (!checkNetworkException(presences, exception)) {
                super.onPostExecute(presences);
                for (Presence presence : presences) {
                    LocalDate currentDate = presence.getStart();
                    while (!currentDate.isAfter(presence.getEnd())) {
                        if (!presence.getStatus().equals(kindergarten.getDefaultAttendance())) {
                            calendarView.setDateSelected(currentDate.toDate(), true);
                            currentDate = currentDate.withFieldAdded(DurationFieldType.days(), 1);
                        }
                    }
                }
                AttendanceFragment.this.presences = presences;
            }
            pendingTasks.remove(this);
        }
    }
}
