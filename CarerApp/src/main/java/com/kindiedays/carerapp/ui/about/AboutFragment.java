package com.kindiedays.carerapp.ui.about;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.CarerBusiness;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.ui.MainActivity;
import com.kindiedays.carerapp.ui.childselection.CarerChildPositionSelectionActivity;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.ChildProfile;
import com.kindiedays.common.ui.CustomTimePickerDialog;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.utils.GlideUtils;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by pleonard on 11/09/2015.
 */
public class AboutFragment extends MainActivityFragment {
    private static final String TAG = AboutFragment.class.getSimpleName();
    private static final int MY_REQUEST_CODE = 1;
    private static final String OUTPUT_FILE_URI_KEY = "someVarA";

    //Child

    private ImageView childPicture;
    private Button dropoffTime;
    private Button pickupTime;
    private CheckBox photoConsent;
    private Button birthDate;
    private EditText socialSecurityNumber;
    private EditText nativeLanguage;
    private EditText streetAddress;
    private EditText zipCode;
    private EditText city;
    private EditText allergies;
    private EditText diet;

    //Carer
    private TextView carerName;
    private TextView email;
    private EditText occupation;
    private EditText placeOfWork;
    private EditText workPhoneNumber;
    private EditText homePhoneNumber;
    private EditText mobilePhoneNumber;
    private EditText carerNativeLanguage;

    private Uri outputFileUri;
    private Child child;
    public static final int SELECT_PICTURE_INTENT_CODE = 1;
    public static final int TAKE_PICTURE_INTENT_CODE = 2;
    public static final int SELECT_FACE_INTENT_CODE = 3;

    private ChildProfile childProfile;
    private ChildCarer carer;

    View.OnClickListener onDateClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (childProfile != null) {
                LocalDate date;
                if (childProfile.getDateOfBirth() != null) {
                    date = childProfile.getDateOfBirth();
                } else {
                    date = new LocalDate();
                }
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                LocalDate date = new LocalDate(year, monthOfYear + 1, dayOfMonth);
                                childProfile.setDateOfBirth(date);
                                birthDate.setText(getString(R.string.Date).concat(date.toString("dd/MM/yyyy")));
                            }
                        },
                        date.getYear(), date.getMonthOfYear() - 1, date.getDayOfMonth()
                );
                dialog.getDatePicker().setCalendarViewShown(false);
                dialog.show();
            }
        }
    };

    View.OnClickListener onPictureUpdateClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setItems(new String[]{getString(R.string.chooseFromLibrary), getString(R.string.takePicture)}, onAlertClickListener);
            builder.setTitle(getString(R.string.choosepicture));
            builder.create().show();
        }
    };

    AlertDialog.OnClickListener onAlertClickListener = new AlertDialog.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE_INTENT_CODE);

            } else if (which == 1) {
                if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE);
                } else {
                    navigateToCameraScreen();
                }

            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
                navigateToCameraScreen();
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(getActivity(), "Need to turn on camera permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void navigateToCameraScreen() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG" + "_" + timeStamp + "_atop";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        Log.d(TAG, "storageDir: " + storageDir);
        try {
            File image = new File(storageDir, imageFileName + ".jpg");
            outputFileUri = Uri.fromFile(image);

            Log.d(TAG, "onClick: " + outputFileUri);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            startActivityForResult(takePictureIntent, TAKE_PICTURE_INTENT_CODE);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Impossible to take picture", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "onClick: ", e);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(OUTPUT_FILE_URI_KEY, outputFileUri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            outputFileUri = savedInstanceState.getParcelable(OUTPUT_FILE_URI_KEY);
        }
    }

    View.OnClickListener onTimeClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View v) {
            if (childProfile != null) {
                LocalTime time;
                if (v.getId() == R.id.pickupTime) {
                    time = childProfile.getPickupTime();
                } else {
                    time = childProfile.getDropoffTime();
                }
                if (time == null) {
                    time = LocalTime.now();
                }
                TimePickerDialog dialog = new CustomTimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            @SuppressWarnings("squid:S1192")
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                LocalTime localTime = new LocalTime(hourOfDay, minute);
                                if (v.getId() == R.id.pickupTime) {
                                    childProfile.setPickupTime(localTime);
                                    pickupTime.setText(localTime.toString("HH:mm"));
                                } else {
                                    childProfile.setDropoffTime(localTime);
                                    dropoffTime.setText(localTime.toString("HH:mm"));
                                }
                            }
                        },
                        time.getHourOfDay(), time.getMinuteOfHour(), true);
                dialog.show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        child = ChildBusiness.getInstance().getCurrentChild();
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_about, null);
        childPicture = (ImageView) v.findViewById(R.id.kidPicture);
        TextView childName = (TextView) v.findViewById(R.id.childName);
        dropoffTime = (Button) v.findViewById(R.id.dropoffTime);
        pickupTime = (Button) v.findViewById(R.id.pickupTime);
        photoConsent = (CheckBox) v.findViewById(R.id.photoPermission);
        birthDate = (Button) v.findViewById(R.id.dateBirth);
        socialSecurityNumber = (EditText) v.findViewById(R.id.socialSecurityNumber);
        nativeLanguage = (EditText) v.findViewById(R.id.nativeLanguage);
        streetAddress = (EditText) v.findViewById(R.id.streetAddress);
        zipCode = (EditText) v.findViewById(R.id.zipCode);
        city = (EditText) v.findViewById(R.id.city);
        allergies = (EditText) v.findViewById(R.id.allergies);
        diet = (EditText) v.findViewById(R.id.diet);
        carerName = (TextView) v.findViewById(R.id.carerName);
        email = (TextView) v.findViewById(R.id.email_text_view);
        occupation = (EditText) v.findViewById(R.id.occupation);
        placeOfWork = (EditText) v.findViewById(R.id.placeOfWork);
        workPhoneNumber = (EditText) v.findViewById(R.id.workPhoneNumber);
        homePhoneNumber = (EditText) v.findViewById(R.id.homePhoneNumber);
        mobilePhoneNumber = (EditText) v.findViewById(R.id.mobilePhoneNumber);
        carerNativeLanguage = (EditText) v.findViewById(R.id.carerNativeLanguage);
        setHasOptionsMenu(true);
        childName.setText(child.getName());
        Log.d(TAG, "onCreateView: " + child.getProfilePictureUrl());
        Glide.with(this).load(child.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(childPicture);

        if (carer != null) {
            initChildCarer();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        TextViewCustom title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.About));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode + "resultCode" + resultCode);
        String pathToPicture;
        if (resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "onActivityResult: " + requestCode);
            switch (requestCode) {
                case SELECT_PICTURE_INTENT_CODE:
                    pathToPicture = data.getData().toString();
                    Log.d(TAG, "onActivityResult: SELECT_PICTURE_INTENT_CODE" + child.getProfilePictureUrl());
                    startSelectFaceActivity(pathToPicture);
                    break;
                case TAKE_PICTURE_INTENT_CODE:
                    Log.d(TAG, "onActivityResult: TAKE_PICTURE_INTENT_CODE" + child.getProfilePictureUrl());
                    startSelectFaceActivity(outputFileUri.toString());
                    break;
                case SELECT_FACE_INTENT_CODE:
                    //update child picture
                    child = ChildBusiness.getInstance().getCurrentChild();
                    Log.d(TAG, "onActivityResult: SELECT_FACE_INTENT_CODE " + child.getProfilePictureUrl());
                    Glide.with(this).load(child.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(childPicture);
                    ((MainActivity) getActivity()).updateChild();
                    break;
                default:
                    break;
            }
            //Open select face activity
        }
    }

    private void startSelectFaceActivity(String pathToPicture) {
        Intent intent = new Intent(getActivity(), CarerChildPositionSelectionActivity.class);
        intent.putExtra(CarerChildPositionSelectionActivity.CHILD_ID, child.getId());
        intent.putExtra(CarerChildPositionSelectionActivity.PATH_OF_PICTURE, pathToPicture);
        startActivityForResult(intent, SELECT_FACE_INTENT_CODE);
    }

    @Override
    public void refreshData() {
        GetChildProfileTask task = new GetChildProfileTask(child.getId());
        task.execute();
        pendingTasks.add(task);

        GetCurrentChildCarerTask getCarerTask = new GetCurrentChildCarerTask(child.getId());
        getCarerTask.execute();
        pendingTasks.add(task);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (carer == null || !carer.isReadonly()) {
            inflater.inflate(com.kindiedays.common.R.menu.menu_ok, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_ok && carer != null) {
            if (childProfile != null && !carer.isReadonly()) {
                if (allergies.getText().toString().length() != 0) {
                    childProfile.setAllergies(allergies.getText().toString());
                }
                if (city.getText().toString().length() != 0) {
                    childProfile.setCity(city.getText().toString());
                }
                if (diet.getText().toString().length() != 0) {
                    childProfile.setDiet(diet.getText().toString());
                }
                if (nativeLanguage.getText().toString().length() != 0) {
                    childProfile.setNativeLanguage(nativeLanguage.getText().toString());
                }
                childProfile.setPhotoConsent(photoConsent.isChecked());
                if (socialSecurityNumber.getText().toString().length() != 0) {
                    childProfile.setSocialSecurityNumber(socialSecurityNumber.getText().toString());
                }
                if (streetAddress.getText().toString().length() != 0) {
                    childProfile.setStreetAddress(streetAddress.getText().toString());
                }
                if (zipCode.getText().toString().length() != 0) {
                    childProfile.setZipCode(zipCode.getText().toString());
                }
                SaveChildProfileTask task = new SaveChildProfileTask(childProfile);
                task.execute();
                pendingTasks.add(task);
            }
            carer.setContactDetailsPublic(carer.isContactDetailsPublic());
            carer.setHomePhoneNumber(homePhoneNumber.getText().toString());
            carer.setMobilePhoneNumber(mobilePhoneNumber.getText().toString());
            carer.setNativeLanguage(carerNativeLanguage.getText().toString());
            carer.setOccupation(occupation.getText().toString());
            carer.setPlaceOfWork(placeOfWork.getText().toString());
            carer.setWorkPhoneNumber(workPhoneNumber.getText().toString());
            SaveChildCarerTask saveCarertask = new SaveChildCarerTask(carer);
            saveCarertask.execute();
            pendingTasks.add(saveCarertask);

        }
        return true;
    }

    class GetCurrentChildCarerTask extends CarerBusiness.LoadCurrentCarerForChild {

        public GetCurrentChildCarerTask(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(ChildCarer childCarer) {
            if (!checkNetworkException(childCarer, exception)) {

                super.onPostExecute(childCarer);
                carer = childCarer;
                if (carer != null) {
                    initChildCarer();
                }
            }
            pendingTasks.remove(this);
        }
    }

    class SaveChildProfileTask extends ChildBusiness.SaveChildProfile {
        public SaveChildProfileTask(ChildProfile childProfile) {
            super(childProfile);
        }

        @Override
        protected void onPostExecute(ChildProfile childProfile) {
            if (!checkNetworkException(childProfile, exception)) {
                super.onPostExecute(childProfile);
                Toast.makeText(getActivity(), getString(R.string.child_profile_saved), Toast.LENGTH_SHORT).show();
            }
            pendingTasks.remove(this);
        }

    }

    class SaveChildCarerTask extends CarerBusiness.PutCarerTask {
        public SaveChildCarerTask(ChildCarer childCarer) {
            super(childCarer);
        }

        @Override
        protected void onPostExecute(ChildCarer childCarer) {
            if (!checkNetworkException(childCarer, exception)) {
                super.onPostExecute(childCarer);
            }
            pendingTasks.remove(this);
        }
    }

    class GetChildProfileTask extends ChildBusiness.LoadChildProfile {

        public GetChildProfileTask(long id) {
            super(id);
        }

        @Override
        protected void onPostExecute(ChildProfile childProfile) {
            if (!checkNetworkException(childProfile, exception) && childProfile != null) {
                super.onPostExecute(childProfile);
                //Set values
                AboutFragment.this.childProfile = childProfile;
                photoConsent.setChecked(childProfile.isPhotoConsent());
                if (childProfile.getPickupTime() != null) {
                    pickupTime.setText(childProfile.getPickupTime().toString("HH:mm"));
                }
                if (childProfile.getDropoffTime() != null) {
                    dropoffTime.setText(childProfile.getDropoffTime().toString("HH:mm"));
                }
                if (childProfile.getDateOfBirth() != null) {
                    birthDate.setText(childProfile.getDateOfBirth().toString("dd/MM/yyyy"));
                }
                socialSecurityNumber.setText(childProfile.getSocialSecurityNumber());
                nativeLanguage.setText(childProfile.getNativeLanguage());
                streetAddress.setText(childProfile.getStreetAddress());
                zipCode.setText(childProfile.getZipCode());
                city.setText(childProfile.getCity());
                allergies.setText(childProfile.getAllergies());
                diet.setText(childProfile.getDiet());
            }
            pendingTasks.remove(this);
        }
    }

    private void initChildCarer() {
        carerName.setText(carer.getName());
        email.setText(carer.getEmail());
        occupation.setText(carer.getOccupation());
        placeOfWork.setText(carer.getPlaceOfWork());
        workPhoneNumber.setText(carer.getWorkPhoneNumber());
        homePhoneNumber.setText(carer.getHomePhoneNumber());
        mobilePhoneNumber.setText(carer.getMobilePhoneNumber());
        carerNativeLanguage.setText(carer.getNativeLanguage());
        if (!carer.isReadonly()) {
            pickupTime.setOnClickListener(onTimeClickListener);
            dropoffTime.setOnClickListener(onTimeClickListener);
            birthDate.setOnClickListener(onDateClickListener);
            childPicture.setOnClickListener(onPictureUpdateClickListener);
        }
        photoConsent.setEnabled(!carer.isReadonly());
        childPicture.setClickable(!carer.isReadonly());
        dropoffTime.setClickable(!carer.isReadonly());
        pickupTime.setClickable(!carer.isReadonly());
        birthDate.setClickable(!carer.isReadonly());
        socialSecurityNumber.setEnabled(!carer.isReadonly());
        nativeLanguage.setEnabled(!carer.isReadonly());
        streetAddress.setEnabled(!carer.isReadonly());
        zipCode.setEnabled(!carer.isReadonly());
        city.setEnabled(!carer.isReadonly());
        allergies.setEnabled(!carer.isReadonly());
        diet.setEnabled(!carer.isReadonly());
    }
}
