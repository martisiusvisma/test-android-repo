package com.kindiedays.carerapp.ui.attendance;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import org.joda.time.LocalDate;

import java.util.Date;

public class LockDecorator implements DayViewDecorator {

    private int color;
    private Date limitDay;
    private Date today;

    public LockDecorator(int color, int lockDuration) {
        this.color = color;
        limitDay = LocalDate.now().plusDays(lockDuration).toDate();
        today = LocalDate.now().toDate();
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return day.getDate().getTime() < limitDay.getTime() && day.getDate().getTime() >= today.getTime();
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new DotSpan(5, color));
    }
}