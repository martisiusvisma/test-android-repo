package com.kindiedays.carerapp.ui.childselection;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.utils.PictureUtils;

import java.io.File;

/**
 * Created by pleonard on 07/01/2016.
 */
@SuppressWarnings({"squid:MaximumInheritanceDepth", "squid:S1226"})
public class CarerChildPositionSelectionActivity extends com.kindiedays.common.ui.personselection.ChildPositionSelectionActivity {
    private static final String TAG = CarerChildPositionSelectionActivity.class.getSimpleName();
    private long childId;
    private int radius;
    private Point circleCenter;
    private boolean isOkClicked;

    public static final String CHILD_ID = "childId";


    @SuppressWarnings({"squid:S3776", "squid:S1067"})
    public CarerChildPositionSelectionActivity() {
        super();
        surfaceTouchListener = new View.OnTouchListener() {
            Point moveFrom = null;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isOkClicked) {
                    int x = (int) event.getX();
                    int y = (int) event.getY();
                    int maxX = mSurfaceView.getRight() - mSurfaceView.getLeft();
                    int maxY = mSurfaceView.getBottom() - mSurfaceView.getTop();
                    if (x < 0) {
                        x = 0;
                    }
                    if (y < 0) {
                        y = 0;
                    }
                    if (x >= maxX) {
                        x = maxX - 1;
                    }
                    if (y >= maxY) {
                        y = maxY - 1;
                    }
                    int action = event.getAction();
                    Point corner = null;
                    if ((action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_MOVE) && moveFrom == null && rectBoxFirstCorner != null && rectBoxFirstCorner.get() != null) {
                        corner = calculateOtherCornerofSquare(rectBoxFirstCorner.get(), x, y);
                    } else {
                        corner = new Point(x, y);
                    }

                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            if (rectangleBox != null && rectangleBox.contains(x, y)) { //Check if tap inside the rectangle
                                moveFrom = corner;//Enter move mode
                            } else {
                                if (rectBoxFirstCorner != null) { //Start a new selection
                                    rectBoxFirstCorner.set(corner);
                                }
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            if (circleCenter != null) {
                                rectangleBox = createRect(maxX, maxY);
                            }
                            moveFrom = null;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            final Canvas canvas = mSurfaceView.getHolder().lockCanvas();
                            if (canvas == null){
                                break;
                            }
                            if (moveFrom != null) {
                                int offsetx = x - moveFrom.x;
                                int offsety = y - moveFrom.y;
                                if (circleCenter.x + offsetx - radius > 0 && circleCenter.x + offsetx + radius < maxX) {
                                    circleCenter.x = circleCenter.x + offsetx;
                                }
                                if (circleCenter.y + offsety - radius > 0 && circleCenter.y + offsety + radius < maxY) {
                                    circleCenter.y = circleCenter.y + offsety;
                                }
                                moveFrom.x = x;
                                moveFrom.y = y;
                            } else {
                                if (rectBoxFirstCorner != null) {
                                    radius = calculateDistance(rectBoxFirstCorner.get(), corner) / 2;
                                    circleCenter = new Point();
                                    circleCenter.x = calculateCoordinateCenter(rectBoxFirstCorner.get().x, corner.x, radius);
                                    circleCenter.y = calculateCoordinateCenter(rectBoxFirstCorner.get().y, corner.y, radius);
                                }

                                if (circleCenter.x - radius < 0) {
                                    radius = circleCenter.x;
                                }
                                if (circleCenter.x + radius > maxX) {
                                    radius = maxX - circleCenter.x;
                                }
                                if (circleCenter.y - radius < 0) {
                                    radius = circleCenter.y;
                                }
                                if (circleCenter.y + radius > maxY) {
                                    radius = maxY - circleCenter.y;
                                }
                            }
                            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR); // remove old rectangle
                            canvas.drawCircle(circleCenter.x, circleCenter.y, (float) radius, rectStrokePaint);
                            canvas.drawCircle(circleCenter.x, circleCenter.y, (float) radius, rectFillPaint);
                            mSurfaceView.getHolder().unlockCanvasAndPost(canvas);
                            break;
                        default:
                            break;
                    }
                }
                return true;
            }
        };
    }

    private int calculateCoordinateCenter(int point1Coordinate, int point2Coordinate, int radius) {
        if (point1Coordinate - point2Coordinate < 0) {
            return point1Coordinate + radius;
        } else {
            return point1Coordinate - radius;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isOkClicked = false;
        childId = getIntent().getLongExtra(CHILD_ID, 0);
        showInstructions();
    }

    public void showInstructions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.Select_face_title).setMessage(R.string.Select_face_desc).setCancelable(true);
        AlertDialog dialog = builder.show();
        dialog.setCanceledOnTouchOutside(true);
    }

    public int calculateDistance(Point point1, Point point2) {
        return (int) Math.round(Math.sqrt(Math.pow((point2.x - point1.x), 2) + Math.pow((point2.y - point1.y), 2)));
    }

    private Point calculateOtherCornerofSquare(Point firstCorner, int x, int y) {
        int width = x - firstCorner.x;
        int height = y - firstCorner.y;
        if (Math.abs(width) > Math.abs(height)) {
            //y should be changed so that height would be equal to width
            if (y > firstCorner.y) {
                y = firstCorner.y + Math.abs(width);
            } else {
                y = firstCorner.y - Math.abs(width);
            }

        } else {
            //x should be changed so that width would be equal to height
            if (x > firstCorner.x) {
                x = firstCorner.x + Math.abs(height);
            } else {
                x = firstCorner.x - Math.abs(height);
            }
        }
        return new Point(x, y);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.kindiedays.common.R.menu.menu_ok, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == com.kindiedays.common.R.id.action_ok && !isOkClicked) {
            isOkClicked = rectangleBox == null || rectangleBox.isEmpty() ? false : true;
            if (rectangleBox == null || rectangleBox.isEmpty()) {
                showInstructions();
            } else {
                //Capture the selected area and save it to a file
                try {
                    File file = new File(getFilesDir(), "profilepicture.jpg");

                    PictureUtils.cropRotateAndCreate(this, picturePath, file,
                            new Rect(mPictureView.getOriginalXPosition(rectangleBox.left)
                                    , mPictureView.getOriginalYPosition(rectangleBox.top)
                                    , mPictureView.getOriginalXPosition(rectangleBox.right)
                                    , mPictureView.getOriginalYPosition(rectangleBox.bottom)));

                    //Send it to the server
                    PatchProfilePicture task = new PatchProfilePicture(childId, file);
                    getPendingTasks().add(task);
                    task.execute();
                    Log.d(TAG, "onOptionsItemSelected: task.execute()");
                } catch (Exception e) {
                    //Ooops, unable to create the file
                    Log.e(CarerChildPositionSelectionActivity.class.getName(), "Unable to save selected area", e);
                }
            }

        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;

    }

    private Rect createRect(int maxX, int maxY) {
        int left = circleCenter.x - radius;
        if (left < 0) {
            left = 0;
        }
        int top = circleCenter.y - radius;
        if (top < 0) {
            top = 0;
        }
        int right = circleCenter.x + radius;
        if (right > maxX) {
            right = maxX;
        }
        int bottom = circleCenter.y + radius;
        if (bottom > maxY) {
            bottom = maxY;
        }
        return new Rect(left, top, right, bottom);
    }

    class PatchProfilePicture extends ChildBusiness.PatchChildPicture {
        public PatchProfilePicture(long childId, File file) {
            super(childId, file);
        }

        @Override
        protected Child doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(Child child) {
            Log.d(TAG, "onPostExecute: 2");
            if (!checkNetworkException(child, exception)) {
                super.onPostExecute(child);
                ChildBusiness.getInstance().setCurrentChild(child);
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                Toast.makeText(CarerChildPositionSelectionActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
            getPendingTasks().remove(this);
        }
    }
}
