package com.kindiedays.carerapp.ui.messages;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Message;
import com.kindiedays.common.utils.AppUtils;
import com.kindiedays.common.utils.FormatUtils;

import java.io.File;
import java.util.Set;

import static com.kindiedays.common.utils.constants.CommonConstants.CARER_APP_PACKAGE;


/**
 * Created by pleonard on 15/09/2015.
 */
public class ConversationActivity extends com.kindiedays.common.ui.messages.ConversationActivity {
    private static final String TAG = ConversationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        messageAdapter = new MessageAdapter(this, this);
        messageList.setAdapter(messageAdapter);
    }

    @Override
    protected void setPageTitle(Set<ConversationParty> parties) {
        for (ConversationParty party : parties) {
            if (!party.getType().equals(ConversationParty.CARER_TYPE)) {
                getSupportActionBar().setTitle(party.getName());
                break;
            }
        }
    }

    @Override
    protected void openDownloadedAttachment(final Context context, Uri attachmentUri, String attachmentMimeType) {
        if (attachmentUri != null) {
            Uri parsedUri = attachmentUri;
            if (ContentResolver.SCHEME_FILE.equals(attachmentUri.getScheme())) {
                File file = new File(attachmentUri.getPath());
                parsedUri = FileProvider.getUriForFile(this, CARER_APP_PACKAGE, file);
            }

            Intent openAttachmentIntent = new Intent(Intent.ACTION_VIEW);
            openAttachmentIntent.setDataAndType(parsedUri, attachmentMimeType);
            openAttachmentIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                context.startActivity(openAttachmentIntent);
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "openDownloadedAttachment: ", e);
                String format = FormatUtils.getFormat(parsedUri.getLastPathSegment());
                Toast.makeText(context, getString(com.kindiedays.common.R.string.no_application_found, format),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onTextLongClick(Message message) {
        String text = message != null ? message.getText() : "";
        AppUtils.copyToClipboard(getBaseContext(), text);
    }
}
