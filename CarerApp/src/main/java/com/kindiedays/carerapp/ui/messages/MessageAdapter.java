package com.kindiedays.carerapp.ui.messages;

import android.app.Activity;

import com.kindiedays.common.pojo.ConversationParty;

/**
 * Created by pleonard on 15/09/2015.
 */
public class MessageAdapter extends com.kindiedays.common.ui.messages.MessageAdapter {

    public MessageAdapter(Activity activity, OnMediaClickListener listener){
        super(activity, listener);
    }

    protected boolean isConversationPartyCurrentUser(ConversationParty party){
        return party.getType().equals(ConversationParty.CARER_TYPE);
    }

}
