package com.kindiedays.carerapp.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.network.carer.ResetPassword;
import com.kindiedays.common.network.NetworkAsyncTask;

/**
 * Created by pleonard on 23/12/2015.
 */
public class LoginActivity extends com.kindiedays.common.ui.login.LoginActivity {
    private TextView resetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        resetPassword = (TextView) findViewById(R.id.resetPassword);
        EditText emailEditText = (EditText) findViewById(R.id.email);
        emailEditText.setHint(R.string.Email);

        bgImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        if (getResources().getBoolean(com.kindiedays.common.R.bool.isTablet)) {
            bgImage.setImageResource(R.drawable.kd_app_login_fam_ipad);
        } else {
            bgImage.setImageResource(R.drawable.kd_app_login_fam);
        }
        resetPassword.setVisibility(View.VISIBLE);
        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmailView.getText().toString();
                if (isEmailValid(email)) {
                    ResetPasswordTask task = new ResetPasswordTask();
                    task.execute();
                } else {
                    mEmailView.setError(getString(R.string.Error_invalid_email));
                }
            }
        });
    }

    class ResetPasswordTask extends NetworkAsyncTask<Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(LoginActivity.this, getString(R.string.reset_password_link_will_be_sent), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            ResetPassword.reset(mEmailView.getText().toString(), LoginActivity.this.isProd());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (exception != null) {
                if (exception.isLocalizedMessage()) {
                    Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Log.e(LoginActivity.class.getSimpleName(), "Internal error");
                }
            }
            super.onPostExecute(aVoid);
        }
    }
}
