package com.kindiedays.carerapp.ui.attendance;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.kindiedays.carerapp.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

public class SelectionDecorator implements DayViewDecorator {

    private Context context;

    public SelectionDecorator(Context context) {
        this.context = context;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return true;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(ContextCompat.getDrawable(context, R.color.transparent));
    }
}
