package com.kindiedays.carerapp.ui.dailyreport;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.DailyReportBusiness;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.GeneralDailyRes;
import com.kindiedays.common.pojo.Generic;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.ui.dailyreport.DRActivityActivity;
import com.kindiedays.common.ui.dailyreport.DRBathActivity;
import com.kindiedays.common.ui.dailyreport.DRMealActivity;
import com.kindiedays.common.ui.dailyreport.DRMedicationActivity;
import com.kindiedays.common.ui.dailyreport.DRMoodActivity;
import com.kindiedays.common.ui.dailyreport.DRNapActivity;
import com.kindiedays.common.ui.dailyreport.DRNoteActivity;
import com.kindiedays.common.ui.dailyreport.ParallaxRecyclerAdapter;
import com.kindiedays.common.utils.comparator.DateComparator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

/**
 * Created by pleonard on 11/09/2015.
 */
public class DailyReportFragment extends MainActivityFragment {
    private RecyclerView mRecyclerView;
    private Activity activity;

    public static final String MOOD = "MOOD";
    public static final String NOTE = "NOTE";
    public static final String MED = "MED";
    public static final String MEAL = "MEAL";
    public static final String NAP = "NAP";
    public static final String ACTIVITY_TAG = "ACTIVITY";
    public static final String BATH = "BATH";
    private static final int NOTE_LINES_CAP = 34;

    private GalleryPicture headerImg;
    private GeneralDailyRes dailyItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_daily_report, null);
        activity = getActivity();
        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler);

        GetChildPhotosTask imageHeader = new GetChildPhotosTask(ChildBusiness.getInstance().getCurrentChild().getId());
        imageHeader.execute();
        pendingTasks.add(imageHeader);

        refreshDailyReportData(new DateTime());
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(com.kindiedays.common.R.menu.menu_daily_report, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_openCalendarPicker) {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog timePickerDialog = new DatePickerDialog(getActivity(), R.style.datePickerStyle, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    String input = String.format(Locale.getDefault(), "%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);

                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                    DateTime dateTime = formatter.parseDateTime(input);
                    refreshDailyReportData(dateTime);
                }
            }, year, month, day);

            timePickerDialog.show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        TextViewCustom title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.Daily_Report));
    }

    @Override
    public void refreshData() {
        // this is called at every refresh
    }

    @SuppressWarnings("squid:S3398")
    private void createAdapter(RecyclerView recyclerView, GalleryPicture headerImg, GeneralDailyRes daily) {

        String url = "";
        if (headerImg != null) {
            url = headerImg.getUrl();
        }

        final ParallaxRecyclerAdapter<Generic> adapter = new DailyReportRecycleViewAdapter(daily.getGeneric());

        adapter.setOnClickEvent(new ParallaxRecyclerAdapter.OnClickEvent() {
            @Override
            public void onClick(View v, int position) {
                Intent mIntent;
                Bundle mBundle = new Bundle();
                Generic item = dailyItems.getGeneric().get(position);
                switch (item.getType()) {
                    case MOOD:
                        mIntent = new Intent(activity, DRMoodActivity.class);
                        mBundle.putSerializable(MOOD, item.getMood());
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        break;
                    case NOTE:
                        if (item.getNote().getText().length() > NOTE_LINES_CAP) {
                            mIntent = new Intent(activity, DRNoteActivity.class);
                            mBundle.putSerializable(NOTE, item.getNote());
                            mIntent.putExtras(mBundle);
                            startActivity(mIntent);
                        }
                        break;
                    case MED:
                        mIntent = new Intent(activity, DRMedicationActivity.class);
                        mBundle.putSerializable(MED, item.getMed());
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        break;
                    case MEAL:
                        mIntent = new Intent(activity, DRMealActivity.class);
                        mBundle.putSerializable(MEAL, item.getMeal());
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        break;
                    case NAP:
                        mIntent = new Intent(activity, DRNapActivity.class);
                        mBundle.putSerializable(NAP, item.getNap());
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        break;
                    case ACTIVITY_TAG:
                        mIntent = new Intent(activity, DRActivityActivity.class);
                        mBundle.putSerializable(ACTIVITY_TAG, item.getActivity());
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        break;
                    case BATH:
                        mIntent = new Intent(activity, DRBathActivity.class);
                        mBundle.putSerializable(BATH, item.getBath());
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        break;
                    default:
                        break;
                }
                activity.overridePendingTransition(R.anim.from_right_to_left, R.anim.exit_to_left);
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        View header = getActivity().getLayoutInflater().inflate(R.layout.header, recyclerView, false);

        ImageView headerIV = (ImageView) header.findViewById(R.id.imageView);
        TextViewCustom dateTv = (TextViewCustom) header.findViewById(R.id.textView);
        Glide.with(activity).load(url).centerCrop().into(headerIV);
        dateTv.setText(dailyItems.getDate().toString("dd/MM/yyyy"));
        headerIV.setImageResource(R.color.transparent);
        adapter.setParallaxHeader(header, recyclerView);
        adapter.setData(daily.getGeneric());
        recyclerView.setAdapter(adapter);
    }

    private class GetChildPhotosTask extends PicturesBusiness.GetPicturesForChildAsyncTask {

        GetChildPhotosTask(long id) {
            super(id);
        }

        @Override
        protected PictureList doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            if (!checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                if (!pictureList.getPictures().isEmpty()) {
                    headerImg = pictureList.getPictures().get(0);
                }
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), R.string.dialog_no_internet_message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
            }
            Log.d("TAG", "Header image");
            pendingTasks.remove(this);
        }
    }


    private class GetChildDailyReportTask extends DailyReportBusiness.GetDailyForChildAsyncTask {

        GetChildDailyReportTask(Long id, DateTime date) {
            super(id, date);
        }

        @Override
        protected GeneralDailyRes doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.data));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(GeneralDailyRes daily) {
            if (getActivity() != null && !checkNetworkException(daily, exception)) {
                Collections.sort(daily.getGeneric(), new DateComparator());
                dailyItems = daily;
                createAdapter(mRecyclerView, headerImg, daily);
            }
            pendingTasks.remove(this);
        }
    }

    private void refreshDailyReportData(DateTime day) {
        GetChildDailyReportTask dailyReport = new GetChildDailyReportTask(ChildBusiness.getInstance().getCurrentChild().getId(), day);
        dailyReport.execute();
        pendingTasks.add(dailyReport);
    }

}
