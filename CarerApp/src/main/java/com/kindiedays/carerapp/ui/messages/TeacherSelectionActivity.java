package com.kindiedays.carerapp.ui.messages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.kindiedays.carerapp.R;
import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.personselection.PersonSelectionActivity;
import com.kindiedays.common.ui.personselection.teacherselection.TeacherSelectionFragment;

/**
 * Created by Pierem on 15/01/2016.
 */
public class TeacherSelectionActivity extends PersonSelectionActivity {

    private static final String TEACHER_SELECTION_TAG = "teacherFragment";



    @Override
    public void onPersonClick(Person teacher) {
        if(teacher.isSelected()){
            selectedTeachers.remove(((ConversationParty) teacher).getPartyId());
        }
        else{
            selectedTeachers.add(((ConversationParty) teacher).getPartyId());
        }
        ConversationPartyBusiness.getInstance().select((ConversationParty) teacher, !teacher.isSelected());
    }


    @Override
    public void onPersonLongClick(Person person) {
        //No action
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ConversationPartyBusiness.getInstance().unselectAll();
    }

    @Override
    protected void setResult() {
        Intent intent = new Intent();

        long[] teacherIdsArray =  new long[selectedTeachers.size()];
        int i = 0;
        for(long id : selectedTeachers){
            teacherIdsArray[i] = id;
            i++;
        }

        intent.putExtra(SELECTED_TEACHER_IDS, teacherIdsArray);
        setResult(Activity.RESULT_OK, intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_selection);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new TeacherSelectionFragment(), TEACHER_SELECTION_TAG)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ConversationPartyBusiness conversationPartyBusiness = ConversationPartyBusiness.getInstance();
        for(Long teacherId :selectedTeachers){
            ConversationParty conversationParty = conversationPartyBusiness.getConversationParty(ConversationParty.TEACHER_TYPE, teacherId);
            ConversationPartyBusiness.getInstance().select(conversationParty, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ok, menu);
        return true;
    }


}
