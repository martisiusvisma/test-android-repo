package com.kindiedays.carerapp.ui.events;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindiedays.carerapp.R;
import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.pojo.Event;

import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by pleonard on 30/06/2015.
 */
public class EventsAdapter extends BaseAdapter {

    private List<Event> events;
    private Activity activity;

    @SuppressWarnings("squid:S1068")
    private EventsFragment fragment;
    @SuppressWarnings("squid:S1068")
    private boolean readOnly = true;

    private boolean pastEvents;

    public EventsAdapter(Activity activity, EventsFragment fragment, boolean pastEvents) {
        this.activity = activity;
        this.fragment = fragment;
        this.pastEvents = pastEvents;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (events == null) {
            return 0;
        }
        return events.size();
    }

    @Override
    public Event getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return events.get(position).getId();
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_event, null);
            holder = new ViewHolder();
            holder.eventDate = (TextView) convertView.findViewById(R.id.eventDate);
            holder.eventTime = (TextView) convertView.findViewById(R.id.eventTime);
            holder.eventName = (TextView) convertView.findViewById(R.id.eventName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Event event = getItem(position);
        holder.eventTime.setText(event.getStart().toString("HH:mm"));
        holder.eventName.setText(event.getName());
        LocalDate date;
        if (event.getInterval() == null) {
            date = event.getDate();
        } else {
            if (pastEvents) {
                date = EventsBusiness.getInstance().getLastOccurence(event).toLocalDate();
            } else {
                date = EventsBusiness.getInstance().getNextOccurence(event).toLocalDate();
            }

        }
        holder.eventDate.setText(date.toString("dd-MM-yyyy"));
        return convertView;
    }

    static class ViewHolder {
        TextView eventTime;
        TextView eventName;
        TextView eventDate;
    }
}
