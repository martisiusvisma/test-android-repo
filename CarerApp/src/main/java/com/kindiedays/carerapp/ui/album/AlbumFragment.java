package com.kindiedays.carerapp.ui.album;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.PictureList;

/**
 * Created by pleonard on 11/09/2015.
 */
public class AlbumFragment extends com.kindiedays.common.ui.album.AlbumFragment {

    public AlbumFragment() {
        onPictureClickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
                intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
                getActivity().startActivity(intent);
            }
        };
    }

    @Override
    public void refreshData() {
        GetPicturesForKidAsyncTask task = new GetPicturesForKidAsyncTask(ChildBusiness.getInstance().getCurrentChild().getId());
        pendingTasks.add(task);
        task.execute();
    }

    class GetPicturesForKidAsyncTask extends PicturesBusiness.GetPicturesForChildAsyncTask {

        public GetPicturesForKidAsyncTask(long childId) {
            super(childId);
        }

        @Override
        protected PictureList doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            if (!checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                galleryPictureList = pictureList.getPictures();
                thumbnailAdapter.setPictures(galleryPictureList);
            }
            progressBar.setVisibility(View.GONE);
            pendingTasks.remove(this);
        }
    }
}
