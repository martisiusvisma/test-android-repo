package com.kindiedays.carerapp.ui.messages;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.ui.MainActivity;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;

import java.util.List;

/**
 * Created by pleonard on 11/09/2015.
 */
public class ConversationsFragment extends com.kindiedays.common.ui.messages.ConversationsFragment {
    public ConversationsFragment() {
        onConversationClickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ConversationActivity.class);
                intent.putExtra(ConversationActivity.CONVERSATION_ID, id);
                getActivity().startActivity(intent);
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_createNewMessage) {
            Intent intent = new Intent(getActivity(), NewMessageActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        conversationAdapter = new ConversationAdapter(getActivity());
        if (ChildBusiness.getInstance().getChildren() == null) {
            LoadChildrenListTask loadChildrenListTask = new LoadChildrenListTask();
            pendingTasks.add(loadChildrenListTask);
            loadChildrenListTask.execute();
        } else
            conversationAdapter.setChildren(ChildBusiness.getInstance().getChildren());
        super.setOnAsynkPostExecuteListener(onAsynkPostExecuteListener);
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    private class LoadChildrenListTask extends ChildBusiness.LoadChildrenTask {

        @Override
        protected List<Child> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(List<Child> children) {
            if (!checkNetworkException(children, exception)) {
                conversationAdapter.setChildren(children);
            }
            pendingTasks.remove(this);
        }
    }

    private OnAsynkPostExecuteListener onAsynkPostExecuteListener = new OnAsynkPostExecuteListener() {
        @Override
        public void onPostExecute() {
            ((MainActivity)getActivity()).refresh();
        }
    };
}
