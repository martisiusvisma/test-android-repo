package com.kindiedays.carerapp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.CarerBusiness;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.carerapp.ui.about.AboutFragment;
import com.kindiedays.carerapp.ui.album.AlbumFragment;
import com.kindiedays.carerapp.ui.attendance.AttendanceFragment;
import com.kindiedays.carerapp.ui.childselection.ChildSelectionDialogFragment;
import com.kindiedays.carerapp.ui.dailyreport.DailyReportFragment;
import com.kindiedays.carerapp.ui.events.EventsFragment;
import com.kindiedays.carerapp.ui.journal.JournalFragment;
import com.kindiedays.carerapp.ui.messages.ConversationsFragment;
import com.kindiedays.carerapp.ui.settings.SettingsFragment;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.GroupBusiness;
import com.kindiedays.common.business.SessionBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.session.PatchSession;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Session;
import com.kindiedays.common.pushnotifications.BroadcastReceiver;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.ui.MenuAdapter;
import com.kindiedays.common.ui.meals.MealsFragment;
import com.kindiedays.common.utils.GlideUtils;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import io.fabric.sdk.android.Fabric;

import static com.kindiedays.carerapp.util.constants.FamilyAppConstants.CHANGE_LANGUAGE_TAG;

/**
 * Created by pleonard on 11/09/2015.
 */
public class MainActivity extends com.kindiedays.common.ui.MainActivity implements Observer {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int MENU_POSITION_JOURNAL = 0;
    private static final int MENU_POSITION_MESSAGES = 1;
    private static final int MENU_POSITION_ALBUM = 2;
    private static final int MENU_POSITION_EVENTS = 3;
    private static final int MENU_POSITION_MEALS = 4;
    private static final int MENU_POSITION_ATTENDANCE = 5;
    private static final int MENU_POSITION_ABOUT = 6;
    private static final int MENU_POSITION_DAILY_REPORT = 7;
    private static final int MENU_POSITION_SETTINGS = 8;

    private static final Integer[] topMenuStringIds = new Integer[]{R.string.Journal, R.string.Messages, R.string.Album, R.string.Events, R.string.Meals, R.string.CareTimes, R.string.About, R.string.Daily_Report, R.string.Settings};
    private static final Integer[] topMenuIconIds = new Integer[]{R.raw.bug_wbg, R.raw.comments_wbg, R.raw.image_wbg, R.raw.morph_star_wbg, R.raw.apple_wbg, R.raw.calendar_wbg, R.raw.user_wbg, R.raw.morph_bar_chart_wbg, R.raw.gears_wbg};
    private static final Integer[] topMenuSelectedIconIds = new Integer[]{R.raw.bug_bbg, R.raw.comments_bbg, R.raw.image_bbg, R.raw.morph_star_bbg, R.raw.apple_bbg, R.raw.calendar_bbg, R.raw.user_bbg, R.raw.morph_bar_chart_bbg, R.raw.gears_bbg};
    private static final Boolean[] topMenuEnabled =
            new Boolean[]{true, MenuItemsStates.MESSAGES_STATE.getState(),
                    MenuItemsStates.CAMERA_STATE.getState(), MenuItemsStates.EVENTS_STATE.getState(),
                    MenuItemsStates.MEALS_STATE.getState(), MenuItemsStates.CALENDAR_STATE.getState(), true,
                    MenuItemsStates.DAILY_REPORT_STATE.getState(), true};

    private Integer[] topMenuCounterIds = new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0}; //counter left menu

    private static final String JOURNAL_TAG = "journal";
    private static final String MESSAGES_TAG = "messages";
    private static final String ALBUM_TAG = "album";
    private static final String EVENTS_TAG = "events";
    private static final String MEALS_TAG = "meals";
    private static final String ATTENDANCE_TAG = "attendance";
    private static final String ABOUT_TAG = "about";
    private static final String DAILY_REPORT_TAG = "daily_report";
    private static final String SETTINGS_TAG = "settings";

    private TextView childName;
    private ImageView childPicture;

    private boolean isDREnabledInGroup;
    private boolean isDREnabledInKindergarten;
    private int numberOfDailyReportTasks = 2;
    private boolean onSaveInstanceStateCalled;

    private static final String CHILD_SELECTION_DIALOG_TAG = "childSelectionDialog";

    View.OnClickListener onSwitchChildClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switchChildren();
        }
    };

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Webservice.isAuthorizationTokenInitialized()) {
                refresh();
            }
        }
    };

    public MainActivity() {
        super();
        fragmentTags = Arrays.asList(JOURNAL_TAG, MESSAGES_TAG, ALBUM_TAG, EVENTS_TAG, MEALS_TAG,
                ATTENDANCE_TAG, ABOUT_TAG, DAILY_REPORT_TAG, SETTINGS_TAG);
    }

    private void switchChildren() {
        onSaveInstanceStateCalled = false;
        PatchSessionTask task = new PatchSessionTask();
        task.execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onSaveInstanceStateCalled = false;
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menu = (ListView) findViewById(R.id.menuList);
        bottomMenu = (ListView) findViewById(R.id.bottomMenu);
        navigationMenu = (RelativeLayout) findViewById(R.id.left_drawer);
        childName = (TextView) findViewById(R.id.childName);
        childPicture = (ImageView) findViewById(R.id.childPicture);
        ImageView switchChild = (ImageView) findViewById(R.id.switchChildButton);
        switchChild.setOnClickListener(onSwitchChildClickListener);
        childName.setOnClickListener(onSwitchChildClickListener);
        childPicture.setOnClickListener(onSwitchChildClickListener);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        onCreateInitMenu();

        Intent intent = getIntent();
        String tag = intent.getStringExtra(CHANGE_LANGUAGE_TAG);
        if (!TextUtils.isEmpty(tag)) {
            openPage(MENU_POSITION_SETTINGS, true);
            setJustCreated(false);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE);
        if (!sharedPreferences.contains(com.kindiedays.common.ui.MainActivity.NOTIFICATIONS)){
            SharedPreferences.Editor editor = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE).edit();
            editor.putBoolean(com.kindiedays.common.ui.MainActivity.NOTIFICATIONS, true).apply();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        ChildBusiness.getInstance().addObserver(this);
        if (Webservice.isAuthorizationTokenInitialized()) {
            if (ChildBusiness.getInstance().getCurrentChild() == null) {
                if (getSupportFragmentManager().findFragmentByTag(CHILD_SELECTION_DIALOG_TAG) == null) {
                    ChildSelectionDialogFragment dialog = new ChildSelectionDialogFragment();
                    dialog.show(getSupportFragmentManager(), CHILD_SELECTION_DIALOG_TAG);
                }
            } else {
                initDailyReport();
            }
        }
    }

    public void initDailyReport() {
        GroupTask groupTask = new GroupTask();
        groupTask.execute();
        pendingTasks.add(groupTask);
        KindergartenTask task = new KindergartenTask();
        task.execute();
        pendingTasks.add(task);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("message_notification"));
        super.onResume();
        if (ChildBusiness.getInstance().getCurrentChild() != null){
            updateChild();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void refreshData() {
        refresh();
    }

    @Override
    public void onPersonSelected() {
        refresh();
        initDailyReport();
    }

    @Override
    public void onStop() {
        super.onStop();
        ChildBusiness.getInstance().deleteObserver(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        onSaveInstanceStateCalled = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int initMenu() {
        menu.setAdapter(new MenuAdapter(this, R.layout.item_menu, topMenuStringIds, topMenuIconIds, topMenuSelectedIconIds, topMenuCounterIds, topMenuEnabled));
        menu.setOnItemClickListener(onMenuClickListener);
        return topMenuStringIds.length;
    }

    @Override
    protected int initBottomMenu() {
        //No bottom menu for now
        return 0;
    }

    @Override
    protected void openPage(int pageIndex, boolean addToBackStack) {
        MainActivityFragment fragment = null;
        switch (pageIndex) {
        case MENU_POSITION_JOURNAL:
            fragment = new JournalFragment();
            break;
        case MENU_POSITION_MESSAGES:
            fragment = new ConversationsFragment();
            break;
        case MENU_POSITION_ALBUM:
            fragment = new AlbumFragment();
            break;
        case MENU_POSITION_EVENTS:
            fragment = new EventsFragment();
            break;
        case MENU_POSITION_MEALS:
            fragment = new MealsFragment();
            break;
        case MENU_POSITION_ATTENDANCE:
            fragment = new AttendanceFragment();
            break;
        case MENU_POSITION_ABOUT:
            fragment = new AboutFragment();
            break;
        case MENU_POSITION_DAILY_REPORT:
            fragment = new DailyReportFragment();
            break;
        case MENU_POSITION_SETTINGS:
            fragment = new SettingsFragment();
            break;
        default:
            break;
        }
        menu.setItemChecked(pageIndex, true);
        if (addToBackStack) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment, fragmentTags.get(pageIndex))
                    .addToBackStack(fragmentTags.get(pageIndex))
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment, fragmentTags.get(pageIndex))
                    .commit();
        }
    }

    public void updateChild() {
        Child child = ChildBusiness.getInstance().getCurrentChild();
        if (child != null) {
            CarerBusiness.getInstance().setCarerForCurrentChild(null);
            getSupportActionBar().setTitle(child.getName());
            childName.setText(child.getName());
            Glide.with(this).load(child.getProfilePictureUrl())
                    .bitmapTransform(GlideUtils.roundTransformation).into(childPicture);
            mDrawerLayout.closeDrawer(navigationMenu);
            //Refresh data of current fragment
            String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            ((MainActivityFragment) getSupportFragmentManager().findFragmentByTag(tag)).refreshData();
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof ChildBusiness && data != null && data instanceof Child) {
            updateChild();
        }
    }

    private class GroupTask extends GroupBusiness.LoadGroupsTask {
        @Override
        protected void onPostExecute(List<Group> groups) {
            super.onPostExecute(groups);
            Group group = GroupBusiness.getInstance().getGroup(ChildBusiness.getInstance().getCurrentChild().getGroupId());
            if (!checkNetworkException(groups, exception) && group != null) {
                isDREnabledInGroup = group.isDailyReportEnabled();
            }
            if (--numberOfDailyReportTasks == 0) {
                topMenuEnabled[MENU_POSITION_DAILY_REPORT] = isDREnabledInKindergarten && isDREnabledInGroup;
                ((MenuAdapter) menu.getAdapter()).setEnabled(topMenuEnabled);
            }
            pendingTasks.remove(this);
        }
    }

    public void refresh() {
        if (Webservice.isAuthorizationTokenInitialized()) {
            SessionTask task = new SessionTask();
            pendingTasks.add(task);
            task.execute();
        } else {
            switchChildren();
        }
    }

    @Override
    public void refreshMenuStates() {
        topMenuEnabled[MENU_POSITION_MESSAGES] = MenuItemsStates.MESSAGES_STATE.getState();
        topMenuEnabled[MENU_POSITION_ALBUM] = MenuItemsStates.CAMERA_STATE.getState();
        topMenuEnabled[MENU_POSITION_EVENTS] = MenuItemsStates.EVENTS_STATE.getState();
        topMenuEnabled[MENU_POSITION_MEALS] = MenuItemsStates.MEALS_STATE.getState();
        topMenuEnabled[MENU_POSITION_ATTENDANCE] = MenuItemsStates.CALENDAR_STATE.getState();
        isDREnabledInKindergarten = MenuItemsStates.DAILY_REPORT_STATE.getState();

        if (--numberOfDailyReportTasks == 0) {
            topMenuEnabled[MENU_POSITION_DAILY_REPORT] = isDREnabledInKindergarten && isDREnabledInGroup;
            ((MenuAdapter) menu.getAdapter()).setEnabled(topMenuEnabled);
        }
    }

    @Override
    public int getLockedMenuMessageRes() {
        return R.string.locked_carer_message;
    }

    private class SessionTask extends SessionBusiness.GetSessionTask {
        @Override
        protected void onPostExecute(Session item) {
            if (!checkNetworkException(item, exception)) {
                super.onPostExecute(item);
                if (item != null) {
                    CarerTask carerInfo = new CarerTask(item.getSelfLink());
                    pendingTasks.add(carerInfo);
                    carerInfo.execute();
                }
            }
            pendingTasks.remove(this);
        }
    }


    private class CarerTask extends CarerBusiness.LoadCurrentCarerDetails {
        CarerTask(String link) {
            super(link);
        }

        @Override
        protected void onPostExecute(ChildCarer carer) {
            if (!checkNetworkException(carer, exception)) {
                super.onPostExecute(carer);
                Child activeChild = ChildBusiness.getInstance().getCurrentChild();
                if (carer != null && activeChild != null) {
                    topMenuCounterIds[1] = carer.getUnreadMessagesActiveChild((int) activeChild.getId());
                    topMenuCounterIds[3] = carer.getUnreadEventsActiveChild((int) activeChild.getId());
                }
                initMenu();
            }
            pendingTasks.remove(this);
        }
    }


    private class PatchSessionTask extends NetworkAsyncTask<Boolean> {

        @Override
        protected Boolean doNetworkTask() throws Exception {
            try {
                PatchSession.patchKindergarden();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.data));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (onSaveInstanceStateCalled) {
                return;
            }
            if (success != null && success) {
                ChildSelectionDialogFragment prevDialog = (ChildSelectionDialogFragment) getSupportFragmentManager().findFragmentByTag(CHILD_SELECTION_DIALOG_TAG);
                ChildSelectionDialogFragment selectionDialogFragment = new ChildSelectionDialogFragment();
                selectionDialogFragment.show(getSupportFragmentManager(), CHILD_SELECTION_DIALOG_TAG);
                if (prevDialog != null) {
                    prevDialog.dismiss();
                }
            } else {
                if (exception != null && exception.isLocalizedMessage()) {
                    Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Internal application error", Toast.LENGTH_LONG).show();
                }
            }
            pendingTasks.remove(this);
        }
    }
}
