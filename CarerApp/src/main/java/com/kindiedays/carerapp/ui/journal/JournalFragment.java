package com.kindiedays.carerapp.ui.journal;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.carerapp.R;
import com.kindiedays.carerapp.business.ChildBusiness;
import com.kindiedays.common.business.JournalBusiness;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.business.PlaceBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildJournalList;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.ui.journal.JournalPostDisplayer;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;
import java.util.TreeMap;

/**
 * Created by pleonard on 11/09/2015.
 */
public class JournalFragment extends MainActivityFragment implements JournalPostDisplayer.JournalPostSource {

    private TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPosts;
    private List<Place> places;
    private JournalPostDisplayer journalPostDisplayer = new JournalPostDisplayer();

    @Override
    public void onResume() {
        super.onResume();
        TextViewCustom title = (TextViewCustom) getActivity().findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.Journal));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        journalPostDisplayer.setChild(ChildBusiness.getInstance().getCurrentChild());
        View v = inflater.inflate(R.layout.fragment_journal, null);
        journalPostDisplayer.init(v, R.id.journalPostList, R.id.progress, this);

        if (journalPosts != null) {
            journalPostDisplayer.setJournalPosts(journalPosts);
        }
        if (places != null) {
            journalPostDisplayer.setPlaces(places);
        }

        return v;
    }

    @Override
    public void refreshData() {
        Child currentChild = ChildBusiness.getInstance().getCurrentChild();
        if (currentChild != null) {
            PicturesBusiness.GetPicturesForChildAsyncTask getPicturesForChildAsyncTask
                    = new PicturesBusiness.GetPicturesForChildAsyncTask(currentChild.getId());
            getPicturesForChildAsyncTask.execute();
            journalPostDisplayer.setChild(ChildBusiness.getInstance().getCurrentChild());
            journalPostDisplayer.nullPreviousTotal();
            GetJournalTask task = new GetJournalTask(currentChild.getId());
            pendingTasks.add(task);
            task.execute();
        }

        loadPlaces();
    }

    @Override
    public void requestMoreJournalPosts() {
        GetMoreJournalPostsTask task = new GetMoreJournalPostsTask();
        pendingTasks.add(task);
        task.execute();
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @SuppressWarnings("squid:S1319")
    public void setJournalPosts(TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPosts) {
        this.journalPosts = journalPosts;
        journalPostDisplayer.setJournalPosts(journalPosts);
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
        journalPostDisplayer.setPlaces(places);
    }

    private void loadPlaces() {
        if (this.places == null) {
            List<Place> loadedPlaces = PlaceBusiness.getInstance().getContainers(false);
            if (loadedPlaces != null) {
                setPlaces(loadedPlaces);
            } else {
                GetPlacesTask task = new GetPlacesTask();
                pendingTasks.add(task);
                task.execute();
            }
        } else {
            setPlaces(places);
        }
    }

    private class GetMoreJournalPostsTask extends JournalBusiness.GetMoreChildJournalPostsAsyncTask {
        GetMoreJournalPostsTask() {
            super(JournalBusiness.BEFORE);
        }

        @Override
        protected void onPostExecute(ChildJournalList childJournalList) {
            if (!checkNetworkException(childJournalList, exception)) {
                super.onPostExecute(childJournalList);
                setJournalPosts(JournalBusiness.getInstance().getJournalPostMap());
                loadPlaces();
            }
            pendingTasks.remove(this);
        }
    }

    private class GetJournalTask extends JournalBusiness.GetChildJournalPostsAsyncTask {
        GetJournalTask(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(ChildJournalList journalPosts) {
            if (!checkNetworkException(journalPosts, exception)) {
                super.onPostExecute(journalPosts);
                setJournalPosts(JournalBusiness.getInstance().getJournalPostMap());
                loadPlaces();
            }
            pendingTasks.remove(this);
        }
    }


    private class GetPlacesTask extends PlaceBusiness.LoadPlacesTask {
        @Override
        protected void onPostExecute(List<Place> places) {
            if (!checkNetworkException(places, exception)) {
                super.onPostExecute(places);
                setPlaces(places);
            }
            pendingTasks.remove(this);
        }
    }
}
