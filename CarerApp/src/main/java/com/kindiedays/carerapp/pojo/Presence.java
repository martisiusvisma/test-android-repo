package com.kindiedays.carerapp.pojo;

import com.kindiedays.common.pojo.ServerObject;

import org.joda.time.LocalDate;

/**
 * Created by pleonard on 21/12/2015.
 */
public class Presence extends ServerObject {

    private long childId;
    private LocalDate start;
    private LocalDate end;
    private String status;

    public static final String ABSENT = "ABSENT";
    public static final String PRESENT = "PRESENT";


    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
