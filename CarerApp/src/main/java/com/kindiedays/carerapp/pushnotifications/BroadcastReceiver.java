package com.kindiedays.carerapp.pushnotifications;

import com.kindiedays.carerapp.ui.MainActivity;

/**
 * Created by pleonard on 17/09/2015.
 */
public class BroadcastReceiver extends com.kindiedays.common.pushnotifications.BroadcastReceiver {

    public BroadcastReceiver(){
        super();
        clazz = MainActivity.class;
    }
}
