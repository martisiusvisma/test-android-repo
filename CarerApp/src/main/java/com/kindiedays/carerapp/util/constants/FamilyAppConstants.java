package com.kindiedays.carerapp.util.constants;


public class FamilyAppConstants {
    public static final String CHANGE_LANGUAGE_TAG = "change_lang";

    // child preferences
    public static final String CHILD_ID = "child_id";
    public static final String KINDERGARTEN_ID = "kindergarten_id";

    private FamilyAppConstants() {
        //no instance
    }
}
