package com.kindiedays.carerapp.util;


import android.support.annotation.StringRes;

import com.kindiedays.common.pojo.EventInvitation;

public class EventUtils {
    private EventUtils() {
        //no instance
    }

    @StringRes
    public static int getEventStateName(EventInvitation.InvitationState state) {
        switch (state) {
        case INVITED:
            return com.kindiedays.common.R.string.Invited;

        case CONFIRMED:
            return com.kindiedays.common.R.string.Confirmed;

        case DECLINED:
            return com.kindiedays.common.R.string.Declined;

        default:
            return com.kindiedays.common.R.string.Invited;
        }
    }
}
