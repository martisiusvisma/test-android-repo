package com.kindiedays.carerapp.business;

import com.kindiedays.carerapp.network.presence.DeletePresence;
import com.kindiedays.carerapp.network.presence.GetPresences;
import com.kindiedays.carerapp.network.presence.PostPresences;
import com.kindiedays.carerapp.pojo.Presence;
import com.kindiedays.common.network.NetworkAsyncTask;

import java.util.List;

/**
 * Created by pleonard on 21/12/2015.
 */
public class PresenceBusiness {

    private PresenceBusiness() {
        //no instance
    }

    public static class GetPresencesAsyncTask extends NetworkAsyncTask<List<Presence>>{

        String startDate;
        String endDate;
        long childId;

        public GetPresencesAsyncTask(String startDate, String endDate, long childId){
            this.startDate = startDate;
            this.endDate = endDate;
            this.childId = childId;
        }
        @Override
        protected List<Presence> doNetworkTask() throws Exception {
            return GetPresences.getPresences(startDate, endDate, childId);
        }
    }

    public static class PostPresenceAsyncTask extends NetworkAsyncTask<Presence>{

        Presence presence;

        public PostPresenceAsyncTask(Presence presence){
            this.presence = presence;
        }

        @Override
        protected Presence doNetworkTask() throws Exception {
            return PostPresences.postPresence(presence);
        }
    }


    public static class DeletePresenceAsyncTask extends NetworkAsyncTask<Void>{

        long presenceId;

        public DeletePresenceAsyncTask(long presenceId){
            this.presenceId = presenceId;
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            DeletePresence.delete(presenceId);
            return null;
        }
    }

}
