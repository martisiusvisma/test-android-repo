package com.kindiedays.carerapp.business;

import android.util.Log;

import com.kindiedays.carerapp.network.carer.GetCarer;
import com.kindiedays.carerapp.network.carer.PutCarer;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.carers.GetChildCarer;
import com.kindiedays.common.network.session.GetSession;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.Session;

import java.util.List;

/**
 * Created by pleonard on 25/01/2016.
 */
public class CarerBusiness {

    private static CarerBusiness instance;
    private ChildCarer carerForCurrentChild;

    private CarerBusiness(){}

    public static CarerBusiness getInstance(){
        if(instance == null){
            instance = new CarerBusiness();
        }
        return instance;
    }

    public ChildCarer getCarerForCurrentChild(){
        return carerForCurrentChild;
    }

    public void setCarerForCurrentChild(ChildCarer carerForCurrentChild) {
        this.carerForCurrentChild = carerForCurrentChild;
    }

    public static class GetCarerTask extends NetworkAsyncTask<ChildCarer>{

        long carerId;

        public GetCarerTask(long id){
            this.carerId = id;
        }

        @Override
        protected ChildCarer doNetworkTask() throws Exception {
            return GetCarer.getCarer(carerId);
        }
    }

    public static class PutCarerTask extends NetworkAsyncTask<ChildCarer>{

        ChildCarer carer;

        public PutCarerTask(ChildCarer carer){
            this.carer = carer;
        }

        @Override
        protected ChildCarer doNetworkTask() throws Exception {
            return PutCarer.updateCarer(carer);
        }
    }

    public static class LoadCurrentCarerForChild extends NetworkAsyncTask<ChildCarer> {

        protected long childId;

        public LoadCurrentCarerForChild(long childId){
            this.childId = childId;
        }

        @Override
        protected ChildCarer doNetworkTask() throws Exception {
            Log.d(LoadCurrentCarerForChild.class.getName(), "Starting get carer background task");
            if(CarerBusiness.getInstance().carerForCurrentChild != null){
                return CarerBusiness.getInstance().carerForCurrentChild;
            }
            else{
                Session session = GetSession.getSession();
                List<ChildCarer> carers = GetChildCarer.getCarers(childId);
                for(ChildCarer carer : carers){
                    if(carer.getEmail().equals(session.getEmail())){
                        CarerBusiness.getInstance().carerForCurrentChild = carer;
                        return carer;
                    }
                }
            }

            return null;
        }
    }

    public static class LoadCurrentCarerDetails extends NetworkAsyncTask<ChildCarer> {
        private String mLink;
        public LoadCurrentCarerDetails(String link) {
            this.mLink = link;
        }

        @Override
        protected ChildCarer doNetworkTask() throws Exception {
            return GetCarer.getCurrentCarer(mLink);
        }
    }
}
