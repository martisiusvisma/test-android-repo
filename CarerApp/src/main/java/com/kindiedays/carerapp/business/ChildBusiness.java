package com.kindiedays.carerapp.business;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.kindiedays.carerapp.CarerApp;
import com.kindiedays.carerapp.network.child.GetChildTotalInfo;
import com.kindiedays.carerapp.network.child.PatchChild;
import com.kindiedays.carerapp.network.child.PutChildProfile;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.children.GetChildProfile;
import com.kindiedays.common.network.children.GetChildren;
import com.kindiedays.common.network.fileupload.PostPendingFiles;
import com.kindiedays.common.network.fileupload.PutFile;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildProfile;
import com.kindiedays.common.pojo.PendingFile;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Set;

import static com.kindiedays.carerapp.util.constants.FamilyAppConstants.CHILD_ID;
import static com.kindiedays.carerapp.util.constants.FamilyAppConstants.KINDERGARTEN_ID;

/**
 * Created by pleonard on 14/09/2015.
 */
public class ChildBusiness extends Observable {

    private Child currentChild;
    private List<Child> children;
    private Set<AsyncTask> pendingTasks = new HashSet<>();
    private SharedPreferences sharedPreferences;

    private static ChildBusiness instance;

    private ChildBusiness() {
        sharedPreferences = CarerApp.getApp().getCarerSharedPreferences();
    }

    public static ChildBusiness getInstance() {
        if (instance == null) {
            instance = new ChildBusiness();
        }
        return instance;
    }
    public Child getCurrentChild() {
        if (currentChild == null && sharedPreferences.contains(CHILD_ID)) {
            currentChild = new Child();
            currentChild.setId(sharedPreferences.getLong(CHILD_ID, -1));
            currentChild.setKindergartenId(sharedPreferences.getLong(KINDERGARTEN_ID, -1));
            GetChildInfo task = new GetChildInfo(currentChild.getId());
            pendingTasks.add(task);
            task.execute();
        }
        return currentChild;
    }

    public void setCurrentChild(Child currentChild) {
        if (this.currentChild != currentChild) {
            this.currentChild = currentChild;
            if (currentChild != null) {
                sharedPreferences.edit().putLong(CHILD_ID, currentChild.getId()).apply();
                sharedPreferences.edit().putLong(KINDERGARTEN_ID, currentChild.getKindergartenId()).apply();
            } else sharedPreferences.edit().remove(CHILD_ID).apply();

            getInstance().setChanged();
            getInstance().notifyObservers(currentChild);
        }
    }

    public List<Child> getChildren() {
        return children;
    }

    public static class LoadChildrenTask extends NetworkAsyncTask<List<Child>> {

        @Override
        protected void onPostExecute(List<Child> children) {
            getInstance().setChanged();
            getInstance().notifyObservers(children);
        }

        @Override
        protected List<Child> doNetworkTask() throws Exception {
            getInstance().children = GetChildren.getChildren();
            return getInstance().children;
        }
    }

    public static class GetChildInfo extends NetworkAsyncTask<Child> {
        private Long id;

        public GetChildInfo(Long id) {
            this.id = id;
        }

        @Override
        protected Child doNetworkTask() throws Exception {
            return GetChildTotalInfo.getChild(id);
        }

        @Override
        protected void onPostExecute(Child child) {
            if (child != null)
                ChildBusiness.getInstance().setCurrentChild(child);
            getInstance().pendingTasks.remove(this);
        }
    }

    public static class LoadChildProfile extends NetworkAsyncTask<ChildProfile> {

        private long id;

        public LoadChildProfile(long id) {
            this.id = id;
        }

        @Override
        protected ChildProfile doNetworkTask() throws Exception {
            return GetChildProfile.getChild(id);
        }
    }

    public static class SaveChildProfile extends NetworkAsyncTask<ChildProfile> {

        private ChildProfile childProfile;

        public SaveChildProfile(ChildProfile childProfile) {
            this.childProfile = childProfile;
        }

        @Override
        protected ChildProfile doNetworkTask() throws Exception {
            return PutChildProfile.putProfile(childProfile);
        }
    }

    public static class PatchChildPicture extends NetworkAsyncTask<Child> {

        private long childId;
        private File file;

        public PatchChildPicture(long childId, File file) {
            this.childId = childId;
            this.file = file;
        }

        @Override
        protected Child doNetworkTask() throws Exception {
            PendingFile pendingFile = PostPendingFiles.post();
            PutFile.uploadFile(file, pendingFile.getUploadUrl());
            return PatchChild.changePicture(pendingFile.getId(), childId);
        }
    }
}
