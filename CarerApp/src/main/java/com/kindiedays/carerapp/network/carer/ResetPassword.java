package com.kindiedays.carerapp.network.carer;

import android.content.Context;
import android.content.SharedPreferences;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 23/12/2015.
 */
public class ResetPassword extends Webservice<Void> {

    public static void reset(String email, boolean isProd) throws CommonKindieException {
        ResetPassword instance = new ResetPassword();
        JSONObject json = new JSONObject();
        try {
            json.put("email", email);
            json.put("principalType", "CARER");
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }

        instance.postObjects("reset-password/send-link", json
                , isProd ? PROD_CHANGE_PASS_ENDPOINT : STAGING_CHANGE_PASS_ENDPOINT);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
