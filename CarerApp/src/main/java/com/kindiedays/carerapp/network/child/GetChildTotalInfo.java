package com.kindiedays.carerapp.network.child;


import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetChildTotalInfo extends Webservice<Child> {

    public static Child getChild(Long id) throws CommonKindieException {
        GetChildTotalInfo getChildren = new GetChildTotalInfo();
        return getChildren.getObjects("children/" + id);
    }

    @Override
    public Child parseJsonFromServer(JSONObject jsonKid) throws JSONException {
        Child child = new Child();
        child.setId(jsonKid.getInt("id"));
        child.setGroupId(jsonKid.getInt("group"));
        child.setName(jsonKid.getString("name"));
        child.setNickname(jsonKid.optString("nickname", null));
        child.setKindergartenId(jsonKid.getInt("kindergarten"));

        JSONObject jsonPicture = jsonKid.getJSONObject("profilePicture");
        JSONArray jsonThumbnails = jsonPicture.getJSONArray("thumbnails");
        for (int j = jsonThumbnails.length() - 1; j >= 0; j--) {
            JSONObject thumbnail = jsonThumbnails.getJSONObject(j);
            if (thumbnail.getLong("width") == 256) {
                child.setProfilePictureUrl(thumbnail.getString("url"));
                break;
            }
        }

        JSONArray jsonCarers = jsonKid.getJSONArray("carersIds");
        List<Long> carerIds = new ArrayList<>(jsonCarers.length());
        for (int i = 0; i < jsonCarers.length(); i++) {
            carerIds.add(jsonCarers.getLong(i));
        }
        child.setCarerIds(carerIds);
        return child;
    }
}
