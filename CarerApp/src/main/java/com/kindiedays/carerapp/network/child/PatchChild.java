package com.kindiedays.carerapp.network.child;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.children.ChildJsonParser;
import com.kindiedays.common.pojo.Child;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 07/01/2016.
 */
public class PatchChild extends Webservice<Child> {

    public static Child changePicture(String pendingFileId, long childId) throws CommonKindieException {
        PatchChild instance = new PatchChild();
        JSONObject json = new JSONObject();
        JSONObject profilePicture = new JSONObject();
        try {
            profilePicture.put("pendingFileId", pendingFileId);
            profilePicture.put("format", "JPEG");
            json.put("profilePicture", profilePicture);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return instance.patchObjects("children/" + childId, json);
    }

    @Override
    public Child parseJsonFromServer(JSONObject json) throws JSONException {
        return ChildJsonParser.parseChild(json);
    }
}
