package com.kindiedays.carerapp.network.presence;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.HttpResponse;

/**
 * Created by pleonard on 23/12/2015.
 */
public class DeletePresence extends Webservice<Void> {

    public static void delete(long presenceId) throws CommonKindieException {
        DeletePresence instance = new DeletePresence();
        instance.deleteObjects("presences/" + presenceId);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }

    protected void responseForbidden(HttpResponse response) throws IOException {
        connectionError(response, true, true);
    }
}
