package com.kindiedays.carerapp.network.child;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.children.ChildJsonParser;
import com.kindiedays.common.pojo.ChildProfile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 18/12/2015.
 */
public class PutChildProfile extends Webservice<ChildProfile> {

    public static ChildProfile putProfile(ChildProfile childProfile) throws CommonKindieException {
        PutChildProfile instance = new PutChildProfile();
        JSONObject json = new JSONObject();
        try {
            if (childProfile.getDropoffTime() != null) {
                json.put("dropoffTime", childProfile.getDropoffTime().toString("HH:mm:ss"));
            }
            if (childProfile.getPickupTime() != null) {
                json.put("pickupTime", childProfile.getPickupTime().toString("HH:mm:ss"));
            }
            json.put("photoConsent", childProfile.isPhotoConsent());
            if (childProfile.getDateOfBirth() != null) {
                json.put("dateOfBirth", childProfile.getDateOfBirth().toString("YYYY-MM-dd"));
            }
            json.put("socialSecurityNumber", childProfile.getSocialSecurityNumber());
            json.put("streetAddress", childProfile.getStreetAddress());
            json.put("zipCode", childProfile.getZipCode());
            json.put("city", childProfile.getCity());
            json.put("nativeLanguage", childProfile.getNativeLanguage());
            json.put("allergies", childProfile.getAllergies());
            json.put("diet", childProfile.getDiet());
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return instance.putObjects("children/" + childProfile.getChildId() + "/profile", json);
    }

    @Override
    public ChildProfile parseJsonFromServer(JSONObject json) throws JSONException {
        return ChildJsonParser.parseProfile(json);
    }
}
