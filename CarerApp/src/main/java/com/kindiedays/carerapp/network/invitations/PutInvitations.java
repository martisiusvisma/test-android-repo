package com.kindiedays.carerapp.network.invitations;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.EventInvitation;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 18/09/2015.
 */
public class PutInvitations extends Webservice<Void> {

    public static void putInvitation(long eventId, long childId, EventInvitation.InvitationState state) throws CommonKindieException {
        PutInvitations instance = new PutInvitations();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("state", state.toString());
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        instance.putObjects("events/" + eventId + "/invitations/" + childId, jsonObject);
    }


    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
