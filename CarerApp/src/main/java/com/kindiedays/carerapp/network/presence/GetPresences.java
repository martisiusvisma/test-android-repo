package com.kindiedays.carerapp.network.presence;

import com.kindiedays.carerapp.pojo.Presence;
import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 21/12/2015.
 */
public class GetPresences extends Webservice<List<Presence>> {

    public static List<Presence> getPresences(String startDate, String endDate, long childId) throws CommonKindieException {
        GetPresences instance = new GetPresences();
        return instance.getObjects("children/" + childId + "/presences?start=" + startDate + "&end=" + endDate);
    }

    @Override
    public List<Presence> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Presence> presences = new ArrayList<>();
        JSONArray presenceArray = json.getJSONArray("presences");
        for (int i = 0; i < presenceArray.length(); i++) {
            JSONObject object = presenceArray.getJSONObject(i);
            presences.add(PresenceParser.parsePresence(object));
        }
        return presences;
    }
}
