package com.kindiedays.carerapp.network.presence;

import com.kindiedays.carerapp.pojo.Presence;

import org.joda.time.LocalDate;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/12/2015.
 */
public class PresenceParser {

    private PresenceParser() {
        //no instance
    }

    public static Presence parsePresence(JSONObject object) throws JSONException {
        Presence presence = new Presence();
        presence.setId(object.getLong("id"));
        presence.setChildId(object.getLong("child"));
        presence.setStart(LocalDate.parse(object.getString("start")));
        presence.setEnd(LocalDate.parse(object.getString("end")));
        presence.setStatus(object.getString("status"));
        return presence;
    }
}
