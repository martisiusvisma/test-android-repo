package com.kindiedays.carerapp.network.carer;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.carers.CarerParser;
import com.kindiedays.common.pojo.ChildCarer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 18/12/2015.
 */
public class GetCarer extends Webservice<ChildCarer> {

    public static ChildCarer getCarer(long id) throws CommonKindieException {
        GetCarer getCarer = new GetCarer();
        return getCarer.getObjects("carers/" + id);
    }

    public static ChildCarer getCurrentCarer(String selfUrl) throws CommonKindieException {
        GetCarer getCarer = new GetCarer();
        return getCarer.getObjectsFromUrl(selfUrl);
    }


    @Override
    public ChildCarer parseJsonFromServer(JSONObject json) throws JSONException {
        return CarerParser.parseSingleCarer(json);
    }
}
