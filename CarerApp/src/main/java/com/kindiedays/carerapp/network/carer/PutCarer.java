package com.kindiedays.carerapp.network.carer;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.carers.CarerParser;
import com.kindiedays.common.pojo.ChildCarer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 25/01/2016.
 */
public class PutCarer extends Webservice<ChildCarer> {

    public static ChildCarer updateCarer(ChildCarer carer) throws CommonKindieException {
        PutCarer instance = new PutCarer();
        JSONObject json = new JSONObject();
        try {
            json.put("name", carer.getName());
            json.put("email", carer.getEmail());
            json.put("occupation", carer.getOccupation() == null || "".equals(carer.getOccupation()) ? JSONObject.NULL : carer.getOccupation());
            json.put("placeOfWork", carer.getPlaceOfWork() == null || "".equals(carer.getPlaceOfWork()) ? JSONObject.NULL : carer.getPlaceOfWork());
            json.put("workPhoneNumber", carer.getWorkPhoneNumber() == null || "".equals(carer.getWorkPhoneNumber()) ? JSONObject.NULL : carer.getWorkPhoneNumber());
            json.put("homePhoneNumber", carer.getHomePhoneNumber() == null || "".equals(carer.getHomePhoneNumber()) ? JSONObject.NULL : carer.getHomePhoneNumber());
            json.put("mobilePhoneNumber", carer.getMobilePhoneNumber() == null || "".equals(carer.getMobilePhoneNumber()) ? JSONObject.NULL : carer.getMobilePhoneNumber());
            json.put("nativeLanguage", carer.getNativeLanguage() == null || "".equals(carer.getNativeLanguage()) ? JSONObject.NULL : carer.getNativeLanguage());
            json.put("contactDetailsPublic", carer.isContactDetailsPublic());
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return instance.putObjects("carers/" + carer.getId(), json);
    }

    @Override
    public ChildCarer parseJsonFromServer(JSONObject json) throws JSONException {
        return CarerParser.parseSingleCarer(json);
    }
}
