package com.kindiedays.carerapp.network.presence;

import com.kindiedays.carerapp.pojo.Presence;
import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/12/2015.
 */
public class PostPresences extends Webservice<Presence> {

    public static Presence postPresence(Presence presence) throws CommonKindieException {
        PostPresences instance = new PostPresences();
        JSONObject json = new JSONObject();
        try {
            json.put("id", presence.getId());
            json.put("child", presence.getChildId());
            json.put("start", presence.getStart().toString("YYYY-MM-dd"));
            json.put("end", presence.getStart().toString("YYYY-MM-dd"));
            json.put("status", presence.getStatus());
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return instance.postObjects("presences", json);
    }

    @Override
    public Presence parseJsonFromServer(JSONObject json) throws JSONException {
        return PresenceParser.parsePresence(json);
    }
}
