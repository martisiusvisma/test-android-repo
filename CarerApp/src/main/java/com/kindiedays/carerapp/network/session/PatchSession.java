package com.kindiedays.carerapp.network.session;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.HttpResponse;

/**
 * Created by pleonard on 14/09/2015.
 */
public class PatchSession extends Webservice<Void> {

    public static void patch(long kindergartenId) throws CommonKindieException {
        PatchSession patchSession = new PatchSession();
        JSONObject json = new JSONObject();
        try {
            json.put("kindergarten", kindergartenId);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        patchSession.patchObjects("sessions/current", json, authEndpoint);
    }

    protected void responseUnauthorized(HttpResponse response) throws IOException {
        connectionError(response, true, true);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
