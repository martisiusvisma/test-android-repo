package com.kindiedays.teacherapp.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.GetPaginatedResource;
import com.kindiedays.teacherapp.pojo.Notification;
import com.kindiedays.teacherapp.pojo.NotificationList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 16/07/2015.
 */
public class GetNotifications extends GetPaginatedResource<NotificationList> {

    public static NotificationList getNotifications(long childId) throws CommonKindieException {
        GetNotifications getNotifications = new GetNotifications();
        return getNotifications.getObjects("children/" + childId + "/notifications");
    }

    public static NotificationList getNotifications(String url) throws CommonKindieException {
        GetNotifications getNotifications = new GetNotifications();
        return getNotifications.getObjectsFromUrl(url);
    }

    @Override
    public NotificationList parseJsonFromServer(JSONObject json) throws JSONException {
        NotificationList notificationList = new NotificationList();
        JSONArray array = json.getJSONArray("notifications");
        for (int i = 0; i < array.length(); i++) {
            Notification notification = NotificationParser.parseNotification(array.getJSONObject(i));
            notificationList.getNotifications().add(notification);
        }
        return notificationList;
    }
}
