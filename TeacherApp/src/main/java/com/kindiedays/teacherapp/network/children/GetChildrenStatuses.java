package com.kindiedays.teacherapp.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.children.ChildJsonParser;
import com.kindiedays.teacherapp.pojo.ChildStatus;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import static com.kindiedays.common.business.PlaceBusiness.ABSENT_PLACE_ID;

/**
 * Created by pleonard on 15/05/2015.
 */
public class GetChildrenStatuses extends Webservice<Set<ChildStatus>> {

    public static Set<ChildStatus> getStatuses() throws CommonKindieException {
        GetChildrenStatuses getStatuses = new GetChildrenStatuses();
        return getStatuses.getObjects("children/statuses");
    }

    @Override
    public Set<ChildStatus> parseJsonFromServer(JSONObject json) throws JSONException {
        Set<ChildStatus> statuses = new HashSet<>();
        JSONArray array = json.getJSONArray("childStatuses");
        for (int i = 0; i < array.length(); i++) {
            JSONObject jsonChild = array.getJSONObject(i);
            ChildStatus status = new ChildStatus();
            status.setChildId(jsonChild.getInt("child"));
            status.setUnreadMessages(jsonChild.getInt("unreadNotifications"));
            if (!jsonChild.isNull("signin")) {
                JSONObject jsonSignin = jsonChild.getJSONObject("signin");
                status.setCurrentPlaceId(jsonSignin.getInt("place"));
            }
            if (!jsonChild.isNull("presence")) {
                JSONObject jsonPresence = jsonChild.getJSONObject("presence");
                status.setPresence(ChildJsonParser.parsePresence(jsonPresence));
                if(jsonChild.isNull("signin")){
                    status.setCurrentPlaceId(ABSENT_PLACE_ID);
                }
            }
            if (!jsonChild.isNull("temporaryGroup")) {
                JSONObject jsonTemporaryGroup = jsonChild.getJSONObject("temporaryGroup");
                status.setTemporaryGroupId(jsonTemporaryGroup.getInt("group"));
                status.setTemporaryGroupUrl(jsonTemporaryGroup.getJSONObject("links").getString("self"));
            }
            if (!jsonChild.isNull("nap")) {
                JSONObject jsonNapping = jsonChild.getJSONObject("nap");
                String start = jsonNapping.getString("start");
                DateTime date = DateTime.parse(start);
                status.setStartedNapping(date);
            }
            statuses.add(status);
        }

        return statuses;
    }
}
