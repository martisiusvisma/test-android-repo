package com.kindiedays.teacherapp.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.teacherapp.pojo.Notification;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 16/07/2015.
 */
public class PatchNotification extends Webservice<Notification> {

    public static Notification markNotificationRead(String notificationLink) throws CommonKindieException {
        try {
            PatchNotification instance = new PatchNotification();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "MARK_READ");
            return instance.patchObjects(jsonObject, notificationLink);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public Notification parseJsonFromServer(JSONObject json) throws JSONException {
        return NotificationParser.parseNotification(json);
    }
}
