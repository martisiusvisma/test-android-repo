package com.kindiedays.teacherapp.network.groups;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 25/05/2015.
 */
public class PatchTemporaryGroup extends Webservice<String> {

    public static String changeGroup(Child child, long temporaryGroupId) throws CommonKindieException {
        PatchTemporaryGroup patchTemporaryGroup = new PatchTemporaryGroup();
        JSONObject json = new JSONObject();
        try {
            json.put("child", child.getId());
            json.put("group", temporaryGroupId);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return patchTemporaryGroup.patchObjects(json, child.getTemporaryGroupUrl());
    }

    @Override
    public String parseJsonFromServer(JSONObject json) throws JSONException {
        return json.getJSONObject("links").getString("self");
    }

}
