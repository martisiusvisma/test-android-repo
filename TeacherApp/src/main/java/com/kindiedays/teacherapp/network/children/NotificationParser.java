package com.kindiedays.teacherapp.network.children;

import com.kindiedays.teacherapp.pojo.Notification;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 16/07/2015.
 */
public class NotificationParser {
    private NotificationParser() {
        //no instance
    }

    public static Notification parseNotification(JSONObject jsonNotification) throws JSONException {
        Notification notification = new Notification();
        notification.setChildId(jsonNotification.getLong("child"));
        notification.setCreated(DateTime.parse(jsonNotification.getString("created")));
        notification.setIndex(jsonNotification.getLong("index"));
        notification.setType(jsonNotification.getString("type"));
        notification.setText(jsonNotification.getString("text"));
        notification.setRead(jsonNotification.getBoolean("read"));
        notification.setSelfLink(jsonNotification.getJSONObject("links").getString("self"));
        return notification;
    }
}