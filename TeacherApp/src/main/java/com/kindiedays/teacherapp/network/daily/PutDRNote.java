package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.NoteDRParser;
import com.kindiedays.common.pojo.NoteRes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PutDRNote extends Webservice<NoteRes> {


    public static NoteRes updateNote(NoteRes note) throws CommonKindieException {
        try {
            PutDRNote updNote = new PutDRNote();
            JSONObject json = new JSONObject();
            json.put("child", note.getChild());
            json.put("date", note.getDate().toString("yyyy-MM-dd"));
            json.put("text", note.getText());
            json.put("created", note.getCreated());
            json.put("modified", note.getModified());
            return updNote.putObjects("daily-report/notes/" + note.getChild() + "/" + note.getDate().toString("yyyy-MM-dd"), json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public NoteRes parseJsonFromServer(JSONObject json) throws JSONException {
        return NoteDRParser.parseNote(json);
    }
}
