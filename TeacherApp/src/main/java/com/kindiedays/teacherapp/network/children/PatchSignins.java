package com.kindiedays.teacherapp.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by pleonard on 20/05/2015.
 */
public class PatchSignins extends Webservice<Void> {


    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }

    public static void signout(List<Child> children) throws CommonKindieException {
        try {
            PatchSignins patchSignin = new PatchSignins();
            JSONObject json = new JSONObject();
            JSONArray signouts = new JSONArray();
            json.put("patchSignins", signouts);
            for (Child child : children) {
                JSONObject signout = new JSONObject();
                signout.put("child", child.getId());
                signout.put("action", "END_ACTIVE");
                signouts.put(signout);
            }
            patchSignin.patchObjects("signins", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }


}
