package com.kindiedays.teacherapp.network.notes;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.teacherapp.pojo.Note;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class GetChildrenNotes extends Webservice<List<Note>> {

    public static List<Note> getNotes(long childId) throws CommonKindieException {
        GetChildrenNotes getChildrenNotes = new GetChildrenNotes();
        return getChildrenNotes.getObjects("children/" + childId + "/notes");
    }

    @Override
    public List<Note> parseJsonFromServer(JSONObject json) throws JSONException {
        List<Note> notes = new ArrayList<>();
        JSONArray notesJson = json.getJSONArray("notes");
        for (int i = 0; i < notesJson.length(); i++) {
            JSONObject jsonNote = notesJson.getJSONObject(i);
            notes.add(NoteParser.parseNote(jsonNote));
        }
        return notes;
    }
}
