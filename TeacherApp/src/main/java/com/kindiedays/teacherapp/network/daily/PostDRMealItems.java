package com.kindiedays.teacherapp.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.MealParser;
import com.kindiedays.common.pojo.MealRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRMealItems extends Webservice<List<MealRes>> {
    private static final String TAG = PostDRMealItems.class.getSimpleName();

    public static List<MealRes> createMealItems(List<MealRes> items) throws CommonKindieException {

        PostDRMealItems mealBath = new PostDRMealItems();

        JSONArray array = new JSONArray();

        try {
            for (int i = 0; i < items.size(); i++) {
                JSONObject item = new JSONObject();
                item.put("child", items.get(i).getChild());
                item.put("date", items.get(i).getDate().toString("yyyy-MM-dd"));
                item.put("reaction", items.get(i).getReaction());
                item.put("drink", items.get(i).getDrink());
                item.put("mealIndex", items.get(i).getMealIndex());
                item.put("drinkQuantity", items.get(i).getDrinkQuantity());
                item.put("drinkUnit", items.get(i).getDrinkUnit());
                item.put("created", items.get(i).getCreated());
                item.put("notes", items.get(i).getNotes());

                array.put(item);

            }
            JSONObject itemWithLabel = new JSONObject();
            itemWithLabel.put("dailyReportMeals", array);
            Log.i(TAG, "createMealItems: " + itemWithLabel);
            Log.d("myLogs", itemWithLabel.toString());
            return mealBath.postObjects("daily-report/create-meals", itemWithLabel);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public List<MealRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<MealRes> mealItems = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportMeals");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            mealItems.add(MealParser.parserMealItem(object));
        }
        return mealItems;
    }
}