package com.kindiedays.teacherapp.network.places;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.teacherapp.pojo.Place;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 09/07/2015.
 */
public class PostPlaces extends Webservice<Place> {

    public static Place createTemporaryPlace(String name, String address) throws CommonKindieException {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
            json.put("address", address);
            json.put("type", "NORMAL");
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        PostPlaces instance = new PostPlaces();
        return instance.postObjects("places", json);
    }

    @Override
    public Place parseJsonFromServer(JSONObject json) throws JSONException {
        return PlaceParserTeacher.parserPlace(json);
    }
}
