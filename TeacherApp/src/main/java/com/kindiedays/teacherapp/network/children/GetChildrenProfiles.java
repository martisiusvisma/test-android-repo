package com.kindiedays.teacherapp.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.children.ChildJsonParser;
import com.kindiedays.common.pojo.ChildProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 21/07/2015.
 */
public class GetChildrenProfiles extends Webservice<List<ChildProfile>> {

    public static List<ChildProfile> getProfiles() throws CommonKindieException{
        GetChildrenProfiles instance = new GetChildrenProfiles();
        return instance.getObjects("children/profiles");
    }


    @Override
    public List<ChildProfile> parseJsonFromServer(JSONObject json) throws JSONException {
        List<ChildProfile> childrenProfile = new ArrayList<>();
        JSONArray array = json.getJSONArray("profiles");
        for(int i = 0 ; i < array.length() ; i++){
            JSONObject jsonObject = array.getJSONObject(i);
            childrenProfile.add(ChildJsonParser.parseProfile(jsonObject));
        }
        return childrenProfile;
    }
}
