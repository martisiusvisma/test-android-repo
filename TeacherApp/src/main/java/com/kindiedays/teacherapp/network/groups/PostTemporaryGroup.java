package com.kindiedays.teacherapp.network.groups;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by pleonard on 22/05/2015.
 */
public class PostTemporaryGroup extends Webservice<String> {
    private static final String TAG = PostTemporaryGroup.class.getSimpleName();

    public static String changeGroup(Child child, long temporaryGroupId) throws CommonKindieException {
        PostTemporaryGroup postTemporaryGroup = new PostTemporaryGroup();
        JSONObject json = new JSONObject();
        try {
            json.put("child", child.getId());
            json.put("group", temporaryGroupId);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return postTemporaryGroup.postObjects("children/temporary-groups", json);
    }

    @Override
    public String parseJsonFromServer(JSONObject json) throws JSONException {
        return json.getJSONObject("links").getString("self");
    }

    @Override
    protected String responseCreated(HttpResponse response) throws IOException {
        String responseString = EntityUtils.toString(response.getEntity());
        if (responseString != null && responseString.length() > 0) {
            try {
                return parseJsonFromServer(new JSONObject(responseString));
            } catch (Exception e) {
                Log.e(TAG, "responseCreated: ", e);
                return null;
            }
        } else {
            return null;
        }
    }
}
