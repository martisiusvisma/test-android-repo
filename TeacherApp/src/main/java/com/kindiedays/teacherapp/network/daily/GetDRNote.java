package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.NoteRes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Giuseppe Franco - Starcut on 04/05/16.
 */
public class GetDRNote extends Webservice<NoteRes> {

    public static NoteRes getItem(int child, DateTime date) throws CommonKindieException {
        GetDRNote items = new GetDRNote();
        return items.getObjects("daily-report/notes/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public NoteRes parseJsonFromServer(JSONObject json) throws JSONException {
        DateTimeFormatter dataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter longDataFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.000Z");

        NoteRes noteRes = new NoteRes();
        noteRes.setChild(json.getInt("child"));
        noteRes.setDate(dataFormatter.parseDateTime(json.getString("date")));
        noteRes.setText(json.getString("text"));
        noteRes.setCreated(longDataFormatter.parseDateTime(json.getString("created")));
        noteRes.setModified(longDataFormatter.parseDateTime(json.getString("modified")));

        return noteRes;
    }

}
