package com.kindiedays.teacherapp.network.pictures;


import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.album.PictureParser;
import com.kindiedays.teacherapp.pojo.AlbumPictureModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class GetPreviewPictures extends Webservice<AlbumPictureModel> {
    private static final int PREVIEW_COUNT = 4;
    private static final String MAX_PATTENR = "?max=%d";

    public static AlbumPictureModel loadPrimaryGroupPart(long groupId) throws CommonKindieException {
        return loadPrimaryGroupPart(groupId, PREVIEW_COUNT);
    }

    public static AlbumPictureModel loadPrimaryGroupPart(long groupId, int photosAmount) throws CommonKindieException {
        GetPreviewPictures getPreviewPictures = new GetPreviewPictures();
        return getPreviewPictures.getObjects("pictures/group/" + groupId +
                String.format(Locale.getDefault(), MAX_PATTENR, photosAmount));
    }

    public static AlbumPictureModel loadPrimaryTeacherPart(long teacherId, int photosAmount) throws CommonKindieException {
        GetPreviewPictures getPreviewPictures = new GetPreviewPictures();
        return getPreviewPictures.getObjects("pictures/teacher/" + teacherId +
                String.format(Locale.getDefault(), MAX_PATTENR, photosAmount));
    }

    public static AlbumPictureModel loadPrimaryTeacherPart(long teacherId) throws CommonKindieException {
        return loadPrimaryTeacherPart(teacherId, PREVIEW_COUNT);
    }

    public static AlbumPictureModel loadPrimaryPart() throws CommonKindieException {
        return loadPrimaryPart(PREVIEW_COUNT);
    }

    public static AlbumPictureModel loadPrimaryPart(int photosAmount) throws CommonKindieException {
        GetPreviewPictures getPreviewPictures = new GetPreviewPictures();
        return getPreviewPictures.getObjects("pictures/" +
                String.format(Locale.getDefault(), MAX_PATTENR, photosAmount));
    }

    @Override
    public AlbumPictureModel parseJsonFromServer(JSONObject json) throws JSONException {
        AlbumPictureModel pictureList = new AlbumPictureModel();
        JSONArray jsonPictures = json.getJSONArray("pictures");
        for (int i = 0; i < jsonPictures.length(); i++) {
            JSONObject jsonPicture = jsonPictures.getJSONObject(i);
            pictureList.getPictures().add(PictureParser.parsePicture(jsonPicture));
        }
        return pictureList;
    }
}
