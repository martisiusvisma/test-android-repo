package com.kindiedays.teacherapp.network.notes;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 26/06/2015.
 */
public class DeleteNote extends Webservice<Void> {


    public static Void deleteNote(long noteId) throws CommonKindieException {
        DeleteNote deleteNote = new DeleteNote();
        return deleteNote.deleteObjects("notes/" + noteId);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
