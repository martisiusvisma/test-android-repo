package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.MoodParser;
import com.kindiedays.common.pojo.MoodRes;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRMood extends Webservice<List<MoodRes>> {

    public static List<MoodRes> getItems(int child, DateTime date) throws CommonKindieException {
        GetDRMood items = new GetDRMood();
        return items.getObjects("daily-report/moods/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public List<MoodRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<MoodRes> moodRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportMoods");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            moodRes.add(MoodParser.parserMoodItem(object));
        }
        return moodRes;
    }

}
