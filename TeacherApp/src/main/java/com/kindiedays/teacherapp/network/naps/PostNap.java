package com.kindiedays.teacherapp.network.naps;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 26/05/2015.
 */
public class PostNap extends Webservice<Void> {

    public static void startNap(Child child) throws CommonKindieException{
        PostNap postNap = new PostNap();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject nap = new JSONObject();
        try {
            nap.put("child", child.getId());
            array.put(nap);
            json.put("naps", array);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        postNap.postObjects("naps", json);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
