package com.kindiedays.teacherapp.network.notes;

import com.kindiedays.teacherapp.pojo.Note;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 26/11/2015.
 */
public class NoteParser {
    private NoteParser() {
        //no instance
    }

    public static Note parseNote(JSONObject json) throws JSONException {
        Note note = new Note();
        note.setId(json.getInt("id"));
        note.setText(json.getString("text"));
        note.setTitle(json.getString("title"));
        note.setAuthorTeacherId(json.getLong("authorTeacher"));
        note.setChildId(json.getLong("child"));
        note.setCreationDate(DateTime.parse(json.getString("created")));
        note.setModificationDate(DateTime.parse(json.getString("modified")));
        return note;
    }
}