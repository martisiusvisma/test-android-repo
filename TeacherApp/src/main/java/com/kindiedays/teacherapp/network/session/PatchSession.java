package com.kindiedays.teacherapp.network.session;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.HttpResponse;

/**
 * Created by pleonard on 05/05/2015.
 */
public class PatchSession extends Webservice<Void> {

    public static void patch(long teacherId, String pinCode) throws CommonKindieException {
        PatchSession patchSession = new PatchSession();
        JSONObject json = new JSONObject();
        try {
            json.put("teacher", teacherId);
            json.put("teacherPin", pinCode);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        patchSession.patchObjects("sessions/current", json, authEndpoint);
    }

    protected void responseUnauthorized(HttpResponse response) throws KindieDaysNetworkException {
        connectionError(response, true, true);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
