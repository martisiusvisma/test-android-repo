package com.kindiedays.teacherapp.network.pictures;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.GalleryPicture;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 16/12/2015.
 */
public class DeletePicture extends Webservice<Void> {

    public static void deletePicture(GalleryPicture picture) throws CommonKindieException {
        DeletePicture instance = new DeletePicture();
        if ("JPEG".equals(picture.getFormat())) {
            instance.deleteObjects("pictures/" + picture.getHash() + ".jpg");
        }
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
