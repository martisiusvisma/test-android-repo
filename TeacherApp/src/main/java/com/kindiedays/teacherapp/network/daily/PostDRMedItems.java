package com.kindiedays.teacherapp.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.MedParser;
import com.kindiedays.common.pojo.MedRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRMedItems extends Webservice<List<MedRes>> {
    private static final String TAG = PostDRMedItems.class.getSimpleName();


    public static List<MedRes> createMedItems(List<MedRes> items) throws CommonKindieException {

        PostDRMedItems postMedication = new PostDRMedItems();

        JSONArray array = new JSONArray();

        try {
            for (int i = 0; i < items.size(); i++) {
                JSONObject item = new JSONObject();
                item.put("child", items.get(i).getChild());
                item.put("date", items.get(i).getDate().toString("yyyy-MM-dd"));
                item.put("time", items.get(i).getTime().toString("HH:mm:ss"));
                item.put("drugName", items.get(i).getDrugName());
                item.put("dosage", items.get(i).getDosage());
                item.put("notes", items.get(i).getNotes());

                array.put(item);

            }
            JSONObject itemWithLabel = new JSONObject();
            itemWithLabel.put("dailyReportMedications", array);
            Log.i(TAG, "createMedItems: " + itemWithLabel);
            return postMedication.postObjects("daily-report/medications", itemWithLabel);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public List<MedRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<MedRes> medicationItems = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportMedications");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            medicationItems.add(MedParser.parserMedItem(object));
        }
        return medicationItems;
    }
}