package com.kindiedays.teacherapp.network.teachers;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.teachers.ParseTeacher;
import com.kindiedays.common.pojo.Teacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 28/01/2016.
 */
public class PatchTeacher extends Webservice<Teacher> {

    public static Teacher setGroups(long[] groups, long teacherId) throws CommonKindieException {
        PatchTeacher instance = new PatchTeacher();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        for (long groupId : groups) {
            array.put(groupId);
        }
        try {
            json.put("groups", array);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return instance.patchObjects("teachers/" + teacherId, json);
    }

    @Override
    public Teacher parseJsonFromServer(JSONObject json) throws JSONException {
        return ParseTeacher.parse(json);
    }
}
