package com.kindiedays.teacherapp.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.ActivityParser;
import com.kindiedays.common.pojo.ActivityRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRActivityItems extends Webservice<List<ActivityRes>> {
    private static final String TAG = PostDRActivityItems.class.getSimpleName();

    public static List<ActivityRes> createItems(List<ActivityRes> items) throws CommonKindieException {

        PostDRActivityItems postActivity = new PostDRActivityItems();

        JSONArray array = new JSONArray();

        try {
            for (int i = 0; i < items.size(); i++) {
                JSONObject item = new JSONObject();
                item.put("child", items.get(i).getChild());
                item.put("date", items.get(i).getDate().toString("yyyy-MM-dd"));
                item.put("category", items.get(i).getCategory());
                item.put("text", items.get(i).getText());
                array.put(item);

            }
            JSONObject itemWithLabel = new JSONObject();
            itemWithLabel.put("dailyReportActivities", array);
            Log.i(TAG, "createItems: " + itemWithLabel);
            return postActivity.postObjects("daily-report/activities-for-group", itemWithLabel);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public List<ActivityRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<ActivityRes> activityItems = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportActivities");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            activityItems.add(ActivityParser.parserActivityItem(object));
        }
        return activityItems;
    }
}