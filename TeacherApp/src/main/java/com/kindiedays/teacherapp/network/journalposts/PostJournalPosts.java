package com.kindiedays.teacherapp.network.journalposts;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.journalposts.JournalPostJsonParser;
import com.kindiedays.common.pojo.JournalPost;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Created by pleonard on 10/06/2015.
 */
public class PostJournalPosts extends Webservice<JournalPost> {

    public static JournalPost post(Collection<Long> childrenIds, String text,
                                   Collection<String> pictureHashes) throws CommonKindieException {
        PostJournalPosts postJournalPosts = new PostJournalPosts();
        JSONObject json = new JSONObject();
        JSONArray childrenIdArray = new JSONArray();
        for (Long id : childrenIds) {
            childrenIdArray.put(id);
        }
        try {
            json.put("children", childrenIdArray);
            json.put("text", text);
            JSONArray pictureArray = new JSONArray();
            for (String hash : pictureHashes) {
                JSONObject picJson = new JSONObject();
                picJson.put("hash", hash);
                picJson.put("format", "JPEG");
                pictureArray.put(picJson);
            }
            json.put("pictures", pictureArray);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return postJournalPosts.postObjects("journal-posts", json);
    }

    @Override
    public JournalPost parseJsonFromServer(JSONObject json) throws JSONException {
        return JournalPostJsonParser.parseJson(json);
    }
}
