package com.kindiedays.teacherapp.network.calendar;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.children.ChildJsonParser;
import com.kindiedays.common.network.events.EventJsonParser;
import com.kindiedays.common.network.menus.MenuJsonParser;
import com.kindiedays.common.network.utils.Link;
import com.kindiedays.common.network.utils.LinkHeader;
import com.kindiedays.common.network.utils.LinkHeaderParser;
import com.kindiedays.common.pojo.ChildPresence;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.teacherapp.pojo.CalendarEvents;

import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;

/**
 * Created by pleonard on 09/07/2015.
 */
public class GetCalendarEvents extends Webservice<CalendarEvents> {

    public static CalendarEvents getCalendarEventsForDate(LocalDate date) throws CommonKindieException {
        GetCalendarEvents getCalendarEvents = new GetCalendarEvents();
        return getCalendarEvents.getObjects("calendar", apiEndpoint, "date", date.toString());
    }


    public static CalendarEvents getCalendarEvents() throws CommonKindieException {
        GetCalendarEvents getCalendarEvents = new GetCalendarEvents();
        return getCalendarEvents.getObjects("calendar");
    }

    public static CalendarEvents getCalendarEventsFromLink(String link) throws CommonKindieException {
        GetCalendarEvents getCalendarEvents = new GetCalendarEvents();
        return getCalendarEvents.getObjectsFromUrl(link);
    }

    @Override
    protected CalendarEvents responseOk(HttpResponse response) throws IOException {
        CalendarEvents calendarEvents = super.responseOk(response);
        LinkHeaderParser parser = new LinkHeaderParser();
        Header[] headers = response.getHeaders("Link");
        for (int i = 0; i < headers.length; i++) {
            LinkHeader header = parser.parse(headers[i].getValue());
            for (int j = 0; j < header.getLinks().size(); j++) {
                Link link = header.getLinks().get(j);
                if ("previous".equals(link.getRelationship())) {
                    calendarEvents.setPreviousLink(link.getHref());
                }
                if ("next".equals(link.getRelationship())) {
                    calendarEvents.setAfterLink(link.getHref());
                }
            }
        }
        return calendarEvents;
    }

    @Override
    public CalendarEvents parseJsonFromServer(JSONObject json) throws JSONException {
        CalendarEvents calendarEvents = new CalendarEvents();
        if (json.has("start")) {
            calendarEvents.setStart(LocalDate.parse(json.getString("start")));
        }
        if (json.has("end")) {
            calendarEvents.setEnd(LocalDate.parse(json.getString("end")));
        }
        //Absences
        Set<ChildPresence> childPresences = new HashSet<>();
        if (json.has("presences")) {
            JSONArray jsonPresences = json.getJSONArray("presences");
            for (int i = 0; i < jsonPresences.length(); i++) {
                JSONObject jsonPresence = jsonPresences.getJSONObject(i);
                childPresences.add(ChildJsonParser.parsePresence(jsonPresence));
            }
            calendarEvents.setPresences(childPresences);
        }
        //Events
        if (json.has("events")) {
            Set<Event> events = new HashSet<>();
            JSONArray jsonEvents = json.getJSONArray("events");
            for (int i = 0; i < jsonEvents.length(); i++) {
                JSONObject jsonEvent = jsonEvents.getJSONObject(i);
                events.add(EventJsonParser.parseEvent(jsonEvent));
            }
            calendarEvents.setEvents(events);
        }
        //Menus
        if (json.has("menus")) {
            Set<Menu> menus = new HashSet<>();
            JSONArray jsonMenus = json.getJSONArray("menus");
            for (int i = 0; i < jsonMenus.length(); i++) {
                JSONObject jsonMenu = jsonMenus.getJSONObject(i);
                menus.add(MenuJsonParser.parseMenu(jsonMenu));
            }
            calendarEvents.setMenus(menus);
        }
        return calendarEvents;
    }
}
