package com.kindiedays.teacherapp.network.pictures;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.album.PictureParser;
import com.kindiedays.common.pojo.GalleryPicture;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 17/07/2015.
 */
public class PatchPicture extends Webservice<GalleryPicture> {

    public static GalleryPicture patchChildTag(String url, String action, long childId, int x,
                                               int y, int width, int height) throws CommonKindieException {
        JSONObject json = new JSONObject();
        try {
            json.put("action", action);
            json.put("child", childId);
            if (width != 0 && height != 0) {
                json.put("x", x);
                json.put("y", y);
                json.put("width", width);
                json.put("height", height);
            }
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        PatchPicture instance = new PatchPicture();
        return instance.patchObjects(json, url);
    }

    @Override
    public GalleryPicture parseJsonFromServer(JSONObject json) throws JSONException {
        return PictureParser.parsePicture(json);
    }


}
