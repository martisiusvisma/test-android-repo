package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.MedParser;
import com.kindiedays.common.pojo.MedRes;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRMed extends Webservice<List<MedRes>> {

    public static List<MedRes> getItems(int child, DateTime date) throws CommonKindieException {
        GetDRMed items = new GetDRMed();
        return items.getObjects("daily-report/medications/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public List<MedRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<MedRes> medRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportMedications");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            medRes.add(MedParser.parserMedItem(object));
        }
        return medRes;
    }

}
