package com.kindiedays.teacherapp.network.places;

import com.kindiedays.teacherapp.pojo.Place;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class PlaceParserTeacher {
    private PlaceParserTeacher() {
        //static only
    }

    public static Place parserPlace(JSONObject json) throws JSONException {
        Place pl = new Place();
        pl.setAddress(json.getString("address"));
        pl.setName(json.getString("name"));
        pl.setId(json.getInt("id"));
        pl.setType(json.getString("type"));
        pl.setValidityDate(json.optString("date"));
        return pl;
    }
}
