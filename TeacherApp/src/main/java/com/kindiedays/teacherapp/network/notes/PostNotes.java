package com.kindiedays.teacherapp.network.notes;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.teacherapp.pojo.Note;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostNotes extends Webservice<Note> {


    public static Note createNote(long childId, String title, String text) throws CommonKindieException {
        PostNotes postNotes = new PostNotes();
        JSONObject json = new JSONObject();
        try {
            json.put("child", childId);
            json.put("title", title);
            json.put("text", text);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        return postNotes.postObjects("notes", json);
    }

    @Override
    public Note parseJsonFromServer(JSONObject json) throws JSONException {
        return NoteParser.parseNote(json);
    }
}
