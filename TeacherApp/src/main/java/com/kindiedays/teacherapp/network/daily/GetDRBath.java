package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.BathParser;
import com.kindiedays.common.pojo.BathRes;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRBath extends Webservice<List<BathRes>> {

    public static List<BathRes> getItems(int child, DateTime date) throws CommonKindieException {
        GetDRBath items = new GetDRBath();
        return items.getObjects("daily-report/bathroom-log-entries/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public List<BathRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<BathRes> bathRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportBathroomLogEntries");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            bathRes.add(BathParser.parserBathItem(object));
        }
        return bathRes;
    }

}
