package com.kindiedays.teacherapp.network.naps;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 26/05/2015.
 */
public class PatchNap extends Webservice<Void> {

    public static void endNap(Child child) throws CommonKindieException {
        PatchNap patchNap = new PatchNap();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject nap = new JSONObject();
        try {
            nap.put("child", child.getId());
            nap.put("action", "END_ACTIVE");
            array.put(nap);
            json.put("patchNaps", array);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        patchNap.patchObjects("naps", json);
    }


    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
