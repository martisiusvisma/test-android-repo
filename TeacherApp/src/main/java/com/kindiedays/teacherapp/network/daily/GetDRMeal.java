package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.MealParser;
import com.kindiedays.common.pojo.MealRes;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRMeal extends Webservice<List<MealRes>> {

    public static List<MealRes> getItems(int child, DateTime date) throws CommonKindieException {
        GetDRMeal items = new GetDRMeal();
        return items.getObjects("daily-report/get-meals/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public List<MealRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<MealRes> mealRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportMeals");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            mealRes.add(MealParser.parserMealItem(object));
        }
        return mealRes;
    }

}
