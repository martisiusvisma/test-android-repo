package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.NoteDRParser;
import com.kindiedays.common.pojo.NoteRes;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRNote extends Webservice<NoteRes> {


    public static NoteRes createNote(long childId, DateTime creationDate, String text) throws CommonKindieException {
        try {
            PostDRNote postNote = new PostDRNote();
            JSONObject json = new JSONObject();
            json.put("child", childId);
            json.put("date", creationDate.toString("yyyy-MM-dd"));
            json.put("text", text);
            return postNote.postObjects("daily-report/notes", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public NoteRes parseJsonFromServer(JSONObject json) throws JSONException {
        return NoteDRParser.parseNote(json);
    }
}
