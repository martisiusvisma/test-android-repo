package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.ActivityParser;
import com.kindiedays.common.pojo.ActivityRes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 08/06/2015.
 */
public class UpdDRActivityItems extends Webservice<ActivityRes> {

    public static ActivityRes updItems(ActivityRes item) throws CommonKindieException {

        UpdDRActivityItems postActivity = new UpdDRActivityItems();


        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("child", item.getChild());
            jsonObject.put("date", item.getDate().toString("yyyy-MM-dd"));
            jsonObject.put("category", item.getCategory());
            jsonObject.put("text", item.getText());
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }

        String uri = "daily-report/activities/" + item.getChild() + "/" + item.getDate().toString("yyyy-MM-dd") + "/" + item.getCategory();
        return postActivity.putObjects(uri, jsonObject);
    }

    @Override
    public ActivityRes parseJsonFromServer(JSONObject json) throws JSONException {
        return ActivityParser.parserActivityItem(json);
    }
}