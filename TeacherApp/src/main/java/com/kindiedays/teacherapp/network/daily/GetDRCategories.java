package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.ActivityCategoryDRParser;
import com.kindiedays.common.pojo.ActivityCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRCategories extends Webservice<List<ActivityCategory>> {

    public static List<ActivityCategory> getCategoryRes() throws CommonKindieException {
        GetDRCategories getCategories = new GetDRCategories();
        return getCategories.getObjects("daily-report/activity-categories");
    }

    @Override
    public List<ActivityCategory> parseJsonFromServer(JSONObject json) throws JSONException {
        List<ActivityCategory> activityRes = new ArrayList<>();
        JSONArray categoriesJson = json.getJSONArray("dailyReportActivityCategories");
        for (int i = 0; i < categoriesJson.length(); i++) {
            JSONObject jsonNote = categoriesJson.getJSONObject(i);
            activityRes.add(ActivityCategoryDRParser.parseCategory(jsonNote));
        }
        return activityRes;
    }

}
