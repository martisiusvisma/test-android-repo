package com.kindiedays.teacherapp.network.notes;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PutNotes extends Webservice<Void> {


    public static void updateNote(long noteId, String title, String text) throws CommonKindieException{
        JSONObject json = new JSONObject();
        try {
            json.put("title", title);
            json.put("text", text);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
        PutNotes putNotes = new PutNotes();
        putNotes.putObjects("notes/" + noteId, json);
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
