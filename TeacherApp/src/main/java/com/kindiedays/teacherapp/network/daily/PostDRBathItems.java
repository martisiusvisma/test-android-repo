package com.kindiedays.teacherapp.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.BathParser;
import com.kindiedays.common.pojo.BathRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRBathItems extends Webservice<List<BathRes>> {
    private static final String TAG = PostDRBathItems.class.getSimpleName();

    public static List<BathRes> createBathItems(List<BathRes> items) throws CommonKindieException {

        PostDRBathItems postBath = new PostDRBathItems();

        JSONArray array = new JSONArray();

        try {
            for (int i = 0; i < items.size(); i++) {
                JSONObject item = new JSONObject();
                item.put("child", items.get(i).getChild());
                item.put("date", items.get(i).getDate().toString("yyyy-MM-dd"));
                item.put("time", items.get(i).getTime().toString("HH:mm:ss"));

                if (items.get(i).getEquipment() != null) {
                    JSONArray equipArray = new JSONArray();
                    List<String> temp = items.get(i).getEquipment();
                    handleEquipArray(temp, equipArray);

                    item.put("equipment", equipArray);
                }
                item.put("type", items.get(i).getType());
                if (items.get(i).getConstitution() != null) {
                    item.put("constitution", items.get(i).getConstitution());
                }
                if (items.get(i).getNotes() != null) {
                    item.put("notes", items.get(i).getNotes());
                }
                array.put(item);

            }
            JSONObject itemWithLabel = new JSONObject();
            itemWithLabel.put("dailyReportBathroomLogEntries", array);
            Log.i(TAG, "createBathItems: TEST " + itemWithLabel);
            return postBath.postObjects("daily-report/bathroom-log-entries", itemWithLabel);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    private static void handleEquipArray(List<String> temp, JSONArray equipArray) {
        for (int j = 0; j < temp.size(); j++) {
            equipArray.put(temp.get(j));
        }
    }

    @Override
    public List<BathRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<BathRes> bathItems = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportBathroomLogEntries");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            bathItems.add(BathParser.parserBathItem(object));
        }
        return bathItems;
    }
}