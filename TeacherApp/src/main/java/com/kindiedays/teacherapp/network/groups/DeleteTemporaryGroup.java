package com.kindiedays.teacherapp.network.groups;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pleonard on 21/07/2015.
 */
public class DeleteTemporaryGroup extends Webservice<Void> {


    public static Void deleteGroupAssignment(String url) throws CommonKindieException {
        DeleteTemporaryGroup instance = new DeleteTemporaryGroup();
        instance.deleteObjectsAtUrl(url);
        return null;
    }

    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
