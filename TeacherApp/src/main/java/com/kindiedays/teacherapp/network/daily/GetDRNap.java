package com.kindiedays.teacherapp.network.daily;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.NapParser;
import com.kindiedays.common.pojo.NapRes;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GetDRNap extends Webservice<List<NapRes>> {

    public static List<NapRes> getItems(int child, DateTime date) throws CommonKindieException {
        GetDRNap items = new GetDRNap();
        return items.getObjects("daily-report/naps/" + child + "/" + date.toString("yyyy-MM-dd"));
    }

    @Override
    public List<NapRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<NapRes> napRes = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportNaps");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            napRes.add(NapParser.parserNapItem(object));
        }
        return napRes;
    }

}
