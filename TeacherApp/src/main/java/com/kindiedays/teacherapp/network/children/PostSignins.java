package com.kindiedays.teacherapp.network.children;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Created by pleonard on 15/05/2015.
 */
public class PostSignins extends Webservice<Void> {

    public static void signin(Collection<Child> children, long placeId) throws CommonKindieException {
        try {
            PostSignins postSignins = new PostSignins();
            JSONObject json = new JSONObject();
            JSONArray array = new JSONArray();
            for (Child child : children) {
                JSONObject signin = new JSONObject();
                signin.put("child", child.getId());
                signin.put("place", placeId);
                array.put(signin);
            }
            json.put("signins", array);
            postSignins.postObjects("signins", json);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }


    @Override
    public Void parseJsonFromServer(JSONObject json) throws JSONException {
        return null;
    }
}
