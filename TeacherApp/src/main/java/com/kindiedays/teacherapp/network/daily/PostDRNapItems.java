package com.kindiedays.teacherapp.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.NapParser;
import com.kindiedays.common.pojo.NapRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRNapItems extends Webservice<List<NapRes>> {
    private static final String TAG = PostDRMedItems.class.getSimpleName();

    public static List<NapRes> createNapItems(List<NapRes> items) throws CommonKindieException {

        PostDRNapItems postNap = new PostDRNapItems();

        JSONArray array = new JSONArray();

        try {
            for (int i = 0; i < items.size(); i++) {
                JSONObject item = new JSONObject();
                item.put("child", items.get(i).getChild());
                item.put("date", items.get(i).getDate().toString("yyyy-MM-dd"));
                item.put("start", items.get(i).getStart().toString("yyyy-MM-dd'T'HH:mm:ss"));
                item.put("end", items.get(i).getEnd().toString("yyyy-MM-dd'T'HH:mm:ss"));
                item.put("notes", items.get(i).getNotes());
                array.put(item);

            }
            JSONObject itemWithLabel = new JSONObject();
            itemWithLabel.put("dailyReportNaps", array);
            Log.i(TAG, "createNapItems: ");
            return postNap.postObjects("daily-report/naps", itemWithLabel);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public List<NapRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<NapRes> napItems = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportNaps");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            napItems.add(NapParser.parserNapItem(object));
        }
        return napItems;
    }
}