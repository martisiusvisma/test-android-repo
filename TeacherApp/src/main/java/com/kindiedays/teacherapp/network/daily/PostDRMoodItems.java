package com.kindiedays.teacherapp.network.daily;

import android.util.Log;

import com.kindiedays.common.network.CommonKindieException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.network.daily.parser.MoodParser;
import com.kindiedays.common.pojo.MoodRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class PostDRMoodItems extends Webservice<List<MoodRes>> {
    private static final String TAG = PostDRMoodItems.class.getSimpleName();

    public static List<MoodRes> createMoodItems(List<MoodRes> items) throws CommonKindieException {

        PostDRMoodItems postMood = new PostDRMoodItems();

        JSONArray array = new JSONArray();

        try {
            for (int i = 0; i < items.size(); i++) {
                JSONObject item = new JSONObject();
                item.put("child", items.get(i).getChild());
                item.put("date", items.get(i).getDate().toString("yyyy-MM-dd"));
                item.put("time", items.get(i).getTime().toString("HH:mm:ss"));
                item.put("temper", items.get(i).getTemper());
                item.put("notes", items.get(i).getNotes());

                array.put(item);

            }
            JSONObject itemWithLabel = new JSONObject();
            itemWithLabel.put("dailyReportMoods", array);
            Log.i(TAG, "createMoodItems: " + itemWithLabel);
            return postMood.postObjects("daily-report/moods", itemWithLabel);
        } catch (JSONException e) {
            throw new CommonKindieException(e);
        }
    }

    @Override
    public List<MoodRes> parseJsonFromServer(JSONObject json) throws JSONException {
        List<MoodRes> moodItems = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray("dailyReportMoods");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            moodItems.add(MoodParser.parserMoodItem(object));
        }
        return moodItems;
    }
}