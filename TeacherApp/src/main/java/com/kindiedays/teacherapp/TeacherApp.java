package com.kindiedays.teacherapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import com.facebook.stetho.Stetho;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.ui.login.LoginActivity;

/**
 * Created by pleonard on 16/04/2015.
 */
public class TeacherApp extends KindieDaysApp {
    private static final String LOGIN_SETTINGS = "com.kindiedays.teacherapp.loginSettings";
    private static final String CHANNEL_ID = BuildConfig.APPLICATION_ID + ".pushnotifications";

    private SharedPreferences sharedPreferences;
    private static TeacherApp app;

    public TeacherApp() {
        super();
        loginActivityClass = LoginActivity.class;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter theFilter = new IntentFilter();
        theFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(new LockScreenBroadcastReceiver(), theFilter);
        app = this;
        Stetho.initializeWithDefaults(this);
    }

    @Override
    public String getChanelId() {
        return CHANNEL_ID;
    }

    public static TeacherApp getApp() {
        return app;
    }

    private class LockScreenBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ///Feature disabled until we can cache teachers
        }
    }

    public SharedPreferences getTeacherSharedPreferences() {
        if (sharedPreferences == null)
            sharedPreferences = getSharedPreferences(LOGIN_SETTINGS, Context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
