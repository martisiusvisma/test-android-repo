package com.kindiedays.teacherapp.business;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.teachers.GetTeacher;
import com.kindiedays.common.network.teachers.GetTeachers;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.teacherapp.TeacherApp;
import com.kindiedays.teacherapp.network.teachers.PatchTeacher;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import static com.kindiedays.teacherapp.util.constants.TeacherAppConstants.TEACHER_ID;

/**
 * Created by pleonard on 28/01/2016.
 */
public class TeacherBusiness extends Observable {

    private List<Teacher> teachers;
    private Teacher currentTeacher;
    private Set<AsyncTask> pendingTasks = new HashSet<>();
    private Map<Long, Teacher> teachersById;
    private SharedPreferences sharedPreferences;

    private static TeacherBusiness instance;

    protected TeacherBusiness() {
        sharedPreferences = TeacherApp.getApp().getTeacherSharedPreferences();
    }

    public static TeacherBusiness getInstance() {
        if (instance == null) {
            instance = new TeacherBusiness();
        }
        return instance;
    }

    public boolean isRequirePinCode() {
        return currentTeacher == null && !sharedPreferences.contains(TEACHER_ID);
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public Teacher getTeacherForId(long id) {
        return teachersById != null ? teachersById.get(id) : null;
    }

    public Teacher getCurrentTeacher() {
        if (currentTeacher == null && sharedPreferences.contains(TEACHER_ID)) {
            long teacherId = sharedPreferences.getLong(TEACHER_ID, -1);
            currentTeacher = new Teacher();
            if (teachers == null) {
                currentTeacher.setId(teacherId);
                LoadTeachersTask loadTeachersTask = new LoadTeachersTask();
                pendingTasks.add(loadTeachersTask);
                loadTeachersTask.execute();
            } else {
                currentTeacher = getTeacherForId(teacherId);
            }
        }
        return currentTeacher;
    }

    public void setCurrentTeacher(Teacher currentTeacher) {
        if (this.currentTeacher != currentTeacher) {
            this.currentTeacher = currentTeacher;
            if (currentTeacher != null) {
                sharedPreferences.edit().putLong(TEACHER_ID, currentTeacher.getId()).apply();
            } else sharedPreferences.edit().remove(TEACHER_ID).apply();

            getInstance().setChanged();
            getInstance().notifyObservers(currentTeacher);
        }
    }

    public void selectTeacher(Teacher teacher, boolean selected) {
        teacher.setSelected(selected);
        getInstance().setChanged();
        getInstance().notifyObservers(teacher);
    }

    public void unselectAllTeachers() {
        for (Teacher teacher : teachers) {
            teacher.setSelected(false);
        }
    }

    public static class LoadTeachersTask extends NetworkAsyncTask<List<Teacher>> {

        @Override
        protected void onPostExecute(List<Teacher> teachers) {
            getInstance().setChanged();
            getInstance().notifyObservers(teachers);
            if (getInstance().currentTeacher != null) {
                getInstance().setCurrentTeacher(getInstance().getTeacherForId(getInstance().currentTeacher.getId()));
            }
            getInstance().pendingTasks.remove(this);
        }

        @Override
        protected List<Teacher> doNetworkTask() throws Exception {
            getInstance().teachers = GetTeachers.getTeachers();
            getInstance().teachersById = new HashMap<>();
            for (Teacher teacher : getInstance().teachers) {
                getInstance().teachersById.put(teacher.getId(), teacher);
            }
            return getInstance().teachers;
        }
    }

    public static class LoadCurrentTeacherDetails extends NetworkAsyncTask<Teacher> {

        protected String mLink;

        public LoadCurrentTeacherDetails(String link) {
            this.mLink = link;
        }


        @Override
        protected Teacher doNetworkTask() throws Exception {
            return GetTeacher.getTeacherByLink(mLink);
        }
    }

    public static class PatchTeacherGroupsTask extends NetworkAsyncTask<Teacher>{

        long teacherId;
        long[] groupIds;

        public PatchTeacherGroupsTask(long teacherId, long[] groupIds){
            this.teacherId = teacherId;
            this.groupIds = groupIds;
        }

        @Override
        protected Teacher doNetworkTask() throws Exception {
            return PatchTeacher.setGroups(groupIds, teacherId);
        }
    }
}
