package com.kindiedays.teacherapp.business;

import com.kindiedays.teacherapp.pojo.ChildContainer;

import java.util.List;
import java.util.Observable;

/**
 * Created by pleonard on 25/05/2015.
 */
public abstract class ContainerBusiness<T extends ChildContainer> extends Observable {

    public abstract List<T> getContainers(boolean includeVirtualContainers);


}
