package com.kindiedays.teacherapp.business;

import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildPresence;
import com.kindiedays.common.pojo.Kindergarten;

/**
 * Created by pleonard on 19/05/2015.
 */
public class ChildHelper {

    public static final int SIGNED_IN = 1;
    public static final int SIGNED_OUT = 2;
    public static final int ABSENT = 3;

    private ChildHelper() {
        //no instance
    }

   public static int getStatus(Child child){
       if(child.getCurrentPlaceId() != PlaceBusiness.NO_PLACE_ID && child.getCurrentPlaceId() != PlaceBusiness.ABSENT_PLACE_ID) {
           return SIGNED_IN;
       }
       if(child.getPresence() == null){
           if(KindergartenBusiness.getInstance().getKindergarten() == null || KindergartenBusiness.getInstance().getKindergarten().getDefaultAttendance().equals(Kindergarten.ABSENT)){
                return ABSENT;
           }
           else{
               return SIGNED_OUT;
           }
       }
       else{
            if (child.getPresence().getStatus().equals(ChildPresence.ABSENT_STATUS)){
               return ABSENT;
           }
           else{
               return SIGNED_OUT;
           }
       }
    }

    public static long getPlace(Child child){

        if(getStatus(child) != ABSENT){
            return child.getCurrentPlaceId();
        }
        else{
            if(child.getCurrentPlaceId() == PlaceBusiness.NO_PLACE_ID){
                return PlaceBusiness.ABSENT_PLACE_ID;
            }
            else{
                return child.getCurrentPlaceId();
            }
        }
    }

    public static long getCurrentGroup(Child child){
        if(child.getTemporaryGroupId() == GroupBusiness.NO_GROUP_ID){
            return child.getGroupId();
        }
        else{
            return child.getTemporaryGroupId();
        }
    }

}
