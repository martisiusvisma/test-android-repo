package com.kindiedays.teacherapp.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.carers.GetChildCarer;
import com.kindiedays.common.pojo.ChildCarer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pleonard on 26/06/2015.
 */
public class CarerBusiness {


    //Data is populated every time the carers of a childId are loaded
    private Map<Long, List<ChildCarer>> carersPerChildId = new HashMap<>();
    private Map<Long, ChildCarer> carersPerId = new HashMap<>();

    private static CarerBusiness instance;

    private CarerBusiness(){}

    public static CarerBusiness getInstance(){
        if(instance == null){
            instance = new CarerBusiness();
        }
        return instance;
    }

    public ChildCarer getCarer(long id){
        return carersPerId.get(id);
    }

    public List<ChildCarer> getCarersForChildId(long id){
        return carersPerChildId.get(id);
    }

    public List<ChildCarer> getDefaultConversationPartiesForChildId(long id){
        List<ChildCarer> allCarers = carersPerChildId.get(id);
        if(allCarers == null) {
            return Collections.emptyList();
        }
        List<ChildCarer> defaultCarers = new ArrayList<>();
        for(ChildCarer cc : allCarers) {
            if (cc.isDefaultConversationParty()){
                defaultCarers.add(cc);
            }
        }
        return defaultCarers;
    }


    public static class LoadCarers extends NetworkAsyncTask<List<ChildCarer>> {

        protected long childId;

        public LoadCarers(long childId){
            this.childId = childId;
        }

        @Override
        protected List<ChildCarer> doNetworkTask() throws Exception {
            List<ChildCarer> carers = GetChildCarer.getCarers(childId);
            for(ChildCarer carer : carers){
                CarerBusiness.getInstance().carersPerId.put(carer.getId(), carer);
            }
            CarerBusiness.getInstance().carersPerChildId.put(childId, carers);
            return carers;
        }
    }

}
