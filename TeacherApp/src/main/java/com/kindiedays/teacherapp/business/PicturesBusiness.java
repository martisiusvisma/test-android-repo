package com.kindiedays.teacherapp.business;

import android.net.Uri;
import android.widget.Toast;

import com.kindiedays.common.model.Model;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.fileupload.PostPendingFiles;
import com.kindiedays.common.network.fileupload.PutFile;
import com.kindiedays.common.network.pictures.PostPicture;
import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.PendingFile;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.utils.NetworkUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.TeacherApp;
import com.kindiedays.teacherapp.network.pictures.DeletePicture;
import com.kindiedays.teacherapp.network.pictures.GetPreviewPictures;
import com.kindiedays.teacherapp.network.pictures.PatchPicture;
import com.kindiedays.teacherapp.pojo.AlbumCategory;
import com.kindiedays.teacherapp.pojo.AlbumPictureModel;

import java.io.File;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by pleonard on 16/09/2015.
 */
public class PicturesBusiness extends com.kindiedays.common.business.PicturesBusiness {

    public static class PatchChildTagAsyncTask extends NetworkAsyncTask<GalleryPicture> {

        protected static final String ADD_CHILD = "ADD_CHILD";
        protected static final String REMOVE_CHILD = "REMOVE_CHILD";

        protected String url;
        protected long child;
        private String action;
        private int x;
        private int y;
        protected int width;
        protected int height;

        public PatchChildTagAsyncTask(String url, String action, long child) {
            this.url = url;
            this.child = child;
            this.action = action;
        }

        public PatchChildTagAsyncTask(String url, String action, long child, int x, int y, int width, int height) {
            this.url = url;
            this.child = child;
            this.action = action;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        @Override
        protected void onPostExecute(GalleryPicture picture) {
            super.onPostExecute(picture);
            if (exception != null && picture == null) {
                Collection<ChildTag> tags = getInstance().getPictureFromKey(picture.getLink()).getChildTags();
                if (action.equals(ADD_CHILD)) {
                    tags.add(new ChildTag(x, y, width, height, child));

                } else if (action.equals(REMOVE_CHILD)) {
                    ChildTag tagToRemove = null;
                    for (ChildTag tag : tags) {
                        if (tag.getChildId() == child) {
                            tagToRemove = tag;
                            break;
                        }
                    }
                    tags.remove(tagToRemove);
                }
            } else if (exception != null && UnknownHostException.class.equals(exception.getCause().getClass())) {
                Toast.makeText(TeacherApp.getApp(), TeacherApp.getApp().getString(R.string.failed_to_load_message, TeacherApp.getApp().getString(R.string.nap_list)), Toast.LENGTH_SHORT).show();
            } else if (exception != null){
                Toast.makeText(TeacherApp.getApp(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected GalleryPicture doNetworkTask() throws Exception {
            try {
                return PatchPicture.patchChildTag(url, action, child, x, y, width, height);
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }
    }


    public static class DeletePictureAsyncTask extends NetworkAsyncTask<Void> {

        protected GalleryPicture galleryPicture;

        public DeletePictureAsyncTask(GalleryPicture galleryPicture) {
            this.galleryPicture = galleryPicture;
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            DeletePicture.deletePicture(galleryPicture);
            return null;
        }
    }


    public static class UploadPicturesAsyncTask extends NetworkAsyncTask<List<GalleryPicture>> {
        private List<File> files;

        public UploadPicturesAsyncTask(List<File> files) {
            this.files = files;
        }

        @Override
        protected List<GalleryPicture> doNetworkTask() throws Exception {
            List<GalleryPicture> pictures = new ArrayList<>();
            for (int i = 0; i < files.size(); i++) {
                PendingFile pendingFile = PostPendingFiles.post();
                File file = files.get(i);
                GalleryPicture picture = getInstance().getPictureFromKey(Uri.fromFile(file).toString());
                String caption = picture.getCaption();
                List<ChildTag> childTags = picture.getChildTags();
                PutFile.uploadFile(file, pendingFile.getUploadUrl());
                GalleryPicture uploadedPicture = PostPicture.post(pendingFile.getId(), caption, childTags);
                pictures.add(uploadedPicture);
                publishProgress();
            }
            return pictures;
        }

    }


    public static class GetPicturesPerCategory extends NetworkAsyncTask<List<Model>> {

        private List<Group> groups;

        public GetPicturesPerCategory(List<Group> groups) {
            this.groups = groups;
        }

        @Override
        protected List<Model> doNetworkTask() throws Exception {
            try {
                List<Model> albumList = new ArrayList<>();
                List<Long> idsSet = new ArrayList<>();
                start:
                for (Group group : groups) {
                    AlbumPictureModel model;
                    AlbumCategory category;
                    if (group.getId() == ALL_GROUPS) {
                        if (idsSet.contains(new Long(group.getId()))) {
                            continue start;
                        }
                        model = GetPreviewPictures.loadPrimaryPart();
                        category = new AlbumCategory(group.getId(), group.getName(), ALL_GROUPS);
                        idsSet.add(category.getId());
                    } else {
                        if (idsSet.contains(new Long(group.getId()))) {
                            continue start;
                        }
                        model = GetPreviewPictures.loadPrimaryGroupPart(group.getId());
                        category = new AlbumCategory(group.getId(), group.getName(), AlbumCategory.GROUP);
                        idsSet.add(category.getId());
                    }
                    if (model != null && model.getPictures().isEmpty()) {
                        continue;
                    }

                    model.setCategoryModel(category);
                    albumList.add(category);
                    albumList.add(model);
                }
                return albumList;
            } catch (Exception e) {
                if (e instanceof UnknownHostException && !NetworkUtils.isOnline(TeacherApp.getApp())) {
                    throw new KindieDaysNetworkException(0, TeacherApp.getApp().getString(R.string.dialog_no_internet_message), true);
                } else {
                    throw new KindieDaysNetworkException(0, e.getMessage(), true);
                }
            }
        }
    }


    public static class GetPicturesPerTeacher extends NetworkAsyncTask<List<Model>> {
        private List<Teacher> teachers;

        public GetPicturesPerTeacher(List<Teacher> teachers) {
            this.teachers = teachers;
        }

        @Override
        protected List<Model> doNetworkTask() throws Exception {
            List<Model> albumList = new ArrayList<>();
            for (Teacher teacher : teachers) {
                AlbumPictureModel model = GetPreviewPictures.loadPrimaryTeacherPart(teacher.getId());
                if (model.getPictures().isEmpty()) {
                    continue;
                }

                AlbumCategory category = new AlbumCategory(teacher.getId(), teacher.getName(), AlbumCategory.TEACHER);
                model.setCategoryModel(category);
                albumList.add(category);
                albumList.add(model);
            }

            return albumList;
        }
    }
}
