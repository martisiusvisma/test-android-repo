package com.kindiedays.teacherapp.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.teacherapp.network.journalposts.PostJournalPosts;

import java.util.Collection;

/**
 * Created by pleonard on 08/07/2015.
 */
public class JournalBusiness extends com.kindiedays.common.business.JournalBusiness {

    public static class PostJournalPostTask extends NetworkAsyncTask<JournalPost> {

        String text;
        Collection<Long> childrenIds;
        Collection<String> pictureHashes;

        public PostJournalPostTask(String text, Collection<Long> childrenIds, Collection<String> pictureHashes) {
            this.text = text;
            this.childrenIds = childrenIds;
            this.pictureHashes = pictureHashes;
        }

        @Override
        protected JournalPost doNetworkTask() throws Exception {
            return PostJournalPosts.post(childrenIds, text, pictureHashes);
        }
    }

}
