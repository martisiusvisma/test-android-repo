package com.kindiedays.teacherapp.business;

import com.kindiedays.common.pojo.Place;
import com.kindiedays.teacherapp.TeacherApp;
import com.kindiedays.teacherapp.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 04/11/2015.
 */
public class PlaceBusiness extends com.kindiedays.common.business.PlaceBusiness {


    public static PlaceBusiness getInstance(){
        if(instance == null){
            instance = new PlaceBusiness();
            initInstance();
        }
        return (PlaceBusiness) instance;
    }

    public List<Place> findNonEmptyContainers(long groupId, boolean includeVirtualPlaces, boolean includeSignedOut){
        List<Place> nonEmptyPlaces = new ArrayList<>();
        List<Place> places;
        if(includeVirtualPlaces){
            places = allPlaces;
        }
        else{
            places = realPlaces;
        }
        if(places != null){
            for(Place place : places){
                if((place.isMainBuilding() && includeSignedOut) || ChildBusiness.getInstance().getNumberPresentChildrenInPlace(groupId, null) > 0){
                    nonEmptyPlaces.add(place);
                }
            }
        }
        return nonEmptyPlaces;
    }
}
