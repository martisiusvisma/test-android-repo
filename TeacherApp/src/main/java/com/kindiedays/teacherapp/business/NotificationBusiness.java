package com.kindiedays.teacherapp.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.teacherapp.network.children.GetNotifications;
import com.kindiedays.teacherapp.network.children.PatchNotification;
import com.kindiedays.teacherapp.pojo.Notification;
import com.kindiedays.teacherapp.pojo.NotificationList;

import java.util.Observable;

/**
 * Created by pleonard on 16/07/2015.
 */
public class NotificationBusiness extends Observable {

    protected static NotificationBusiness instance;

    private static NotificationList notificationList;
    public static NotificationBusiness getInstance(){
        if(instance == null) {
            instance = new NotificationBusiness();
        }
        return instance;
    }

    public NotificationList getNotifications(){
        return notificationList;
    }

    public static class LoadNotificationsTask extends NetworkAsyncTask<NotificationList>{

        protected long childId;

        public LoadNotificationsTask(long childId){
            this.childId = childId;
        }

        @Override
        protected NotificationList doNetworkTask() throws Exception {
            return GetNotifications.getNotifications(childId);
        }

        @Override
        protected void onPostExecute(NotificationList newNotificationList) {
            super.onPostExecute(newNotificationList);
            getInstance().setChanged();
            getInstance().notifyObservers(newNotificationList);
        }
    }

    public static class LoadMoreNotificationsTask extends NetworkAsyncTask<NotificationList>{
        protected String mUrl;

        public LoadMoreNotificationsTask(String url){
            this.mUrl = url;
        }

        @Override
        @SuppressWarnings("squid:S2209")
        protected void onPostExecute(NotificationList newNotificationList) {
            super.onPostExecute(newNotificationList);
            getInstance().notificationList.setLinkNext(newNotificationList.getLinkNext());
            getInstance().notificationList.getNotifications().addAll(newNotificationList.getNotifications());
            getInstance().setChanged();
            getInstance().notifyObservers(notificationList);
        }

        @Override
        protected NotificationList doNetworkTask() throws Exception {
            return GetNotifications.getNotifications(mUrl);
        }
    }

    public static class MarkNotificationReadAsyncTask extends NetworkAsyncTask<Notification>{

        protected String link;

        public MarkNotificationReadAsyncTask(String link){
            this.link = link;
        }

        @Override
        protected Notification doNetworkTask() throws Exception {
            return PatchNotification.markNotificationRead(link);
        }
    }
}
