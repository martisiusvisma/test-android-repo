package com.kindiedays.teacherapp.business;

import android.util.Log;

import com.kindiedays.common.business.PlaceBusiness;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.children.GetChildProfile;
import com.kindiedays.common.network.children.GetChildren;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildProfile;
import com.kindiedays.teacherapp.network.children.GetChildrenProfiles;
import com.kindiedays.teacherapp.network.children.GetChildrenStatuses;
import com.kindiedays.teacherapp.network.children.PatchSignins;
import com.kindiedays.teacherapp.network.children.PostSignins;
import com.kindiedays.teacherapp.network.groups.DeleteTemporaryGroup;
import com.kindiedays.teacherapp.network.groups.PatchTemporaryGroup;
import com.kindiedays.teacherapp.network.groups.PostTemporaryGroup;
import com.kindiedays.teacherapp.network.naps.PatchNap;
import com.kindiedays.teacherapp.network.naps.PostNap;
import com.kindiedays.teacherapp.pojo.ChildStatus;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pleonard on 29/04/2015.
 */
public class ChildBusiness extends java.util.Observable {

    private volatile List<Child> children = new ArrayList<>();
    private Map<Long, Child> childrenById;
    private Set<ChildStatus> latestStatuses;

    private static ChildBusiness instance;

    private ChildBusiness() {}

    public static ChildBusiness getInstance() {
        if (instance == null) {
            instance = new ChildBusiness();
        }
        return instance;
    }

    public List<Child> getChildren() {
        return children;
    }

    private List<Child> updateChildrenWithStatus() {
        if (children == null || children.isEmpty() || latestStatuses == null) {
            return children;
        }
        Map<Long, ChildStatus> mapStatuses = toMap(latestStatuses);
        for (Child child : children) {
            ChildStatus status = mapStatuses.get(child.getId());
            if (status != null){
                child.setPresence(status.getPresence());
                child.setCurrentPlaceId(status.getCurrentPlaceId());
                child.setTemporaryGroupUrl(status.getTemporaryGroupUrl());
                child.setTemporaryGroupId(status.getTemporaryGroupId());
                child.setStartedNapping(status.getStartedNapping());
                child.setUnreadMessages(status.getUnreadMessages());
            }
        }
        Collections.sort(children);
        return children;
    }

    private Map<Long, ChildStatus> toMap(Set<ChildStatus> statuses) {
        Map<Long, ChildStatus> map = new HashMap<>();
        for (ChildStatus status : statuses) {
            map.put(status.getChildId(), status);
        }
        return map;
    }

    private Map<Long, ChildProfile> toMap(Collection<ChildProfile> children) {
        Map<Long, ChildProfile> map = new HashMap<>();
        for (ChildProfile child : children) {
            map.put(child.getChildId(), child);
        }
        return map;
    }

    @SuppressWarnings("squid:S3398")
    private List<Child> mergeChildrenWithProfiles(List<Child> children, List<ChildProfile> profiles) {
        Map<Long, ChildProfile> map = toMap(profiles);
        for (Child child : children) {
            ChildProfile profile = map.get(child.getId());
            child.setChildProfile(profile);
        }
        return children;
    }

    /**
     * @param groupId:            group to check. If GroupBusiness.ALL_GROUP_ID, returns the sum of present children in every group accessible
     * @param groupIdsToConsider: groups the teacher has access to. If empty, ALL the groups are considered to be accessible
     * @param presentOnly:        if true, only counts present children
     * @return the number of present children in a group the teacher has access to, 0 otherwise
     */

    @SuppressWarnings("squid:S1067")
    public int getNumberTotalChildrenInGroup(long groupId, List<Long> groupIdsToConsider, boolean presentOnly) {
        if (children == null) {
            return 0;
        }
        int count = 0;
        for (Child child : children) {
            if ((groupIdsToConsider == null || groupIdsToConsider.isEmpty() || groupIdsToConsider.contains(ChildHelper.getCurrentGroup(child))) && //Child group to be considered?
                    (groupId == GroupBusiness.ALL_GROUP_ID || ChildHelper.getCurrentGroup(child) == groupId) && //All groups or child group?
                    (!presentOnly || ChildHelper.getStatus(child) == ChildHelper.SIGNED_IN)) { //Present only?
                count++;
            }
        }
        return count;
    }

    /**
     * @param groupId          group id for which we want children. If GroupBusiness.ALL_GROUP_ID, returns a list of children in every group of groupsToConsider
     * @param groupsToConsider if null or empty, ALL groups will be considered
     * @return Children of the given group id if this group should be considered. Empty list otherwise
     */
    @SuppressWarnings("squid:S1067")
    public List<Child> getChildren(long groupId, List<Long> groupsToConsider) {
        List<Child> groupChildren = new ArrayList<>();
        if (children == null) {
            return groupChildren;
        }
        for (Child child : children) {
            if ((groupsToConsider == null || groupsToConsider.isEmpty() || groupsToConsider.contains(ChildHelper.getCurrentGroup(child))) &&
                    (groupId == GroupBusiness.ALL_GROUP_ID || ChildHelper.getCurrentGroup(child) == groupId)) {
                groupChildren.add(child);
            }
        }
        return groupChildren;
    }

    /**
     * @param placeId
     * @param groupIdsToConsider if null or empty, ALL groups will be considered
     * @return number of children in the given place, if the children are in the groups to consider
     */
    public int getNumberPresentChildrenInPlace(long placeId, List<Long> groupIdsToConsider) {
        return getChildrenForPlace(placeId, groupIdsToConsider).size();
    }

    /**
     * @param placeId
     * @param groupIdsToConsider if null or empty, ALL groups will be considered
     * @return children in the given place, if the children are in the groups to consider
     */
    public List<Child> getChildrenForPlace(long placeId, List<Long> groupIdsToConsider) {
        List<Child> placeChildren = new ArrayList<>();
        if (children == null) {
            return placeChildren;
        }
        for (Child child : children) {
            if (ChildHelper.getPlace(child) == placeId &&
                    (groupIdsToConsider == null || groupIdsToConsider.isEmpty() || groupIdsToConsider.contains(ChildHelper.getCurrentGroup(child)))) {
                placeChildren.add(child);
            }
        }
        return placeChildren;
    }

    /**
     * @param groupId
     * @param placeId
     * @param searchString
     * @param includeSignedOutChildren
     * @param groupIdsToConsider       if null or empty, ALL groups will be considered
     * @return returns children in the given group (if the group should be considered) AND the given place (OR Signed out children if includeSignedOutChildren is true) AND the search string if any
     */
    @SuppressWarnings({"squid:S1067", "squid:MethodCyclomaticComplexity"})
    public synchronized List<Child> getChildren(long groupId, long placeId, String searchString, boolean includeSignedOutChildren, List<Long> groupIdsToConsider) {
        if (children == null) {
            return Collections.emptyList();
        }
        List<Child> placeGroupChildren = new ArrayList<>();
        for (Child child : children) {
            if ((ChildHelper.getPlace(child) == placeId || (includeSignedOutChildren && ChildHelper.getStatus(child) == ChildHelper.SIGNED_OUT)) &&
                    (groupIdsToConsider == null || groupIdsToConsider.isEmpty() || groupIdsToConsider.contains(ChildHelper.getCurrentGroup(child))) &&
                    (ChildHelper.getCurrentGroup(child) == groupId || groupId == GroupBusiness.ALL_GROUP_ID) &&
                    (child.getName().toLowerCase().contains(searchString.toLowerCase()) || ((child.getNickname() != null) && child.getNickname().toLowerCase().contains(searchString.toLowerCase())))) {
                placeGroupChildren.add(child);
            }
        }
        return placeGroupChildren;
    }

    public Child getChild(long childId) {
        if (childrenById == null) {
            return null;
        }
        return childrenById.get(childId);
    }

    public void selectChild(long childId, boolean selected) {
        Child child = getChild(childId);
        if (child != null && child.isSelected() != selected) {
            child.setSelected(selected);
            getInstance().setChanged();
            getInstance().notifyObservers(child);
        }
    }

    public void selectChildrenFromGroupAndPlace(long groupId, long placeId, String searchString, boolean signedInOnly, boolean selected, List<Long> groupsToConsider) {
        List<Child> childList = getChildren(groupId, placeId, searchString, signedInOnly, groupsToConsider);
        selectChildren(childList, signedInOnly, selected);
    }

    public void selectChildrenFromGroup(long groupId, boolean signedInOnly, boolean selected, List<Long> groupsToConsider) {
        List<Child> childList = getChildren(groupId, groupsToConsider);
        selectChildren(childList, signedInOnly, selected);
    }

    public void selectChildrenFromPlace(long placeId, boolean signedInOnly, boolean selected, List<Long> groupsToConsider) {
        List<Child> childList = getChildrenForPlace(placeId, groupsToConsider);
        selectChildren(childList, signedInOnly, selected);
    }

    public void selectChildren(List<Child> children, boolean signedInOnly, boolean selected) {
        for (Child child : children) {
            if (!signedInOnly || ChildHelper.getStatus(child) == ChildHelper.SIGNED_IN) {
                child.setSelected(selected);
            }
        }
        getInstance().setChanged();
        getInstance().notifyObservers(children);
    }

    public void selectAllChildren(long groupId) {
        Log.i(ChildBusiness.class.getSimpleName(), "selectAllChildren in group " + groupId);
        for (Child child : children) {
            if (child.getGroupId() == groupId && !child.isSelected()) {
                child.setSelected(true);
            }
        }
        getInstance().setChanged();
        getInstance().notifyObservers(children);
    }

    public void unselectAllChildern(long groupId) {
        Log.d(ChildBusiness.class.getSimpleName(), "unselectAllChildren in group");
        for (Child child : children) {
            if (child.getGroupId() == groupId && child.isSelected()) {
                child.setSelected(false);
            }
        }
        getInstance().setChanged();
        getInstance().notifyObservers(children);
    }

    public void unselectAllChildren() {
        Log.d(ChildBusiness.class.getSimpleName(), "unselectAllChildren");
        for (Child child : children) {
            if (child.isSelected()) {
                child.setSelected(false);
            }
        }
        getInstance().setChanged();
        getInstance().notifyObservers(children);
    }

    public Set<Child> getSelectedChildren() {
        Set<Child> selectedChildren = new HashSet<>();
        for (Child child : children) {
            if (child.isSelected()) {
                selectedChildren.add(child);
            }
        }
        return selectedChildren;
    }

    public Set<Child> getGroupChildren(long groupId) {
        Set<Child> groupChildren = new HashSet<>();
        for (Child child : children) {
            if (child.getGroupId() == groupId) {
                groupChildren.add(child);
            }
        }
        return groupChildren;
    }

    public List<Long> getSelectedGroups() {
        List<Long> groupIds = new ArrayList<>();
        Set<Child> selectedChildren = getSelectedChildren();
        for(Child child : selectedChildren) {
            long groupId = (long) child.getGroupId();
            if (selectedChildren.containsAll(getGroupChildren(groupId)) &&
                    !groupIds.contains(groupId)) {
                groupIds.add(groupId);
            }
        }
        Log.d(ChildBusiness.class.getSimpleName(), "getSelectedGroups " + groupIds);
        return groupIds;
    }

    //Tasks


    public static class LoadChildrenTask extends NetworkAsyncTask<List<Child>> {

        @Override
        protected void onPostExecute(List<Child> children) {
            getInstance().setChanged();
            getInstance().notifyObservers(children);
        }

        @Override
        protected List<Child> doNetworkTask() throws Exception {
            getInstance().children = Collections.synchronizedList(GetChildren.getChildren());
            getInstance().childrenById = new HashMap<>();
            for (Child child : getInstance().children) {
                getInstance().childrenById.put(child.getId(), child);
            }
            getInstance().children = getInstance().updateChildrenWithStatus();
            Collections.sort(ChildBusiness.getInstance().children);
            return instance.children;
        }
    }


    public static class LoadChildrenStatusTask extends NetworkAsyncTask<Set<ChildStatus>> {

        @Override
        protected void onPostExecute(Set<ChildStatus> statuses) {
            getInstance().setChanged();
            getInstance().notifyObservers(statuses);
        }

        @Override
        protected Set<ChildStatus> doNetworkTask() throws Exception {
            getInstance().latestStatuses = GetChildrenStatuses.getStatuses();
            getInstance().children = getInstance().updateChildrenWithStatus();
            Collections.sort(ChildBusiness.getInstance().children);
            return getInstance().latestStatuses;
        }
    }


    public static class LoadChildProfile extends NetworkAsyncTask<ChildProfile> {

        private long id;

        public LoadChildProfile(long id) {
            this.id = id;
        }

        @Override
        protected ChildProfile doNetworkTask() throws Exception {
            return GetChildProfile.getChild(id);
        }

    }


    public static class SigninChildren extends NetworkAsyncTask<Void> {

        private Collection<Child> children;
        private long placeId;

        public SigninChildren(Collection<Child> children, long placeId) {
            this.children = children;
            this.placeId = placeId;
        }

        public SigninChildren(Child child, long placeId) {
            children = new ArrayList<>();
            children.add(child);
            this.placeId = placeId;
        }

        @Override
        protected void onPostExecute(Void nothing) {
            //Update children
            for (Child child : children) {
                getInstance().getChild(child.getId()).setCurrentPlaceId(placeId);
            }
            Collections.sort(ChildBusiness.getInstance().children);
            getInstance().setChanged();
            getInstance().notifyObservers(children);
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            PostSignins.signin(children, placeId);
            return null;
        }

    }


    public static class SignoutChildren extends NetworkAsyncTask<Void> {

        private List<Child> children;

        public SignoutChildren(List<Child> children) {
            this.children = children;
        }

        public SignoutChildren(Child child) {
            children = new ArrayList<>();
            children.add(child);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            ChildBusiness childBusiness = getInstance();

            //Update children
            for (Child child : children) {
                Child childById = childBusiness.getChild(child.getId());
                if (childById != null){
                    childById.setCurrentPlaceId(PlaceBusiness.NO_PLACE_ID);
                }
                child.setStartedNapping(null);
            }
            Collections.sort(ChildBusiness.getInstance().children);
            childBusiness.setChanged();
            childBusiness.notifyObservers(children);
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            for (Child child : children) {
                if (child.isNapping()) {
                    PatchNap.endNap(child);
                }
            }
            PatchSignins.signout(children);
            return null;
        }

    }


    public static class ChangeGroupTemporarily extends NetworkAsyncTask<String> {

        Child child;
        long groupId;

        public ChangeGroupTemporarily(Child child, long groupId) {
            this.child = child;
            this.groupId = groupId;
        }

        @Override
        protected void onPostExecute(String url) {
            if (url == null) {
                child.setTemporaryGroupId(GroupBusiness.NO_GROUP_ID);
            } else {
                child.setTemporaryGroupId(groupId);
            }
            child.setTemporaryGroupUrl(url);
            getInstance().setChanged();
            getInstance().notifyObservers(ChildBusiness.getInstance().children);
        }

        @Override
        protected String doNetworkTask() throws Exception {
            String url = null;
            if (child.getTemporaryGroupUrl() != null) {
                if (groupId == child.getGroupId()) {
                    DeleteTemporaryGroup.deleteGroupAssignment(child.getTemporaryGroupUrl());
                } else {
                    url = PatchTemporaryGroup.changeGroup(child, groupId);
                }

            } else {
                url = PostTemporaryGroup.changeGroup(child, groupId);
            }
            return url;
        }
    }


    public static class StartNap extends NetworkAsyncTask<Void> {

        private Child child;

        public StartNap(Child child) {
            this.child = child;
        }

        @Override
        protected void onPostExecute(Void nothing) {
            child.setStartedNapping(new DateTime());
            getInstance().setChanged();
            getInstance().notifyObservers(getInstance().children);
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            PostNap.startNap(child);
            return null;
        }
    }


    public static class EndNap extends NetworkAsyncTask<Void> {

        private Child child;

        public EndNap(Child child) {
            this.child = child;
        }

        @Override
        protected void onPostExecute(Void nothing) {
            child.setStartedNapping(null);
            getInstance().setChanged();
            getInstance().notifyObservers(getInstance().children);
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            PatchNap.endNap(child);
            return null;
        }
    }


    public static class GetChildrenProfilesAsyncTask extends NetworkAsyncTask<List<ChildProfile>> {

        @Override
        protected void onPostExecute(List<ChildProfile> profiles) {
            //Merge profiles with children
            getInstance().setChanged();
            getInstance().notifyObservers(getInstance().children);
        }

        @Override
        protected List<ChildProfile> doNetworkTask() throws Exception {
            List<ChildProfile> profiles = GetChildrenProfiles.getProfiles();
            getInstance().mergeChildrenWithProfiles(getInstance().getChildren(), profiles);
            return profiles;
        }
    }

}
