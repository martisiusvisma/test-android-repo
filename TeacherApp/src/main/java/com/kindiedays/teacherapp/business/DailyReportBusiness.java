package com.kindiedays.teacherapp.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.daily.GetDRActivities;
import com.kindiedays.common.network.daily.GetDRActivity;
import com.kindiedays.common.network.menus.GetDRDrink;
import com.kindiedays.common.pojo.ActivityCategory;
import com.kindiedays.common.pojo.ActivityRes;
import com.kindiedays.common.pojo.BathRes;
import com.kindiedays.common.pojo.DrinkRes;
import com.kindiedays.common.pojo.MealRes;
import com.kindiedays.common.pojo.MedRes;
import com.kindiedays.common.pojo.MoodRes;
import com.kindiedays.common.pojo.NapRes;
import com.kindiedays.common.pojo.NoteRes;
import com.kindiedays.teacherapp.network.daily.GetDRBath;
import com.kindiedays.teacherapp.network.daily.GetDRCategories;
import com.kindiedays.teacherapp.network.daily.GetDRMeal;
import com.kindiedays.teacherapp.network.daily.GetDRMed;
import com.kindiedays.teacherapp.network.daily.GetDRMood;
import com.kindiedays.teacherapp.network.daily.GetDRNap;
import com.kindiedays.teacherapp.network.daily.GetDRNote;
import com.kindiedays.teacherapp.network.daily.PostDRActivityItems;
import com.kindiedays.teacherapp.network.daily.PostDRBathItems;
import com.kindiedays.teacherapp.network.daily.PostDRMealItems;
import com.kindiedays.teacherapp.network.daily.PostDRMedItems;
import com.kindiedays.teacherapp.network.daily.PostDRMoodItems;
import com.kindiedays.teacherapp.network.daily.PostDRNapItems;
import com.kindiedays.teacherapp.network.daily.PostDRNote;
import com.kindiedays.teacherapp.network.daily.PutDRNote;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Giuseppe Franco - Starcut on 04/04/16.
 */
public class DailyReportBusiness extends com.kindiedays.common.business.DailyReportBusiness{
    //

    private ActivityRes activities;
    private List<ActivityCategory> activityCategories;

    private DailyReportBusiness(){
        super();
    }

    public static DailyReportBusiness getInstance(){
        if(instance == null){
            instance = new DailyReportBusiness();
        }
        return (DailyReportBusiness) instance;
    }

    public static class LoadActivityTask extends NetworkAsyncTask<ActivityRes> {

        private Long child;
        private DateTime date;
        private Integer category;

        public LoadActivityTask(Long child, DateTime date, Integer category) {
            this.child = child;
            this.date = date;
            this.category = category;
        }

        @Override
        protected ActivityRes doNetworkTask() throws Exception {
            getInstance().activities = GetDRActivity.getItems(child, date, category);
            return getInstance().activities;
        }

    }

    public static class PostNoteTask extends NetworkAsyncTask<NoteRes> {

        private long childId;
        private DateTime creationDate;
        private String text;

        public PostNoteTask(long childId, DateTime creationDate, String text) {
            this.childId = childId;
            this.creationDate = creationDate;
            this.text = text;
        }

        @Override
        protected NoteRes doNetworkTask() throws Exception {
            return PostDRNote.createNote(childId, creationDate, text);
        }

    }

    public static  class PutNoteTask  extends NetworkAsyncTask<NoteRes> {

        private NoteRes note;

        public PutNoteTask(NoteRes note) {
            this.note = note;
        }

        @Override
        protected NoteRes doNetworkTask() throws Exception {
            return PutDRNote.updateNote(note);
        }

    }

    public  static class PostBathTask extends NetworkAsyncTask< List<BathRes>> {

        private List<BathRes> bathItems;

        public PostBathTask(List<BathRes> bathItems) {
            this.bathItems = bathItems;
        }

        @Override
        protected  List<BathRes> doNetworkTask() throws Exception {
            return PostDRBathItems.createBathItems(bathItems);
        }

    }

    public  static class PostNapTask extends NetworkAsyncTask< List<NapRes>> {

        private List<NapRes> napItems;

        public PostNapTask(List<NapRes> napItems) {
            this.napItems = napItems;
        }

        @Override
        protected  List<NapRes> doNetworkTask() throws Exception {
            return PostDRNapItems.createNapItems(napItems);
        }

    }

    public static class PostMoodTask extends NetworkAsyncTask< List<MoodRes>> {

        private List<MoodRes> moodItems;

        public PostMoodTask(List<MoodRes> moodItems) {
            this.moodItems = moodItems;
        }

        @Override
        protected  List<MoodRes> doNetworkTask() throws Exception {
            return PostDRMoodItems.createMoodItems(moodItems);
        }

    }

    public static class PostActivityTask extends NetworkAsyncTask< List<ActivityRes>> {

        private List<ActivityRes> activityItems;

        public PostActivityTask(List<ActivityRes> activityItems) {
            this.activityItems = activityItems;
        }

        @Override
        protected  List<ActivityRes> doNetworkTask() throws Exception {
            return PostDRActivityItems.createItems(activityItems);
        }

    }

    public static class PostNewMealTask extends NetworkAsyncTask< List<MealRes>> {

        private List<MealRes> mealItems;

        public PostNewMealTask(List<MealRes> mealItems) {
            this.mealItems = mealItems;
        }

        @Override
        protected  List<MealRes> doNetworkTask() throws Exception {
            return PostDRMealItems.createMealItems(mealItems);
        }

    }

    public static class PostNewMedicationTask extends NetworkAsyncTask< List<MedRes>> {

        private List<MedRes> medicationItems;

        public PostNewMedicationTask(List<MedRes> medicationItems) {
            this.medicationItems = medicationItems;
        }

        @Override
        protected  List<MedRes> doNetworkTask() throws Exception {
            return PostDRMedItems.createMedItems(medicationItems);
        }

    }

    public static class GetBathTask extends NetworkAsyncTask<List<BathRes>> {

        private Integer childId;
        private DateTime date;

        public GetBathTask(Integer childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected List<BathRes> doNetworkTask() throws Exception {
            return GetDRBath.getItems(childId, date);
        }
    }

    public static class GetNoteTask extends NetworkAsyncTask<NoteRes> {

        private Integer childId;
        private DateTime date;

        public GetNoteTask(Integer childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected NoteRes doNetworkTask() throws Exception {
            return GetDRNote.getItem(childId, date);
        }
    }


    public static class GetNapTask extends NetworkAsyncTask<List<NapRes>> {

        private Integer childId;
        private DateTime date;

        public GetNapTask(Integer childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected List<NapRes> doNetworkTask() throws Exception {
            return GetDRNap.getItems(childId, date);
        }
    }

    public static class GetMoodTask extends NetworkAsyncTask<List<MoodRes>> {

        private Integer childId;
        private DateTime date;

        public GetMoodTask(Integer childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected List<MoodRes> doNetworkTask() throws Exception {
            return GetDRMood.getItems(childId, date);
        }
    }

    public static class GetMedTask extends NetworkAsyncTask<List<MedRes>> {

        private Integer childId;
        private DateTime date;

        public GetMedTask(Integer childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected List<MedRes> doNetworkTask() throws Exception {
            return GetDRMed.getItems(childId, date);
        }
    }

    public static class GetMealTask extends NetworkAsyncTask<List<MealRes>> {

        private Integer childId;
        private DateTime date;

        public GetMealTask(Integer childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected List<MealRes> doNetworkTask() throws Exception {
            return GetDRMeal.getItems(childId, date);
        }
    }

    public static class GetDrinkTask extends NetworkAsyncTask<List<DrinkRes>> {
        @Override
        protected List<DrinkRes> doNetworkTask() throws Exception {
            return GetDRDrink.getItems();
        }
    }

    public static class LoadActivityCategoriesTask extends NetworkAsyncTask<List<ActivityCategory>> {

        @Override
        protected List<ActivityCategory> doNetworkTask() throws Exception {
            getInstance().activityCategories = GetDRCategories.getCategoryRes();
            return getInstance().activityCategories;
        }

    }

    public static class LoadActivitiesDescTask extends NetworkAsyncTask<List<ActivityRes>> {
        private int childId;
        private DateTime date;

        public LoadActivitiesDescTask(int childId, DateTime date) {
            this.childId = childId;
            this.date = date;
        }

        @Override
        protected List<ActivityRes> doNetworkTask() throws Exception {
            return GetDRActivities.getItems(childId, date);
        }

    }
}
