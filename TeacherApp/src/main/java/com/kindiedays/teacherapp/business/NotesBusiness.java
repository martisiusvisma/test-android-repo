package com.kindiedays.teacherapp.business;

import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.teacherapp.network.notes.DeleteNote;
import com.kindiedays.teacherapp.network.notes.GetChildrenNotes;
import com.kindiedays.teacherapp.network.notes.PostNotes;
import com.kindiedays.teacherapp.network.notes.PutNotes;
import com.kindiedays.teacherapp.pojo.Note;

import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class NotesBusiness {

    private NotesBusiness() {
        //no instance
    }

    public static class PostNotesTask extends NetworkAsyncTask<Note>{

        private long childId;
        private String title;
        private String text;

        public PostNotesTask(long childId, String title, String text){
            this.childId = childId;
            this.title = title;
            this.text = text;
        }

        @Override
        protected Note doNetworkTask() throws Exception {
            return PostNotes.createNote(childId, title, text);
        }
    }

    public static class PutNotesTask extends NetworkAsyncTask<Void>{

        private long noteId;
        private String title;
        private String text;

        public PutNotesTask(long noteId, String title, String text){
            this.noteId = noteId;
            this.title = title;
            this.text = text;
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            PutNotes.updateNote(noteId, title, text);
            return null;
        }
    }

    public static class GetNotesTask extends NetworkAsyncTask<List<Note>>{

        private long childId;

        public GetNotesTask(long childId){
            this.childId = childId;
        }

        @Override
        protected List<Note> doNetworkTask() throws Exception {
            return GetChildrenNotes.getNotes(childId);
        }
    }

    public static class DeleteNoteTask extends NetworkAsyncTask<Void>{

        protected Note note;

        public DeleteNoteTask(Note note){
            this.note = note;
        }

        @Override
        protected Void doNetworkTask() throws Exception {
            return DeleteNote.deleteNote(note.getId());
        }
    }
}
