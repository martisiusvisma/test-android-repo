package com.kindiedays.teacherapp.business;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Teacher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 30/04/2015.
 */
public class GroupBusiness extends com.kindiedays.common.business.GroupBusiness {

    private GroupBusiness() {
    }

    public static GroupBusiness getInstance() {
        GroupBusiness localInstance = (GroupBusiness) instance;
        if (localInstance == null) {
            synchronized (GroupBusiness.class) {
                localInstance = (GroupBusiness) instance;
                if (localInstance == null) {
                    localInstance = new GroupBusiness();
                    localInstance.allGroup = new Group();
                    localInstance.allGroup.setId(ALL_GROUP_ID);
                    localInstance.allGroup.setName(KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.All));
                    localInstance.initAllGroups();
                    instance = localInstance;
                }
            }
        }
        return localInstance;
    }

    public List<Group> getContainers(boolean includeVirtualGroups) {
        if (includeVirtualGroups) {
            return allGroups;
        } else {
            return realGroups;
        }
    }

    public List<Group> getTeacherGroups(Teacher teacher, boolean includeVirtualGroups) {
        List<Group> groupsToReturn = new ArrayList<>();
        if (includeVirtualGroups) {
            groupsToReturn.add(allGroup);
        }
        for (Group group : realGroups) {
            if (teacher.getGroups().size() == 0 || teacher.getGroups().contains(group.getId())) {
                groupsToReturn.add(group);
            }
        }
        return groupsToReturn;
    }

}
