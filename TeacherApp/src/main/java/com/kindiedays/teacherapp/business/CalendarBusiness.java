package com.kindiedays.teacherapp.business;

import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildPresence;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.teacherapp.network.calendar.GetCalendarEvents;
import com.kindiedays.teacherapp.pojo.CalendarEvents;
import com.kindiedays.teacherapp.pojo.DayEvents;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by pleonard on 10/07/2015.
 */
public class CalendarBusiness {

    private SortedMap<LocalDate, DayEvents> dayEventsMap;

    private static CalendarBusiness instance;

    private String afterUrl;
    private String previousUrl;

    public static final int AFTER = 1;
    public static final int BEFORE = 2;

    private CalendarBusiness() {
        dayEventsMap = new TreeMap<>();
    }

    public static CalendarBusiness getInstance() {
        if (instance == null) {
            instance = new CalendarBusiness();
        }
        return instance;
    }

    public SortedMap<LocalDate, DayEvents> getDayEventsMap() {
        return dayEventsMap;
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void populateDayEventMap(Collection<Event> events) {
        if (events != null) {
            for (Event event : events) {
                handleEventInterval(event);
            }
        }
    }

    private void handleEventInterval(Event event) {
        if (event.getInterval() != null) {
            List<LocalDateTime> allOccurences = EventsBusiness.getInstance().getAllOccurences(event);
            for (LocalDateTime occurence : allOccurences) {
                LocalDate date = occurence.toLocalDate();
                if (dayEventsMap.get(date) == null) {
                    dayEventsMap.put(date, new DayEvents(date));
                }
                dayEventsMap.get(date).getEvents().add(event);
            }
        } else {
            if (dayEventsMap.get(event.getDate()) == null) {
                dayEventsMap.put(event.getDate(), new DayEvents(event.getDate()));
            }
            dayEventsMap.get(event.getDate()).getEvents().add(event);
        }
    }

    @SuppressWarnings("squid:MethodCyclomaticComplexity")
    private void populateDayEventMap(CalendarEvents events) {
        populateDayEventMap(events.getEvents());
        if (events.getMenus() != null) {
            for (Menu menu : events.getMenus()) {
                if (dayEventsMap.get(menu.getDate()) == null) {
                    dayEventsMap.put(menu.getDate(), new DayEvents(menu.getDate()));
                }
                dayEventsMap.get(menu.getDate()).setMenu(menu);
            }
        }
        if (events.getPresences() != null) {
            for (ChildPresence presence : events.getPresences()) {
                Child child = ChildBusiness.getInstance().getChild(presence.getChildId());
                if (child != null) {
                    LocalDate end = (events.getEnd().isBefore(presence.getEnd()) ? events.getEnd() : presence.getEnd());
                    LocalDate currentDate = (events.getStart().isAfter(presence.getStart()) ? events.getStart() : presence.getStart());
                    handleCurrentDate(currentDate, end, child, presence);
                }
            }
        }
    }

    private void handleCurrentDate(LocalDate curDate, LocalDate end, Child child, ChildPresence presence) {
        LocalDate currentDate = curDate;
        while (currentDate.isBefore(end) || currentDate.isEqual(end)) {
            if (dayEventsMap.get(currentDate) == null) {
                dayEventsMap.put(currentDate, new DayEvents(currentDate));
            }
            if (presence.getStatus().equals(ChildPresence.ABSENT_STATUS)) {
                dayEventsMap.get(currentDate).getAbsents().add(child.getName());
            } else {
                dayEventsMap.get(currentDate).getPresents().add(child.getName());
            }
            currentDate = currentDate.plusDays(1);
        }
    }

    public String getAfterUrl() {
        return afterUrl;
    }

    public String getPreviousUrl() {
        return previousUrl;
    }


    public static class GetCloseCalendarEventsAsyncTask extends NetworkAsyncTask<CalendarEvents> {

        protected int direction;

        public GetCloseCalendarEventsAsyncTask(int direction) {
            this.direction = direction;
        }

        @Override
        protected void onPostExecute(CalendarEvents calendarEvents) {
            getInstance().populateDayEventMap(calendarEvents);
            if (direction == AFTER) {
                getInstance().afterUrl = calendarEvents.getAfterLink();

            } else if (direction == BEFORE) {
                getInstance().previousUrl = calendarEvents.getPreviousLink();

            }
        }

        @Override
        protected CalendarEvents doNetworkTask() throws Exception {
            String link = "";
            if (direction == AFTER) {
                link = getInstance().afterUrl;

            } else if (direction == BEFORE) {
                link = getInstance().previousUrl;

            }
            return GetCalendarEvents.getCalendarEventsFromLink(link);
        }
    }


    public static class GetCalendarEventsForDayAsyncTask extends NetworkAsyncTask<CalendarEvents> {

        protected LocalDate date;

        public GetCalendarEventsForDayAsyncTask(LocalDate date) {
            this.date = date;
        }

        @Override
        protected void onPostExecute(CalendarEvents calendarEvents) {
            if (calendarEvents != null) {
                getInstance().dayEventsMap.clear();
                getInstance().afterUrl = calendarEvents.getAfterLink();
                getInstance().previousUrl = calendarEvents.getPreviousLink();
                getInstance().populateDayEventMap(calendarEvents);
            }
        }

        @Override
        protected CalendarEvents doNetworkTask() throws Exception {
            return GetCalendarEvents.getCalendarEventsForDate(date);
        }

    }


    public static class GetCalendarEventsAsyncTask extends NetworkAsyncTask<CalendarEvents> {

        @Override
        protected void onPostExecute(CalendarEvents calendarEvents) {
            getInstance().dayEventsMap.clear();
            getInstance().afterUrl = calendarEvents.getAfterLink();
            getInstance().previousUrl = calendarEvents.getPreviousLink();
            getInstance().populateDayEventMap(calendarEvents);
        }

        @Override
        protected CalendarEvents doNetworkTask() throws Exception {
            return GetCalendarEvents.getCalendarEvents();
        }
    }
}


