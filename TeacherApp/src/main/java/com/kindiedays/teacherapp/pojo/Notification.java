package com.kindiedays.teacherapp.pojo;

import org.joda.time.DateTime;

/**
 * Created by pleonard on 16/07/2015.
 */
public class Notification {

    private long childId;
    private DateTime created;
    private long index;
    private String text;
    private boolean read;
    private String type;
    private String selfLink;

    public static final String PRESENCE_CREATED = "PRESENCE_CREATED";
    public static final String CHILD_PROFILE_CHANGED = "CHILD_PROFILE_CHANGED";
    public static final String MESSAGE_CREATED = "MESSAGE_CREATED";


    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
