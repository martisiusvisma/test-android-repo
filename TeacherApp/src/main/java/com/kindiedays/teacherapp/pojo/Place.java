package com.kindiedays.teacherapp.pojo;

/**
 * Created by pleonard on 15/04/2015.
 */
public class Place extends ChildContainer{
    private String address;
    private String type;
    private String validityDate;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isMainBuilding() {
        return "MAIN_BUILDING".equals(type);
    }

    public boolean isNapRoom() {
        return "NAPROOM".equals(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(String validityDate) {
        this.validityDate = validityDate;
    }
}
