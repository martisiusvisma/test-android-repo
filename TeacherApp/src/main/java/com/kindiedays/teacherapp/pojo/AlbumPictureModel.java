package com.kindiedays.teacherapp.pojo;


import com.kindiedays.common.model.Model;
import com.kindiedays.common.pojo.GalleryPicture;

import java.util.ArrayList;
import java.util.List;

public class AlbumPictureModel implements Model {
    private AlbumCategory category;
    private List<GalleryPicture> pictures;

    public AlbumPictureModel() {
        pictures = new ArrayList<>();
        category = new AlbumCategory();
    }

    public String getCategoryName() {
        return category.getName();
    }

    public void setCategoryName(String categoryName) {
        this.category.setName(categoryName);
    }

    public List<GalleryPicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<GalleryPicture> pictures) {
        this.pictures = pictures;
    }

    public long getGroupId() {
        return category.getId();
    }

    public void setGroupId(long groupId) {
        this.category.setId(groupId);
    }

    public void setCategoryModel(AlbumCategory category) {
        this.category.setId(category.getId());
        this.category.setName(category.getName());
        this.category.setType(category.getType());
    }

    public AlbumCategory getCategory() {
        return category;
    }
}
