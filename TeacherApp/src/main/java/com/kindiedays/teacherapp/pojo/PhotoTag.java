package com.kindiedays.teacherapp.pojo;

public class PhotoTag {
    public static final int BLUR_TAG = 1;
    public static final int NAME_TAG = 2;

    private String tag;
    private int top;
    private int left;
    private int width;
    private int height;
    private int tagType;
    private long childId;

    public PhotoTag(String tag, int top, int left, int width, int height, int tagType, long childId) {
        this.tag = tag;
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
        this.tagType = tagType;
        this.childId = childId;
    }

    public PhotoTag() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getTagType() {
        return tagType;
    }

    public void setTagType(int tagType) {
        this.tagType = tagType;
    }

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }
}
