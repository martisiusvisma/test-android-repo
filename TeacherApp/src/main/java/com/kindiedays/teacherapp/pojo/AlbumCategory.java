package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.model.Model;

import org.parceler.Parcel;

@Parcel
public class AlbumCategory implements Model {
    public static final int TEACHER = 1;
    public static final int GROUP = 2;

    //TODO compiler asked to make them public? why
    public long id;
    public String name;
    public int type;

    public AlbumCategory(long id, String name, int type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public AlbumCategory() {
        //default
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
