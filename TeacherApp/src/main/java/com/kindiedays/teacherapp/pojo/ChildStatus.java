package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.pojo.ChildPresence;
import com.kindiedays.teacherapp.business.PlaceBusiness;

import org.joda.time.DateTime;

/**
 * Created by pleonard on 15/06/2015.
 */
public class ChildStatus {

    private long childId;
    private long currentPlaceId = PlaceBusiness.NO_PLACE_ID;
    private ChildPresence presence;
    private long temporaryGroupId;
    private String temporaryGroupUrl;
    private int unreadMessages;
    private DateTime startedNapping;

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public long getCurrentPlaceId() {
        return currentPlaceId;
    }

    public void setCurrentPlaceId(long currentPlaceId) {
        this.currentPlaceId = currentPlaceId;
    }

    public ChildPresence getPresence() {
        return presence;
    }

    public void setPresence(ChildPresence presence) {
        this.presence = presence;
    }

    public long getTemporaryGroupId() {
        return temporaryGroupId;
    }

    public void setTemporaryGroupId(long temporaryGroupId) {
        this.temporaryGroupId = temporaryGroupId;
    }

    public String getTemporaryGroupUrl() {
        return temporaryGroupUrl;
    }

    public void setTemporaryGroupUrl(String temporaryGroupUrl) {
        this.temporaryGroupUrl = temporaryGroupUrl;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public DateTime getStartedNapping() {
        return startedNapping;
    }

    public void setStartedNapping(DateTime startedNapping) {
        this.startedNapping = startedNapping;
    }
}
