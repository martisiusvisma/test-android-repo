package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.pojo.ServerObject;

/**
 * Created by pleonard on 15/05/2015.
 */
public abstract class ChildContainer extends ServerObject {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
