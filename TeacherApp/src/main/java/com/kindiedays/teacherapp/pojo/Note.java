package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.pojo.ServerObject;

import org.joda.time.DateTime;

/**
 * Created by pleonard on 08/06/2015.
 */
public class Note extends ServerObject {

    private String title = "";
    private String text = "";
    private Long authorTeacherId;
    private Long childId;
    private DateTime creationDate;
    private DateTime modificationDate;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getAuthorTeacherId() {
        return authorTeacherId;
    }

    public void setAuthorTeacherId(Long authorTeacherId) {
        this.authorTeacherId = authorTeacherId;
    }

    public Long getChildId() {
        return childId;
    }

    public void setChildId(Long childId) {
        this.childId = childId;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime noteDate) {
        this.creationDate = noteDate;
    }

    public DateTime getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(DateTime modificationDate) {
        this.modificationDate = modificationDate;
    }
}
