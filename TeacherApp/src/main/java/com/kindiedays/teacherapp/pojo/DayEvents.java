package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.Menu;

import org.joda.time.LocalDate;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pleonard on 10/07/2015.
 */
public class DayEvents {

    private LocalDate date;
    private Set<Event> events = new HashSet<>();
    private Menu menu;
    private Set<String> absents = new HashSet<>();
    private Set<String> presents = new HashSet<>();

    public DayEvents(LocalDate date){
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Set<String> getAbsents() {
        return absents;
    }

    public void setAbsents(Set<String> absents) {
        this.absents = absents;
    }

    public Set<String> getPresents() {
        return presents;
    }

    public void setPresents(Set<String> presents) {
        this.presents = presents;
    }
}
