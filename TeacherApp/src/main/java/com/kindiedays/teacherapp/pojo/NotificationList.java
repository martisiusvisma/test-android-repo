package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.pojo.PaginatedResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 16/07/2015.
 */
public class NotificationList extends PaginatedResource {

    private List<Notification> notifications = new ArrayList<>();

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
    public void setNotifications(NotificationList notificationsList) {
        this.notifications.addAll(notificationsList.getNotifications());
    }

}
