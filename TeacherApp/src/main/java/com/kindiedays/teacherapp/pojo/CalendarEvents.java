package com.kindiedays.teacherapp.pojo;

import com.kindiedays.common.pojo.ChildPresence;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.Menu;

import org.joda.time.LocalDate;

import java.util.Set;

/**
 * Created by pleonard on 09/07/2015.
 */
public class CalendarEvents {

    private LocalDate start;
    private LocalDate end;
    private Set<ChildPresence> presences;
    private Set<Event> events;
    private Set<Menu> menus;
    private String previousLink;
    private String afterLink;

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Set<ChildPresence> getPresences() {
        return presences;
    }

    public void setPresences(Set<ChildPresence> presences) {
        this.presences = presences;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Set<Menu> getMenus() {
        return menus;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    public String getPreviousLink() {
        return previousLink;
    }

    public void setPreviousLink(String previousLink) {
        this.previousLink = previousLink;
    }

    public String getAfterLink() {
        return afterLink;
    }

    public void setAfterLink(String afterLink) {
        this.afterLink = afterLink;
    }
}
