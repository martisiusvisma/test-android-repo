package com.kindiedays.teacherapp.util.constants;


public class TeacherAppConstants {
    public static final String CHANGE_LANGUAGE_TAG = "change_lang";

    // child preferences
    public static final String TEACHER_ID = "teacher_id";
    public static final String SESSION_LINK = "session_link";

    private TeacherAppConstants() {
        //no instance
    }
}
