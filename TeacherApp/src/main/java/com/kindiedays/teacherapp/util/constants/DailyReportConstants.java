package com.kindiedays.teacherapp.util.constants;


public class DailyReportConstants {
    public static final String ACTIVITIES_TAG = "activities";
    public static final String BATH_TAG = "bathroom";
    public static final String MEAL_TAG = "meals";
    public static final String MED_TAG = "medication";
    public static final String MOOD_TAG = "mood";
    public static final String NAP_TAG = "naps";
    public static final String NOTE_TAG = "notes";

    private DailyReportConstants() {
        //no instance
    }
}
