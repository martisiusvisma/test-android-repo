package com.kindiedays.teacherapp.util;


import android.support.annotation.StringRes;

import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.ACTIVITIES_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.BATH_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.MEAL_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.MED_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.MOOD_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.NAP_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.NOTE_TAG;

public class DailyReportUtils {
    private DailyReportUtils() {
        //no instance
    }

    @StringRes
    public static int getTitleResId(String tag) {
        switch (tag) {
        case ACTIVITIES_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Activities;

        case BATH_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Bath;

        case MEAL_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Meals;

        case MED_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Medications;

        case MOOD_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Moods;

        case NAP_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Naps;

        case NOTE_TAG:
            return com.kindiedays.common.R.string.Daily_Report_Notes;

        default:
            return com.kindiedays.common.R.string.Daily_Report;
        }
    }
}
