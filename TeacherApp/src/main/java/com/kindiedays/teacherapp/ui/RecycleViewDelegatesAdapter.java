package com.kindiedays.teacherapp.ui;


import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager;
import com.kindiedays.common.model.Model;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewDelegatesAdapter extends RecyclerView.Adapter {
    private AdapterDelegatesManager<List<Model>> delegatesManager = new AdapterDelegatesManager<>();
    private List<Model> items = new ArrayList<>();

    @Override
    public int getItemViewType(int position) {
        return delegatesManager.getItemViewType(items, position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegatesManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        delegatesManager.onBindViewHolder(items, position, holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addDelegate(AdapterDelegate<List<Model>> delegate) {
        delegatesManager.addDelegate(delegate);
        notifyDataSetChanged();
    }

    public void setItems(List<Model> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void addItemToStart(Model model) {
        this.items.add(0, model);
        notifyItemInserted(0);
    }

    public void addItems(List<Model> items, int start) {
        if (this.items.size() >= start) {
            this.items.addAll(start, items);
            notifyItemRangeInserted(start, items.size());
        }
    }

    public void deleteItem(int position) {
        this.items.remove(position);
        notifyItemRemoved(position);
    }

    public void deleteItems() {
        int size = this.items.size();
        this.items.clear();
        notifyItemRangeRemoved(0, size);
    }
}
