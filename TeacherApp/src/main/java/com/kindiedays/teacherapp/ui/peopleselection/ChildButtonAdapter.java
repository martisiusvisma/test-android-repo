package com.kindiedays.teacherapp.ui.peopleselection;

import android.content.Context;

import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.personselection.PersonButtonAdapter;

import java.util.List;

/**
 * Created by pleonard on 06/10/2015.
 */
public abstract class ChildButtonAdapter extends PersonButtonAdapter {

    public ChildButtonAdapter(Context context) {
        super(context);
    }

    public void setChildren(List<Person> children){
        persons = children;
        notifyDataSetChanged();
    }

}
