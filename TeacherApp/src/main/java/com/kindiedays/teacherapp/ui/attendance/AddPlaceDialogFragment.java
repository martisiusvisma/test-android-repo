package com.kindiedays.teacherapp.ui.attendance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 02/02/2016.
 */
public class AddPlaceDialogFragment extends DialogFragment {

    View.OnClickListener onOkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            owner.createTemporaryPlace(name.getText().toString(), address.getText().toString());
            AddPlaceDialogFragment.this.dismiss();
        }
    };

    View.OnClickListener onCancelClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            AddPlaceDialogFragment.this.dismiss();
        }
    };

    public interface TemporaryPlaceCreator {
        public void createTemporaryPlace(String name, String address);
    }

    Button okButton;
    Button cancelButton;
    EditText name;
    EditText address;
    protected TemporaryPlaceCreator owner;

    public AddPlaceDialogFragment(){
        super();
    }

    public void setOwner(TemporaryPlaceCreator owner){
        this.owner = owner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_add_place, null);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setTitle(getString(R.string.Add_Location));
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        name = (EditText) v.findViewById(R.id.et_name);
        address = (EditText) v.findViewById(R.id.et_address);
        okButton = (Button) v.findViewById(R.id.okButton);
        cancelButton = (Button) v.findViewById(R.id.cancelButton);
        okButton.setOnClickListener(onOkClickListener);
        cancelButton.setOnClickListener(onCancelClickListener);
        return v;
    }
}
