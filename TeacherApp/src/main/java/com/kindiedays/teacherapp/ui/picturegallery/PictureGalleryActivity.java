package com.kindiedays.teacherapp.ui.picturegallery;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.ui.picturegallery.JournalPictureGalleryAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.peopleselection.tagselection.PictureTagSelectionActivity;

/**
 * Created by pleonard on 15/07/2015.
 */
public class PictureGalleryActivity extends com.kindiedays.common.ui.picturegallery.PictureGalleryActivity {

    private Dialog dialog;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(!journalMode){
            getMenuInflater().inflate(R.menu.menu_photo_gallery, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        else{
            final GalleryPicture picture = PicturesBusiness.getInstance().getCurrentPictureList().getPictures().get(viewPager.getCurrentItem());
            int i = item.getItemId();
            if (i == R.id.action_tag) {
                Intent intent = new Intent(this, PictureTagSelectionActivity.class);
                intent.putExtra(PictureTagSelectionActivity.PICTURE_KEY, picture.getLink());
                startActivity(intent);

            } else if (i == R.id.action_delete) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PictureGalleryActivity.this);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PicturesBusiness.getInstance().getCurrentPictureList().getPictures().remove(picture);
                        DeletePicture task = new DeletePicture(picture);
                        task.execute();
                        getPendingTasks().add(task);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setTitle(R.string.Delete_Picture);
                builder.setMessage(R.string.Remove_picture_from_gallery);
                builder.setCancelable(false);
                dialog = builder.create();
                dialog.show();

            }
        }
        return true;
    }

    class DeletePicture extends com.kindiedays.teacherapp.business.PicturesBusiness.DeletePictureAsyncTask {

        public DeletePicture(GalleryPicture galleryPicture) {
            super(galleryPicture);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(!checkNetworkException(null, exception)){
                super.onPostExecute(aVoid);
                int currentPosition = viewPager.getCurrentItem();

                if(currentPosition != 0){
                    viewPager.setCurrentItem(currentPosition);
                }
                else{
                    if(viewPager.getAdapter().getCount() <= 1){
                        PictureGalleryActivity.this.finish();
                        return;
                    }
                }
                pictureGalleryAdapter.notifyDataSetChanged();
            }
            getPendingTasks().remove(this);
            dialog.dismiss();
        }
    }

    protected void initAdapter(){
        if(journalMode){
            pictureGalleryAdapter = new JournalPictureGalleryAdapter(this, getIntent().getLongExtra(JOURNAL_POST_ID, 0));
        }
        else{
            pictureGalleryAdapter = new PictureGalleryAdapter(this, blurringChild);
            pictureGalleryAdapter.setPictures(pictures);
        }

    }
}
