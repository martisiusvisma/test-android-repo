package com.kindiedays.teacherapp.ui.peopleselection.tagselection;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.kindiedays.common.utils.BlurUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.album.PictureNoConsentDialog;

/**
 * Created by pleonard on 07/01/2016.
 */
@SuppressWarnings("squid:S1151")
public class ChildPositionSelectionActivity extends com.kindiedays.common.ui.personselection.ChildPositionSelectionActivity {
    public ChildPositionSelectionActivity() {
        surfaceTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                int maxX = mSurfaceView.getRight() - mSurfaceView.getLeft();
                int maxY = mSurfaceView.getBottom() - mSurfaceView.getTop();
                if (x < 0) {
                    x = 0;
                }
                if (y < 0) {
                    y = 0;
                }
                if (x > maxX) {
                    x = maxX;
                }
                if (y > maxY) {
                    y = maxY;
                }
                final Point corner = new Point(x, y);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mPictureView.setImageBitmap(srcBitmap);
                        rectBoxFirstCorner.set(corner);
                        break;

                    //move it to AsyncTask
                    case MotionEvent.ACTION_UP:
                        rectangleBox = new Rect(rectBoxFirstCorner.get().x, rectBoxFirstCorner.get().y,
                                corner.x, corner.y);
                        rectangleBox.sort();

                        //clear surface view
                        final Canvas surfCanvas = mSurfaceView.getHolder().lockCanvas();
                        surfCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                        mSurfaceView.getHolder().unlockCanvasAndPost(surfCanvas);

                        //after clearing blue rect - check valid dimens
                        if (rectangleBox.width() <= 0 || rectangleBox.height() <= 0) {
                            break;
                        }

                        //get blurred bitmap
                        Bitmap resultBitmap = BlurUtils.getBlurredBitmap(ChildPositionSelectionActivity.this,
                                srcBitmap, rectangleBox);

                        //idk why, but imageView is being replaced all the time
                        mPictureView.setImageBitmap(resultBitmap);
                        RelativeLayout.LayoutParams lp = ((RelativeLayout.LayoutParams) mPictureView.getLayoutParams());
                        lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                        mPictureView.setLayoutParams(lp);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        final android.graphics.Rect rect = new android.graphics.Rect(rectBoxFirstCorner.get().x,
                                rectBoxFirstCorner.get().y, corner.x, corner.y);
                        final Canvas canvas = mSurfaceView.getHolder().lockCanvas();
                        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR); // remove old rectangle
                        canvas.drawRect(rect, rectStrokePaint);
                        canvas.drawRect(rect, rectFillPaint);
                        mSurfaceView.getHolder().unlockCanvasAndPost(canvas);
                        break;
                    default:
                        break;
                }

                return true;
            }
        };

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogFragment dialog = new PictureNoConsentDialog();
        dialog.show(getFragmentManager(), PictureNoConsentDialog.TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ok:
                if (rectangleBox == null) {
                    setResult(RESULT_CANCELED);
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(X, mPictureView.getOriginalXPosition(rectangleBox.left));
                    intent.putExtra(Y, mPictureView.getOriginalYPosition(rectangleBox.top));
                    intent.putExtra(WIDTH, mPictureView.getOriginalXPosition(rectangleBox.right - rectangleBox.left));
                    intent.putExtra(HEIGHT, mPictureView.getOriginalYPosition(rectangleBox.bottom - rectangleBox.top));
                    intent.putExtra(IMAGE_WIDTH, mPictureView.getOriginalXPosition(srcBitmap.getWidth()));
                    intent.putExtra(IMAGE_HEIGHT, mPictureView.getOriginalYPosition(srcBitmap.getHeight()));
                    setResult(RESULT_OK, intent);
                }
                finish();
                break;
            case R.id.action_skip:
                setResult(RESULT_CANCELED);
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }

}
