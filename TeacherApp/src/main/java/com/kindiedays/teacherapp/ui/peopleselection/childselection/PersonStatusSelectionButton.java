package com.kindiedays.teacherapp.ui.peopleselection.childselection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.ui.personselection.PersonSelectionButton;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildHelper;

/**
 * Created by pleonard on 21/04/2015.
 */
public class PersonStatusSelectionButton extends PersonSelectionButton {


    public PersonStatusSelectionButton(Context context) {
        super(context);
    }

    public PersonStatusSelectionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PersonStatusSelectionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void refreshPerson() {
        if (person.isSelected()) {
            ivPersonPicture.setColorFilter(getContext().getResources().getColor(R.color.selectedfilter));
            ivPersonPicture.setAlpha(0xFF);
            ivSelectionMark.setVisibility(View.VISIBLE);
        } else {
            ivSelectionMark.setVisibility(View.GONE);
            ivPersonPicture.clearColorFilter();
            if (person instanceof Child) {
                switch (ChildHelper.getStatus((Child) person)) {
                    case ChildHelper.SIGNED_IN:
                        ivPersonPicture.setAlpha(0xFF);
                        break;
                    case ChildHelper.SIGNED_OUT:
                        //Gray picture
                        ivPersonPicture.setAlpha(0x77);
                        break;
                    case ChildHelper.ABSENT:
                        ivPersonPicture.setAlpha(0x77);
                        break;
                    default:
                        break;
                }
            } else if (person instanceof ConversationParty) {
                ivPersonPicture.setAlpha(0xFF);
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }
}
