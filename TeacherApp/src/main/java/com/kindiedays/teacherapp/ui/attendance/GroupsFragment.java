package com.kindiedays.teacherapp.ui.attendance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TabHost;

import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PlaceBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GroupsFragment extends Fragment implements Observer {

    FragmentTabHost groupTabHost;
    ProgressBar progressBar;
    View v;

    Set<String> addedTabs = new HashSet<>();

    private boolean groupsReady = false;
    private boolean childrenReady = false;
    private boolean placesReady = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_groups, container, false);
        groupTabHost = (FragmentTabHost) v.findViewById(R.id.groupTabHost);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        groupTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        //Placeholder is necessary when initializing the view as we can't display an empty tab host
        return v;
    }

    public void cancelSelecting(){

    }

    @Override
    public void onStart() {
        super.onStart();
        GroupBusiness.getInstance().addObserver(this);
        ChildBusiness.getInstance().addObserver(this);
        PlaceBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        GroupBusiness.getInstance().deleteObserver(this);
        ChildBusiness.getInstance().deleteObserver(this);
        PlaceBusiness.getInstance().deleteObserver(this);
    }

    private GroupTabIndicator createTabView(int current, int total, String groupname) {
        GroupTabIndicator indicator = new GroupTabIndicator(getActivity());
        indicator.setCurrent(current);
        indicator.setTotal(total);
        indicator.setGroupName(groupname);
        return indicator;
    }

    private void refreshGroupTabs(){
        Teacher teacher = TeacherBusiness.getInstance().getCurrentTeacher();
        if(teacher != null) {
            progressBar.setVisibility(View.GONE);
            groupTabHost.setVisibility(View.VISIBLE);
            List<Group> groups = GroupBusiness.getInstance().getTeacherGroups(teacher, true);
            ChildBusiness childBusiness = ChildBusiness.getInstance();

            for (int i = 0; i < groups.size(); i++) {
                Group group = groups.get(i);
                int present = childBusiness.getNumberTotalChildrenInGroup(group.getId(), teacher.getGroups(), true);
                int total = childBusiness.getNumberTotalChildrenInGroup(group.getId(), teacher.getGroups(), false);

                TabHost.TabSpec ts = groupTabHost.newTabSpec("GroupSubtab " + group.getName())
                        .setIndicator(createTabView(present, total, group.getName()));
                Bundle bundle = new Bundle();
                bundle.putLong(GroupFragment.GROUP_ID, group.getId());

                if (!addedTabs.contains(ts.getTag())){
                    addedTabs.add(ts.getTag());
                    groupTabHost.addTab(ts, GroupFragment.class, bundle);
                } else {
                    GroupTabIndicator tabIndicator = (GroupTabIndicator) groupTabHost.getTabWidget().getChildTabViewAt(i);
                    tabIndicator.setCurrent(present);
                    tabIndicator.setTotal(total);
                }

            }

            int currentTab = groupTabHost.getCurrentTab();
            if(currentTab < groupTabHost.getTabWidget().getTabCount()){
                groupTabHost.setCurrentTab(currentTab);
            }
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof GroupBusiness) {
            groupsReady = true;
        }
        if (observable instanceof ChildBusiness && data != null) {
            if (data instanceof Set && !((Set) data).isEmpty())
                childrenReady = true;
        }
        if (observable instanceof PlaceBusiness) {
            placesReady = true;
        }

        if (groupsReady && childrenReady && placesReady) {
            refreshGroupTabs();
        }
    }
}
