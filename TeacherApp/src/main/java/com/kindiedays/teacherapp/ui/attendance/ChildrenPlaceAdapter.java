package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.view.ViewGroup;

import com.kindiedays.common.pojo.Place;


/**
 * Created by pleonard on 15/05/2015.
 */
public class ChildrenPlaceAdapter extends ChildrenAttendanceContainerAdapter<Place> {

    public ChildrenPlaceAdapter(Context context) {
        super(context);
    }

    @Override
    public ChildrenAttendanceContainerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ChildAttendanceButton button = new ChildAttendanceButton(mContext);
        return new ChildrenAttendanceContainerAdapter.ItemViewHolder(button);
    }
}
