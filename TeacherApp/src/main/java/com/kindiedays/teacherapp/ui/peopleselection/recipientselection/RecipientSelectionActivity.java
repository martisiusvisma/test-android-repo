package com.kindiedays.teacherapp.ui.peopleselection.recipientselection;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TabHost;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.ConversationBusiness;
import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.utils.ArrayUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.CarerBusiness;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleGroupSelectionActivity;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleSelectionGroupFragment;
import com.kindiedays.teacherapp.ui.peopleselection.TeacherSelectionGroupFragment;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pleonard on 11/06/2015.
 */
public class RecipientSelectionActivity extends PeopleGroupSelectionActivity implements CarerSelectionDialogFragment.SelectedCarersIdRecipient {

    public static final Group TEACHER_GROUP = new Group();

    private static final String CARER_SELECTION_DIALOG_TAG = "carerSelectionDialog";

    @Override
    public void setSelectedCarersForChild(long childId, Set<Long> selectedCarers) {
        ConversationBusiness.getInstance().getSelectedCarersPerChild().put(childId, selectedCarers);
        ChildBusiness.getInstance().selectChild(childId, !selectedCarers.isEmpty());
    }

    @Override
    public Set<Long> getSelectedCarersForChild(long childId) {
        return ConversationBusiness.getInstance().getSelectedCarersPerChild().get(childId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Map<Long, Set<Long>> selectedCarersPerChild = ConversationBusiness.getInstance().getSelectedCarersPerChild();
        for (Map.Entry<Long, Set<Long>> entry : selectedCarersPerChild.entrySet()) {
            if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                ChildBusiness.getInstance().selectChild(entry.getKey(), true);
            }
        }
        new GetConversationPartiesAsyncTask(selectedTeachers).execute();
    }

    @Deprecated
    @Override
    protected void initTabs() {
        /*
         * replaced with {@link #initGroupTabs()}
         */
    }

    @Override
    public void onPersonClick(Person person) {
        if (person instanceof Child) {
            if (ConversationBusiness.getInstance().getSelectedCarersPerChild().get(person.getId()) != null && !ConversationBusiness.getInstance().getSelectedCarersPerChild().get(person.getId()).isEmpty()) {
                ConversationBusiness.getInstance().getSelectedCarersPerChild().put(person.getId(), null);
                ChildBusiness.getInstance().selectChild(person.getId(), false);
            } else {
                LoadCarersAndSelectDefaultRecipientsAsyncTask task = new LoadCarersAndSelectDefaultRecipientsAsyncTask(person.getId());
                getPendingTasks().add(task);
                task.execute();
            }
        }
        if (person instanceof ConversationParty) {
            if (person.isSelected()) {
                selectedTeachers.remove(((ConversationParty) person).getPartyId());
            } else {
                selectedTeachers.add(((ConversationParty) person).getPartyId());
            }
            ConversationPartyBusiness.getInstance().select((ConversationParty) person, !person.isSelected());
        }
    }

    @Override
    public void onPersonLongClick(Person person) {
        if (person instanceof Child) {
            CarerSelectionDialogFragment selectionDialogFragment = new CarerSelectionDialogFragment();
            selectionDialogFragment.setOwner(this);
            Bundle bundle = new Bundle();
            bundle.putLong(CarerSelectionDialogFragment.CHILD_ID, person.getId());
            selectionDialogFragment.setArguments(bundle);
            selectionDialogFragment.show(getSupportFragmentManager(), CARER_SELECTION_DIALOG_TAG);
        }
    }

    @Override
    public void onGroupClick(Group group) {
        if (TEACHER_GROUP == group) {
            boolean anySelected = false;
            for (ConversationParty party : ConversationPartyBusiness.getInstance().getConversationPartyPerType().get(ConversationParty.TEACHER_TYPE)) {
                if (party.isSelected()) {
                    anySelected = true;
                    break;
                }
            }
            if (anySelected) {
                selectedTeachers.clear();
            } else {
                for (ConversationParty party : ConversationPartyBusiness.getInstance().getConversationPartyPerType().get(ConversationParty.TEACHER_TYPE)) {
                    if (!selectedTeachers.contains(party.getPartyId())) {
                        selectedTeachers.add(party.getPartyId());
                    }
                }
            }
            ConversationPartyBusiness.getInstance().selectAll(!anySelected);
        } else {
            List<Child> children = ChildBusiness.getInstance().getChildren(group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
            boolean childSelected = false;
            for (Child child : children) {
                if (child.isSelected()) {
                    childSelected = true;
                }
            }
            if (childSelected) {
                //Unselect all children of this group
                for (Child child : children) {
                    ConversationBusiness.getInstance().getSelectedCarersPerChild().remove(child.getId());
                }
                ChildBusiness.getInstance().selectChildrenFromGroup(group.getId(), false, false, TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
            } else {
                //Select default carers for each childId of the group
                for (Child child : children) {
                    LoadCarersAndSelectDefaultRecipientsAsyncTask task = new LoadCarersAndSelectDefaultRecipientsAsyncTask(child.getId());
                    getPendingTasks().add(task);
                    task.execute();
                }
            }
        }
    }

    class LoadCarersAndSelectDefaultRecipientsAsyncTask extends CarerBusiness.LoadCarers {

        public LoadCarersAndSelectDefaultRecipientsAsyncTask(long child) {
            super(child);
        }

        @Override
        protected void onPostExecute(List<ChildCarer> childCarers) {
            super.onPostExecute(childCarers);
            if (!checkNetworkException(childCarers, exception)) {
                super.onPostExecute(childCarers);
                Set<Long> selectedCarers = ConversationBusiness.getInstance().getSelectedCarersPerChild().get(childId);
                if (selectedCarers == null) {
                    selectedCarers = new HashSet<>();
                    ConversationBusiness.getInstance().getSelectedCarersPerChild().put(childId, selectedCarers);
                }
                if (childCarers != null) {
                    for (ChildCarer carer : childCarers) {
                        if (carer.isDefaultConversationParty()) {
                            selectedCarers.add(carer.getId());
                        }
                    }
                }
                if (!selectedCarers.isEmpty()) {
                    ChildBusiness.getInstance().selectChild(childId, true);
                }
            }
            getPendingTasks().remove(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChildBusiness.getInstance().unselectAllChildren();
        ConversationPartyBusiness.getInstance().unselectAll();
    }

    @Override
    protected void setResult() {
        //Carers and children should be retrieved with ConversationBusiness.getInstance().getSelectedCarersPerChild()
        Intent intent = new Intent();
        long[] teacherIdsArray = ArrayUtils.longCollectionToArray(selectedTeachers);

        intent.putExtra(SELECTED_TEACHER_IDS, teacherIdsArray);
        setResult(Activity.RESULT_OK, intent);
    }

    private void initGroupTabs() {
        List<Group> groups = GroupBusiness.getInstance().getTeacherGroups(TeacherBusiness.getInstance().getCurrentTeacher(), true);
        for (Group group : groups) {
            TabHost.TabSpec ts = groupTabHost.newTabSpec("GroupSubtab " + group.getName()).setIndicator(createTabView(group.getName()));
            Bundle bundle = new Bundle();
            bundle.putLong(PeopleSelectionGroupFragment.GROUP_ID, group.getId());
            bundle.putBoolean(PeopleSelectionGroupFragment.ADD_TEACHERS, true);
            groupTabHost.addTab(ts, CarerSelectionGroupFragment.class, bundle);
        }
        //Add Teacher group
        TabHost.TabSpec ts = groupTabHost.newTabSpec("GroupSubtab teacher").setIndicator(createTabView(getString(R.string.Teachers)));
        groupTabHost.addTab(ts, TeacherSelectionGroupFragment.class, new Bundle());
    }

    private class GetConversationPartiesAsyncTask extends ConversationPartyBusiness.LoadConversationParties {

        public GetConversationPartiesAsyncTask(List<Long> selected) {
            super(selected);
        }

        @Override
        protected void onPreExecute() {
            showProgress();
            getPendingTasks().add(this);
        }

        protected List<ConversationParty> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.Conversations).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<ConversationParty> conversationParties) {
            getPendingTasks().remove(this);
            if (isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && isDestroyed())) {
                return;
            }
            hideProgress();
            if (!checkNetworkException(conversationParties, exception)) {
                initGroupTabs();
            }
        }
    }
}
