package com.kindiedays.teacherapp.ui.peopleselection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.ChildTouchListener;
import com.kindiedays.common.ui.SectionedGridRecyclerViewAdapter;
import com.kindiedays.common.ui.personselection.PeopleSelectionFragment;
import com.kindiedays.common.ui.personselection.PersonButtonAdapter;
import com.kindiedays.common.ui.personselection.teacherselection.TeacherButtonAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.peopleselection.recipientselection.RecipientSelectionActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 18/06/2015.
 */
public class TeacherSelectionGroupFragment extends PeopleSelectionFragment {

    public static final String GROUP_ID = "groupId";

    private static final ConversationParty CONVERSATION_PARTY_GROUP = new ConversationParty();

    @Override
    public void onStart() {
        super.onStart();
        ConversationPartyBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ConversationPartyBusiness.getInstance().deleteObserver(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        List<ConversationParty> conversationParties = new ArrayList<>(ConversationPartyBusiness.getInstance().getConversationPartyPerType().get(ConversationParty.TEACHER_TYPE));

        List<SectionedGridRecyclerViewAdapter.Section> sections = new ArrayList<>();

        List<Person> personList = new ArrayList<>();

        if (conversationParties.size() != 0) {
            sections.add(new SectionedGridRecyclerViewAdapter.Section(0, new String[]{getString(R.string.Teachers)}, CONVERSATION_PARTY_GROUP));
            personList.addAll(conversationParties);
        }

        ((TeacherButtonAdapter) personAdapter).setParties(personList);

        SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
        SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                SectionedGridRecyclerViewAdapter(getActivity(), R.layout.item_group_section, new int[]{R.id.groupName}, recyclerView, personAdapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));
        recyclerView.setAdapter(mSectionedAdapter);
        recyclerView.addOnItemTouchListener(new ChildTouchListener(this));
        return v;
    }

    @Override
    protected PersonButtonAdapter initPeopleAdapter() {
        return new TeacherButtonAdapter(getActivity());
    }

    @Override
    public void onChildButtonTapped(View childView) {
        if (childView != null && childView.getTag() instanceof ConversationParty) {
            ConversationParty conversationParty = (ConversationParty) childView.getTag();
            if (CONVERSATION_PARTY_GROUP == conversationParty) {
                ((PeopleGroupSelectionActivity) getActivity()).onGroupClick(RecipientSelectionActivity.TEACHER_GROUP);
            } else {
                ((PeopleGroupSelectionActivity) getActivity()).onPersonClick(conversationParty);
            }
        }
    }
}
