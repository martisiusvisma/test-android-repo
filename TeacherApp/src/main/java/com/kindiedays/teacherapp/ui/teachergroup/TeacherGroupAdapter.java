package com.kindiedays.teacherapp.ui.teachergroup;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindiedays.common.pojo.Group;
import com.kindiedays.teacherapp.R;

import java.util.List;

/**
 * Created by pleonard on 29/01/2016.
 */
public class TeacherGroupAdapter extends BaseAdapter {

    List<Group> groups;
    Activity activity;

    public TeacherGroupAdapter(List<Group> groups, Activity activity){
        this.groups = groups;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Group getItem(int i) {
        return groups.get(i);
    }

    @Override
    public long getItemId(int i) {
        return groups.get(i).getId();
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view == null) {
            view = activity.getLayoutInflater().inflate(R.layout.item_group_selection, null);
        }
        Group group = getItem(i);
        ((TextView)view).setText(group.getName());
        view.setTag(group.getId());
        return view;
    }


}
