package com.kindiedays.teacherapp.ui.messages;

import android.os.Bundle;
import android.view.MenuItem;

import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 23/09/2015.
 */
public class ChildConversationActivity extends KindieDaysActivity {

    public static final String CHILD_ID = "childId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long childId = getIntent().getLongExtra(CHILD_ID, -1);
        setContentView(R.layout.activity_child_conversation);
        Bundle arguments = new Bundle();
        arguments.putLong(ConversationsFragment.CHILD_ID, childId);
        ConversationsFragment fragment = new ConversationsFragment();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction().replace(R.id.conversationFragmentHolder, fragment).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        } else {
            return false;
        }
    }
}
