package com.kindiedays.teacherapp.ui.childprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.ui.journal.JournalPostDisplayer;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.newjournalpost.NewJournalPostActivity;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;
import java.util.TreeMap;

/**
 * Created by pleonard on 19/05/2015.
 */

public class JournalFragment extends Fragment {

    private Button newJournalPostButton;
    private JournalPostDisplayer journalPostDisplayer = new JournalPostDisplayer();
    private TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPosts;
    private List<Place> places;

    View.OnClickListener onNewPostClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), NewJournalPostActivity.class);
            intent.putExtra(NewJournalPostActivity.CHILD_ID, ((ChildProfileActivity) getActivity()).getChild().getId());
            startActivity(intent);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if(journalPostDisplayer.getJournalPosts() != null) {
            newJournalPostButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_journal, null);
        journalPostDisplayer.setChild(((ChildProfileActivity) getActivity()).getChild());
        journalPostDisplayer.init(v, R.id.journalPostList, R.id.journalProgress,
                (JournalPostDisplayer.JournalPostSource) getActivity());
        journalPostDisplayer.setJournalPosts(journalPosts);
        journalPostDisplayer.setPlaces(places);
        newJournalPostButton = (Button) v.findViewById(R.id.newJournalPostButton);
        newJournalPostButton.setOnClickListener(onNewPostClickListener);
        return v;
    }

    @SuppressWarnings("squid:S1319")
    public void setJournalPosts(TreeMap<LocalDate, TreeMap<LocalTime, Object>> journalPosts) {
        this.journalPosts = journalPosts;
        journalPostDisplayer.setJournalPosts(journalPosts);
        if (newJournalPostButton != null && journalPostDisplayer.getJournalPosts() != null) {
            newJournalPostButton.setVisibility(View.VISIBLE);
        }
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
        journalPostDisplayer.setPlaces(places);
    }

}

