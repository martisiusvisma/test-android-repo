package com.kindiedays.teacherapp.ui.attendance;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.ChildTouchListener;
import com.kindiedays.common.ui.SectionedGridRecyclerViewAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.ChildHelper;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PlaceBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static com.kindiedays.common.business.PlaceBusiness.ABSENT_PLACE_ID;

/**
 * Created by pleonard on 22/04/2015.
 */
public class GroupFragment extends Fragment implements ChildTouchListener.OnChildButtonTouchListener, Observer {

    public static final String GROUP_ID = "groupId";
    private static final String RECYCLE_STATE = "recycle_state";

    private long groupId;
    private RecyclerView placeList;
    private EditText searchField;
    private ChildrenPlaceAdapter childrenAdapter;
    private Group group;
    private View header;
    private TextView selectDeselectTV;
    private AttendanceFragment owner;

    public interface OnMovedListenerPlace {
        void onMoved();
    }

    private OnMovedListenerPlace onMovedListener = new OnMovedListenerPlace() {
        @Override
        public void onMoved() {
            selectDeselectTV.setText(R.string.select_tv);
            owner.cancelMove(null);
        }
    };

    public void saveScrolledState() {
        getArguments().putParcelable(RECYCLE_STATE, placeList.getLayoutManager().onSaveInstanceState());
    }

    TextWatcher searchTextWatcher = new TextWatcher(){
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //ignore
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //ignore
        }

        @Override
        public void afterTextChanged(Editable s) {
            //Trigger new search
            updateSections();
            searchField.requestFocus();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            groupId = getArguments().getLong(GROUP_ID);
        }
        else{
            groupId = GroupBusiness.ALL_GROUP_ID;
        }
        owner = (AttendanceFragment) getParentFragment().getParentFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        owner.setOnMovedListenerPlace(onMovedListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_recyclerview, null);
        placeList = (RecyclerView) v.findViewById(R.id.childrenList);

        header = inflater.inflate(R.layout.header_container_list, null, false);
        TextView groupTv = (TextView) header.findViewById(R.id.containerName);
        searchField = (EditText) header.findViewById(R.id.search);
        searchField.addTextChangedListener(searchTextWatcher);

        selectDeselectTV = (TextView) header.findViewById(R.id.select_deselect_tv);
        selectDeselectTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveScrolledState();
                if (selectDeselectTV.getText().equals(getString(R.string.select_tv))){
                    selectDeselectTV.setText(R.string.deselect_tv);
                    owner.enterMoveMode();
                } else {
                    selectDeselectTV.setText(R.string.select_tv);
                    owner.cancelMove(null);
                }
            }
        });

        placeList.setHasFixedSize(true);
        placeList.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        group = GroupBusiness.getInstance().getGroup(groupId);

        if(group != null) {
            groupTv.setText(group.getName());
        }
        groupTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AttendanceFragment owner = (AttendanceFragment) getParentFragment().getParentFragment();
                ChildBusiness.getInstance().selectChildrenFromGroup(groupId, true, true, TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
                owner.enterMoveMode();
            }
        });

        childrenAdapter = new ChildrenPlaceAdapter(getActivity());
        updateSections();
        placeList.addOnItemTouchListener(new ChildTouchListener(this));

        return v;
    }

    public void updateSections(){
        List<SectionedGridRecyclerViewAdapter.Section> sections = new ArrayList<>();
        List<Place> places;
        int currentPosition = 0;
        List<Child> children = new ArrayList<>();
        Teacher currentTeacher = TeacherBusiness.getInstance().getCurrentTeacher();

        if (currentTeacher != null && group != null) {
            places = PlaceBusiness.getInstance().getContainers(group.getId() == GroupBusiness.ALL_GROUP_ID);
            if (places != null) {
                for (Place _place : places) {
                    List<Child> childrenOfPlace = ChildBusiness.getInstance().getChildren(group.getId(), _place.getId(), searchField.getText().toString(), _place.isMainBuilding(), currentTeacher.getGroups());
                    if (childrenOfPlace != null && !childrenOfPlace.isEmpty()) {
                        sections.add(new SectionedGridRecyclerViewAdapter.Section(currentPosition, new String[]{_place.getName()}, _place));
                        currentPosition += childrenOfPlace.size();
                        children.addAll(childrenOfPlace);
                    }
                }
            }
        }
        SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
        SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                SectionedGridRecyclerViewAdapter(getActivity(),R.layout.item_place_section,new int[]{R.id.placeName}, placeList, childrenAdapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));
        placeList.setAdapter(mSectionedAdapter);
        mSectionedAdapter.setHeader(header);
        childrenAdapter.setChildren(children);

        Bundle args = getArguments();
        if (args != null) {
            Parcelable recycleViewState = getArguments().getParcelable(RECYCLE_STATE);
            placeList.getLayoutManager().onRestoreInstanceState(recycleViewState);
        } else if (placeList.getAdapter().getItemCount() > 0){
            placeList.scrollToPosition(1);
        }
    }

    @Override
    public void onChildButtonTapped(View v) {
        saveScrolledState();

        if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_MOVE) {
            if (v != null && v.getTag() instanceof Child
                    && ChildHelper.getStatus(((ChildAttendanceButton) v).getChild()) == ChildHelper.ABSENT){
                return;
            }
        }

        if(v != null && v.getTag() instanceof Child){
            ChildAttendanceButton childAttendanceButton = (ChildAttendanceButton) v;

            if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_NORMAL) {
                Child child = childAttendanceButton.getChild();
                owner.openChildProfile(child.getId());

            } else if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_MOVE) {
                ChildBusiness.getInstance().selectChild(childAttendanceButton.getChild().getId(), !childAttendanceButton.getChild().isSelected()); //same selection state than button
                childAttendanceButton.changeSelectionState();
            }
        }

        if(v != null && v.getTag() instanceof Place && ((Place) v.getTag()).getId() != ABSENT_PLACE_ID){
            selectDeselectTV.setText(R.string.deselect_tv);
            //Select all signed in children
            ChildBusiness.getInstance().selectChildrenFromGroupAndPlace(group.getId(), ((Place) v.getTag()).getId()
                    , searchField.getText().toString(), true, true, TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
            //Display move to dialog
            owner.enterMoveMode();
        }
    }

    @Override
    public void onChildButtonLongPress(View v) {
        if(v instanceof ChildAttendanceButton) {
            ChildAttendanceButton childAttendanceButton = (ChildAttendanceButton) v;
            AttendanceFragment owner = (AttendanceFragment) getParentFragment().getParentFragment();
            if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_NORMAL) {
                Child child = childAttendanceButton.getChild();
                int i = ChildHelper.getStatus(child);
                if (i == ChildHelper.ABSENT || i == ChildHelper.SIGNED_OUT) {
                    long newPlaceId = PlaceBusiness.getInstance().getMainPlace().getId();
                    owner.signIn(child, newPlaceId);
                    child.setCurrentPlaceId(newPlaceId);

                } else if (i == ChildHelper.SIGNED_IN) {
                    owner.signOut(child);
                    child.setCurrentPlaceId(PlaceBusiness.NO_PLACE_ID);

                }

                saveScrolledState();
                childAttendanceButton.launchAnimation();
                childAttendanceButton.setChild(child);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ChildBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ChildBusiness.getInstance().deleteObserver(this);
    }


    @Override
    public void update(Observable o, Object data) {
        if (o instanceof ChildBusiness){
            updateSections();
        }
    }
}
