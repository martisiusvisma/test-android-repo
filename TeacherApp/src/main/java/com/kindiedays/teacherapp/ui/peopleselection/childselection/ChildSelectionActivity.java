package com.kindiedays.teacherapp.ui.peopleselection.childselection;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TabHost;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleGroupSelectionActivity;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleSelectionGroupFragment;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 11/06/2015.
 */
public class ChildSelectionActivity extends PeopleGroupSelectionActivity {

    private Set<Long> preselectedChildren = new HashSet<>();
    private List<Long> newlySelectedChildren = new ArrayList<>();

    public static final String SELECTED_READ_ONLY = "selectedReadOnly";
    public static final String SELECTED_RW = "selectedRW";

    protected boolean noRedrawTabNeeded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent().hasExtra(SELECTED_READ_ONLY)){
            long[] invitationsSentTo = getIntent().getLongArrayExtra(SELECTED_READ_ONLY);
            for(int i = 0 ; i < invitationsSentTo.length ; i++){
                long childId = invitationsSentTo[i];
                preselectedChildren.add(childId);
                ChildBusiness.getInstance().selectChild(childId, true);
            }
        }

        if(getIntent().hasExtra(SELECTED_RW)){
            long[] invitationsNotSentYet = getIntent().getLongArrayExtra(SELECTED_RW);
            for(int i = 0 ; i < invitationsNotSentYet.length ; i++){
                long childId = invitationsNotSentYet[i];
                newlySelectedChildren.add(childId);
                ChildBusiness.getInstance().selectChild(childId, true);
            }
        }
        new GetConversationPartiesAsyncTask(selectedTeachers).execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChildBusiness.getInstance().unselectAllChildren();
    }

    @Override
    protected void setResult() {
        Intent intent = new Intent();
        long[] selectedChildrenIds = new long[newlySelectedChildren.size()];
        for(int i = 0 ; i < newlySelectedChildren.size() ; i++){
            selectedChildrenIds[i] = newlySelectedChildren.get(i);
        }

        intent.putExtra(SELECTED_CHILDREN_IDS, selectedChildrenIds);
        setResult(Activity.RESULT_OK, intent);
    }


    @Deprecated
    @Override
    protected void initTabs() {
        /*
         * replaced with {@link #initGroupTabs()}
         */
    }

    private void initGroupTabs() {
        List<Group> groups = GroupBusiness.getInstance().getTeacherGroups(TeacherBusiness.getInstance().getCurrentTeacher(), true);
        for(Group group : groups){
            TabHost.TabSpec ts = groupTabHost.newTabSpec("GroupSubtab " + group.getName()).setIndicator(createTabView(group.getName()));
            Bundle bundle = new Bundle();
            bundle.putLong(PeopleSelectionGroupFragment.GROUP_ID, group.getId());
            groupTabHost.addTab(ts, ChildSelectionGroupFragment.class, bundle);
        }
    }

    @Override
    public void onPersonClick(Person child) {
        if(!child.isSelected()){
            newlySelectedChildren.add(child.getId());
            ChildBusiness.getInstance().selectChild(child.getId(), true);
        }
        else{
            if(!preselectedChildren.contains(child.getId())){
                newlySelectedChildren.remove(child.getId());
                ChildBusiness.getInstance().selectChild(child.getId(), false);
            }
        }
    }

    @Override
    public void onPersonLongClick(Person person) {
        //no impl
    }

    @Override
    public void onGroupClick(Group group) {
        List<Child> children = ChildBusiness.getInstance().getChildren(group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
        List<Child> newlySelectedFromGroup = new ArrayList<>();
        for(Child child : children){
            if(newlySelectedChildren.contains(child.getId())){
                newlySelectedFromGroup.add(child);
            }
        }
        if(!newlySelectedFromGroup.isEmpty()){
            //Unselect newly selected
            ChildBusiness.getInstance().selectChildren(newlySelectedFromGroup, false, false);
            for(Child child : newlySelectedFromGroup){
                newlySelectedChildren.remove(child.getId());
            }
        }
        else{
            //Select
            for(Child child : children){
                if(!child.isSelected()){
                    newlySelectedChildren.add(child.getId());
                }
                else{
                    if(!preselectedChildren.contains(child.getId())){
                        newlySelectedChildren.remove(child.getId());
                    }
                }
            }
            ChildBusiness.getInstance().selectChildren(children, false, true);
        }
    }

    private class GetConversationPartiesAsyncTask extends ConversationPartyBusiness.LoadConversationParties {

        public GetConversationPartiesAsyncTask(List<Long> selected) {
            super(selected);
        }

        @Override
        protected void onPreExecute() {
            showProgress();
            getPendingTasks().add(this);
        }

        protected List<ConversationParty> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.Conversations).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<ConversationParty> conversationParties) {
            getPendingTasks().remove(this);
            if (isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && isDestroyed())) {
                return;
            }
            hideProgress();
            if (!checkNetworkException(conversationParties, exception)) {
                if (!noRedrawTabNeeded) {
                    initGroupTabs();
                }

            }
        }
    }
}
