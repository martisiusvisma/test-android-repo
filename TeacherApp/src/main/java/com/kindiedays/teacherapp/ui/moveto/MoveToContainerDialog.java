package com.kindiedays.teacherapp.ui.moveto;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kindiedays.common.business.ContainerBusiness;
import com.kindiedays.common.pojo.ChildContainer;
import com.kindiedays.teacherapp.R;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by pleonard on 20/05/2015.
 */
public abstract class MoveToContainerDialog<T extends ChildContainer> extends LinearLayout implements Observer {
    private ContainerBusiness containerBusiness;
    protected ChildContainerMover owner;

    View.OnClickListener buttonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            T place = (T) v.getTag();
            owner.moveChildren(MoveToContainerDialog.this, place.getId());
        }
    };

    public MoveToContainerDialog(Context context, ContainerBusiness containerBusiness) {
        super(context);
        this.containerBusiness = containerBusiness;
        initViews();
    }

    public MoveToContainerDialog(Context context, AttributeSet attrs, ContainerBusiness containerBusiness) {
        super(context, attrs);
        this.containerBusiness = containerBusiness;
        initViews();
    }

    public MoveToContainerDialog(Context context, AttributeSet attrs, int defStyleAttr, ContainerBusiness containerBusiness) {
        super(context, attrs, defStyleAttr);
        this.containerBusiness = containerBusiness;
        initViews();
    }

    public void setOwner(ChildContainerMover owner) {
        this.owner = owner;
    }

    @Override
    public void update(Observable observable, Object data) {
        refreshContainerList();
    }

    public interface ChildContainerMover {
        void moveChildren(MoveToContainerDialog dialog, long containerId);

        void cancelMove(MoveToContainerDialog dialog);
    }

    protected void initViews() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.dialog_move_to, this);
        Button cancel = (Button) v.findViewById(R.id.cancel);
//        cancel.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                owner.cancelMove(MoveToContainerDialog.this);
//            }
//        });
        containerSpecificInitViews(v);
        refreshContainerList();
    }

    protected abstract void containerSpecificInitViews(View v);

    private void refreshContainerList() {
        LinearLayout placeContainer = (LinearLayout) this.findViewById(R.id.placesContainer);
        placeContainer.removeAllViews();
        List<T> containers = containerBusiness.getContainers(false);
        if (containers != null) {
            for (T container : containers) {
                Button placeButton = new Button(getContext());
                placeButton.setText(container.getName());
                placeButton.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.circleborder));
                int pixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getContext().getResources().getDisplayMetrics());
                int marginPixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getContext().getResources().getDisplayMetrics());
                placeButton.setTag(container);
                placeButton.setTransformationMethod(null);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pixels, pixels);
                params.weight = 1.0f;
                params.gravity = Gravity.CENTER;
                params.setMargins(marginPixels, 0, marginPixels, 0);

                placeButton.setLayoutParams(params);
                placeContainer.addView(placeButton);
                placeButton.setOnClickListener(buttonClicked);
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        containerBusiness.addObserver(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        containerBusiness.deleteObserver(this);
        super.onDetachedFromWindow();
    }

}
