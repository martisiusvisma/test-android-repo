package com.kindiedays.teacherapp.ui.album;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.PicturesBusiness;
import com.kindiedays.teacherapp.ui.picturegallery.PictureGalleryActivity;

/**
 * Created by pleonard on 14/07/2015.
 */
@SuppressWarnings("squid:S2176")
public class AlbumFragment extends com.kindiedays.common.ui.album.AlbumFragment {

    @SuppressWarnings("squid:S1188")
    public AlbumFragment(){
        onPictureClickListener = new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
                intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
                getActivity().startActivity(intent);
            }
        };
        onPictureLongClickListener = new AdapterView.OnItemLongClickListener(){

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true)
                    .setMessage(getString(R.string.Delete_Picture))
                    .setTitle(getString(R.string.Delete_Picture))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DeletePictureAsyncTask task = new DeletePictureAsyncTask(PicturesBusiness.getInstance().getCurrentPictureList().getPictures().get(position));
                            task.execute();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                builder.show();
                return true;
            }
        };

    }

    class DeletePictureAsyncTask extends com.kindiedays.teacherapp.business.PicturesBusiness.DeletePictureAsyncTask{

        public DeletePictureAsyncTask(GalleryPicture galleryPicture) {
            super(galleryPicture);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(!checkNetworkException(aVoid, exception)){
                super.onPostExecute(aVoid);
                PicturesBusiness.getInstance().getCurrentPictureList().getPictures().remove(galleryPicture);
                thumbnailAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void refreshData() {
        GetPicturesAsyncTask task = new GetPicturesAsyncTask();
        pendingTasks.add(task);
        task.execute();
    }

    class GetPicturesAsyncTask extends PicturesBusiness.GetPicturesAsyncTask{

        @Override
        protected void onPostExecute(PictureList pictureList) {

            if(!checkNetworkException(pictureList, exception)){
                super.onPostExecute(pictureList);
                thumbnailAdapter.setPictures(pictureList.getPictures());
            }
            progressBar.setVisibility(View.GONE);
            pendingTasks.remove(this);
            isUpdating = false;
        }
    }
}
