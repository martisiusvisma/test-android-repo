package com.kindiedays.teacherapp.ui.dailyreport;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kindiedays.common.components.CheckedTextViewCustom;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.daily.ResourceSquaredTab;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.NapRes;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRNapsDialogFragment extends MainActivityDialogFragment {
    private HorizontalScrollView counterTabHostlScrollView;
    private TextViewCustom title;

    private List<NapRes> napItems = new ArrayList<>();
    private EditText bodyEt;
    private Button startPickerEt;
    private Button endPickerEt;
    private FragmentTabHost napTabHost;
    private View loaderContainer;

    private static final String TYPE = "Type";
    private int napCounter = 0;
    private int selectedTab = 0;
    private List<Child> childOrChildren = new ArrayList<>();

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            List<NapRes> outgoingItems = new ArrayList<>();
            for (int i = 0; i < napItems.size(); i++) {
                if (napItems.get(i).getLocal()) {
                    outgoingItems.add(napItems.get(i));
                }
            }
            if (!outgoingItems.isEmpty()) {
                for (int i = 0; i < outgoingItems.size(); i++) {
                    if (outgoingItems.get(i).getChildren() != null) {
                        for (int n = 0; n < outgoingItems.get(i).getChildren().size(); n++) {
                            NapRes newNapItem = new NapRes();
                            newNapItem.setChild(outgoingItems.get(i).getChildren().get(n));
                            newNapItem.setDate(outgoingItems.get(i).getDate());
                            newNapItem.setEnd(outgoingItems.get(i).getEnd());
                            newNapItem.setLocal(outgoingItems.get(i).getLocal());
                            newNapItem.setStart(outgoingItems.get(i).getStart());
                            newNapItem.setNotes(outgoingItems.get(i).getNotes());
                            outgoingItems.add(newNapItem);
                        }
                        outgoingItems.remove(i);
                    }
                }
                SaveNewNapTask task = new SaveNewNapTask(outgoingItems);
                pendingTasks.add(task);
                task.execute();
            } else {
                dismiss();
            }
        }
    };

    @Override
    public void onDestroyView() {
        selectedTab = 0;
        napCounter = 0;
        napItems.clear();
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        dismiss();
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void refreshData() {
        // We should use this to get the existing data
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Naps));
        if (childOrChildren != null && childOrChildren.size() == 1) {
            DateTime now = new DateTime();
            LoadNapTask task = new LoadNapTask((int) childOrChildren.get(0).getId(), now);
            pendingTasks.add(task);
            task.execute();
        } else {
            NapRes newNapItem = new NapRes();
            ArrayList<Integer> childrenId = new ArrayList<>();
            for (Child str : childOrChildren) {
                childrenId.add((int) str.getId());
            }
            newNapItem.setChildren(childrenId);
            DateTime now = new DateTime();
            newNapItem.setDate(now);
            newNapItem.setLocal(true);
            napItems.add(newNapItem);
            napCounter++;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dr_naps, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        scrollingView = (ScrollView) v.findViewById(R.id.parentLayout);
        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        TextViewCustom childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);

        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder();
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }
        counterTabHostlScrollView = (HorizontalScrollView) v.findViewById(R.id.counterTabHostlScrollView);

        CheckedTextViewCustom timePickerTitle = (CheckedTextViewCustom) v.findViewById(R.id.timePickerTitle);
        timePickerTitle.setText(getString(com.kindiedays.common.R.string.Starts));
        startPickerEt = (Button) v.findViewById(R.id.startPickerEt);
        endPickerEt = (Button) v.findViewById(R.id.endPickerEt);

        CheckedTextViewCustom endPickerTitle = (CheckedTextViewCustom) v.findViewById(R.id.endPickerTitle);
        endPickerTitle.setText(getString(com.kindiedays.common.R.string.Ends));

        napTabHost = (FragmentTabHost) v.findViewById(android.R.id.tabhost);
        napTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);
        napTabHost.addTab(napTabHost.newTabSpec("item0").setIndicator(createSquaredTabView("1")), NapFragment.class, new Bundle());
        napTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                selectedTab = napTabHost.getCurrentTab();
                refresh();
            }
        });

        Button addItemBtn = (Button) v.findViewById(R.id.addBtn);
        addItemBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
                    String newId = String.valueOf(napCounter + 1);
                    indicator.setResourceName(newId);
                    TabHost.TabSpec ts = napTabHost.newTabSpec("item" + newId).setIndicator(indicator);
                    Bundle bundle = new Bundle();
                    bundle.putString(TYPE, newId);
                    napTabHost.addTab(ts, NapFragment.class, bundle);
                    napCounter = napCounter + 1;

                    counterTabHostlScrollView.postDelayed(new Runnable() {
                        public void run() {
                            counterTabHostlScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                            int currentTab = napTabHost.getCurrentTab();
                            napTabHost.setCurrentTab(currentTab + 1);
                            refresh();
                        }
                    }, 100L);

                    NapRes newNapItem = new NapRes();
                    if (childOrChildren.size() == 1) {
                        newNapItem.setChild((int) childOrChildren.get(0).getId());
                    } else {
                        ArrayList<Integer> childrenId = new ArrayList<>();
                        for (Child str : childOrChildren) {
                            childrenId.add((int) str.getId());
                        }
                        newNapItem.setChildren(childrenId);
                    }
                    DateTime now = new DateTime();
                    newNapItem.setDate(now);
                    newNapItem.setLocal(true);
                    napItems.add(newNapItem);
                }
                return false;
            }
        });

        startPickerEt = (Button) v.findViewById(R.id.startPickerEt);
        startPickerEt.setInputType(InputType.TYPE_NULL);
        startPickerEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int mHour;
                int mMinute;
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.timePickerStyle,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    startPickerEt.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                                    for (int i = 0; i < napItems.size(); i++) {
                                        if (i == selectedTab) {
                                            DateTime dateTime = new DateTime()
                                                    .withHourOfDay(hourOfDay)
                                                    .withMinuteOfHour(minute)
                                                    .withZone(DateTimeZone.UTC);
                                            napItems.get(i).setStart(dateTime);
                                        }
                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
                return false;
            }
        });
        endPickerEt = (Button) v.findViewById(R.id.endPickerEt);
        endPickerEt.setInputType(InputType.TYPE_NULL);
        endPickerEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int mHour;
                int mMinute;
                if (MotionEvent.ACTION_UP == event.getAction()) {

                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.timePickerStyle,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    endPickerEt.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                                    for (int i = 0; i < napItems.size(); i++) {
                                        if (i == selectedTab) {
                                            DateTime dateTime = new DateTime()
                                                    .withHourOfDay(hourOfDay)
                                                    .withMinuteOfHour(minute)
                                                    .withZone(DateTimeZone.UTC);
                                            napItems.get(i).setEnd(dateTime);
                                        }
                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
                return false;
            }
        });

        bodyEt = (EditText) v.findViewById(R.id.bodyEdit);
        bodyEt.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                for (int i = 0; i < napItems.size(); i++) {
                    if (i == selectedTab) {
                        napItems.get(i).setNotes(bodyEt.getText().toString());
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                scrollingView.fullScroll(View.FOCUS_DOWN);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ignore
            }
        });

        Button okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickedListener);
        loaderContainer = v.findViewById(R.id.loader_container);
        return v;
    }

    public static class NapFragment extends Fragment {
    }

    private ResourceSquaredTab createSquaredTabView(String current) {

        ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
        indicator.setResourceName(current);
        return indicator;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
        }
        super.onDismiss(dialog);
    }

    class SaveNewNapTask extends DailyReportBusiness.PostNapTask {
        public SaveNewNapTask(List<NapRes> items) {
            super(items);
        }

        @Override
        protected void onPostExecute(List<NapRes> items) {
            if (!checkNetworkException(items, exception)) {
                super.onPostExecute(items);
                Log.d(TAG, "Nap created");
            }
            pendingTasks.remove(this);
            dismiss();
        }
    }


    class LoadNapTask extends DailyReportBusiness.GetNapTask {

        public LoadNapTask(Integer child, DateTime data) {
            super(child, data);
        }

        @Override
        protected List<NapRes> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPreExecute() {
            loaderContainer.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<NapRes> items) {
            super.onPostExecute(items);
            if (!checkNetworkException(items, exception)) {
                final int tabSize = items.size();
                if (tabSize != 0) {
                    napItems = items;
                    napCounter = items.size();

                    // I start from one cause the first tab is fixed to dont to leave the tabhost empty
                    napTabHost.getTabWidget().removeView(napTabHost.getTabWidget().getChildTabViewAt(0));
                    for (int i = 0; i < tabSize; i++) {
                        String newId = String.valueOf(i + 1);
                        TabHost.TabSpec tsMood = napTabHost.newTabSpec("item" + newId).setIndicator(createSquaredTabView(newId));
                        Bundle bundleMood = new Bundle();
                        bundleMood.putString(TYPE, newId);
                        napTabHost.addTab(tsMood, NapFragment.class, bundleMood);
                        if ("item1".equals(tsMood.getTag())) {
                            napTabHost.getTabWidget().getChildAt(0).setSelected(true);
                        }
                    }

                    NapRes newNapItem = new NapRes();
                    newNapItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newNapItem.setDate(now);
                    newNapItem.setLocal(true);
                    napItems.add(newNapItem);

                    String newId = String.valueOf(items.size());
                    TabHost.TabSpec tsMood = napTabHost.newTabSpec("item" + newId).setIndicator(createSquaredTabView(newId));
                    Bundle bundleMood = new Bundle();
                    bundleMood.putString(TYPE, newId);
                    napTabHost.addTab(tsMood, NapFragment.class, bundleMood);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            napTabHost.getTabWidget().getChildAt(tabSize).performClick();
                        }
                    }, 1);
                } else {
                    // If there are not datas for the current date i create an object and sign the current selection on that object
                    NapRes newNapItem = new NapRes();
                    newNapItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newNapItem.setDate(now);
                    newNapItem.setLocal(true);
                    napItems.add(newNapItem);
                    napCounter++;
                }
                refresh();
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), getString(R.string.failed_to_load_message, getString(R.string.nap_list)), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
                dismiss();
            }
            loaderContainer.setVisibility(View.GONE);
            pendingTasks.remove(this);
        }
    }

    public void refresh() {
        for (int i = 0; i < napItems.size(); i++) {
            if (i == selectedTab) {
                if (napItems.get(i).getStart() != null) {
                    startPickerEt.setText(napItems.get(i).getStart().toString("HH:mm"));
                } else {
                    startPickerEt.setText(R.string.select_button_dialog_daily_report);
                }
                if (napItems.get(i).getEnd() != null) {
                    endPickerEt.setText(napItems.get(i).getEnd().toString("HH:mm"));
                } else {
                    endPickerEt.setText(R.string.select_button_dialog_daily_report);
                }
                if (napItems.get(i).getNotes() != null) {
                    bodyEt.setText(napItems.get(i).getNotes());
                } else {
                    bodyEt.setText("");
                }
                if (napItems.get(i).getLocal()) {
                    startPickerEt.setEnabled(true);
                    endPickerEt.setEnabled(true);
                    bodyEt.setEnabled(true);
                } else {
                    startPickerEt.setEnabled(false);
                    endPickerEt.setEnabled(false);
                    bodyEt.setEnabled(false);
                }
            }
        }
    }
}