package com.kindiedays.teacherapp.ui.peopleselection.tagselection;

import android.content.Context;
import android.view.ViewGroup;

import com.kindiedays.teacherapp.ui.peopleselection.ChildButtonAdapter;

/**
 * Created by pleonard on 07/10/2015.
 */
public class ChildTagSelectionButtonAdapter extends ChildButtonAdapter {

    public ChildTagSelectionButtonAdapter(Context context) {
        super(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PersonTagSelectionButton button = new PersonTagSelectionButton(mContext);
        return new ItemViewHolder(button);
    }
}
