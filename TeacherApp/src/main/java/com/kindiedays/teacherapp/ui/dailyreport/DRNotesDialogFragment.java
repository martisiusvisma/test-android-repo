package com.kindiedays.teacherapp.ui.dailyreport;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.NoteRes;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRNotesDialogFragment extends MainActivityDialogFragment {

    private static final String TAG = DRNotesDialogFragment.class.getName();

    private EditText bodyEditText;
    private List<Child> childOrChildren = new ArrayList<>();
    private SparseArray<NoteRes> existingNotes = new SparseArray<>();
    private TextViewCustom title;
    private TextViewCustom modified;
    private TextViewCustom created;

    private int numberOfLoadingNotes;

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            for (Child child : childOrChildren) {
                long childId = child.getId();
                NoteRes note = existingNotes.get((int) childId);

                if (note == null) {
                    note = new NoteRes();
                    note.setCreated(new DateTime());
                    note.setText(bodyEditText.getText().toString());
                    note.setChild(child.getId());
                    SaveNewNoteTask task = new SaveNewNoteTask(note);
                    task.execute();
                } else {
                    note.setModified(new DateTime());
                    note.setText(bodyEditText.getText().toString());
                    UpdateNoteTask task = new UpdateNoteTask(note);
                    task.execute();
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dr_notes, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);

        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);

        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        TextViewCustom childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);
        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder();
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }
        modified = (TextViewCustom) v.findViewById(R.id.modified);
        created = (TextViewCustom) v.findViewById(R.id.created);
        Button okButton = (Button) v.findViewById(R.id.okButton);
        bodyEditText = (EditText) v.findViewById(R.id.bodyEdit);
        bodyEditText.setEnabled(false);
        okButton.setOnClickListener(onOkClickedListener);
        return v;
    }

    @Override
    public void onDestroyView() {
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        dismiss();
        super.onPause();  // Always call the superclass method first
    }

    @Override
    protected boolean checkNetworkException(Object value, KindieDaysNetworkException exception) {
        boolean exceptionDetected = (exception != null && value == null);
        if (exceptionDetected) {
            if (exception.isLocalizedMessage() && exception.getStatusCode() != 404) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            } else if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                stopPendingTasks();
                Intent i = new Intent(getActivity(), ((KindieDaysApp) getActivity().getApplication()).getLoginActivityClass());
                startActivity(i);
            }
            Log.d(TAG, "An exception has been raised");
        }
        return exceptionDetected;
    }

    @Override
    public void refreshData() {
        // We should use this to get the existing data
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Notes));
        if (childOrChildren != null) {
            numberOfLoadingNotes = childOrChildren.size();
            DateTime now = new DateTime();
            for (Child child : childOrChildren) {
                LoadNoteTask task = new LoadNoteTask((int) child.getId(), now);
                pendingTasks.add(task);
                task.execute();
            }
        }
    }

    private void setUpNoteField() {
        NoteRes item = existingNotes.get((int) childOrChildren.get(0).getId());
        if (childOrChildren.size() == 1 && item != null) {

            bodyEditText.setText(item.getText());

            String createdStr = getResources().getString(R.string.created_at) + " ";
            String modifiedStr = getResources().getString(R.string.modified_at) + " ";

            created.setText(createdStr.concat(item.getCreated().toString("HH:mm")));
            if (item.getCreated().equals(item.getModified())) {
                modified.setVisibility(View.GONE);
            } else {
                modified.setText(modifiedStr.concat(item.getModified().toString("HH:mm")));
                modified.setVisibility(View.VISIBLE);
            }
        }
        bodyEditText.setEnabled(true);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
        }
        super.onDismiss(dialog);
    }

    class LoadNoteTask extends DailyReportBusiness.GetNoteTask {
        public LoadNoteTask(Integer child, DateTime data) {
            super(child, data);
        }

        @Override
        protected void onPostExecute(NoteRes item) {
            super.onPostExecute(item);
            if (getActivity() == null)
                return;

            if (!checkNetworkException(item, exception)) {
                if (item != null) {
                    existingNotes.put((int) item.getChild(), item);
                }
            }
            if (--numberOfLoadingNotes == 0) {
                setUpNoteField();
            }
            pendingTasks.remove(this);
        }
    }


    class UpdateNoteTask extends DailyReportBusiness.PutNoteTask {
        public UpdateNoteTask(NoteRes note) {
            super(note);
        }

        @Override
        protected void onPostExecute(NoteRes note) {
            if (getActivity() == null) {
                return;
            }
            if (!checkNetworkException(note, exception)) {
                super.onPostExecute(note);
            }
            dismiss();
        }
    }


    class SaveNewNoteTask extends DailyReportBusiness.PostNoteTask {
        public SaveNewNoteTask(NoteRes note) {
            super(note.getChild(), note.getCreated(), note.getText());
        }

        @Override
        protected void onPostExecute(NoteRes note) {
            if (getActivity() == null) {
                return;
            }
            if (!checkNetworkException(note, exception)) {
                super.onPostExecute(note);
            }
            dismiss();
        }
    }
}