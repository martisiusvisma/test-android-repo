package com.kindiedays.teacherapp.ui.newpicture;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.JournalBusiness;
import com.kindiedays.teacherapp.business.PicturesBusiness;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 14/12/2015.
 */
public class NewPictureActivity extends KindieDaysActivity {

    private String currentFragmentTag;
    private Fragment currentFragment;
    private RecyclerView pictureRoll;
    private PictureRollAdapter pictureRollAdapter;
    private List<File> pictureFiles = new ArrayList<>();
    private int selectedPictureIndex = NO_PICTURE_SELECTED;
    public static final int NO_PICTURE_SELECTED = -1;

    public static final String TAKE_PICTURE_FRAGMENT = "takePictureFragment";
    public static final String POST_PICTURE_FRAGMENT = "postPictureFragment";
    public static final String NEW_PICTURES_DIRECTORY = "/new_pictures";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int NEW_PICTURE_ACTIVITY = 1;
    public static final int SELECT_CHILDREN_ACTIVITY = 2;

    public static final String FORCE_PICTURE_MODE = "forcePictureMode";
    public static final String NEW_PICTURE_PREFERENCES = "newPicturePreferences";

    private static final String TAG = NewPictureActivity.class.getName();

    @Override
    @SuppressWarnings("squid:S1066")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_picture);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pictureRoll = (RecyclerView) findViewById(R.id.pictureRoll);

        LinearLayoutManager layoutManager;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        } else {
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        }

        pictureRoll.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() {

            GestureDetector mGestureDetector = new GestureDetector(NewPictureActivity.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
                View childView = view.findChildViewUnder(e.getX(), e.getY());
                if (childView != null && mGestureDetector.onTouchEvent(e)) {
                    if (POST_PICTURE_FRAGMENT.equals(currentFragmentTag)) {
                        //Save caption for currently displayed picture
                        if (selectedPictureIndex != NO_PICTURE_SELECTED) {
                            ((PostNewPicturesFragment) currentFragment).saveCurrentCaption();
                        }

                        //Change selected picture
                        selectedPictureIndex = view.getChildAdapterPosition(childView);
                        ((PostNewPicturesFragment) currentFragment).displaySelectedPicture();
                    }
                }
                return false;
            }

        });

        pictureRoll.setLayoutManager(layoutManager);
        pictureRollAdapter = new PictureRollAdapter();
        pictureRoll.setAdapter(pictureRollAdapter);
        final String pathToNewPictures = getApplicationInfo().dataDir + NewPictureActivity.NEW_PICTURES_DIRECTORY;
        File mediaStorageDir = new File(pathToNewPictures);
        File[] files = mediaStorageDir.listFiles();
        if (files != null) {
            for (File file : files) {
                pictureRollAdapter.addPicture(file);
                pictureFiles.add(file);
            }
        }
        SharedPreferences sharedPreferences = getSharedPreferences(NEW_PICTURE_PREFERENCES, MODE_PRIVATE);
        if (pictureFiles.isEmpty() || sharedPreferences.getBoolean(FORCE_PICTURE_MODE, false)) {
            openPage(TAKE_PICTURE_FRAGMENT);
        } else {
            selectedPictureIndex = 0;
            PicturesBusiness.getInstance().setNewPictures(pictureFiles);
            openPage(POST_PICTURE_FRAGMENT);
        }

    }

    @Override
    @SuppressWarnings("squid:S899")
    public void onDestroy() {
        if (!this.isChangingConfigurations()) {
            //Delete pictures
            final String pathToNewPictures = getApplicationInfo().dataDir + NewPictureActivity.NEW_PICTURES_DIRECTORY;
            File mediaStorageDir = new File(pathToNewPictures);
            File[] files = mediaStorageDir.listFiles();
            if (files != null) {
                for (File file : files) {
                    file.delete();
                }
            }
        }
        super.onDestroy();

    }

    public int getPicturesTaken() {
        return pictureRollAdapter.getItemCount();
    }

    public File getSelectedPictureFile() {
        if (selectedPictureIndex == NO_PICTURE_SELECTED) {
            return null;
        }
        return pictureFiles.get(selectedPictureIndex);
    }

    @SuppressWarnings({"squid:S899", "squid:S2250"})
    public void removeSelectedPicture() {
        File file = getSelectedPictureFile();
        if (file != null) {
            file.delete();
            pictureFiles.remove(file);
            pictureRollAdapter.removePicture(selectedPictureIndex);
            selectedPictureIndex--;
            if (selectedPictureIndex == NO_PICTURE_SELECTED && !pictureFiles.isEmpty()) {
                selectedPictureIndex = 0;
            }
            ((PostNewPicturesFragment) currentFragment).displaySelectedPicture();
        }
    }

    @Override
    public void onBackPressed() {
        if (TAKE_PICTURE_FRAGMENT.equals(currentFragmentTag)) {
            setResult(RESULT_CANCELED);
            finish();
        } else {
            openPage(TAKE_PICTURE_FRAGMENT);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }

    }

    private void openPage(String fragmentName) {
        Fragment fragment;
        //In case the user rotates the SCREEN, the activity will be recreated. It is important to persist which SCREEN he was on
        SharedPreferences.Editor editor = getSharedPreferences(NEW_PICTURE_PREFERENCES, MODE_PRIVATE).edit();
        if (TAKE_PICTURE_FRAGMENT.equals(fragmentName)) {
            fragment = new TakeNewPictureFragment();
            editor.putBoolean(FORCE_PICTURE_MODE, true);
        } else {
            fragment = new PostNewPicturesFragment();
            editor.putBoolean(FORCE_PICTURE_MODE, false);
        }
        editor.commit();
        currentFragment = fragment;
        currentFragmentTag = fragmentName;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment, currentFragmentTag)
                .commit();
        invalidateOptionsMenu();
    }

    /**
     * Create a File for saving an image or video
     */
    @SuppressWarnings("squid:S3398")
    private File getOutputMediaFile(int type) {
        final String pathToNewPictures = getApplicationInfo().dataDir + NEW_PICTURES_DIRECTORY;
        File mediaStorageDir = new File(pathToNewPictures);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d("MyCameraApp", "failed to create directory");
            return null;
        }


        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void savePictures(byte[] data) {
        SavePictureTask task = new SavePictureTask(data);
        task.execute();
    }

    public void openPostPage() {
        if (!pictureFiles.isEmpty()) {
            //Change fragment
            selectedPictureIndex = 0;
            PicturesBusiness.getInstance().setNewPictures(pictureFiles);
            openPage(POST_PICTURE_FRAGMENT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (POST_PICTURE_FRAGMENT.equals(currentFragmentTag)) { // Should always be true
            PostNewPicturesFragment postFragment = (PostNewPicturesFragment) currentFragment;
            if (requestCode == NEW_PICTURE_ACTIVITY) {
                if (resultCode == RESULT_OK) {
                    pictureRollAdapter = new PictureRollAdapter();
                    final String pathToNewPictures = getApplicationInfo().dataDir + NewPictureActivity.NEW_PICTURES_DIRECTORY;
                    File mediaStorageDir = new File(pathToNewPictures);
                    pictureFiles = new ArrayList<>(Arrays.asList(mediaStorageDir.listFiles()));
                    PicturesBusiness.getInstance().setNewPictures(pictureFiles);
                    for (File file : pictureFiles) {
                        pictureRollAdapter.addPicture(file);
                    }
                    pictureRoll.setAdapter(pictureRollAdapter);
                    if (!pictureFiles.isEmpty()) {
                        selectedPictureIndex = 0;
                        postFragment.displaySelectedPicture();
                    }
                } else {
                    finish();
                }

            } else if (requestCode == SELECT_CHILDREN_ACTIVITY && resultCode == RESULT_OK) {
                File file = pictureFiles.get(selectedPictureIndex);
                GalleryPicture picture = PicturesBusiness.getInstance().getPictureFromKey(Uri.fromFile(file).toString());
                postFragment.updateButtonText(picture);
            }
        }

    }

    public void sendPictures(boolean postToJournal) {
        UploadPicturesTask task = new UploadPicturesTask(pictureFiles, postToJournal);
        task.execute();
        getPendingTasks().add(task);
    }

    //region asynctasks
    class UploadPicturesTask extends PicturesBusiness.UploadPicturesAsyncTask {

        ProgressDialog progressDialog;
        boolean postToJournal;

        public UploadPicturesTask(List<File> files, boolean postToJournal) {
            super(files);
            this.postToJournal = postToJournal;
        }

        @Override
        protected void onPreExecute() {
            //Save caption for currently displayed picture
            ((PostNewPicturesFragment) currentFragment).saveCurrentCaption();
            //Display progress dialog
            progressDialog = new ProgressDialog(NewPictureActivity.this);
            progressDialog.setTitle(R.string.Upload_In_Progress);
            progressDialog.setMax(pictureFiles.size());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(1);
            progressDialog.setMax(pictureFiles.size());
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(getString(R.string.File_Upload, 1, pictureFiles.size()));
            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            int currentProgress = progressDialog.getProgress() + 1;
            progressDialog.setMessage(getString(R.string.File_Upload, currentProgress, pictureFiles.size()));
            progressDialog.setProgress(currentProgress);
        }

        @Override
        protected void onPostExecute(List<GalleryPicture> newPictures) {
            if (!checkNetworkException(newPictures, exception)) {
                super.onPostExecute(newPictures);
                if (postToJournal) {
                    //Create one journal post with ALL the pictures for ALL the children tagged in at least one of them
                    Set<ChildTag> taggedChildren = new HashSet<>();
                    Set<String> hashes = new HashSet<>();
                    for (GalleryPicture picture : newPictures) {
                        taggedChildren.addAll(picture.getChildTags());
                        hashes.add(picture.getHash());
                    }
                    Set<Long> childrenIds = new HashSet<>();
                    for (ChildTag taggedChild : taggedChildren) {
                        childrenIds.add(taggedChild.getChildId());
                    }
                    PostInJournalAsyncTask task = new PostInJournalAsyncTask(childrenIds, hashes);
                    task.execute();
                    getPendingTasks().add(task);
                } else {
                    finish();
                }
            }
            progressDialog.dismiss();
            getPendingTasks().remove(this);
        }
    }


    class PostInJournalAsyncTask extends JournalBusiness.PostJournalPostTask {
        public PostInJournalAsyncTask(Collection<Long> childrenIds, Collection<String> pictureHashes) {
            super("New journal post", childrenIds, pictureHashes);
        }

        @Override
        protected void onPostExecute(JournalPost journalPost) {
            if (!checkNetworkException(journalPost, exception)) {
                super.onPostExecute(journalPost);
            }
            finish();
            getPendingTasks().remove(this);
        }
    }


    class SavePictureTask extends AsyncTask<Void, Void, File> {

        byte[] data;
        File pictureFile;

        public SavePictureTask(byte[] data) {
            this.data = data;
            pictureFile = new File(Environment.getExternalStorageDirectory(),String.valueOf(System.currentTimeMillis()) + "_temp.jpg");
        }

        @Override
        protected void onPostExecute(File pictureFile) {
            super.onPostExecute(pictureFile);
            pictureRollAdapter.pictureSaved(pictureFile);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(TAG, "Adding picture to roll: " + new Date().getTime());

            pictureFiles.add(pictureFile);
            pictureRollAdapter.addPicture(data, pictureFile);
            ((TakeNewPictureFragment) currentFragment).previewPicture();
        }

        @Override
        @SuppressWarnings("squid:S2093")
        protected File doInBackground(Void[] params) {
            Log.i(TAG, "Starting saving picture: " + new Date().getTime());
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            InputStream is = null;
            try {
                is = new ByteArrayInputStream(data);
                PictureUtils.rotateAndCreate(is, pictureFile, bmp);
            } catch (Exception e) {
                Log.e(TAG, "exception while saving picture", e);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "doInBackground: ", e);
                }
            }

            return pictureFile;
        }
    }

    // endregion


}
