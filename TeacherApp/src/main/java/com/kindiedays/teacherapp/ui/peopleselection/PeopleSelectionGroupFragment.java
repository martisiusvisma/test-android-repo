package com.kindiedays.teacherapp.ui.peopleselection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.ui.ChildTouchListener;
import com.kindiedays.common.ui.SectionedGridRecyclerViewAdapter;
import com.kindiedays.common.ui.personselection.PeopleSelectionFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.peopleselection.recipientselection.RecipientSelectionActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pleonard on 18/06/2015.
 */
public abstract class PeopleSelectionGroupFragment extends PeopleSelectionFragment {

    public static final String GROUP_ID = "groupId";
    public static final String ADD_TEACHERS = "ADD_TEACHERS";

    private static final ConversationParty CONVERSATION_PARTY_GROUP = new ConversationParty();

    private long groupId;
    private boolean addTeachers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            groupId = arguments.getLong(GROUP_ID);
            addTeachers = arguments.getBoolean(ADD_TEACHERS, false);
        } else {
            groupId = GroupBusiness.ALL_GROUP_ID;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ChildBusiness.getInstance().addObserver(this);
        ConversationPartyBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ChildBusiness.getInstance().deleteObserver(this);
        ConversationPartyBusiness.getInstance().deleteObserver(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        List<Group> groups;
        List<ConversationParty> conversationParties = new ArrayList<>();
        if (groupId == GroupBusiness.ALL_GROUP_ID) {
            groups = GroupBusiness.getInstance().getTeacherGroups(TeacherBusiness.getInstance().getCurrentTeacher(), false);
            if (addTeachers) {
                conversationParties.addAll(ConversationPartyBusiness.getInstance().getConversationPartyPerType().get(ConversationParty.TEACHER_TYPE));
            }
        } else {
            groups = Arrays.asList(new Group[]{GroupBusiness.getInstance().getGroup(groupId)});
        }

        List<SectionedGridRecyclerViewAdapter.Section> sections = new ArrayList<>();

        int currentPosition = 0;
        List<Person> personList = new ArrayList<>();
        for (Group _group : groups) {
            sections.add(new SectionedGridRecyclerViewAdapter.Section(currentPosition, new String[]{_group.getName()}, _group));
            currentPosition += ChildBusiness.getInstance().getChildren(_group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups()).size();
            personList.addAll(ChildBusiness.getInstance().getChildren(_group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups()));
        }

        if (addTeachers && conversationParties.size() != 0) {
            sections.add(new SectionedGridRecyclerViewAdapter.Section(currentPosition, new String[]{getString(R.string.Teachers)}, CONVERSATION_PARTY_GROUP));
            currentPosition += conversationParties.size();
            personList.addAll(conversationParties);
        }

        ((ChildButtonAdapter) personAdapter).setChildren(personList);

        SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
        SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                SectionedGridRecyclerViewAdapter(getActivity(), R.layout.item_group_section, new int[]{R.id.groupName}, recyclerView, personAdapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));
        recyclerView.setAdapter(mSectionedAdapter);
        recyclerView.addOnItemTouchListener(new ChildTouchListener(this));
        return v;
    }

    @Override
    public void onChildButtonTapped(View childView) {
        if (childView != null) {
            if (childView.getTag() instanceof Group) {
                Group group = (Group) childView.getTag();
                ((PeopleGroupSelectionActivity) getActivity()).onGroupClick(group);
            }
            if (childView.getTag() instanceof Child) {
                Child child = (Child) childView.getTag();
                ((PeopleGroupSelectionActivity) getActivity()).onPersonClick(child);
            }
            if (childView.getTag() instanceof ConversationParty) {
                ConversationParty conversationParty = (ConversationParty) childView.getTag();
                if (CONVERSATION_PARTY_GROUP == conversationParty) {
                    ((PeopleGroupSelectionActivity) getActivity()).onGroupClick(RecipientSelectionActivity.TEACHER_GROUP);
                } else {
                    ((PeopleGroupSelectionActivity) getActivity()).onPersonClick(conversationParty);
                }
            }
        }
    }
}
