package com.kindiedays.teacherapp.ui.peopleselection.tagselection;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildProfile;
import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Person;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PicturesBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.album.PictureNoConsentDialog;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleSelectionGroupFragment;
import com.kindiedays.teacherapp.ui.peopleselection.childselection.ChildSelectionActivity;

import java.util.ArrayList;
import java.util.List;

import static com.kindiedays.common.utils.NetworkUtils.isOnline;

/**
 * Created by pleonard on 17/07/2015.
 */
public class PictureTagSelectionActivity extends ChildSelectionActivity {
    private static final String TAG = PictureTagSelectionActivity.class.getSimpleName();
    public static final String PICTURE_KEY = "pictureKey";
    GalleryPicture picture;
    Child clickedChild;

    private static final int CHILD_POSITION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        noRedrawTabNeeded = true;
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        String pictureKey = getIntent().getStringExtra(PICTURE_KEY);
        picture = PicturesBusiness.getInstance().getPictureFromKey(pictureKey);
        if (picture.getChildTags() != null) {
            for (ChildTag photoTag : picture.getChildTags()) {
                ChildBusiness.getInstance().selectChild(photoTag.getChildId(), true);
            }
        }
        LoadAllChildrenProfiles task = new LoadAllChildrenProfiles();
        getPendingTasks().add(task);
        task.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChildBusiness.getInstance().unselectAllChildren();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHILD_POSITION) {
            if (picture.getLink() != null) {
                //picture on the server
                PicturesBusiness.PatchChildTagAsyncTask task;
                if (resultCode == RESULT_OK) {
                    task = new TagChildAsyncTask(picture.getLink(), clickedChild.getId(),
                            data.getIntExtra(ChildPositionSelectionActivity.X, 0),
                            data.getIntExtra(ChildPositionSelectionActivity.Y, 0),
                            data.getIntExtra(ChildPositionSelectionActivity.WIDTH, 0),
                            data.getIntExtra(ChildPositionSelectionActivity.HEIGHT, 0),
                            true
                    );
                } else {
                    //Create tag anyway with no position
                    task = new TagChildAsyncTask(picture.getLink(), clickedChild.getId(), true);
                }
                getPendingTasks().add(task);
                task.execute();
            } else {
                //local picture
                ChildTag childTag = new ChildTag();
                childTag.setChildId(clickedChild.getId());
                picture.getChildTags().add(childTag);
                if (resultCode == RESULT_OK) {
                    childTag.setX(data.getIntExtra(ChildPositionSelectionActivity.X, 0));
                    childTag.setY(data.getIntExtra(ChildPositionSelectionActivity.Y, 0));
                    childTag.setWidth(data.getIntExtra(ChildPositionSelectionActivity.WIDTH, 0));
                    childTag.setHeight(data.getIntExtra(ChildPositionSelectionActivity.HEIGHT, 0));
                    picture.setWidth(data.getIntExtra(ChildPositionSelectionActivity.IMAGE_WIDTH, 0));
                    picture.setHeight(data.getIntExtra(ChildPositionSelectionActivity.IMAGE_HEIGHT, 0));
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onPersonClick(Person child) {
        clickedChild = (Child) child;
        if (picture.getLink() != null) {
            //Picture already on the server
            if (!child.isSelected()) {
                if (clickedChild.getChildProfile() != null && clickedChild.getChildProfile().isPhotoConsent()) {
                    PicturesBusiness.PatchChildTagAsyncTask task = new TagChildAsyncTask(picture.getLink(), child.getId(), true);
                    getPendingTasks().add(task);
                    task.execute();
                } else {
                    Intent intent = new Intent(PictureTagSelectionActivity.this, ChildPositionSelectionActivity.class);
                    intent.putExtra(ChildPositionSelectionActivity.HEIGHT, picture.getHeight());
                    intent.putExtra(ChildPositionSelectionActivity.WIDTH, picture.getWidth());
                    Point screenDimensions = PictureUtils.getScreenDimensions(this);
                    intent.putExtra(ChildPositionSelectionActivity.PATH_OF_PICTURE, picture.getUrl() + "&preferredWidth=" + screenDimensions.x + "&preferredHeight=" + screenDimensions.y);
                    startActivityForResult(intent, CHILD_POSITION);
                }
            } else {
                PicturesBusiness.PatchChildTagAsyncTask task = new UntagChildAsynkTask(picture.getLink(), child.getId(), true);
                getPendingTasks().add(task);
                task.execute();
            }
        } else {
            //Picture not yet on the server
            if (!child.isSelected()) {
                ChildTag tag = new ChildTag();
                tag.setChildId(child.getId());
                if (clickedChild.getChildProfile() != null && clickedChild.getChildProfile().isPhotoConsent()) {
                    picture.getChildTags().add(tag);
                } else {
                    Intent intent = new Intent(PictureTagSelectionActivity.this, ChildPositionSelectionActivity.class);
                    intent.putExtra(ChildPositionSelectionActivity.PATH_OF_PICTURE, picture.getUrl());
                    startActivityForResult(intent, CHILD_POSITION);
                }
            } else {
                for (ChildTag tag : picture.getChildTags()) {
                    if (tag.getChildId() == child.getId()) {
                        picture.getChildTags().remove(tag);
                        break;
                    }
                }
            }
            ChildBusiness.getInstance().selectChild(child.getId(), !child.isSelected());
        }
    }

    @Override
    public void onGroupClick(Group group) {
        Log.e(PictureTagSelectionActivity.class.getName(), "onGroupClick " + group.getId());
        List<Child> children = ChildBusiness.getInstance().getChildren(group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
        boolean childOfGroupSelected = false;
        for (Child child : children) {
            if (child.isSelected()) {
                childOfGroupSelected = true;
            }
        }
        if (childOfGroupSelected) {
            //Unselect children of group
            List<Child> childrenToUnselect = new ArrayList<>();
            if (picture.getLink() != null) {
                //Picture already on the server
                for (Child child : children) {
                    if (child.isSelected()) {
                        PicturesBusiness.PatchChildTagAsyncTask task = new UntagChildAsynkTask(picture.getLink(), child.getId(), false);
                        getPendingTasks().add(task);
                        task.execute();
                        childrenToUnselect.add(child);
                    }
                }
            } else {
                //Picture not yet on the server
                for (Child child : children) {
                    if (child.isSelected()) {
                        for (ChildTag tag : picture.getChildTags()) {
                            if (tag.getChildId() == child.getId()) {
                                picture.getChildTags().remove(tag);
                                break;
                            }
                        }
                        childrenToUnselect.add(child);
                    }
                }
            }
            ChildBusiness.getInstance().selectChildren(childrenToUnselect, false, false);
        } else {
            //Select all children with picture permission
            List<Child> childrenToSelect = new ArrayList<>();
            if (picture.getLink() != null) {
                //Picture already on the server
                for (Child child : children) {
                    if (!child.isSelected() && clickedChild.getChildProfile() != null && child.getChildProfile().isPhotoConsent()) {
                        PicturesBusiness.PatchChildTagAsyncTask task = new TagChildAsyncTask(picture.getLink(), child.getId(), false);
                        getPendingTasks().add(task);
                        task.execute();
                        childrenToSelect.add(child);
                    }
                }
            } else {
                //Picture not yet on the server
                for (Child child : children) {
                    if (!child.isSelected() && clickedChild.getChildProfile() != null && child.getChildProfile().isPhotoConsent()) {
                        ChildTag tag = new ChildTag();
                        tag.setChildId(child.getId());
                        picture.getChildTags().add(tag);
                        childrenToSelect.add(child);
                    }
                }
            }
            ChildBusiness.getInstance().selectChildren(childrenToSelect, false, true);
        }
    }

    @Override
    protected void initTabs() {
        List<Group> groups = GroupBusiness.getInstance().getTeacherGroups(TeacherBusiness.getInstance().getCurrentTeacher(), true);
        for (Group group : groups) {
            TabHost.TabSpec ts = groupTabHost.newTabSpec("GroupSubtab " + group.getName()).setIndicator(createTabView(group.getName()));
            Bundle bundle = new Bundle();
            bundle.putLong(PeopleSelectionGroupFragment.GROUP_ID, group.getId());
            groupTabHost.addTab(ts, ChildTagSelectionGroupFragment.class, bundle);
        }
    }

    class UntagChildAsynkTask extends PicturesBusiness.PatchChildTagAsyncTask {
        boolean updateChild;// When using the group toggle, there's no need to update the child again

        public UntagChildAsynkTask(String url, long child, boolean updateChild) {
            super(url, PicturesBusiness.PatchChildTagAsyncTask.REMOVE_CHILD, child);
            this.updateChild = updateChild;
        }

        @Override
        protected void onPostExecute(GalleryPicture picture) {
            if (!checkNetworkException(picture, exception)) {
                super.onPostExecute(picture);
            }
            getPendingTasks().remove(this);
            if (updateChild) {
                ChildBusiness.getInstance().selectChild(child, false);
            }
        }
    }

    class TagChildAsyncTask extends PicturesBusiness.PatchChildTagAsyncTask {

        boolean updateChild;// When using the group toggle, there's no need to update the child again

        public TagChildAsyncTask(String url, long child, int x, int y, int width, int height, boolean updateChild) {
            super(url, PicturesBusiness.PatchChildTagAsyncTask.ADD_CHILD, child, x, y, width, height);
            this.updateChild = updateChild;
        }

        public TagChildAsyncTask(String url, long child, boolean updateChild) {
            super(url, PicturesBusiness.PatchChildTagAsyncTask.ADD_CHILD, child);
            this.updateChild = updateChild;
            if (!isOnline(PictureTagSelectionActivity.this)) {
                Log.d(TAG, "onPostExecute: RESULT_FEILD - offline");
            }

        }

        @Override
        protected void onPostExecute(GalleryPicture picture) {
            if (!checkNetworkException(picture, exception)) {
                super.onPostExecute(picture);
                showShareSuccessDialog();
            }
            getPendingTasks().remove(this);
            if (updateChild) {
                ChildBusiness.getInstance().selectChild(child, true);
            }
        }
    }

    private void showShareSuccessDialog() {
        Fragment oldDialog = getFragmentManager().findFragmentByTag(PictureNoConsentDialog.TAG);
        if (oldDialog == null) {
            DialogFragment dialog = PictureNoConsentDialog.newInstance(getString(R.string.dialog_photo_shared), "");
            dialog.show(getFragmentManager(), PictureNoConsentDialog.TAG);
        }

    }

    class LoadAllChildrenProfiles extends ChildBusiness.GetChildrenProfilesAsyncTask {

        @Override
        protected void onPostExecute(List<ChildProfile> profiles) {
            if (!checkNetworkException(profiles, exception)) {
                super.onPostExecute(profiles);
            }
            getPendingTasks().remove(this);
        }
    }
}
