package com.kindiedays.teacherapp.ui.album;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.kindiedays.common.model.Model;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.pojo.AlbumCategory;

import java.util.List;

public class AlbumCategoryDelegate extends AdapterDelegate<List<Model>> {
    private OnAlbumCategoryClickListener listener;

    public AlbumCategoryDelegate(OnAlbumCategoryClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<Model> items, int position) {
        return items.get(position) instanceof AlbumCategory;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album_teacher_category, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<Model> items, int position,
                                    @NonNull RecyclerView.ViewHolder holder,
                                    @NonNull List<Object> payloads) {
        CategoryViewHolder vh = (CategoryViewHolder) holder;
        AlbumCategory model = (AlbumCategory) items.get(position);
        vh.bind(model);
    }

    private class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitle;
        private AlbumCategory model;

        CategoryViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            itemView.setOnClickListener(this);
        }

        private void bind(AlbumCategory model) {
            this.model = model;
            tvTitle.setText(this.model.getName());
        }

        @Override
        public void onClick(View v) {
            listener.onCategoryClick(model);
        }
    }


    public interface OnAlbumCategoryClickListener {
        void onCategoryClick(AlbumCategory model);
    }
}
