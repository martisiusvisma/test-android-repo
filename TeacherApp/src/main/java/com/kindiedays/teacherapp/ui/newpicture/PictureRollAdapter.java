package com.kindiedays.teacherapp.ui.newpicture;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.teacherapp.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pleonard on 21/08/2015.
 */
public class PictureRollAdapter extends RecyclerView.Adapter<PictureRollAdapter.Holder> {

    List<File> thumbnailUrl = new ArrayList<>();
    Map<File, byte[]> pendingFiles = new HashMap<>();


    private static final String TAG = PictureRollAdapter.class.getName();

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pictureroll_thumbnail, null);
        return new Holder(v);
    }

    public void addPicture(File url) {
        thumbnailUrl.add(url);
        this.notifyDataSetChanged();
    }

    //Store unsaved pictures in memory temporarily so that they can be displayed
    public void addPicture(byte[] picture, File tempFile) {
        thumbnailUrl.add(tempFile);
        pendingFiles.put(tempFile, picture);
        this.notifyDataSetChanged();
    }

    public void pictureSaved(File file) {
        pendingFiles.remove(file);
    }

    public void removePicture(int position) {
        thumbnailUrl.remove(position);
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        File url = thumbnailUrl.get(position);
        Log.i(TAG, "Loading picture in roll: " + new Date().getTime());
        if (pendingFiles.get(url) != null) {
            Glide.with(holder.imageView.getContext()).load(pendingFiles.get(url)).into(new MyGlideDrawableImageViewTarget(holder));
        } else {
            Glide.with(holder.imageView.getContext()).load(url).into(new MyGlideDrawableImageViewTarget(holder));
        }
    }

    @Override
    public int getItemCount() {
        return thumbnailUrl.size();
    }

    class MyGlideDrawableImageViewTarget extends GlideDrawableImageViewTarget {

        Holder holder;

        public MyGlideDrawableImageViewTarget(Holder holder) {
            super(holder.imageView);
            this.holder = holder;
        }

        @Override
        public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
            super.onResourceReady(drawable, anim);
            holder.progressBar.setVisibility(View.GONE);
            Log.i(TAG, "Picture displayed in roll: " + new Date().getTime());
        }

        @Override
        public void onLoadFailed(Exception e, Drawable errorDrawable) {
            super.onLoadFailed(e, errorDrawable);
            holder.progressBar.setVisibility(View.GONE);
        }
    }


    public class Holder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;
        ImageView imageView;

        public Holder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
