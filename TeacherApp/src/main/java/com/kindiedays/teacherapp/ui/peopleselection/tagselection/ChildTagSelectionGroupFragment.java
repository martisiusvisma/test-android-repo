package com.kindiedays.teacherapp.ui.peopleselection.tagselection;

import com.kindiedays.teacherapp.ui.peopleselection.ChildButtonAdapter;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleSelectionGroupFragment;

/**
 * Created by pleonard on 07/10/2015.
 */
public class ChildTagSelectionGroupFragment extends PeopleSelectionGroupFragment {


    @Override
    protected ChildButtonAdapter initPeopleAdapter() {
        return new ChildTagSelectionButtonAdapter(getActivity());
    }
}
