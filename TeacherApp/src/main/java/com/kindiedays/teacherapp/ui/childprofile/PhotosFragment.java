package com.kindiedays.teacherapp.ui.childprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.ui.album.ThumbnailAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.picturegallery.PictureGalleryActivity;

import java.util.List;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 19/05/2015.
 */
public class PhotosFragment extends Fragment {
    private static final String EXTRA_CHILD_ID = "extra_child_id";

    private GridView pictureGrid;
    private ThumbnailAdapter thumbnailAdapter;
    private ProgressBar progressBar;
    private List<GalleryPicture> pictures;
    private String blurringChild;

    AdapterView.OnItemClickListener onPictureClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
            intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
            intent.putExtra(PictureGalleryActivity.PICTURE_PARAMS, blurringChild);
            getActivity().startActivity(intent);
        }
    };
    
    public static PhotosFragment newInstance(Long childId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_CHILD_ID, childId);
        PhotosFragment fragment = new PhotosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photos, null);
        Long childId = getArguments().getLong(EXTRA_CHILD_ID, 0L);
        pictureGrid = (GridView) v.findViewById(R.id.pictureGrid);
        pictureGrid.setNumColumns(3);
        blurringChild = "com.kindiedays.teacherapp.TeacherApp".equals(getActivity().getApplicationInfo().className)
                ? "&child=" + childId
                : STRING_EMPTY;
        thumbnailAdapter = new ThumbnailAdapter(getActivity(), blurringChild);
        pictureGrid.setAdapter(thumbnailAdapter);
        pictureGrid.setOnItemClickListener(onPictureClickListener);
        progressBar = (ProgressBar) v.findViewById(R.id.progress);
        displayPictures();
        return v;
    }

    private void displayPictures() {
        if (pictures != null && pictureGrid != null) {
            thumbnailAdapter.setPictures(pictures);
            progressBar.setVisibility(View.GONE);
        }
    }

    public void setPictureList(List<GalleryPicture> pictures) {
        this.pictures = pictures;
        displayPictures();
    }

}
