package com.kindiedays.teacherapp.ui.dailyreport.activities;

import android.view.View;
import android.widget.EditText;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.kindiedays.common.pojo.ActivityRes;
import com.kindiedays.teacherapp.R;


public class DescriptionViewHolder extends ChildViewHolder {

    private EditText mDescriptionEditText;
    ActivityAdapter.MyCustomEditTextListener myCustomEditTextListener;

    public DescriptionViewHolder(View itemView, ActivityAdapter.MyCustomEditTextListener myCustomEditTextListener) {
        super(itemView);
        mDescriptionEditText = (EditText) itemView.findViewById(R.id.ingredient_edittext);
        this.myCustomEditTextListener = myCustomEditTextListener;
        mDescriptionEditText.addTextChangedListener(myCustomEditTextListener);
    }

    public void bind(ActivityRes description) {
        mDescriptionEditText.setText(description.getText());
    }

}
