package com.kindiedays.teacherapp.ui.events;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.EventInvitation;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 03/07/2015.
 */
public class InvitationExpandableBaseAdapter extends BaseExpandableListAdapter {

    private List<EventInvitation> confirmedList = new ArrayList<>();
    private List<EventInvitation> pendingList = new ArrayList<>();
    private List<EventInvitation> rejectedList = new ArrayList<>();

    private Activity activity;

    private static final int CONFIRMED = 0;
    private static final int DECLINED = 1;
    private static final int PENDING = 2;

    public InvitationExpandableBaseAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setInvitations(List<EventInvitation> invitations) {
        confirmedList = new ArrayList<>();
        pendingList = new ArrayList<>();
        rejectedList = new ArrayList<>();
        for (EventInvitation invitation : invitations) {
            switch (invitation.getState()) {
                case CONFIRMED:
                    confirmedList.add(invitation);
                    break;
                case INVITED:
                    pendingList.add(invitation);
                    break;
                case DECLINED:
                    rejectedList.add(invitation);
                    break;
                default:
                    break;
            }
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return 3;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        switch (groupPosition) {
            case CONFIRMED:
                return confirmedList.size();
            case PENDING:
                return pendingList.size();
            case DECLINED:
                return rejectedList.size();
            default:
                return 0;
        }
    }

    @Override
    public String getGroup(int groupPosition) {
        switch (groupPosition) {
            case CONFIRMED:
                return activity.getString(R.string.Confirmed) + ": " + getChildrenCount(groupPosition) ;
            case PENDING:
                return activity.getString(R.string.No_Response) + ": " + getChildrenCount(groupPosition) ;
            case DECLINED:
                return activity.getString(R.string.Declined) + ": " + getChildrenCount(groupPosition) ;
            default:
                return "";
        }
    }

    @Override
    public Child getChild(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case CONFIRMED:
                return ChildBusiness.getInstance().getChild(confirmedList.get(childPosition).getChildId());
            case PENDING:
                return ChildBusiness.getInstance().getChild(pendingList.get(childPosition).getChildId());
            case DECLINED:
                return ChildBusiness.getInstance().getChild(rejectedList.get(childPosition).getChildId());
            default:
                return null;
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case CONFIRMED:
                return confirmedList.get(childPosition).getChildId();
            case PENDING:
                return pendingList.get(childPosition).getChildId();
            case DECLINED:
                return rejectedList.get(childPosition).getChildId();
            default:
                return 0;
        }
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TextView tv;
        if(convertView != null){
            tv = (TextView) convertView;
        }
        else{
            tv = (TextView) activity.getLayoutInflater().inflate(R.layout.item_expandablelistgroupitem, null);
        }
        tv.setText(getGroup(groupPosition));
        return tv;
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if(convertView != null){
            holder = (ChildViewHolder) convertView.getTag();
        }
        else{
            convertView = activity.getLayoutInflater().inflate(R.layout.item_person, null);
            holder = new ChildViewHolder();
            holder.ivPicture = (ImageView) convertView.findViewById(R.id.personThumbnail);
            holder.tvName = (TextView) convertView.findViewById(R.id.personName);
            convertView.setTag(holder);
        }
        Child child = getChild(groupPosition, childPosition);
        if(child != null) {
            Glide.with(activity).load(child.getProfilePictureUrl()).into(holder.ivPicture);
            holder.tvName.setText(child.getName());
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    static class ChildViewHolder{
        ImageView ivPicture;
        TextView tvName;
    }
}
