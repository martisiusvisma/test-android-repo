package com.kindiedays.teacherapp.ui.album;


import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.kindiedays.common.model.Model;
import com.kindiedays.common.ui.journal.JournalPostPictureAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.pojo.AlbumCategory;
import com.kindiedays.teacherapp.pojo.AlbumPictureModel;

import java.util.List;

public class AlbumPictureDelegate extends AdapterDelegate<List<Model>> {
    private OnAlbumPictureClickListener listener;

    public AlbumPictureDelegate(OnAlbumPictureClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<Model> items, int position) {
        return items.get(position) instanceof AlbumPictureModel;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new PictureViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album_teacher_pictures, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<Model> items, int position,
                                    @NonNull RecyclerView.ViewHolder holder,
                                    @NonNull List<Object> payloads) {
        PictureViewHolder vh = (PictureViewHolder) holder;
        AlbumPictureModel model = (AlbumPictureModel) items.get(position);
        vh.bind(model);
    }

    private class PictureViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar pb1;
        private ProgressBar pb2;
        private ProgressBar pb3;

        private ImageView ivPhoto1;
        private ImageView ivPhoto2;
        private ImageView ivPhoto3;

        private AlbumPictureModel model;

        PictureViewHolder(View itemView) {
            super(itemView);
            pb1 = (ProgressBar) itemView.findViewById(R.id.pb_1);
            pb2 = (ProgressBar) itemView.findViewById(R.id.pb_2);
            pb3 = (ProgressBar) itemView.findViewById(R.id.pb_3);
            ivPhoto1 = (ImageView) itemView.findViewById(R.id.iv_photo_1);
            ivPhoto2 = (ImageView) itemView.findViewById(R.id.iv_photo_2);
            ivPhoto3 = (ImageView) itemView.findViewById(R.id.iv_photo_3);

            initClickListener(ivPhoto1, 0);
            initClickListener(ivPhoto2, 1);
            initClickListener(ivPhoto3, 2);
        }

        private void initClickListener(ImageView imageView, final int index) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPictureClick(model.getCategory(), index);
                }
            });
        }

        private void bind(AlbumPictureModel model) {
            if (model == null || model.getPictures().isEmpty()) {
                return;
            }

            this.model = model;
            setPictureSafe(ivPhoto1, model, pb1, 0);
            setPictureSafe(ivPhoto2, model, pb2, 1);
            setPictureSafe(ivPhoto3, model, pb3, 2);
        }

        private void setPictureSafe(ImageView imageView, AlbumPictureModel model,
                                    final ProgressBar progress, int index) {
            if (model.getPictures().size() > index) {
                setProgressGlidePicture(imageView, model.getPictures().get(index).getUrl(), progress);
            } else {
                progress.setVisibility(View.GONE);
            }
        }

        private void setProgressGlidePicture(ImageView imageView, String url, final ProgressBar progress) {
            Glide.with(itemView.getContext()).load(url).into(new GlideDrawableImageViewTarget(imageView) {
                @Override
                public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                    super.onResourceReady(drawable, anim);
                    progress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    progress.setVisibility(View.GONE);
                    Log.e(AlbumPictureDelegate.class.getName(), e.getMessage(), e);
                }
            });
        }
    }


    public interface OnAlbumPictureClickListener {
        void onPictureClick(AlbumCategory category, int pos);
    }
}
