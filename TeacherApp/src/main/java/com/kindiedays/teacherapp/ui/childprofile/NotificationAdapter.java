package com.kindiedays.teacherapp.ui.childprofile;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.pojo.Kindergarten;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.pojo.Notification;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.List;

/**
 * Created by pleonard on 17/07/2015.
 */
public class NotificationAdapter extends BaseAdapter {

    private List<Notification> notifications;
    private Activity activity;
    private PrettyTime p = new PrettyTime();

    public NotificationAdapter(Activity activity){
        this.activity = activity;
    }

    public void setNotifications(List<Notification> notifications){
        this.notifications = notifications;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(notifications == null) {
            return 0;
        }
        return notifications.size();
    }

    @Override
    public Notification getItem(int position) {
        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    @SuppressWarnings({"squid:S1226", "squid:S1151"})
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = activity.getLayoutInflater().inflate(R.layout.item_notification, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.when = (TextView) convertView.findViewById(R.id.when);
            viewHolder.body = (TextView) convertView.findViewById(R.id.body);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Notification notification = getItem(position);
        switch(notification.getType()){
            case Notification.PRESENCE_CREATED:
                if (KindergartenBusiness.getInstance().getKindergarten() == null || KindergartenBusiness.getInstance().getKindergarten().getDefaultAttendance().equals(Kindergarten.ABSENT)) {
                    viewHolder.title.setText(activity.getString(R.string.New_Absence));
                } else {
                    viewHolder.title.setText(activity.getString(R.string.New_Presence));
                }
                break;
            case Notification.CHILD_PROFILE_CHANGED:
                viewHolder.title.setText(activity.getString(R.string.Profile_Changed));
                break;
            case Notification.MESSAGE_CREATED:
                viewHolder.title.setText(activity.getString(R.string.New_Message));
                break;
            default:
                break;
        }
        viewHolder.body.setText(notification.getText());
        viewHolder.when.setText(p.format(notification.getCreated().toDateTime(KindergartenBusiness.getInstance().getTimeZone()).toDate()));
        if(notification.isRead()){
            setFont(viewHolder, Typeface.NORMAL);
        }
        else{
            setFont(viewHolder, Typeface.BOLD);
        }

        return convertView;
    }

    private void setFont(ViewHolder holder, int typeface){
        holder.title.setTypeface(null, typeface);
        holder.body.setTypeface(null, typeface);
        holder.when.setTypeface(null, typeface);
    }

    public void updateNotification(Notification notification, int position) {
        notifications.set(position, notification);
        this.notifyDataSetChanged();
    }

    static class ViewHolder{
        TextView title;
        TextView body;
        TextView when;
    }

}
