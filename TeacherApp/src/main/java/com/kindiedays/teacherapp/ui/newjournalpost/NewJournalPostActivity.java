package com.kindiedays.teacherapp.ui.newjournalpost;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kindiedays.common.business.AppMenuBusiness;
import com.kindiedays.common.network.NetworkProgressAsyncTask;
import com.kindiedays.common.network.pictures.PostPicture;
import com.kindiedays.common.pojo.AppMenuState;
import com.kindiedays.common.pojo.Attachment;
import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.JournalPost;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.ui.messages.CustomPhotoGalleryActivity;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.JournalBusiness;
import com.kindiedays.teacherapp.ui.album.PictureSelectionActivity;
import com.kindiedays.teacherapp.ui.peopleselection.childselection.ChildSelectionActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.kindiedays.common.business.ConversationBusiness.prepareFile;
import static com.kindiedays.common.utils.constants.CommonConstants.EXTRA_GALLERY;
import static com.kindiedays.common.utils.constants.CommonConstants.PICK_IMAGE_MULTIPLE;

/**
 * Created by pleonard on 07/07/2015.
 */
public class NewJournalPostActivity extends KindieDaysActivity implements ChooseAlbumDialog.OnChooseAlbumDialogListener {
    private static final String TAG = NewJournalPostActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_READ_STORAGE = 3;

    private EditText etPost;
    private Button btnTag;
    private Button btnPicture;

    private static final int TAG_ACTIVITY = 1;
    static final int PICTURE_ACTIVITY = 2;

    public static final String CHILD_ID = "childId";

    private List<Long> selectedChildren;
    private List<String> selectedPicturesFromGalleryHashes;
    private List<File> selectedPicturesFromDevice = new ArrayList<>();

    private View.OnClickListener onTagClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(NewJournalPostActivity.this, ChildSelectionActivity.class);
            long[] selectedChildIds = new long[selectedChildren.size()];
            for (int i = 0; i < selectedChildren.size(); i++) {
                selectedChildIds[i] = selectedChildren.get(i);
            }
            intent.putExtra(ChildSelectionActivity.SELECTED_RW, selectedChildIds);
            startActivityForResult(intent, TAG_ACTIVITY);
        }
    };

    protected View.OnClickListener onDisabledItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewJournalPostActivity.this);
            builder.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setTitle(R.string.Locked);
            builder.setMessage(R.string.locked_edu_message);
            Dialog dialog = builder.create();
            dialog.show();
        }
    };

    private View.OnClickListener onAddPictureClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            AppMenuState appMenuState = AppMenuBusiness.getInstance().getMenuStates();
            if (appMenuState == null) {
                NewJournalPostActivity.this.relaunchActivity();
                return;
            }

            //if (AppMenuBusiness.getInstance().getMenuStates().isCalendarEnabled()) {
            if (AppMenuBusiness.getInstance().getMenuStates().isCameraEnabled()) {
                ChooseAlbumDialog newFragment = new ChooseAlbumDialog();
                newFragment.show(getFragmentManager(), ChooseAlbumDialog.TAG);
            } else {
                onDisabledItemClickListener.onClick(v);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_journal_post);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etPost = (EditText) findViewById(R.id.post);
        btnTag = (Button) findViewById(R.id.tagPeopleButton);
        btnTag.setOnClickListener(onTagClickListener);
        btnPicture = (Button) findViewById(R.id.addPictures);
        btnPicture.setOnClickListener(onAddPictureClickListener);
        selectedChildren = new ArrayList<>();
        if (getIntent().hasExtra(CHILD_ID)) {
            selectedChildren.add(getIntent().getLongExtra(CHILD_ID, 0));
        }
        selectedPicturesFromGalleryHashes = new ArrayList<>();
        initButtonsText();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAG_ACTIVITY) {
            if (data != null) {
                long[] childrenIds = data.getLongArrayExtra(ChildSelectionActivity.SELECTED_CHILDREN_IDS);
                selectedChildren = toLongList(childrenIds);
            }

        } else if (requestCode == PICTURE_ACTIVITY && resultCode == RESULT_OK && data != null) {
            selectedPicturesFromGalleryHashes.clear();
            selectedPicturesFromGalleryHashes.addAll(Arrays.asList(data.getStringArrayExtra((PictureSelectionActivity.SELECTED_PICTURES_HASHES))));
        } else if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && data != null) {
            selectedPicturesFromDevice.clear();
            String[] imagesPath = data.getStringExtra(EXTRA_GALLERY).split("\\|");
            for (String path : imagesPath) {
                if (!path.equals("")){
                    selectedPicturesFromDevice.add(new File(path));
                }
            }
        }
        initButtonsText();
    }

    private List<Long> toLongList(long[] array) {
        List<Long> list = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
        return list;
    }

    private void initButtonsText() {
        if (selectedChildren.isEmpty()) {
            btnTag.setText(getString(R.string.Tag_people));
        } else {
            btnTag.setText(getString(R.string.Tag_people) + " (" + selectedChildren.size() + ")");
        }
        if (selectedPicturesFromGalleryHashes.isEmpty() && selectedPicturesFromDevice.isEmpty()) {
            btnPicture.setText(getString(R.string.Add_photo));
        } else {
            int totalPictures = getSelectedPhotosNumber();
            btnPicture.setText(getString(R.string.Add_photo) + " (" + totalPictures + ")");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sendPost:
                if (selectedChildren.isEmpty()) {
                    Toast.makeText(this, R.string.child_not_tagged, Toast.LENGTH_SHORT).show();
                    return super.onOptionsItemSelected(item);
                }
                UploadImages uploadImagesTask = new UploadImages(selectedChildren, selectedPicturesFromDevice);
                getPendingTasks().add(uploadImagesTask);
                uploadImagesTask.execute();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAlbumClick() {
        int selectedNumber = getSelectedPhotosNumber();
        ArrayList<CharSequence> selected = new ArrayList<>();
        for (int i = 0; i < selectedPicturesFromGalleryHashes.size(); i ++){
            selected.add(selectedPicturesFromGalleryHashes.get(i));
        }
        Intent intent = PictureSelectionActivity.getInstance(this, selected, selectedNumber);

        startActivityForResult(intent, PICTURE_ACTIVITY);
    }

    private int getSelectedPhotosNumber(){
        return selectedPicturesFromDevice.size() + selectedPicturesFromGalleryHashes.size();
    }

    @Override
    public void photoLibraryClick() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_READ_STORAGE);

            // PERMISSIONS_REQUEST_READ_STORAGE is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            navigateToCustomPhotoGallary();
        }
    }

    private void navigateToCustomPhotoGallary() {
        ArrayList<CharSequence> selected = new ArrayList<>();
        for (int i = 0; i < selectedPicturesFromDevice.size(); i ++){
            selected.add(selectedPicturesFromDevice.get(i).getAbsolutePath());
        }
        int selectedNumber = getSelectedPhotosNumber();
        Intent intent = CustomPhotoGalleryActivity.getInstance(NewJournalPostActivity.this, selected, selectedNumber);
        intent.putExtra(CustomPhotoGalleryActivity.ONLY_JPEG_IMAGES, true);
        startActivityForResult(intent, PICK_IMAGE_MULTIPLE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_READ_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                navigateToCustomPhotoGallary();
            }
        }
    }

    private class SendJournalPostTask extends JournalBusiness.PostJournalPostTask {

        public SendJournalPostTask(String text, Collection<Long> childrenIds,
                                   Collection<String> pictureHashes) {
            super(text, childrenIds, pictureHashes);
        }

        @Override
        protected void onPostExecute(JournalPost journalPost) {
            if (!checkNetworkException(journalPost, exception)) {
                super.onPostExecute(journalPost);
                finish();
            }
            getPendingTasks().remove(this);
        }
    }

    public class UploadImages extends NetworkProgressAsyncTask<List<GalleryPicture>, Integer> {
        private ProgressDialog progressDialog;
        private List<Long> selectedChildren;
        protected List<File> files;

        public UploadImages(List<Long> selectedChildren, List<File> files) {
            this.selectedChildren = selectedChildren;
            this.files = files;
        }

        @Override
        protected void onPreExecute() {
            if (files.isEmpty()) {
                return;
            }

            initProgressDialog();
            super.onPreExecute();
        }

        private void initProgressDialog() {
            progressDialog = new ProgressDialog(NewJournalPostActivity.this);
            progressDialog.setTitle(com.kindiedays.common.R.string.Upload_In_Progress);
            progressDialog.setMax(files.size());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(1);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(getString(com.kindiedays.common.R.string.dialog_progress_file_uploading, 1,
                    files.size()));
            progressDialog.show();
        }

        @Override
        protected List<GalleryPicture> doNetworkTask() throws Exception {
            List<GalleryPicture> pictures = new ArrayList<>();
            for (int i = 0; i < files.size(); i++) {
                Attachment attachment = prepareFile(files.get(i));
                List<ChildTag> childTags = populateChildTagsList();
                pictures.add(PostPicture.post(attachment.getPendingFileId(), "", childTags));
                publishProgress(i + 1);
            }

            return pictures;
        }

        private List<ChildTag> populateChildTagsList() {
            List<ChildTag> childTags = new ArrayList<>();
            for (int i = 0; i < selectedChildren.size(); i++) {
                Long selectedChild = selectedChildren.get(i);
                ChildTag childTag = new ChildTag();
                childTag.setChildId(selectedChild);
                childTags.add(childTag);
            }
            return childTags;
        }

        @Override
        protected void onPostExecute(List<GalleryPicture> pictures) {
            if (!checkNetworkException(pictures, exception)) {
                super.onPostExecute(pictures);
                finish();
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            getPendingTasks().remove(this);

            try {
                for (GalleryPicture picture : pictures) {
                    selectedPicturesFromGalleryHashes.add(picture.getHash());
                }
            } catch (NullPointerException e){
                //there is no such file
            }

            SendJournalPostTask task = new SendJournalPostTask(etPost.getText().toString(),
                    selectedChildren, selectedPicturesFromGalleryHashes);
            getPendingTasks().add(task);
            task.execute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int currentProgress = values[0];
            progressDialog.setMessage(getString(com.kindiedays.common.R.string.dialog_progress_file_uploading, currentProgress,
                    files.size()));
            progressDialog.setProgress(currentProgress);
        }

    }
}
