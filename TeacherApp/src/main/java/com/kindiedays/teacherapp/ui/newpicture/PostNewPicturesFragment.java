package com.kindiedays.teacherapp.ui.newpicture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.utils.BlurUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.PicturesBusiness;
import com.kindiedays.teacherapp.ui.peopleselection.tagselection.PictureTagSelectionActivity;

import java.io.File;

/**
 * Created by pleonard on 14/12/2015.
 */
public class PostNewPicturesFragment extends Fragment {

    private ImageView preview;
    private EditText etCaption;
    private ProgressBar progress;
    private NewPictureActivity activity;
    private TextView taggedChildren;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_post_pictures, null);
        etCaption = (EditText) v.findViewById(R.id.et_caption);
        progress = (ProgressBar) v.findViewById(R.id.progress);
        preview = (ImageView) v.findViewById(R.id.picture);
        taggedChildren = (TextView) v.findViewById(R.id.taggedChildren);
        activity = (NewPictureActivity) getActivity();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        displaySelectedPicture();
    }

    public void saveCurrentCaption() {
        File file = activity.getSelectedPictureFile();
        if (file != null) {
            PicturesBusiness.getInstance().getPictureFromKey(Uri.fromFile(file).toString()).setCaption(etCaption.getText().toString());
        }
    }

    public void displaySelectedPicture() {
        File file = activity.getSelectedPictureFile();

        if (file == null) {
            preview.setImageBitmap(null);
            return;
        }

        final GalleryPicture picture = PicturesBusiness.getInstance().getPictureFromKey(Uri.fromFile(file).toString());

        if (picture == null) {
            preview.setImageBitmap(null);
            return;
        }

        progress.setVisibility(View.VISIBLE);
        Glide.with(this).load(file).into(new GlideDrawableImageViewTarget(preview) {
            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                super.onResourceReady(drawable, anim);
                progress.setVisibility(View.GONE);
                preview.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (preview.getDrawable() != null) {
                            blur(picture);
                        }
                    }
                }, 10);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                Toast.makeText(getActivity(), "Unable to display picture", Toast.LENGTH_LONG).show();
            }
        });

        etCaption.setText(picture.getCaption());
        updateButtonText(picture);
    }


    private void blur(GalleryPicture picture) {
        Bitmap srcBitmap = BlurUtils.loadBitmapFromView(preview);
        Drawable drawable = preview.getDrawable();
        Rect bounds = drawable.getBounds();
        RectF boundsF = new RectF(bounds);
        preview.getImageMatrix().mapRect(boundsF);
        boundsF.round(bounds);
        int viewWidth = bounds.width();
        int viewHeight = bounds.height();

        float widthRatio = BlurUtils.getRatio(viewWidth, picture.getWidth());
        float heightRatio = BlurUtils.getRatio(viewHeight, picture.getHeight());

        for (ChildTag childTag : picture.getChildTags()) {
            int left = bounds.left + BlurUtils.getIncreasedDimen(childTag.getX(), widthRatio, viewWidth);
            int top = bounds.top + BlurUtils.getIncreasedDimen(childTag.getY(), heightRatio, viewHeight);
            int right = left + BlurUtils.getIncreasedDimen(childTag.getWidth(), widthRatio, viewWidth);
            int bottom = top + BlurUtils.getIncreasedDimen(childTag.getHeight(), heightRatio, viewHeight);
            Rect rect = new Rect(left, top, right, bottom);
            srcBitmap = BlurUtils.getBlurredBitmap(getContext(), srcBitmap, rect);
        }
        preview.setImageBitmap(srcBitmap);
    }

    public void updateButtonText(GalleryPicture picture) {
        int nbTags = picture.getChildTags().size();
        if (nbTags > 0) {
            taggedChildren.setText(Integer.toString(nbTags));
        } else {
            taggedChildren.setText("");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_post_new_picture, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_sendPost:
            activity.sendPictures(false);
            return true;
        case R.id.action_delete:
            activity.removeSelectedPicture();
            return true;
        case R.id.action_tag:
            saveCurrentCaption();
            File file = activity.getSelectedPictureFile();
            if (file != null) {
                sendIntent(file);
            }
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void sendIntent(File file) {
        Intent intent = new Intent(getActivity(), PictureTagSelectionActivity.class);
        intent.putExtra(PictureTagSelectionActivity.PICTURE_KEY, Uri.fromFile(file).toString());
        startActivityForResult(intent, NewPictureActivity.SELECT_CHILDREN_ACTIVITY);
    }
}
