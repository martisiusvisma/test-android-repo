package com.kindiedays.teacherapp.ui.peopleselection.tagselection;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.ui.personselection.PersonSelectionButton;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildHelper;

/**
 * Created by pleonard on 20/07/2015.
 */
public class PersonTagSelectionButton extends PersonSelectionButton {
    public PersonTagSelectionButton(Context context) {
        super(context);
    }

    public PersonTagSelectionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PersonTagSelectionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void refreshPerson() {
        int alpha = 0xFF;
        Child child = (Child) person;
        ivPersonPicture.clearColorFilter();
        if(ChildHelper.getStatus(child) == ChildHelper.SIGNED_IN){
            tvName.setTextColor(getResources().getColor(R.color.kindieblue));
            tvName.setTypeface(null, Typeface.BOLD);
        }
        else{
            tvName.setTextColor(getResources().getColor(android.R.color.tab_indicator_text));
            tvName.setTypeface(null, Typeface.NORMAL);
        }

        if(child.isSelected()){
            ivPersonPicture.setColorFilter(getContext().getResources().getColor(R.color.selectedfilter));
            ivSelectionMark.setVisibility(View.VISIBLE);
        }
        else {
            if(ChildHelper.getStatus(child) != ChildHelper.SIGNED_IN){
                alpha = 0x77;
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0.7f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                ivPersonPicture.setColorFilter(filter);
            }
            ivSelectionMark.setVisibility(View.GONE);
        }

        if(child.getChildProfile() != null && child.getChildProfile().isPhotoConsent()) {
            tvPicturesNotAllowed.setVisibility(View.GONE);
        }
        else{
            tvPicturesNotAllowed.setVisibility(View.VISIBLE);
        }
        ivPersonPicture.setAlpha(alpha);
    }

}
