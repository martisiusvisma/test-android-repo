package com.kindiedays.teacherapp.ui.peopleselection.recipientselection;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.teacherapp.R;

import java.util.List;

/**
 * Created by pleonard on 24/06/2015.
 */
public class CarerAdapter extends BaseAdapter {


    private List<ChildCarer> carers;
    private Activity activity;

    public CarerAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setCarers(List<ChildCarer> carers) {
        this.carers = carers;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (carers == null) {
            return 0;
        }
        return carers.size();
    }

    @Override
    public ChildCarer getItem(int position) {
        return carers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return carers.get(position).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CheckedTextView ctv;
        if (convertView == null) {
            ctv = (CheckedTextView) activity.getLayoutInflater().inflate(R.layout.item_carer_selection, null);
        } else {
            ctv = (CheckedTextView) convertView;
        }
        ChildCarer carer = getItem(position);
        ctv.setText(carer.getName());
        return ctv;
    }
}
