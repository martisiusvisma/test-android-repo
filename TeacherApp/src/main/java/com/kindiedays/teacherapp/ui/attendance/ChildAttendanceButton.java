package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.utils.GlideUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildHelper;

import org.joda.time.DateTime;
import org.joda.time.Period;

/**
 * Created by pleonard on 21/04/2015.
 */
public class ChildAttendanceButton extends LinearLayout {

    private Child child;

    private ImageView kidPicture;
    private ImageView selection;
    private TextView name;
    private TextView pendingMessages;
    private ImageView absentMarker;
    private ImageView nappingMarker;
    private TextView nappingTime;
    Animation signInOutAnimation;
    private boolean pendingRefresh = false;
    private static final String TAG = ChildAttendanceButton.class.getName();

    Animation.AnimationListener animationListener = new Animation.AnimationListener(){

        @Override
        public void onAnimationStart(Animation anim) {
            //ignore
        }

        @Override
        public void onAnimationEnd(Animation anim) {
            if(pendingRefresh){
                Log.i(TAG, "Starting pending animation");
                pendingRefresh = false;
                refreshChild();
            }
        }

        @Override
        public void onAnimationRepeat(Animation anim) {
            //ignore
        }
    };


    public ChildAttendanceButton(Context context) {
        super(context);
        initViews();
    }

    public ChildAttendanceButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public ChildAttendanceButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }


    @SuppressWarnings("squid:CommentedOutCodeLine")
    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.button_child, this);
        kidPicture = (ImageView) v.findViewById(R.id.kidPicture);
        //ImageView statusIcon = (ImageView) v.findViewById(R.id.statusIcon);
        selection = (ImageView) v.findViewById(R.id.selectionmarker);
        name = (TextView) v.findViewById(R.id.kidName);
        pendingMessages = (TextView) v.findViewById(R.id.pendingMessages);
        absentMarker = (ImageView) v.findViewById(R.id.absentmarker);
        nappingMarker = (ImageView) v.findViewById(R.id.nappingmarker);
        nappingTime = (TextView) v.findViewById(R.id.nappingTime);
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
        refreshChild();
        name.setText((child.getNickname() != null ? child.getNickname() : child.getName()));
        Glide.with(getContext()).load(child.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(kidPicture);
    }

    public void changeSelectionState(){
        super.setSelected(child.isSelected());
        refreshChild();
    }


    private void refreshChild(){
        if(signInOutAnimation != null && signInOutAnimation.hasStarted() && !signInOutAnimation.hasEnded()){
            Log.i(TAG, "Setting refresh pending");
            pendingRefresh = true;
        }
        else {
            Log.i(TAG, "Refreshing child " + child.getName());

            switch (ChildHelper.getStatus(child)) {
                case ChildHelper.SIGNED_IN:
                    kidPicture.setAlpha(0xFF);
                    absentMarker.setVisibility(View.GONE);
                    name.setTypeface(null, Typeface.BOLD);
                    name.setTextColor(getResources().getColor(R.color.kindieblue));
                    break;
                case ChildHelper.SIGNED_OUT:
                    //Gray picture
                    kidPicture.setAlpha(0xFF);
                    kidPicture.setAlpha(0x66);
                    absentMarker.setVisibility(View.GONE);
                    name.setTypeface(null, Typeface.NORMAL);
                    name.setTextColor(getResources().getColor(android.R.color.tab_indicator_text));
                    break;
                case ChildHelper.ABSENT:
                    kidPicture.setAlpha(0xFF);
                    kidPicture.setAlpha(0x66);
                    absentMarker.setVisibility(View.VISIBLE);
                    name.setTypeface(null, Typeface.NORMAL);
                    name.setTextColor(getResources().getColor(android.R.color.tab_indicator_text));
                    break;

                default:
                    break;
            }

            resolveSelection();
            resolveNapping();
        }
    }

    private void resolveSelection() {
        if (child.isSelected()) {
            kidPicture.setAlpha(0xFF);
            selection.setVisibility(View.VISIBLE);
            kidPicture.setColorFilter(getContext().getResources().getColor(R.color.selectedfilter));
        } else {
            kidPicture.clearColorFilter();
            selection.setVisibility(View.GONE);
        }
    }
    private void resolveNapping() {
        if (child.isNapping()) {
            nappingMarker.setVisibility(View.VISIBLE);
            nappingTime.setVisibility(View.VISIBLE);
            //Calculating time elapsed since beginning of nap
            DateTime now = new DateTime();
            Period period = new Period(child.getStartedNapping(), now);
            int minutes = period.getMinutes() + period.getHours() * 60;
            nappingTime.setText(Integer.toString(minutes));
        } else {
            nappingMarker.setVisibility(View.GONE);
            nappingTime.setVisibility(View.GONE);
        }
        if (child.getUnreadMessages() > 0) {
            pendingMessages.setVisibility(View.VISIBLE);
            pendingMessages.setText(Integer.toString(child.getUnreadMessages()));
        } else {
            pendingMessages.setVisibility(View.GONE);
        }
    }

    public void launchAnimation() {
        signInOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.signinout);
        signInOutAnimation.setAnimationListener(animationListener);
        kidPicture.startAnimation(signInOutAnimation);
    }
}
