package com.kindiedays.teacherapp.ui.messages;

import android.app.Activity;

import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.common.pojo.Message;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 15/09/2015.
 */
public class MessageAdapter extends com.kindiedays.common.ui.messages.MessageAdapter {

    private boolean isTeacherAsKindergarten;

    public MessageAdapter(Activity activity, OnMediaClickListener listener){
        super(activity, listener);
    }

    protected boolean isConversationPartyCurrentUser(ConversationParty party){
        return (isTeacherAsKindergarten && party.getType().equals(ConversationParty.KINDERGARTEN_TYPE))
                || (party.getType().equals(ConversationParty.TEACHER_TYPE) && party.getPartyId()
                == TeacherBusiness.getInstance().getCurrentTeacher().getId());
    }

    @Override
    public void setContent(List<Message> messages, Set<ConversationParty> conversationParties) {
        super.setContent(messages, conversationParties);
        boolean currentTeacherInConversation = false;
        boolean kindergartenInConversation = false;
        for(ConversationParty conversationParty : conversationParties){
            if(conversationParty.getType().equals(ConversationParty.KINDERGARTEN_TYPE)){
                kindergartenInConversation = true;
            }
            if(conversationParty.getType().equals(ConversationParty.TEACHER_TYPE)
                    && conversationParty.getPartyId() == TeacherBusiness.getInstance().getCurrentTeacher().getId()){
                currentTeacherInConversation = true;
            }
        }
        isTeacherAsKindergarten = kindergartenInConversation && !currentTeacherInConversation;
    }
}
