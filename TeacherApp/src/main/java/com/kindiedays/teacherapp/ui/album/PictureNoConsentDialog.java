package com.kindiedays.teacherapp.ui.album;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.kindiedays.teacherapp.R;

/**
 * @author Eugeniy Shein on 21/11/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 21/11/2017
 */
public class PictureNoConsentDialog extends DialogFragment {

    private static final String TITLE_KEY = "title";
    private static final String MASSAGE_KEY = "massage";
    public static final String TAG = PictureNoConsentDialog.class.getSimpleName();
    private TextView okButton;
    private TextView titleView;
    private TextView messageView;

    public static PictureNoConsentDialog newInstance(String title, String massage) {
        PictureNoConsentDialog f = new PictureNoConsentDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(TITLE_KEY, title);
        args.putString(MASSAGE_KEY, massage);
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_no_cosent, null);
        builder.setView(view);
        okButton = (TextView) view.findViewById(R.id.okButton);
        titleView = (TextView) view.findViewById(R.id.title);
        messageView = (TextView) view.findViewById(R.id.massage);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PictureNoConsentDialog.this.dismiss();
            }
        });

        Bundle arguments = getArguments();
        if (arguments != null)  {
            String title = arguments.getString(TITLE_KEY);
            String massage = arguments.getString(MASSAGE_KEY);
            titleView.setText(title);
            messageView.setText(massage);
        }

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
