package com.kindiedays.teacherapp.ui.album;


import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.ui.CheckableRelativeLayout;

import java.util.List;

class SimplePicturesAdapter extends RecyclerView.Adapter<SimplePicturesAdapter.ImageViewHolder> {
    private List<GalleryPicture> pictures;

    public List<GalleryPicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<GalleryPicture> pictures) {
        this.pictures = pictures;
        this.notifyDataSetChanged();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(com.kindiedays.common.R.layout.item_thumbnail, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        GalleryPicture picture = pictures.get(position);
        holder.bind(picture);
    }

    @Override
    public int getItemCount() {
        return pictures != null ? pictures.size() : 0;
    }

    public void setItems(List<GalleryPicture> items) {
        this.pictures.clear();
        this.pictures.addAll(items);
        notifyDataSetChanged();
    }

    public void addItems(List<GalleryPicture> items, int start) {
        if (this.pictures.size() >= start) {
            this.pictures.addAll(start, items);
            notifyItemRangeInserted(start, items.size());
        }
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPhoto;
        private ProgressBar pbLoader;

        ImageViewHolder(View itemView) {
            super(itemView);
            ivPhoto = (ImageView) itemView.findViewById(com.kindiedays.common.R.id.thumbnail);
            pbLoader = (ProgressBar) itemView.findViewById(com.kindiedays.common.R.id.progressBar);
        }

        private void bind(GalleryPicture picture) {
            String url = picture.getUrl() + "&preferredWidth=200&preferredHeight=200";

            Glide.with(itemView.getContext()).load(url).into(new GlideDrawableImageViewTarget(ivPhoto) {
                @Override
                public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                    super.onResourceReady(drawable, anim);
                    pbLoader.setVisibility(View.GONE);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    pbLoader.setVisibility(View.GONE);
                }
            });

            if(((CheckableRelativeLayout)itemView).isChecked()){
                ivPhoto.setColorFilter(itemView.getContext().getResources()
                        .getColor(com.kindiedays.common.R.color.selectedfilter));
            }
            else{
                ivPhoto.clearColorFilter();
            }
        }
    }
}
