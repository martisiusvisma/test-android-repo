package com.kindiedays.teacherapp.ui.dailyreport;

import android.content.Context;
import android.view.ViewGroup;

import com.kindiedays.common.pojo.Place;


/**
 * Created by pleonard on 15/05/2015.
 */
public class DRChildrenAdapter extends DRChildrenContainerAdapter<Place> {

    public DRChildrenAdapter(Context context) {
        super(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DRChildButton button = new DRChildButton(mContext);
        return new ItemViewHolder(button);
    }
}
