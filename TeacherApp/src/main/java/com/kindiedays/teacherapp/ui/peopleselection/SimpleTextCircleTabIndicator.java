package com.kindiedays.teacherapp.ui.peopleselection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 18/06/2015.
 */
public class SimpleTextCircleTabIndicator extends LinearLayout {

    private TextView tvText;

    public SimpleTextCircleTabIndicator(Context context) {
        super(context);
        initViews();
    }

    public SimpleTextCircleTabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public SimpleTextCircleTabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_text_circle, this);
        tvText = (TextView) v.findViewById(R.id.text);
    }

    public void setText(String text){
        tvText.setText(text);
    }
}
