package com.kindiedays.teacherapp.ui.attendance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.components.ButtonCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PlaceBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.childprofile.ChildProfileActivity;
import com.kindiedays.teacherapp.ui.moveto.MoveToContainerDialog;
import com.kindiedays.teacherapp.ui.teacherselection.TeacherSelectionDialogFragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import static com.kindiedays.common.business.PlaceBusiness.NO_PLACE_ID;

/**
 * Created by pleonard on 27/05/2015.
 */
public class AttendanceFragment extends MainActivityFragment implements MoveToContainerDialog.ChildContainerMover,
        AddPlaceDialogFragment.TemporaryPlaceCreator, ChildrenAttendanceContainerAdapter.AttendanceInterface, Observer {
    private static final String TEACHER_SELECTION_DIALOG_TAG = "TeacherSelectionDialog";

    TabHost firstLevelTabHost;
    GroupsFragment groupsFragment;
    PlacesFragment placesFragment;
    private ButtonCustom title;
    private MoveToContainerDialog moveToDialog;
    private GroupFragment.OnMovedListenerPlace onMovedListenerPlace;
    private PlaceFragment.OnMovedListenerGroup onMovedListenerGroup;
    private List<Place> placesList = new ArrayList<>(0);
    private List<Child> childrenList = new ArrayList<>(0);
    private int loadedListsCounter;

    public void setOnMovedListenerGroup(PlaceFragment.OnMovedListenerGroup onMovedListenerGroup) {
        this.onMovedListenerGroup = onMovedListenerGroup;
    }

    public void setOnMovedListenerPlace(GroupFragment.OnMovedListenerPlace onMovedListenerPlace) {
        this.onMovedListenerPlace = onMovedListenerPlace;
    }

    private int mode = MODE_NORMAL;

    View.OnClickListener onSwitchTeacherClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            TeacherSelectionDialogFragment selectionDialogFragment = new TeacherSelectionDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean(TeacherSelectionDialogFragment.TEACHER_SELECTION_MANDATORY, false);
            selectionDialogFragment.setArguments(bundle);
            selectionDialogFragment.show(getActivity().getSupportFragmentManager(), TEACHER_SELECTION_DIALOG_TAG);
        }
    };


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_attendance, container, false);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        groupsFragment = new GroupsFragment();
        placesFragment = new PlacesFragment();
        placesFragment.setOwner(this);
        ft.replace(R.id.groups, groupsFragment).replace(R.id.places, placesFragment).commit();
        moveToDialog = (MoveToContainerDialog) v.findViewById(R.id.moveToDialog);
        moveToDialog.setOwner(this);
        firstLevelTabHost = (TabHost) v.findViewById(R.id.tabHost);
        initTabs();

        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (ButtonCustom) getActivity().findViewById(R.id.titleBtn);
        title.setVisibility(View.VISIBLE);
        Teacher currentTeacher = TeacherBusiness.getInstance().getCurrentTeacher();
        if (currentTeacher != null && currentTeacher.getName() != null) {
            title.setText(currentTeacher.getName());
        } else {
            title.setText(R.string.Attendance);
        }
        title.setOnClickListener(onSwitchTeacherClickListener);
    }

    public void switchMode(int newMode) {
        if (this.mode == newMode) {
            return;
        }
        if (newMode == MODE_NORMAL) {
            moveToDialog.setVisibility(View.GONE);
            ChildBusiness.getInstance().unselectAllChildren();

        } else if (newMode == MODE_MOVE) {
            moveToDialog.setVisibility(View.VISIBLE);
        }
        this.mode = newMode;
    }

    @Override
    protected void updateData() {
        //remove parent logic
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Webservice.isAuthorizationTokenInitialized()) {
            refreshData();
            if (TeacherBusiness.getInstance().getCurrentTeacher() != null) {
                ChildBusiness.LoadChildrenStatusTask task = new ChildBusiness.LoadChildrenStatusTask();
                task.execute();
            }
        }
        //When the teacher changes and the user is on the attendance SCREEN, this fragment should be
        //notified so that the children status can be updated. The Groups and Places fragments will
        //then be updated when the children status is updated.
        KindergartenBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        KindergartenBusiness.getInstance().deleteObserver(this);
    }

    @Override
    public void refreshData() {
        loadedListsCounter = 0;
        RefreshChildren refreshChildren = new RefreshChildren();
        pendingTasks.add(refreshChildren);
        refreshChildren.execute();
        RefreshGroups refreshGroups = new RefreshGroups();
        pendingTasks.add(refreshGroups);
        refreshGroups.execute();
        RefreshPlaces refreshPlaces = new RefreshPlaces();
        pendingTasks.add(refreshPlaces);
        refreshPlaces.execute();
    }

    private void refreshIfPlaceIsAbsent() {
        for (Child child : childrenList) {
            if (child.getCurrentPlaceId() != NO_PLACE_ID) {
                boolean isPlaceExists = false;
                for (Place place : placesList) {
                    if (child.getCurrentPlaceId() == place.getId()) {
                        isPlaceExists = true;
                    }
                }
                if (!isPlaceExists) {
                    SignoutTask signoutTask = new SignoutTask(child);
                    signoutTask.execute();
                }
            }
        }
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public void moveChildren(MoveToContainerDialog dialog, long placeId) {
        //Get selected children
        Set<Child> childrenToMove = ChildBusiness.getInstance().getSelectedChildren();
        //Move children
        SigninTask task = new SigninTask(childrenToMove, placeId);
        task.execute();
        //Switch back to normal
        switchMode(MODE_NORMAL);

        if (onMovedListenerPlace != null) {
            onMovedListenerPlace.onMoved();
        }

        if (onMovedListenerGroup != null) {
            onMovedListenerGroup.onMoved();
        }
    }

    @Override
    public void cancelMove(MoveToContainerDialog dialog) {
        switchMode(MODE_NORMAL);
    }

    public void enterMoveMode() {
        switchMode(MODE_MOVE);
    }


    public void startNap(Child child) {
        StartNapTask task = new StartNapTask(child);
        task.execute();
    }

    public void endNap(Child child) {
        EndNapTask task = new EndNapTask(child);
        task.execute();
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof KindergartenBusiness && TeacherBusiness.getInstance().getCurrentTeacher() != null) {
            //Reload children status
            ChildBusiness.LoadChildrenStatusTask task = new ChildBusiness.LoadChildrenStatusTask();
            //As there's no UI update directly involved in this task, it is not necessary to add it to the pending tasks
            task.execute();
        }
    }

    @Override
    public void createTemporaryPlace(String name, String address) {
        CreateTemporaryPlace task = new CreateTemporaryPlace(name, address);
        pendingTasks.add(task);
        task.execute();
    }

    private class CreateTemporaryPlace extends PlaceBusiness.CreateTemporaryPlaceAsyncTask {

        CreateTemporaryPlace(String name, String address) {
            super(name, address);
        }

        @Override
        protected void onPostExecute(Place place) {
            if (!checkNetworkException(place, exception)) {
                super.onPostExecute(place);
            }
            pendingTasks.remove(this);
        }
    }


    private class StartNapTask extends ChildBusiness.StartNap {

        StartNapTask(Child child) {
            super(child);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            pendingTasks.remove(this);
        }

    }


    private class EndNapTask extends ChildBusiness.EndNap {

        EndNapTask(Child child) {
            super(child);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            pendingTasks.remove(this);
        }

    }


    private class RefreshGroups extends GroupBusiness.LoadGroupsTask {

        @Override
        protected void onPostExecute(List<Group> groups) {
            if (!checkNetworkException(groups, exception)) {
                super.onPostExecute(groups);
            }
            pendingTasks.remove(this);
        }
    }


    private class RefreshChildren extends ChildBusiness.LoadChildrenTask {

        @Override
        protected List<Child> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(List<Child> children) {
            if (!checkNetworkException(children, exception)) {
                super.onPostExecute(children);
                loadedListsCounter++;
                childrenList = children;
                if (loadedListsCounter >= 2 && childrenList != null) {
                    refreshIfPlaceIsAbsent();
                }
            }
            pendingTasks.remove(this);
        }
    }


    private class RefreshPlaces extends PlaceBusiness.LoadPlacesTask {

        @Override
        protected List<Place> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(List<Place> places) {
            if (!checkNetworkException(places, exception)) {
                super.onPostExecute(places);
                loadedListsCounter++;
                placesList = places;
                if (loadedListsCounter >= 2 && childrenList != null) {
                    refreshIfPlaceIsAbsent();
                }
            }
            pendingTasks.remove(this);
        }
    }


    public void openChildProfile(long childId) {
        Intent intent = new Intent(getActivity(), ChildProfileActivity.class);
        intent.putExtra(ChildProfileActivity.CHILD_ID, childId);
        startActivity(intent);
    }

    private void initTabs() {
        firstLevelTabHost.setup();
        TabHost.TabSpec ts = firstLevelTabHost.newTabSpec("tag1");
        ts.setContent(R.id.groups);
        ts.setIndicator(createTabView(getString(R.string.Groups)));
        firstLevelTabHost.addTab(ts);
        ts = firstLevelTabHost.newTabSpec("tag2");
        ts.setContent(R.id.places);
        ts.setIndicator(createTabView(getString(R.string.Places)));
        firstLevelTabHost.addTab(ts);
    }

    private ContainersTabIndicator createTabView(String name) {
        ContainersTabIndicator indicator = new ContainersTabIndicator(getActivity());
        indicator.setText(name);
        return indicator;
    }

    public void signIn(Child child, long placeId) {
        SigninTask task = new SigninTask(child, placeId);
        task.execute();
    }

    public void signOut(Child child) {
        SignoutTask task = new SignoutTask(child);
        task.execute();
    }

    private class SigninTask extends ChildBusiness.SigninChildren {
        SigninTask(Child child, long placeId) {
            super(child, placeId);
        }

        SigninTask(Collection<Child> children, long placeId) {
            super(children, placeId);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            pendingTasks.remove(this);
        }

    }


    private class SignoutTask extends ChildBusiness.SignoutChildren {
        SignoutTask(Child child) {
            super(child);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            pendingTasks.remove(this);
        }
    }
}
