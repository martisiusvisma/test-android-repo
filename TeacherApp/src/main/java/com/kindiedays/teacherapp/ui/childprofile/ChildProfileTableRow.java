package com.kindiedays.teacherapp.ui.childprofile;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 07/10/2015.
 */
public class ChildProfileTableRow extends LinearLayout {

    private TextView tvLabel;
    private TextView tvValue;
    private CheckBox cbValue;

    public static final int NORMAL_MODE = 1;
    public static final int PHONE_MODE = 2;
    public static final int BOOLEAN_MODE = 3;

    public ChildProfileTableRow(Context context, int mode) {
        super(context);
        this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        initViews(mode);
    }

    protected void initViews(int mode) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        switch (mode) {
        case NORMAL_MODE:
            inflater.inflate(R.layout.table_row_fragment_about, this, true);
            tvValue = (TextView) findViewById(R.id.value);
            break;
        case PHONE_MODE:
            inflater.inflate(R.layout.table_row_fragment_about_phone_style, this, true);
            tvValue = (TextView) findViewById(R.id.value);
            break;
        case BOOLEAN_MODE:
            inflater.inflate(R.layout.table_row_fragment_about_boolean, this, true);
            cbValue = (CheckBox) findViewById(R.id.cb_value);
            break;
        default:
            break;
        }
        tvLabel = (TextView) findViewById(R.id.label);
        this.setOrientation(HORIZONTAL);
        this.setWeightSum(1);

    }

    public void setValues(String label, String value) {
        tvLabel.setText(label);
        tvValue.setText(value);
    }

    public void setValues(String label, boolean checked) {
        tvLabel.setText(label);
        cbValue.setChecked(checked);
    }

}
