package com.kindiedays.teacherapp.ui.dailyreport.meals;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kindiedays.common.pojo.Meal;

/**
 * Created by Giuseppe Franco - Starcut on 13/06/16.
 */
public class SpinAdapterMeal extends ArrayAdapter<Meal>{


    public SpinAdapterMeal(Context context, int textViewResourceId,
                           Meal[] values) {
        super(context, textViewResourceId, values);
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(this.getItem(position).getText());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(this.getItem(position).getText());
        return label;
    }
}