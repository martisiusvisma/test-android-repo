package com.kindiedays.teacherapp.ui.childprofile;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.CalendarTitleFormatter;
import com.kindiedays.common.utils.GlideUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.TeacherApp;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.pojo.Note;

import java.util.Calendar;
import java.util.List;

/**
 * Created by pleonard on 08/06/2015.
 */
public class NotesAdapter extends BaseAdapter {

    List<Note> notes;
    private Activity activity;


    public NotesAdapter(Activity activity) {
        this.activity = activity;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (notes == null) {
            return 0;
        }
        return notes.size();
    }

    @Override
    public Note getItem(int position) {
        if (notes == null) {
            return null;
        }
        return notes.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (notes == null) {
            return 0;
        }
        Note note = getItem(position);
        return note.getId();
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = activity.getLayoutInflater().inflate(R.layout.item_note, null);
            holder.bodyText = (TextView) convertView.findViewById(R.id.bodyText);
            holder.titleText = (TextView) convertView.findViewById(R.id.titleText);
            holder.teacherName = (TextView) convertView.findViewById(R.id.teacherName);
            holder.teacherPicture = (ImageView) convertView.findViewById(R.id.teacherPicture);
            holder.creationDate = (TextView) convertView.findViewById(R.id.creationDate);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Note note = getItem(position);
        if (note != null) {
            Teacher teacher = TeacherBusiness.getInstance().getTeacherForId(note.getAuthorTeacherId());
            holder.titleText.setText(note.getTitle());
            holder.bodyText.setText(note.getText());
            if (teacher != null) {
                holder.teacherName.setText(teacher.getName());
                Glide.with(activity).load(teacher.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(holder.teacherPicture);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(note.getCreationDate().toDate());
            holder.creationDate.setText(CalendarTitleFormatter.getDayOfWeekString(TeacherApp.getApp(), calendar)
                    .concat(note.getCreationDate().toString(" dd.MM")));
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView teacherPicture;
        TextView teacherName;
        TextView creationDate;
        TextView titleText;
        TextView bodyText;
    }

}
