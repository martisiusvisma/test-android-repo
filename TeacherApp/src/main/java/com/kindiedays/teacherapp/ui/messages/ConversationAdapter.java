package com.kindiedays.teacherapp.ui.messages;

import android.app.Activity;

import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.Set;

/**
 * Created by pleonard on 01/10/2015.
 */
public class ConversationAdapter extends com.kindiedays.common.ui.messages.ConversationAdapter {
    public ConversationAdapter(Activity activity) {
        super(activity);
    }

    @Override
    protected boolean isConversationPartyCurrentUser(ConversationParty party, boolean isTeacherAsKindergarten) {
        return ((isTeacherAsKindergarten && party.getType().equals(ConversationParty.KINDERGARTEN_TYPE)) || party.getType().equals(ConversationParty.TEACHER_TYPE) && party.getPartyId() == TeacherBusiness.getInstance().getCurrentTeacher().getId());
    }

    protected boolean isTeacherAsKindergarten(Set<ConversationParty> conversationParties){
        boolean isKindergartenInParties = false;
        boolean isCurrentTeacherInParties = false;
        for(ConversationParty conversationParty : conversationParties){
            if(conversationParty.getType().equals(ConversationParty.KINDERGARTEN_TYPE)){
                isKindergartenInParties = true;
            }
            if(conversationParty.getType().equals(ConversationParty.TEACHER_TYPE) && conversationParty.getPartyId() == TeacherBusiness.getInstance().getCurrentTeacher().getId()){
                isCurrentTeacherInParties = true;
            }
        }
        return isKindergartenInParties && !isCurrentTeacherInParties;
    }

}
