package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by pleonard on 21/04/2015.
 */
public class PlaceButton extends LinearLayout {
    public PlaceButton(Context context) {
        super(context);
    }

    public PlaceButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlaceButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
