package com.kindiedays.teacherapp.ui.teacherselection;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.PersonSelectionDialogFragment;
import com.kindiedays.common.ui.personselection.PersonAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.network.session.PatchSession;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.msebera.android.httpclient.HttpStatus;

/**
 * Created by pleonard on 12/06/2015.
 */
public class TeacherSelectionDialogFragment extends PersonSelectionDialogFragment {
    public static final String TEACHER_SELECTION_MANDATORY = "teacherSelectionMandatory";
    private static final int NO_SELECTION = -1;

    private ListView teacherList;
    private PersonAdapter<Teacher> teacherAdapter;
    private EditText pinCode;
    private Button okButton;
    private ProgressBar progress;
    private boolean teacherSelectionMandatory;
    private boolean teacherSelected;
    private Set<AsyncTask> pendingTasks = new HashSet<>();
    private Set<PatchSessionTask> patchTasks = new HashSet<>();
    private int selectedPersonPosition = NO_SELECTION;

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (selectedPersonPosition != NO_SELECTION) {
                int checkedTeacherPosition = teacherList.getCheckedItemPosition();
                Teacher selectedTeacher = TeacherBusiness.getInstance().getTeachers().get(checkedTeacherPosition);
                PatchSessionTask task = new PatchSessionTask(selectedTeacher, pinCode.getText().toString());
                patchTasks.add(task);
                task.execute();
            }
        }
    };

    AdapterView.OnItemClickListener onTeacherClickedListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectedPersonPosition = position;
            //Open keyboard and send focus to pin code edittext
            pinCode.setVisibility(View.VISIBLE);
            pinCode.requestFocus();
            ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(pinCode, InputMethodManager.SHOW_IMPLICIT);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_pincode, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        teacherList = (ListView) v.findViewById(R.id.personList);
        teacherAdapter = new PersonAdapter<>(getActivity());
        teacherList.setAdapter(teacherAdapter);
        pinCode = (EditText) v.findViewById(R.id.pincode);
        okButton = (Button) v.findViewById(R.id.okButton);
        progress = (ProgressBar) v.findViewById(R.id.progressBar);
        getDialog().setCanceledOnTouchOutside(!teacherSelectionMandatory);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        okButton.setOnClickListener(onOkClickedListener);
        teacherList.setOnItemClickListener(onTeacherClickedListener);
        LoadTeachersListTask task = new LoadTeachersListTask();
        pendingTasks.add(task);
        task.execute();
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        teacherSelectionMandatory = getArguments().getBoolean(TEACHER_SELECTION_MANDATORY);
    }

    @Override
    public void onStop() {
        super.onStop();
        stopPendingTasks();
    }

    @SuppressWarnings("squid:S3398")
    private void checkNetworkException(KindieDaysNetworkException exception) {
        if (exception.isLocalizedMessage()) {
            Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            if (exception.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                listener.onNotAuthorized();
            } else {
                Log.e(this.getClass().getSimpleName(), "Internal error");
            }
        }
    }

    protected void stopPendingTasks() {
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        for (PatchSessionTask task : patchTasks) {
            task.cancel(true);
        }
        patchTasks.clear();
        pendingTasks.clear();
    }

    private void hideSoftKeyboard() {
        if (getActivity() != null) {
            View view = getActivity().getWindow().getCurrentFocus();
            if (view != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        hideSoftKeyboard();
        if (teacherSelectionMandatory && !teacherSelected && getActivity() != null) {
            listener.onNoOneSelected();
        }
    }

    private class LoadTeachersListTask extends TeacherBusiness.LoadTeachersTask {

        @Override
        protected List<Teacher> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.failed_to_load_message, KindieDaysApp.getApp().getString(R.string.teacher_list));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Teacher> teachers) {
            super.onPostExecute(teachers);
            if (teachers == null && exception != null) {
                checkNetworkException(exception);
            } else {
                if (teachers != null) {
                    teacherAdapter.setPersons(teachers);
                    if (TeacherBusiness.getInstance().getCurrentTeacher() != null) {
                        teacherList.setItemChecked(teachers.indexOf(TeacherBusiness.getInstance().getCurrentTeacher()), true);
                        selectedPersonPosition = teachers.indexOf(TeacherBusiness.getInstance().getCurrentTeacher());
                        if (selectedPersonPosition != NO_SELECTION) {
                            pinCode.setVisibility(View.VISIBLE);
                            pinCode.requestFocus();
                            ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(pinCode, InputMethodManager.SHOW_IMPLICIT);
                        }
                        teacherList.setSelection(teachers.indexOf(TeacherBusiness.getInstance().getCurrentTeacher()));
                    }
                    progress.setVisibility(View.GONE);
                    okButton.setVisibility(View.VISIBLE);
                }
            }
            pendingTasks.remove(this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
            okButton.setVisibility(View.GONE);
        }
    }


    private class PatchSessionTask extends NetworkAsyncTask<Boolean> {

        private final Teacher teacher;
        private final String pinCode;

        public PatchSessionTask(Teacher teacher, String pinCode) {
            this.teacher = teacher;
            this.pinCode = pinCode;
        }

        @Override
        protected Boolean doNetworkTask() throws Exception {
            try {
                PatchSession.patch(teacher.getId(), pinCode);
                return true;
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = getString(R.string.failed_to_load_message, getString(R.string.data));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (!isCancelled() && exception == null && success != null && success) {
                TeacherBusiness.getInstance().setCurrentTeacher(teacher);

                teacherSelected = true;
                listener.onPersonSelected();

                dismiss();
            } else {
                if (exception != null && exception.isLocalizedMessage()) {
                    Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "6 Internal application error", Toast.LENGTH_LONG).show();
                }
            }
            pendingTasks.remove(this);
            patchTasks.remove(this);
        }
    }
}
