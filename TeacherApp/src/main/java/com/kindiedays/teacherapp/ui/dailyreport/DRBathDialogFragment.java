package com.kindiedays.teacherapp.ui.dailyreport;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kindiedays.common.components.CheckedTextViewCustom;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.daily.ResourceSquaredTab;
import com.kindiedays.common.pojo.BathRes;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.common.utils.constants.DailyReportConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRBathDialogFragment extends MainActivityDialogFragment {
    private HorizontalScrollView counterTabHostlScrollView;

    private List<BathRes> bathItems = new ArrayList<>();
    private TextViewCustom title;
    private TextViewCustom childrenList;

    private RadioGroup rgToilet;
    private RadioButton rbDiaper;
    private RadioButton rbPot;
    private RadioButton rbToilet;

    private EditText bodyEt;
    private Spinner typeSpinner;
    private Spinner constSpinner;
    private Button timePickerEtBath;
    private FragmentTabHost timesTabHost;
    private View loaderContainer;

    private static final String TYPE = "Type";
    private int bathCounter = 0;
    private int selectedTab = 0;

    List<Child> childOrChildren = new ArrayList<>();

    @Override
    public void onDestroyView() {
        selectedTab = 0;
        bathCounter = 0;
        bathItems.clear();
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    @Override
    public void refreshData() {
        // We should use this to get the existing data
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Bath));
        if (childOrChildren != null && childOrChildren.size() == 1) {
            DateTime now = new DateTime();
            LoadBathTask task = new LoadBathTask((int) childOrChildren.get(0).getId(), now);
            pendingTasks.add(task);
            task.execute();
        } else {
            BathRes newBathItem = new BathRes();
            ArrayList<Integer> childrenId = new ArrayList<>();
            for (Child str : childOrChildren) {
                childrenId.add((int) str.getId());
            }
            newBathItem.setChildren(childrenId);
            DateTime now = new DateTime();
            newBathItem.setDate(now);
            newBathItem.setTime(now);
            newBathItem.setLocal(true);
            bathItems.add(newBathItem);
            bathCounter++;
        }
    }

    @Override
    public void onPause() {
        dismiss();
        super.onPause();  // Always call the superclass method first
    }

    @Override
    @SuppressWarnings({"squid:S1188", "squid:S1604"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dr_bath, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        scrollingView = (ScrollView) v.findViewById(R.id.parentLayout);
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);

        CheckedTextViewCustom timePickerTitle = (CheckedTextViewCustom) v.findViewById(R.id.timePickerTitle);
        timePickerTitle.setText(getString(com.kindiedays.common.R.string.daily_time));

        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);
        childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);
        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder();
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }
        counterTabHostlScrollView = (HorizontalScrollView) v.findViewById(R.id.counterTabHostlScrollView);

        timePickerEtBath = (Button) v.findViewById(R.id.timePickerEt);
        timePickerEtBath.setInputType(InputType.TYPE_NULL);
        timePickerEtBath.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent bathEvent) {
                int mHour;
                int mMinute;
                if (MotionEvent.ACTION_UP == bathEvent.getAction()) {

                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.timePickerStyle,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    timePickerEtBath.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                                    for (int i = 0; i < bathItems.size(); i++) {
                                        if (i == selectedTab) {
                                            DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
                                            bathItems.get(i).setDate(new DateTime());
                                            bathItems.get(i).setTime(formatter.parseDateTime(hourOfDay + ":" + minute));
                                        }
                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
                return false;
            }
        });

        Button addBathBtn = (Button) v.findViewById(R.id.addBtn);
        addBathBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
                    String newId = String.valueOf(bathCounter + 1);
                    indicator.setResourceName(newId);
                    TabHost.TabSpec ts = timesTabHost.newTabSpec("item" + newId).setIndicator(indicator);
                    Bundle bundle = new Bundle();
                    bundle.putString(TYPE, newId);
                    timesTabHost.addTab(ts, ItemCellFragment.class, bundle);
                    bathCounter = bathCounter + 1;

                    counterTabHostlScrollView.postDelayed(new Runnable() {
                        public void run() {
                            counterTabHostlScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                            int currentTab = timesTabHost.getCurrentTab();
                            timesTabHost.setCurrentTab(currentTab + 1);
                            refresh();
                        }
                    }, 100L);

                    BathRes newBathItem = new BathRes();
                    if (childOrChildren.size() == 1) {
                        newBathItem.setChild((int) childOrChildren.get(0).getId());
                    } else {
                        ArrayList<Integer> childrenId = new ArrayList<>();
                        for (Child str : childOrChildren) {
                            childrenId.add((int) str.getId());
                        }
                        newBathItem.setChildren(childrenId);
                    }
                    DateTime now = new DateTime();
                    newBathItem.setIndex(0);
                    newBathItem.setDate(now);
                    newBathItem.setTime(now);
                    newBathItem.setLocal(true);
                    bathItems.add(newBathItem);
                }
                return false;
            }
        });

        rgToilet = (RadioGroup) v.findViewById(R.id.rg_toilet);
        rbDiaper = (RadioButton) v.findViewById(R.id.rb_diaper);
        rbPot = (RadioButton) v.findViewById(R.id.rb_pot);
        rbToilet = (RadioButton) v.findViewById(R.id.rb_toilet);
        rbDiaper.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_wc_diaper_bg, R.raw.dr_wc_diaper_circle_tp), null, null, null);
        rbPot.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_wc_potty_bg, R.raw.dr_wc_potty_circle_tp), null, null, null);
        rbToilet.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_wc_toilet_bg, R.raw.dr_wc_toilet_circle_tp), null, null, null);

        rgToilet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                List<String> temp = new ArrayList<>();
                switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.rb_diaper:
                    temp.add(DailyReportConstants.DIAPER_TAG);
                    bathItems.get(selectedTab).setEquipment(temp);
                    break;
                case R.id.rb_pot:
                    temp.add(DailyReportConstants.POTTY_TAG);
                    bathItems.get(selectedTab).setEquipment(temp);
                    break;
                case R.id.rb_toilet:
                    temp.add(DailyReportConstants.TOILET_TAG);
                    bathItems.get(selectedTab).setEquipment(temp);
                    break;
                default:
                    break;
                }
            }
        });

        timesTabHost = (FragmentTabHost) v.findViewById(android.R.id.tabhost);
        timesTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realmoodtabcontent);
        timesTabHost.addTab(timesTabHost.newTabSpec("item0").setIndicator(createSquaredTabView("1")), ItemCellFragment.class, new Bundle());
        timesTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                selectedTab = timesTabHost.getCurrentTab();
                refresh();
            }
        });

        bodyEt = (EditText) v.findViewById(R.id.bodyEdit);
        bodyEt.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                for (int i = 0; i < bathItems.size(); i++) {
                    if (i == selectedTab) {
                        bathItems.get(i).setNotes(bodyEt.getText().toString());
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                scrollingView.fullScroll(View.FOCUS_DOWN);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //no impl
            }
        });

        typeSpinner = (Spinner) v.findViewById(R.id.shit_type_spinner);
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                for (int i = 0; i < bathItems.size(); i++) {
                    if (i == selectedTab) {
                        if (typeSpinner.getSelectedItemId() == 1) {
                            bathItems.get(i).setType(DailyReportConstants.DRY);
                        } else if (typeSpinner.getSelectedItemId() == 2) {
                            bathItems.get(i).setType(DailyReportConstants.WET);
                        } else if (typeSpinner.getSelectedItemId() == 3) {
                            bathItems.get(i).setType(DailyReportConstants.BOWEL_MOVEMENT);
                        } else if (typeSpinner.getSelectedItemId() == 4) {
                            bathItems.get(i).setType(DailyReportConstants.WET_AND_BOWEL_MOVEMENT);
                        }
                    }
                }
                if (arg2 == 3 || arg2 == 4) {
                    constSpinner.setVisibility(View.VISIBLE);
                } else {
                    constSpinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // mo impl

            }
        });
        constSpinner = (Spinner) v.findViewById(R.id.shit_constitution_spinner);
        constSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                for (int i = 0; i < bathItems.size(); i++) {
                    if (i == selectedTab) {
                        if (constSpinner.getSelectedItemId() == 1) {
                            bathItems.get(i).setConstitution(DailyReportConstants.LOOSE_TAG);
                        } else if (constSpinner.getSelectedItemId() == 2) {
                            bathItems.get(i).setConstitution(DailyReportConstants.NORMAL_TAG);
                        } else if (constSpinner.getSelectedItemId() == 3) {
                            bathItems.get(i).setConstitution(DailyReportConstants.SOLID_TAG);
                        }
                    }
                }

            }


            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // no impl
            }
        });

        Button okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickedListener);
        loaderContainer = v.findViewById(R.id.loader_container);
        return v;
    }

    public static class ItemCellFragment extends Fragment {
    }

    private ResourceSquaredTab createSquaredTabView(String current) {

        ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
        indicator.setResourceName(current);
        return indicator;
    }

    @SuppressWarnings({"squid:S1213", "squid:S1188", "squid:S134"})
    View.OnClickListener onOkClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            List<BathRes> outgoingItems = new ArrayList<>();
            for (int i = 0; i < bathItems.size(); i++) {
                if (bathItems.get(i).getLocal()) {
                    outgoingItems.add(bathItems.get(i));
                }
            }
            if (!outgoingItems.isEmpty()) {
                for (int i = 0; i < outgoingItems.size(); i++) {
                    if (outgoingItems.get(i).getChildren() != null) {
                        for (int n = 0; n < outgoingItems.get(i).getChildren().size(); n++) {
                            BathRes newBathItem = new BathRes();
                            newBathItem.setChild(outgoingItems.get(i).getChildren().get(n));
                            newBathItem.setDate(outgoingItems.get(i).getDate());
                            newBathItem.setTime(outgoingItems.get(i).getTime());
                            newBathItem.setLocal(outgoingItems.get(i).getLocal());
                            newBathItem.setEquipment(outgoingItems.get(i).getEquipment());
                            newBathItem.setType(outgoingItems.get(i).getType());
                            newBathItem.setConstitution(outgoingItems.get(i).getConstitution());
                            newBathItem.setNotes(outgoingItems.get(i).getNotes());
                            outgoingItems.add(newBathItem);
                        }
                        outgoingItems.remove(i);
                    }
                }
                SaveNewBathTask task = new SaveNewBathTask(outgoingItems);
                pendingTasks.add(task);
                task.execute();
            } else {
                dismiss();
            }
        }
    };

    @Override
    public void onDismiss(DialogInterface bathDialog) {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
        }
        super.onDismiss(bathDialog);
    }

    class SaveNewBathTask extends DailyReportBusiness.PostBathTask {
        public SaveNewBathTask(List<BathRes> items) {
            super(items);
        }

        @Override
        protected void onPostExecute(List<BathRes> items) {
            if (!checkNetworkException(items, exception)) {
                super.onPostExecute(items);
                Log.d(TAG, "Bath created");
            }
            pendingTasks.remove(this);
            dismiss();
        }
    }


    class LoadBathTask extends DailyReportBusiness.GetBathTask {
        public LoadBathTask(Integer child, DateTime data) {
            super(child, data);
        }

        @Override
        protected List<BathRes> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<BathRes> items) {
            super.onPostExecute(items);
            if (exception == null && items != null) {
                final int tabSize = items.size();
                if (tabSize != 0) {
                    bathItems = items;
                    bathCounter = items.size() + 1;

                    // I start from one cause the first tab is fixed to dont to leave the tabhost empty
                    timesTabHost.getTabWidget().removeView(timesTabHost.getTabWidget().getChildTabViewAt(0));
                    for (int i = 0; i < tabSize; i++) {
                        String newId = String.valueOf(i + 1);
                        TabHost.TabSpec tsMood = timesTabHost.newTabSpec("item" + newId).setIndicator(createSquaredTabView(newId));
                        Bundle bundleMood = new Bundle();
                        bundleMood.putString(TYPE, newId);
                        timesTabHost.addTab(tsMood, ItemCellFragment.class, bundleMood);
                    }
                    BathRes newBathItem = new BathRes();
                    newBathItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newBathItem.setDate(now);
                    newBathItem.setTime(now);
                    newBathItem.setLocal(true);
                    bathItems.add(newBathItem);

                    String newId = String.valueOf(items.size());
                    TabHost.TabSpec tsMood = timesTabHost.newTabSpec("item" + newId).setIndicator(createSquaredTabView(newId));
                    Bundle bundleMood = new Bundle();
                    bundleMood.putString(TYPE, newId);
                    timesTabHost.addTab(tsMood, ItemCellFragment.class, bundleMood);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            timesTabHost.getTabWidget().getChildAt(tabSize).performClick();
                        }
                    }, 1);

                } else {
                    // If there are not datas for the current date i create an object and sign the current selection on that object
                    BathRes newBathItem = new BathRes();
                    newBathItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newBathItem.setDate(now);
                    newBathItem.setTime(now);
                    newBathItem.setLocal(true);
                    bathItems.add(newBathItem);
                    bathCounter++;
                }
                refresh();
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), R.string.dialog_no_internet_message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
                dismiss();
            }
            pendingTasks.remove(this);
            loaderContainer.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings({"squid:S134", "squid:MethodCyclomaticComplexity"})
    public void refresh() {
        for (int i = 0; i < bathItems.size(); i++) {
            if (i == selectedTab) {

                if (bathItems.get(i).getTime() != null) {
                    timePickerEtBath.setText(bathItems.get(i).getTime().toString("HH:mm"));
                    if (bathItems.get(i).getLocal()) {
                        timePickerEtBath.setEnabled(true);
                    } else {
                        timePickerEtBath.setEnabled(false);
                    }
                } else {
                    timePickerEtBath.setText("select");
                }

                if (bathItems.get(i).getNotes() != null) {
                    bodyEt.setText(bathItems.get(i).getNotes());
                    if (bathItems.get(i).getLocal()) {
                        bodyEt.setEnabled(true);
                    } else {
                        bodyEt.setEnabled(false);
                    }
                } else {
                    bodyEt.setText("");
                    bodyEt.setEnabled(true);
                }

                if (bathItems.get(i).getEquipment() != null) {
                    if (bathItems.get(i).getEquipment().get(0).equals(DailyReportConstants.DIAPER_TAG)) {
                        rbDiaper.setChecked(true);
                    } else if (bathItems.get(i).getEquipment().get(0).equals(DailyReportConstants.POTTY_TAG)) {
                        rbPot.setChecked(true);
                    } else if (bathItems.get(i).getEquipment().get(0).equals(DailyReportConstants.TOILET_TAG)) {
                        rbToilet.setChecked(true);
                    }
                } else {
                    rbDiaper.setChecked(false);
                    rbPot.setChecked(false);
                    rbToilet.setChecked(false);
                }

                if (bathItems.get(i).getType() != null) {
                    if (bathItems.get(i).getType().equals(DailyReportConstants.DRY)) {
                        typeSpinner.setSelection(1);
                    } else if (bathItems.get(i).getType().equals(DailyReportConstants.WET)) {
                        typeSpinner.setSelection(2);
                    } else if (bathItems.get(i).getType().equals(DailyReportConstants.BOWEL_MOVEMENT)) {
                        typeSpinner.setSelection(3);
                    } else if (bathItems.get(i).getType().equals(DailyReportConstants.WET_AND_BOWEL_MOVEMENT)) {
                        typeSpinner.setSelection(4);
                    }
                } else {
                    typeSpinner.setSelection(0);
                }

                if (bathItems.get(i).getConstitution() != null) {
                    if (bathItems.get(i).getConstitution().equals(DailyReportConstants.LOOSE_TAG)) {
                        constSpinner.setSelection(1);
                        constSpinner.setVisibility(View.VISIBLE);
                    } else if (bathItems.get(i).getConstitution().equals(DailyReportConstants.NORMAL_TAG)) {
                        constSpinner.setSelection(2);
                        constSpinner.setVisibility(View.VISIBLE);
                    } else if (bathItems.get(i).getConstitution().equals(DailyReportConstants.SOLID_TAG)) {
                        constSpinner.setSelection(3);
                        constSpinner.setVisibility(View.VISIBLE);
                    }
                } else {
                    constSpinner.setVisibility(View.GONE);
                    constSpinner.setSelection(0);
                }

                if (bathItems.get(i).getLocal()) {
                    for (int r = 0; r < rgToilet.getChildCount(); r++) {
                        rgToilet.getChildAt(r).setEnabled(true);
                    }

                    typeSpinner.setEnabled(true);
                    constSpinner.setEnabled(true);
                } else {
                    for (int r = 0; r < rgToilet.getChildCount(); r++) {
                        rgToilet.getChildAt(r).setEnabled(false);
                    }
                    typeSpinner.setEnabled(false);
                    constSpinner.setEnabled(false);
                }
            }
        }
    }
}