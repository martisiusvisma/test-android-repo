package com.kindiedays.teacherapp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.SessionBusiness;
import com.kindiedays.common.components.ButtonCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.Webservice;
import com.kindiedays.common.pojo.Session;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.pushnotifications.BroadcastReceiver;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.ui.MenuAdapter;
import com.kindiedays.common.ui.meals.MealsFragment;
import com.kindiedays.common.utils.ArrayUtils;
import com.kindiedays.common.utils.GlideUtils;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.DailyReportBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.album.AlbumMainTeacherFragment;
import com.kindiedays.teacherapp.ui.attendance.AttendanceFragment;
import com.kindiedays.teacherapp.ui.calendar.MonthCalendarFragment;
import com.kindiedays.teacherapp.ui.dailyreport.DailyReportFragment;
import com.kindiedays.teacherapp.ui.events.EventsFragment;
import com.kindiedays.teacherapp.ui.messages.ConversationsFragment;
import com.kindiedays.teacherapp.ui.newjournalpost.NewJournalPostActivity;
import com.kindiedays.teacherapp.ui.newpicture.NewPictureActivity;
import com.kindiedays.teacherapp.ui.settings.SettingsFragment;
import com.kindiedays.teacherapp.ui.teacherselection.TeacherSelectionDialogFragment;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import io.fabric.sdk.android.Fabric;

import static com.kindiedays.teacherapp.util.constants.TeacherAppConstants.CHANGE_LANGUAGE_TAG;

/**
 * Created by pleonard on 27/05/2015.
 */
public class MainActivity extends com.kindiedays.common.ui.MainActivity implements Observer {

    private static final int MENU_POSITION_ATTENDANCE = 0;
    private static final int MENU_POSITION_MESSAGES = 1;
    private static final int MENU_POSITION_EVENTS = 2;
    private static final int MENU_POSITION_ALBUM = 3;
    private static final int MENU_POSITION_MEALS = 4;
    private static final int MENU_POSITION_CALENDAR = 5;
    private static final int MENU_POSITION_DAILY_REPORT = 6;
    private static final int MENU_POSITION_SETTINGS = 7;

    private static Integer[] topMenuStringIds = new Integer[]{R.string.Attendance, R.string.Messages, R.string.Events, R.string.Album, R.string.Meals, R.string.Calendar, R.string.Daily_Report, R.string.Settings};
    private static Integer[] bottomMenuStringIds = new Integer[]{R.string.New_picture, R.string.New_journal_post, R.string.Support};

    private static final Integer[] topMenuIconIds = new Integer[]{R.raw.user_wbg, R.raw.comments_wbg, R.raw.morph_star_wbg, R.raw.image_wbg, R.raw.apple_wbg, R.raw.calendar_wbg, R.raw.morph_bar_chart_wbg, R.raw.gears_wbg};
    private static final Integer[] topMenuSelectedIconIds = new Integer[]{R.raw.user_bbg, R.raw.comments_bbg, R.raw.morph_star_bbg, R.raw.image_bbg, R.raw.apple_bbg, R.raw.calendar_bbg, R.raw.morph_bar_chart_bbg, R.raw.gears_bbg};
    private static final Integer[] bottomMenuIconIds = new Integer[]{R.raw.camera_wbg, R.raw.bug_wbg, R.raw.help_wbg};
    private static final Integer[] bottomMenuSelectedIconIds = new Integer[]{R.raw.camera_bbg, R.raw.bug_bbg, R.raw.help_bbg};

    @SuppressWarnings("squid:S1170")
    private final Boolean[] topMenuBoolean =
            new Boolean[]{true, MenuItemsStates.MESSAGES_STATE.getState(),
                    MenuItemsStates.EVENTS_STATE.getState(), MenuItemsStates.CAMERA_STATE.getState(),
                    MenuItemsStates.MEALS_STATE.getState(), MenuItemsStates.CALENDAR_STATE.getState(),
                    MenuItemsStates.DAILY_REPORT_STATE.getState(), true};
    private final Boolean[] bottomMenuBoolean =
            new Boolean[]{MenuItemsStates.CAMERA_STATE.getState(), MenuItemsStates.JOURNAL_STATE.getState(), true};

    private Integer[] topMenuCounterIds = new Integer[]{0, 0, 0, 0, 0, 0, 0, 0}; //counter left menu
    private static final Integer[] bottomMenuCounterIds = new Integer[]{0, 0, 0};

    private static final String ATTENDANCE_TAG = "attendance";
    private static final String MESSAGES_TAG = "messages";
    private static final String EVENTS_TAG = "events";
    private static final String ALBUM_TAG = "album";
    private static final String MEALS_TAG = "meals";
    private static final String CALENDAR_TAG = "calendar";
    private static final String DAILY_TAG = "daily_report";
    private static final String SETTINGS_TAG = "settings";

    //To be updated if items are added in the top menu
    private static final int OFFSET_BOTTOM_MENU_INDEX = 8;
    private static final int MENU_POSITION_NEW_PICTURE = 8;
    private static final int MENU_POSITION_NEW_JOURNAL_POST = 9;
    private static final int MENU_POSITION_SUPPORT = 10;

    private static final int MENU_MERGE_THRESHOLD = OFFSET_BOTTOM_MENU_INDEX * 65 + 180; //if SCREEN smaller than this, merge the bottom and top menu. 65 is about the size of a menu item, 180 should be enough to fit the action bar, status bar and navigation buttons

    private static final String TEACHER_SELECTION_DIALOG_TAG = "TeacherSelectionDialog";

    public static final String OPEN_FROM_NOTIFICATION = "fromNotification";

    private TextView teacherName;
    private ImageView teacherPicture;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Webservice.isAuthorizationTokenInitialized()) {
                SessionTask task = new SessionTask();
                pendingTasks.add(task);
                task.execute();
            }
        }
    };
    
    private View.OnClickListener onSwitchTeacherClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            TeacherSelectionDialogFragment selectionDialogFragment = new TeacherSelectionDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean(TeacherSelectionDialogFragment.TEACHER_SELECTION_MANDATORY, false);
            selectionDialogFragment.setArguments(bundle);
            selectionDialogFragment.show(getSupportFragmentManager(), TEACHER_SELECTION_DIALOG_TAG);
        }
    };

    public MainActivity() {
        super();
        fragmentTags = Arrays.asList(ATTENDANCE_TAG, MESSAGES_TAG, EVENTS_TAG, ALBUM_TAG, MEALS_TAG, CALENDAR_TAG, DAILY_TAG, SETTINGS_TAG);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        teacherName = (TextView) findViewById(R.id.teacherName);
        teacherPicture = (ImageView) findViewById(R.id.teacherPicture);
        ImageView switchTeacher = (ImageView) findViewById(R.id.switchTeacherButton);
        switchTeacher.setOnClickListener(onSwitchTeacherClickListener);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menu = (ListView) findViewById(R.id.menuList);
        bottomMenu = (ListView) findViewById(R.id.bottomMenu);
        navigationMenu = (RelativeLayout) findViewById(R.id.left_drawer);
        onCreateInitMenu();
        DailyReportBusiness.getInstance(); //Init dailyReportBusiness with the Teacher module instance

        Intent intent = getIntent();
        String tag = intent.getStringExtra(CHANGE_LANGUAGE_TAG);
        if (!TextUtils.isEmpty(tag)) {
            openPage(MENU_POSITION_SETTINGS, true);
            setJustCreated(false);
        }
        SharedPreferences sharedPreferences = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE);
        if (!sharedPreferences.contains(com.kindiedays.common.ui.MainActivity.NOTIFICATIONS)){
            SharedPreferences.Editor editor = getSharedPreferences(KindieDaysApp.SETTINGS, MODE_PRIVATE).edit();
            editor.putBoolean(com.kindiedays.common.ui.MainActivity.NOTIFICATIONS, true).commit();
        }

//        Button crashButton = new Button(this);
//        crashButton.setText("Crash!");
//        crashButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Crashlytics.getInstance().crash(); // Force a crash
//            }
//        });
//
//        addContentView(crashButton, new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT));

    }

    @Override
    public void onStop() {
        super.onStop();
        TeacherBusiness.getInstance().deleteObserver(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        TeacherBusiness.getInstance().addObserver(this);
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("message_notification"));
        if (Webservice.isAuthorizationTokenInitialized()) {
            if (TeacherBusiness.getInstance().isRequirePinCode()) {
                if (getSupportFragmentManager().findFragmentByTag(TEACHER_SELECTION_DIALOG_TAG) == null) {
                    TeacherSelectionDialogFragment dialog = new TeacherSelectionDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(TeacherSelectionDialogFragment.TEACHER_SELECTION_MANDATORY, true);
                    dialog.setArguments(bundle);
                    dialog.show(getSupportFragmentManager(), TEACHER_SELECTION_DIALOG_TAG);
                }
            } else if (TeacherBusiness.getInstance().getCurrentTeacher() != null) {
                SessionTask task = new SessionTask();
                pendingTasks.add(task);
                task.execute();
                updateTeacher();
            }
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }


    protected void initKindergarten() {
        Log.d(TAG, "initKindergarten");
        KindergartenTask task = new KindergartenTask();
        task.execute();
        pendingTasks.add(task);
    }


    public int initMenu() {
        Point size = PictureUtils.getScreenDimensions(this);
        if (PictureUtils.convertPixelsToDp(size.y, this) > MENU_MERGE_THRESHOLD) {
            menu.setAdapter(new MenuAdapter(this, R.layout.item_menu, topMenuStringIds, topMenuIconIds, topMenuSelectedIconIds, topMenuCounterIds, topMenuBoolean));
        } else {
            Integer[] mergeMenuStrings = ArrayUtils.mergeArrays(topMenuStringIds, bottomMenuStringIds);
            Integer[] mergeMenuDrawables = ArrayUtils.mergeArrays(topMenuIconIds, bottomMenuIconIds);
            Integer[] mergeMenuSelectedDrawables = ArrayUtils.mergeArrays(topMenuSelectedIconIds, bottomMenuSelectedIconIds);
            Integer[] mergeMenuCounters = ArrayUtils.mergeArrays(topMenuCounterIds, bottomMenuCounterIds);
            Boolean[] mergeMenuBoolean = ArrayUtils.mergeArrays(topMenuBoolean, bottomMenuBoolean);
            menu.setAdapter(new MenuAdapter(this, R.layout.item_menu, mergeMenuStrings, mergeMenuDrawables, mergeMenuSelectedDrawables, mergeMenuCounters, mergeMenuBoolean));
        }
        menu.setOnItemClickListener(onMenuClickListener);
        return topMenuStringIds.length;
    }

    protected int initBottomMenu() {
        Point size = PictureUtils.getScreenDimensions(this);
        if (PictureUtils.convertPixelsToDp(size.y, this) > MENU_MERGE_THRESHOLD) {
            bottomMenu.setAdapter(new MenuAdapter(this, R.layout.item_menu, bottomMenuStringIds, bottomMenuIconIds, bottomMenuSelectedIconIds, bottomMenuCounterIds, bottomMenuBoolean));
            bottomMenu.setOnItemClickListener(onBottomMenuClickListener);
        }
        return bottomMenuStringIds.length;
    }

    @SuppressWarnings("squid:MethodCyclomaticComplexity")
    protected void openPage(int pageIndex, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Intent intent;
        MainActivityFragment fragment = null;
        switch (pageIndex) {
        case MENU_POSITION_ATTENDANCE:
            fragment = new AttendanceFragment();
            break;
        case MENU_POSITION_MESSAGES:
            fragment = new ConversationsFragment();
            break;
        case MENU_POSITION_ALBUM:
            fragment = new AlbumMainTeacherFragment();
            break;
        case MENU_POSITION_EVENTS:
            fragment = new EventsFragment();
            break;
        case MENU_POSITION_MEALS:
            fragment = new MealsFragment();
            break;
        case MENU_POSITION_CALENDAR:
            fragment = new MonthCalendarFragment();
            break;
        case MENU_POSITION_DAILY_REPORT:
            fragment = new DailyReportFragment();
            break;
        case MENU_POSITION_SETTINGS:
            fragment = new SettingsFragment();
            break;
        case MENU_POSITION_NEW_PICTURE:
            intent = new Intent(MainActivity.this, NewPictureActivity.class);
            startActivity(intent);
            break;
        case MENU_POSITION_NEW_JOURNAL_POST:
            intent = new Intent(MainActivity.this, NewJournalPostActivity.class);
            startActivity(intent);
            break;
        case MENU_POSITION_SUPPORT:
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.support_url)));
            startActivity(browserIntent);
            return;
        default:
            break;
        }
        if (fragment != null) {
            if (addToBackStack) {
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment, fragmentTags.get(pageIndex))
                        .addToBackStack(fragmentTags.get(pageIndex))
                        .commit();
            } else {
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment, fragmentTags.get(pageIndex))
                        .commit();
            }
        }
        if (pageIndex < OFFSET_BOTTOM_MENU_INDEX) { //Lower menu doesn't need highlight as it opens other activities
            bottomMenu.clearChoices();
            menu.setItemChecked(pageIndex, true);
        }

    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof TeacherBusiness && data != null && data instanceof Teacher) {
            updateTeacher();
        }
    }

    public void updateTeacher() {
        Teacher teacher = TeacherBusiness.getInstance().getCurrentTeacher();
        if (teacher != null) {
            if (findViewById(R.id.titleBtn) != null) {
                ButtonCustom navTitle = (ButtonCustom) findViewById(R.id.titleBtn);
                navTitle.setText(teacher.getName());
            }
            teacherName.setText(teacher.getName());
            if (!isFinishing() && !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && isDestroyed())) {
                Glide.with(this).load(teacher.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(teacherPicture);
            }
            mDrawerLayout.closeDrawer(navigationMenu);
            if (getSupportFragmentManager().getBackStackEntryCount() - 1 >= 0) {
                String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
                ((MainActivityFragment) getSupportFragmentManager().findFragmentByTag(tag)).refreshData();
            }
        }
    }

    @Override
    public void onPersonSelected() {
        Teacher teacher = TeacherBusiness.getInstance().getCurrentTeacher();
        if (teacher != null) {
            SessionTask task = new SessionTask();
            pendingTasks.add(task);
            task.execute();
            updateTeacher();
        }
    }

    @Override
    public void refreshData() {
        if (Webservice.isAuthorizationTokenInitialized() && TeacherBusiness.getInstance().isRequirePinCode()
                && getSupportFragmentManager().findFragmentByTag(TEACHER_SELECTION_DIALOG_TAG) == null) {
            SessionTask task = new SessionTask();
            pendingTasks.add(task);
            task.execute();
        }
    }

    private class SessionTask extends SessionBusiness.GetSessionTask {

        @Override
        protected void onPostExecute(Session item) {
            if (!checkNetworkException(item, exception)) {
                super.onPostExecute(item);
                if (item != null) {
                    TeacherTask teacherInfo = new TeacherTask(item.getSelfLink());
                    pendingTasks.add(teacherInfo);
                    teacherInfo.execute();
                }
            }
            pendingTasks.remove(this);
        }
    }


    public class TeacherTask extends TeacherBusiness.LoadCurrentTeacherDetails {

        public TeacherTask(String link) {
            super(link);
        }

        @Override
        protected Teacher doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                throw new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
            }
        }

        @Override
        protected void onPostExecute(Teacher teacher) {
            if (!checkNetworkException(teacher, exception)) {
                super.onPostExecute(teacher);
                // This refresh the side menu
                TeacherBusiness.getInstance().setCurrentTeacher(teacher);
                topMenuCounterIds[1] = teacher.getUnreadMessages();
                teacherName.setText(teacher.getName());
                Glide.with(MainActivity.this).load(teacher.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(teacherPicture);
                initKindergarten();
                refreshMenuStates();
            }
            pendingTasks.remove(this);
        }
    }

    @Override
    public int getLockedMenuMessageRes() {
        return R.string.locked_edu_message;
    }

    @Override
    public void refreshMenuStates() {
        Log.d(this.getClass().getName(), "refreshMenuState");
        topMenuBoolean[MENU_POSITION_MESSAGES] = MenuItemsStates.MESSAGES_STATE.getState();
        topMenuBoolean[MENU_POSITION_EVENTS] = MenuItemsStates.EVENTS_STATE.getState();
        topMenuBoolean[MENU_POSITION_MEALS] = MenuItemsStates.MEALS_STATE.getState();
        topMenuBoolean[MENU_POSITION_CALENDAR] = MenuItemsStates.CALENDAR_STATE.getState();
        topMenuBoolean[MENU_POSITION_DAILY_REPORT] = MenuItemsStates.DAILY_REPORT_STATE.getState();

        bottomMenuBoolean[MENU_POSITION_NEW_PICTURE - OFFSET_BOTTOM_MENU_INDEX] = MenuItemsStates.CAMERA_STATE.getState();
        topMenuBoolean[MENU_POSITION_ALBUM] = MenuItemsStates.CAMERA_STATE.getState();

        bottomMenuBoolean[MENU_POSITION_NEW_JOURNAL_POST - OFFSET_BOTTOM_MENU_INDEX] = MenuItemsStates.JOURNAL_STATE.getState();

        initMenu();
        initBottomMenu();
    }
}
