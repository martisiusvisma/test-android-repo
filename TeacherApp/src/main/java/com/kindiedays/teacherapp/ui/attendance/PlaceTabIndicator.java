package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 07/05/2015.
 */
public class PlaceTabIndicator extends LinearLayout {

    private TextView tvCurrent;
    private TextView tvPlaceName;

    public PlaceTabIndicator(Context context) {
        super(context);
        initViews();
    }

    public PlaceTabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public PlaceTabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_place, this);
        tvCurrent = (TextView) v.findViewById(R.id.current);
        tvPlaceName = (TextView) v.findViewById(R.id.placeName);
    }

    public void setCurrent(String current){
        tvCurrent.setText(current);
    }

    public void setPlaceName(String placeName) {
        tvPlaceName.setText(placeName);
    }
}
