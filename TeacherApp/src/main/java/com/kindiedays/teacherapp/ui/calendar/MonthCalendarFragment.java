package com.kindiedays.teacherapp.ui.calendar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.ui.CalendarTitleFormatter;
import com.kindiedays.common.ui.MainActivity;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.CalendarBusiness;
import com.kindiedays.teacherapp.pojo.CalendarEvents;
import com.kindiedays.teacherapp.pojo.DayEvents;
import com.kindiedays.teacherapp.ui.events.EditEventActivity;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.kindiedays.common.ui.MainActivity.MenuItemsStates.EVENTS_STATE;

/**
 * Created by pleonard on 06/07/2015.
 */
public class MonthCalendarFragment extends MainActivityFragment {

    private MaterialCalendarView calendarView;
    private TextViewCustom title;
    Set<CalendarDay> daysWithEvents = new HashSet<>();
    LinearLayout calendarDayRoot;
    LocalDate selectedDate;

    OnDateSelectedListener onDateChangedListener = new OnDateSelectedListener() {
        @Override
        public void onDateSelected(MaterialCalendarView widget, CalendarDay calendarDay, boolean selected) {
            selectedDate = new LocalDate(calendarDay.getDate());
            LoadCalendarForDay task = new LoadCalendarForDay();
            task.execute();
            pendingTasks.add(task);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_calendar_month, null);
        calendarView = (MaterialCalendarView) v.findViewById(R.id.calendarView);
        calendarView.setOnDateChangedListener(onDateChangedListener);
        calendarDayRoot = (LinearLayout) v.findViewById(R.id.calendarDayRoot);
        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        calendarView.setTitleFormatter(new CalendarTitleFormatter());
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Calendar));
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_events, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_createNewEvent) {
            if (EVENTS_STATE.getState()){
                Intent intent = new Intent(getActivity(), EditEventActivity.class);
                startActivity(intent);
            } else {
                ((MainActivity)getActivity()).lockedMenuItemDialogShow(getContext());
            }
        }
        return true;
    }

    private void populateChildrenWithPresenceData(Set<String> children, TextView content) {
        //Absences
        String[] childArray = new String[children.size()];
        children.toArray(childArray);
        if (childArray == null || childArray.length == 0) {
            content.setText("-");
        } else {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < childArray.length; i++) {
                sb.append(childArray[i]);
                if (i < childArray.length - 1) {
                    sb.append(", ");
                }
            }
            content.setText(sb.toString());
        }
    }

    public void setEvents(List<Event> events) {
        if (events != null) {
            for (Event event : events) {
                if (event.getInterval() != null) {
                    handleAllOccurences(event);
                } else {
                    CalendarDay day = CalendarDay.from(event.getDate().toDate());
                    daysWithEvents.add(day);
                }
            }
        }
    }

    private void handleAllOccurences(Event event) {
        List<LocalDateTime> allOccurences = EventsBusiness.getInstance().getAllOccurences(event);
        for (LocalDateTime occurence : allOccurences) {
            LocalDate date = occurence.toLocalDate();
            CalendarDay day = CalendarDay.from(date.toDate());
            daysWithEvents.add(day);
        }
    }

    @Override
    public void refreshData() {
        LoadEvents task = new LoadEvents();
        pendingTasks.add(task);
        task.execute();
    }

    class LoadCalendarForDay extends CalendarBusiness.GetCalendarEventsForDayAsyncTask {

        public LoadCalendarForDay() {
            super(selectedDate);
        }

        @Override
        protected CalendarEvents doInBackground(Void... params) {
            //We might have that day in memory already, no need to fetch it in that case
            if (CalendarBusiness.getInstance().getDayEventsMap().get(date) == null) {
                return super.doInBackground(params);
            } else {
                return null;
            }

        }

        @Override
        protected void onPostExecute(CalendarEvents events) {
            if (!checkNetworkException(events, exception)) {
                super.onPostExecute(events);
                if (getResources().getBoolean(com.kindiedays.common.R.bool.isTablet)) {
                    displayEventsAtBottom(date);
                } else {
                    displayEventsInPopup(date);
                }
            }
            pendingTasks.remove(this);
        }
    }

    @SuppressWarnings({"squid:MethodCyclomaticComplexity", "squid:S134"})
    private void displayEvents(LocalDate date, View root) {
        TextView day = (TextView) root.findViewById(R.id.day);
        TextView menu = (TextView) root.findViewById(R.id.menu);
        TextView absencesLabel = (TextView) root.findViewById(R.id.absencesLabel);
        TextView absences = (TextView) root.findViewById(R.id.absences);
        TextView presences = (TextView) root.findViewById(R.id.presences);
        TextView presencesLabel = (TextView) root.findViewById(R.id.presencesLabel);
        TextView noEvents = (TextView) root.findViewById(R.id.noEvents);
        LinearLayout eventContainer = (LinearLayout) root.findViewById(R.id.eventContainer);

        DayEvents dayEvents = CalendarBusiness.getInstance().getDayEventsMap().get(date);
        if (day != null) { // Null for tablets
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date.toDate());
            day.setText(CalendarTitleFormatter.getDayOfWeekString(getContext(), calendar)
                    .concat(date.toString(" dd/MM", Locale.getDefault())));
        }
        if (dayEvents == null) {
            menu.setText("-");
            absences.setText("-");
            presences.setText("-");
            noEvents.setVisibility(View.VISIBLE);
        } else {
            //Menu
            Menu menuLocal = dayEvents.getMenu();
            if (menuLocal == null || menuLocal.getMeals().isEmpty()) {
                menu.setText("-");
            } else {
                menu.setVisibility(View.VISIBLE);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < menuLocal.getMeals().size(); i++) {
                    sb.append(menuLocal.getMeals().get(i).getText());
                    if (i < menuLocal.getMeals().size() - 1) {
                        sb.append("\n");
                    }
                }
                menu.setText(sb.toString());
            }


            if (dayEvents.getPresents() != null && !dayEvents.getPresents().isEmpty()) {
                absencesLabel.setVisibility(View.GONE);
                presencesLabel.setVisibility(View.VISIBLE);
                populateChildrenWithPresenceData(dayEvents.getPresents(), presences);
            } else {
                absencesLabel.setVisibility(View.VISIBLE);
                presencesLabel.setVisibility(View.GONE);
                populateChildrenWithPresenceData(dayEvents.getAbsents(), absences);
            }

            //Events
            eventContainer.removeAllViews();
            if (dayEvents.getEvents() == null || dayEvents.getEvents().isEmpty()) {
                noEvents.setVisibility(View.VISIBLE);
                eventContainer.setVisibility(View.GONE);
            } else {
                noEvents.setVisibility(View.GONE);
                eventContainer.setVisibility(View.VISIBLE);
                eventContainer.removeAllViews();
                for (Event event : dayEvents.getEvents()) {
                    View eventLayout = getActivity().getLayoutInflater().inflate(R.layout.item_day_calendar_event, null);
                    String timeString = "";
                    if (event.getStart() != null) {
                        timeString += event.getStart().toString("HH:mm");
                        if (event.getEnd() != null) {
                            timeString += " - " + event.getEnd().toString("HH:mm");
                        }
                        ((TextView) eventLayout.findViewById(R.id.eventHour)).setText(timeString);
                    }
                    ((TextView) eventLayout.findViewById(R.id.eventName)).setText(event.getName());
                    eventContainer.addView(eventLayout);
                }
            }
        }
    }

    private void displayEventsInPopup(LocalDate date) {
        DialogFragment dialog = new DayEventsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("date", date.toString());
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "DayCalendarDialog");
    }

    public static class DayEventsDialogFragment extends DialogFragment {


        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.dialog_calendar_day_details, null);
            MonthCalendarFragment fragment = ((MonthCalendarFragment) getParentFragment());
            ((MonthCalendarFragment) getParentFragment()).displayEvents(fragment.selectedDate, root);
            getDialog().setCancelable(true);
            getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
            return root;
        }

    }

    private void displayEventsAtBottom(LocalDate date) {
        displayEvents(date, calendarDayRoot);
    }

    class LoadEvents extends EventsBusiness.GetEventsTask {
        @Override
        protected void onPostExecute(List<Event> events) {
            if (!checkNetworkException(events, exception)) {
                super.onPostExecute(events);
                calendarView.addDecorator(new EventDecorator(Color.RED, daysWithEvents));
            }
            pendingTasks.remove(this);
        }

        @Override
        protected List<Event> doNetworkTask() throws Exception {
            List<Event> events = super.doNetworkTask();
            MonthCalendarFragment.this.setEvents(events);
            return events;
        }
    }
}
