package com.kindiedays.teacherapp.ui.album;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.ui.messages.CustomPhotoGalleryActivity;
import com.kindiedays.teacherapp.R;

import java.util.ArrayList;

/**
 * Created by pleonard on 18/08/2015.
 */
public class PictureSelectionActivity extends KindieDaysActivity {

    public static final int TEXT_SIZE = 16;
    private TextView selectedNumberTextView;

    public static final String SELECTED_PICTURES_HASHES = "selectedPicturesIds";
    private static final String PREVIOUS_SELECTED_PICTURES_HASHES = "previousSelectedPicturesIds";
    private static final String SELECTED_PICTURES_NUMBER = "selectedPicturesFromAlbumNumber";
    private ArrayList<Integer> selectedPictures;
    private ArrayList<String> selectedPicturesHashes = new ArrayList<>();
    private int selectedNumber;
    private CoordinatorLayout clContent;

    PictureSelectionFragment fragment;

    public static Intent getInstance(Context context, ArrayList<CharSequence> selectedPicturesHashes, int selectedNumber){
        Intent intent = new Intent(context, PictureSelectionActivity.class);
        intent.putExtra(CustomPhotoGalleryActivity.HIDE_SPINNER_KEY, true);
        intent.putExtra(SELECTED_PICTURES_NUMBER, selectedNumber);
        intent.putCharSequenceArrayListExtra(PREVIOUS_SELECTED_PICTURES_HASHES, selectedPicturesHashes);
        return intent;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_picture_selection);
        clContent = (CoordinatorLayout) findViewById(R.id.cl_content_album);
        selectedPictures = getIntent().getIntegerArrayListExtra(PREVIOUS_SELECTED_PICTURES_HASHES);
        for (CharSequence pictureHash: getIntent().getCharSequenceArrayListExtra(PREVIOUS_SELECTED_PICTURES_HASHES)){
            selectedPicturesHashes.add(pictureHash.toString());
        }
        selectedNumber = getIntent().getIntExtra(SELECTED_PICTURES_NUMBER, 0);
        selectedNumberTextView = new TextView(this);
        fragment = (PictureSelectionFragment) getSupportFragmentManager().findFragmentById(R.id.albumFragment);
        fragment.setClContent(clContent);
        fragment.setSelectedPicturesHashes(selectedPicturesHashes);
        updateSelectedNumberTextView(selectedNumber);
    }

    public int getSelectedNumber() {
        return selectedNumber;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ok, menu);
        selectedNumberTextView.setTextColor(getResources().getColor(R.color.kindieblue));
        selectedNumberTextView.setBackgroundResource(R.drawable.drawable_circle);
        selectedNumberTextView.setTypeface(null, Typeface.BOLD);
        selectedNumberTextView.setTextSize(TEXT_SIZE);
        selectedNumberTextView.setGravity(Gravity.CENTER);
        menu.add(0, 0, 1, R.string.Select).setActionView(selectedNumberTextView).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    public void updateSelectedNumberTextView(int number){
        selectedNumberTextView.setText(String.valueOf(number));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_ok:
                setResult();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void setResult() {
        Intent intent = new Intent();
        intent.putExtra(SELECTED_PICTURES_HASHES,fragment.getSelectedPicturesIds());
        setResult(Activity.RESULT_OK, intent);
    }
}
