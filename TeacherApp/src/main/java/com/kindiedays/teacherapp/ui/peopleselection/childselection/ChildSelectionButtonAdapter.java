package com.kindiedays.teacherapp.ui.peopleselection.childselection;

import android.content.Context;
import android.view.ViewGroup;

import com.kindiedays.teacherapp.ui.peopleselection.ChildButtonAdapter;

/**
 * Created by pleonard on 07/10/2015.
 */
public class ChildSelectionButtonAdapter  extends ChildButtonAdapter {

    public ChildSelectionButtonAdapter(Context context) {
        super(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PersonStatusSelectionButton button = new PersonStatusSelectionButton(mContext);
        return new ItemViewHolder(button);
    }
}
