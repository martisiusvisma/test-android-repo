package com.kindiedays.teacherapp.ui.dailyreport.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.kindiedays.common.pojo.ActivityRes;
import com.kindiedays.teacherapp.R;

import java.util.List;

public class ActivityAdapter extends ExpandableRecyclerAdapter<ActivityViewHolder, DescriptionViewHolder> {

    private LayoutInflater mInflator;

    private RecyclerView owningRecyclerView;

    int lastPos = -1;

    public ActivityAdapter(Context context, RecyclerView owningRecyclerView, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        this.mInflator = LayoutInflater.from(context);
        this.owningRecyclerView = owningRecyclerView;

    }

    @Override
    public ActivityViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View recipeView = mInflator.inflate(R.layout.activity_title, parentViewGroup, false);
        return new ActivityViewHolder(recipeView);
    }

    @Override
    public DescriptionViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View ingredientView = mInflator.inflate(R.layout.activity_desc, childViewGroup, false);
        return new DescriptionViewHolder(ingredientView, new MyCustomEditTextListener());
    }

    @Override
    public void onBindParentViewHolder(ActivityViewHolder recipeViewHolder, int position, ParentListItem parentListItem) {
        RecyclerViewItem activityItem = (RecyclerViewItem) parentListItem;
        recipeViewHolder.bind(activityItem);
    }

    @Override
    public void onBindChildViewHolder(DescriptionViewHolder descriptionViewHolder, int position, Object childListItem) {
        ActivityRes description = (ActivityRes) childListItem;
        descriptionViewHolder.myCustomEditTextListener.updateCategory(description);
        descriptionViewHolder.bind(description);
    }

    @SuppressWarnings("unchecked")
    public class MyCustomEditTextListener implements TextWatcher {
        private ActivityRes activityRes;

        public void updateCategory(ActivityRes activityRes) {
            this.activityRes = activityRes;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void afterTextChanged(Editable editable) {
            //Testing if change because this is called as soon as the text box gets focus
            if (!activityRes.getText().equals(editable.toString())) {
                activityRes.setValueUpdated(true);
                activityRes.setText(editable.toString());
                Log.i(ActivityAdapter.class.getName(), "Text updated to: " + editable.toString());
            }
        }
    }

    @Override
    public void onParentListItemExpanded(int position) {
        List<? extends ParentListItem> parentItemList = this.getParentItemList();
        collapseAllParents();
        int finalPos = position;
        if (lastPos != -1 && lastPos < position) {
            finalPos = position - parentItemList.get(lastPos).getChildItemList().size();
        }
        expandParent(finalPos);
        if (finalPos > 0) {
            finalPos--;
        }
        owningRecyclerView.smoothScrollToPosition(finalPos);
        lastPos = position;
    }

}
