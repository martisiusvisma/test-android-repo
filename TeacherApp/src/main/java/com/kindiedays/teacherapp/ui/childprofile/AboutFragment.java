package com.kindiedays.teacherapp.ui.childprofile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.ChildProfile;
import com.kindiedays.teacherapp.R;

import java.util.List;

/**
 * Created by pleonard on 19/05/2015.
 */
public class AboutFragment extends Fragment {

    private ChildProfile childProfile;
    private List<ChildCarer> carers;
    private LinearLayout profiletable;
    private LinearLayout carerstable;

    private static final String SEPARATOR = ": ";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about, null);
        profiletable = (LinearLayout) v.findViewById(R.id.profiletable);
        carerstable = (LinearLayout) v.findViewById(R.id.carerstable);
        displayChildProfile();
        displayCarers();
        return v;
    }

    private void addRowToTable(String label, String value, LinearLayout table){
        addRowToTable(label, value, table, ChildProfileTableRow.NORMAL_MODE);
    }

    private void addRowToTable(String label, String value, LinearLayout table, int mode){
        if(value != null && value.length() > 0){
            ChildProfileTableRow tr = new ChildProfileTableRow(getActivity(), mode);
            tr.setValues(label, value);
            table.addView(tr, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    private void addRowToTable(String label, boolean value, LinearLayout table){
        ChildProfileTableRow tr = new ChildProfileTableRow(getActivity(), ChildProfileTableRow.BOOLEAN_MODE);
        tr.setValues(label, value);
        table.addView(tr, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

    }

    private void displayChildProfile(){
        //Make sure the fragment is initialized
        if(childProfile != null && profiletable != null) {
            if (childProfile.getDateOfBirth() != null){
                addRowToTable(getString(R.string.Date_of_Birth) + SEPARATOR, childProfile.getDateOfBirth().toString("dd/MM/yyyy"), profiletable);
            }
            addRowToTable(getString(R.string.Social_Security_Number) + SEPARATOR, childProfile.getSocialSecurityNumber(), profiletable);
            addRowToTable(getString(R.string.Native_Language) + SEPARATOR, childProfile.getNativeLanguage(), profiletable);
            addRowToTable(getString(R.string.Street_Address) + SEPARATOR, childProfile.getStreetAddress(), profiletable);
            addRowToTable(getString(R.string.ZipCode) + SEPARATOR, childProfile.getZipCode(), profiletable);
            addRowToTable(getString(R.string.City) + SEPARATOR, childProfile.getCity(), profiletable);
            addRowToTable(getString(R.string.Allergies) + SEPARATOR, childProfile.getAllergies(), profiletable);
            addRowToTable(getString(R.string.Diet) + SEPARATOR, childProfile.getDiet(), profiletable);
        }
    }

    private void displayCarers(){
        //Make sure the fragment is initialized
        if(carers != null && carerstable != null) {
            for (ChildCarer carer : carers){
                addRowToTable(getString(R.string.Carer) + SEPARATOR, carer.getName(), carerstable);
                addRowToTable(getString(R.string.Home_Phone) + SEPARATOR, carer.getHomePhoneNumber(), carerstable, ChildProfileTableRow.PHONE_MODE);
                addRowToTable(getString(R.string.Work_Phone) + SEPARATOR, carer.getWorkPhoneNumber(), carerstable, ChildProfileTableRow.PHONE_MODE);
                addRowToTable(getString(R.string.Mobile_Phone) + SEPARATOR, carer.getMobilePhoneNumber(), carerstable, ChildProfileTableRow.PHONE_MODE);
                addRowToTable(getString(R.string.Email) + SEPARATOR, carer.getEmail(), carerstable);
                addRowToTable(getString(R.string.Native_Language) + SEPARATOR, carer.getNativeLanguage(), carerstable);
                addRowToTable(getString(R.string.Occupation) + SEPARATOR, carer.getOccupation(), carerstable);
                addRowToTable(getString(R.string.Place_Of_Work) + SEPARATOR, carer.getPlaceOfWork(), carerstable);
                addRowToTable(getString(R.string.Allowed_To_Collect) + SEPARATOR, carer.isPickupPermission(), carerstable);
                addRowToTable(getString(R.string.Photo_consent) + SEPARATOR, childProfile.isPhotoConsent(), carerstable);
                addRowToTable(" ", " ", carerstable);
            }
        }
    }

    public void setChild(ChildProfile childProfile){
        this.childProfile = childProfile;
        displayChildProfile();
    }

    public void setCarers(List<ChildCarer> carers){
        this.carers = carers;
        displayCarers();
    }

}
