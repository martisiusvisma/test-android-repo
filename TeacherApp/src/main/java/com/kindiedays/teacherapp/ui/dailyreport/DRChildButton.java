package com.kindiedays.teacherapp.ui.dailyreport;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.utils.GlideUtils;
import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 21/04/2015.
 */
public class DRChildButton extends LinearLayout {

    private Child child;

    private ImageView kidPicture;
    private ImageView selectionImage;
    private TextView name;
    Animation signInOutAnimation;
    private boolean pendingRefresh = false;
    private static final String TAG = DRChildButton.class.getName();

    Animation.AnimationListener animationListener = new Animation.AnimationListener(){

        @Override
        public void onAnimationStart(Animation animation) {
            //ignore
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if(pendingRefresh){
                Log.i(TAG, "Starting pending animation");
                pendingRefresh = false;
                refreshChild();
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            //ignore
        }
    };

    public DRChildButton(Context context) {
        super(context);
        initViews();
    }

    public DRChildButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public DRChildButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.button_child_basic, this);
        kidPicture = (ImageView) v.findViewById(R.id.kidPicture);
        selectionImage = (ImageView) v.findViewById(R.id.selectionmarker);
        name = (TextView) v.findViewById(R.id.kidName);
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
        refreshChild();
        name.setText((child.getNickname() != null ? child.getNickname() : child.getName()));
        Glide.with(getContext()).load(child.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(kidPicture);
    }

    public void changeSelectionState(Boolean state){
        this.child.setSelected(state);
        refreshChild();
    }


    public void refreshChild(){
        if (signInOutAnimation != null && signInOutAnimation.hasStarted() && !signInOutAnimation.hasEnded()){
            Log.i(TAG, "Setting refresh pending");
            pendingRefresh = true;
        } else {
            if (child.isSelected()) {
                kidPicture.setAlpha(0xFF);
                selectionImage.setVisibility(View.VISIBLE);
                kidPicture.setColorFilter(getContext().getResources().getColor(R.color.selectedfilter));
            } else {
                kidPicture.setColorFilter(null);
                selectionImage.setVisibility(View.GONE);
            }
        }
    }
}
