package com.kindiedays.teacherapp.ui.childprofile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.DailyReportBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.GeneralDailyRes;
import com.kindiedays.common.pojo.Generic;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.common.ui.dailyreport.DRActivityActivity;
import com.kindiedays.common.ui.dailyreport.DRBathActivity;
import com.kindiedays.common.ui.dailyreport.DRMealActivity;
import com.kindiedays.common.ui.dailyreport.DRMedicationActivity;
import com.kindiedays.common.ui.dailyreport.DRMoodActivity;
import com.kindiedays.common.ui.dailyreport.DRNapActivity;
import com.kindiedays.common.ui.dailyreport.DRNoteActivity;
import com.kindiedays.common.ui.dailyreport.ParallaxRecyclerAdapter;
import com.kindiedays.common.utils.comparator.DateComparator;
import com.kindiedays.teacherapp.R;

import org.joda.time.DateTime;

import java.net.UnknownHostException;
import java.util.Collections;

/**
 * Created by Giuseppe Franco - Starcut on 04/05/16.
 */
public class DailyCarerFragment extends MainActivityFragment {
    Child currentChild;
    private RecyclerView mRecyclerView;
    private Activity activity;
    private TextViewCustom dateTv;

    public static final String MOOD = "MOOD";
    public static final String NOTE = "NOTE";
    public static final String MED = "MED";
    public static final String MEAL = "MEAL";
    public static final String NAP = "NAP";
    public static final String ACTIVITY_TAG = "ACTIVITY";
    public static final String BATH = "BATH";

    private GeneralDailyRes dailyItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_daily_report_client, null);
        currentChild = ((ChildProfileActivity) getActivity()).getChild();
        activity = getActivity();
        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        dateTv = (TextViewCustom) v.findViewById(R.id.textView);
        refreshDailyReportData(new DateTime());
        setHasOptionsMenu(true);
        return v;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(com.kindiedays.common.R.menu.menu_daily_report, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        TextViewCustom tvTitle = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.Profile));
    }

    @Override
    public void refreshData() {
        // this is called at every refresh
    }

    @SuppressWarnings({"squid:S1188", "squid:S1151", "squid:S3398"})
    private void createAdapter(RecyclerView recyclerView, GalleryPicture headerImg, GeneralDailyRes daily) {

        String photoUrl = "";
        if (headerImg != null) {
            photoUrl = headerImg.getUrl();
        }

        final ParallaxRecyclerAdapter<Generic> genericAdapter = new DailyReportRecycleViewAdapter(daily.getGeneric());
        genericAdapter.setOnClickEvent(new ParallaxRecyclerAdapter.OnClickEvent() {
            @Override
            public void onClick(View v, int position) {
                Intent intent;
                Bundle mBundle = new Bundle();
                Generic item = dailyItems.getGeneric().get(position);
                switch (item.getType()) {
                case MOOD:
                    intent = new Intent(activity, DRMoodActivity.class);
                    mBundle.putSerializable(MOOD, item.getMood());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    break;
                case NOTE:
                    if (item.getNote().getText().length() > 34) {
                        intent = new Intent(activity, DRNoteActivity.class);
                        mBundle.putSerializable(NOTE, item.getNote());
                        intent.putExtras(mBundle);
                        startActivity(intent);
                    }
                    break;
                case MED:
                    intent = new Intent(activity, DRMedicationActivity.class);
                    mBundle.putSerializable(MED, item.getMed());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    break;
                case MEAL:
                    intent = new Intent(activity, DRMealActivity.class);
                    mBundle.putSerializable(MEAL, item.getMeal());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    break;
                case NAP:
                    intent = new Intent(activity, DRNapActivity.class);
                    mBundle.putSerializable(NAP, item.getNap());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    break;
                case ACTIVITY_TAG:
                    intent = new Intent(activity, DRActivityActivity.class);
                    mBundle.putSerializable(ACTIVITY_TAG, item.getActivity());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    break;
                case BATH:
                    intent = new Intent(activity, DRBathActivity.class);
                    mBundle.putSerializable(BATH, item.getBath());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    break;
                default:
                    break;
                }
                activity.overridePendingTransition(R.anim.from_right_to_left, R.anim.exit_to_left);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        View header = getActivity().getLayoutInflater().inflate(R.layout.header, recyclerView, false);
        dateTv.setText(daily.getDate().toString("dd/MM/yyyy"));

        if (headerImg != null) {
            ImageView headerIV = (ImageView) header.findViewById(R.id.imageView);
            Glide.with(activity).load(photoUrl).centerCrop().into(headerIV);
            headerIV.setImageResource(R.color.transparent);
            genericAdapter.setParallaxHeader(header, recyclerView);
        }
        genericAdapter.setData(daily.getGeneric());
        recyclerView.setAdapter(genericAdapter);
    }

    private class GetChildDailyReportTask extends DailyReportBusiness.GetDailyForChildAsyncTask {

        GetChildDailyReportTask(Long id, DateTime date) {
            super(id, date);
        }

        protected GeneralDailyRes doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.data));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(GeneralDailyRes daily) {
            Log.d("TAG", "Daily report objects");
            if (!checkNetworkException(daily, exception)) {
                Collections.sort(daily.getGeneric(), new DateComparator());
                dailyItems = daily;
                createAdapter(mRecyclerView, null, daily);
            }
            pendingTasks.remove(this);
        }
    }

    public void refreshDailyReportData(DateTime day) {
        GetChildDailyReportTask dailyReport = new GetChildDailyReportTask(currentChild.getId(), day);
        dailyReport.execute();
        pendingTasks.add(dailyReport);
    }

}


