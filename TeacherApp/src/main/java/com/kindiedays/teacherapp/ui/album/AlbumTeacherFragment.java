package com.kindiedays.teacherapp.ui.album;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.teacherapp.business.PicturesBusiness;
import com.kindiedays.teacherapp.pojo.AlbumCategory;
import com.kindiedays.teacherapp.ui.picturegallery.PictureGalleryActivity;

import org.parceler.Parcels;

import java.net.UnknownHostException;

/**
 * Created by pleonard on 14/07/2015.
 */
public class AlbumTeacherFragment extends com.kindiedays.common.ui.album.AlbumFragment {
    public static final String TAG = AlbumTeacherFragment.class.getSimpleName();
    private static final String EXTRA_CATEGORY = "extra_category";

    private AlbumCategory category;

    public AlbumTeacherFragment() {
        onPictureClickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
                intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
                getActivity().startActivity(intent);
            }
        };
        onPictureLongClickListener = new PictureLongClickListener(this);
    }

    public static AlbumTeacherFragment newInstance(AlbumCategory model) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_CATEGORY, Parcels.wrap(model));
        AlbumTeacherFragment fragment = new AlbumTeacherFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.category = Parcels.unwrap(getArguments().getParcelable(EXTRA_CATEGORY));
    }

    class DeletePictureTeacherAsyncTask extends com.kindiedays.teacherapp.business.PicturesBusiness.DeletePictureAsyncTask {

        DeletePictureTeacherAsyncTask(GalleryPicture galleryPicture) {
            super(galleryPicture);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (!checkNetworkException(aVoid, exception)) {
                super.onPostExecute(aVoid);
                PicturesBusiness.getInstance().getCurrentPictureList().getPictures().remove(galleryPicture);
                thumbnailAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        title.setText(category.getName());
    }

    @Override
    public void refreshData() {
        if (category == null) {
            return;
        }

        NetworkAsyncTask<PictureList> task = getAsyncTask(category);
        pendingTasks.add(task);
        task.execute();
    }

    private NetworkAsyncTask<PictureList> getAsyncTask(AlbumCategory category) {
        if (category.getId() == PicturesBusiness.ALL_GROUPS) {
            return new GetPicturesAsyncTask();
        } else if (category.getType() == AlbumCategory.GROUP) {
            return new GetGroupPicturesTeacher(category.getId());
        } else {
            return new GetTeacherPictures(category.getId());
        }
    }

    private class GetPicturesAsyncTask extends PicturesBusiness.GetPicturesAsyncTask {

        protected PictureList doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.pictures));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            if (getActivity() != null && !checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                thumbnailAdapter.setPictures(pictureList.getPictures());
            }
            progressBar.setVisibility(View.GONE);
            pendingTasks.remove(this);
            isUpdating = false;
        }
    }


    private class GetGroupPicturesTeacher extends PicturesBusiness.GetGroupPicturesAsyncTask {
        GetGroupPicturesTeacher(long groupId) {
            super(groupId);
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            if (!checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                thumbnailAdapter.setPictures(pictureList.getPictures());
            }
            progressBar.setVisibility(View.GONE);
            pendingTasks.remove(this);
            isUpdating = false;
        }
    }


    private class GetTeacherPictures extends PicturesBusiness.GetTeacherPicturesAsyncTask {
        GetTeacherPictures(long teacherId) {
            super(teacherId);
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            if (!checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                thumbnailAdapter.setPictures(pictureList.getPictures());
            }
            progressBar.setVisibility(View.GONE);
            pendingTasks.remove(this);
            isUpdating = false;
        }
    }
}
