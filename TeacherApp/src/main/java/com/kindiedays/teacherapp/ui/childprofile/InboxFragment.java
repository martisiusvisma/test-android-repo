package com.kindiedays.teacherapp.ui.childprofile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.kindiedays.common.ui.InfiniteListViewScrollListener;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.NotificationBusiness;
import com.kindiedays.teacherapp.pojo.Notification;
import com.kindiedays.teacherapp.pojo.NotificationList;

import java.util.List;

/**
 * Created by pleonard on 20/05/2015.
 */
public class InboxFragment extends Fragment {

    private NotificationAdapter notificationAdapter;
    private ProgressBar progressBar;
    private List<Notification> listNotifications;

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Notification notification = notificationAdapter.getItem(position);
            if(!notification.isRead()){
                ((ChildProfileActivity) getActivity()).markNotificationAsRead(notification, position);
            }
        }
    };

    InfiniteListViewScrollListener.EndListReachedListener endGridReachedListener = new InfiniteListViewScrollListener.EndListReachedListener() {
        @Override
        public void topReached() {
            //Do nothing
        }

        @Override
        public void bottomReached() {
            if(!((ChildProfileActivity) getContext()).isUpdating()
                    && NotificationBusiness.getInstance().getNotifications() != null
                    && NotificationBusiness.getInstance().getNotifications().getLinkNext() != null) {
                ((ChildProfileActivity) getContext()).callMoreNotificationsFromMainActitivty(NotificationBusiness.getInstance().getNotifications().getLinkNext());
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inbox, null);
        ListView notificationList = (ListView) v.findViewById(R.id.notifications);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        notificationAdapter = new NotificationAdapter(getActivity());
        notificationList.setAdapter(notificationAdapter);
        notificationList.setOnItemClickListener(onItemClickListener);
        notificationList.setOnScrollListener(new InfiniteListViewScrollListener(endGridReachedListener));
        displayNotifications();
        return v;
    }

    public void setNotifications(List<Notification> listNotifications) {
        this.listNotifications = listNotifications;
        if (isAdded()) {
            displayNotifications();
        }
    }

    private void displayNotifications() {
        if (listNotifications != null && progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        notificationAdapter.setNotifications(listNotifications);
    }

    public void updateNotification(Notification notification, int position){
        notificationAdapter.updateNotification(notification, position);
    }

    public void updateNotifications(NotificationList newNotificationList){
        listNotifications.addAll(newNotificationList.getNotifications());
        notificationAdapter.notifyDataSetChanged();
    }

}
