package com.kindiedays.teacherapp.ui.dailyreport.meals;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.menus.GetMenus;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.DrinkRes;
import com.kindiedays.common.pojo.Meal;
import com.kindiedays.common.pojo.MealRes;
import com.kindiedays.common.pojo.Menu;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.common.utils.constants.DailyReportConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRMealsDialogFragment extends MainActivityDialogFragment {
    private EditText bodyEditText;
    private Spinner mealSpinner;
    private Spinner drinkSpinner;
    private TextViewCustom title;
    private SpinAdapterMeal adapterMeal;
    private SpinAdapterDrink adapterDrink;
    private MealRes mealItem;
    private RadioGroup rgDrink;
    private RadioGroup rgEat;
    private Button okButton;
    private View loaderContainer;

    private List<Meal> menuOfTheDay;

    private List<Meal> meal = new ArrayList<>();
    private List<DrinkRes> drinkList = new ArrayList<>();
    private LoadState loadState = LoadState.DEFAULT;

    private List<Child> childOrChildren = new ArrayList<>();

    private RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        static final String DRINK_UNIT_GLASS = "GLASS";

        //Changing the alpha shouldn't be done at this level but rather when building the multi state drawable for the radio button. Howver it doesn't seem to work with SVG...
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (rgDrink.getCheckedRadioButtonId() != -1) {
                ((TextView) drinkSpinner.getChildAt(0)).setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            if (rgEat.getCheckedRadioButtonId() != -1) {
                ((TextView) mealSpinner.getChildAt(0)).setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }
            enableDisableOckButton();
//            if (rgDrink.getCheckedRadioButtonId() != -1 || rgEat.getCheckedRadioButtonId() != -1) {
//                okButton.setEnabled(true);
//            } else {
//                okButton.setEnabled(false);
//            }

            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.rb_emptyGlass:
                    mealItem.setDrinkQuantity("0");
                    mealItem.setDrinkUnit(DRINK_UNIT_GLASS);
                    break;
                case R.id.rb_halfGlass:
                    mealItem.setDrinkQuantity("0.5");
                    mealItem.setDrinkUnit(DRINK_UNIT_GLASS);
                    break;
                case R.id.rb_fullGlass:
                    mealItem.setDrinkQuantity("1");
                    mealItem.setDrinkUnit(DRINK_UNIT_GLASS);
                    break;
                case R.id.rb_emptyPlate:
                    mealItem.setReaction(DailyReportConstants.NOT_ATE);
                    break;
                case R.id.rb_halfPlate:
                    mealItem.setReaction(DailyReportConstants.ATE_SOME);
                    break;
                case R.id.rb_fullPlate:
                    mealItem.setReaction(DailyReportConstants.ATE_WELL);
                    break;
                default:
                    break;
            }
        }
    };

    private void enableDisableOckButton() {
        if (rgDrink.getCheckedRadioButtonId() != -1 || rgEat.getCheckedRadioButtonId() != -1) {
            if (((mealItem.getMealIndex() != null && rgEat.getCheckedRadioButtonId() != -1) || mealItem.getMealIndex() == null)
                    && ((mealItem.getDrink() != null && rgDrink.getCheckedRadioButtonId() != -1) || mealItem.getDrink() == null)) {
                okButton.setEnabled(true);
            } else {
                okButton.setEnabled(false);
            }
        } else {
            okButton.setEnabled(false);
        }
    }

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            List<MealRes> outgoingItems = new ArrayList<>();
            outgoingItems.add(mealItem);
            for (int i = 0; i < outgoingItems.size(); i++) {
                if (outgoingItems.get(i).getChildren() != null) {
                    for (int n = 0; n < outgoingItems.get(i).getChildren().size(); n++) {
                        MealRes newMealItem = new MealRes();
                        newMealItem.setChild(outgoingItems.get(i).getChildren().get(n));
                        newMealItem.setDate(outgoingItems.get(i).getDate());
                        newMealItem.setCreated(outgoingItems.get(i).getCreated());
                        newMealItem.setReaction(outgoingItems.get(i).getReaction());
                        newMealItem.setDrink(outgoingItems.get(i).getDrink());
                        newMealItem.setMealIndex(outgoingItems.get(i).getMealIndex());
                        newMealItem.setDrinkQuantity(outgoingItems.get(i).getDrinkQuantity());
                        newMealItem.setDrinkUnit(outgoingItems.get(i).getDrinkUnit());
                        newMealItem.setNotes(outgoingItems.get(i).getNotes());
                        outgoingItems.add(newMealItem);
                    }
                    outgoingItems.remove(i);
                }
            }
            PostNewMealAsyncTask task = new PostNewMealAsyncTask(outgoingItems);
            pendingTasks.add(task);
            task.execute();
        }
    };

    @Override
    public void refreshData() {
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Meals));
    }

    @Override
    public void onDestroyView() {
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dr_meals, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        scrollingView = (ScrollView) v.findViewById(R.id.parentLayout);
        TextViewCustom childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);
        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder();
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }
        DateTime now = new DateTime();
        mealItem = new MealRes();
        if (childOrChildren != null && childOrChildren.size() == 1) {
            mealItem.setChild((int) childOrChildren.get(0).getId());
        } else {
            ArrayList<Integer> childrenId = new ArrayList<>();
            for (Child str : childOrChildren) {
                childrenId.add((int) str.getId());
            }
            mealItem.setChildren(childrenId);
        }

        mealItem.setDate(now);
        mealItem.setCreated(now);

        rgDrink = (RadioGroup) v.findViewById(R.id.rg_drink);
        RadioButton rbEmptyGlass = (RadioButton) v.findViewById(R.id.rb_emptyGlass);
        RadioButton rbHalfGlass = (RadioButton) v.findViewById(R.id.rb_halfGlass);
        RadioButton rbFullGlass = (RadioButton) v.findViewById(R.id.rb_fullGlass);
        rbEmptyGlass.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_meals_glass_empty_bg, R.raw.dr_meals_glass_empty_circle_tp), null, null, null);
        rbHalfGlass.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_meals_glass_half_bg, R.raw.dr_meals_glass_half_circle_tp), null, null, null);
        rbFullGlass.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_meals_glass_full_bg, R.raw.dr_meals_glass_full_circle_tp), null, null, null);
        rgDrink.setOnCheckedChangeListener(onCheckedChangeListener);

        rgEat = (RadioGroup) v.findViewById(R.id.rg_eat);
        RadioButton rbEmptyPlate = (RadioButton) v.findViewById(R.id.rb_emptyPlate);
        RadioButton rbHalfPlate = (RadioButton) v.findViewById(R.id.rb_halfPlate);
        RadioButton rbFullPlate = (RadioButton) v.findViewById(R.id.rb_fullPlate);
        rbEmptyPlate.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_meals_plate_empty_bg, R.raw.dr_meals_plate_empty_circle_tp), null, null, null);
        rbHalfPlate.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_meals_plate_half_bg, R.raw.dr_meals_plate_half_circle_tp), null, null, null);
        rbFullPlate.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_meals_plate_full_bg, R.raw.dr_meals_plate_full_circle_tp), null, null, null);
        rgEat.setOnCheckedChangeListener(onCheckedChangeListener);

        bodyEditText = (EditText) v.findViewById(R.id.bodyEdit);
        bodyEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                mealItem.setNotes(bodyEditText.getText().toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                scrollingView.fullScroll(View.FOCUS_DOWN);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ignore
            }

        });
        mealSpinner = (Spinner) v.findViewById(R.id.meals_spinner);

        meal.add(new Meal(0, getString(com.kindiedays.common.R.string.daily_select_meal)));
        Meal[] array = meal.toArray(new Meal[meal.size()]);
        adapterMeal = new SpinAdapterMeal(getActivity(), android.R.layout.simple_spinner_dropdown_item, array);
        mealSpinner.setAdapter(adapterMeal);
        mealSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                TextView childAt = (TextView) arg0.getChildAt(0);
                if (childAt != null) {
                    childAt.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                }
                int spinnerPositionMeal = (int) mealSpinner.getSelectedItemId();
                if (spinnerPositionMeal != 0) {
                    mealItem.setMealIndex(menuOfTheDay.get(spinnerPositionMeal - 1).getIndex());
                } else {
                    mealItem.setMealIndex(null);
                }
                enableDisableOckButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // no impl
            }
        });

        drinkSpinner = (Spinner) v.findViewById(R.id.drink_spinner);
        drinkList.add(new DrinkRes(0, getString(com.kindiedays.common.R.string.daily_select_drink)));
        DrinkRes[] drinkArray = drinkList.toArray(new DrinkRes[drinkList.size()]);
        adapterDrink = new SpinAdapterDrink(getActivity(), android.R.layout.simple_spinner_dropdown_item, drinkArray);
        drinkSpinner.setAdapter(adapterDrink);

        drinkSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                int selectedIdDrink = (int) (long) drinkSpinner.getSelectedItemId();
                if (selectedIdDrink != 0) {
                    mealItem.setDrink(drinkList.get(selectedIdDrink).getId());
                } else {
                    mealItem.setDrink(null);
                }
                enableDisableOckButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // no impl
            }

        });

        okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickedListener);
        loaderContainer = v.findViewById(R.id.loader_container);
        setContentView();
        return v;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
            title.setText(getString(com.kindiedays.common.R.string.Daily_Report));
        }
        super.onDismiss(dialog);
    }

    class PostNewMealAsyncTask extends DailyReportBusiness.PostNewMealTask {
        public PostNewMealAsyncTask(List<MealRes> items) {
            super(items);
        }

        @Override
        protected void onPostExecute(List<MealRes> items) {
            if (!checkNetworkException(items, exception)) {
                super.onPostExecute(items);
                Log.d(TAG, "Meal created");
            }
            pendingTasks.remove(this);
            dismiss();
        }
    }

    private class GetMealsAsyncTask extends NetworkAsyncTask<List<Menu>> {

        @Override
        protected List<Menu> doNetworkTask() throws Exception {
            try {
                return GetMenus.getMenus();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Menu> menus) {
            super.onPostExecute(menus);
            if (getActivity() == null || loadState == LoadState.PREVIOUS_FAILED) {
                return;
            }

            if (!checkNetworkException(menus, exception)) {
                for (int i = 0; i < menus.size(); i++) {
                    LocalDate mealDate = menus.get(i).getDate();
                    LocalDate today = new LocalDate();
                    if (mealDate.compareTo(today) == 0) {
                        menuOfTheDay = menus.get(i).getMeals();
                        for (int j = 0; j < menuOfTheDay.size(); j++) {
                            meal.add(new Meal(menuOfTheDay.get(j).getIndex(), menuOfTheDay.get(j).getText()));
                        }

                        Meal[] array = meal.toArray(new Meal[meal.size()]);
                        adapterMeal = new SpinAdapterMeal(getActivity(), android.R.layout.simple_spinner_dropdown_item, array);
                        mealSpinner.setAdapter(adapterMeal);
                        break;
                    }
                }
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), getString(R.string.failed_to_load_message, getString(R.string.menu_list)), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
                loadState = LoadState.PREVIOUS_FAILED;
                dismiss();
            }
            if (loadState == LoadState.PREVIOUS_LOADED) {
                loaderContainer.setVisibility(View.GONE);
            } else if (loadState != LoadState.PREVIOUS_FAILED) {
                loadState = LoadState.PREVIOUS_LOADED;
            }
            pendingTasks.remove(this);
        }
    }

    class DrinkListTask extends DailyReportBusiness.GetDrinkTask {
        @Override
        protected List<DrinkRes> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<DrinkRes> items) {
            super.onPostExecute(items);
            if (getActivity() == null || loadState == LoadState.PREVIOUS_FAILED) {
                return;
            }

            if (!checkNetworkException(items, exception)) {
                drinkList.clear();
                drinkList.add(new DrinkRes(0, getString(com.kindiedays.common.R.string.daily_select_drink)));
                for (int j = 0; j < items.size(); j++) {
                    drinkList.add(new DrinkRes(items.get(j).getId(), items.get(j).getName()));
                }
                DrinkRes[] array = drinkList.toArray(new DrinkRes[drinkList.size()]);
                adapterDrink = new SpinAdapterDrink(getActivity(), android.R.layout.simple_spinner_dropdown_item, array);
                drinkSpinner.setAdapter(adapterDrink);
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), getString(R.string.failed_to_load_message, getString(R.string.drinks_list)), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
                loadState = LoadState.PREVIOUS_FAILED;
                dismiss();
            }
            if (loadState == LoadState.PREVIOUS_LOADED) {
                loaderContainer.setVisibility(View.GONE);
            } else if (loadState != LoadState.PREVIOUS_FAILED) {
                loadState = LoadState.PREVIOUS_LOADED;
            }
            pendingTasks.remove(this);
        }
    }

    private void setContentView() {
        loadState = LoadState.DEFAULT;
        GetMealsAsyncTask mealsList = new GetMealsAsyncTask();
        pendingTasks.add(mealsList);
        mealsList.execute();
        DrinkListTask drinkTask = new DrinkListTask();
        pendingTasks.add(mealsList);
        drinkTask.execute();
    }

    enum LoadState {
        DEFAULT, PREVIOUS_LOADED, PREVIOUS_FAILED
    }
}