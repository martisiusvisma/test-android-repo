package com.kindiedays.teacherapp.ui.events;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;

import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.attendance.ContainersTabIndicator;

import java.util.List;

/**
 * Created by pleonard on 27/05/2015.
 */
public class EventsFragment extends MainActivityFragment {
    private static final String COMING_UP_TAG = "tag1";
    private static final String PAST_TAG = "tag2";

    private TabHost tabHost;
    private EventAdapter comingUpAdapter;
    private EventAdapter pastAdapter;
    private ProgressBar progressBar;
    List<Event> comingUpEvents;
    List<Event> pastEvents;

    AdapterView.OnItemClickListener onEventClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Event event = (Event) parent.getItemAtPosition(position);
            Intent intent = new Intent(getActivity(), EditEventActivity.class);
            intent.putExtra(EditEventActivity.EVENT_ID, event.getId());
            startActivity(intent);

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_events, null);
        tabHost = (TabHost) v.findViewById(R.id.tabHost);
        ListView lvComingUpEvents = (ListView) v.findViewById(R.id.comingup);
        ListView lvPastEvents = (ListView) v.findViewById(R.id.past);
        comingUpAdapter = new EventAdapter(getActivity(), false);
        lvComingUpEvents.setAdapter(comingUpAdapter);
        pastAdapter = new EventAdapter(getActivity(), true);
        lvPastEvents.setAdapter(pastAdapter);
        lvPastEvents.setOnItemClickListener(onEventClickListener);
        lvComingUpEvents.setOnItemClickListener(onEventClickListener);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        if (comingUpEvents != null) {
            comingUpAdapter.setEvents(comingUpEvents);
        }

        if (pastEvents != null) {
            pastAdapter.setEvents(pastEvents);
            progressBar.setVisibility(View.GONE);
        }

        initTabs();
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        TextViewCustom title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Events));
    }

    private void initTabs() {
        tabHost.setup();
        TabHost.TabSpec ts = tabHost.newTabSpec(COMING_UP_TAG);
        ts.setContent(R.id.comingup);
        ts.setIndicator(createTabView(getString(R.string.Coming_up)));
        tabHost.addTab(ts);
        ts = tabHost.newTabSpec(PAST_TAG);
        ts.setContent(R.id.past);
        ts.setIndicator(createTabView(getString(R.string.Past)));
        tabHost.addTab(ts);
    }

    private ContainersTabIndicator createTabView(String name) {
        ContainersTabIndicator indicator = new ContainersTabIndicator(getActivity());
        indicator.setText(name);
        return indicator;
    }

    @Override
    public void refreshData() {
        LoadEvents task = new LoadEvents();
        pendingTasks.add(task);
        task.execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_events, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_createNewEvent) {
            Intent intent = new Intent(getActivity(), EditEventActivity.class);
            startActivity(intent);

        }
        return true;
    }

    private class LoadEvents extends EventsBusiness.GetEventsTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            tabHost.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(List<Event> events) {
            if (!checkNetworkException(events, exception)) {
                super.onPostExecute(events);
                comingUpEvents = EventsBusiness.getInstance().getComingUpEvents(-1);
                comingUpAdapter.setEvents(comingUpEvents);
                pastEvents = EventsBusiness.getInstance().getPastEvents();
                pastAdapter.setEvents(pastEvents);
            }
            progressBar.setVisibility(View.GONE);
            tabHost.setVisibility(View.VISIBLE);
            pendingTasks.remove(this);
        }
    }
}
