package com.kindiedays.teacherapp.ui.peopleselection;

import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;

import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.ui.personselection.PersonSelectionActivity;
import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 11/06/2015.
 */
public abstract class PeopleGroupSelectionActivity extends PersonSelectionActivity {

    public static final String SELECTED_CARER_IDS = "selectedCarerIds";
    public static final String SELECTED_CHILDREN_IDS = "selectedChildrenIds";

    protected FragmentTabHost groupTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_people);
        groupTabHost = (FragmentTabHost) findViewById(R.id.groupTabHost);
        groupTabHost.setup(this, getSupportFragmentManager(), R.id.grouprealtabcontent);
        initTabs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ok, menu);
        return true;
    }

    protected abstract void initTabs();

    protected abstract void onGroupClick(Group group);

    protected SimpleTextCircleTabIndicator createTabView(String groupname) {
        SimpleTextCircleTabIndicator indicator = new SimpleTextCircleTabIndicator(this);
        indicator.setText(groupname);
        return indicator;
    }

}
