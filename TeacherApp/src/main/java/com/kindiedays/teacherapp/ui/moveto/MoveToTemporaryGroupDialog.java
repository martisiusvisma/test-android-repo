package com.kindiedays.teacherapp.ui.moveto;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.kindiedays.common.pojo.Group;
import com.kindiedays.teacherapp.business.GroupBusiness;

/**
 * Created by pleonard on 25/05/2015.
 */
public class MoveToTemporaryGroupDialog extends MoveToContainerDialog<Group> {

    public MoveToTemporaryGroupDialog(Context context) {
        super(context, GroupBusiness.getInstance());
    }

    public MoveToTemporaryGroupDialog(Context context, AttributeSet attrs) {
        super(context, attrs, GroupBusiness.getInstance());
    }

    public MoveToTemporaryGroupDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, GroupBusiness.getInstance());
    }

    protected void containerSpecificInitViews(View v){
        //Nothing group specific
    }


}
