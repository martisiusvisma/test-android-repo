package com.kindiedays.teacherapp.ui.messages;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.kindiedays.common.business.ConversationBusiness;
import com.kindiedays.common.business.ConversationPartyBusiness;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.Conversation;
import com.kindiedays.common.pojo.ConversationParty;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.CarerBusiness;
import com.kindiedays.teacherapp.ui.peopleselection.PeopleGroupSelectionActivity;
import com.kindiedays.teacherapp.ui.peopleselection.recipientselection.RecipientSelectionActivity;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pleonard on 29/05/2015.
 */
public class NewMessageActivity extends com.kindiedays.common.ui.messages.NewMessageActivity {

    public NewMessageActivity() {
        onSelectPeopleClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewMessageActivity.this, RecipientSelectionActivity.class);
                intent.putExtra(PeopleGroupSelectionActivity.SELECTED_TEACHER_IDS, selectedTeachersId);
                startActivityForResult(intent, RECIPIENT_SELECTION_TAG);
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED && requestCode == RECIPIENT_SELECTION_TAG) {
            finish();
        } else if (resultCode == RESULT_OK && requestCode == RECIPIENT_SELECTION_TAG) {
            selectedTeachersId = data.getLongArrayExtra(PeopleGroupSelectionActivity.SELECTED_TEACHER_IDS);
            initRecipients();
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConversationBusiness.getInstance().setSelectedCarersPerChild(new HashMap<Long, Set<Long>>());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
        }
        return super.onOptionsItemSelected(item);
    }

    protected void sendNewMessage(String subject, String message, List<File> attachments, boolean isDelete) {
        CreateTeacherConversationsAsyncTask task = new CreateTeacherConversationsAsyncTask(subject, message,
                ConversationBusiness.getInstance().buildChildrenPerSelectedCarerMap(), false,
                selectedTeachersId, attachments, isDelete);
        task.execute();
        getPendingTasks().add(task);
    }

    private void initRecipients() {
        CarerBusiness carerBusiness = CarerBusiness.getInstance();
        ConversationPartyBusiness conversationPartyBusiness = ConversationPartyBusiness.getInstance();
        StringBuilder sb = new StringBuilder();
        Map<Long, Set<Long>> childrenPerCarer = ConversationBusiness.getInstance().buildChildrenPerSelectedCarerMap();
        if (childrenPerCarer.size() > 0) {
            for (Long carerId : childrenPerCarer.keySet()) {
                ChildCarer carer = carerBusiness.getCarer(carerId);
                sb.append(carer.getName()).append("\n");
            }
        }
        if (selectedTeachersId != null) {
            for (long id : selectedTeachersId) {
                ConversationParty conversationParty = conversationPartyBusiness.getConversationParty(ConversationParty.TEACHER_TYPE, id);
                sb.append(conversationParty.getName()).append("\n");
            }
        }
        recipientsTextView.setText(sb.toString());
        if (!TextUtils.isEmpty(sb)) {
            isFieldsFilled();
        }
    }


    private class CreateTeacherConversationsAsyncTask extends ConversationBusiness.CreateTeacherConversationsTask {
        private ProgressDialog progressDialog;

        CreateTeacherConversationsAsyncTask(String subject, String message, Map<Long, Set<Long>> childrenPerCarer,
                                            boolean createAsKindergarten, long[] teacherIds, List<File> attachments,
                                            boolean isDelete) {
            super(subject, message, childrenPerCarer, createAsKindergarten, teacherIds, attachments, isDelete);
        }

        @Override
        protected void onPreExecute() {
            if (attachments.isEmpty()) {
                return;
            }

            initProgressDialog();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int currentProgress = values[0];
            progressDialog.setMessage(getString(R.string.dialog_progress_file_uploading, currentProgress,
                    attachments.size()));
            progressDialog.setProgress(currentProgress);
        }

        @Override
        protected void onPostExecute(Set<Conversation> conversations) {
            if (!checkNetworkException(conversations, exception)) {
                super.onPostExecute(conversations);
                Intent data = getIntent();
                data.putExtra(NewMessageActivity.NEW_CONVERSATIONS_CREATED, true);
                setResult(RESULT_OK, data);
                finish();
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            getPendingTasks().remove(this);
        }

        private void initProgressDialog() {
            progressDialog = new ProgressDialog(NewMessageActivity.this);
            progressDialog.setTitle(com.kindiedays.common.R.string.Upload_In_Progress);
            progressDialog.setMax(attachments.size());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(1);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(getString(R.string.dialog_progress_file_uploading, 1,
                    attachments.size()));
            progressDialog.show();
        }
    }

}
