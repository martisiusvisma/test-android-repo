package com.kindiedays.teacherapp.ui.messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.ui.MainActivity;

/**
 * Created by pleonard on 15/09/2015.
 */
public class ConversationsFragment extends com.kindiedays.common.ui.messages.ConversationsFragment {

    public static final int REQUEST_CODE = 8000;

    public ConversationsFragment() {
        onConversationClickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ConversationActivity.class);
                intent.putExtra(ConversationActivity.CONVERSATION_ID, id);
                getActivity().startActivity(intent);
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_createNewMessage) {
            Intent intent = new Intent(getActivity(), NewMessageActivity.class);
            startActivityForResult(intent, REQUEST_CODE);
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        conversationAdapter = new ConversationAdapter(getActivity());
        conversationAdapter.setChildren(ChildBusiness.getInstance().getChildren());
        super.setOnAsynkPostExecuteListener(onAsynkPostExecuteListener);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && data.getBooleanExtra(NewMessageActivity.NEW_CONVERSATIONS_CREATED, false)) {
            refreshData();
        }
    }

    private OnAsynkPostExecuteListener onAsynkPostExecuteListener = new OnAsynkPostExecuteListener() {
        @Override
        public void onPostExecute() {
            FragmentActivity activity = getActivity();
            if (activity instanceof  MainActivity){
                ((MainActivity) activity).onPersonSelected();
            }
        }
    };
}
