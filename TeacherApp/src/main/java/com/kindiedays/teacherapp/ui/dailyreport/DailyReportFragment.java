package com.kindiedays.teacherapp.ui.dailyreport;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.daily.ResourceFaceTab;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.DRSettings;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Kindergarten;
import com.kindiedays.common.ui.ChildTouchListener;
import com.kindiedays.common.ui.DRSectionedGridRecyclerViewAdapter;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.dailyreport.meals.DRMealsDialogFragment;
import com.kindiedays.teacherapp.util.DailyReportUtils;

import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import static com.kindiedays.teacherapp.ui.dailyreport.DRChildrenContainerAdapter.DailyReportInterface.MODE_MOVE;
import static com.kindiedays.teacherapp.ui.dailyreport.DRChildrenContainerAdapter.DailyReportInterface.MODE_NORMAL;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.ACTIVITIES_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.BATH_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.MEAL_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.MED_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.MOOD_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.NAP_TAG;
import static com.kindiedays.teacherapp.util.constants.DailyReportConstants.NOTE_TAG;

/**
 * Created by Giuseppe Franco on 27/05/2015.
 */
public class DailyReportFragment extends MainActivityFragment implements ChildTouchListener.OnChildButtonTouchListener, Observer,
        DRSectionedGridRecyclerViewAdapter.OnSectionViewClickListener{

    private RecyclerView recyclerView;
    private DRChildrenAdapter childrenAdapter;
    private TextViewCustom title;
    private long groupId;
    private View fragmentView;
    FragmentTabHost sResourceTabHost;

    private int mode = MODE_NORMAL;
    private String activeTab = "activities";
    public static final String GROUP_ID = "groupId";
    private static final String CATEGORY = "Type";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        fragmentView = inflater.inflate(R.layout.fragment_daily_report, container, false);
        if (getArguments() != null) {
            groupId = getArguments().getLong(GROUP_ID);
        } else {
            groupId = GroupBusiness.ALL_GROUP_ID;
        }

        sResourceTabHost = (FragmentTabHost) fragmentView.findViewById(android.R.id.tabhost);
        sResourceTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);

        recyclerView = (RecyclerView) fragmentView.findViewById(R.id.childrenList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        childrenAdapter = new DRChildrenAdapter(getActivity());
        recyclerView.addOnItemTouchListener(new ChildTouchListener(this));

        if (DailyReportBusiness.getInstance().getSettings() == null) {
            Kindergarten kindergarten = KindergartenBusiness.getInstance().getKindergarten();
            if (kindergarten == null) {
                return fragmentView;
            }
            Long idKindergarten = kindergarten.getId();
            LoadDRSettings categories = new LoadDRSettings(idKindergarten);
            pendingTasks.add(categories);
            categories.execute();
        } else {
            populateTabs(DailyReportBusiness.getInstance().getSettings());
        }

        DailyReportBusiness.getInstance();
        ChildBusiness.getInstance();
        return fragmentView;
    }

    public void onStart() {
        super.onStart();
        TeacherBusiness.getInstance().addObserver(this);
        ChildBusiness.getInstance().addObserver(this);
    }

    public void onStop() {
        super.onStop();
        TeacherBusiness.getInstance().deleteObserver(this);
        ChildBusiness.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        childrenAdapter.notifyDataSetChanged();
    }

    public static class DummySectionFragment extends Fragment {

    }

    private ResourceFaceTab createTabViewDrawable(Integer resourceName, Integer selectedResourceName) {
        ResourceFaceTab indicator = new ResourceFaceTab(getActivity());
        indicator.setResourceImage(resourceName, selectedResourceName);
        return indicator;
    }

    public void updateSections() {
        List<Group> groups;
        if (groupId == GroupBusiness.ALL_GROUP_ID) {
            groups = GroupBusiness.getInstance().getTeacherGroups(TeacherBusiness.getInstance().getCurrentTeacher(), false);
        } else {
            groups = Arrays.asList(new Group[]{GroupBusiness.getInstance().getGroup(groupId)});
        }

        List<DRSectionedGridRecyclerViewAdapter.Section> sections =
                new ArrayList<>();

        int currentPosition = 0;
        List<Child> children = new ArrayList<>();
        for (Group _group : groups) {
            if (_group.isDailyReportEnabled()) {
                sections.add(new DRSectionedGridRecyclerViewAdapter.Section(currentPosition, new String[]{_group.getName()}, _group));
                currentPosition += ChildBusiness.getInstance().getChildren(_group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups()).size();
                children.addAll(ChildBusiness.getInstance().getChildren(_group.getId(), TeacherBusiness.getInstance().getCurrentTeacher().getGroups()));
            }
        }

        enterNormalMode();
        DRSectionedGridRecyclerViewAdapter mSectionedAdapter = new
                DRSectionedGridRecyclerViewAdapter(getActivity(), R.layout.item_group_section,
                new int[]{R.id.groupName}, new int[]{R.id.selectMode}, recyclerView,
                childrenAdapter,this);
        mSectionedAdapter.setSelectionView(new int[] {R.string.Select});
        mSectionedAdapter.setSections(sections);
        recyclerView.setAdapter(mSectionedAdapter);
        recyclerView.addOnItemTouchListener(new ChildTouchListener(this));
        childrenAdapter.setChildren(children);
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(DailyReportUtils.getTitleResId(activeTab));
    }

    @Override
    public void refreshData() {
        RefreshChildren refreshChildren = new RefreshChildren();
        pendingTasks.add(refreshChildren);
        refreshChildren.execute();
    }

    private class RefreshChildren extends ChildBusiness.LoadChildrenTask {

        @Override
        protected List<Child> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = getString(R.string.failed_to_load_message, getString(R.string.child_list));
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Child> children) {
            if (!checkNetworkException(children, exception)) {
                super.onPostExecute(children);
                updateSections();
            }
            pendingTasks.remove(this);
        }
    }

    private void populateTabs(DRSettings drSettings) {
        for (int i = 0; i < drSettings.getCategoryResList().size(); i++) {
            Integer tabIcon = 0;
            Integer selectedTabIcon = 0;
            switch (drSettings.getCategoryResList().get(i).getName()) {
            case ACTIVITIES_TAG:
                tabIcon = R.raw.dr_activites_circle_bg;
                selectedTabIcon = R.raw.dr_activites_white;
                break;
            case BATH_TAG:
                tabIcon = R.raw.dr_wc_bg;
                selectedTabIcon = R.raw.dr_wc_white;
                break;
            case MEAL_TAG:
                tabIcon = R.raw.dr_meals_bg;
                selectedTabIcon = R.raw.dr_meals_white;
                break;
            case MED_TAG:
                tabIcon = R.raw.dr_medication_bg;
                selectedTabIcon = R.raw.dr_medication_white;
                break;
            case MOOD_TAG:
                tabIcon = R.raw.dr_mood_happy_bg;
                selectedTabIcon = R.raw.dr_mood_happy_white;
                break;
            case NAP_TAG:
                tabIcon = R.raw.dr_sleep_bg;
                selectedTabIcon = R.raw.dr_sleep_white;
                break;
            case NOTE_TAG:
                tabIcon = R.raw.dr_notes_bg;
                selectedTabIcon = R.raw.dr_notes_white;
                break;
            default:
                break;
            }
            TabHost.TabSpec ts = sResourceTabHost.newTabSpec(drSettings.getCategoryResList().get(i).getName()).setIndicator(createTabViewDrawable(tabIcon, selectedTabIcon));
            Bundle bundle = new Bundle();
            bundle.putString(CATEGORY, drSettings.getCategoryResList().get(i).getName());
            sResourceTabHost.addTab(ts, DummySectionFragment.class, bundle);
        }

        sResourceTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tab) {
                activeTab = tab;
                if (getActivity() != null) {
                    TextViewCustom title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
                    if (title != null) {
                        title.setText(DailyReportUtils.getTitleResId(tab));
                    }
                }
            }
        });

    }

    private class LoadDRSettings extends DailyReportBusiness.LoadDRSettingsTask {
        public LoadDRSettings(Long kindergardenId) {
            super(kindergardenId);
        }

        @Override
        protected void onPostExecute(DRSettings drSettings) {
            if (!checkNetworkException(drSettings, exception)) {
                super.onPostExecute(drSettings);
                populateTabs(drSettings);
            }
            pendingTasks.remove(this);
        }
    }

    @Override
    public void onChildButtonLongPress(View childView) {
        //ignore
    }

    @Override
    public void onChildButtonTapped(View v) {
        if (v != null && v.getTag() instanceof Child) {
            Set<Child> selectedChildren = ChildBusiness.getInstance().getSelectedChildren();
            DRChildButton childAttendanceButton = (DRChildButton) v;
            if (getMode() == MODE_NORMAL) {
                selectedChildren.add((Child) v.getTag());
                Bundle args = new Bundle();
                args.putSerializable("child", (Serializable) selectedChildren);
                displayDialog(activeTab, args);

            } else if (getMode() == MODE_MOVE) {
                ChildBusiness.getInstance().selectChild(childAttendanceButton.getChild().getId(), !childAttendanceButton.getChild().isSelected());
                if (ChildBusiness.getInstance().getSelectedChildren().size() == 0) {
                    enterNormalMode();
                    switchSelectionView();
                }
            }
        }
    }

    private void switchSelectionView() {
        Log.d(DailyReportFragment.class.getName(), "switchSelectionView from mode = " + getMode());
        DRSectionedGridRecyclerViewAdapter adapter =
                (DRSectionedGridRecyclerViewAdapter) recyclerView.getAdapter();
        if (getMode() == MODE_NORMAL) {
            adapter.setSelectionView(new int[] {R.string.Select});
        } else if (getMode() == MODE_MOVE) {
            adapter.setSelectionView(new int[] {R.string.Done});
        }
    }

    @Override
    public void onSelectionViewClick(View selectionView) {
        if (selectionView.getId() == R.id.selectMode) {
            if (getMode() == MODE_MOVE) {
                Set<Child> selectedChildren = ChildBusiness.getInstance().getSelectedChildren();
                if (!selectedChildren.isEmpty()) {
                    Bundle args = new Bundle();
                    args.putSerializable("child", (Serializable) selectedChildren);
                    displayDialog(activeTab, args);
                }
                ChildBusiness.getInstance().unselectAllChildren();
                enterNormalMode();
            } else
                enterMoveMode();

            switchSelectionView();
        }
    }

    @Override
    public void onTextViewClick(View textView) {
        if (textView.getId() == R.id.groupName) {
            long groupId = ((Group) textView.getTag()).getId();
            List<Long> groupIds = ChildBusiness.getInstance().getSelectedGroups();
            if (getMode() == MODE_NORMAL) {
                ChildBusiness.getInstance().selectAllChildren(groupId);
                enterMoveMode();
                switchSelectionView();
            } else if (getMode() == MODE_MOVE) {
                if (groupIds.contains(groupId)) {
                    ChildBusiness.getInstance().unselectAllChildern(groupId);
                    if (ChildBusiness.getInstance().getSelectedChildren().size() == 0) {
                        enterNormalMode();
                        switchSelectionView();
                    }
                } else {
                    ChildBusiness.getInstance().selectAllChildren(groupId);
                }
            }
        }
    }

    public void displayDialog(String category, Bundle args) {
        switch (category) {
            case NOTE_TAG:
                DRNotesDialogFragment note = new DRNotesDialogFragment();
                note.setArguments(args);
                note.show(getFragmentManager(), NOTE_TAG);
                break;
            case ACTIVITIES_TAG:
                DRActivitiesDialogFragment activities = new DRActivitiesDialogFragment();
                activities.setArguments(args);
                activities.show(getFragmentManager(), ACTIVITIES_TAG);
                break;
            case MED_TAG:
                DRMedicationDialogFragment medication = new DRMedicationDialogFragment();
                medication.setArguments(args);
                medication.show(getFragmentManager(), MED_TAG);
                break;
            case BATH_TAG:
                DRBathDialogFragment bath = new DRBathDialogFragment();
                bath.setArguments(args);
                bath.show(getFragmentManager(), BATH_TAG);
                break;
            case MEAL_TAG:
                DRMealsDialogFragment meals = new DRMealsDialogFragment();
                meals.setArguments(args);
                meals.show(getFragmentManager(), MEAL_TAG);
                break;
            case MOOD_TAG:
                DRMoodsDialogFragment mood = new DRMoodsDialogFragment();
                mood.setArguments(args);
                mood.show(getFragmentManager(), MOOD_TAG);
                break;
            case NAP_TAG:
                DRNapsDialogFragment naps = new DRNapsDialogFragment();
                naps.setArguments(args);
                naps.show(getFragmentManager(), NAP_TAG);
                break;
            default:
                DRActivitiesDialogFragment dialog = new DRActivitiesDialogFragment();
                dialog.setArguments(args);
                dialog.show(getFragmentManager(), ACTIVITIES_TAG);
        }
    }

    public void switchMode(int newMode) {
        if (this.mode == newMode) {
            return;
        }
        this.mode = newMode;
    }

    public int getMode() {
        return mode;
    }

    public void enterNormalMode() {
        switchMode(MODE_NORMAL);
    }

    public void enterMoveMode() {
        switchMode(MODE_MOVE);
    }
}
