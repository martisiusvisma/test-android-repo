package com.kindiedays.teacherapp.ui.album;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;

import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.PicturesBusiness;

class PictureLongClickListener implements AdapterView.OnItemLongClickListener {
    private final AlbumTeacherFragment context;

    PictureLongClickListener(AlbumTeacherFragment context) {
        this.context = context;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setCancelable(true)
                .setMessage(context.getString(R.string.Delete_Picture))
                .setTitle(context.getString(R.string.Delete_Picture))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AlbumTeacherFragment.DeletePictureTeacherAsyncTask task = context.new DeletePictureTeacherAsyncTask(PicturesBusiness
                                .getInstance().getCurrentPictureList().getPictures().get(position));
                        task.execute();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
        return true;
    }
}