package com.kindiedays.teacherapp.ui.dailyreport;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kindiedays.common.components.CheckedTextViewCustom;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.daily.ResourceSquaredTab;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.MoodRes;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.PictureUtils;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.common.utils.constants.DailyReportConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRMoodsDialogFragment extends MainActivityDialogFragment {
    private TextViewCustom title;
    private HorizontalScrollView counterTabHostlScrollView;
    private EditText bodyEt;
    private Button timePickerEt;
    private List<MoodRes> moodItems = new ArrayList<>();
    private FragmentTabHost moodTabHost;
    private RadioGroup rgMood;
    private RadioButton rbSad;
    private RadioButton rbOk;
    private RadioButton rbHappy;
    private View loaderContainer;

    private int moodCounter = 0;
    private int selectedTab = 0;

    private List<Child> childOrChildren = new ArrayList<>();

    RadioGroupListener radioGroubListener = new RadioGroupListener();

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            List<MoodRes> outgoingItems = new ArrayList<>();

            for (int i = 0; i < moodItems.size(); i++) {
                if (moodItems.get(i).getLocal()) {
                    outgoingItems.add(moodItems.get(i));
                }
            }
            if (!outgoingItems.isEmpty()) {
                for (int i = 0; i < outgoingItems.size(); i++) {
                    if (outgoingItems.get(i).getChildren() != null) {
                        for (int n = 0; n < outgoingItems.get(i).getChildren().size(); n++) {
                            MoodRes newMoodItem = new MoodRes();
                            newMoodItem.setChild(outgoingItems.get(i).getChildren().get(n));
                            newMoodItem.setDate(outgoingItems.get(i).getDate());
                            newMoodItem.setLocal(outgoingItems.get(i).getLocal());
                            newMoodItem.setTime(outgoingItems.get(i).getTime());
                            newMoodItem.setTemper(outgoingItems.get(i).getTemper());
                            newMoodItem.setNotes(outgoingItems.get(i).getNotes());
                            outgoingItems.add(newMoodItem);
                        }
                        outgoingItems.remove(i);
                    }
                }
                for (int i = outgoingItems.size() - 1; i >= 0; i--) {
                    if (TextUtils.isEmpty(outgoingItems.get(i).getTemper())) {
                        outgoingItems.remove(i);
                    }
                }
                SaveNewMoodTask task = new SaveNewMoodTask(outgoingItems);
                pendingTasks.add(task);
                task.execute();
            } else {
                dismiss();
            }

        }
    };

    @Override
    public void onDestroyView() {
        selectedTab = 0;
        moodCounter = 0;
        moodItems.clear();
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        dismiss();
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void refreshData() {
        // We should use this to get the existing data
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Moods));
        if (childOrChildren != null && childOrChildren.size() == 1) {
            DateTime now = new DateTime();
            LoadMoodTask task = new LoadMoodTask(((int) childOrChildren.get(0).getId()), now);
            pendingTasks.add(task);
            task.execute();
        } else {
            MoodRes newMoodItem = new MoodRes();
            ArrayList<Integer> childrenId = new ArrayList<>();
            for (Child str : childOrChildren) {
                childrenId.add((int) str.getId());
            }
            newMoodItem.setChildren(childrenId);
            DateTime now = new DateTime();
            newMoodItem.setDate(now);
            newMoodItem.setTime(now);
            newMoodItem.setLocal(true);
            moodItems.add(newMoodItem);
            moodCounter++;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dr_moods, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);

        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);

        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        scrollingView = (ScrollView) v.findViewById(R.id.parentLayout);

        counterTabHostlScrollView = (HorizontalScrollView) v.findViewById(R.id.counterTabHostlScrollView);
        TextViewCustom childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);
        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder();
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }

        CheckedTextViewCustom timePickerTitle = (CheckedTextViewCustom) v.findViewById(R.id.timePickerTitle);
        timePickerTitle.setText(getString(com.kindiedays.common.R.string.daily_time));

        timePickerEt = (Button) v.findViewById(R.id.timePickerEt);
        timePickerEt.setInputType(InputType.TYPE_NULL);
        timePickerEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int mHour;
                int mMinute;
                if (MotionEvent.ACTION_UP == event.getAction()) {

                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.timePickerStyle,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    timePickerEt.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                                    for (int i = 0; i < moodItems.size(); i++) {
                                        if (i == selectedTab) {
                                            DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
                                            moodItems.get(i).setDate(new DateTime());
                                            moodItems.get(i).setTime(formatter.parseDateTime(hourOfDay + ":" + minute));
                                        }
                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
                return false;
            }
        });

        Button addItemBtn = (Button) v.findViewById(R.id.addBtn);
        addItemBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
                    String newId = String.valueOf(moodCounter + 2);
                    indicator.setResourceName(newId);
                    TabHost.TabSpec ts = moodTabHost.newTabSpec("item" + newId).setIndicator(indicator);
                    Bundle bundle = new Bundle();
                    bundle.putString(DailyReportConstants.TYPE, newId);
                    moodTabHost.addTab(ts, MoodFragment.class, bundle);
                    moodCounter = moodCounter + 1;

                    counterTabHostlScrollView.postDelayed(new Runnable() {
                        public void run() {
                            counterTabHostlScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                            int currentTab = moodTabHost.getCurrentTab();
                            moodTabHost.setCurrentTab(currentTab + 1);
                            refresh();
                        }
                    }, 100L);

                    MoodRes newMoodItem = new MoodRes();
                    if (childOrChildren.size() == 1) {
                        newMoodItem.setChild((int) childOrChildren.get(0).getId());
                    } else {
                        ArrayList<Integer> childrenId = new ArrayList<>();
                        for (Child str : childOrChildren) {
                            childrenId.add((int) str.getId());
                        }
                        newMoodItem.setChildren(childrenId);
                    }
                    DateTime now = new DateTime();
                    newMoodItem.setId(0);
                    newMoodItem.setDate(now);
                    newMoodItem.setLocal(true);
                    newMoodItem.setTime(now);
                    newMoodItem.setNotes("");

                    moodItems.add(newMoodItem);
                }
                return false;
            }
        });

        rgMood = (RadioGroup) v.findViewById(R.id.rg_mood);
        rbSad = (RadioButton) v.findViewById(R.id.rb_sad);
        rbOk = (RadioButton) v.findViewById(R.id.rb_ok);
        rbHappy = (RadioButton) v.findViewById(R.id.rb_happy);
        rbSad.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_mood_sad_bg, R.raw.dr_mood_sad_circle_tp), null, null, null);
        rbOk.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_mood_ok_bg, R.raw.dr_mood_ok_circle_tp), null, null, null);
        rbHappy.setCompoundDrawablesWithIntrinsicBounds(PictureUtils.createStateDrawableFromSvg(getResources(), R.raw.dr_mood_happy_bg, R.raw.dr_mood_happy_circle_tp), null, null, null);

        moodTabHost = (FragmentTabHost) v.findViewById(android.R.id.tabhost);
        moodTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realmoodtabcontent);
        moodTabHost.addTab(moodTabHost.newTabSpec("item0").setIndicator(createSquaredTabView("1")), MoodFragment.class, new Bundle());
        moodTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                selectedTab = moodTabHost.getCurrentTab();
                refresh();
            }
        });

        bodyEt = (EditText) v.findViewById(R.id.bodyEdit);
        bodyEt.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                for (int i = 0; i < moodItems.size(); i++) {
                    if (i == selectedTab) {
                        moodItems.get(i).setNotes(bodyEt.getText().toString());
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                scrollingView.fullScroll(View.FOCUS_DOWN);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ignore
            }
        });

        Button okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickedListener);
        loaderContainer = v.findViewById(R.id.loader_container);
        return v;
    }

    public static class MoodFragment extends Fragment {
    }

    private ResourceSquaredTab createSquaredTabView(String current) {

        ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
        indicator.setResourceName(current);
        return indicator;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {

        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
        }
        super.onDismiss(dialog);
    }

    class SaveNewMoodTask extends DailyReportBusiness.PostMoodTask {
        public SaveNewMoodTask(List<MoodRes> items) {
            super(items);
        }

        @Override
        protected void onPostExecute(List<MoodRes> items) {
            if (!checkNetworkException(items, exception)) {
                super.onPostExecute(items);
                Log.d(TAG, "Mood created");
            }
            pendingTasks.remove(this);
            dismiss();
        }
    }

    class RadioGroupListener implements RadioGroup.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.rb_sad:
                    moodItems.get(selectedTab).setTemper(DailyReportConstants.SAD);
                    break;
                case R.id.rb_ok:
                    moodItems.get(selectedTab).setTemper(DailyReportConstants.SERIOUS);
                    break;
                case R.id.rb_happy:
                    moodItems.get(selectedTab).setTemper(DailyReportConstants.SMILING);
                    break;
                default:
                    break;
            }
        }
    }

    class LoadMoodTask extends DailyReportBusiness.GetMoodTask {

        public LoadMoodTask(Integer child, DateTime data) {
            super(child, data);
        }

        @Override
        protected List<MoodRes> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPreExecute() {
            loaderContainer.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<MoodRes> items) {
            super.onPostExecute(items);
            if (!checkNetworkException(items, exception)) {
                final int tabSize = items.size();
                if (tabSize != 0) {
                    moodItems = items;
                    moodCounter = items.size();

                    // I start from one cause the first tab is fixed to dont to leave the tabhost empty
                    moodTabHost.getTabWidget().removeView(moodTabHost.getTabWidget().getChildTabViewAt(0));
                    for (int i = 0; i < tabSize; i++) {
                        String newId = String.valueOf(i + 1);
                        TabHost.TabSpec tsMood = moodTabHost.newTabSpec(newId).setIndicator(createSquaredTabView(newId));
                        Bundle bundleMood = new Bundle();
                        bundleMood.putString(DailyReportConstants.TYPE, newId);
                        moodTabHost.addTab(tsMood, MoodFragment.class, bundleMood);
                    }
                    MoodRes newMoodItem = new MoodRes();
                    newMoodItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newMoodItem.setDate(now);
                    newMoodItem.setTime(now);
                    newMoodItem.setLocal(true);
                    moodItems.add(newMoodItem);

                    String newId = String.valueOf(items.size());
                    TabHost.TabSpec tsMood = moodTabHost.newTabSpec(newId).setIndicator(createSquaredTabView(newId));
                    Bundle bundleMood = new Bundle();
                    bundleMood.putString(DailyReportConstants.TYPE, newId);
                    moodTabHost.addTab(tsMood, MoodFragment.class, bundleMood);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            moodTabHost.getTabWidget().getChildAt(tabSize).performClick();
                        }
                    }, 1);

                } else {
                    // If there are not datas for the current date i create an object and sign the current selection on that object
                    MoodRes newMoodItem = new MoodRes();
                    newMoodItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newMoodItem.setDate(now);
                    newMoodItem.setTime(now);
                    newMoodItem.setLocal(true);
                    moodItems.add(newMoodItem);
                }
                refresh();
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), getString(R.string.failed_to_load_message, getString(R.string.mood_list)), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
                dismiss();
            }
            loaderContainer.setVisibility(View.GONE);
            pendingTasks.remove(this);
        }
    }

    private void refresh() {
        rgMood.setOnCheckedChangeListener(null);
        for (int i = 0; i < moodItems.size(); i++) {
            if (i == selectedTab) {
                rgMood.clearCheck();

                if (moodItems.get(i).getTime() != null) {
                    timePickerEt.setText(moodItems.get(i).getTime().toString("HH:mm"));
                } else {
                    timePickerEt.setText("select");
                }
                if (moodItems.get(i).getNotes() != null) {
                    bodyEt.setText(moodItems.get(i).getNotes());
                } else {
                    bodyEt.setText("");
                }

                rbHappy.setChecked(false);
                rbOk.setChecked(false);
                rbSad.setChecked(false);
                if (moodItems.get(i).getTemper() != null) {
                    if (moodItems.get(i).getTemper().equals(DailyReportConstants.SMILING)) {
                        rbHappy.setChecked(true);
                        // rbHappy.seSe
                    } else if (moodItems.get(i).getTemper().equals(DailyReportConstants.SERIOUS)) {
                        rbOk.setChecked(true);
                    } else if (moodItems.get(i).getTemper().equals(DailyReportConstants.SAD)) {
                        rbSad.setChecked(true);
                    }
                }

                if (moodItems.get(i).getLocal()) {
                    timePickerEt.setEnabled(true);
                    bodyEt.setEnabled(true);
                    for (int r = 0; r < rgMood.getChildCount(); r++) {
                        rgMood.getChildAt(r).setEnabled(true);
                    }
                } else {
                    timePickerEt.setEnabled(false);
                    bodyEt.setEnabled(false);
                    rgMood.setEnabled(false);
                    for (int r = 0; r < rgMood.getChildCount(); r++) {
                        rgMood.getChildAt(r).setEnabled(false);
                    }
                }
            }
        }

        rgMood.setOnCheckedChangeListener(radioGroubListener);
    }
}