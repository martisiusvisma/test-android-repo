package com.kindiedays.teacherapp.ui.dailyreport.activities;

import com.kindiedays.common.pojo.ActivityRes;

/**
 * Created by giuseppefranco on 11/05/16.
 */
public interface AdapterCallback {
    void onMethodCallback(ActivityRes activityRes);
}
