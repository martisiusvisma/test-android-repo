package com.kindiedays.teacherapp.ui.dailyreport;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TimePicker;

import com.kindiedays.common.components.CheckedTextViewCustom;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.network.daily.ResourceSquaredTab;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.MedRes;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRMedicationDialogFragment extends MainActivityDialogFragment {
    private HorizontalScrollView counterTabHostlScrollView;
    private TextViewCustom title;
    private List<MedRes> medItems = new ArrayList<>();

    private Button timePickerEt;
    private EditText medicineNameEt;
    private EditText medicineDosageEt;
    private FragmentTabHost medicationTabHost;

    private static final String TYPE = "Type";
    private Integer medCounter = 0;
    private Integer selectedTab = 0;
    List<Child> childOrChildren = new ArrayList<>();

    View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            List<MedRes> outgoingItems = new ArrayList<>();
            for (int i = 0; i < medItems.size(); i++) {
                if (medItems.get(i).getLocal()) {
                    outgoingItems.add(medItems.get(i));
                }
            }
            if (!outgoingItems.isEmpty()) {
                for (int i = 0; i < outgoingItems.size(); i++) {
                    if (outgoingItems.get(i).getChildren() != null) {
                        for (int n = 0; n < outgoingItems.get(i).getChildren().size(); n++) {
                            MedRes newMedItem = new MedRes();
                            newMedItem.setChild(outgoingItems.get(i).getChildren().get(n));
                            newMedItem.setDate(outgoingItems.get(i).getDate());
                            newMedItem.setTime(outgoingItems.get(i).getTime());
                            newMedItem.setLocal(outgoingItems.get(i).getLocal());
                            newMedItem.setDrugName(outgoingItems.get(i).getDrugName());
                            newMedItem.setDosage(outgoingItems.get(i).getDosage());
                            newMedItem.setNotes(outgoingItems.get(i).getNotes());
                            outgoingItems.add(newMedItem);
                        }
                        outgoingItems.remove(i);
                    }
                }
                PostMedicationTask task = new PostMedicationTask(outgoingItems);
                pendingTasks.add(task);
                task.execute();
            } else {
                dismiss();
            }
        }
    };

    @Override
    public void onDestroyView() {
        selectedTab = 0;
        medCounter = 0;
        medItems.clear();
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        dismiss();
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void refreshData() {
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Medications));
        if (childOrChildren != null && childOrChildren.size() == 1) {
            DateTime now = new DateTime();
            LoadMedTask task = new LoadMedTask((int) childOrChildren.get(0).getId(), now);
            pendingTasks.add(task);
            task.execute();
        } else {
            MedRes newMedItem = new MedRes();
            ArrayList<Integer> childrenId = new ArrayList<>();
            for (Child str : childOrChildren) {
                childrenId.add((int) str.getId());
            }
            newMedItem.setChildren(childrenId);
            DateTime now = new DateTime();
            newMedItem.setDate(now);
            newMedItem.setTime(now);
            newMedItem.setLocal(true);
            medItems.add(newMedItem);
            medCounter++;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dr_medication, null);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        TextViewCustom childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);
        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder("");
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }

        counterTabHostlScrollView = (HorizontalScrollView) v.findViewById(R.id.counterTabHostlScrollView);

        CheckedTextViewCustom timePickerTitle = (CheckedTextViewCustom) v.findViewById(R.id.timePickerTitle);
        timePickerTitle.setText(getString(com.kindiedays.common.R.string.daily_time));

        TextViewCustom medicineTitle = (TextViewCustom) v.findViewById(R.id.medicineTitle);
        medicineTitle.setText(getString(com.kindiedays.common.R.string.daily_medicine));
        TextViewCustom dosageTitle = (TextViewCustom) v.findViewById(R.id.dosageTitle);
        dosageTitle.setText(getString(com.kindiedays.common.R.string.daily_dosage));

        timePickerEt = (Button) v.findViewById(R.id.timePickerEt);
        timePickerEt.setInputType(InputType.TYPE_NULL);
        timePickerEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int mHour;
                int mMinute;
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.timePickerStyle,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    timePickerEt.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                                    for (int i = 0; i < medItems.size(); i++) {
                                        if (i == selectedTab) {
                                            DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
                                            medItems.get(i).setDate(new DateTime());
                                            medItems.get(i).setTime(formatter.parseDateTime(hourOfDay + ":" + minute));
                                        }
                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
                return false;
            }
        });

        medicineNameEt = (EditText) v.findViewById(R.id.medicineContent);
        medicineNameEt.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                for (int i = 0; i < medItems.size(); i++) {
                    if (i == selectedTab) {
                        medItems.get(i).setDrugName(medicineNameEt.getText().toString());
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //ignore
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ignore
            }
        });

        medicineDosageEt = (EditText) v.findViewById(R.id.dosageEdit);
        medicineDosageEt.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                for (int i = 0; i < medItems.size(); i++) {
                    if (i == selectedTab) {
                        medItems.get(i).setDosage(medicineDosageEt.getText().toString());
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //ignore
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ignore
            }
        });

        Button addItemBtn = (Button) v.findViewById(R.id.addBtn);
        addItemBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
                    String newId = String.valueOf(medCounter + 1);
                    indicator.setResourceName(newId);
                    TabHost.TabSpec ts = medicationTabHost.newTabSpec("item" + newId).setIndicator(indicator);
                    Bundle bundle = new Bundle();
                    bundle.putString(TYPE, newId);
                    medicationTabHost.addTab(ts, MedicationFragment.class, bundle);
                    medCounter = medCounter + 1;
                    counterTabHostlScrollView.postDelayed(new Runnable() {
                        public void run() {
                            counterTabHostlScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                            int currentTab = medicationTabHost.getCurrentTab();
                            medicationTabHost.setCurrentTab(currentTab + 1);
                            refresh();
                        }
                    }, 100L);

                    MedRes newMedItem = new MedRes();
                    if (childOrChildren.size() == 1) {
                        newMedItem.setChild((int) childOrChildren.get(0).getId());
                    } else {
                        ArrayList<Integer> childrenId = new ArrayList<>();
                        for (Child str : childOrChildren) {
                            childrenId.add((int) str.getId());
                        }
                        newMedItem.setChildren(childrenId);
                    }
                    DateTime now = new DateTime();
                    newMedItem.setDate(now);
                    newMedItem.setTime(now);
                    newMedItem.setLocal(true);
                    medItems.add(newMedItem);
                }
                return false;
            }
        });

        medicationTabHost = (FragmentTabHost) v.findViewById(android.R.id.tabhost);
        medicationTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);
        medicationTabHost.addTab(medicationTabHost.newTabSpec("item0").setIndicator(createSquaredTabView("1")), MedicationFragment.class, new Bundle());
        medicationTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                selectedTab = medicationTabHost.getCurrentTab();
                refresh();
            }
        });

        Button okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickedListener);
        return v;
    }

    public static class MedicationFragment extends Fragment {
    }

    private ResourceSquaredTab createSquaredTabView(String current) {

        ResourceSquaredTab indicator = new ResourceSquaredTab(getActivity());
        indicator.setResourceName(current);
        return indicator;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
        }
        super.onDismiss(dialog);
    }

    class PostMedicationTask extends DailyReportBusiness.PostNewMedicationTask {
        public PostMedicationTask(List<MedRes> items) {
            super(items);
        }

        @Override
        protected void onPostExecute(List<MedRes> items) {
            if (!checkNetworkException(items, exception)) {
                super.onPostExecute(items);
                Log.d(TAG, "Medication created");
            }
            pendingTasks.remove(this);
            dismiss();
        }
    }


    class LoadMedTask extends DailyReportBusiness.GetMedTask {
        public LoadMedTask(Integer child, DateTime data) {
            super(child, data);
        }

        @Override
        protected void onPostExecute(List<MedRes> items) {
            super.onPostExecute(items);
            if (!checkNetworkException(items, exception)) {
                final int tabSize = items.size();
                super.onPostExecute(items);
                if (tabSize != 0) {
                    medItems = items;
                    medCounter = items.size();

                    // I start from one cause the first tab is fixed to dont to leave the tabhost empty
                    medicationTabHost.getTabWidget().removeView(medicationTabHost.getTabWidget().getChildTabViewAt(0));
                    for (int i = 0; i < tabSize; i++) {
                        String newId = String.valueOf(i + 1);
                        TabHost.TabSpec tsMood = medicationTabHost.newTabSpec("item" + newId).setIndicator(createSquaredTabView(newId));
                        Bundle bundleMood = new Bundle();
                        bundleMood.putString(TYPE, newId);
                        medicationTabHost.addTab(tsMood, MedicationFragment.class, bundleMood);
                    }
                    MedRes newMedItem = new MedRes();
                    newMedItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newMedItem.setDate(now);
                    newMedItem.setTime(now);
                    newMedItem.setLocal(true);
                    medItems.add(newMedItem);

                    String newId = String.valueOf(items.size());
                    TabHost.TabSpec tsMood = medicationTabHost.newTabSpec("item" + newId).setIndicator(createSquaredTabView(newId));
                    Bundle bundleMood = new Bundle();
                    bundleMood.putString(TYPE, newId);
                    medicationTabHost.addTab(tsMood, MedicationFragment.class, bundleMood);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            medicationTabHost.getTabWidget().getChildAt(tabSize).performClick();
                        }
                    }, 1);
                } else {
                    // If there are not datas for the current date i create an object and sign the current selection on that object
                    MedRes newMedItem = new MedRes();
                    newMedItem.setChild((int) childOrChildren.get(0).getId());
                    DateTime now = new DateTime();
                    newMedItem.setDate(now);
                    newMedItem.setTime(now);
                    newMedItem.setLocal(true);
                    medItems.add(newMedItem);
                    medCounter++;
                }
                refresh();
            }
            pendingTasks.remove(this);
        }
    }

    public void refresh() {
        for (int i = 0; i < medItems.size(); i++) {
            if (i == selectedTab) {
                if (medItems.get(i).getTime() != null) {
                    timePickerEt.setText(medItems.get(i).getTime().toString("HH:mm"));
                } else {
                    timePickerEt.setText("select");
                }
                if (medItems.get(i).getDrugName() != null) {
                    medicineNameEt.setText(medItems.get(i).getDrugName());
                } else {
                    medicineNameEt.setText("");
                }
                if (medItems.get(i).getDosage() != null) {
                    medicineDosageEt.setText(medItems.get(i).getDosage());
                } else {
                    medicineDosageEt.setText("");
                }

                if (medItems.get(i).getLocal()) {
                    timePickerEt.setEnabled(true);
                    medicineNameEt.setEnabled(true);
                    medicineDosageEt.setEnabled(true);
                } else {
                    timePickerEt.setEnabled(false);
                    medicineNameEt.setEnabled(false);
                    medicineDosageEt.setEnabled(false);
                }
            }
        }
    }
}