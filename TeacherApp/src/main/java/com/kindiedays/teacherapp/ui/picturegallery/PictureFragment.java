package com.kindiedays.teacherapp.ui.picturegallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildTag;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.common.ui.picturegallery.GalleryPictureFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;

import java.util.List;

/**
 * Created by pleonard on 16/09/2015.
 */
public class PictureFragment extends GalleryPictureFragment {

    private TextView taggedChildren;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        taggedChildren = (TextView) v.findViewById(R.id.taggedChildren);
        StringBuilder childrenTagged = new StringBuilder(getActivity().getString(R.string.ChildrenTaggedOnPicture));
        List<ChildTag> photoTags = ((GalleryPicture)picture).getChildTags();
        for(int i = 0 ; i < photoTags.size() ; i++){
            Child child = ChildBusiness.getInstance().getChild(photoTags.get(i).getChildId());
            if(child != null) {
                childrenTagged.append(" ").append(child.getName());
                if (i < photoTags.size() - 1) {
                    childrenTagged.append(",");
                }
            }
        }
        taggedChildren.setText(childrenTagged.toString());
        return v;
    }

}
