package com.kindiedays.teacherapp.ui.teachergroup;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.List;

/**
 * Created by pleonard on 28/01/2016.
 */
public class TeacherGroupActivity extends KindieDaysActivity {

    private ListView groupList;
    private TeacherGroupAdapter groupAdapter;
    private Teacher teacher;
    private TextViewCustom title;
    private Button backBtn;

    AdapterView.OnItemClickListener onGroupClickListener  = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            long groupId = (long) view.getTag();
            if(teacher.getGroups().contains(groupId)){
                teacher.getGroups().remove(groupId);
            }
            else{
                teacher.getGroups().add(groupId);
            }
            groupList.setItemChecked(i, teacher.getGroups().contains(groupId));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teachergroup);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.color.kindieblue, null));
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_actionbar);

        backBtn = (Button) findViewById(R.id.leftIcon);
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setBackgroundResource(R.drawable.menu_left);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long[] groupIds = new long[teacher.getGroups().size()];
                for(int i = 0; i < groupIds.length ; i++){
                    groupIds[i] = teacher.getGroups().get(i);
                }
                PatchGroups task = new PatchGroups(teacher.getId(), groupIds);
                task.execute();
                finish();
            }
        });
        title = (TextViewCustom) findViewById(R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.Groups);

        groupList = (ListView) findViewById(R.id.groupList);
        groupList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        groupList.setOnItemClickListener(onGroupClickListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        teacher = TeacherBusiness.getInstance().getCurrentTeacher();
        GetGroups task = new GetGroups();
        task.execute();
        getPendingTasks().add(task);

    }

    class GetGroups extends GroupBusiness.LoadGroupsTask{

        @Override
        protected void onPostExecute(List<Group> groups) {
            if(!checkNetworkException(groups, exception)) {
                super.onPostExecute(groups);
                groupAdapter = new TeacherGroupAdapter(groups, TeacherGroupActivity.this);
                groupList.setAdapter(groupAdapter);
                for(int i = 0 ; i < groups.size() ; i++){
                    groupList.setItemChecked(i, teacher.getGroups().contains(groups.get(i).getId()));
                }
            }
            getPendingTasks().remove(this);
        }
    }

    class PatchGroups extends TeacherBusiness.PatchTeacherGroupsTask{

        public PatchGroups(long teacherId, long[] groupIds) {
            super(teacherId, groupIds);
        }

        @Override
        protected void onPostExecute(Teacher teacher) {
            if(!checkNetworkException(teacher, exception)) {
                super.onPostExecute(teacher);
            }
        }
    }
}
