package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildContainer;

import java.util.List;

/**
 * Created by pleonard on 15/05/2015.
 */
@SuppressWarnings("squid:S2326")
public abstract class ChildrenAttendanceContainerAdapter<T extends ChildContainer>
        extends RecyclerView.Adapter<ChildrenAttendanceContainerAdapter.ItemViewHolder> {

    protected final Context mContext;
    private List<Child> mChildren;

    public ChildrenAttendanceContainerAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public void onBindViewHolder(ChildrenAttendanceContainerAdapter.ItemViewHolder holder, int position) {
        Child child = mChildren.get(position);
        ((ChildAttendanceButton) holder.itemView).setChild(child);
        holder.itemView.setTag(child);
    }

    @Override
    public int getItemCount() {
        return mChildren == null ? 0 : mChildren.size();
    }

    public void setChildren(List<Child> children) {
        mChildren = children;
        notifyDataSetChanged();
    }

    public void addItem(int position, Child ch) {
        mChildren.add(position, ch);
        notifyItemInserted(position);
    }

    public void removeItem(int pos) {
        mChildren.remove(pos);
        notifyItemRemoved(pos);
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public ItemViewHolder(ChildAttendanceButton button) {
            super(button);
        }
    }


    @SuppressWarnings("squid:S1214")
    public interface AttendanceInterface {
        int MODE_NORMAL = 1;
        int MODE_MOVE = 2;

        int getMode();

        void endNap(Child child);

        void startNap(Child child);

        void signIn(Child child, long placeId);

        void signOut(Child child);

        void openChildProfile(long childId);

        void enterMoveMode();

    }

}
