package com.kindiedays.teacherapp.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.business.KindergartenBusiness;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.network.session.DeleteSession;
import com.kindiedays.common.pojo.Kindergarten;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.ui.MainActivity;
import com.kindiedays.teacherapp.ui.teachergroup.TeacherGroupActivity;

import java.net.UnknownHostException;
import java.util.List;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_NOT_EMPTY;
import static com.kindiedays.teacherapp.util.constants.TeacherAppConstants.CHANGE_LANGUAGE_TAG;


/**
 * Created by pleonard on 16/06/2015.
 */
public class SettingsFragment extends com.kindiedays.common.ui.settings.SettingsFragment {
    private static final String TAG = SettingsFragment.class.getSimpleName();

    Button groupSelection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_settings, null);
        ivKindergartenPicture = (ImageView) v.findViewById(R.id.kindergartenPicture);
        tvName = (TextView) v.findViewById(R.id.kindergartenName);
        tvAddress = (TextView) v.findViewById(R.id.kindergartenAddress);
        tvPhone = (TextView) v.findViewById(R.id.kindergartenPhoneNumber);
        tvManagerEmail = (TextView) v.findViewById(R.id.managerEmail);
        tvVersionNumber = (TextView) v.findViewById(R.id.versionNumber);
        spLanguages = (Spinner) v.findViewById(R.id.language);
        logoutProgress = (ProgressBar) v.findViewById(R.id.logoutProgress);
        swNotifications = (Switch) v.findViewById(R.id.notifications);
        btnLogout = (Button) v.findViewById(R.id.logout);
        v = onCreateView(v);
        groupSelection = (Button) v.findViewById(R.id.groupSelectionButton);
        groupSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TeacherGroupActivity.class);
                startActivity(intent);

            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogoutAsyncTask task = new LogoutAsyncTask();
                task.execute();
                pendingTasks.add(task);
            }
        });
        return v;
    }

    private class LogoutAsyncTask extends NetworkAsyncTask<Void> {

        @Override
        protected void onPostExecute(Void o) {
            if (getActivity() != null && exception == null) {
                clearCurrentUser();
                restartApp(null);
            } else if (getActivity() != null && exception != null) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
            pendingTasks.remove(this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            logoutProgress.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.GONE);
        }

        protected Void doNetworkTask() throws Exception {
            try {
                KindieDaysApp.getApp().unsubscribePushNotifications(getActivity());
                DeleteSession.logout();
                return null;
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(R.string.OperationFailed);
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }
    }

    @Override
    public void switchToLanguage(String language) {
        super.switchToLanguage(language);
        restartApp(STRING_NOT_EMPTY);
    }

    @Override
    protected void clearCurrentUser() {
        TeacherBusiness.getInstance().setCurrentTeacher(null);
    }

    @Override
    protected void restartApp(String key) {
        Intent refresh = new Intent(getActivity(), MainActivity.class);
        refresh.putExtra(CHANGE_LANGUAGE_TAG, key);
        startActivity(refresh);
        getActivity().finish();
    }

    @Override
    public void refreshData() {
        LoadKindergartenDataAsyncTask task = new LoadKindergartenDataAsyncTask();
        pendingTasks.add(task);
        task.execute();
    }

    private class LoadKindergartenDataAsyncTask extends KindergartenBusiness.LoadKindergartenDataTask {

        @Override
        protected List<Kindergarten> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Kindergarten> kindergartens) {
            if (!checkNetworkException(kindergartens, exception)) {
                super.onPostExecute(kindergartens);
                updateKindergartenInformation(kindergartens.get(0));
            } else {
                if (UnknownHostException.class.equals(exception.getCause().getClass())) {
                    Toast.makeText(getContext(), getString(R.string.failed_to_load_message, getString(R.string.nap_list)), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + exception.getCause(), Toast.LENGTH_SHORT).show();
                }
            }
            pendingTasks.remove(this);
        }

    }

}
