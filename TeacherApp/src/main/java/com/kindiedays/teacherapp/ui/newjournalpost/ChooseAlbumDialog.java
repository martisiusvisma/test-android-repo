package com.kindiedays.teacherapp.ui.newjournalpost;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.ui.album.PictureSelectionActivity;

import static com.kindiedays.teacherapp.ui.newjournalpost.NewJournalPostActivity.PICTURE_ACTIVITY;

/**
 * @author Eugeniy Shein on 11/12/2017.
 *         e.shein@andersenlab.com
 *         Last edit by Eugeniy Shein on 11/12/2017
 */
public class ChooseAlbumDialog extends DialogFragment {

    public interface OnChooseAlbumDialogListener {
        void onAlbumClick();

        void photoLibraryClick();

    }
    private OnChooseAlbumDialogListener mListener;

    public static final String TAG = ChooseAlbumDialog.class.getSimpleName();
    private View photoLibraryBtn;
    private View albumBtn;
    private View cancelBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.dialog_choose_album, container, false);
        photoLibraryBtn = view.findViewById(R.id.photoLibraryBtn);
        albumBtn = view.findViewById(R.id.albumBtn);
        cancelBtn = view.findViewById(R.id.cancel);
        initListeners();
        return view;
    }

    // make sure the Activity implemented it
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnChooseAlbumDialogListener)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }

    private void initListeners() {
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        albumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAlbumClick();
                dismiss();
            }
        });

        photoLibraryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.photoLibraryClick();
                dismiss();
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }
}
