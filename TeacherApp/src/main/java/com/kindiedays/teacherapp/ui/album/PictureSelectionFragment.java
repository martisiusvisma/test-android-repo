package com.kindiedays.teacherapp.ui.album;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.pojo.GalleryPicture;
import com.kindiedays.teacherapp.R;

import java.util.ArrayList;
import java.util.List;

import static com.kindiedays.common.ui.messages.CustomPhotoGalleryActivity.RESOURCES_CAP;

/**
 * Created by pleonard on 18/08/2015.
 */
public class PictureSelectionFragment extends AlbumFragment {

    private CoordinatorLayout clContent;
    private int selectedPicturesNumber;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectedPicturesNumber = ((PictureSelectionActivity) getActivity()).getSelectedNumber();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public PictureSelectionFragment(){
        super();
        onPictureClickListener = new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GalleryPicture picture = PicturesBusiness.getInstance().getCurrentPictureList().getPictures().get(position);
                if (!thumbnailAdapter.getSelectedPicturesHashes().contains(picture.getHash()) && checkResourcesOnCap()){
                    thumbnailAdapter.getSelectedPicturesHashes().add(picture.getHash());
                    selectedPicturesNumber++;
                } else {
                    if (thumbnailAdapter.getSelectedPicturesHashes().remove(picture.getHash())) {
                        selectedPicturesNumber--;
                    }
                }
                ((PictureSelectionActivity) getActivity()).updateSelectedNumberTextView(selectedPicturesNumber);
                ((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();
            }
        };
    }

    public void setSelectedPicturesHashes(ArrayList<String> selectedPicturesHashes) {
        thumbnailAdapter.setSelectedPicturesHashes(selectedPicturesHashes);
    }

    public void setClContent(CoordinatorLayout clContent) {
        this.clContent = clContent;
    }

    protected void showTopSnackBar(@StringRes int stringRes) {
        TSnackbar snackbar = TSnackbar.make(clContent, stringRes, TSnackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    protected boolean checkResourcesOnCap() {
        if (selectedPicturesNumber >= RESOURCES_CAP) {
            showTopSnackBar(R.string.gallery_max_items_cap);
            return false;
        }
        return true;
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        grid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        return view;
    }

    public String[] getSelectedPicturesIds() {
        String[] array = new String[thumbnailAdapter.getSelectedPicturesHashes().size()];
        return thumbnailAdapter.getSelectedPicturesHashes().toArray(array);
    }
}
