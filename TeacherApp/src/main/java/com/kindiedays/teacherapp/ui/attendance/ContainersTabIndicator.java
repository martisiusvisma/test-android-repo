package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 17/06/2015.
 */
public class ContainersTabIndicator extends LinearLayout {

    private TextView tvContainerName;

    public ContainersTabIndicator(Context context) {
        super(context);
        initViews();
    }

    public ContainersTabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public ContainersTabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_containers, this);
        tvContainerName = (TextView) v.findViewById(R.id.containerName);
    }

    public void setText(String text){
        tvContainerName.setText(text);
    }
}
