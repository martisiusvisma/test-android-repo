package com.kindiedays.teacherapp.ui.dailyreport.activities;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.kindiedays.common.pojo.ActivityCategory;
import com.kindiedays.common.pojo.ActivityRes;

import java.util.List;

public class RecyclerViewItem implements ParentListItem {

    private ActivityCategory mMaster;
    //The list should always contain 1 element. We use a list to be able to implement ParentListItem
    private List<ActivityRes> mSlave;

    public RecyclerViewItem(ActivityCategory name, List<ActivityRes> descriptions) {
        mMaster = name;
        mSlave = descriptions;
    }

    public ActivityCategory getCarerItem() {
        return mMaster;
    }

    @Override
    public List<?> getChildItemList() {
        return mSlave;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
