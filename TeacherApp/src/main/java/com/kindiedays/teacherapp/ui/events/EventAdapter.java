package com.kindiedays.teacherapp.ui.events;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;
import com.kindiedays.teacherapp.R;

import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by pleonard on 30/06/2015.
 */
public class EventAdapter extends BaseAdapter {

    private List<Event> events;
    private Activity activity;
    private boolean pastEvents;

    public EventAdapter(Activity activity, boolean pastEvents) {
        this.activity = activity;
        this.pastEvents = pastEvents;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (events == null) {
            return 0;
        }
        return events.size();
    }

    @Override
    public Event getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return events.get(position).getId();
    }

    @Override
    @SuppressWarnings("squid:S1226")
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_event, null);
            holder = new ViewHolder();
            holder.attendance = (TextView) convertView.findViewById(R.id.attendance);
            holder.eventDate = (TextView) convertView.findViewById(R.id.eventDate);
            holder.eventName = (TextView) convertView.findViewById(R.id.eventName);
            holder.eventTime = (TextView) convertView.findViewById(R.id.eventHour);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Event event = getItem(position);
        int counter = 0;
        for (EventInvitation invitation : event.getInvitations()) {
            if (invitation.getState() == EventInvitation.InvitationState.CONFIRMED) {
                counter++;
            }
        }
        holder.attendance.setText(Integer.toString(counter));

        holder.eventName.setText(event.getName());
        LocalDate date;
        if (event.getInterval() == null) {
            date = event.getDate();
        } else {
            if (pastEvents) {
                date = EventsBusiness.getInstance().getLastOccurence(event).toLocalDate();
            } else {
                date = EventsBusiness.getInstance().getNextOccurence(event).toLocalDate();
            }
        }
        holder.eventDate.setText(date.toString("dd/MM/yyyy"));
        holder.eventTime.setText(event.getStart().toString("HH:mm"));
        return convertView;
    }

    static class ViewHolder {
        TextView attendance;
        TextView eventDate;
        TextView eventName;
        TextView eventTime;
    }
}
