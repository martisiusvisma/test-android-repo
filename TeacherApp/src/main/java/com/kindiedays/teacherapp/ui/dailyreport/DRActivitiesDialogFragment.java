package com.kindiedays.teacherapp.ui.dailyreport;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.pojo.ActivityCategory;
import com.kindiedays.common.pojo.ActivityRes;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.ui.MainActivityDialogFragment;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.DailyReportBusiness;
import com.kindiedays.teacherapp.ui.dailyreport.activities.ActivityAdapter;
import com.kindiedays.teacherapp.ui.dailyreport.activities.RecyclerViewItem;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by giuseppefranco on 24/03/16.
 */
public class DRActivitiesDialogFragment extends MainActivityDialogFragment {
    private Button okButton;
    private ActivityAdapter mAdapter;
    private Context context;
    private TextViewCustom title;
    private RecyclerView recyclerView;

    private List<ActivityCategory> activitiesTitle = new ArrayList<>();
    private List<Child> childOrChildren = new ArrayList<>();
    private boolean isDismissed;

    @SuppressWarnings({"squid:S1188", "squid:S134"})
    private View.OnClickListener onOkClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Log.d(DRActivitiesDialogFragment.class.getName(), "OK Clicked");

            final List<? extends ParentListItem> parentItemList = mAdapter.getParentItemList();

            if (parentItemList.size() == 0) {
                isDismissed = true;
                dismiss();
                return;
            }

            List<ActivityRes> activityRes = findUpdatedCategories(parentItemList);
            List<ActivityRes> activityMultiplied = multiplyActivityResWithChildren(activityRes, childOrChildren);
            Log.w(DRActivitiesDialogFragment.class.getName(), "activityMultiplied: " + activityMultiplied);
            SaveActivity task = new SaveActivity(activityMultiplied);
            task.execute();
        }
    };

    private List<ActivityRes> findUpdatedCategories(List<? extends ParentListItem> parentListItems) {
        List<ActivityRes> activityResFiltered = new ArrayList<>();
        for (ParentListItem parentListItem : parentListItems) {
            for (Object o : parentListItem.getChildItemList()) {
                ActivityRes activityRes = (ActivityRes) o;
                if (activityRes.isValueUpdated()) {
                    activityResFiltered.add(activityRes);
                }
            }
        }
        return activityResFiltered;
    }

    private List<ActivityRes> multiplyActivityResWithChildren(List<ActivityRes> activityList, List<Child> childList) {
        List<ActivityRes> activityMultiplied = new ArrayList<>(activityList.size() * childList.size());
        for (ActivityRes activityRes : activityList) {
            for (Child child : childList) {
                ActivityRes res = new ActivityRes();
                res.setChild((int) child.getId());
                res.setDate(activityRes.getDate());
                res.setCategory(activityRes.getCategory());
                res.setText(activityRes.getText());
                activityMultiplied.add(res);
            }
        }
        return activityMultiplied;
    }

    @Override
    public void refreshData() {
        // We should use this to get the existing data
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Daily_Report_Activities));
        LoadActivityCategoriesTitle task = new LoadActivityCategoriesTitle();
        pendingTasks.add(task);
        task.execute();
    }

    @Override
    public void onPause() {
        dismiss();
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        Set<Child> setOfChildren = (Set<Child>) getArguments().getSerializable("child");
        childOrChildren.addAll(setOfChildren);
        context = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        isDismissed = false;
        View v = inflater.inflate(R.layout.dialog_dr_categories, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        TextViewCustom childrenList = (TextViewCustom) v.findViewById(R.id.childrenList);
        if (childOrChildren != null) {
            StringBuilder titleStr = new StringBuilder();
            for (int i = 0; i < childOrChildren.size(); i++) {
                titleStr.append(childOrChildren.get(i).getNickname());
                if (!childOrChildren.get(i).equals(childOrChildren.get(childOrChildren.size() - 1))) {
                    titleStr.append(CommonConstants.STRING_COMMA);
                }
            }
            childrenList.setText(titleStr);
        }
        okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickedListener);
        setUpAdapter(new HashMap<ActivityCategory, ActivityRes>());
        return v;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            title.setVisibility(View.VISIBLE);
        }
        super.onDismiss(dialog);
    }

    @Override
    public void onDestroyView() {
        ChildBusiness.getInstance().unselectAllChildren();
        super.onDestroyView();
    }

    private void setUpAdapter(Map<ActivityCategory, ActivityRes> categoryActivities) {
        List<RecyclerViewItem> recyclerViewItemsList = new ArrayList<>();
        for (Map.Entry<ActivityCategory, ActivityRes> entry : categoryActivities.entrySet()) {
            RecyclerViewItem item = new RecyclerViewItem(entry.getKey(), Collections.singletonList(entry.getValue()));
            recyclerViewItemsList.add(item);
        }
        Log.d(DRActivitiesDialogFragment.class.getName(), "categories to display count = " + categoryActivities.size());

        mAdapter = new ActivityAdapter(/*DRActivitiesDialogFragment.this,*/ context, recyclerView, recyclerViewItemsList);
        if (recyclerView != null) {
            recyclerView.setAdapter(mAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            //position to be clicked
            final int pos = 0;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(pos);
                    if (viewHolder != null) {
                        viewHolder.itemView.performClick();
                    }
                }
            }, 1);
        }
    }

    class LoadActivityCategoriesTitle extends DailyReportBusiness.LoadActivityCategoriesTask {
        @Override
        protected void onPostExecute(List<ActivityCategory> categories) {
            super.onPostExecute(categories);
            if (getActivity() == null)
                return;

            if (getActivity() != null && !isDismissed && !checkNetworkException(categories, exception)) {
                super.onPostExecute(categories);
                activitiesTitle = categories;
                if (childOrChildren.size() == 1) {
                    LoadActivitiesDescription activitiesDescription = new LoadActivitiesDescription((int) childOrChildren.get(0).getId(), new DateTime());
                    pendingTasks.add(activitiesDescription);
                    activitiesDescription.execute();
                } else {
                    Map<ActivityCategory, ActivityRes> categoryActivitiesMap = new HashMap<>();
                    for (ActivityCategory category : categories) {
                        ActivityRes description = new ActivityRes(0, new DateTime(), "", false);
                        description.setCategory(category.getId());
                        description.setCategoryName(category.getName());
                        categoryActivitiesMap.put(category, description);
                    }
                    setUpAdapter(categoryActivitiesMap);
                }
            }
            pendingTasks.remove(this);
        }
    }


    class LoadActivitiesDescription extends DailyReportBusiness.LoadActivitiesDescTask {
        private int childId;

        public LoadActivitiesDescription(int childId, DateTime date) {
            super(childId, date);
            this.childId = childId;
        }

        @Override
        protected void onPostExecute(List<ActivityRes> activities) {
            super.onPostExecute(activities);
            if (getActivity() == null || isDismissed)
                return;

            Map<ActivityCategory, ActivityRes> categoryActivities = new HashMap<>();
            if (!checkNetworkException(activities, exception)) {
                super.onPostExecute(activities);
                categoryActivities = handleActivitiesTitle(childId, activities);
            }
            setUpAdapter(categoryActivities);
            pendingTasks.remove(this);
        }

        private Map<ActivityCategory, ActivityRes> handleActivitiesTitle(int childId, List<ActivityRes> activities) {
            Map<ActivityCategory, ActivityRes> categoryActivities = new HashMap<>();
            for (ActivityCategory category : activitiesTitle) {
                ActivityRes description = null;
                for (ActivityRes activityRes : activities) {
                    if (activityRes.getCategory().equals(category.getId())) {
                        description = activityRes;
                        break;
                    }
                }

                if (description == null) {
                    //No activity on the server yet, let's create an empty one
                    description = new ActivityRes(childId, new DateTime(), "", false);
                    description.setCategory(category.getId());
                    description.setCategoryName(category.getName());
                }

                if (childOrChildren.size() == 1 || !description.isExistsOnServer()) {
                    categoryActivities.put(category, description);
                }
            }
            return categoryActivities;
        }
    }


    class SaveActivity extends DailyReportBusiness.PostActivityTask {
        public SaveActivity(List<ActivityRes> items) {
            super(items);
        }

        @Override
        protected void onPostExecute(List<ActivityRes> items) {
            if (items != null && !checkNetworkException(items, exception)) {
                super.onPostExecute(items);
                for (int i = 0; i < items.size(); i++) {
                    Log.d(TAG, "Activity created: " + items.get(i).getCategoryName());
                }
            }
            dismiss();
        }
    }
}
