package com.kindiedays.teacherapp.ui.peopleselection.recipientselection;

import android.content.Context;
import android.view.ViewGroup;

import com.kindiedays.teacherapp.ui.peopleselection.ChildButtonAdapter;
import com.kindiedays.teacherapp.ui.peopleselection.childselection.PersonStatusSelectionButton;

/**
 * Created by pleonard on 07/10/2015.
 */
public class CarerSelectionButtonAdapter extends ChildButtonAdapter {

    public CarerSelectionButtonAdapter(Context context) {
        super(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        PersonStatusSelectionButton button = new PersonStatusSelectionButton(mContext);
        return new ItemViewHolder(button);
    }
}
