package com.kindiedays.teacherapp.ui.childprofile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.pojo.Note;

import java.util.List;

/**
 * Created by pleonard on 19/05/2015.
 */
public class NotesFragment extends Fragment {

    private ListView notesList;
    private Button newNoteButton;
    private NotesAdapter notesAdapter;
    private ProgressBar progress;
    private List<Note> notes;

    public interface NotesInterface{
        void saveNote(Note note);
        void editNote(Note note);
        void deleteNote(Note note);
    }

    AdapterView.OnItemClickListener onNoteClickedListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ((NotesInterface) getActivity()).editNote(notesAdapter.getItem(position));
        }

    };

    View.OnClickListener onNewNoteClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            ((NotesInterface) getActivity()).editNote(new Note());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notes, null);
        newNoteButton = (Button) v.findViewById(R.id.newNoteButton);
        newNoteButton.setOnClickListener(onNewNoteClickListener);
        notesList = (ListView) v.findViewById(R.id.noteslist);
        notesAdapter = new NotesAdapter(getActivity());
        notesList.setAdapter(notesAdapter);
        notesList.setOnItemClickListener(onNoteClickedListener);
        progress = (ProgressBar) v.findViewById(R.id.notesProgress);
        displayNotes();
        return v;
    }

    private void displayNotes(){
        if(notes != null && notesList != null) {
            newNoteButton.setVisibility(View.VISIBLE);
            notesList.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            notesAdapter.setNotes(notes);
        }
    }

    public void setNotes(List<Note> notes){
        this.notes = notes;
        displayNotes();
    }

    @SuppressWarnings("squid:S1172")
    public void updateNote(Note note) {
        notesAdapter.notifyDataSetChanged();
    }

    public void addNote(Note note){
        notesAdapter.getNotes().add(0, note);
        notesAdapter.notifyDataSetChanged();
    }

    public void deleteNote(Note note){
        notesAdapter.getNotes().remove(note);
        notesAdapter.notifyDataSetChanged();
    }
}
