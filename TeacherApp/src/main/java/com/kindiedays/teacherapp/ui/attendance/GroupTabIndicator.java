package com.kindiedays.teacherapp.ui.attendance;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindiedays.teacherapp.R;

/**
 * Created by pleonard on 21/04/2015.
 */
public class GroupTabIndicator extends LinearLayout {

    private TextView tvCurrent;
    private TextView tvTotal;
    private TextView tvGroupName;


    public GroupTabIndicator(Context context) {
        super(context);
        initViews();
    }

    public GroupTabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }


    public GroupTabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    protected void initViews(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.tab_indicator_group, this);
        tvCurrent = (TextView) v.findViewById(R.id.current);
        tvTotal = (TextView) v.findViewById(R.id.total);
        tvGroupName = (TextView) v.findViewById(R.id.groupName);
    }

    public void setCurrent(int current) {
        tvCurrent.setText(String.valueOf(current));
    }

    public void setTotal(int total) {
        tvTotal.setText(String.valueOf(total));
    }

    public void setGroupName(String groupName) {
        tvGroupName.setText(groupName);
    }


}
