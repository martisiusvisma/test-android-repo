package com.kindiedays.teacherapp.ui.childprofile;


import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindiedays.common.pojo.Generic;
import com.kindiedays.common.svg.SVGImageView;
import com.kindiedays.common.ui.dailyreport.ParallaxRecyclerAdapter;
import com.kindiedays.teacherapp.R;

import java.util.List;

import static com.kindiedays.common.utils.constants.DailyReportConstants.ACTIVITY;
import static com.kindiedays.common.utils.constants.DailyReportConstants.BATH;
import static com.kindiedays.common.utils.constants.DailyReportConstants.DIAPER_TAG;
import static com.kindiedays.common.utils.constants.DailyReportConstants.MEAL;
import static com.kindiedays.common.utils.constants.DailyReportConstants.MED;
import static com.kindiedays.common.utils.constants.DailyReportConstants.MOOD;
import static com.kindiedays.common.utils.constants.DailyReportConstants.NAP;
import static com.kindiedays.common.utils.constants.DailyReportConstants.NOTE;
import static com.kindiedays.common.utils.constants.DailyReportConstants.POTTY_TAG;
import static com.kindiedays.common.utils.constants.DailyReportConstants.SAD;
import static com.kindiedays.common.utils.constants.DailyReportConstants.SERIOUS;
import static com.kindiedays.common.utils.constants.DailyReportConstants.SMILING;
import static com.kindiedays.common.utils.constants.DailyReportConstants.TOILET_TAG;

public class DailyReportRecycleViewAdapter extends ParallaxRecyclerAdapter<Generic> {
    private List<Generic> generics;

    public DailyReportRecycleViewAdapter(List<Generic> items) {
        super(items);
        this.generics = items;
    }

    @Override
    public void onBindViewHolderImpl(RecyclerView.ViewHolder viewHolder, ParallaxRecyclerAdapter<Generic> adapter, int i) {
        Generic item = adapter.getData().get(i);
        DailyReportViewHolder vh = (DailyReportViewHolder) viewHolder;
        vh.bind(item);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup viewGroup,
                                                          final ParallaxRecyclerAdapter<Generic> adapter,
                                                          int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_recyclerview, viewGroup, false);
        return new DailyReportViewHolder(itemView);
    }

    @Override
    public int getItemCountImpl(ParallaxRecyclerAdapter<Generic> adapter) {
        return generics.size();
    }


    private class DailyReportViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTime;
        private SVGImageView ivLogo;
        private TextView tvRaw1;
        private TextView tvRaw2;
        private ImageView ivInfo;

        private DailyReportViewHolder(View itemView) {
            super(itemView);
            tvTime = (TextView) itemView.findViewById(R.id.time);
            ivLogo = (SVGImageView) itemView.findViewById(R.id.logo);
            tvRaw1 = (TextView) itemView.findViewById(R.id.raw1);
            tvRaw2 = (TextView) itemView.findViewById(R.id.raw2);
            ivInfo = (ImageView) itemView.findViewById(R.id.iv_more_info);
        }

        private String validateAcceptableLength(final String str) {
            String temp = str;
            if (str.length() > 34) {
                temp = str.substring(0, 24) + "...";
                ivInfo.setImageResource(R.drawable.chevrone_right);
                ivInfo.setVisibility(View.VISIBLE);
            } else {
                ivInfo.setVisibility(View.GONE);
            }
            return temp;
        }

        private void bind(Generic currentItem) {
            tvTime.setText(currentItem.getTime().toString("HH:mm"));
            switch (currentItem.getType()) {
            case MOOD:
                resolveMood(currentItem);
                break;
            case NOTE:
                ivLogo.setImageResource(+R.raw.dr_notes_circle);
                tvRaw1.setText(validateAcceptableLength(currentItem.getRow1()));
                tvRaw2.setVisibility(View.GONE);
                break;
            case MED:
                ivLogo.setImageResource(+R.raw.dr_medication_circle);
                tvRaw1.setText(validateAcceptableLength(currentItem.getRow1()));
                tvRaw2.setText(validateAcceptableLength(currentItem.getRow2()));
                break;
            case MEAL:
                resolveMeal(currentItem);
                break;
            case NAP:
                ivLogo.setImageResource(+R.raw.dr_sleep_circle);
                tvRaw1.setText(currentItem.getRow1());
                tvRaw2.setText(validateAcceptableLength(currentItem.getNap().getNotes()));
                break;
            case ACTIVITY:
                ivLogo.setImageResource(+R.raw.dr_activites_circle);
                tvRaw1.setText(currentItem.getRow1());
                tvRaw2.setText(validateAcceptableLength(currentItem.getRow2()));
                break;
            case BATH:
                resolveBath(currentItem);
                break;
            default:
                break;
            }
        }

        private void resolveMood(Generic currentItem) {
            switch (currentItem.getRow1()) {
            case SAD:
                tvRaw1.setText(R.string.Sad);
                ivLogo.setImageResource(+R.raw.dr_mood_sad_circle);
                break;
            case SMILING:
                tvRaw1.setText(R.string.Happy);
                ivLogo.setImageResource(+R.raw.dr_mood_happy_circle);
                break;
            case SERIOUS:
                tvRaw1.setText(R.string.OK);
                ivLogo.setImageResource(+R.raw.dr_mood_ok_circle);
                break;
            default:
                break;
            }

            tvRaw2.setText(validateAcceptableLength(currentItem.getMood().getNotes()));
        }

        private void resolveMeal(Generic currentItem) {
            ivLogo.setImageResource(+R.raw.dr_meals_circle);
            if (currentItem.getRow1().length() != 0) {
                tvRaw1.setVisibility(View.VISIBLE);
            } else {
                tvRaw1.setVisibility(View.GONE);
            }
            tvRaw1.setText(currentItem.getMeal().getReactionDescription());

            if (!TextUtils.isEmpty(currentItem.getMeal().getNotes())) {
                ivInfo.setImageResource(R.drawable.chevrone_right);
                ivInfo.setVisibility(View.VISIBLE);
            } else {
                ivInfo.setVisibility(View.GONE);
            }

            //check it
            resolveMealRawTwo(currentItem);
        }

        private void resolveMealRawTwo(Generic currentItem) {
            if (currentItem.getRow2().length() != 0) {
                tvRaw2.setVisibility(View.VISIBLE);
            } else {
                tvRaw2.setVisibility(View.GONE);
            }
            switch (currentItem.getRow2()) {
            case "0.0":
                tvRaw2.setText(R.string.empty_glass);
                break;
            case "0.5":
                tvRaw2.setText(R.string.half_glass);
                break;
            case "1.0":
                tvRaw2.setText(R.string.full_glass);
                break;
            default:
                break;
            }
        }


        private void resolveBath(Generic currentItem) {
            ivLogo.setImageResource(+R.raw.dr_wc_circle);
            switch (currentItem.getRow1()) {
                case DIAPER_TAG:
                    tvRaw1.setText(R.string.Diaper);
                    break;
                case POTTY_TAG:
                    tvRaw1.setText(R.string.Potty);
                    break;
                case TOILET_TAG:
                    tvRaw1.setText(R.string.Toilet);
                    break;
                default:
                    break;
            }
            tvRaw2.setText(currentItem.getBath().getTypeDescription());
            if (!TextUtils.isEmpty(currentItem.getBath().getNotes())) {
                ivInfo.setImageResource(R.drawable.chevrone_right);
                ivInfo.setVisibility(View.VISIBLE);
            } else {
                ivInfo.setVisibility(View.GONE);
            }
        }
    }
}
