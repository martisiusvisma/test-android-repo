package com.kindiedays.teacherapp.ui.album;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.kindiedays.common.KindieDaysApp;
import com.kindiedays.common.components.TextViewCustom;
import com.kindiedays.common.model.Model;
import com.kindiedays.common.network.KindieDaysNetworkException;
import com.kindiedays.common.network.NetworkAsyncTask;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.MainActivityFragment;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PicturesBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.pojo.AlbumCategory;
import com.kindiedays.teacherapp.ui.RecycleViewDelegatesAdapter;
import com.kindiedays.teacherapp.ui.picturegallery.PictureGalleryActivity;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

public class AlbumMainTeacherFragment extends MainActivityFragment implements
        AlbumCategoryDelegate.OnAlbumCategoryClickListener, AlbumPictureDelegate.OnAlbumPictureClickListener {

    private List<Group> groups;
    private List<Model> albumList;

    protected TextViewCustom title;
    private RecyclerView rvAlbum;
    private RecycleViewDelegatesAdapter adapter;
    private ProgressBar pbLoader;

    protected AdapterView.OnItemLongClickListener onPictureLongClickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album_teacher, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvAlbum = (RecyclerView) view.findViewById(R.id.rv_album);
        pbLoader = (ProgressBar) view.findViewById(R.id.pb_loader);
        adapter = new RecycleViewDelegatesAdapter();
        adapter.addDelegate(new AlbumCategoryDelegate(this));
        adapter.addDelegate(new AlbumPictureDelegate(this));
        rvAlbum.setAdapter(adapter);
        rvAlbum.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void refreshData() {
        if (albumList != null && !albumList.isEmpty()) {
            return;
        }

        groups = GroupBusiness.getInstance().getContainers(true);

        if (groups == null || groups.isEmpty()) {
            GetGroupsAsyncTask groupsTask = new GetGroupsAsyncTask();
            pendingTasks.add(groupsTask);
            groupsTask.execute();
        } else {
            GetPicturesPerCategoryTask picsTask = new GetPicturesPerCategoryTask(groups);
            pendingTasks.add(picsTask);
            picsTask.execute();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (albumList != null && !albumList.isEmpty()) {
            if (adapter != null && adapter.getItemCount() == 0) {
                adapter.setItems(albumList);
            }
            pbLoader.setVisibility(View.GONE);
            rvAlbum.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        title = (TextViewCustom) getActivity().findViewById(com.kindiedays.common.R.id.titleTv);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(com.kindiedays.common.R.string.Album));
    }

    @Override
    public void onCategoryClick(AlbumCategory model) {
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, AlbumTeacherFragment.newInstance(model), AlbumTeacherFragment.TAG)
                .addToBackStack(AlbumTeacherFragment.TAG)
                .commit();
    }

    @Override
    public void onPictureClick(AlbumCategory category, int pos) {
        NetworkAsyncTask<PictureList> task = getAsyncTask(category, pos);
        pendingTasks.add(task);
        task.execute();
    }

    private NetworkAsyncTask<PictureList> getAsyncTask(AlbumCategory category, int pos) {
        if (category.getId() == PicturesBusiness.ALL_GROUPS) {
            return new LoadCachePictures(pos);
        } else if (category.getType() == AlbumCategory.GROUP) {
            return new LoadCachePicturesPerGroup(category.getId(), pos);
        } else {
            return new LoadCachePicturesPerTeacher(category.getId(), pos);
        }
    }

    private class GetGroupsAsyncTask extends GroupBusiness.LoadGroupsTask {
        @Override
        protected void onPostExecute(List<Group> groups) {
            if (!checkNetworkException(groups, exception)) {
                super.onPostExecute(groups);
                AlbumMainTeacherFragment.this.groups = GroupBusiness.getInstance().getContainers(true);
                GetPicturesPerCategoryTask picsTask = new GetPicturesPerCategoryTask(groups);
                pendingTasks.add(picsTask);
                picsTask.execute();
            }
            pendingTasks.remove(this);
        }
    }


    private class GetTeachersAsyncTask extends TeacherBusiness.LoadTeachersTask {
        @Override
        protected void onPostExecute(List<Teacher> teachers) {
            super.onPostExecute(teachers);
            if (!checkNetworkException(teachers, exception)) {
                super.onPostExecute(teachers);
                Teacher currentTeacher = TeacherBusiness.getInstance().getCurrentTeacher();
                LoadTeacherPicturesTask picturesTask = new LoadTeacherPicturesTask(Collections.singletonList(currentTeacher));
                pendingTasks.add(picturesTask);
                picturesTask.execute();
            }
            pendingTasks.remove(this);
        }
    }


    private class GetPicturesPerCategoryTask extends PicturesBusiness.GetPicturesPerCategory {
        GetPicturesPerCategoryTask(List<Group> groups) {
            super(groups);
        }

        @Override
        protected List<Model> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, e.getCause().getMessage(), true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Model> albumList) {
            super.onPostExecute(albumList);
            if (!checkNetworkException(albumList, exception)) {
                super.onPostExecute(albumList);
                AlbumMainTeacherFragment.this.albumList = albumList;
                adapter.setItems(albumList);
                Teacher currentTeacher = TeacherBusiness.getInstance().getCurrentTeacher();
                if (currentTeacher == null) {
                    GetTeachersAsyncTask teacherTask = new GetTeachersAsyncTask();
                    pendingTasks.add(teacherTask);
                    teacherTask.execute();
                } else {
                    LoadTeacherPicturesTask picturesTask = new LoadTeacherPicturesTask(Collections.singletonList(currentTeacher));
                    pendingTasks.add(picturesTask);
                    picturesTask.execute();
                }
            }
        }
    }


    private class LoadCachePicturesPerGroup extends PicturesBusiness.GetGroupPicturesAsyncTask {
        private int position;

        LoadCachePicturesPerGroup(long groupId, int position) {
            super(groupId);
            this.position = position;
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            super.onPostExecute(pictureList);
            pendingTasks.remove(this);
            Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
            intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
            getActivity().startActivity(intent);
        }
    }


    private class LoadTeacherPicturesTask extends PicturesBusiness.GetPicturesPerTeacher {

        LoadTeacherPicturesTask(List<Teacher> teachers) {
            super(teachers);
        }

        protected List<Model> doNetworkTask() throws Exception {
            try {
                return super.doNetworkTask();
            } catch (Exception e) {
                String message;
                if (UnknownHostException.class.equals(e.getCause().getClass())) {
                    message = KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.failed_to_load_message, KindieDaysApp.getApp().getString(com.kindiedays.common.R.string.Conversations).toLowerCase());
                } else {
                    message = e.getCause().getMessage();
                }
                KindieDaysNetworkException exception = new KindieDaysNetworkException(0, message, true);
                exception.initCause(e.getCause());
                throw exception;
            }
        }

        @Override
        protected void onPostExecute(List<Model> albumList) {
            super.onPostExecute(albumList);
            pendingTasks.remove(this);
            if (!isAdded() || isRemoving()) {
                return;
            }
            if (!checkNetworkException(albumList, exception)) {
                super.onPostExecute(albumList);
                AlbumMainTeacherFragment.this.albumList.addAll(albumList);
                adapter.addItems(albumList, adapter.getItemCount());
                pbLoader.setVisibility(View.GONE);
                rvAlbum.setVisibility(View.VISIBLE);
            }
        }
    }


    private class LoadCachePicturesPerTeacher extends PicturesBusiness.GetTeacherPicturesAsyncTask {
        private int position;

        LoadCachePicturesPerTeacher(long teacherId, int position) {
            super(teacherId);
            this.position = position;
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            super.onPostExecute(pictureList);
            pendingTasks.remove(this);
            Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
            intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
            getActivity().startActivity(intent);
        }
    }


    private class LoadCachePictures extends PicturesBusiness.GetPicturesAsyncTask {
        private int position;

        LoadCachePictures(int position) {
            this.position = position;
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            super.onPostExecute(pictureList);
            pendingTasks.remove(this);
            Intent intent = new Intent(getActivity(), PictureGalleryActivity.class);
            intent.putExtra(PictureGalleryActivity.PICTURE_POSITION, position);
            getActivity().startActivity(intent);
        }
    }
}
