package com.kindiedays.teacherapp.ui.dailyreport.meals;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kindiedays.common.pojo.DrinkRes;

/**
 * Created by Giuseppe Franco - Starcut on 13/06/16.
 */
public class SpinAdapterDrink extends ArrayAdapter<DrinkRes>{


    public SpinAdapterDrink(Context context, int textViewResourceId,
                            DrinkRes[] values) {
        super(context, textViewResourceId, values);
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(this.getItem(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(this.getItem(position).getName());
        return label;
    }
}