package com.kindiedays.teacherapp.ui.events;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.kindiedays.common.business.EventsBusiness;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Event;
import com.kindiedays.common.pojo.EventInvitation;
import com.kindiedays.common.ui.CustomTimePickerDialog;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.utils.constants.CommonConstants;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.ui.peopleselection.childselection.ChildSelectionActivity;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pleonard on 11/06/2015.
 */
public class EditEventActivity extends KindieDaysActivity {
    private static final String TIME_PATTERN = "HH:mm";
    private static final String DATE_PATTERN = "dd/MM/yyyy";
    private static final String TAG = EditEventActivity.class.getSimpleName();

    private EditText etName;
    private EditText etDescription;
    private EditText etLocation;
    private Button btnStarts;
    private Button btnEnds;
    private Button btnDate;
    private Button btnEndRepeating;
    private Spinner repeatingSpinner;
    private TextView tvInvited;
    private InvitationExpandableBaseAdapter invitedAdapter;
    private boolean savingInProgress;

    String[] repeatingPeriods = new String[]{Event.NEVER, Event.DAILY, Event.WEEKLY, Event.BIWEEKLY};

    private List<Long> selectedChildren = new ArrayList<>();
    private List<Long> sentInvitationsTo = new ArrayList<>();

    private Event event;

    public static final String EVENT_ID = "eventId";

    private static final int SELECT_PEOPLE = 1;

    @SuppressWarnings("squid:S1481")
    View.OnClickListener onRemoveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            Log.i(TAG, "remove: " + event.getId());

            AlertDialog.Builder builder = new AlertDialog.Builder(EditEventActivity.this);
            builder.setTitle(getString(R.string.Confirmation)).setMessage(getString(R.string.Are_you_sure_to_cancel_the_event)).setCancelable(true)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            if (!savingInProgress) {
                                savingInProgress = true;
                                event.setName(etName.getText().toString());
                                event.setNotes(etDescription.getText().toString());
                                event.setLocation(etLocation.getText().toString());
                                event.setState(Event.CANCELLED);
                                event.getInvitations().clear();
                                for (Long _id : selectedChildren) {
                                    EventInvitation invitation = new EventInvitation();
                                    invitation.setState(EventInvitation.InvitationState.INVITED);
                                    invitation.setChildId(id);
                                    event.getInvitations().add(invitation);
                                }
                                SaveEvent saveEvent = new SaveEvent(event);
                                getPendingTasks().add(saveEvent);
                                saveEvent.execute();
                            }
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            Dialog dialog = builder.create();
            dialog.show();
        }
    };

    View.OnClickListener onDateClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View v) {
            if (event.getId() == 0 || !EventsBusiness.getInstance().isPastEvent(event)) {
                LocalDate date;
                if (event.getDate() != null && v == btnDate) {
                    date = event.getDate();
                } else if (event.getEndRecurrence() != null && v == btnEndRepeating) {
                    date = event.getEndRecurrence();
                } else {
                    date = new LocalDate();
                }

                DatePickerDialog dialog = new DatePickerDialog(EditEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                LocalDate date = new LocalDate(year, monthOfYear + 1, dayOfMonth);
                                if (v == btnDate) {
                                    event.setDate(date);
                                    btnDate.setText(getString(R.string.Date) + " " + date.toString(DATE_PATTERN));
                                }
                                if (v == btnEndRepeating) {
                                    event.setEndRecurrence(date);
                                    btnEndRepeating.setText(getString(R.string.Ends) + " " + date.toString(DATE_PATTERN));
                                }
                            }
                        },
                        date.getYear(), date.getMonthOfYear() - 1, date.getDayOfMonth()
                );
                if (v == btnEndRepeating) {
                    dialog.getDatePicker().setMinDate(event.getDate().plusDays(1).toDate().getTime());
                }
                dialog.getDatePicker().setCalendarViewShown(false);
                dialog.show();
            }
        }
    };

    View.OnClickListener onTimeClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View v) {
            if (event.getId() == 0 || !EventsBusiness.getInstance().isPastEvent(event)) {
                LocalTime time;
                if (v.getId() == R.id.btn_startTime) {
                    time = event.getStart();
                } else {
                    time = event.getEnd();
                }
                TimePickerDialog dialog = new CustomTimePickerDialog(EditEventActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                LocalTime localTime = new LocalTime(hourOfDay, minute);
                                if (v.getId() == R.id.btn_startTime) {
                                    event.setStart(localTime);
                                    btnStarts.setText(getString(R.string.Starts) + " " + localTime.toString(TIME_PATTERN));
                                } else {
                                    event.setEnd(localTime);
                                    btnEnds.setText(getString(R.string.Ends) + " " + localTime.toString(TIME_PATTERN));
                                }
                            }
                        },
                        time.getHourOfDay(), time.getMinuteOfHour(), true);
                dialog.show();
            }
        }
    };

    View.OnClickListener onInvitePeopleClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(EditEventActivity.this, ChildSelectionActivity.class);
            long[] selectedChildIds = new long[selectedChildren.size()];
            for (int i = 0; i < selectedChildren.size(); i++) {
                selectedChildIds[i] = selectedChildren.get(i);
            }
            long[] invitationsSentToIds = new long[sentInvitationsTo.size()];
            for (int i = 0; i < sentInvitationsTo.size(); i++) {
                invitationsSentToIds[i] = sentInvitationsTo.get(i);
            }
            intent.putExtra(ChildSelectionActivity.SELECTED_READ_ONLY, invitationsSentToIds);
            intent.putExtra(ChildSelectionActivity.SELECTED_RW, selectedChildIds);
            startActivityForResult(intent, SELECT_PEOPLE);
        }
    };

    private AdapterView.OnItemSelectedListener onRepeatingItemSelected = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i != 0) {
                event.setInterval(repeatingPeriods[i]);
                btnEndRepeating.setEnabled(true);
            } else {
                event.setInterval(null);
                btnEndRepeating.setEnabled(false);
                btnEndRepeating.setText(getString(R.string.end_repetition));
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            //ignore
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ExpandableListView elvInvited = (ExpandableListView) findViewById(R.id.invites);
        invitedAdapter = new InvitationExpandableBaseAdapter(this);
        elvInvited.addHeaderView(getLayoutInflater().inflate(R.layout.header_edit_event, null));
        elvInvited.setAdapter(invitedAdapter);
        repeatingSpinner = (Spinner) findViewById(R.id.repeating_spinner);
        etName = (EditText) findViewById(R.id.name);
        etDescription = (EditText) findViewById(R.id.description);
        etLocation = (EditText) findViewById(R.id.location);
        tvInvited = (TextView) findViewById(R.id.invited);
        btnStarts = (Button) findViewById(R.id.btn_startTime);
        btnEnds = (Button) findViewById(R.id.btn_endTime);
        btnDate = (Button) findViewById(R.id.btn_date);
        Button btnRemove = (Button) findViewById(R.id.remove);
        Button btnInvitePeople = (Button) findViewById(R.id.btn_invitePeople);
        btnEndRepeating = (Button) findViewById(R.id.btn_end_repeating);
        btnStarts.setOnClickListener(onTimeClickListener);
        btnEnds.setOnClickListener(onTimeClickListener);
        btnRemove.setOnClickListener(onRemoveClickListener);
        btnDate.setOnClickListener(onDateClickListener);
        btnEndRepeating.setOnClickListener(onDateClickListener);
        btnInvitePeople.setOnClickListener(onInvitePeopleClickListener);

        String[] repeatingIntervals = new String[]{getString(R.string.NEVER), getString(R.string.DAILY), getString(R.string.WEEKLY), getString(R.string.BIWEEKLY)};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.item_text, R.id.textitem, repeatingIntervals);
        repeatingSpinner.setAdapter(adapter);
        repeatingSpinner.setOnItemSelectedListener(onRepeatingItemSelected);

        if (getIntent().hasExtra(EVENT_ID)) {
            getSupportActionBar().setTitle(R.string.Edit_Event);
            long eventId = getIntent().getLongExtra(EVENT_ID, 0);
            event = EventsBusiness.getInstance().getEvent(eventId);
            if (event == null) {
                relaunchActivity();
                return;
            }

            populateFields();
            if (EventsBusiness.getInstance().isPastEvent(event)) {
                btnInvitePeople.setVisibility(View.GONE);
                btnRemove.setVisibility(View.GONE);
                tvInvited.setVisibility(View.GONE);
                findViewById(R.id.invitedLabel).setVisibility(View.GONE);
                etName.setEnabled(false);
                etDescription.setEnabled(false);
                etLocation.setEnabled(false);
                btnStarts.setEnabled(false);
                btnEnds.setEnabled(false);
                btnDate.setEnabled(false);
                btnEndRepeating.setEnabled(false);
                repeatingSpinner.setEnabled(false);
            }
        } else {
            event = new Event();
            event.setDate(new LocalDate());
            event.setStart(new LocalTime());
            event.setEnd(new LocalTime());
            event.setInvitations(new ArrayList<EventInvitation>());
            getSupportActionBar().setTitle(R.string.New_Event);
            btnRemove.setVisibility(View.GONE);
        }

    }

    private void populateFields() {
        etName.setText(event.getName());
        etDescription.setText(event.getNotes());
        etLocation.setText(event.getLocation());
        btnStarts.setText(getString(R.string.Starts) + " " + event.getStart().toString(TIME_PATTERN));
        btnDate.setText(getString(R.string.Date) + " " + event.getDate().toString(DATE_PATTERN));
        btnEnds.setText(getString(R.string.Ends) + " " + event.getEnd().toString(TIME_PATTERN));
        for (EventInvitation invitation : event.getInvitations()) {
            sentInvitationsTo.add(invitation.getChildId());
        }
        if (event.getInterval() != null) {
            btnEndRepeating.setText(event.getEndRecurrence().toString(DATE_PATTERN));
            for (int i = 0; i < repeatingPeriods.length; i++) {
                if (repeatingPeriods[i].equals(event.getInterval())) {
                    repeatingSpinner.setSelection(i);
                    break;
                }
            }
        } else {
            btnEndRepeating.setText(getString(R.string.end_repetition));
        }
        updateInvitationList();
    }

    private void updateInvitationList() {
        invitedAdapter.setInvitations(event.getInvitations());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_event, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_PEOPLE && resultCode == RESULT_OK) {
            long[] childrenIds = data.getLongArrayExtra(ChildSelectionActivity.SELECTED_CHILDREN_IDS);
            selectedChildren = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < childrenIds.length; i++) {
                selectedChildren.add(childrenIds[i]);
                Child child = ChildBusiness.getInstance().getChild(childrenIds[i]);
                sb.append((child.getNickname() != null ? child.getNickname() : child.getName()));
                if (i < childrenIds.length - 1) {
                    sb.append(CommonConstants.STRING_COMMA);
                }
            }
            tvInvited.setText(sb.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        } else if (i == R.id.action_saveEvent && !savingInProgress) {
            savingInProgress = true;
            event.setName(etName.getText().toString());
            event.setNotes(etDescription.getText().toString());
            event.setLocation(etLocation.getText().toString());
            event.setState(Event.OPEN);
            event.getInvitations().clear();
            for (Long id : selectedChildren) {
                EventInvitation invitation = new EventInvitation();
                invitation.setState(EventInvitation.InvitationState.INVITED);
                invitation.setChildId(id);
                event.getInvitations().add(invitation);
            }

            SaveEvent saveEvent = new SaveEvent(event);
            getPendingTasks().add(saveEvent);
            saveEvent.execute();
        }
        return true;
    }

    class SaveEvent extends EventsBusiness.SaveEventTask {
        public SaveEvent(Event event) {
            super(event);
        }

        @Override
        protected void onPostExecute(Event event) {
            if (!checkNetworkException(event, exception)) {
                super.onPostExecute(event);
                finish();
            }
            getPendingTasks().remove(this);
            savingInProgress = false;
        }
    }
}
