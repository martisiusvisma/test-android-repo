package com.kindiedays.teacherapp.ui.newpicture;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.kindiedays.teacherapp.R;

import java.util.Date;

/**
 * Created by pleonard on 14/12/2015.
 */
public class TakeNewPictureFragment extends Fragment {

    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout preview;

    private static final String TAG = TakeNewPictureFragment.class.getName();
    public static final String NEW_PICTURES_DIRECTORY = "/new_pictures";

    private Camera.PictureCallback pictureTakenCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.i(TAG, "Callback (picture taken): " + new Date().getTime());
            ((NewPictureActivity) getActivity()).savePictures(data);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_take_new_picture, null);
        preview = (FrameLayout) v.findViewById(R.id.camera_preview);


        // Add a listener to the Capture button
        Button captureButton = (Button) v.findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (((NewPictureActivity) getActivity()).getPicturesTaken() >= 5) {
                            Toast.makeText(getActivity(), R.string.FivePicsMax, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Log.i(TAG, "Button pressed: " + new Date().getTime());
                        try {
                            // get an image from the camera
                            mCamera.takePicture(null, null, pictureTakenCallback);

                        } catch (Exception e) {
                            Log.e(TakeNewPictureFragment.class.getName(), e.getMessage(), e);
                        }
                    }
                }
        );
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, 50);
        } else {
            setCamera();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 50: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setCamera();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void setCamera() {
        releaseCameraAndPreview();
        mCamera = getCameraInstance();
        if (mCamera == null) {
            Toast.makeText(getContext(), "Unable to open camera", Toast.LENGTH_SHORT).show();
            return;
        }
        Camera.Parameters param;
        param = mCamera.getParameters();
        Camera.Size currentSize = null;

        for (Camera.Size size : param.getSupportedPictureSizes()) {
            if (currentSize == null || currentSize.width <= 1280) {
                currentSize = size;
            }
        }
        if (currentSize != null) {
            param.setPictureSize(currentSize.width, currentSize.height);
        }
        mCamera.setParameters(param);
        mPreview = new CameraPreview(getActivity(), mCamera);
        preview.addView(mPreview);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (null != mCamera) {
            mCamera.release();
            mCamera = null;
        }
        preview.removeView(mPreview);
        mPreview = null;
    }

    public void previewPicture() {
        mCamera.startPreview();
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                Camera.CameraInfo info = new Camera.CameraInfo();
                Camera.getCameraInfo(i, info);
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    c = Camera.open(i); // attempt to get a Camera instance
                    break;
                }
                if (c == null) {
                    c = Camera.open(0);
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "getCameraInstance", e);
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable

    }

    private void releaseCameraAndPreview() {
   //     mPreview.setCamera(null);
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_ok, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_ok) {
            ((NewPictureActivity) getActivity()).openPostPage();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
