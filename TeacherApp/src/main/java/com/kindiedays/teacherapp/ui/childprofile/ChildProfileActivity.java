package com.kindiedays.teacherapp.ui.childprofile;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kindiedays.common.business.AppMenuBusiness;
import com.kindiedays.common.business.PicturesBusiness;
import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.common.pojo.ChildJournalList;
import com.kindiedays.common.pojo.ChildProfile;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.PictureList;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.ui.KindieDaysActivity;
import com.kindiedays.common.ui.MainActivity;
import com.kindiedays.common.ui.journal.JournalPostDisplayer;
import com.kindiedays.common.utils.GlideUtils;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.CarerBusiness;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.ChildHelper;
import com.kindiedays.teacherapp.business.DailyReportBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.JournalBusiness;
import com.kindiedays.teacherapp.business.NotesBusiness;
import com.kindiedays.teacherapp.business.NotificationBusiness;
import com.kindiedays.teacherapp.business.PlaceBusiness;
import com.kindiedays.teacherapp.pojo.Note;
import com.kindiedays.teacherapp.pojo.Notification;
import com.kindiedays.teacherapp.pojo.NotificationList;
import com.kindiedays.teacherapp.ui.childprofile.NotesFragment.NotesInterface;
import com.kindiedays.teacherapp.ui.messages.ChildConversationActivity;
import com.viewpagerindicator.TabPageIndicator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import static com.kindiedays.common.ui.MainActivity.MenuItemsStates.MESSAGES_STATE;

/**
 * Created by pleonard on 19/05/2015.
 */
public class ChildProfileActivity extends KindieDaysActivity implements Observer, NotesInterface,
        JournalPostDisplayer.JournalPostSource {
    public static final String CHILD_ID = "childId";

    private LinearLayout signInButton;
    private LinearLayout signOutButton;
    private LinearLayout placesContainer;
    private LinearLayout groupBackground;
    private LinearLayout groupsContainer;

    private TextView dailyHours;
    private Button groupButton;

    private AboutFragment aboutFragment;
    private InboxFragment inboxFragment;
    private JournalFragment journalFragment;
    private NotesFragment notesFragment;
    private PhotosFragment photosFragment;

    private boolean isUpdating;
    private Child child;

    @SuppressWarnings("squid:S1188")
    View.OnClickListener onSignInClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ChildProfileActivity.this);
            builder.setTitle(getString(R.string.Confirmation)).setMessage(getString(R.string.Are_you_sure_to_mark_in, child.getName())).setCancelable(true)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SigninTask signin = new SigninTask(child, PlaceBusiness.getInstance().getMainPlace().getId());
                            signin.execute();
                            getPendingTasks().add(signin);
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            Dialog dialog = builder.create();
            dialog.show();

        }
    };

    @SuppressWarnings("squid:S1188")
    View.OnClickListener onSignOutClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ChildProfileActivity.this);
            builder.setTitle(getString(R.string.Confirmation)).setMessage(getString(R.string.Are_you_sure_to_mark_out, child.getName())).setCancelable(true)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SignoutTask signout = new SignoutTask(child);
                            signout.execute();
                            getPendingTasks().add(signout);
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            Dialog dialog = builder.create();
            dialog.show();
        }
    };

    View.OnClickListener onCurrentGroupClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            //Toggle change group pseudo dialog
            if (groupBackground.getVisibility() == View.GONE) {
                groupBackground.setVisibility(View.VISIBLE);
            } else {
                groupBackground.setVisibility(View.GONE);
            }
        }
    };

    View.OnClickListener onNewGroupClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Long groupId = (Long) v.getTag();
            //Same group, just dismiss the group overlay
            if (ChildHelper.getCurrentGroup(child) == groupId) {
                groupBackground.setVisibility(View.GONE);
            } else {
                ChangeGroupTask task = new ChangeGroupTask(child, groupId);
                task.execute();
                getPendingTasks().add(task);
            }
        }
    };

    View.OnClickListener onPlaceClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Long placeId = (Long) v.getTag();
            SigninTask task = new SigninTask(child, placeId);
            task.execute();
            getPendingTasks().add(task);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isUpdating = true;
        long childId = getIntent().getExtras().getLong(CHILD_ID);
        child = ChildBusiness.getInstance().getChild(childId);
        setContentView(R.layout.activity_child_profile);

        if (child == null) {
            relaunchActivity();
            return;
        }

        TextView name = (TextView) findViewById(R.id.childName);
        dailyHours = (TextView) findViewById(R.id.dailyHours);
        groupButton = (Button) findViewById(R.id.groupButton);
        ImageView thumbnail = (ImageView) findViewById(R.id.kidPicture);
        placesContainer = (LinearLayout) findViewById(R.id.placesContainer);
        groupBackground = (LinearLayout) findViewById(R.id.groupBackground);
        groupsContainer = (LinearLayout) findViewById(R.id.groupsContainer);

        signInButton = (LinearLayout) getLayoutInflater().inflate(R.layout.button_place_profile, null);
        Button button = (Button) signInButton.findViewById(R.id.button);
        button.setText(getString(R.string.Mark_in));
        button.setOnClickListener(onSignInClick);

        signOutButton = (LinearLayout) getLayoutInflater().inflate(R.layout.button_place_profile, null);
        button = (Button) signOutButton.findViewById(R.id.button);
        button.setText(getString(R.string.Mark_out));
        button.setOnClickListener(onSignOutClick);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        aboutFragment = new AboutFragment();
        inboxFragment = new InboxFragment();
        journalFragment = new JournalFragment();
        notesFragment = new NotesFragment();
        DailyCarerFragment dailyCarerFragment = new DailyCarerFragment();
        photosFragment = PhotosFragment.newInstance(childId);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        List<Fragment> fragments = new ArrayList<>(Arrays.asList(inboxFragment,
                aboutFragment, notesFragment, journalFragment));
        List<String> titles = new ArrayList<>(Arrays.asList(getString(R.string.Inbox), getString(R.string.About), getString(R.string.Notes), getString(R.string.Journal)));

        long groupId = child.getTemporaryGroupId();
        if (groupId == 0) {
            groupId = child.getGroupId();
        }
        Group group = GroupBusiness.getInstance().getGroup(groupId);
        if (DailyReportBusiness.getInstance().getSettings() != null && DailyReportBusiness.getInstance().getSettings().isEnabled() && group != null && group.isDailyReportEnabled()) {
            fragments.add(dailyCarerFragment);
            titles.add(getString(R.string.Daily_Report));
        }
        if (AppMenuBusiness.getInstance().getMenuStates() != null && AppMenuBusiness.getInstance().getMenuStates().isCameraEnabled()) {
            fragments.add(photosFragment);
            titles.add(getString(R.string.Photos));

        }
        viewPager.setAdapter(new TabFragmentAdapter(getSupportFragmentManager(), fragments, titles));
        TabPageIndicator tpi = (TabPageIndicator) findViewById(R.id.titles);
        tpi.setViewPager(viewPager);

        initPlacesButtons();
        if (group != null) {
            initGroup(group);
        }
        initGroupButtons();

        name.setText(child.getName());
        groupButton.setOnClickListener(onCurrentGroupClickListener);

        Glide.with(this).load(child.getProfilePictureUrl()).bitmapTransform(GlideUtils.roundTransformation).into(thumbnail);
        ChildProfileTask task = new ChildProfileTask(childId);
        task.execute();
        getPendingTasks().add(task);

        ChildCarerTask carerTask = new ChildCarerTask(childId);
        carerTask.execute();
        getPendingTasks().add(carerTask);

        GetNotesTaskTeacher notesTask = new GetNotesTaskTeacher(childId);
        notesTask.execute();
        getPendingTasks().add(notesTask);

        GetPicturesTask picturesTask = new GetPicturesTask(childId);
        picturesTask.execute();
        getPendingTasks().add(picturesTask);

        LoadNotificationsTaskTeacher notificationsTask = new LoadNotificationsTaskTeacher(childId);
        notificationsTask.execute();
        getPendingTasks().add(notificationsTask);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_createNewMessage:
                if (MESSAGES_STATE.getState()) {
                    Intent intent = new Intent(this, ChildConversationActivity.class);
                    intent.putExtra(ChildConversationActivity.CHILD_ID, child.getId());
                    startActivity(intent);
                } else {
                    MainActivity.lockedMenuItemDialogShow(this);
                }

                break;
            case R.id.action_openCalendarPicker:
                handleOpenCalendarPicker();
                return true;
            default:
                break;
        }
        return true;
    }

    private void handleOpenCalendarPicker() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog timePickerDialog = new DatePickerDialog(this, R.style.datePickerStyle, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                String input = String.format(Locale.getDefault(), "%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                DateTime dateTime = formatter.parseDateTime(input);
                DailyCarerFragment fragment = (DailyCarerFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":4");
                if (fragment != null && fragment.getView() != null) {
                    fragment.refreshDailyReportData(dateTime);
                }
            }
        }, year, month, day);
        timePickerDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ChildBusiness.getInstance().addObserver(this);
        NotificationBusiness.getInstance().addObserver(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //New journal posts may be added from the new journal post activity
        GetJournalTask journalTask = new GetJournalTask(child.getId());
        journalTask.execute();
        getPendingTasks().add(journalTask);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ChildBusiness.getInstance().deleteObserver(this);
        NotificationBusiness.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        initPlacesButtons();
        long groupId = child.getTemporaryGroupId();
        if (groupId == 0) {
            groupId = child.getGroupId();
        }
        Group group = GroupBusiness.getInstance().getGroup(groupId);
        if (group != null) {
            initGroup(group);
        }
    }

    @Override
    public void saveNote(Note note) {
        if (note.getId() == 0) {
            SaveNewNoteTask task = new SaveNewNoteTask(note);
            getPendingTasks().add(task);
            task.execute();
        } else {
            SaveEditedNoteTask task = new SaveEditedNoteTask(note);
            getPendingTasks().add(task);
            task.execute();
        }
    }

    @Override
    public void editNote(Note note) {
        EditNoteDialogFragment dialog = new EditNoteDialogFragment();
        dialog.setNote(note);
        dialog.show(getSupportFragmentManager(), "EditNoteDialog");
    }

    @Override
    public void deleteNote(Note note) {
        DeleteNoteTaskTeacher task = new DeleteNoteTaskTeacher(note);
        getPendingTasks().add(task);
        task.execute();
    }

    private void initGroup(@NonNull Group group) {
        groupButton.setText(group.getName());
    }

    private void initGroupButtons() {
        List<Group> groups = GroupBusiness.getInstance().getContainers(false);
        LayoutInflater inflater = getLayoutInflater();
        for (Group group : groups) {
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.button_group_profile, null);
            Button button = (Button) layout.findViewById(R.id.button);
            button.setText(group.getName());
            groupsContainer.addView(layout);
            button.setOnClickListener(onNewGroupClickListener);
            button.setTag(group.getId());
        }
    }

    private void initPlacesButtons() {
        placesContainer.removeAllViews();
        LayoutInflater inflater = getLayoutInflater();
        if (ChildHelper.getStatus(child) == ChildHelper.SIGNED_IN) {
            placesContainer.addView(signOutButton);
            List<Place> places = PlaceBusiness.getInstance().getContainers(false);
            for (Place place : places) {
                LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.button_place_profile, null);
                Button button = (Button) layout.findViewById(R.id.button);
                button.setText(place.getName());
                placesContainer.addView(layout);
                if (place.getId() == child.getCurrentPlaceId()) {
                    button.setSelected(true);
                    button.setTextColor(getResources().getColor(R.color.white));
                } else {
                    button.setTag(place.getId());
                    button.setOnClickListener(onPlaceClickListener);
                }
            }
        } else { //Absent or signed out
            placesContainer.addView(signInButton);
        }

    }

    public void markNotificationAsRead(Notification notification, int position) {
        MarkNotificationAsReadTask task = new MarkNotificationAsReadTask(notification.getSelfLink(), position);
        task.execute();
        getPendingTasks().add(task);
    }

    public Child getChild() {
        return child;
    }

    public boolean isUpdating() {
        return isUpdating;
    }

    @Override
    public void requestMoreJournalPosts() {
        GetMoreJournalPostsTask task = new GetMoreJournalPostsTask();
        task.execute();
        getPendingTasks().add(task);
    }

    @Override
    public Context getContext() {
        return this;
    }

    private class ChildProfileTask extends ChildBusiness.LoadChildProfile {
        ChildProfileTask(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(ChildProfile childProfile) {
            if (!checkNetworkException(childProfile, exception)) {
                super.onPostExecute(childProfile);
                aboutFragment.setChild(childProfile);
                if (childProfile != null && (childProfile.getDropoffTime() != null || childProfile.getPickupTime() != null)) {
                    dailyHours.setText((childProfile.getDropoffTime() != null ? childProfile.getDropoffTime().toString("HH:mm") : "?") + " -  " + (childProfile.getPickupTime() != null ? childProfile.getPickupTime().toString("HH:mm") : "?"));
                }
            }
            getPendingTasks().remove(this);
        }
    }


    private class ChildCarerTask extends CarerBusiness.LoadCarers {
        ChildCarerTask(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(List<ChildCarer> childCarers) {
            if (!checkNetworkException(childCarers, exception) && childCarers != null) {
                super.onPostExecute(childCarers);
                aboutFragment.setCarers(childCarers);
            }
            getPendingTasks().remove(this);
        }
    }


    private class GetNotesTaskTeacher extends NotesBusiness.GetNotesTask {
        GetNotesTaskTeacher(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(List<Note> notes) {
            if (!checkNetworkException(notes, exception)) {
                super.onPostExecute(notes);
                notesFragment.setNotes(notes);
            }
            getPendingTasks().remove(this);
        }
    }


    private class GetMoreJournalPostsTask extends JournalBusiness.GetMoreChildJournalPostsAsyncTask {
        GetMoreJournalPostsTask() {
            super(JournalBusiness.BEFORE);
        }

        @Override
        protected void onPostExecute(ChildJournalList childJournalList) {
            if (!checkNetworkException(childJournalList, exception)) {
                super.onPostExecute(childJournalList);
                journalFragment.setJournalPosts(JournalBusiness.getInstance().getJournalPostMap());
                journalFragment.setPlaces(PlaceBusiness.getInstance().getContainers(false));
            }
            getPendingTasks().remove(this);
        }
    }


    private class GetJournalTask extends JournalBusiness.GetChildJournalPostsAsyncTask {
        GetJournalTask(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(ChildJournalList journalPosts) {
            if (!checkNetworkException(journalPosts, exception)) {
                super.onPostExecute(journalPosts);
                journalFragment.setJournalPosts(JournalBusiness.getInstance().getJournalPostMap());
                journalFragment.setPlaces(PlaceBusiness.getInstance().getContainers(false));
            }
            getPendingTasks().remove(this);
        }
    }


    private class SaveEditedNoteTask extends NotesBusiness.PutNotesTask {
        Note note;

        SaveEditedNoteTask(Note note) {
            super(note.getId(), note.getTitle(), note.getText());
            this.note = note;
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            notesFragment.updateNote(note);
            getPendingTasks().remove(this);
        }
    }


    private class SaveNewNoteTask extends NotesBusiness.PostNotesTask {
        SaveNewNoteTask(Note note) {
            super(child.getId(), note.getTitle(), note.getText());
        }

        @Override
        protected void onPostExecute(Note note) {
            if (!checkNetworkException(note, exception)) {
                super.onPostExecute(note);
                notesFragment.addNote(note);
            }
            getPendingTasks().remove(this);
        }
    }


    private class DeleteNoteTaskTeacher extends NotesBusiness.DeleteNoteTask {
        DeleteNoteTaskTeacher(Note note) {
            super(note);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
                notesFragment.deleteNote(note);
            }
            getPendingTasks().remove(this);
        }
    }


    private class SigninTask extends ChildBusiness.SigninChildren {
        SigninTask(Child child, long placeId) {
            super(child, placeId);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            getPendingTasks().remove(this);
        }
    }


    private class SignoutTask extends ChildBusiness.SignoutChildren {
        SignoutTask(Child child) {
            super(child);
        }

        @Override
        protected void onPostExecute(Void nothing) {
            if (!checkNetworkException(nothing, exception)) {
                super.onPostExecute(nothing);
            }
            getPendingTasks().remove(this);
        }
    }


    private class ChangeGroupTask extends ChildBusiness.ChangeGroupTemporarily {
        ChangeGroupTask(Child child, long groupId) {
            super(child, groupId);
        }

        @Override
        protected void onPostExecute(String url) {
            super.onPostExecute(url);
            if (!isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && !isDestroyed())) {
                groupBackground.setVisibility(View.GONE);
            }
            if (exception != null) {
                Toast.makeText(ChildProfileActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
            getPendingTasks().remove(this);
        }
    }


    private class GetPicturesTask extends PicturesBusiness.GetPicturesForChildAsyncTask {
        GetPicturesTask(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(PictureList pictureList) {
            if (!checkNetworkException(pictureList, exception)) {
                super.onPostExecute(pictureList);
                photosFragment.setPictureList(pictureList.getPictures());
            }
            getPendingTasks().remove(this);
        }
    }


    private class LoadNotificationsTaskTeacher extends NotificationBusiness.LoadNotificationsTask {
        LoadNotificationsTaskTeacher(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(NotificationList notificationList) {
            if (!checkNetworkException(notificationList, exception)) {
                super.onPostExecute(notificationList);
                Log.d(LoadNotificationsTaskTeacher.class.getSimpleName(), "onPostExecute: " + notificationList.getLinkNext());
                inboxFragment.setNotifications(notificationList.getNotifications());
            }
            getPendingTasks().remove(this);
            isUpdating = false;
        }
    }


    private class MarkNotificationAsReadTask extends NotificationBusiness.MarkNotificationReadAsyncTask {
        int position;

        MarkNotificationAsReadTask(String link, int position) {
            super(link);
            this.position = position;
        }

        @Override
        protected void onPostExecute(Notification notification) {
            if (!checkNetworkException(notification, exception)) {
                super.onPostExecute(notification);
                inboxFragment.updateNotification(notification, position);
            }
            getPendingTasks().remove(this);
        }
    }

    public void callMoreNotificationsFromMainActitivty(String url) {
        GetMoreNotificationsAsyncTask task = new GetMoreNotificationsAsyncTask(url);
        getPendingTasks().add(task);
        task.execute();
    }

    private class GetMoreNotificationsAsyncTask extends NotificationBusiness.LoadMoreNotificationsTask {
        GetMoreNotificationsAsyncTask(String url) {
            super(url);
        }

        @Override
        protected void onPreExecute() {
            isUpdating = true;
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(NotificationList notificationList) {
            if (!checkNetworkException(notificationList, exception)) {
                super.onPostExecute(notificationList);
                inboxFragment.updateNotifications(notificationList);
            }
            getPendingTasks().remove(this);
            isUpdating = false;
        }
    }
}
