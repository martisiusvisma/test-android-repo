package com.kindiedays.teacherapp.ui.dailyreport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.ChildContainer;

import java.util.List;

/**
 * Created by pleonard on 15/05/2015.
 */
@SuppressWarnings("squid:S2326")
public abstract class DRChildrenContainerAdapter<T extends ChildContainer> extends RecyclerView.Adapter<DRChildrenContainerAdapter.ItemViewHolder> {

    protected final Context mContext;
    private List<Child> mChildren;

    @SuppressWarnings("squid:S1214")
    public interface DailyReportInterface {
        int MODE_NORMAL = 1;
        int MODE_MOVE = 2;
    }

    public DRChildrenContainerAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public void onBindViewHolder(DRChildrenContainerAdapter.ItemViewHolder holder, int position) {
        Child child = mChildren.get(position);
        ((DRChildButton) holder.itemView).setChild(child);
        holder.itemView.setTag(child);
    }


    @Override
    public int getItemCount() {
        if (mChildren == null) {
            return 0;
        }
        return mChildren.size();
    }

    public void setChildren(List<Child> children) {
        mChildren = children;
        notifyDataSetChanged();
    }

    public void addItem(int position, Child child) {
        mChildren.add(position, child);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mChildren.remove(position);
        notifyItemRemoved(position);
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public ItemViewHolder(DRChildButton button) {
            super(button);
        }
    }
}
