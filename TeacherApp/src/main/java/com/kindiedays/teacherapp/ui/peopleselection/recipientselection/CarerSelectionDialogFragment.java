package com.kindiedays.teacherapp.ui.peopleselection.recipientselection;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.kindiedays.common.pojo.ChildCarer;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.CarerBusiness;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pleonard on 24/06/2015.
 */
public class CarerSelectionDialogFragment extends DialogFragment {

    private ListView carerList;
    private CarerAdapter carerAdapter;
    private long childId;
    private SelectedCarersIdRecipient owner;
    public static final String CHILD_ID = "childId";
    private Set<AsyncTask> pendingTasks = new HashSet<>();

    View.OnClickListener onOkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getDialog().dismiss();
        }
    };

    public void setOwner(SelectedCarersIdRecipient owner) {
        this.owner = owner;
    }

    public interface SelectedCarersIdRecipient {
        void setSelectedCarersForChild(long childId, Set<Long> selectedCarers);

        Set<Long> getSelectedCarersForChild(long childId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getActivity().getString(R.string.Select_Carer));
        View v = inflater.inflate(R.layout.dialog_carer_selection, null);
        carerList = (ListView) v.findViewById(R.id.carerList);
        carerAdapter = new CarerAdapter(getActivity());
        carerList.setAdapter(carerAdapter);
        carerList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        Button okButton = (Button) v.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickListener);
        GetCarers task = new GetCarers(childId);
        pendingTasks.add(task);
        task.execute();
        return v;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        childId = getArguments().getLong(CHILD_ID);
    }

    private class GetCarers extends CarerBusiness.LoadCarers {
        public GetCarers(long childId) {
            super(childId);
        }

        @Override
        protected void onPostExecute(List<ChildCarer> childCarers) {
            super.onPostExecute(childCarers);
            carerAdapter.setCarers(childCarers);
            Set<Long> preselectedCarers = owner.getSelectedCarersForChild(childId);
            if (preselectedCarers != null) {
                for (int j = 0; j < carerAdapter.getCount(); j++) {
                    carerList.setItemChecked(j, preselectedCarers.contains(carerAdapter.getItemId(j)));
                }
            }
            pendingTasks.remove(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        for (AsyncTask task : pendingTasks) {
            task.cancel(true);
        }
        pendingTasks.clear();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Set<Long> selectedIds = new HashSet<>();
        long[] selectedIdsArray = carerList.getCheckedItemIds();
        for (int i = 0; i < selectedIdsArray.length; i++) {
            selectedIds.add(selectedIdsArray[i]);
        }
        owner.setSelectedCarersForChild(childId, selectedIds);
    }
}
