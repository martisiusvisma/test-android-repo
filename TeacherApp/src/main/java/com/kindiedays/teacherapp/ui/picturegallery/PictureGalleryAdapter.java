package com.kindiedays.teacherapp.ui.picturegallery;

import android.support.v4.app.FragmentActivity;

import com.kindiedays.common.ui.picturegallery.GalleryPictureFragment;

import static com.kindiedays.common.utils.constants.CommonConstants.STRING_EMPTY;

/**
 * Created by pleonard on 15/12/2015.
 */
public class PictureGalleryAdapter extends com.kindiedays.common.ui.picturegallery.PictureGalleryAdapter {
    public PictureGalleryAdapter(FragmentActivity activity) {
        super(activity);
    }

    public PictureGalleryAdapter(FragmentActivity activity, String blurringChild) {
        super(activity, blurringChild);
    }

    @Override
    public GalleryPictureFragment getItem(int position) {
        GalleryPictureFragment pictureFragment = new PictureFragment();
        String imageParams = blurringChild != null ? blurringChild : STRING_EMPTY;
        String url = pictures.get(position).getUrl() + imageParams;
        pictures.get(position).setUrl(url);
        pictureFragment.setPicture(pictures.get(position));
        return pictureFragment;
    }
}
