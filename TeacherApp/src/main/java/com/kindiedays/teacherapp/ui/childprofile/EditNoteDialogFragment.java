package com.kindiedays.teacherapp.ui.childprofile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.TeacherBusiness;
import com.kindiedays.teacherapp.pojo.Note;

/**
 * Created by pleonard on 09/06/2015.
 */
public class EditNoteDialogFragment extends DialogFragment {

    EditText titleEdit;
    EditText bodyEdit;
    Button okButton;
    Button deleteNoteButton;
    Note note;

    View.OnClickListener onDeleteNoteClickedListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            ((NotesFragment.NotesInterface) getActivity()).deleteNote(note);
            dismiss();
        }
    };

    View.OnClickListener onSaveNoteClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            note.setText(bodyEdit.getText().toString());
            note.setTitle(titleEdit.getText().toString());
            ((NotesFragment.NotesInterface) getActivity()).saveNote(note);
            dismiss();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_edit_note, null);
        titleEdit = (EditText) v.findViewById(R.id.titleEdit);
        bodyEdit = (EditText) v.findViewById(R.id.bodyEdit);
        okButton = (Button) v.findViewById(R.id.buttonSaveNote);
        deleteNoteButton = (Button) v.findViewById(R.id.buttonDeleteNote);
        if (note.getId() == 0) {
            deleteNoteButton.setVisibility(View.GONE);
        }
        titleEdit.setText(note.getTitle());
        bodyEdit.setText(note.getText());
        okButton.setOnClickListener(onSaveNoteClickedListener);
        deleteNoteButton.setOnClickListener(onDeleteNoteClickedListener);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_rounded_edittext);
        if (note.getAuthorTeacherId() != null && note.getAuthorTeacherId() != TeacherBusiness.getInstance().getCurrentTeacher().getId()) {
            titleEdit.setEnabled(false);
            bodyEdit.setEnabled(false);
            okButton.setVisibility(View.GONE);
            deleteNoteButton.setVisibility(View.GONE);
        }
        return v;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
