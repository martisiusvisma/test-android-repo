package com.kindiedays.teacherapp.ui.attendance;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.kindiedays.common.pojo.Child;
import com.kindiedays.common.pojo.Group;
import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.common.ui.ChildTouchListener;
import com.kindiedays.common.ui.SectionedGridRecyclerViewAdapter;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.ChildHelper;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PlaceBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by pleonard on 22/04/2015.
 */
public class PlaceFragment extends Fragment implements ChildTouchListener.OnChildButtonTouchListener, Observer {

    public static final String PLACE_ID = "placeId";
    private static final String RECYCLE_STATE = "recycle_state";

    private long placeId;
    private RecyclerView childrenList;
    private TextView tvPlace;
    private Place place;
    private EditText searchField;
    private ChildrenGroupAdapter childrenAdapter;
    private View header;
    private TextView selectDeselectTV;
    private AttendanceFragment owner;

    public interface OnMovedListenerGroup{
        void onMoved();
    }

    private OnMovedListenerGroup onMovedListener = new OnMovedListenerGroup() {
        @Override
        public void onMoved() {
            selectDeselectTV.setText(R.string.select_tv);
            owner.cancelMove(null);
        }
    };

    public void saveScrolledState() {
        getArguments().putParcelable(RECYCLE_STATE, childrenList.getLayoutManager().onSaveInstanceState());
    }

    TextWatcher searchTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            //no impl
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            //no impl
        }

        @Override
        public void afterTextChanged(Editable editable) {
            //Trigger new search
            updateSections();
            searchField.requestFocus();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            placeId = getArguments().getLong(PLACE_ID);
        } else {
            placeId = -1;
        }
        owner = (AttendanceFragment) getParentFragment().getParentFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        owner.setOnMovedListenerGroup(onMovedListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_recyclerview, null);
        childrenList = (RecyclerView) v.findViewById(R.id.childrenList);

        header = inflater.inflate(R.layout.header_container_list, null, false);
        tvPlace = (TextView) header.findViewById(R.id.containerName);
        searchField = (EditText) header.findViewById(R.id.search);
        searchField.addTextChangedListener(searchTextWatcher);
        place = PlaceBusiness.getInstance().getPlace(placeId);
        if (place != null) {
            tvPlace.setText(place.getName());
        }
        tvPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AttendanceFragment owner = (AttendanceFragment) getParentFragment().getParentFragment();
                ChildBusiness.getInstance().selectChildrenFromPlace(placeId, true, true, TeacherBusiness.getInstance().getCurrentTeacher().getGroups());
                owner.enterMoveMode();
            }
        });

        selectDeselectTV = (TextView) header.findViewById(R.id.select_deselect_tv);
        selectDeselectTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveScrolledState();
                if (selectDeselectTV.getText().equals(getString(R.string.select_tv))){
                    selectDeselectTV.setText(R.string.deselect_tv);
                    owner.enterMoveMode();
                } else {
                    selectDeselectTV.setText(R.string.select_tv);
                    owner.cancelMove(null);
                }
            }
        });

        childrenList.setHasFixedSize(true);
        childrenList.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        childrenAdapter = new ChildrenGroupAdapter(getActivity());

        updateSections();
        childrenList.addOnItemTouchListener(new ChildTouchListener(this));

        return v;
    }

    @SuppressWarnings("squid:S134")
    private void updateSections() {
        List<SectionedGridRecyclerViewAdapter.Section> sections = new ArrayList<>();
        Teacher currentTeacher = TeacherBusiness.getInstance().getCurrentTeacher();
        if (currentTeacher != null) {
            List<Group> groups = GroupBusiness.getInstance().getTeacherGroups(currentTeacher, false);
            int currentPosition = 0;
            List<Child> children = new ArrayList<>();

            if (groups != null) {
                for (Group _group : groups) {
                    List<Child> childrenForGroup = ChildBusiness.getInstance().getChildren(_group.getId(), placeId, searchField.getText().toString(), false, currentTeacher.getGroups());
                    if (!childrenForGroup.isEmpty()) {
                        sections.add(new SectionedGridRecyclerViewAdapter.Section(currentPosition, new String[]{_group.getName()}, _group));
                        currentPosition += childrenForGroup.size();
                        children.addAll(childrenForGroup);
                    }
                }
            }

            childrenAdapter.setChildren(children);
        }

        SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
        SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                SectionedGridRecyclerViewAdapter(getActivity(), R.layout.item_group_section, new int[]{R.id.groupName}, childrenList, childrenAdapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));
        mSectionedAdapter.setHeader(header);
        childrenList.setAdapter(mSectionedAdapter);

        Bundle args = getArguments();
        if (args != null) {
            Parcelable recycleViewState = getArguments().getParcelable(RECYCLE_STATE);
            childrenList.getLayoutManager().onRestoreInstanceState(recycleViewState);
        } else if (childrenList.getAdapter().getItemCount() > 0) {
            childrenList.scrollToPosition(1);
        }
    }

    @Override
    public void onChildButtonTapped(View childButton) {
        saveScrolledState();
        AttendanceFragment owner = (AttendanceFragment) getParentFragment().getParentFragment();
        if (childButton != null && childButton.getTag() instanceof Child) {
            ChildAttendanceButton childAttendanceButton = (ChildAttendanceButton) childButton;
            if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_NORMAL) {
                Child child = childAttendanceButton.getChild();
                owner.openChildProfile(child.getId());

            } else if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_MOVE) {
                ChildBusiness.getInstance().selectChild(childAttendanceButton.getChild().getId(), !childAttendanceButton.getChild().isSelected()); //same selection state than button
                childAttendanceButton.changeSelectionState();

            }
        }
        if (childButton != null && childButton.getTag() instanceof Group) {
            selectDeselectTV.setText(R.string.deselect_tv);
            //Select all signed in children
            ChildBusiness.getInstance().selectChildrenFromGroupAndPlace(((Group) childButton.getTag()).getId(), place.getId(), searchField.getText().toString(), true, true, TeacherBusiness.getInstance().getCurrentTeacher().getGroups());

            //Display move to dialog
            owner.enterMoveMode();
        }
    }

    @Override
    @SuppressWarnings("squid:S134")
    public void onChildButtonLongPress(View v) {
        if (v instanceof ChildAttendanceButton) {
            ChildAttendanceButton childAttendanceButton = (ChildAttendanceButton) v;
            AttendanceFragment owner = (AttendanceFragment) getParentFragment().getParentFragment();
            if (owner.getMode() == ChildrenAttendanceContainerAdapter.AttendanceInterface.MODE_NORMAL) {
                Child child = childAttendanceButton.getChild();
                long newPlaceId = 0;
                if (ChildHelper.getStatus(child) == ChildHelper.SIGNED_IN && PlaceBusiness.getInstance().getPlace(child.getCurrentPlaceId()).isNapRoom()) {
                    if (child.isNapping()) {
                        //Wake up
                        owner.endNap(child);
                    } else {
                        //Sweet dreams
                        owner.startNap(child);
                    }
                } else {
                    switch (ChildHelper.getStatus(child)) {
                    case ChildHelper.ABSENT:
                    case ChildHelper.SIGNED_OUT:
                        newPlaceId = PlaceBusiness.getInstance().getMainPlace().getId();
                        owner.signIn(child, newPlaceId);
                        child.setCurrentPlaceId(newPlaceId);
                        break;
                    case ChildHelper.SIGNED_IN:
                        child.setCurrentPlaceId(PlaceBusiness.NO_PLACE_ID);
                        owner.signOut(child);
                        break;
                    default:
                        break;
                    }

                    saveScrolledState();
                    childAttendanceButton.setChild(child);
                    childAttendanceButton.launchAnimation();
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ChildBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ChildBusiness.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ChildBusiness) {
            updateSections();
        }
    }
}
