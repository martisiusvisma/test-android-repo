package com.kindiedays.teacherapp.ui.moveto;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.kindiedays.common.pojo.Place;
import com.kindiedays.teacherapp.business.PlaceBusiness;

/**
 * Created by pleonard on 25/05/2015.
 */
public class MoveToPlaceDialog extends MoveToContainerDialog<Place> {

    public MoveToPlaceDialog(Context context) {
        super(context, PlaceBusiness.getInstance());
    }

    public MoveToPlaceDialog(Context context, AttributeSet attrs) {
        super(context, attrs, PlaceBusiness.getInstance());
    }

    public MoveToPlaceDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, PlaceBusiness.getInstance());
    }


    @Override
    protected void containerSpecificInitViews(View v) {
        //no impl
    }
}
