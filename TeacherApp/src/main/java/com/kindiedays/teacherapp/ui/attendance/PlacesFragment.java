package com.kindiedays.teacherapp.ui.attendance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TabHost;

import com.kindiedays.common.pojo.Place;
import com.kindiedays.common.pojo.Teacher;
import com.kindiedays.teacherapp.R;
import com.kindiedays.teacherapp.business.ChildBusiness;
import com.kindiedays.teacherapp.business.GroupBusiness;
import com.kindiedays.teacherapp.business.PlaceBusiness;
import com.kindiedays.teacherapp.business.TeacherBusiness;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

/**
 * Created by pleonard on 22/04/2015.
 */
public class PlacesFragment extends Fragment implements Observer {
    private static final String ADD_PLACE_TAG = "addPlace";

    private boolean placesReady = false;
    private boolean childrenReady = false;
    private boolean groupsReady = false;

    FragmentTabHost placeTabHost;
    ProgressBar progressBar;
    PlaceTabIndicator addPlace;
    AttendanceFragment owner;
    View v;

    private Set<String> addedTabs = new LinkedHashSet<>();

    View.OnClickListener onAddPlaceClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AddPlaceDialogFragment dialog = new AddPlaceDialogFragment();
            dialog.setOwner(owner);
            dialog.show(getActivity().getSupportFragmentManager(), ADD_PLACE_TAG);
            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(ADD_PLACE_TAG);
        }
    };

    public void setOwner(AttendanceFragment owner) {
        this.owner = owner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_places, container, false);
        placeTabHost = (FragmentTabHost) v.findViewById(R.id.groupTabHost);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        addPlace = (PlaceTabIndicator) v.findViewById(R.id.addPlace);
        addPlace.setCurrent("+");
        addPlace.setPlaceName(getString(R.string.Add_Location));
        addPlace.setOnClickListener(onAddPlaceClickListener);

        placeTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        PlaceBusiness.getInstance().addObserver(this);
        ChildBusiness.getInstance().addObserver(this);
        GroupBusiness.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        PlaceBusiness.getInstance().deleteObserver(this);
        ChildBusiness.getInstance().deleteObserver(this);
        GroupBusiness.getInstance().deleteObserver(this);
    }

    private PlaceTabIndicator createTabView(int current, String placeName) {
        return createTabView(String.valueOf(current), placeName);
    }

    private PlaceTabIndicator createTabView(String current, String placeName) {
        PlaceTabIndicator indicator = new PlaceTabIndicator(getActivity());
        indicator.setCurrent(current);
        indicator.setPlaceName(placeName);
        return indicator;
    }

    private void refreshPlacesTabs() {
        Teacher currentTeacher = TeacherBusiness.getInstance().getCurrentTeacher();
        if (currentTeacher != null) {
            progressBar.setVisibility(View.GONE);
            placeTabHost.setVisibility(View.VISIBLE);
            List<Place> places = PlaceBusiness.getInstance().getContainers(true);
            for (int i = 0; i < places.size(); i++) {
                Place place = places.get(i);
                int present = ChildBusiness.getInstance().getNumberPresentChildrenInPlace(place.getId(), currentTeacher.getGroups());

                //* this, selected part, is for localisation of "Absent", forgive me, I had no choice
                if (place.getName().equals(String.valueOf(R.string.Absent)) || place.getNameRes() == R.string.Absent){
                    place.setName(getString(R.string.Absent));
                    place.setNameRes(R.string.Absent);
                }
                //*

                TabHost.TabSpec ts = placeTabHost.newTabSpec("PlaceSubtab " + place.getName())
                        .setIndicator(createTabView(present, place.getName()));

                if (!addedTabs.contains(ts.getTag())) {
                    addedTabs.add(ts.getTag());

                    Bundle bundle = new Bundle();
                    bundle.putLong(PlaceFragment.PLACE_ID, place.getId());
                    placeTabHost.addTab(ts, PlaceFragment.class, bundle);
                } else {
                    int trueIndex = new ArrayList<>(addedTabs).indexOf(ts.getTag());
                    PlaceTabIndicator tabIndicator = (PlaceTabIndicator) placeTabHost.getTabWidget().getChildTabViewAt(trueIndex);
                    tabIndicator.setCurrent(String.valueOf(present));
                }
            }

            int currentTab = placeTabHost.getCurrentTab();
            if (currentTab < placeTabHost.getTabWidget().getTabCount()) {
                placeTabHost.setCurrentTab(currentTab);
            }
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof PlaceBusiness) {
            placesReady = true;
        }
        if (observable instanceof ChildBusiness && data != null) {
            if (data instanceof Set && !((Set) data).isEmpty())
                childrenReady = true;
        }
        if (observable instanceof GroupBusiness) {
            groupsReady = true;
        }
        if (placesReady && childrenReady && groupsReady) {
            refreshPlacesTabs();
        }

    }
}
