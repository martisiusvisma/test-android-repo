package com.kindiedays.teacherapp.pushnotifications;

import com.kindiedays.teacherapp.ui.MainActivity;

/**
 * Created by pleonard on 17/09/2015.
 */
public class BroadcastReceiver extends com.kindiedays.common.pushnotifications.BroadcastReceiver {

    public BroadcastReceiver() {
        super();
        clazz = MainActivity.class;
    }
}
